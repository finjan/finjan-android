package com.electrolyte.utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class DateHelper {

//    private static final String serverFormat="dd/MM/yyyy hh:mm:ss.SSS";
    private static final String serverFormat="yyyy-MM-dd HH:mm:ss";
    private static final class DateHelperHolder{
        private static final DateHelper INSTANCE=new DateHelper();
    }
    public static DateHelper getInstnace(){
        return DateHelperHolder.INSTANCE;
    }
    private DateHelper(){};
    public int numofdays(Date date) {
        Date today = new Date();
        return (int) ((today.getTime() - date.getTime()) / (1000 * 60 * 60 * 24));
    }
public boolean isFutureDate(String dateString){
        return isFuture(getLongFromDate(dateString));
}

    public boolean isFuture(long timeToCompare){
        Calendar calendarToCompare=Calendar.getInstance();
        calendarToCompare.setTimeInMillis(timeToCompare);

        Calendar currentCalender=Calendar.getInstance();
        return currentCalender.before(calendarToCompare);
    }
    public String getServerFormatDate(long miliSec){
        return getServerFormatDate(miliSec,SERVER_FORMAT);
    }
    public String getCurrentDate(String format){
        return getServerFormatDate(System.currentTimeMillis(),format);
    }

    public String getServerFormatDate(long miliSec, String format){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliSec);
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.US);
        String serverDate=formatter.format(calendar.getTime());
        if(!TextUtils.isEmpty(serverDate) && serverDate.split("-",1)[0].startsWith("-")){
            formatter.setTimeZone(TimeZone.getTimeZone("gmt"));
        }
        return formatter.format(calendar.getTime());
    }


    public long getLongFromDate(String dateString){
        SimpleDateFormat format = new SimpleDateFormat(SERVER_FORMAT,Locale.US);
//        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            Date date = format.parse(dateString);
            Calendar cal = Calendar.getInstance();

            cal.setTime(date);
            return cal.getTimeInMillis();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
    public boolean isApproxSameTime(long compareDate,String backEndDate){
        long different=compareDate-getLongFromDate(backEndDate);
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;
        return elapsedDays==0 && elapsedHours<10;
    }

    public boolean isFutureDate(String dateToCompare, String dateToCompareWith){
        long longDateToCompare = getLongFromDate(dateToCompare);
        long longDateToCompareWith = getLongFromDate(dateToCompareWith);
        return isFutureDate(longDateToCompare,longDateToCompareWith);
    }

    public boolean isFutureDate(long firstDate,long secondDate ){
        Calendar calendarFirst=Calendar.getInstance();
        calendarFirst.setTimeInMillis(firstDate);
        Calendar calendarSecond=Calendar.getInstance();
        calendarSecond.setTimeInMillis(secondDate);
        return calendarFirst.after(calendarSecond);
    }

    public boolean isNotMoreThan1DayDiff(long compareDate){
        return !isMoreThanDaysDiff(1,compareDate,"",false);
    }
    public boolean isNotMoreThan1DayDiff(String compareDate, String dateToCompareWith){
        return !isMoreThanDaysDiff(1,getLongFromDate(compareDate),dateToCompareWith,true);
    }

    public boolean isMoreThanDaysDiff(int days, long dateToCompare, String stringDateToCompareWith, boolean equalValue){
        long dateToCompareWith = TextUtils.isEmpty(stringDateToCompareWith)?Calendar.getInstance().getTimeInMillis():
                getLongFromDate(stringDateToCompareWith);

        Long difference=dateToCompareWith-dateToCompare;
        difference = Math.abs(difference);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = difference / daysInMilli;
        difference = difference % daysInMilli;
        if(equalValue){
            return elapsedDays>=days;
        }
        return elapsedDays>days;    }

    public boolean isMoreThan3DaysDiff(long dateToCompare,String backEndDate){
        return isMoreThanDaysDiff(3,dateToCompare,backEndDate,false);
    }
    public static final String SERVER_FORMAT_3= "dd MMM yyyy HH:mm:ss";
    public static final String SERVER_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String CANCLE_PROMO_FORMAT = "yyyy-MM-dd";
    public static final String SERVER_FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public String getCurrentGMTTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);
        formatter.setTimeZone(TimeZone.getTimeZone("gmt"));
        return formatter.format(new Date());
    }
    public Calendar getServerDateCalender(String serverFormat){
        return getServerDateCalender(serverFormat,SERVER_FORMAT);
    }
    public Calendar getServerDateCalender(String serverFormat,String dateFormat){
        SimpleDateFormat format = new SimpleDateFormat(dateFormat,Locale.US);
        try {
            Date date = format.parse(serverFormat);
            Calendar calendar=Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        } catch (ParseException e) {
            e.printStackTrace();
            return Calendar.getInstance();
        }
    }
    public String getCurrentDateMonth(){
        SimpleDateFormat formatter = new SimpleDateFormat("MMM",Locale.US);
        return formatter.format(new Date());
    }

    /**
     * @param monthly     is monthly purchase
     * @param orignalTime orignal purchase time
     * @return future time depends upon monthly/yearly
     */
    public static String getFutureExpireTime(boolean monthly, long orignalTime) {
        return getFutureTime(monthly ? 30 : 365, orignalTime);
    }

    public static String getFutureTime(int days, long orignalTime){
        orignalTime = validateIfCurrentTime(orignalTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(orignalTime);
        calendar.add(Calendar.DATE, days);
        SimpleDateFormat formatter = new SimpleDateFormat(SERVER_FORMAT,Locale.US);
        return formatter.format(calendar.getTime());
    }


    public static long validateIfCurrentTime(long orignalTime){
        if(DateHelper.getInstnace().isNotMoreThan1DayDiff(orignalTime)){
            return orignalTime;
        }else return Calendar.getInstance().getTimeInMillis();
    }

    public String getDateWithoutTime(String stringdate)
    {
        SimpleDateFormat format = new SimpleDateFormat(CANCLE_PROMO_FORMAT,Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            Date date = format.parse(stringdate);

            return format.format(date)+" 00:00:00";
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
      // return stringdate;
    }
    public int getNoOfDaysLeft(String expirydate,String currentdate)
    {
        long expiryDate = 0;
        SimpleDateFormat format = new SimpleDateFormat(CANCLE_PROMO_FORMAT,Locale.US);
        try {
            Date date = format.parse(expirydate);
           /* Calendar cal = Calendar.getInstance();

            cal.setTime(date);
            expiryDate = cal.getTimeInMillis();*/
           expiryDate = getLongFromDate(format.format(date)+" 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long currentDate = 0;
        try {
            currentDate = getLongFromDate(format.format(format.parse(currentdate))+" 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (expiryDate!=0 && !(expiryDate<currentDate))
        {
            return (int) ((expiryDate - currentDate)/(1000*60*60*24));
        }
        else
        return 0;
    }


}
