package com.electrolyte.utils

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat

/**
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *
 * Created by anurag on 12/09/17.
 */


class ResourceHelper{

    companion object {
        lateinit var context: Context

        private object DrawableHelperHolder {
            val instance = ResourceHelper()
        }
        fun getInstance(): ResourceHelper {
            return DrawableHelperHolder.instance
        }
    }

    fun getDrawable(resource: Int): Drawable? {
        return ContextCompat.getDrawable(InitLib.context, resource)
    }

    fun getColor(resource: Int): Int {
        return ContextCompat.getColor(InitLib.context, resource)
    }

    fun getDrawable(setFirstRes: Boolean, firstRes: Int, secoundRes: Int): Drawable? {
        return if (setFirstRes) getDrawable(firstRes) else getDrawable(secoundRes)
    }

    fun getColor(setFirstRes: Boolean, firstRes: Int, secondRes: Int): Int {
        return if (setFirstRes) getColor(firstRes) else getColor(secondRes)
    }

    fun getString(resouce: Int): String {
        return InitLib.context.getString(resouce)
    }

    fun getImageResources(res: String, defaultRes: Int): Int {
        var res = res
        res = res.trim { it <= ' ' }.toLowerCase().replace(" ", "_")
        res = res.trim { it <= ' ' }.toLowerCase().replace("-", "_")
        var country = 0
        try {
            country = InitLib.context.getResources().getIdentifier(res, "drawable",
                    InitLib.context.getPackageName())
        } catch (e: Exception) {
            country = defaultRes
        }

        return country
    }
}