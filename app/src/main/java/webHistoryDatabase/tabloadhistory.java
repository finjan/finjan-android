package webHistoryDatabase;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TABLOADHISTORY".
 */
@Entity
public class tabloadhistory {

    @Id
    private Long id;
    private String tabId="";
    private String url="";
    private String currenturl="";

    @Generated(hash = 852718402)
    public tabloadhistory() {
    }

    public tabloadhistory(Long id) {
        this.id = id;
    }

    @Generated(hash = 96995751)
    public tabloadhistory(Long id, String tabId, String url, String currenturl) {
        this.id = id;
        this.tabId = tabId;
        this.url = url;
        this.currenturl = currenturl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTabId() {
        return tabId;
    }

    public void setTabId(String tabId) {
        this.tabId = tabId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCurrenturl() {
        return currenturl;
    }

    public void setCurrenturl(String currenturl) {
        this.currenturl = currenturl;
    }

}
