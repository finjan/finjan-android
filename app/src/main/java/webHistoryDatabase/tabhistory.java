package webHistoryDatabase;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TABHISTORY".
 */
@Entity
public class tabhistory {

    @Id(autoincrement = true)
    private Long id;
    private String pagetitle="";
    private String url="";
    private String rating="";
    private String country="";
    private String status="";
    private String categories="";
    private String type="";

    @Generated(hash = 539045184)
    public tabhistory() {
    }

    public tabhistory(Long id) {
        this.id = id;
    }

    @Generated(hash = 1381626175)
    public tabhistory(Long id, String pagetitle, String url, String rating, String country, String status, String categories, String type) {
        this.id = id;
        this.pagetitle = pagetitle;
        this.url = url;
        this.rating = rating;
        this.country = country;
        this.status = status;
        this.categories = categories;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPagetitle() {
        return pagetitle;
    }

    public void setPagetitle(String pagetitle) {
        this.pagetitle = pagetitle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
