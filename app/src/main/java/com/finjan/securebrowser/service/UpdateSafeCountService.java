package com.finjan.securebrowser.service;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Base64;

import com.finjan.securebrowser.Constants;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.ModelManager;
import com.finjan.securebrowser.util.FinjanUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static android.util.Base64.encodeToString;
/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class UpdateSafeCountService extends IntentService {
    private String response="";
    private static final String TAG=UpdateSafeCountService.class.getSimpleName();

    public UpdateSafeCountService(){
        super("UpdateSafeCountService");
    }
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public UpdateSafeCountService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent == null  || intent.getExtras() == null||
        intent.getExtras().get("type")==null||
                intent.getExtras().get("uuid")==null) {
            return;
        } else {

            try {
                if(intent.getExtras().get("type")!=null && intent.getExtras().get("uuid")!=null){

                    String type = intent.getExtras().get("type").toString();
                    String uuid = intent.getExtras().get("uuid").toString();
                    HttpURLConnection connection = (HttpURLConnection) new URL(FinjanUrls.URL_safetyCheckURL).openConnection();
                    byte[] authEncBytes = Base64.encode("Madhup:Z8@mIxAq#i(QiQBo".getBytes(), Base64.NO_WRAP);
                    String authStringEnc = new String(authEncBytes);
                    String authToken = "Basic " + authStringEnc;
                    connection.setRequestProperty("Authorization", authToken);
                    connection.setRequestProperty("Content-Type","application/json");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);
                    connection.setConnectTimeout(60 * 1000);
                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getPostDataString(type,uuid));
                    writer.flush();
                    writer.close();
                    os.close();
                    response="";
                    int responseCode = connection.getResponseCode();
                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        while ((line = br.readLine()) != null) {
                            response += line;
                        }
                    }
                    if (!TextUtils.isEmpty(response)){
                        ModelManager.getInstance().setSafeSearchCountModel(response);
//                    Preferences pref = new Preferences(getApplicationContext());
                        if (ModelManager.getInstance().getSafeSearchCountModel()!=null && !TextUtils.isEmpty(ModelManager.getInstance().getSafeSearchCountModel().getCount())){
                            String count = ModelManager.getInstance().getSafeSearchCountModel().getCount();
//                        //~Anurag
                            if(!TextUtils.isEmpty(count)){
                                try{
                                    int countInt=Integer.parseInt(count);
                                    UserPref.getInstance().setSafeScanRemain(countInt);

                                }catch (NumberFormatException e){
                                    Logger.logE(TAG," "+e);
                                }
                            }

//                        if (TextUtils.isEmpty(count)){
//                            if (pref.getInt(Constants.PREF_KEY_SAFE_SCAN_REMAIN)==0){
//                                pref.set(Constants.PREF_KEY_SAFE_SCAN_REMAIN, 0).commit();
//                            }
//                        }else{
//                            pref.set(Constants.PREF_KEY_SAFE_SCAN_REMAIN, Integer.parseInt(count)).commit();
////                            //TODO change this after testing
////                            pref.set(Constants.PREF_KEY_SAFE_SCAN_REMAIN, 2).commit();
//                        }

                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private String getPostDataString(String type,String uuid) throws UnsupportedEncodingException {
        JSONObject parent=new JSONObject();
        try {
            parent.put("action",type);
            parent.put("udid",uuid);
            parent.put("value",type.equalsIgnoreCase(Constants.UPDATE_PARAM)?1:0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parent.toString();
    }
}