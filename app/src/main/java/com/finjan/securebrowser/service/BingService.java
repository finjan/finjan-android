package com.finjan.securebrowser.service;

import com.finjan.securebrowser.model.BingSearchResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public interface BingService {

    // TODO: Make Adult be a Query below, not hard-coded in the POST macro.

    @GET("")
    Call<BingSearchResponse> search(@Header("Ocp-Apim-Subscription-Key") String authorization,
                                    @Url  String url,
                                    @Query("$skip") int resultStart,
                                    @Query("$top") int resultCount,
                                    @Query("responseFilter") String filter);
}
