package com.finjan.securebrowser.service;

import com.finjan.securebrowser.model.SafetyResult;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public interface SafetyService {
//    @FormUrlEncoded
//    @POST("/")
//    void getSafetyReport(@Field("url") String url, @Field("parent") String parent, @Field("version") int version, @Field("use_local_db") int use_local_db,
//                         Callback<SafetyResult> callback);

    @FormUrlEncoded
    @POST("api/v1/validate/")
    Call<SafetyResult> getSafetyReport(@Field("url") String url, @Field("parent") String parent,
                                       @Field("version") int version, @Field("use_local_db") int use_local_db);
}
