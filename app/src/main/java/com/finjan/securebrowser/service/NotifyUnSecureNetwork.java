package com.finjan.securebrowser.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.HomeActivity;
import com.finjan.securebrowser.activity.SplashScreen;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.security.WifiSecurityHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;

/**
 * Created by anurag on 31/10/17.
 */

public class NotifyUnSecureNetwork extends Service {

    private static final String TAG=NotifyUnSecureNetwork.class.getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private Intent getVpnIntent(){
        return new Intent(this, VpnActivity.class);
    }
    private Intent getAppIntent(){
        Intent notificationIntent=null;
        //default behaviour
        notificationIntent = new Intent(this, SplashScreen.class);

        if(UserPref.getInstance().getIsBrowser()){
            if(NativeHelper.getInstnace().checkActivity(HomeActivity.getInstance())){
                notificationIntent = new Intent(this, HomeActivity.class);
            }
        }else if(NativeHelper.getInstnace().checkActivity(VpnActivity.getInstance())) {
            notificationIntent = new Intent(this, VpnActivity.class);
        }
        return notificationIntent;
    }
    private Intent getStartAppIntent(){
        return getAppIntent().setAction(AppConstants.SERVICE_App_OPEN);
    }
    private Intent getStartVpnIntent(){
        return getVpnIntent().setAction(AppConstants.SERVICE_VPN_Open);
    }
    private Intent getConnectVpnIntent(){
        return getVpnIntent().setAction(AppConstants.SERVICE_VPN_Connect);
    }

    private void startNotification(){
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, getStartAppIntent(), 0);
        PendingIntent connectVPN = PendingIntent.getActivity(this, 0, getConnectVpnIntent(), 0);
        PendingIntent openVPN= PendingIntent.getActivity(this, 0, getStartVpnIntent(), 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.app_icon);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.vitalsecurity_alert))
                .setTicker(getString(R.string.your_data_is_on_risk))
                .setContentText(getString(R.string.your_data_is_on_high_risk_as_current_network_is_not_secure))
                .setSmallIcon(R.drawable.icon_vpn)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .addAction(android.R.drawable.ic_menu_view, getString(R.string.open_vpn), openVPN)
                .addAction(android.R.drawable.ic_media_play, getString(R.string.connect_vpn), connectVPN).build();
        startForeground(AppConstants.SERVICE_notification_id, notification);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(!WifiSecurityHelper.getInstance().getNetworkType(getBaseContext()).isSecure()){
            startNotification();
            GlobalVariables.getInstnace().notifyUnSecureNetworkService=this;
        }
        Logger.logE(TAG, "Received Start Foreground Intent ");
        return START_STICKY;
    }
    public static void startServiceWhenRequired(Context context){
        if(UserPref.autoConnectVersion && GlobalVariables.getInstnace().notifyUnSecureNetworkService==null
                 && !WifiSecurityHelper.getInstance().getNetworkType(context).isSecure()){
            context.startService(getNotificationServiceIntent(context));
        }
    }
    public static Intent getNotificationServiceIntent(Context context){
        Intent intent = new Intent(context.getApplicationContext(), NotifyUnSecureNetwork.class);
        intent.setAction(AppConstants.SERVICE_Main_Nofification);
        return intent;
    }

    public static void stopService(){
        if(UserPref.autoConnectVersion &&
                GlobalVariables.getInstnace().notifyUnSecureNetworkService!=null){
            GlobalVariables.getInstnace().notifyUnSecureNetworkService.stopSelf();
            GlobalVariables.getInstnace().notifyUnSecureNetworkService=null;
        }
    }

}
