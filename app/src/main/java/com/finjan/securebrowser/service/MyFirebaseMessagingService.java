/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.finjan.securebrowser.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.SplashScreen;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.VpnUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
//import com.localytics.android.Localytics;

import java.util.Calendar;
import java.util.Map;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class    MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
        Logger.logD(TAG, "new token: " + token);

//        Localytics.setPushRegistrationId(token);

    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String, String> data = remoteMessage.getData();
        Logger.logE("FirebaseMessage", "notification recevied");
        if(remoteMessage!=null && remoteMessage.getData()!=null ){
            if(remoteMessage.getData().containsKey("is_silent_push")){
                if(remoteMessage.getData().get("is_silent_push").equalsIgnoreCase("true")){
                    Logger.logE("FirebaseMessage", "traffic profile updated");
                    LocalyticsTrackers.Companion.getInstance().updateTrafficProfile();
                    if(VpnUtil.isVpnActive(FinjanVPNApplication.getInstance())){
                        LocalyticsTrackers.Companion.getInstance().addVPNONEvent();
                    }else {
                        LocalyticsTrackers.Companion.getInstance().addVPNOFFEvent();
                    }
                    if(PlistHelper.getInstance().shouldShowLocalNotificationForTraffic()){
                        String msg = data.get("message");
                        String traffic = "consumed = "+ TrafficController.getInstance(getApplicationContext()).getConsumedTraffic()+" and updated on lacalytics";
                        if(TextUtils.isEmpty(msg)){
                            msg =traffic;
                        }else {msg = msg+" "+traffic;}
                        sendNotification(msg);
                    }
                    return;
                }
            }
            if(remoteMessage.getData().containsKey("firebase")){
                if(remoteMessage.getData().get("firebase").equalsIgnoreCase("true")){
                    Logger.logE("FirebaseMessage", "traffic profile updated");
                    LocalyticsTrackers.Companion.getInstance().updateTrafficProfile();
                    return;
                }
            }
        }

//        if (Localytics.handleFirebaseMessage(data)) {
//            // Localytics handled the message; no need to continue.
//            return;
//        }
//                  else {
                // The notification is not from Localytics or the Firebase Dashboard, so the app must handle it.
                sendNotification(data.get("message"));
//            }

        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
//        Logger.logE("Tag", "msg received");
//        AppLog.printLog(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
//        if (remoteMessage.getData().size() > 0) {
//            AppLog.printLog(TAG, "Message data payload: " + remoteMessage.getData());
//        }

        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            AppLog.printLog(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//            sendNotification(remoteMessage.getNotification().getBody().toString());


//        }else{
//            Toast.makeText(this,"error",Toast.LENGTH_SHORT).show();
//        }


    }


    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, SplashScreen.class);
        intent.putExtra("fromNotification", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String channelName = "Invincibill VPN";
        String channelId = "com.invincibull.messaging";

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(mChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,channelId)

                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);
        notificationBuilder.build().flags = Notification.FLAG_AUTO_CANCEL;
        notificationBuilder.setContentIntent(pendingIntent);

        notificationBuilder.setSmallIcon(R.drawable.notification_icon);
//        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//        } else {
//            notificationBuilder.setSmallIcon(R.drawable.icon);
//        }


        int notificationId = (int) Calendar.getInstance().getTimeInMillis();
        notificationManager.notify(notificationId, notificationBuilder.build());
    }


//    private void showNotification(String message) {
//        if (!TextUtils.isEmpty(message)) {
//            Intent intent = new Intent(this, MainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                    PendingIntent.FLAG_ONE_SHOT);
//
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "SOME_CHANNEL_ID")
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentTitle("FCM Message")
//                    .setContentText(message)
//                    .setAutoCancel(true)
//                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
//                    .setContentIntent(pendingIntent);
//
//            NotificationManager notificationManager =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//            notificationManager.notify(0, notificationBuilder.build());
//        }
//    }

}
