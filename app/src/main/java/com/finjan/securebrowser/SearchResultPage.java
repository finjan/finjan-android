package com.finjan.securebrowser;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.finjan.securebrowser.fragment.ParentFragment;
import com.finjan.securebrowser.fragment.SearchedResultFragment;
import com.finjan.securebrowser.model.GoogleResults;
import com.finjan.securebrowser.model.SearchResult;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class SearchResultPage extends LinearLayout {

    private SearchResultCell[] cells = new SearchResultCell[4];
    private Button prevButton = null;
    private Button nextButton = null;
    private View divider = null;
    private RelativeLayout buttonContainer = null;
    private int currentDisplayIndex = 0;
    private int resultsPerPage = 4;
    private DisplayWebView delegate = null;

    public SearchResultPage(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    private SearchedResultFragment searchedResultFragment;
    public void setParentFragment(SearchedResultFragment searchedResultFragment){
        this.searchedResultFragment=searchedResultFragment;
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        cells[0] = (SearchResultCell) findViewById(R.id.row1);
        cells[1] = (SearchResultCell) findViewById(R.id.row2);
        cells[2] = (SearchResultCell) findViewById(R.id.row3);
        cells[3] = (SearchResultCell) findViewById(R.id.row4);

        for (int i = 0; i < 4; i++) {
            SearchResultCell cell = cells[i];
            cell.cellIndex = i;
            cell.searchResultPage = this;
        }

        divider = findViewById(R.id.divider);
        buttonContainer = (RelativeLayout) findViewById(R.id.buttonContainer);
        setButtonContainerHeight();

        // Set up next/prev button handlers so we know when those buttons are pressed
        prevButton = (Button) findViewById(R.id.prevButton);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decrementPage();
                // Notify the UrlScannerActivity which page we're on
                delegate.searchResultDisplayed(currentDisplayIndex);

            }
        });

        nextButton = (Button) findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incrementPage();
                // Notify the UrlScannerActivity which page we're on
                delegate.searchResultDisplayed(currentDisplayIndex);
            }
        });

        // Fonts
        Typeface robotoRegular = Typeface.createFromAsset(getContext().getAssets(), "fonts/sf-ui-regular.ttf");
        prevButton.setTypeface(robotoRegular);
        nextButton.setTypeface(robotoRegular);

        resetView();
    }

    public void resetView() {
        currentDisplayIndex = 0;
        for (int i = 0; i < cells.length; i++) {
            cells[i].updateContents(null);
        }
        updateNextPrevButtons();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void setButtonContainerHeight() {
        // Get the number of pixels
        WindowManager windowManager = (WindowManager) this.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        DisplayMetrics resourceDisplayMetrics = getResources().getDisplayMetrics();

        float density  = resourceDisplayMetrics.density;
        float dpHeight = outMetrics.heightPixels / density;

        float statusBarHeight = getStatusBarHeight() / density;

        // All heights are in device independent pixels (dip),
        // which we're then going to translate to actual pixels
        // screen height
        // - search cell height (4 x 121)
        // - search cell margin (2 x 6) for top margin
        // - status bar height
        // - search bar height (50)
        float availableHeight = (dpHeight - ((4 * 121) + (2 * 6)) - statusBarHeight - 50);
        int dipHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, availableHeight, resourceDisplayMetrics);

        LayoutParams layoutParams = new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dipHeight);
        buttonContainer.setLayoutParams(layoutParams);
    }

    public void ratingTouched(int index) {
        // Notify the delegate to load the rating card for the url in this cell
        delegate.showReportAtSearchIndex(currentDisplayIndex + index);
    }

    public void contentTouched(int index) {
        delegate.loadSearchResult(currentDisplayIndex + index);
    }

    private void incrementPage() {
        currentDisplayIndex += resultsPerPage;
        updateSearchResults();
        updateNextPrevButtons();
    }

    private void decrementPage() {
        // Make sure we don't go below 0.
        currentDisplayIndex  = Math.max(0, currentDisplayIndex - resultsPerPage);
        updateSearchResults();
        updateNextPrevButtons();
    }

    private void updateNextPrevButtons() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) nextButton.getLayoutParams();
        if (currentDisplayIndex == 0) {
            prevButton.setVisibility(GONE);
            divider.setVisibility(GONE);
            layoutParams.removeRule(RelativeLayout.RIGHT_OF);
            layoutParams.removeRule(RelativeLayout.END_OF);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        } else {
            prevButton.setVisibility(VISIBLE);
            divider.setVisibility(VISIBLE);
            layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.divider);
            layoutParams.addRule(RelativeLayout.END_OF, R.id.divider);
            layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
            layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_START);
        }

    }

    public void setDelegate(DisplayWebView delegate) {
        this.delegate = delegate;
    }


    public void updateSearchResults() {
        if(delegate==null){
            return;
        }
        for (int i = 0; i < resultsPerPage; i++) {
            // Populate cell
            SearchResult result = delegate.resultAtIndex(currentDisplayIndex + i);
            cells[i].updateContents(result);
            delegate.checkSafety(currentDisplayIndex + i);
        }
    }

    public void updateSafetyRating(int index) {
        // If we're still displaying this result, update the cell
        if (index >= currentDisplayIndex && index < currentDisplayIndex + resultsPerPage) {
            //TODO search REsult page
            cells[index % resultsPerPage].updateSafetyRating(delegate.resultAtIndex(index).safetyResult);
        }
    }
    public void updateSafetyRatingAppPurchaseFalse(int index){
        if (index >= currentDisplayIndex && index < currentDisplayIndex + resultsPerPage) {
            //TODO search REsult page
            cells[index % resultsPerPage].updateSafetyRatingAppPurchaseFalse();
        }
    }
}
