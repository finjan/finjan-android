package com.finjan.securebrowser;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finjan.securebrowser.helpers.DbHelper;
import com.finjan.securebrowser.helpers.NativeHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import webHistoryDatabase.bookmarks;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class BookmarksAdapter extends RecyclerView.Adapter<BookmarksAdapter.MyViewHolder> {

    final private List<bookmarks> webhistoryList = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_url;
        private ImageView iv_delete,iv_status;
        public LinearLayout ll_views ;

        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_url = (TextView) view.findViewById(R.id.tv_url);
            ll_views = (LinearLayout)view.findViewById(R.id.ll_views);
            iv_delete = (ImageView) view
                    .findViewById(R.id.iv_delete);
            iv_status= (ImageView) view.findViewById(R.id.iv_status);
            (view.findViewById(R.id.divider_history_view)).setVisibility(View.VISIBLE);
        }
    }

    private ListItemListener listItemListener;
    private Context context;
    public BookmarksAdapter(final List<bookmarks> webhistoryList, final ListItemListener listItemListener, Context context) {
        if(webhistoryList!=null && webhistoryList.size()>0){
            this.webhistoryList.clear();
            this.webhistoryList.addAll(webhistoryList);
        }else {
            this.webhistoryList.clear();
        }
        this.context=context;
        this.listItemListener=listItemListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        bookmarks webhistory = webhistoryList.get(position);
        Long id = webhistory.getId();
        holder.tv_title.setVisibility(View.VISIBLE);
        holder.tv_title.setText(webhistory.getPagetitle().isEmpty()?"New Tab":webhistory.getPagetitle());
        holder.tv_url.setText(webhistory.getUrl().isEmpty()?"":webhistory.getUrl());
        holder.iv_delete.setTag(R.string.view_tag_idUrl,id);
        holder.iv_delete.setTag(R.string.view_tag_position,position);
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listItemListener.onBookMarkUrlDel(v,(Long) (v.getTag(R.string.view_tag_idUrl)),
                        holder.getLayoutPosition(),webhistoryList);
                List<bookmarks> list=DbHelper.getInstnace().getBookMarsList();
                if(list!=null && list.size()>0){
                    Collections.reverse(list);
                }
                notifyBookmarkTab(list);

//                onBookMarkUrlDel(v,String.valueOf(v.getTag(R.string.idUrl)),String.valueOf(v.getTag(R.string.position)));
            }
        });
        holder.ll_views.setTag(R.string.view_tag_position,position);
        holder.ll_views.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listItemListener.onUrlClicks(v,holder.getLayoutPosition(),webhistoryList);

//                onUrlClicks(v,String.valueOf(v.getTag(R.string.position)));
            }
        });
        NativeHelper.getInstnace().setStatusIcon(context,webhistory.getRating(),holder.iv_status);
    }

    @Override
    public int getItemCount() {
        return webhistoryList.size();
    }
//    public abstract void onBookMarkUrlDel(View view,String idUrl,String position);
//    public abstract void onUrlClicks(View view,String position);

    public interface ListItemListener{
        void onBookMarkUrlDel(View view, Long idUrl, int position,List<bookmarks> bookmarksList);

        void onUrlClicks(View view,int position,List<bookmarks> bookmarksList);
    }
    public void notifyBookmarkTab(List<bookmarks> webhistoryList){
        if(webhistoryList!=null && webhistoryList.size()>0){
            this.webhistoryList.clear();
            this.webhistoryList.addAll(webhistoryList);
        }else {
            this.webhistoryList.clear();
        }
        notifyDataSetChanged();
    }

}