package com.finjan.securebrowser.constants;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 */

public interface DrawerConstants {

    String DKEY_HOME="DKEY_HOME";
    String DKEY_APP_PERCHASE_DIALOG="DKEY_APP_PERCHASE_DIALOG";

    //default webview screens
    String DKEY_Normal_Webview="DKEY_Normal_Webview";
    String DKEY_HELP="DKEY_HELP";
    String DKEY_ABOUT="DKEY_ABOUT";
    String DKEY_EULA="DKEY_EULA";
    String DKEY_PASSWORD_RESET="DKEY_PASSWORD_RESET";
    String DKEY_PRIVATE_PRIVACY="DKEY_PRIVATE_PRIVACY";

    String DKEY_Worning="DKEY_Worning";
    String DKEY_Tracker_Fragemnt="DKEY_Tracker_Fragemnt";
    String DKEY_Search_Fragment="DKEY_Search_Fragment";
    String DKEY_Report_Fragment="DKEY_Report_Fragment";
    String DKEY_Scanning_Fragment="DKEY_Scanning_Fragment";
    String DKEY_History="DKEY_History";
    String DKEY_TABS="DKEY_TABS";
    String DKEY_Settings="DKEY_Settings";
    String DKEY_Bookmarks="DKEY_Bookmarks";
    String DKEY_VPN="DKEY_VPN";
}
