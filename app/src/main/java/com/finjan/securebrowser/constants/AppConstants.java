package com.finjan.securebrowser.constants;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 */

public interface AppConstants {


    String EMPTY_URL = "about:blank";
    String NEW_TAB= "New Tab";
    String NEW_PRIVATE_TAB= "New Private Tab";
    String PRIVATE = "PRIVATE";
    String PACKAGE = "package";
    String SCHEMA = "scheme";
    String SCRIPT_TAG = "<script";
    String SCRIPT_END_TAG = "</script";
    String SCRIPT_TYPE_TAG = "text/javascript";


    String CONNECTED_ACTION = "CONNECTED_ACTION";


    //BKEY= BundleKEY Bundle constants
    String BKEY_TITLE_NAME = "BKEY_TITLE_NAME";
    String BKEY_DKEY_TAG = "BKEY_DKEY_TAG";
    String BKEY_ISHOME = "BKEY_ISHOME";
    String BKEY_BUNDLE = "BKEY_BUNDLE";
    String BKEY_URL = "BKEY_URL";
    String BKEY_REDIRECTED_FUNCTION = "BKEY_REDIRECTED_FUNCTION";
    String BKEY_NEW_TAB = "BKEY_NEW_TAB";
    String BKEY_NEW_PRIVATE_TAB = "BKEY_NEW_PRIVATE_TAB";
    String BKEY_REDIRECT_URL = "BKEY_NEW_REDIRECT_URL";
    String BKEY_SAFE_BAR_AWAY = "BKEY_SAFE_BAR_AWAY";
    String BKEY_REDIRECT_TAB_POSITION = "BKEY_REDIRECT_TAB_POSITION";
    String BKEY_ID = "BKEY_ID";
    String BKEY_IS_PRIVATE="BKEY_IS_PRIVATE";
    String BKEY_SCREEN="BKEY_SCREEN";

    //Subscription
    String BKEY_PURCHASE="BKEY_PURCHASE";
    String BKEY_USERNAME="BKEY_USERNAME";
    String BKEY_ORIGNAL_PURCHASE_JSON ="BKEY_ORIGNAL_PURCHASE_JSON";
    String BKEY_PURCHASE_Sig ="BKEY_PURCHASE_Sig";
    String BKEY_PURCHASE_ITEM_TYPE ="BKEY_PURCHASE_ITEM_TYPE";

    //Job Schedular
    String BKEY_ITEMTYPE="BKEY_ITEMTYPE";
    String BKEY_ORDERID="BKEY_ORDERID";
    String BKEY_PACKAGENAME="BKEY_PACKAGENAME";
    String BKEY_SKU="BKEY_SKU";
    String BKEY_PURCHASETIME="BKEY_PURCHASETIME";
    String BKEY_purchaseState="BKEY_purchaseState";
    String BKEY_developerPayload="BKEY_developerPayload";
    String BKEY_token="BKEY_token";
    String BKEY_originalJson="BKEY_originalJson";
    String BKEY_signature="BKEY_signature";
    String BKEY_expiryTimeMillis="BKEY_expiryTimeMillis";
    String BKEY_autoRenewing="BKEY_autoRenewing";



    //IKEY=Intent KEY
    String IKEY_RESPONSE_ARRAY = "IKEY_RESPONSE_ARRAY";
    String IKEY_AppPurchase_Monthly = "IKEY_AppPurchase_Monthly";
    String IKEY_AppPurchase_Yearly = "IKEY_AppPurchase_Yearly";
    String IKEY_INDEX = "IKEY_INDEX";
    String IKEY_DRAWER_STATUS="IKEY_DRAWER_STATUS";
    String IKEY_PASSWORD_CONFIRMED="IKEY_PASSWORD_CONFIRMED";
    String IKEY_ENABLE_TOUCH_ID="IKEY_ENABLE_TOUCH_ID";
    String IKEY_FINGER_REGISTERED ="IKEY_FINGER_REGISTERED";

    //LBCAST=LocalBroadcasts
    String LBCAST_CLEARHISTORY = "LBCAST_CLEARHISTORY";
    String LBCAST_REFRESH_SCAN = "LBCAST_REFRESH_SCAN";
    String LBCAST_WARNING_BACK_PRESS = "LBCAST_WARNING_BACK_PRESS";
    String LBCAST_WARNING_Continue_PRESS = "LBCAST_WARNING_Continue_PRESS";
    String LBCAST_AppPurchase_connection = "LBCAST_AppPurchase_connection";
    String LBCAST_AppPurchase_Success = "LBCAST_AppPurchase_Success";
    String LBCAST_SearchPage_Safety_Result = "LBCAST_SearchPage_Safety_Result";
    String LBCAST_updateSafetyRatingAppPurchaseFalse = "LBCAST_updateSafetyRatingAppPurchaseFalse";
    String LBCAST_CALL_BOTTOMBARS ="LBCAST_CALL_BOTTOMBARS";
    String LBCAST_BAR_NO_THANKS_CLICK="LBCAST_BAR_NO_THANKS_CLICK";
    String LBCAST_Toggle_Report="LBCAST_Toggle_Report";
    String LBCAST_Tracker_Fab_Click="LBCAST_Tracker_Fab_Click";




    //PER_KEY= permission key
    int READ_PHONE_STATE_PERMISSION = 3;
    int STORAGE_PERMISSION = 2;
    int LOCATION_PERMISSION = 5;


    //Redirecting function
    int RFUC_NEW_TAB = 200;
    int RFUC_NEW_PRIVATE_TAB = 201;
    int RFUC_URL = 202;
    int RFUC_URL_HISTORY = 205;
    int RFUC_URL_BOOKMARK = 206;
    int RFUC_URL_NON_SAFTY_RESULT = 207;
    int RFUC_SAFE_BAR_AWAY = 203;
    int RFUC_TOGGLE_SAFE_BAR = 204;

    //Activity_For_Result
    int AFR_Password_set=121;
    int AFR_callingConfirmPassword=120;
    int AFR_REGISTER_FINGER=123;
    int PICKFILE_REQUEST_CODE=799;
    int PICKFILE_REQUEST_CODE_Pre=798;

    int UPDATE_APP_REQUEST_CODE=888;

//    boolean blockTracking=false;

    String RATING_DANGEROUS="dangerous";
    String RATING_SUSPICIOUS="suspicious";
    String RATING_SAFE="safe";
    String RATING_INVALID="invalid";
    String RATING_GREEN="green";
    String RATING_BLUE="blue";
    /**
     * weather we should user earlier puchase check or not
     */
    boolean paidScanVersion =false;

    String SERVICE_Main_Nofification="SERVICE_Main_Nofification";
    String SERVICE_App_OPEN="SERVICE_App_OPEN";
    String SERVICE_VPN_Open ="SERVICE_VPN_Open";
    String SERVICE_VPN_Connect="SERVICE_VPN_Connect";
    String SERVICE_connect_to_sercure_network="SERVICE_connect_to_sercure_network";
    int SERVICE_notification_id=101;



    //cache key of Api
    int interValHit = 3600000;
    int interValHitMINT = 60000;
    String ALLPROMOLIST = "allpromolist";
    String REGIONS = "regions";
    String PLIST = "plist";
    String ALLCANCLEDPROMOSUBSCRIPTIONLIST = "allpromocancledsubscriptionlist";
    String USER = "user";
    String LICENCE = "licence";
    String ALLSUBSCRIPTION = "allsubscription";
    String TRAFFIC = "traffic";

}
