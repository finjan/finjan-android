package com.finjan.securebrowser;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finjan.securebrowser.helpers.NativeHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import webHistoryDatabase.webhistory;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private ArrayList<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<webhistory>> _listDataChild;
    private ArrayList<webhistory> arrayList = new ArrayList<>();

    private ListItemListener listItemListener;

    public ExpandableListAdapter(Context context, ArrayList<String> listDataHeader,
                                 HashMap<String, ArrayList<webhistory>> listChildData, final ListItemListener listItemListener) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.listItemListener=listItemListener;
    }
    public ArrayList<String> getDaysList(){
        return _listDataHeader;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.history_adapter, null);
        }


        final webhistory webhistory = (webhistory) getChild(groupPosition, childPosition);

        Date dTime = webhistory.getDtime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String finalDate = formatter.format(dTime);


        TextView tv_title = (TextView) convertView
                .findViewById(R.id.tv_title);
        TextView tv_url = (TextView) convertView
                .findViewById(R.id.tv_url);
        ImageView iv_delete = (ImageView) convertView.findViewById(R.id.iv_delete);
        ImageView iv_status= (ImageView) convertView.findViewById(R.id.iv_status);

        LinearLayout ll_views = (LinearLayout) convertView.findViewById(R.id.ll_views);


        if (webhistory.getPagetitle() == null || webhistory.getPagetitle().equals("null") || webhistory.getPagetitle().trim().length() == 0 || webhistory.getPagetitle().trim().equals("about:blank")) {
            tv_title.setVisibility(View.GONE);
        } else {
            tv_title.setVisibility(View.VISIBLE);
        }
        NativeHelper.getInstnace().setStatusIcon(_context,webhistory.getRating(),iv_status);
        tv_title.setText(webhistory.getPagetitle());
        tv_url.setText(webhistory.getUrl());
        iv_delete.setTag(R.string.date, finalDate);
        iv_delete.setTag(R.string.view_tag_childPosition, childPosition);
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listItemListener.onChildDelete(v, String.valueOf(v.getTag(R.string.date)), childPosition);
//                onBookMarkUrlDel(v, String.valueOf(v.getTag(R.string.date)), String.valueOf(v.getTag(R.string.childPosition)));
            }
        });
        ll_views.setTag(R.string.date, finalDate);
        ll_views.setTag(R.string.view_tag_childPosition, childPosition);
        ll_views.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listItemListener.onUrlClicks(v, String.valueOf(v.getTag(R.string.date)), childPosition);
//                onUrlClicks(v, String.valueOf(v.getTag(R.string.date)), String.valueOf(v.getTag(R.string.childPosition)));
            }
        });


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        if(this._listDataHeader.size()==1 && _listDataChild.size()==0){
            return 0;
        }
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        String headerTitle = (String) getGroup(groupPosition);
        String dayOfWeek = "";

        try {
            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date MyDate = newDateFormat.parse(headerTitle);
            newDateFormat.applyPattern("EEEE - MMM d, yyyy");
            dayOfWeek = newDateFormat.format(MyDate);

        } catch (Exception e) {
            e.printStackTrace();
        }

        String[] split = headerTitle.split("-");

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);


        if (Integer.parseInt(split[0]) == year && Integer.parseInt(split[1]) == (month + 1) && Integer.parseInt(split[2]) == day) {

            try {
                SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date MyDate = newDateFormat.parse(headerTitle);
                newDateFormat.applyPattern("MMM d, yyyy");
                dayOfWeek = "Today - " + newDateFormat.format(MyDate);

            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (Integer.parseInt(split[0]) == year && Integer.parseInt(split[1]) == (month + 1) && Integer.parseInt(split[2]) == (day - 1)) {

            try {
                SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date MyDate = newDateFormat.parse(headerTitle);
                newDateFormat.applyPattern("MMM d, yyyy");
                dayOfWeek = "Yesterday - " + newDateFormat.format(MyDate);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setText(dayOfWeek);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

//    public abstract void onBookMarkUrlDel(View view, String date, String childPosition);
//
//    public abstract void onUrlClicks(View view, String date, String childPosition);

    public interface ListItemListener{
         void onChildDelete(View view, String date, int childPosition);

        void onUrlClicks(View view, String date, int childPosition);
    }
    public void notifyAdapter(ArrayList<String> listDataHeader, HashMap<String, ArrayList<webhistory>> listChildData){
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        notifyDataSetChanged();
    }
}

