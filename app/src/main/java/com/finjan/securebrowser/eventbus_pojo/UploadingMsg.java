package com.finjan.securebrowser.eventbus_pojo;

import android.net.Uri;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 */

public class UploadingMsg {
    private Uri[] resultsArray;
    private Uri resultsArrayPre;
    public Uri[] getUploadingUri(){return resultsArray;}
    public Uri getUploadingUriPre(){return resultsArrayPre;}
    public UploadingMsg(Uri[] resultsArray){
        if(resultsArray!=null){
            this.resultsArray=resultsArray;
        }
    }
    public UploadingMsg(Uri resultsArray){
        if(resultsArray!=null){
            this.resultsArrayPre=resultsArray;
        }
    }
}
