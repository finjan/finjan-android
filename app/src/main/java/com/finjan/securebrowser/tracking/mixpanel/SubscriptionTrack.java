package com.finjan.securebrowser.tracking.mixpanel;

import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.utils.PurchaseHelper;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 17/08/17.
 *
 * Used for localytics tracking of events and screens
 *
 */


public class SubscriptionTrack extends EventProperties {

    public SubscriptionTrack(Purchase purchase){
        super();
        if(purchase!=null){
            putProperty("amount", PurchaseHelper.getPurchaseAmount(purchase));
            putProperty("name", purchase.getSku());
            putProperty("device_type", "android");
            putProperty("plan_interval", PurchaseHelper.getExpireInterval(purchase));

        }
    }
    public SubscriptionTrack(){
        if(AuthenticationHelper.getInstnace().getFinjanUser()!=null){
            putProperty("finjan_account", AuthenticationHelper.getInstnace().getFinjanUser().getEmail());
            putProperty("finjan_user_name", AuthenticationHelper.getInstnace().getFinjanUser().getUserName());
        }
    }
}
