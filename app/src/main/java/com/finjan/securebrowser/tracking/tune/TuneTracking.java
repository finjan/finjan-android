package com.finjan.securebrowser.tracking.tune;

import android.content.Context;
import android.text.TextUtils;

import com.avira.common.id.HardwareId;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.tracking.CustomTrackers;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.tune.ITune;
import com.tune.Tune;
import com.tune.TuneEvent;

/**
 * Created by anurag on 18/08/17.
 * <p>
 * <p>
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class TuneTracking {
    public static TuneTracking getInstance() {
        return TuneTrackingHolder.Instance;
    }

    public void trackPurchase(Purchase purchase) {
        if (TuneTrackingConfig.enabled && purchase != null) {
            ITune tune=Tune.getInstance();
            if(tune!=null){
                tune.setUserEmail(AuthenticationHelper.getInstnace().getUserEmail(FinjanVPNApplication.getInstance()));
                tune.setUserName(AuthenticationHelper.getInstnace().getUserName(FinjanVPNApplication.getInstance()));
//                tune.setDeviceId(HardwareId.get(FinjanVPNApplication.getInstance()));
                tune.measureEvent(new TunePurchaseTrack("", purchase));
                tune.measureEvent(new TuneEvent(CustomTrackers.VPN_SUBSCRIBED));
            }
        }
    }

    public void trackSubsRenew(String sku) {
        if (TuneTrackingConfig.enabled && !TextUtils.isEmpty(sku)) {
            ITune tune=Tune.getInstance();
            if(tune!=null){
                tune.setUserEmail(AuthenticationHelper.getInstnace().getUserEmail(FinjanVPNApplication.getInstance()));
                tune.setUserName(AuthenticationHelper.getInstnace().getUserName(FinjanVPNApplication.getInstance()));
                tune.measureEvent(new TunePurchaseTrack(TunePurchaseTrack.eventRenew, sku));
            }
        }
    }

    public void trackRegister(boolean login) {
        if(TuneTrackingConfig.enabled){
            ITune tune=Tune.getInstance();
            if(tune!=null) {
                tune.setUserEmail(AuthenticationHelper.getInstnace().getUserEmail(FinjanVPNApplication.getInstance()));
                tune.setUserName(AuthenticationHelper.getInstnace().getUserName(FinjanVPNApplication.getInstance()));
                tune.measureEvent(new TuneRegisterTrack("", login));
            }
        }
    }
    public void trackAppInstalled() {
        if(TuneTrackingConfig.enabled){
            ITune tune=Tune.getInstance();
            if(tune!=null){
                tune.setUserEmail(AuthenticationHelper.getInstnace().getUserEmail(FinjanVPNApplication.getInstance()));
                tune.setUserName(AuthenticationHelper.getInstnace().getUserName(FinjanVPNApplication.getInstance()));
                tune.measureEvent(new TuneEvent("install"));
            }
        }
    }


    private static final class TuneTrackingHolder {
        private static final TuneTracking Instance = new TuneTracking();
    }


}
