package com.finjan.securebrowser.tracking.tune;

import android.text.TextUtils;

import com.avira.common.id.HardwareId;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.tracking.CustomTrackers;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.utils.PurchaseHelper;
import com.tune.TuneEvent;
import com.tune.TuneEventItem;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by anurag on 18/08/17.
 * <p>
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class TunePurchaseTrack extends TuneEvent {

    public static final String eventPurchase=TuneEvent.PURCHASE;
    public static final String eventRenew= CustomTrackers.VPN_SUBSCRIPTION_RENEWED;


    public TunePurchaseTrack(String eventName, Purchase purchase) {
        super((TextUtils.isEmpty(eventName) || eventName.equals(eventPurchase))?eventPurchase:eventRenew);
        basicInfo(null, purchase);
    }

    public TunePurchaseTrack(String eventName, String sku) {
        super((TextUtils.isEmpty(eventName) || eventName.equals(eventPurchase))?eventPurchase:eventRenew);
        basicInfo(sku, null);
    }

    private void basicInfo(String sku, Purchase purchase) {

        withRevenue((PurchaseHelper.getPurchaseAmount(TextUtils.isEmpty(sku) ? purchase.getSku() : sku))/100.0);
        withCurrencyCode("USD");
        withDate1(new Date(System.currentTimeMillis()));

        ArrayList<TuneEventItem> list = new ArrayList<>();
        list.add(new TuneEventItem("Purchase Type").withAttribute1(purchase==null?"Renew":"Purchase"));
        list.add(new TuneEventItem("Device Id").withAttribute1(HardwareId.get(FinjanVPNApplication.getInstance())));
        list.add(new TuneEventItem("Product").withAttribute1(TextUtils.isEmpty(sku) ? purchase.getSku() : sku));
        list.add(new TuneEventItem("Finjan User").withAttribute1(AuthenticationHelper.getInstnace().getUserEmail(FinjanVPNApplication.getInstance())));
        if (AuthenticationHelper.getInstnace().getFinjanUser() != null) {
            list.add(new TuneEventItem("Finjan Username").withAttribute1(AuthenticationHelper.getInstnace().getFinjanUser().getUserName()));
        }
        list.add(new TuneEventItem("Subs amount(in cents)").withAttribute1(String.valueOf((purchase == null) ? PurchaseHelper.getPurchaseAmount(sku) : PurchaseHelper.getPurchaseAmount(purchase))));
        if (purchase != null) {

            String purchaseEnd=purchase.getmExpiryTimeMillis()>0?DateHelper.getInstnace().getServerFormatDate(purchase.getmExpiryTimeMillis())
                    :PurchaseHelper.getExpireTime(purchase);

            list.add(new TuneEventItem("Subs starts").withAttribute1(DateHelper.getInstnace().getServerFormatDate(purchase.getPurchaseTime())));
            list.add(new TuneEventItem("Subs ends").withAttribute1(purchaseEnd));
            list.add(new TuneEventItem("Subs interval").withAttribute1(PurchaseHelper.getExpireInterval(purchase)));
        }
        withEventItems(list);
    }
}
