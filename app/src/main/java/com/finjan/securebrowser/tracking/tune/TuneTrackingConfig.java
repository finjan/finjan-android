package com.finjan.securebrowser.tracking.tune;

import android.content.Context;

import com.finjan.securebrowser.tracking.facebook.FbTrackingConfig;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.tune.Tune;
import com.tune.application.TuneActivityLifecycleCallbacks;

/**
 * Created by anurag on 18/08/17.
 * <p>
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class TuneTrackingConfig {

//    private static final String ADVERTISERID = "197466";
//    private static final String CONVERSIONKEY = "8c93f8f380640079206369cb6d2827f1";

    public static TuneTrackingConfig getInstance() {
        return TuneTrackingConfigHolder.instance;
    }

    public void initTuneTracking(FinjanVPNApplication application) {
//        Tune.setPromoList(application.getApplicationContext(), ADVERTISERID, CONVERSIONKEY,true);
        Tune.init(application.getApplicationContext(), advertiser_id, conversion_key,null);
        application.registerActivityLifecycleCallbacks(new TuneActivityLifecycleCallbacks());
    }

    private static final class TuneTrackingConfigHolder {
        private static final TuneTrackingConfig instance = new TuneTrackingConfig();
    }

    protected static boolean enabled=true;
    private static String advertiser_id,conversion_key;
    public static void plistConfig(boolean enabled,String advertiser_id,String conversion_key){
        TuneTrackingConfig.enabled=enabled;
        TuneTrackingConfig.advertiser_id=advertiser_id;
        TuneTrackingConfig.conversion_key=conversion_key;
    }
}
