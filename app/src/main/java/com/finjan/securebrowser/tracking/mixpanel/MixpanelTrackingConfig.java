/*
 *  Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */
package com.finjan.securebrowser.tracking.mixpanel;

import android.text.TextUtils;
import android.util.Log;

import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * @author daniela.stamati
 */
public class MixpanelTrackingConfig implements TrackingManager.ITrackingConfig {

    private static String TAG = MixpanelTrackingConfig.class.getSimpleName();


    public static final String TRACKING_BLACK_LIST = "TRACKING_BLACK_LIST";
    private static JSONArray mBlackListedEvents = new JSONArray();
    private static MixpanelTrackingConfig INSTANCE = null;

    private MixpanelTrackingConfig() {
    }

    public static MixpanelTrackingConfig getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MixpanelTrackingConfig();
        }
        return INSTANCE;
    }

    @Override
    public boolean isTracked(TrackingEvent event) {

        return !isBlackListed(event.getEvent());

    }

    /**
     * check term threshold and filter event by black list
     *
     * @return true if event is in the Black List
     */
    private boolean isBlackListed(String event) {
        Logger.logD(TAG, "isBlackListed " + event);
        // restore data if needed
        if (mBlackListedEvents.length() == 0) {
            mBlackListedEvents = restoreEvents();
        }
        boolean contains = contains(event);
        Logger.logD(TAG, " mBlackListedEvents.contains(" + event + ") " + contains);
        if (com.avira.common.BuildConfig.DEBUG) {
            try {
                Logger.logD(TAG, "Here is a list of all black listed events ");
                for (int idx = 0; idx < mBlackListedEvents.length(); idx++) {
                    Logger.logD(TAG, " mBlackListedEvents: " + mBlackListedEvents.getString(idx));
                }
            } catch (JSONException e) {
                Logger.logE(TAG, "json parsing error"+e);
            }
        }
        return contains;
    }

    private boolean contains(String event) {
        try {
            for (int idx = 0; idx < mBlackListedEvents.length(); idx++) {
                if (event.equals(mBlackListedEvents.getString(idx))) {
                    return true;
                }
            }
        } catch (JSONException e) {
            Logger.logE(TAG, "json parsing error"+e);
        }
        return false;
    }

    /**
     * read blacklisted values from a shared preferences and setPromoList a hash set
     *
     * @return JSONArray
     */
    private JSONArray restoreEvents() {
        String eventsString = com.avira.common.utils.SharedPreferencesUtilities.getString(FinjanVPNApplication.getInstance(), TRACKING_BLACK_LIST);
        Logger.logD(TAG, "restoreEvents: " + eventsString);

        if (TextUtils.isEmpty(eventsString)) {
            return new JSONArray();
        }
        JSONArray tempBlackListEventsJson;

        try {
            tempBlackListEventsJson = new JSONArray(eventsString);
        } catch (JSONException e) {
            Logger.logE(TAG, "json parsing error"+e);
            tempBlackListEventsJson = new JSONArray();
        }
        return tempBlackListEventsJson;
    }
    public static boolean enabled=true;
    protected static String token;
    public static void plistConfig(boolean enabled, String token){
        MixpanelTrackingConfig.enabled=enabled;
        MixpanelTrackingConfig.token =token;
    }


    // clearBlackList old black listed events
    public static void clearBlackList() {
        mBlackListedEvents = new JSONArray();
    }
}