package com.finjan.securebrowser.tracking.mixpanel;

import android.util.Log;

import com.avira.common.id.HardwareId;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 17/08/17.
 *
 * Used for localytics tracking of events and screens
 *
 */


public class BasicEventProperties extends JSONObject {

    private static final String TAG = BasicEventProperties.class.getSimpleName();
    private String deviceId="deviceID";

    public BasicEventProperties(){
//        try {
//            put(deviceId,HardwareId.get(FinjanVPNApplication.getInstance()));
//        } catch (JSONException e) {
//            Logger.logE(TAG, "Json Exception", e);
//        }
    }

}
