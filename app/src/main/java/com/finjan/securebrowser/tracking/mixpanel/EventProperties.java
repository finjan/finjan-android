package com.finjan.securebrowser.tracking.mixpanel;

import android.util.Log;

import com.finjan.securebrowser.helpers.logger.Logger;

import org.json.JSONException;

/**
 *
 *
 * A json object to be used to keep track of tracking properties
 */
public class EventProperties extends BasicEventProperties{
    private static final String TAG = EventProperties.class.getSimpleName();
    public void putProperty(String name, int value){
        try {
            put(name,value);
        } catch (JSONException e) {
            Logger.logE(TAG, "Json Exception"+ e);
        }
    }
    public void putProperty(String name, long value){
        try {
            put(name,value);
        } catch (JSONException e) {
            Logger.logE(TAG, "Json Exception"+ e);
        }
    }
    public void putProperty(String name, Object value){
        try {
            put(name,value);
        } catch (JSONException e) {
            Logger.logE(TAG, "Json Exception"+ e);
        }
    }
    public void putProperty(String name, boolean value){
        try {
            put(name,value);
        } catch (JSONException e) {
            Logger.logE(TAG, "Json Exception"+ e);
        }
    }

    public void putProperty(String name, double value){
        try {
            put(name,value);
        } catch (JSONException e) {
            Logger.logE(TAG, "Json Exception"+e);
        }
    }
}
