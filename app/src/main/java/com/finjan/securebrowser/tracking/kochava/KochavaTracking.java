package com.finjan.securebrowser.tracking.kochava;

import android.content.Context;
import android.text.TextUtils;

import com.avira.common.id.HardwareId;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.tracking.CustomTrackers;
import com.finjan.securebrowser.tracking.tune.TunePurchaseTrack;
import com.finjan.securebrowser.tracking.tune.TuneRegisterTrack;
import com.finjan.securebrowser.tracking.tune.TuneTracking;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.utils.PurchaseHelper;
import com.google.api.services.androidpublisher.model.Track;
import com.kochava.base.Tracker;
import com.tune.Tune;
import com.tune.TuneEvent;

import java.util.Date;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 1/9/17.
 *
 * Used for facebook tracking of events and screens
 */
public class KochavaTracking {

    private static Tracker.Event updateTracker(Tracker.Event event){
        return event.setDate(new Date())
                .setQuantity(1)
                .setName(AuthenticationHelper.getInstnace().getUserName(FinjanVPNApplication.getInstance()))
                .setCurrency("USD")
                .setUserName(AuthenticationHelper.getInstnace().getUserEmail(FinjanVPNApplication.getInstance()))
                .setUserId(AuthenticationHelper.getInstnace().getUserName(FinjanVPNApplication.getInstance()));
    }

        private void trackPurchase(Purchase purchase,String sku) {
        if(!KochavaConfig.enabled){return;}
            Tracker.Event event=null;
                if(purchase!=null){
                    event=updateTracker(new Tracker.Event(Tracker.EVENT_TYPE_PURCHASE));
                }else {
                    event=updateTracker(new Tracker.Event(CustomTrackers.VPN_SUBSCRIPTION_RENEWED));
                }

                if(purchase!=null){
                    event=event.setGooglePlayReceipt(purchase.getOriginalJson(),purchase.getSignature());
                }
                event=event.setContentId(TextUtils.isEmpty(sku)?purchase.getSku():sku);
                event= event.setPrice((PurchaseHelper.getPurchaseAmount(TextUtils.isEmpty(sku) ? purchase.getSku() : sku))/100.0);

                Tracker.sendEvent(event);
        }

        public void trackSubsRenew(String sku) {
            if(!KochavaConfig.enabled){return;}
            if (!TextUtils.isEmpty(sku)) {
                trackPurchase(null,sku);
            }
        }
        public void trackPurchase(Purchase purchase){
            if(!KochavaConfig.enabled){return;}
            if(purchase!=null){
                trackPurchase(purchase,null);
            }
            }

        public void trackRegister(boolean login) {
            if(!KochavaConfig.enabled){return;}
            Tracker.Event event=updateTracker(login?new Tracker.Event(CustomTrackers.VPN_LOGIN):
                    new Tracker.Event(Tracker.EVENT_TYPE_REGISTRATION_COMPLETE));
            Tracker.sendEvent(event);
        }
        private static final class KochavaTrackingHolder{
        private static final KochavaTracking instance=new KochavaTracking();
    }
    public static KochavaTracking getInstance(){
        return KochavaTrackingHolder.instance;
    }

}
