package com.finjan.securebrowser.tracking;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 1/9/17.
 *
 * Used for localytics tracking of events and screens
 *
 */

public interface CustomTrackers {
    String VPN_REGISTER = "VPN USER REGISTERED";
    String VPN_SUBSCRIBED = "VPN SUBSCRIBED";
    String VPN_SUBSCRIPTION_RENEWED = "Renewal";
    String VPN_LOGIN="VPN USER LOGIN";
}