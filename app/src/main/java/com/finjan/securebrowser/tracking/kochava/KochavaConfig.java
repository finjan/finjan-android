package com.finjan.securebrowser.tracking.kochava;

import android.content.Context;

import com.finjan.securebrowser.tracking.facebook.FbTrackingConfig;
import com.kochava.base.Tracker;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC. on 1/9/17
 *
 * Used for kochava tracking of events and screens
 */

public class KochavaConfig {

//    private static final String kochava_app_id="kofinjan-vpn-android-mt6fg9duh";
    private static final String app_id="25006";
    private static final class KochavaConfigHolder{
        private static final KochavaConfig instance=new KochavaConfig();
    }
    public static KochavaConfig getInstance(){
        return KochavaConfigHolder.instance;
    }
    public void initKochavaTracking(Context context){
        if(!enabled)return;
        Tracker.configure(new Tracker.Configuration(context.getApplicationContext())
                .setAppGuid(appId)
                .setLogLevel(Tracker.LOG_LEVEL_INFO));
    }
    protected static boolean enabled=true;
    private static String appId;
    public static void plistConfig(boolean enabled, String appId){
        KochavaConfig.enabled=enabled;
        KochavaConfig.appId=appId;
    }
}
