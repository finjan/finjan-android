package com.finjan.securebrowser.tracking.tune;

import android.text.TextUtils;

import com.avira.common.id.HardwareId;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.licensing.utils.PurchaseHelper;
import com.tune.Tune;
import com.tune.TuneEvent;
import com.tune.TuneEventItem;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by anurag on 18/08/17.
 *
 *
 *
 *  Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class TuneRegisterTrack extends TuneEvent{

    private static final String eventRegister="UserRegister";
    private static final String eventLogin="UserLogin";
    public TuneRegisterTrack(String eventName,boolean login) {

        super(login? TuneEvent.LOGIN:TuneEvent.REGISTRATION);
        withDate1(new Date(System.currentTimeMillis()));

        ArrayList<TuneEventItem> list = new ArrayList<>();
        list.add(new TuneEventItem("Device Id").withAttribute1(HardwareId.get(FinjanVPNApplication.getInstance())));
        list.add(new TuneEventItem("Finjan User").withAttribute1(AuthenticationHelper.getInstnace().getUserEmail(FinjanVPNApplication.getInstance())));
        if (AuthenticationHelper.getInstnace().getFinjanUser() != null) {
            list.add(new TuneEventItem("Finjan Username").withAttribute1(AuthenticationHelper.getInstnace().getFinjanUser().getUserName()));
        }
        withEventItems(list);

    }
}
