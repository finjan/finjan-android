package com.finjan.securebrowser.tracking.google_tracking

import com.finjan.securebrowser.parser.PList
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers
import com.finjan.securebrowser.vpn.FinjanVPNApplication
//import com.localytics.android.Localytics

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Used for google tracking of events and screens
 *
 */
class GoogleTrackers {



    val TAG = GoogleTrackers::class.java.simpleName


    var SCREEN_ConfigLoading: String = "AppConfigLoading"

    var SCREEN_Onboarding_1: String = "Onboarding - Features"
    var SCREEN_Onboarding_2: String = "Onboarding - Connect"
    var SCREEN_Onboarding_3: String = "Onboarding - Locations"
    var SCREEN_Onboarding_4: String = "Onboarding - Devices"

//    var SCREEN_Onboarding: String = "Onboarding"


    var SCREEN_VPN_Main: String = "VPN_Main"

    var SCREEN_Browser: String = "Browser"

    var SCREEN_Tabs: String = "Tabs"

    var SCREEN_History: String = "History"

    var SCREEN_Bookmarks: String = "Bookmarks"

    var SCREEN_Settings: String = "Settings"

    var SCREEN_Passcode: String = "App Passcode Lock"
    var SCREEN_PasscodeChange: String = "App Passcode Change"

    var SCREEN_InAppPurchase: String = "InAppPurchase"

    var SCREEN_Privacy : String = "Privacy"

    var SCREEN_Login: String = "Login"
    var SCREEN_EmailLogin: String = "Email Login"
//    var SCREEN_Social: String = "Social"

    var SCREEN_SignUp: String = "Email SignUp"

    var SCREEN_BlockedTrackers : String = "BlockedTrackers"

    var SCREEN_Trackers : String = "Trackers"

    var SCREEN_DangerousURL : String = "DangerousURL"

    var SCREEN_Help : String = "Help"

    var SCREEN_About : String = "About"

    var SCREEN_PrivacyPolicy: String = "PrivacyPolicy"

    var SCREEN_VirusResults: String = "VirusResults"

    var SCREEN_PrivateDataPref: String = "PrivateDataPref"

    var SCREEN_LocationChange : String = "Location Change"
    var SCREEN_Home : String = "Browser Home"
    var SCREEN_FindMyIP : String = "FindMyIp"
    var SCREEN_VPNMenu : String = "VPN Menu"
        var SCREEN_GET_STARTED: String = "Get Started"
        var SCREEN_ScannersReport: String = "Full Virus Scanners Report"
        var SCREEN_PrivateData: String = "Private Data Settings"
        var SCREEN_SettingsVPN: String = "Settings VPN ON Demand"
        var SCREEN_FacebookLogin: String = "Facebook Login"
        var SCREEN_FacebookSignUp: String = "Facebook SignUp"
    var SCREEN_GoogleLogin: String = "Google Login"
    var SCREEN_GoogleSignUp: String = "Google SignUp"





    //events

    var EVENT_VPN_Only: String = "VPN_Only"

    var EVENT_VPN_Browser : String = "VPN_Browser"

    var EVENT_Upgrade_Button : String = "Upgrade_Button"

    var EVENT_LocationChange : String = "LocationChange"

    var EVENT_Desktop_Share : String = "VPN Menu Desktop Share Clicked"

    var EVENT_VPNConnect : String = "VPN_Connect"

    var EVENT_Login : String = "Login"

    var EVENT_Registration: String = "Registration"

    var EVENT_Hamburger_Menu_Open: String = "Browser Menu Open"
    var EVENT_Hamburger_Menu_Closed: String = "Browser Menu Closed"
    var EVENT_Hamburger_Menu_Vpn: String = "Hamburger Menu Vpn Clicked"
    var EVENT_Hamburger_Menu_Bookmarks: String = "Hamburger Menu Bookmarks Clicked"
    var EVENT_Hamburger_Menu_History: String = "Hamburger Menu History Clicked"
    var EVENT_Hamburger_Menu_Share: String = "Hamburger Menu Share Clicked"
    var EVENT_Hamburger_Menu_Rate: String = "Hamburger Menu Rate Clicked"
    var EVENT_Hamburger_Menu_About: String = "Hamburger Menu About Clicked"
    var EVENT_Hamburger_Menu_Help: String = "Hamburger Menu Help Clicked"
    var EVENT_Hamburger_Menu_Settings: String = "Hamburger Menu Settings Clicked"

    var EVENT_Virus_Scan_Button: String = "Virus_Scan_Button"
    var EVENT_Bookmark_Button: String = "Bookmark_Button"
    var EVENT_Virus_Results: String = "Virus_Results"
    var EVENT_HomeButton: String = "HomeButton"
    var EVENT_Share: String = "VPN Menu Share Clicked"
    var EVENT_Rate: String = "VPN Menu Rate Clicked"
    var EVENT_Facebook_login: String = "Facebook_login"
    var EVENT_Google_login: String = "Google_login"


    fun setPlistData(plist: PList) {
        if (plist.get("googleanalytics.screennames") != null) {
            val plist = plist.get("googleanalytics.screennames")

            SCREEN_About=plist?.get("About")?.data?:"About"
            SCREEN_BlockedTrackers=plist?.get("BlockedTrackers")?.data?:"BlockedTrackers"
            SCREEN_Bookmarks=plist?.get("Bookmarks")?.data?:"Bookmarks"
            SCREEN_Browser=plist?.get("Browser")?.data?:"Browser"
            SCREEN_ConfigLoading=plist?.get("ConfigLoading")?.data?:"AppConfigLoading"
            SCREEN_Onboarding_1=plist?.get("Onboarding_1")?.data?:SCREEN_Onboarding_1
            SCREEN_Onboarding_2=plist?.get("Onboarding_2")?.data?:SCREEN_Onboarding_2
            SCREEN_Onboarding_3=plist?.get("Onboarding_3")?.data?:SCREEN_Onboarding_3
            SCREEN_Onboarding_4=plist?.get("Onboarding_4")?.data?:SCREEN_Onboarding_4
            SCREEN_DangerousURL=plist?.get("DangerousURL")?.data?:"DangerousURL"
            SCREEN_Help=plist?.get("Help")?.data?:"Help"
            SCREEN_History=plist?.get("History")?.data?:"History"
            SCREEN_InAppPurchase=plist.get("InAppPurchase").data?:"InAppPurchase"
            SCREEN_Passcode=plist?.get("Passcode")?.data?:"App Passcode Change"
            SCREEN_PasscodeChange=plist?.get("PasscodeChange")?.data?:"App Passcode Change"
            SCREEN_Login=plist?.get("Login")?.data?:"Lock"
            SCREEN_EmailLogin=plist?.get("EmailLogin")?.data?:"Email Login"
            SCREEN_Privacy=plist?.get("Privacy")?.data?:"Privacy"
            SCREEN_PrivacyPolicy=plist?.get("PrivacyPolicy")?.data?:"PrivacyPolicy"
            SCREEN_Settings=plist?.get("Settings")?.data?:"Settings"
            SCREEN_SignUp=plist?.get("SignUp")?.data?:"SignUp"
            SCREEN_PrivateDataPref=plist?.get("PrivateDataPref")?.data?:"PrivateDataPref"
            SCREEN_Tabs=plist?.get("Tabs")?.data?:"Tabs"
            SCREEN_Trackers=plist.get("Trackers")?.data?:"Trackers"
            SCREEN_VPN_Main=plist?.get("VPN_Main")?.data?:"VPN_Main"
            SCREEN_VirusResults=plist?.get("VirusResults")?.data?:"VirusResults"
            SCREEN_LocationChange=plist?.get("LocationChange")?.data?:"Location Change"
            SCREEN_Home=plist?.get("Home")?.data?:"Browser Home"
            SCREEN_VPNMenu=plist?.get("VPNMenu")?.data?:"VPN Menu"
            SCREEN_ScannersReport=plist?.get("ScannersReport")?.data?:"Full Virus Scanners Report"
            SCREEN_PrivateData=plist?.get("PrivateData")?.data?:"Private Data Settings"
            SCREEN_SettingsVPN=plist?.get("PrivateData")?.data?:"Settings VPN ON Demand"
            SCREEN_GET_STARTED=plist?.get("GET_STARTED")?.data?:"Get Started"
            SCREEN_FacebookLogin=plist?.get("FacebookLogin")?.data?:"Facebook Login"
            SCREEN_FacebookSignUp=plist?.get("FacebookSignUp")?.data?:"Facebook SignUp"
            SCREEN_GoogleLogin=plist?.get("FacebookSignUp")?.data?:"Google Login"
            SCREEN_GoogleSignUp=plist?.get("FacebookSignUp")?.data?:"Google SignUp"
            SCREEN_FindMyIP=plist?.get("FindMyIP")?.data?:"FindMyIP"
        }
        if (plist.get("googleanalytics.events") != null) {
            val plist = plist.get("googleanalytics.events")
            EVENT_Desktop_Share=plist?.get("Desktop_Share")?.data?:"Desktop_Share"
            EVENT_Hamburger_Menu_Open=plist?.get("Hamburger_Menu_Open")?.data?:"Browser Menu Open"
            EVENT_Hamburger_Menu_Closed=plist?.get("Hamburger_Menu_Closed")?.data?:"Browser Menu Closed"
            EVENT_Hamburger_Menu_Vpn=plist?.get("Hamburger_Menu_Vpn")?.data?:"Hamburger Menu Vpn Clicked"
            EVENT_Hamburger_Menu_Bookmarks=plist?.get("Hamburger_Menu_Bookmarks")?.data?:"Hamburger Menu Bookmarks Clicked"
            EVENT_Hamburger_Menu_History=plist?.get("Hamburger_Menu_History")?.data?:"Hamburger Menu Bookmarks Clicked"
            EVENT_Hamburger_Menu_Share=plist?.get("Hamburger_Menu_Share")?.data?:"Hamburger Menu Share Clicked"
            EVENT_Hamburger_Menu_Rate=plist?.get("Hamburger_Menu_Rate")?.data?:"Hamburger Menu Rate Clicked"
            EVENT_Hamburger_Menu_About=plist?.get("Hamburger_Menu_About")?.data?:"Hamburger Menu About Clicked"
            EVENT_Hamburger_Menu_Help=plist?.get("Hamburger_Menu_Help")?.data?:"Hamburger Menu Help Clicked"
            EVENT_Hamburger_Menu_Settings=plist?.get("Hamburger_Menu_Settings")?.data?:"Hamburger Menu Settings Clicked"
            EVENT_HomeButton=plist?.get("HomeButton")?.data?:"HomeButton"
            EVENT_LocationChange=plist?.get("LocationChange")?.data?:"LocationChange"
            EVENT_Login=plist?.get("Login")?.data?:"Login"
            EVENT_Rate=plist?.get("Rate")?.data?:"Rate"
            EVENT_Registration=plist?.get("Registration")?.data?:"Registration"
            EVENT_Share=plist?.get("Share")?.data?:"Registration"
            EVENT_Upgrade_Button=plist?.get("Upgrade_Button")?.data?:"Upgrade_Button"
            EVENT_VPNConnect=plist?.get("VPNConnect")?.data?:"VPN_Connect"
            EVENT_VPN_Browser=plist?.get("VPN_Browser")?.data?:"VPN_Browser"
            EVENT_VPN_Only=plist?.get("VPN_Only")?.data?:"VPN_Only"
            EVENT_Virus_Results=plist?.get("Virus_Results")?.data?:"Virus_Results"
            EVENT_Virus_Scan_Button=plist?.get("Virus_Scan_Button")?.data?:"Virus_Scan_Button"
            EVENT_Bookmark_Button=plist?.get("Bookmark_Button")?.data?:"Bookmark_Button"
            EVENT_Facebook_login=plist?.get("Facebook_login")?.data?:"Facebook_login"
            EVENT_Google_login=plist?.get("Google_login")?.data?:"Google_login"
        }
    }

    companion object {

        private object HOLDER {
            var instance = GoogleTrackers()
        }
        val instance: GoogleTrackers by lazy { HOLDER.instance }

        fun setTracking(trackerName: String){
            FinjanVPNApplication.getInstance().setTrcakerName(trackerName)
//            Localytics.tagEvent(trackerName,HashMap<String,String>())
        }
        fun setSTracking(trackerName: String){
            FinjanVPNApplication.getInstance().setTrcakerName(trackerName)
            setLocalyticsScreenTracking(trackerName)
        }

        fun setEventTracking(trackerName: String){
            FinjanVPNApplication.getInstance().setEventTrack("",trackerName)
        }

        private fun setLocalyticsScreenTracking(screen:String) {
            LocalyticsTrackers.setScreenTracking(screen)
        }
        fun setSConfigLoadingTracking(){
            setSTracking(GoogleTrackers.instance.SCREEN_ConfigLoading)
        }

        fun setSOnboarding1() = setSTracking(GoogleTrackers.instance.SCREEN_Onboarding_1)
        fun setSOnboarding2() = setSTracking(GoogleTrackers.instance.SCREEN_Onboarding_2)
        fun setSOnboarding3() = setSTracking(GoogleTrackers.instance.SCREEN_Onboarding_3)
        fun setSOnboarding4() = setSTracking(GoogleTrackers.instance.SCREEN_Onboarding_4)
        fun setSVPNMain() = setSTracking(GoogleTrackers.instance.SCREEN_VPN_Main)
        fun setSBrowser() = setSTracking(GoogleTrackers.instance.SCREEN_Browser)
        fun setSTabs() = setSTracking(GoogleTrackers.instance.SCREEN_Tabs)
        fun setSHistory() = setSTracking(GoogleTrackers.instance.SCREEN_History)
        fun setSBookmarks() = setSTracking(GoogleTrackers.instance.SCREEN_Bookmarks)
        fun setSSettings() = setSTracking(GoogleTrackers.instance.SCREEN_Settings)
        fun setSPasscode() = setSTracking(GoogleTrackers.instance.SCREEN_Passcode)
        fun setSPasscodeChange() = setSTracking(GoogleTrackers.instance.SCREEN_PasscodeChange)
        fun setSPrivacy() = setSTracking(GoogleTrackers.instance.SCREEN_Privacy)
        fun setSInAppPurchase() = setSTracking(GoogleTrackers.instance.SCREEN_InAppPurchase)
        fun setSLogin() = setSTracking(GoogleTrackers.instance.SCREEN_Login)
        fun setSEmailLogin() = setSTracking(GoogleTrackers.instance.SCREEN_EmailLogin)
        fun setSSignUp() = setSTracking(GoogleTrackers.instance.SCREEN_SignUp)
        fun setSBlockedTrackers() = setSTracking(GoogleTrackers.instance.SCREEN_BlockedTrackers)
        fun setSTrackers() = setSTracking(GoogleTrackers.instance.SCREEN_Trackers)
        fun setSDangerousURL() = setSTracking(GoogleTrackers.instance.SCREEN_DangerousURL)
        fun setSHelp() = setSTracking(GoogleTrackers.instance.SCREEN_Help)
        fun setSAbout() = setSTracking(GoogleTrackers.instance.SCREEN_About)
        fun setSPrivacyPolicy() = setSTracking(GoogleTrackers.instance.SCREEN_PrivacyPolicy)
        fun setSVirusResults() = setSTracking(GoogleTrackers.instance.SCREEN_VirusResults)
        fun setSVPNMenu() = setSTracking(GoogleTrackers.instance.SCREEN_VPNMenu)

        fun setSScannersReport() = setSTracking(GoogleTrackers.instance.SCREEN_ScannersReport)
        fun setSPrivateData() = setSTracking(GoogleTrackers.instance.SCREEN_PrivateData)
        fun setSSettingsVPN() = setSTracking(GoogleTrackers.instance.SCREEN_SettingsVPN)

        fun setSGET_STARTED() = setSTracking(GoogleTrackers.instance.SCREEN_GET_STARTED)
        fun setsFacebookLogin() = setSTracking(GoogleTrackers.instance.SCREEN_FacebookLogin)
        fun setSFacebookSignUp() = setSTracking(GoogleTrackers.instance.SCREEN_FacebookSignUp)
        fun setSGoogleLogin() = setSTracking(GoogleTrackers.instance.SCREEN_GoogleLogin)
        fun setSGoogleSignUp() = setSTracking(GoogleTrackers.instance.SCREEN_GoogleSignUp)
        fun setSFindMyIP() = setSTracking(GoogleTrackers.instance.SCREEN_FindMyIP)
        fun setSHome() = setSTracking(GoogleTrackers.instance.SCREEN_Home)
        fun setSLocationChange() = setSTracking(GoogleTrackers.instance.SCREEN_LocationChange)
        fun setSPrivateDataPref() = setSTracking(GoogleTrackers.instance.SCREEN_PrivateDataPref)

        //events
        fun setELogin() = setTracking(GoogleTrackers.instance.EVENT_Login)
        fun setEVPNConnect() = setTracking(GoogleTrackers.instance.EVENT_VPNConnect)
        fun setEDesktop_Share() = setTracking(GoogleTrackers.instance.EVENT_Desktop_Share)
        fun setELocationChange() = setTracking(GoogleTrackers.instance.EVENT_LocationChange)
        fun setEUpgrade_Button() = setTracking(GoogleTrackers.instance.EVENT_Upgrade_Button)
        fun setEVPN_Browser() = setTracking(GoogleTrackers.instance.EVENT_VPN_Browser)
        fun setEVPN_Only() = setTracking(GoogleTrackers.instance.EVENT_VPN_Only)
        fun setERegistration() = setTracking(GoogleTrackers.instance.EVENT_Registration)
        fun setEHamburger_Menu_Open() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_Open)
        fun setEHamburger_Menu_Closed() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_Closed)
        fun setEHamburger_Menu_Vpn() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_Vpn)
        fun setEHamburger_Menu_Bookmarks() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_Bookmarks)
        fun setEHamburger_Menu_History() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_History)
        fun setEHamburger_Menu_Share() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_Share)
        fun setEHamburger_Menu_Rate() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_Rate)
        fun setEHamburger_Menu_About() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_About)
        fun setEHamburger_Menu_Help() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_Help)
        fun setEHamburger_Menu_Settings() = setTracking(GoogleTrackers.instance.EVENT_Hamburger_Menu_Settings)
        fun setEVirus_Scan_Button() = setTracking(GoogleTrackers.instance.EVENT_Virus_Scan_Button)
        fun setEBookmark_Button() = setTracking(GoogleTrackers.instance.EVENT_Bookmark_Button)
        fun setEVirus_Results() = setTracking(GoogleTrackers.instance.EVENT_Virus_Results)
        fun setEHomeButton() = setTracking(GoogleTrackers.instance.EVENT_HomeButton)
        fun setEShare() = setTracking(GoogleTrackers.instance.EVENT_Share)
        fun setERate() = setTracking(GoogleTrackers.instance.EVENT_Rate)
        fun setEFacebook_login() = setTracking(GoogleTrackers.instance.EVENT_Facebook_login)
        fun setEGoogle_login() = setTracking(GoogleTrackers.instance.EVENT_Google_login)
    }
}