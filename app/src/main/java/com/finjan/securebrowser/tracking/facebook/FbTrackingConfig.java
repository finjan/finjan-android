package com.finjan.securebrowser.tracking.facebook;

import android.app.Application;
import android.content.Context;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.utils.PurchaseHelper;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Used for facebook tracking of events and screens
 */


public class FbTrackingConfig {

    private static final class FbTrackingConfigHolder{
        private static final FbTrackingConfig Instance=new FbTrackingConfig();
    }

    private com.electrolyte.sociallogin.tracking.facebook.FbTrackingConfig instance =
            com.electrolyte.sociallogin.tracking.facebook.FbTrackingConfig.Companion.getInstance();
    public static final FbTrackingConfig getInstance(){
        return FbTrackingConfigHolder.Instance;
    }
    public void init(Application context){
        instance.init(context);

    }
    public void logRegistration(Context context) {
        if(!enabled){return;}
        instance.logRegistration(context,AuthenticationHelper.getInstnace().getUserEmail(context),
                AuthenticationHelper.getInstnace().getUserName(context));
    }
    public void logLogin(Context context) {
        if(!enabled){return;}
        instance.logLogin(context,AuthenticationHelper.getInstnace().getUserEmail(context),
                AuthenticationHelper.getInstnace().getUserName(context));
    }
    public void logPurchase(Context context, Purchase purchase) {
        if(!enabled){return;}
        String purchaseEnd=purchase.getmExpiryTimeMillis()>0?DateHelper.getInstnace().getServerFormatDate(purchase.getmExpiryTimeMillis())
                :PurchaseHelper.getExpireTime(purchase);
        instance.logPurchase(context,AuthenticationHelper.getInstnace().getUserEmail(context),
                AuthenticationHelper.getInstnace().getUserName(context),DateHelper.getInstnace().getServerFormatDate(purchase.getPurchaseTime()),
                purchaseEnd,purchase.getSku(),PurchaseHelper.getExpireInterval(purchase),
                (PurchaseHelper.getPurchaseAmount(purchase.getSku())/100.0));
    }
    public void logPurchaseRenew(Context context,String sku) {
        if(!enabled){return;}
        instance.logPurchaseRenew(context,AuthenticationHelper.getInstnace().getUserEmail(context),
                AuthenticationHelper.getInstnace().getUserName(context),sku,
                PurchaseHelper.getPurchaseAmount(sku)/100.0,
                PurchaseHelper.getExpireInterval(sku));
    }
    private static boolean enabled=true;
    private static String appId;
    public static void plistConfig(boolean enabled, String appId){
        FbTrackingConfig.enabled=enabled;
        FbTrackingConfig.appId=appId;
    }
}
