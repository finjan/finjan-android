package com.finjan.securebrowser.tracking.localytics

import android.text.TextUtils
import com.finjan.securebrowser.helpers.AuthenticationHelper
import com.finjan.securebrowser.parser.PList
import com.finjan.securebrowser.vpn.FinjanVPNApplication
import com.finjan.securebrowser.vpn.util.TrafficController

import android.content.Context
import com.finjan.securebrowser.helpers.PlistHelper
import com.finjan.securebrowser.helpers.logger.Logger
import com.finjan.securebrowser.helpers.sharedpref.UserPref
import com.finjan.securebrowser.vpn.util.Util
import com.google.firebase.messaging.FirebaseMessaging
import java.util.*
import kotlin.collections.HashMap


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 * Used for localytics tracking of events and screens
 *
 */
class LocalyticsTrackers {

    //    var SCREEN_LOGIN: String = "LOGIN"
//    var SCREEN_REGISTRATION: String = "REGISTRATION"
//    var SCREEN_VPN: String = "VPN"
//    var SCREEN_VPN_MENU: String = "VPN MENU"
//    var SCREEN_AUTO_CONNECT: String = "AUTO CONNECT"
    var SCREEN_BROWSER: String = "BROWSER"
    var SCREEN_UPGRADE: String = "UPGRADE"
    var SCREEN_IAP_MONTHLY: String = "IAP MONTHLY"
    var SCREEN_IAP_ANNUAL: String = "IAP ANNUAL"
    var SCREEN_LOGOUT: String = "LOGOUT"
    var SCREEN_SHARE: String = "SHARE"
    var SCREEN_RATE: String = "RATE"
    var SCREEN_ABOUT: String = "ABOUT"
    var SCREEN_HELP: String = "HELP"
    var SCREEN_BROWSER_TRACKER: String = "BROWSER TRACKER"
    var SCREEN_BROWSER_SCAN: String = "BROWSER SCAN"
    var SCREEN_BROWSER_HISTORY: String = "BROWSER HISTORY"
    var SCREEN_BROWSER_BOOKMARKS: String = "BROWSER BOOKMARKS"
    var SCREEN_BROWSER_SETTINGS: String = "BROWSER SETTINGS"
    var SCREEN_BROWSER_MENU: String = "BROWSER MENU"
    var SCREEN_BROWSER_HELP: String = "BROWSER HELP"
    var SCREEN_BROWSER_SHARE: String = "BROWSER SHARE"
    var SCREEN_BROWSER_RATE: String = "BROWSER RATE"



    //new trackers as of doc
    var EVENT_Settings: String = "Settings Browser"
    var EVENT_VPN_Page: String = "VPN Page"
    var EVENT_InAppPurchase: String = "IAP Page"
    var EVENT_VPNMenu: String = "Settings VPN"
    var EVENT_Upgrade_Button: String = "Upgrade Button"
    var EVENT_IAP_Success: String = "IAP Success"
    var EVENT_IAP_Leave: String = "IAP Leave"
    var EVENT_VPN_Page_First_Time: String = "VPN Page First Time"
    var EVENT_Browser_Page_First_Time: String = "Browser Page First Time"
    var EVENT_LocationChange: String = "Change Location"
    var EVENT_upgrade_from_location_change: String = "Upgrade from Change Location Fail"
    var EVENT_VPNConnect: String = "VPN Connect"
    var EVENT_VPNDisconnect: String = "VPN Disconnect"
    var EVENT_Logout: String = "Logout"
    var EVENT_Revenue: String = "Revenue"
    var EVENT_Share: String = "Share App"
    var EVENT_Desktop_Share: String = "Share Desktop"
    var EVENT_Rate: String = "Rate"
    var EVENT_Auto_Connect_Page: String = "Auto Connect Page"
    var EVENT_Auto_Connect_Change: String = "Auto Connect Change"
    var EVENT_Auto_Protect_Alert: String = "Auto Protect Alert"
    var EVENT_LOGIN: String = "LOGIN"
    var EVENT_REGISTRATION: String = "REGISTRATION"
    var EVENT_Onboarding_1: String = "Onboarding - Features"
    var EVENT_Onboarding_2: String = "Onboarding - Connect"
    var EVENT_Onboarding_3: String = "Onboarding - Locations"
    var EVENT_Onboarding_4: String = "Onboarding - Devices"
    var EVENT_First_Open: String = "First Open"
    var EVENT_Promo_Start: String = "Promo Start"
    var EVENT_Promo_End: String = "Promo End"
    var EVENT_Promo_one_day_left: String = "Last Promo Day Left"
    var EVENT_Promo_day_left: String = "Promo Days Left"
    var EVENT_Current_Subscription_Status: String = "Current Subscription Status"
    var EVENT_Browser_Page: String = "Browser Page"
    var EVENT_Browser_New_Tab: String = "Browser New Tab"
    var EVENT_Bookmarks: String = "Bookmarks"
    var EVENT_Browser_History: String = "Browser History"
    var EVENT_VPN_Off_with_Auto_Protect: String = "VPN Off with Auto Protect"
    var EVENT_EXHAUSTED_TRAFFIC: String = "Out Of Data"


    fun setPlistData(plist: PList) {
        if (plist.get("localytics.events") != null) {
            val plist = plist.get("localytics.events")
            EVENT_VPNMenu = plist?.get("VPNMenu")?.data ?: "Settings VPN"
            EVENT_InAppPurchase = plist.get("InAppPurchase").data ?: "IAP Page"
            EVENT_Settings = plist?.get("Settings")?.data ?: "Settings Browser"
            EVENT_VPN_Page = plist?.get("VPN_Page")?.data ?: "VPN Page"
            EVENT_Desktop_Share = plist?.get("Desktop_Share")?.data ?: "Share Desktop"
            EVENT_LocationChange = plist?.get("LocationChange")?.data ?: "Change Location"
            EVENT_upgrade_from_location_change = plist?.get("upgrade_from_location_change")?.data ?: "Upgrade from Change Location Fail"
            EVENT_Logout = plist?.get("Logout")?.data ?: "Logout"
            EVENT_Revenue= plist?.get("Revenue")?.data ?: "Revenue"
            EVENT_Rate = plist?.get("Rate")?.data ?: "Rate"
            EVENT_Share = plist?.get("Share")?.data ?: "Share App"
            EVENT_Upgrade_Button = plist?.get("Upgrade_Button")?.data ?: "Upgrade Button"
            EVENT_IAP_Success= plist?.get("IAP_Success")?.data ?: "IAP Success"
            EVENT_IAP_Leave= plist?.get("IAP_Leave")?.data ?: "IAP Leave"
            EVENT_VPN_Page_First_Time= plist?.get("VPN_Page_First_Time")?.data ?: "VPN Page First Time"
            EVENT_Browser_Page_First_Time= plist?.get("Browser_Page_First_Time")?.data ?: "Browser Page First Time"
            EVENT_VPNConnect = plist?.get("VPNConnect")?.data ?: "VPN Connect"
            EVENT_VPNDisconnect= plist?.get("VPNDisconnect")?.data ?: "VPN Disconnect"
            EVENT_Auto_Connect_Page= plist?.get("Auto_Connect_Page")?.data ?: "Auto Connect Page"
            EVENT_Auto_Connect_Change= plist?.get("Auto_Connect_Change")?.data ?: "Auto Connect Change"
            EVENT_Auto_Protect_Alert= plist?.get("Auto_Protect_Alert")?.data ?: "Auto Protect Alert"
            EVENT_LOGIN= plist?.get("login")?.data ?: "LOGIN"
            EVENT_REGISTRATION= plist?.get("registration")?.data ?: "REGISTRATION"
            EVENT_Onboarding_1= plist?.get("Onboarding_1")?.data ?: "Onboarding - Features"
            EVENT_Onboarding_2= plist?.get("Onboarding_2")?.data ?: "Onboarding - Connect"
            EVENT_Onboarding_3= plist?.get("Onboarding_3")?.data ?: "Onboarding - Locations"
            EVENT_Onboarding_4= plist?.get("Onboarding_4")?.data ?: "Onboarding - Devices"
            EVENT_First_Open= plist?.get("First_Open")?.data ?: "First Open"
            EVENT_Promo_Start= plist?.get("Promo_Start")?.data ?: "Promo Start"
            EVENT_Promo_End= plist?.get("Promo_End")?.data ?: "Promo End"
            EVENT_Promo_one_day_left= plist?.get("Last_Promo_Day_Left")?.data ?: "Promo one day left"
            EVENT_Promo_day_left= plist?.get("Promo_Days_Left")?.data ?: "Promo Days Left"
            EVENT_Current_Subscription_Status= plist?.get("Current_Subscription_Status")?.data ?: "Current Subscription Status"
            EVENT_Browser_Page= plist?.get("Browser_Page")?.data ?: "Browser Page"
            EVENT_Browser_New_Tab= plist?.get("Browser_New_Tab")?.data ?: "Browser New Tab"
            EVENT_Bookmarks= plist?.get("Bookmarks")?.data ?: "Bookmarks"
            EVENT_Browser_History= plist?.get("Browser_History")?.data ?: "Browser History"
            EVENT_VPN_Off_with_Auto_Protect= plist?.get("VPN_Off_with_Auto_Protect")?.data ?: "VPN Off with Auto Protect"
            EVENT_EXHAUSTED_TRAFFIC= plist?.get("Out_Of_Data")?.data ?: "Out Of Data"
        }

//        if (plist.get("localytics.screennames") != null) {
//            val plist = plist.get("localytics.screennames")
////            SCREEN_LOGIN = plist?.get("login")?.data ?: "LOGIN"
////            SCREEN_REGISTRATION = plist.get("registration").data ?: "REGISTRATION"
////            SCREEN_VPN = plist?.get("vpn")?.data ?: "VPN"
////            SCREEN_VPN_MENU = plist?.get("vpn_menu")?.data ?: "VPN_MENU"
////            SCREEN_AUTO_CONNECT = plist?.get("auto_connect")?.data ?: "AUTO_CONNECT"
//            EVENT_upgrade_from_location_change = plist?.get("upgrade_from_location_change")?.data ?: "Upgrade from Change Location Fail"
//            EVENT_Logout = plist?.get("Logout")?.data ?: "Logout"
//            EVENT_Revenue= plist?.get("Revenue")?.data ?: "Revenue"
//            EVENT_Rate = plist?.get("Rate")?.data ?: "Rate"
//            EVENT_Share = plist?.get("Share")?.data ?: "Share App"
//            EVENT_Upgrade_Button = plist?.get("Upgrade_Button")?.data ?: "Upgrade Button"
//            EVENT_IAP_Success= plist?.get("IAP_Success")?.data ?: "IAP Success"
//            EVENT_IAP_Leave= plist?.get("IAP_Leave")?.data ?: "IAP Leave"
//            EVENT_VPN_Page_First_Time= plist?.get("VPN_Page_First_Time")?.data ?: "VPN Page First Time"
//            EVENT_Browser_Page_First_Time= plist?.get("Browser_Page_First_Time")?.data ?: "Browser Page First Time"
//            EVENT_VPNConnect = plist?.get("VPNConnect")?.data ?: "VPN Connect"
//            EVENT_VPNDisconnect= plist?.get("VPNDisconnect")?.data ?: "VPN Disconnect"
//            EVENT_Auto_Connect_Page= plist?.get("Auto_Connect_Page")?.data ?: "Auto Connect Page"
//            EVENT_Auto_Connect_Change= plist?.get("Auto_Connect_Change")?.data ?: "Auto Connect Change"
//        }
    }
//    var SCREEN_BROWSER: String = "BROWSER"
//    var SCREEN_UPGRADE: String = "UPGRADE"
//    var SCREEN_IAP_MONTHLY: String = "IAP MONTHLY"
//    var SCREEN_IAP_ANNUAL: String = "IAP ANNUAL"
//    var SCREEN_LOGOUT: String = "LOGOUT"
//    var SCREEN_SHARE: String = "SHARE"
//    var SCREEN_RATE: String = "RATE"
//    var SCREEN_ABOUT: String = "ABOUT"
//    var SCREEN_HELP: String = "HELP"
//    var SCREEN_BROWSER_TRACKER: String = "BROWSER TRACKER"
//    var SCREEN_BROWSER_SCAN: String = "BROWSER SCAN"
//    var SCREEN_BROWSER_HISTORY: String = "BROWSER HISTORY"
//    var SCREEN_BROWSER_BOOKMARKS: String = "BROWSER BOOKMARKS"
//    var SCREEN_BROWSER_SETTINGS: String = "BROWSER SETTINGS"
//    var SCREEN_BROWSER_MENU: String = "BROWSER MENU"
//    var SCREEN_BROWSER_HELP: String = "BROWSER HELP"
//    var SCREEN_BROWSER_SHARE: String = "BROWSER SHARE"
//    var SCREEN_BROWSER_RATE: String = "BROWSER RATE"

    companion object {

        private object HOLDER {
            var instance = LocalyticsTrackers()
        }
        val TAG = LocalyticsTrackers::class.java.simpleName

        val instance: LocalyticsTrackers by lazy { HOLDER.instance }

        fun setDimension(count:Int, value:String){

//                Logger.logE(TAG, "Dimension - " + count + "----" + value)
//                Localytics.setCustomDimension(count, value)

        }
        fun setProfile(name:String, value: String){

//                Localytics.setProfileAttribute(name, value, Localytics.ProfileScope.APPLICATION)
//                Logger.logE(TAG, "Profile - " + name)

        }
        fun setProfile(name: String,value: Long){

//                Localytics.setProfileAttribute(name, value, Localytics.ProfileScope.APPLICATION)
//                Logger.logE(TAG, "Profile - " + name)

        }

        fun setTracking(trackerName: String) {

                val map = HashMap<String, String>()
                setTracking(trackerName, map)

        }
        fun setTracking(trackerName: String, arggs:Array<String>) {

                val map = HashMap<String, String>()
                setTracking(trackerName, map)

        }
        fun setScreenTracking(screenName: String) {

//                Logger.logE(TAG, "Event ----" + screenName)
//                Localytics.tagScreen(screenName)

        }

        private fun setTracking(eventName: String, map: Map<String, String>) {

//                Logger.logE(TAG, "Event ----" + eventName)
//                Localytics.tagEvent(eventName, map)

        }
        private fun setLTV(purchaseName: String,sku:String,type:String,price:Long){

//                Localytics.tagPurchased(purchaseName, sku, type, price, null)

        }


        fun setEVPNMenu() = setTracking(LocalyticsTrackers.instance.EVENT_VPNMenu)
        fun setESettings() = setTracking(LocalyticsTrackers.instance.EVENT_Settings)
        fun setEInAppPurchase() = setTracking(LocalyticsTrackers.instance.EVENT_InAppPurchase)
        fun setELogout() = setTracking(LocalyticsTrackers.instance.EVENT_Logout)
        fun setEVpnMain() = setTracking(LocalyticsTrackers.instance.EVENT_VPN_Page)
        fun setERevenue(purchaseName: String,sku:String,type:String,price:Long) =
                setLTV(purchaseName,sku,type,price)
        fun setEVPNConnect() = setTracking(LocalyticsTrackers.instance.EVENT_VPNConnect)
        fun setEVPNDisconnect() = setTracking(LocalyticsTrackers.instance.EVENT_VPNDisconnect)
        fun setEDesktop_Share() = setTracking(LocalyticsTrackers.instance.EVENT_Desktop_Share)
        fun setELocationChange(status:HashMap<String,String>) = setTracking(LocalyticsTrackers.instance.EVENT_LocationChange,status)
        fun setTrafficExhausted(status:HashMap<String,String>) = setTracking(LocalyticsTrackers.instance.EVENT_EXHAUSTED_TRAFFIC,status)
        fun setEUpgrade_from_location_change() = setTracking(LocalyticsTrackers.instance.EVENT_upgrade_from_location_change)
        fun setEUpgrade_Button() = setTracking(LocalyticsTrackers.instance.EVENT_Upgrade_Button)
        fun setEIAP_Success() = setTracking(LocalyticsTrackers.instance.EVENT_IAP_Success)
        fun setEIAP_Leave(promo_id:HashMap<String,String>) = setTracking(LocalyticsTrackers.instance.EVENT_IAP_Leave,promo_id)
        fun setEVPN_Page_First_Time() = setTracking(LocalyticsTrackers.instance.EVENT_VPN_Page_First_Time)
        fun setEBrowser_Page_First_Time() = setTracking(LocalyticsTrackers.instance.EVENT_Browser_Page_First_Time)
        fun setEShare() = setTracking(LocalyticsTrackers.instance.EVENT_Share)
        fun setAuto_Connect_Page() = setTracking(LocalyticsTrackers.instance.EVENT_Auto_Connect_Page)
        fun setAuto_Connect_Change(map: Map<String, String>) = setTracking(LocalyticsTrackers.instance.EVENT_Auto_Connect_Change,map)
        fun setAuto_Protect_Alert(map: Map<String, String>) = setTracking(LocalyticsTrackers.instance.EVENT_Auto_Protect_Alert,map)
        fun setERate() = setTracking(LocalyticsTrackers.instance.EVENT_Rate)
        fun setERegistration() = setTracking(LocalyticsTrackers.instance.EVENT_REGISTRATION)
        fun setELogin() = setTracking(LocalyticsTrackers.instance.EVENT_LOGIN)
        fun setEOnboarding1() = setTracking(LocalyticsTrackers.instance.EVENT_Onboarding_1)
        fun setEOnboarding2() = setTracking(LocalyticsTrackers.instance.EVENT_Onboarding_2)
        fun setEOnboarding3() = setTracking(LocalyticsTrackers.instance.EVENT_Onboarding_3)
        fun setEOnboarding4() = setTracking(LocalyticsTrackers.instance.EVENT_Onboarding_4)
        fun setFirstOpen() = setTracking(LocalyticsTrackers.instance.EVENT_First_Open)
        fun setBrowserPage() = setTracking(LocalyticsTrackers.instance.EVENT_Browser_Page)
        fun setBrowserNewTab() = setTracking(LocalyticsTrackers.instance.EVENT_Browser_New_Tab)
        fun setBookmarks() = setTracking(LocalyticsTrackers.instance.EVENT_Bookmarks)
        fun setBrowserHistory() = setTracking(LocalyticsTrackers.instance.EVENT_Browser_History)
        fun setVpnOffWithAutoProtect() = setTracking(LocalyticsTrackers.instance.EVENT_VPN_Off_with_Auto_Protect)

        fun setEPromoStart(status:HashMap<String,String>) = setTracking(LocalyticsTrackers.instance.EVENT_Promo_Start,status)
        fun setEPromoEnd(status:HashMap<String,String>) = setTracking(LocalyticsTrackers.instance.EVENT_Promo_End,status)
        fun setEPromo_one_day_left(status:HashMap<String,String>) = setTracking(LocalyticsTrackers.instance.EVENT_Promo_one_day_left,status)

        fun setLastDayPromoWarning(values:HashMap<String,String>) = setTracking(LocalyticsTrackers.instance.EVENT_Promo_day_left,values)
        fun setCurrentSubscriptionStatus(values:HashMap<String,String>) = setTracking(LocalyticsTrackers.instance.EVENT_Current_Subscription_Status,values)

//        fun setGET_STARTED() = setTracking(LocalyticsTrackers.instance.GET_STARTED)

        //Free, Paid
        //User Type
        fun setUserType() {

            var type = 0
            if(TrafficController.getInstance(FinjanVPNApplication.getInstance()).licenseType == TrafficController.LICENSE_PAID){
                type =1
            }

            var typeString = ""
            when (type) {
                0 -> {
                    typeString = "Free"
                }
                1 -> {
                    typeString = "Paid"
                }
            }
            setDimension(0,typeString)
        }

        //Monthly IOS, Monthly Android, Annual IOS, Annual Android, Monthly All, Annual All
        //Paid Type
        fun setUserPaidType(type: Int) {
            var typeString = ""
            when (type) {
                0 -> {
//                    typeString = "Monthly IOS"
                    typeString = "Monthly"
                }
                1 -> {
//                    typeString = "Annual IOS"
                    typeString = "Annual"
                }
                2 -> {
//                    typeString = "Monthly Android"
                    typeString = "Monthly"
                }
                3 -> {
                    typeString = "Annual"
//                    typeString = "Annual Android"
                }
                4 -> {
                    typeString = "Monthly"
//                    typeString = "Monthly All"
                }
                5 -> {
                    typeString = "Annual"
//                    typeString = "Annual All"
                }
                6 -> {
//                    Localytics.setCustomDimension(1,null)
                    return
                }
            }
            setDimension(1,typeString)
        }

        //Email, Facebook, Google
        fun setUserRegisteredType() {
            var type = AuthenticationHelper.getInstnace().finjanUser.userRegisteredType
            var typeString = ""
            when (type.toLowerCase()) {
                "email"-> {
                    typeString = "Email"
                }
                "fb","facebook" -> {
                    typeString = "Facebook"
                }
                "google" -> {
                    typeString = "Google"
                }
            }
            if(TextUtils.isEmpty(typeString)){typeString = "Email"}

            //Registered Type
            setDimension(2,typeString)
        }
        //Paid Source
        fun setPaidSource(){
            var source = AuthenticationHelper.getInstnace().finjanUser.source
            if(TextUtils.isEmpty(source)){source = "Android"}
            source = source.capitalize()
            setDimension(5,source)
        }

        //Last Country Connected
        fun setLastCountryConnected(typeString:String){
            setDimension(6,typeString)
        }

        //Devices Registered
        fun setDevicesRegistered(){

            var count = AuthenticationHelper.getInstnace().finjanUser.devices_count
            var countL:Long = 1
            if(!TextUtils.isEmpty(count)){
                countL= count.toLong()
            }
            setProfile("Devices Registered",countL)
        }

        //Registered Platform
        fun setUserRegisteredPlateformType() {
            var source = AuthenticationHelper.getInstnace().finjanUser.source
            if(TextUtils.isEmpty(source)){source = "Android"}
            source = source.capitalize()
            setDimension(3,source)
        }

        //Browser User
        fun setBorrowerUser(){
            var isBorrower:String = "no"
            if(UserPref.getInstance().isBrowser){
                isBorrower = "yes"
            }
            setDimension(4,isBorrower)
        }

        //Auto Connect User
        fun setIsAutoConnectUser(){
            var isAutoConnect = "no"
            if(UserPref.getInstance().autoConnect){
                isAutoConnect = "yes"
            }
            setDimension(8,isAutoConnect)
        }
        //Desktop User
        fun setIsDesktopUser(){
            val desktopUser = AuthenticationHelper.getInstnace().finjanUser.desktop_registered
            var value = "no"
            if(TextUtils.isEmpty(desktopUser)){
                if(desktopUser.toLowerCase().equals("true")){value="yes"}else{value="no"}
            }
            setDimension(9,value)
        }
        //IOS User
        fun setIsIosUser(){
            //Ios User
            val iosUser = AuthenticationHelper.getInstnace().finjanUser.ios_registered
            var value = "no"
            if(TextUtils.isEmpty(iosUser)){
                if(iosUser.toLowerCase().equals("true")){value="yes"}else{value="no"}
            }
            setDimension(10,value)
        }
        //Android User
        fun setIsAndroidUser(){
            //Android User
            val androidUser = AuthenticationHelper.getInstnace().finjanUser.android_registered
            var value = "yes"
            if(!TextUtils.isEmpty(androidUser)){
                if(androidUser.toLowerCase().equals("true")){value="yes"}else{value="no"}
            }
            setDimension(11,value)
        }
        //Cancelled Subscription
        fun setIsCancellSubscription(value:String){
            //Desktop User
            setDimension(12,value)
        }
    }


    fun installEvent(){
        when (UserPref.getInstance().isNewInstall) {
            0 -> {
                setProfile("VitalSecurity User", 0)
            }
            1 -> {
                setProfile("VitalSecurity User", 1)
            }
            2 -> {
            }
        }
    }

    fun addVPNONEvent(){
        if(TrafficController.getInstance(FinjanVPNApplication.getInstance()).isRegistered){
//            FirebaseMessaging.getInstance().unsubscribeFromTopic("VPN_OFF")
            FirebaseMessaging.getInstance().subscribeToTopic("VPN_ON")
            updateTrafficProfile()
        }
    }

    fun updateTrafficProfile(){
        if(TrafficController.getInstance(FinjanVPNApplication.getInstance()).isRegistered){
            setProfile("Traffic Usage",getConsumedTraffic())
        }
    }

    public fun getTotalTraffic():Long{
        val traffic =Util.humanReadableByteCountLong(TrafficController.getInstance(FinjanVPNApplication.getInstance()).totalTraffic)
        if(traffic>1000){
            return 1000
        }
        return traffic
    }

    public fun getConsumedTraffic():Long{
        val traffic =Util.humanReadableByteCountLong(TrafficController.getInstance(FinjanVPNApplication.getInstance()).consumedTraffic)
        if(traffic>1000){
            return 1000
        }
        return traffic
    }
    fun addToBackgroundChannels(){
        FirebaseMessaging.getInstance().subscribeToTopic("Background")
    }
    fun removeFromBackground(){
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Background")
    }

    fun addVPNOFFEvent(){
        FirebaseMessaging.getInstance().unsubscribeFromTopic("VPN_ON")
//        FirebaseMessaging.getInstance().subscribeToTopic("VPN_OFF")
        updateTrafficProfile()
    }
    fun subscribePaidUser(context:Context){
        if(TrafficController.getInstance(context).isPaid){
            FirebaseMessaging.getInstance().unsubscribeFromTopic("FreeUsers")
            FirebaseMessaging.getInstance().unsubscribeFromTopic("Free")
        }else{
            FirebaseMessaging.getInstance().subscribeToTopic("FreeUsers")
            FirebaseMessaging.getInstance().subscribeToTopic("Free")
        }
    }
//    fun  addAppsUpgradedApp(){
//        if (!PlistHelper.getInstance().isLocalyticsDisabled) {
//            Localytics.tagEvent("VSAppUpgraded")
//        }
//    }
}
