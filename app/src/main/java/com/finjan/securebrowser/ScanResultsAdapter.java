package com.finjan.securebrowser;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public abstract class ScanResultsAdapter extends RecyclerView.Adapter<ScanResultsAdapter.MyViewHolder> {

    ArrayList<String> keyArrayList = new ArrayList<>();
    ArrayList<String> valueArrayList = new ArrayList<>();
    private Context _context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_site, tv_result;

        public MyViewHolder(View view) {
            super(view);
            tv_site = (TextView) view.findViewById(R.id.tv_site);
            tv_result = (TextView) view.findViewById(R.id.tv_result);
        }
    }
    public ScanResultsAdapter(Context context , ArrayList<String> keyArrayList,ArrayList<String> valueArrayList) {
        this.keyArrayList = keyArrayList;
        this.valueArrayList = valueArrayList;
        this._context=context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if(valueArrayList.get(position).replace("\"","").trim().contains("mal") || valueArrayList.get(position).replace("\"","").trim().contains("phi")){
            holder.tv_result.setTextColor(_context.getResources().getColor(R.color.red_2));
        }
        else  if(valueArrayList.get(position).replace("\"","").trim().contains("sus") ){
            holder.tv_result.setTextColor(_context.getResources().getColor(R.color.yellow_2));
        }
        else  {
            holder.tv_result.setTextColor(_context.getResources().getColor(R.color.green_colour_1));
        }


        holder.tv_site.setText(keyArrayList.get(position).substring(0,1).toUpperCase() + keyArrayList.get(position).substring(1));
        holder.tv_result.setText(valueArrayList.get(position).replace("\"","").substring(0,1).toUpperCase() + valueArrayList.get(position).replace("\"","").substring(1));

    }

    @Override
    public int getItemCount() {
        return keyArrayList.size();
    }

}
