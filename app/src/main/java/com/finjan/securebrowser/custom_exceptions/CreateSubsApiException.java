package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log subs api fails on Fabric
 *
 */

public class CreateSubsApiException{

    public CreateSubsApiException(String url, String errorJson, String request){
        CreateSubsApiException1.logException(new CreateSubsApiException1(url, errorJson, request));
    }
    private class CreateSubsApiException1 extends CustomApiException {
        public CreateSubsApiException1(String msg) {
            super(msg);
        }

        public CreateSubsApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private CreateSubsApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }

}


