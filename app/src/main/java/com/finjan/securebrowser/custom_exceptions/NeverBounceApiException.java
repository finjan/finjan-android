package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;
/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class NeverBounceApiException{

    public NeverBounceApiException(String url, String errorJson, String request){
        NeverBounceApiException1.logException(new NeverBounceApiException1(url, errorJson, request));
    }

    private class NeverBounceApiException1 extends CustomApiException {
        public NeverBounceApiException1(String msg) {
            super(msg);
        }

        public NeverBounceApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private NeverBounceApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }

}

