package com.finjan.securebrowser.custom_exceptions;

import android.text.TextUtils;

import org.json.JSONObject;

//import io.fabric.sdk.android.Fabric;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to handle various log on fabric depending upon endpoint
 *
 */

public class CustomExceptionConfig {

//    private static final String BACKEND_URL="https://ulm.finjanmobile.com/wp-json/api/v1/";

    private static final class CustomExceptionConfigHolder{
        private static final CustomExceptionConfig instance=new CustomExceptionConfig();
    }
    public static CustomExceptionConfig getInstance(){return CustomExceptionConfigHolder.instance;}

    public void logException(String url,String request,String response,String className){
//        if(Fabric.isInitialized()){
            postException(className,url,response,request);
//        }
    }
    public void logException(String url, String errorJson, JSONObject request, String className){
//        if(Fabric.isInitialized()){
            postException(className,url,errorJson,(request!=null?request.toString():""));
//        }
    }

    private void postException(String className, String url, String response, String request){
        switch (className){
            case "CreateDeviceApiException":
                new CreateDeviceApiException(url,response,request);
                break;
            case "RenewSubsApiException":
                new RenewSubsApiException(url,response,request);
                break;
            case "CreateSubsApiException":
                new CreateSubsApiException(url,response,request);
                break;
            case "GetDevicesListApiException":
                new GetDevicesListApiException(url,response,request);
                break;
            case "LicenceFetchApiException":
                new LicenceFetchApiException(url,response,request);
                break;
            case "RegionFectchApiException":
                new RegionFectchApiException(url,response,request);
                break;
            case "SubscriptionFetchApiException":
                new SubscriptionFetchApiException(url,response,request);
                break;
            case "TokenRefreshApiException":
                new TokenRefreshApiException(url,response,request);
                break;
            case "TrafficConsumptionApiException":
                new TrafficConsumptionApiException(url,response,request);
                break;
            case "UpdateDeviceApiException":
                new UpdateDeviceApiException(url,response,request);
                break;
            case "UpdateSubcApiException":
                new UpdateSubcApiException(url,response,request);
                break;
            case "UserFetchApiException":
                new UserFetchApiException(url,response,request);
                break;
            case "UserLoginApiException":
                new UserLoginApiException(url,response,request);
                break;
            case "UserRegisterApiException":
                new UserRegisterApiException(url,response,request);
                break;
            default: CustomApiException.logException(new CustomApiException(url,response,request));
        }
    }
}
