package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */
public class LaunchAppApiException {
    public LaunchAppApiException(String url, String errorJson, String request){
        LaunchAppApiException1.logException(new LaunchAppApiException1(url, errorJson, request));
    }
    private class LaunchAppApiException1 extends CustomApiException{
        public LaunchAppApiException1(String msg) {
            super(msg);
        }

        public LaunchAppApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private LaunchAppApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}
