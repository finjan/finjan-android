package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class UserLoginApiException{
    public UserLoginApiException(String url, String errorJson, String request){
        UserLoginApiException1.logException(new UserLoginApiException1(url, errorJson, request));
    }
    private class UserLoginApiException1 extends CustomApiException {
        public UserLoginApiException1(String msg) {
            super(msg);
        }

        public UserLoginApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private UserLoginApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}

