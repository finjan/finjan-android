package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class UpdateSubcApiException{
    public UpdateSubcApiException(String url, String errorJson, String request){
     UpdateSubcApiException1.logException(new UpdateSubcApiException1(url, errorJson, request));
    }

    private class UpdateSubcApiException1 extends CustomApiException {
        public UpdateSubcApiException1(String msg) {
            super(msg);
        }

        public UpdateSubcApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private UpdateSubcApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}

