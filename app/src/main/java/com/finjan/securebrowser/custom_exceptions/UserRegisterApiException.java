package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */


public class UserRegisterApiException{
    public UserRegisterApiException(String url, String errorJson, String request){
        UserRegisterApiException1.logException(new UserRegisterApiException1(url, errorJson, request));
    }
    private class UserRegisterApiException1 extends CustomApiException {
        public UserRegisterApiException1(String msg) {
            super(msg);
        }

        public UserRegisterApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private UserRegisterApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}

