package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class GetDevicesListApiException{

    public GetDevicesListApiException(String url, String errorJson, String request){
        GetDevicesListApiException1.logException(new GetDevicesListApiException1(url, errorJson, request));
    }
    private class GetDevicesListApiException1 extends CustomApiException {
        public GetDevicesListApiException1(String msg) {
            super(msg);
        }

        public GetDevicesListApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private GetDevicesListApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}


