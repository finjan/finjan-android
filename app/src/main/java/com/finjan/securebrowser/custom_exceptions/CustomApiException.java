package com.finjan.securebrowser.custom_exceptions;

import com.avira.common.backend.Backend;
import com.avira.common.id.HardwareId;
//import com.crashlytics.android.Crashlytics;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import org.json.JSONObject;

//import io.fabric.sdk.android.Fabric;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log all api fails on Fabric
 *
 */

public class CustomApiException extends Exception {
    private static final String FINJAN_USER_NAME = "FINJAN_USER_NAME";
    private static final String FINJAN_USER_EMAIL = "FINJAN_USER_EMAIL";
    private static final String DEVICE_ID = "DEVICE_ID";
    private static final String TIME = "TIME";
    private static final String USER_LOGGED_IN = "USER_LOGGED_IN";
    private static final String URL = "URL";
    private static final String EVENTS = "EVENTS";
    private static final String PURCHASETOKEN = "PurchaseToken";
    private static final String RESPONSE = "RESPONSE";
    private static final String REQUEST = "REQUEST ";
    private static final String APP_VERSION = "APP VERSION";


    public CustomApiException(String msg) {
        super(msg);
    }

    public CustomApiException(String url, String errorJson, JSONObject request) {
        super("URL " + url + "\n" + " Response " + errorJson + " \n" + "" + (request != null ? request.toString() : ""));
        addMoreData(url, errorJson, request != null ? request.toString() : "");
    }

    public CustomApiException(String url, String errorJson, String request) {
        super("URL " + url + "\n" + " Response " + errorJson + " \n" + "" + request);
        addMoreData(url, errorJson, request);
    }

    public CustomApiException(boolean renewal, String sku, String errorJson, String request, boolean fromScheduler) {

        super(renewal ? "New Renewal for " + sku + " " + (fromScheduler ? " From scheduler" : "")
                : "New Purchase for " + sku + " " + (fromScheduler ? " From scheduler" : ""));
        addMoreData(/*"https://ulm.finjanmobile.com/wp-json/api/v1/"*/AppConfig.Companion.isStagingUrl()?Backend.stagingurl2:Backend.liveUrl +"subscription", errorJson, request);

        FirebaseCrashlytics.getInstance().setCustomKey(EVENTS, EventsListHelper.Companion.getInstance().getJson().toString());
        FirebaseCrashlytics.getInstance().setCustomKey(PURCHASETOKEN, EventsListHelper.Companion.getInstance().getToken());

    }

    public CustomApiException(boolean renewal, String sku) {
        super(renewal ? "New Renewal for " + sku : "New Purchase for " + sku);
        addMoreData();
//        if (Fabric.isInitialized()) {
        FirebaseCrashlytics.getInstance().setCustomKey(EVENTS, EventsListHelper.Companion.getInstance().getJson().toString());
        FirebaseCrashlytics.getInstance().setCustomKey(PURCHASETOKEN, EventsListHelper.Companion.getInstance().getToken());
//        }
    }

    public static void logException(Throwable t) {
        exceptionStarts(t);
    }

    private static void exceptionStarts(Throwable t) {
//        if (Fabric.isInitialized()) {
            FirebaseCrashlytics.getInstance().recordException(t);
//        }
    }

    private void addMoreData() {
//        if (Fabric.isInitialized()) {
            if (TrafficController.getInstance(FinjanVPNApplication.getInstance()).isRegistered()) {
                FirebaseCrashlytics.getInstance().setCustomKey(FINJAN_USER_NAME, AuthenticationHelper.getInstnace().getUserName(FinjanVPNApplication.getInstance()));
                FirebaseCrashlytics.getInstance().setCustomKey(FINJAN_USER_EMAIL, AuthenticationHelper.getInstnace().getUserEmail(FinjanVPNApplication.getInstance()));
                FirebaseCrashlytics.getInstance().setCustomKey(USER_LOGGED_IN, true);
            } else {
                FirebaseCrashlytics.getInstance().setCustomKey(USER_LOGGED_IN, false);
            }
        FirebaseCrashlytics.getInstance().setCustomKey(DEVICE_ID, HardwareId.get(FinjanVPNApplication.getInstance()));
        FirebaseCrashlytics.getInstance().setCustomKey(TIME, DateHelper.getInstnace().getCurrentGMTTime());
        FirebaseCrashlytics.getInstance().setCustomKey(APP_VERSION, com.finjan.securebrowser.BuildConfig.VERSION_NAME);
//        }
    }

    private void addMoreData(String url, String response, String request) {
//        if (Fabric.isInitialized()) {
            addMoreData();
        FirebaseCrashlytics.getInstance().setCustomKey(URL, url);
        FirebaseCrashlytics.getInstance().setCustomKey(RESPONSE, response);
        FirebaseCrashlytics.getInstance().setCustomKey(REQUEST, request);
//        }

    }

}
