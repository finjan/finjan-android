package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class SubscriptionFetchApiException{

    public SubscriptionFetchApiException(String url, String errorJson, String request){
        SubscriptionFetchApiException1.logException(new SubscriptionFetchApiException1(url, errorJson, request));
    }

    private class SubscriptionFetchApiException1 extends CustomApiException {
        public SubscriptionFetchApiException1(String msg) {
            super(msg);
        }

        public SubscriptionFetchApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private SubscriptionFetchApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }


}
