package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */
public class UserFetchApiException{
    public UserFetchApiException(String url, String errorJson, String request){
        UserFetchApiException1.logException(new UserFetchApiException1(url, errorJson, request));
    }
    private class UserFetchApiException1 extends CustomApiException {
        public UserFetchApiException1(String msg) {
            super(msg);
        }

        public UserFetchApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private UserFetchApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}

