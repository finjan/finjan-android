package com.finjan.securebrowser.custom_exceptions;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class ULMSubscriptionCreatedNonFetal {



    public ULMSubscriptionCreatedNonFetal(boolean isRenewal, String sku, String errorJson,String request,boolean fromScheduler){
        ULMSubscriptionCreatedNonFetal1.logException(new ULMSubscriptionCreatedNonFetal1(isRenewal,sku,errorJson,request,fromScheduler));
    }
    private class ULMSubscriptionCreatedNonFetal1 extends CustomApiException {
        public ULMSubscriptionCreatedNonFetal1(String msg) {
            super(msg);
        }

        private ULMSubscriptionCreatedNonFetal1(boolean isRenewal, String sku, String errorJson,String request,boolean fromScheduler) {
            super(isRenewal,sku,errorJson,request,fromScheduler);
        }
    }
}
