package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */
public class RenewSubsApiException{

    public RenewSubsApiException(String url, String errorJson, String request){
        RenewSubsApiException1.logException(new RenewSubsApiException1(url, errorJson, request));
    }
    private class RenewSubsApiException1 extends CustomApiException {


        public RenewSubsApiException1(String msg) {
            super(msg);
        }

        public RenewSubsApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private RenewSubsApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }

}

