package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class TokenRefreshApiException{

    public TokenRefreshApiException(String url, String errorJson, String request){
        TokenRefreshApiException1.logException(new TokenRefreshApiException1(url, errorJson, request ));
    }

    private class TokenRefreshApiException1 extends CustomApiException {
        public TokenRefreshApiException1(String msg) {
            super(msg);
        }

        public TokenRefreshApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private TokenRefreshApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}

