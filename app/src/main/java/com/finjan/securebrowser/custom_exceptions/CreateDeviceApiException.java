package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Web view helper class that help VideoEnabledWeb to play videos on web view
 *
 */

public class CreateDeviceApiException{
    public CreateDeviceApiException(String url, String errorJson, String request){
        CreateDeviceApiException1.logException(new CreateDeviceApiException1(url, errorJson, request));
    }
    private class CreateDeviceApiException1 extends CustomApiException{
        public CreateDeviceApiException1(String msg) {
            super(msg);
        }

        public CreateDeviceApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private CreateDeviceApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}
