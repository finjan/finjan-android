package com.finjan.securebrowser.custom_exceptions;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class StoreSubscriptionCreatedNonFetal {



    public StoreSubscriptionCreatedNonFetal(boolean isRenewal, String sku){
        StoreSubscriptionCreatedNonFetal1.logException(new StoreSubscriptionCreatedNonFetal1(isRenewal,sku));
    }
    private class StoreSubscriptionCreatedNonFetal1 extends CustomApiException {
        public StoreSubscriptionCreatedNonFetal1(String msg) {
            super(msg);
        }

        private StoreSubscriptionCreatedNonFetal1(boolean isRenewal, String sku) {
            super(isRenewal,sku);
        }
    }
}
