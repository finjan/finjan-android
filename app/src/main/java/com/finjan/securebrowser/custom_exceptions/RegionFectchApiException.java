package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;
/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class RegionFectchApiException{
    public RegionFectchApiException(String url, String errorJson, String request){
        RegionFectchApiException1.logException(new RegionFectchApiException1(url, errorJson, request));
    }

    private class RegionFectchApiException1 extends CustomApiException {
        public RegionFectchApiException1(String msg) {
            super(msg);
        }

        public RegionFectchApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private RegionFectchApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}

