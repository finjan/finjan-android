package com.finjan.securebrowser.custom_exceptions

import org.json.JSONObject

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log all events on Fabric
 *
 */

class EventsListHelper{

    private object Holder{var instance=EventsListHelper()}
    companion object {
        val instance:EventsListHelper by lazy { Holder.instance }
        private var eventsMap=HashMap<String,String>()
    }
    fun add(className:String,key:String,value:String){
        eventsMap.put("class "+className+" method "+key, value)
    }
    fun set(){
        eventsMap=HashMap<String,String>()
    }

    fun getJson():JSONObject{
        val json=JSONObject()
        eventsMap.entries.forEach { entry: MutableMap.MutableEntry<String, String> -> json.put(entry.key,entry.value) }
        return json
    }
    fun getToken():String?{
      if(eventsMap.containsKey("PurchaseToken")){
          return eventsMap.get("PurchaseToken");
      }  else{
          return  "empty token"
      }
    }
}