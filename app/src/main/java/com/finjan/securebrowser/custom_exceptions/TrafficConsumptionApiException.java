package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;
/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class TrafficConsumptionApiException{
    public TrafficConsumptionApiException(String url, String errorJson, String request){
        TrafficConsumptionApiException1.logException(new TrafficConsumptionApiException1(url, errorJson, request));
    }


    private class TrafficConsumptionApiException1 extends CustomApiException {
        public TrafficConsumptionApiException1(String msg) {
            super(msg);
        }

        public TrafficConsumptionApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private TrafficConsumptionApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}

