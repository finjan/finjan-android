package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;
/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class LicenceFetchApiException{

    public LicenceFetchApiException(String url, String errorJson, String request){
        LicenceFetchApiException1.logException(new LicenceFetchApiException1(url, errorJson, request));
    }

    private class LicenceFetchApiException1 extends CustomApiException {
        public LicenceFetchApiException1(String msg) {
            super(msg);
        }

        public LicenceFetchApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private LicenceFetchApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }

}

