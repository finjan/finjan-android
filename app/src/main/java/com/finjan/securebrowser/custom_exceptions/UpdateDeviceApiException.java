package com.finjan.securebrowser.custom_exceptions;

import org.json.JSONObject;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to log api fails on Fabric
 *
 */

public class UpdateDeviceApiException{
    public UpdateDeviceApiException(String url, String errorJson, String request){
        UpdateDeviceApiException1.logException(new UpdateDeviceApiException1(url, errorJson, request));
    }

    private class UpdateDeviceApiException1 extends CustomApiException {
        public UpdateDeviceApiException1(String msg) {
            super(msg);
        }

        public UpdateDeviceApiException1(String url, String errorJson, JSONObject request) {
            super(url, errorJson, request);
        }

        private UpdateDeviceApiException1(String url, String errorJson, String request) {
            super(url, errorJson, request);
        }
    }
}

