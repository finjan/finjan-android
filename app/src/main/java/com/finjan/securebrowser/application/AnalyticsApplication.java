/*
 * Copyright Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.finjan.securebrowser.application;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import android.os.StrictMode;
import android.text.TextUtils;

import com.finjan.securebrowser.BuildConfig;
import com.finjan.securebrowser.Database.DataBaseOpenHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import webHistoryDatabase.DaoMaster;
import webHistoryDatabase.DaoSession;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * This is a subclass of {@link Application} used to provide shared objects for this app, such as
 * the {@link Tracker}.
 */
public class AnalyticsApplication extends MultiDexApplication {
    public static final boolean showLog = false;
    public static final boolean showTracker = true;
    public static final boolean isAppPurchase = true;
    public static DaoSession daoSession = null;
    private static AnalyticsApplication myApplication;
    private Tracker mTracker;

    public static AnalyticsApplication getMyApplication() {
        return myApplication;
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if(TextUtils.isEmpty(PlistHelper.getInstance().getGoogleEnalyticKey())){
            return mTracker;
        }
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(PlistHelper.getInstance().getGoogleEnalyticKey());
        }
        return mTracker;
    }

    public void setTrcakerName(String trcakerName) {

        if (mTracker == null) {
//            String analyticsID = Utils.getPlistvalue(Constants.KEY_GOOGLE_ANALYTICS, Constants.KEY_ANDROID);
//            mTracker = application.getDefaultTracker((analyticsID.isEmpty()) ? "" : analyticsID);
            mTracker = getDefaultTracker();
        }
        if(mTracker!=null){
            mTracker.setScreenName(trcakerName);
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }

    }
    public void setEventTrack(String category, String eventName){
        if (mTracker == null) {
//            String analyticsID = Utils.getPlistvalue(Constants.KEY_GOOGLE_ANALYTICS, Constants.KEY_ANDROID);
//            mTracker = application.getDefaultTracker((analyticsID.isEmpty()) ? "" : analyticsID);
            mTracker = getDefaultTracker();
        }
        if(mTracker!=null){
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("")
                    .setAction(eventName)
                    .setLabel(eventName)
                    .build());
        }

    }


    @Override
    public void onCreate() {
        super.onCreate();
        myApplication = AnalyticsApplication.this;
        Logger.getInstance().start();
        enableStrictModeForDebug();
        if (daoSession == null) {
//            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "webHistoryDatabase-db", null);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    DataBaseOpenHelper helper = new DataBaseOpenHelper(AnalyticsApplication.this, "webHistoryDatabase-db", null);
                    SQLiteDatabase db = helper.getWritableDatabase();
                    DaoMaster daoMaster = new DaoMaster(db);
                    daoSession = daoMaster.newSession();
                }
            }).start();

//            if (DataBaseOpenHelper.db!=null){
//                MigrateHelper.getInstance().migrateData(DataBaseOpenHelper.db, 2);
//            }
        }
    }

    private void enableStrictModeForDebug() {

//        if (BuildConfig.DEBUG) {
//            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                    .detectAll()
//                    .penaltyLog()
//                    .build());
//
//            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                    .detectAll()
//                    .penaltyLog()
//                    .build());
//        }
    }
}
