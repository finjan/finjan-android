package com.finjan.securebrowser.fragment;

import android.graphics.Typeface;
import android.net.http.SslError;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * fragment to warning screen in case if safe result retrun some kind of alert tabs from browser.
 */


public class FragmentWarning extends ParentFragment implements View.OnClickListener {

    public static final String WARNING_FROM_NAVIGATION = "warning_from_navigation";
    public static final String WARNING_TYPE_SAFETY_RESULT = "warning_type_safety_result";
    public static final String WARNING_TYPE_SSL = "warning_type_ssl";
    public static final String WARNING_TYPE = "warning_type";

    private TextView tvUrl;
    private boolean isNavigationPressed;
    private String url, warning_type;
    //    protected void setNavigationPressed(boolean isNavigationPressed){
//        this.isNavigationPressed=isNavigationPressed;
//    }
    private TextView tvWarningDesc, tvContinue, tvLearnMore;
    private SslError sslError;
    private WarningCallback warningCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View parentView = inflater.inflate(R.layout.warning_view, container, false);
        setReferences(parentView);
        return parentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        GoogleTrackers.Companion.setSDangerousURL();
//        LocalyticsTrackers.Companion.setSDangerousURL();
        changeStatusBarColor(R.color.red_1);
    }

    private void setDefaultFromBundle() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.BKEY_BUNDLE)) {
            Bundle bundle = getArguments().getBundle(AppConstants.BKEY_BUNDLE);
            if (bundle != null) {
                this.url = bundle.getString(AppConstants.BKEY_URL);
                this.isNavigationPressed = bundle.getBoolean(WARNING_FROM_NAVIGATION);
                this.warning_type = bundle.getString(WARNING_TYPE);
            }
        }
    }

    private void setTitle() {
        if (!TextUtils.isEmpty(url)) {
            tvUrl.setText(NativeHelper.getInstnace().getUrlDomain(url));
            return;
        }
        tvUrl.setText("");
    }

    private void setLayoutForWarning() {
        if (TextUtils.isEmpty(warning_type)) {
            return;
        }
        switch (warning_type) {
            case WARNING_TYPE_SAFETY_RESULT:
                break;
            case WARNING_TYPE_SSL:
                setViewForSsl();
                break;
        }
    }

    private void setWarningMesssage() {
        if (TextUtils.isEmpty(warning_type)) {
            return;
        }
        switch (warning_type) {
            case WARNING_TYPE_SAFETY_RESULT:
                tvWarningDesc.setText(getString(R.string.warning_verbose));
                break;
            case WARNING_TYPE_SSL:
                tvWarningDesc.setText(getSslMsg());
                break;
        }
    }

    private String getSslMsg() {

        String message = "SSL Certificate error. " + getString(R.string.warning_verbose);
        if (sslError != null) {
            switch (sslError.getPrimaryError()) {
                case SslError.SSL_UNTRUSTED:
                    message = getString(R.string.warning_ssl_untrusted);
                    break;
                case SslError.SSL_EXPIRED:
                    message = getString(R.string.warning_ssl_expired);
                    break;
                case SslError.SSL_IDMISMATCH:
                    message = getString(R.string.warning_ssl_id_miss_matched);
                    break;
                case SslError.SSL_NOTYETVALID:
                    message = getString(R.string.warning_ssl_not_yet_valid);
                    break;
            }
        }

        return message;
    }

    protected void setSslError(SslError sslError) {
        this.sslError = sslError;
    }

    private void setViewForSsl() {
        tvLearnMore.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setDefaultFromBundle();
        setTitle();
        setLayoutForWarning();
        setWarningMesssage();
    }

    private void setReferences(View parentView) {
        tvUrl = ((TextView) parentView.findViewById(R.id.tv_url));
        tvWarningDesc = (TextView) parentView.findViewById(R.id.tv_warning_desc);
        tvLearnMore = (TextView) parentView.findViewById(R.id.learnMoreButton);
        tvLearnMore.setTypeface(null, Typeface.BOLD);
        tvContinue = (TextView) parentView.findViewById(R.id.tv_continue);

        tvLearnMore.setOnClickListener(this);
        tvContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.learnMoreButton:
                onClickMore();
                break;
            case R.id.tv_continue:
                showContinueAlertDialog();
                break;
        }
    }

    private void onClickMore() {
        GlobalVariables.getInstnace().currentHomeFragment.toggleReport();
        //push report fragment to show more about webpage on report page
    }

    private void onClickGoHomeButton() {
        // We could be here from searching a URL on the main screen, or from following a
        // link within a page, or from clicking on a search result
        stopWarningMusic();
        changeFragment(GlobalVariables.getInstnace().currentHomeFragment, false, FragmentHome.TAG, FragmentHome.TAG);
        if (warningCallback != null) {
            warningCallback.continuePage(false);
        }
    }

    private void onClickWarningVerboseHidden() {

        changeFragment(GlobalVariables.getInstnace().currentHomeFragment, false, FragmentHome.TAG, FragmentHome.TAG);
        if (warningCallback != null) {
            warningCallback.continuePage(true);
        }

    }

    private void showContinueAlertDialog() {
        new FinjanDialogBuilder(getActivity()).setMessage(getString(R.string.sure_to_continue))
                .setPositiveButton(getString(R.string.alert_yes)).setNegativeButton(getString(R.string.alert_no))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        continueWithWarning();
                    }
                }).show();
    }

    @Override
    public void onClickParentBack() {
        super.onClickParentBack();
        onClickGoHomeButton();
    }

    private void continueWithWarning() {
        onClickWarningVerboseHidden();
    }

    public void setWarningCallback(WarningCallback warningCallback) {
        this.warningCallback = warningCallback;
    }


    public interface WarningCallback {
        void continuePage(boolean continuepage);
    }

}
