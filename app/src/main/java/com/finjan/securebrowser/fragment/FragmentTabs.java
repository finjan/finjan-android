package com.finjan.securebrowser.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.finjan.securebrowser.CustomPagerAdapter;
import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.TabHistoryAdapter;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.DbHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import webHistoryDatabase.tabhistory;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * fragment to show tabs from browser.
 */


public class FragmentTabs extends ParentFragment implements View.OnClickListener {

    private static final String PRIVATE = "PRIVATE";
    private View parentView;
    private RecyclerView rv_tabs,rv_private;
    private List<tabhistory> tabhistoryList = new ArrayList<>();

    //new ui
    private TabLayout tabs;
    private ViewPager pager;
    private ImageView moreOption,clearHistory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.tab_history_n, container, false);
        setReferences(parentView);
        return parentView;
    }

    private void setReferences(View parentView) {
//        parentView = (ViewGroup) parentView.findViewById(R.id.vg_tabhistory);
        pager = (ViewPager) parentView.findViewById(R.id.viewpager);
        tabs = (TabLayout) parentView.findViewById(R.id.tabs);

        rv_tabs = (RecyclerView) parentView.findViewById(R.id.rv_tabs);
        rv_private= (RecyclerView) parentView.findViewById(R.id.rv_private);

        clearHistory=(ImageView)parentView.findViewById(R.id.clearHistory);

        moreOption=(ImageView)parentView.findViewById(R.id.iv_more_option);
//        moreOption.setColorFilter(ResourceHelper.getInstance().getColor(R.color.col_desc_w));
        moreOption.setVisibility(View.VISIBLE);

        clearHistory.setOnClickListener(this);
        moreOption.setOnClickListener(this);
//        parentView.findViewById(R.id.tv_tabu_private).setOnClickListener(this);
//        ((TextView) parentView.findViewById(R.id.tv_tabu)).setOnClickListener(this);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setPager(pager);
        tabHistoryDataSet();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_tabu:
                onClickNewTab();
                break;
            case R.id.tv_tabu_private:
                onClickTabPrivate();
                break;
            case R.id.iv_more_option:
                onClickMoreOption();
                LocalyticsTrackers.Companion.setBrowserNewTab();
                break;
            case R.id.clearHistory:
                onClickClearHistory();
                break;
        }
    }
    private void onClickClearHistory(){
        if(isPrivate){
            showPrivateDialog();
            return;
        }
        showNonPrivateDialog();
    }
    private void onClickMoreOption(){
        if(isPrivate){
            onClickTabPrivate();
            return;
        }
        onClickNewTab();
    }
    private boolean isPrivate;

    private void onClickNewTab() {

        popCurrentFragment();
        openUrl(AppConstants.NEW_TAB, AppConstants.EMPTY_URL, false, null);
    }

    private void onClickTabPrivate() {

        popCurrentFragment();
        openUrl(AppConstants.NEW_PRIVATE_TAB, AppConstants.EMPTY_URL, true, null);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setTheme(){
        changeStatusBarColor(isPrivate?R.color.content_color:R.color.col_status);
        getToolbar().setBackgroundColor(ContextCompat
                .getColor(getContext(),isPrivate?R.color.content_color:R.color.col_desc_w));
        parentView.setBackgroundColor(ResourceHelper.getInstance().getColor(isPrivate,R.color.content_color,R.color.white));
    }
    private void setTrashIcon(List<tabhistory> tabhistoryList){
        if(tabhistoryList!=null && tabhistoryList.size()>0){
            clearHistory.setVisibility(View.VISIBLE);
        }else {
            clearHistory.setVisibility(View.GONE);
        }
    }

    private void tabHistoryDataSet() {
        tabhistoryList.clear();
        tabhistoryList = DbHelper.getInstnace().getTabList();
        Collections.reverse(tabhistoryList);
        if(GlobalVariables.getInstnace().currentHomeFragment!=null &&
                GlobalVariables.getInstnace().currentHomeFragment.isPrivate){
            selectPrivateTab();
        }else {
            selectNonPrivateTab();
        }
    }
    private List<tabhistory> getNormalTabList(List<tabhistory> tabhistoryList){
        List<tabhistory> normalTabList=new ArrayList<>();
        for (tabhistory tabhistory: tabhistoryList){
            if(TextUtils.isEmpty(tabhistory.getType())){
                normalTabList.add(tabhistory);
            }
        }
        return normalTabList;
    }

    private List<tabhistory> getPrivateTabList(List<tabhistory> tabhistoryList){
        List<tabhistory> privateTabList=new ArrayList<>();
        for (tabhistory tabhistory: tabhistoryList){
            if(!TextUtils.isEmpty(tabhistory.getType())){
                privateTabList.add(tabhistory);
            }
        }
        return privateTabList;
    }
    private TabHistoryAdapter nonPrivateAdapter,privateAdapter;
    private List<tabhistory> nonPrivateList,privateList;
    private void setNonPrivateTabs(){
        isPrivate=false;
        nonPrivateList=getNormalTabList(this.tabhistoryList);
        setTheme();
        setTrashIcon(nonPrivateList);
        nonPrivateAdapter = new TabHistoryAdapter(getContext(), nonPrivateList,false) {
            @Override
            public void onClickDel(View view, String idUrl, final int position) {
                if(position>=0 && nonPrivateList!=null && nonPrivateList.size()>0){
                    notifyItemRemoved(position);
                    onUrlDel(idUrl,nonPrivateList.get(position),nonPrivateList);
                }
            }

            @Override
            public void onUrlClicks(View view, int position, String idUrl) {
                if(position>=0){
                    onUrlTabClick(position,nonPrivateList);
                }
            }
        };
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_tabs.setLayoutManager(layoutManager);
        rv_tabs.setAdapter(nonPrivateAdapter);
    }
    private void setPrivateTabs(){
        isPrivate=true;
        privateList=getPrivateTabList(this.tabhistoryList);
        setTheme();
        setTrashIcon(privateList);
        privateAdapter = new TabHistoryAdapter(getContext(), privateList,true) {
            @Override
            public void onClickDel(View view, String idUrl, int position) {
                if(position>=0 && privateList!=null && privateList.size()>0){
                    notifyItemRemoved(position);
                    onUrlDel(idUrl,privateList.get(position),privateList);
                }
            }

            @Override
            public void onUrlClicks(View view, int position, String idUrl) {
                if(position>=0){
                    onUrlTabClick(position,privateList);
                }
            }
        };
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_private.setLayoutManager(layoutManager);
        rv_private.setAdapter(privateAdapter);
    }






    private void onUrlTabClick(int position,List<tabhistory> tabhistoryList){
        if(!GlobalVariables.getInstnace().justBackPressed){
            Long id = tabhistoryList.get(position).getId();
            onClickTab(id, position,tabhistoryList);
        }
    }
    private void onUrlDel(String idUrl,tabhistory tabhistory,List<tabhistory> tabhistoryList){

         /*===Click on webcrCross image===To remove tabs========*/
        if(!GlobalVariables.getInstnace().justBackPressed){
            nonSecureLoaderVisibility(View.VISIBLE);
//            List<tabhistory> tabhistories = DbHelper.getInstnace().getTabList();
//            if (tabhistories != null && tabhistories.size() > 0) {
//                Collections.reverse(tabhistories);
//                UserPref.getInstance().setLastUrl(tabhistories.get(0).getUrl());
//
//            } else {
//                UserPref.getInstance().setLastUrl("");
//            }
            removeTabFromFragmentMap(Long.valueOf(idUrl));
            if (DbHelper.getInstnace().getTabhistoryDao() != null) {
                DbHelper.getInstnace().getTabhistoryDao().deleteByKey(Long.valueOf(idUrl));
            }

            tabhistoryList.remove(tabhistory);
            setTrashIcon(tabhistoryList);
            this.tabhistoryList.remove(tabhistory);

//            tabhistoryList.clear();
//            tabhistoryList = DbHelper.getInstnace().getTabList();
//            tabHistoryDataSet();
            if (this.tabhistoryList == null || this.tabhistoryList.size() == 0) {
                UserPref.getInstance().setLastUrl("");
                UserPref.getInstance().setLastId(null);
                clearAllFragments();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    nonSecureLoaderVisibility(View.GONE);
                }
            },500);
        }
    }
    private void clearAllPrivateTabs(){
        if(privateList.size()>0){
            if(!GlobalVariables.getInstnace().justBackPressed){
                nonSecureLoaderVisibility(View.VISIBLE);
                clearAllPrivateFragments();
                ArrayList<tabhistory> tabhistoriesToRemove=new ArrayList<>();
                if (DbHelper.getInstnace().getTabhistoryDao() != null) {
                    for (tabhistory tabhistory:privateList){
                        tabhistoriesToRemove.add(tabhistory);
                        DbHelper.getInstnace().getTabhistoryDao().deleteByKey(tabhistory.getId());
                    }
                }
                for (tabhistory tabhistory: tabhistoriesToRemove){
                    this.tabhistoryList.remove(tabhistory);
                    privateList.remove(tabhistory);
                    privateAdapter.notifyDataSetChanged();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        nonSecureLoaderVisibility(View.GONE);
                    }
                },500);
            }
        }
        setTrashIcon(privateList);
    }
    private void clearAllNonPrivateTabs(){
        if(nonPrivateList.size()>0){
            if(!GlobalVariables.getInstnace().justBackPressed){
                nonSecureLoaderVisibility(View.VISIBLE);
                clearAllNonPrivateFragment();
                ArrayList<tabhistory> tabhistoriesToRemove=new ArrayList<>();
                if (DbHelper.getInstnace().getTabhistoryDao() != null) {
                    for (tabhistory tabhistory:nonPrivateList){
                        tabhistoriesToRemove.add(tabhistory);
                        DbHelper.getInstnace().getTabhistoryDao().deleteByKey(tabhistory.getId());
                    }
                }
                for (tabhistory tabhistory: tabhistoriesToRemove){
                    this.tabhistoryList.remove(tabhistory);
                    nonPrivateList.remove(tabhistory);
                    nonPrivateAdapter.notifyDataSetChanged();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        nonSecureLoaderVisibility(View.GONE);
                    }
                },500);
            }
        }
        setTrashIcon(privateList);
    }
    private void showPrivateDialog(){

        new FinjanDialogBuilder(getActivity()).setMessage(getString(R.string.delete_all_private_tab_msg))
                .setPositiveButton(getString(R.string.alert_yes)).setNegativeButton(getString(R.string.alert_no))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        clearAllPrivateTabs();
                    }
                }).show();
    }
    private void showNonPrivateDialog(){
        new FinjanDialogBuilder(getActivity()).setMessage(getString(R.string.delete_all_tab_msg))
                .setPositiveButton(getString(R.string.alert_yes))
                .setNegativeButton(getString(R.string.alert_no))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        clearAllNonPrivateTabs();
                    }
                }).show();
    }

    private void onClickTab(Long id, int tabPosition,List<tabhistory> tabhistoryList) {
        if (tabPosition > 0) {
            Long newId = updateList(id);
            if (newId != null && !newId.equals(id)) {
                if (getFragmentMap().containsKey(id)) {
                    updateFragmentId(id, newId);
                }
                id = newId;
            }
        }
        tabhistory tabhistory = tabhistoryList.get(tabPosition);
        String url = tabhistory.getUrl();
        url = TextUtils.isEmpty(url) ? AppConstants.EMPTY_URL : url;
        openTab(id, tabhistory.getPagetitle(), url, !TextUtils.isEmpty(tabhistory.getType()), false, false);
    }

    private int getTabsCount() {
        return AnalyticsApplication.daoSession.getTabhistoryDao().loadAll().size();
    }

    private Long updateList(Long id) {
        if (id != null) {
            tabhistory tabhistory = DbHelper.getInstnace().getTabHistoy(id);
            tabhistory = DbHelper.getInstnace().makeClone(tabhistory);
            if (tabhistory != null) {
                DbHelper.getInstnace().getTabhistoryDao().deleteByKey(id);
                DbHelper.getInstnace().addTabHistory(tabhistory);
                DbHelper.getInstnace().getTabhistoryDao().getKey(tabhistory);
                return DbHelper.getInstnace().getTabhistoryDao().getKey(tabhistory);
            }
        }
        return null;
    }

    private void removeTabFromFragmentMap(Long id) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && id != null) {
            if (getFragmentMap() != null && getFragmentMap().containsKey(id)) {
                Fragment fragment = getFragmentMap().get(id);
                if(fragment!=null){
                    removeFragment(fragment);
                    getFragmentMap().remove(id);
                }
            }
        }
    }

    public void onClickBack() {

        if (tabhistoryList != null && tabhistoryList.size() > 0) {
            List<tabhistory> tabhistories = DbHelper.getInstnace().getTabList();
            Collections.reverse(tabhistories);

            tabhistory tabhistory = tabhistoryList.get(0);
            String url = tabhistory.getUrl();
            url = TextUtils.isEmpty(url) ? AppConstants.EMPTY_URL : url;
            openTab(tabhistory.getId(), tabhistory.getPagetitle(), url, !TextUtils.isEmpty(tabhistory.getType()), false, false);
        } else {
            if(isPrivate){
                onClickTabPrivate();
            }else {
                onClickNewTab();
            }

        }
    }

    private void setPager(final ViewPager pager) {
        pager.setAdapter(new CustomPagerAdapter(getActivity(),false));
        tabs.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    selectNonPrivateTab();
                } else {
                    selectPrivateTab();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
    private void selectNonPrivateTab(){
        pager.setCurrentItem(0, true);
        // check weather we need to refresh or not as it is costly to use it
        setNonPrivateTabs();
        rv_private.setVisibility(View.GONE);
        rv_tabs.setVisibility(View.VISIBLE);
        rv_tabs.setBackgroundColor(ResourceHelper.getInstance().getColor(R.color.blur_white_2));
    }
    private void selectPrivateTab() {
        pager.setCurrentItem(1, true);
        rv_private.setVisibility(View.VISIBLE);
        rv_tabs.setVisibility(View.GONE);
        rv_private.setBackgroundColor(ResourceHelper.getInstance().getColor(R.color.content_color));
        // check weather we need to refresh or not as it is costly to use it
        setPrivateTabs();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
