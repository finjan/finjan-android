package com.finjan.securebrowser.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.SearchResultPage;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.DbHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class SearchedResultFragment extends ParentFragment implements View.OnClickListener {


    private View parentView;
    private SearchResultPage searchResultPage;
    private ParentHomeFragment fragmentHome;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentView=inflater.inflate(R.layout.search_results_view,container,false);
        setReferences(parentView);
        return parentView;
    }
    private String title;
    private boolean canGoBackward,canGoForward;
    protected void  setFragmentData(ParentHomeFragment fragmentHome,String title,boolean canGoBackward,boolean canGoForward){
        this.fragmentHome=fragmentHome;
        this.title=title;
        this.canGoBackward=canGoBackward;
        this.canGoForward=canGoForward;
    }

    private RelativeLayout header;
    private LinearLayout navigationControls;
    private ImageView iv_tabsOpened,searchIcon,backButton,forwardButton;
    private EditText editText;
    private void setReferences(View parentView) {

        searchResultPage = (SearchResultPage) parentView.findViewById(R.id.searchResultsViewContainer);
        header=(RelativeLayout)parentView.findViewById(R.id.header);
        navigationControls=(LinearLayout)parentView.findViewById(R.id.webViewControls);
        iv_tabsOpened=(ImageView)parentView.findViewById(R.id.iv_tabsopened);
        searchIcon=(ImageView)parentView.findViewById(R.id.searchIcon);
        backButton=(ImageView)parentView.findViewById(R.id.backButton);
        forwardButton=(ImageView)parentView.findViewById(R.id.forwardButton);



        editText=(EditText)parentView.findViewById(R.id.urlEditText);
        editText.setText(title);
        editText.setTextColor(Color.WHITE);
        editText.setFocusable(false);
        editText.setClickable(true);
        (parentView.findViewById(R.id.urlEditText)).setOnClickListener(this);
        (parentView.findViewById(R.id.iv_tabsopened)).setOnClickListener(this);
        (parentView.findViewById(R.id.iv_settings)).setOnClickListener(this);
        searchIcon.setOnClickListener(this);


        setSearchResultPage(searchResultPage);
        searchResultPage.setDelegate(fragmentHome);
        searchResultPage.resetView();
        searchResultPage.updateSearchResults();
    }
    private void setCount(){
        if(parentView!=null){

            int count = DbHelper.getInstnace().getTabCount();
            ((TextView)parentView.findViewById(R.id.txt_tabcount)).setText(count == 0 ? "0" : "" + count);
        }
    }

    private void setUrlHeaderColor() {
        if(fragmentHome!=null){
            setTheme(fragmentHome.isPrivate);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        setUrlHeaderColor();
        setCount();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(safetyResultReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(updateSafetyRatingAppPurchaseFalse);
    }
    protected void safetyResultReceiver(int index){
        if(searchResultPage!=null){
            searchResultPage.updateSafetyRating(index);
        }
    }
    protected void updateSafetyRatingAppPurchaseFalse(int index){
        if(searchResultPage!=null){
            searchResultPage.updateSafetyRatingAppPurchaseFalse(index);
        }
    }

    private void setTheme(boolean isPrivate) {

        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            int color = isPrivate ? R.color.content_color : R.color.accent_color_1;
            color = ResourceHelper.getInstance().getColor(color);

            if (isPrivate) {
                header.setBackgroundColor(color);
                changeStatusBarColor(R.color.content_color);
                editText.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_edittext_private));
            } else {
                header.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.top_title_background_bg));
                changeStatusBarColor(R.color.col_status);
                editText.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_edittext_general));
            }
//            btn_cancel.setTextColor(getResources().getColor(R.color.white));
            navigationControls.setBackgroundColor(color);
            iv_tabsOpened.setBackgroundColor(color);

//            btn_cancel.setBackgroundColor(color);
//            iv_tabsopened.setBackgroundColor(color);
            searchIcon.setBackgroundColor(color);
//            headerCover.setBackgroundColor(color);
//            tv_url.setBackgroundColor(color);
            backButton.setOnClickListener(this);
            forwardButton.setOnClickListener(this);
            if (canGoBackward) {
                backButton.setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_0));
                backButton.setClickable(true);
            } else {
                backButton.setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_2));
                backButton.setClickable(false);
            }
            if (canGoForward) {
                forwardButton.setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_0));
                forwardButton.setClickable(true);
            } else {
                forwardButton.setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_2));
                forwardButton.setClickable(false);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                iv_tabsopened.setBackgroundTintList(ColorStateList.valueOf(color));
                searchIcon.setBackgroundTintList(ColorStateList.valueOf(color));
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).
                registerReceiver(safetyResultReceiver,new IntentFilter(AppConstants.LBCAST_SearchPage_Safety_Result));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(updateSafetyRatingAppPurchaseFalse,
                new IntentFilter(AppConstants.LBCAST_updateSafetyRatingAppPurchaseFalse));
    }
    private BroadcastReceiver safetyResultReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent!=null){
                int index=intent.getIntExtra(AppConstants.IKEY_INDEX,0);
                searchResultPage.updateSafetyRating(index);
            }
        }
    };
    private BroadcastReceiver updateSafetyRatingAppPurchaseFalse=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent!=null){
                int index=intent.getIntExtra(AppConstants.IKEY_INDEX,0);
                searchResultPage.updateSafetyRatingAppPurchaseFalse(index);
            }
        }
    };

    private void handleClick(View v){
        switch (v.getId()){
            case R.id.iv_settings:
                toggleSettings();
                break;
            case R.id.urlEditText:
                onClickEditText();
                break;
            case R.id.iv_tabsopened:
                onClickTabs();
                break;
            case R.id.searchIcon:
                onClickEditText();
                break;
            case R.id.backButton:
                onClickBackward();
                break;
            case R.id.forwardButton:
                onClickForward();
                break;
        }
    }
    private void onClickForward(){
        popCurrentFragment();
        if(fragmentHome!=null){
            fragmentHome.onClickForward();
        }
    }
    private void onClickBackward(){
        popCurrentFragment();
        if(fragmentHome!=null){
            fragmentHome.onClickBack();
        }
    }

    @Override
    public void onClickParentBack() {
        super.onClickParentBack();
        if( GlobalVariables.getInstnace().currentHomeFragment!=null){
            GlobalVariables.getInstnace().currentHomeFragment.onClickSearchUrl(editText.getText().toString());
        }
    }

    @Override
    protected void onClickTabs() {
        super.onClickTabs();
    }

    public void toggleSettings() {

        if (GlobalVariables.getInstnace().isDrawerShowing) {
            hideDrawer();
            return;
        }
        showDrawer();
    }
    private void onClickEditText(){

        popCurrentFragment();
        if(GlobalVariables.getInstnace().currentHomeFragment!=null){
            GlobalVariables.getInstnace().currentHomeFragment.onClickSearchUrl(editText.getText().toString());
        }
    }
    @Override
    public void onClick(View v) {
        handleClick(v);
    }
}
