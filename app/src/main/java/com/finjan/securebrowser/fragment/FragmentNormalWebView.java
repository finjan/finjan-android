package com.finjan.securebrowser.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.NormaWebviewActivity;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.vpn.ui.authentication.SetupAccountActivity;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * fragment to show normal web view for other screen such as about, term and conditions etc.
 */

public class FragmentNormalWebView extends ParentFragment {

    private static FragmentNormalWebView instance;
    private View parentView;
    private WebView webView;
    private LinearLayout llLoader;
    private Activity parentActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.layout_about_help, container, false);
        setReferences(parentView);
        instance = this;
        changeStatusBarColor(R.color.col_status);
        return parentView;
    }

    private void setReferences(View parentView) {
        webView = ((WebView) parentView.findViewById(R.id.webview_about));
        llLoader = (LinearLayout) parentView.findViewById(R.id.vg_loader);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setDefaults();
        setData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void setData() {
        loadAboutHelpURl();
    }

    private void setDefaults() {
        setWebView();
    }

    private void setWebView() {
        webView.setVisibility(View.VISIBLE);
        webView.setWebViewClient(new UrlOverrideWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setSupportZoom(true);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {

                setProgressbar1(newProgress);
                showProggessBar();
                setProgressbarBackGround(NativeHelper.ProgressBarTypes.NORMAL);
                if (newProgress == 100) {
                    hideProggessBar();
                } else {
                    showProggessBar();
                }
            }
        });

        if (getArguments().getBundle(AppConstants.BKEY_BUNDLE) != null) {
            webView.loadUrl(getArguments().getBundle(AppConstants.BKEY_BUNDLE).getString(AppConstants.BKEY_URL));
        }
    }

    public Activity getParentActivity() {
        return parentActivity;
    }

    public void setParentActivity(Activity activity) {
        this.parentActivity = activity;
    }

    private void loadAboutHelpURl() {

        String url = getArguments().getString(AppConstants.BKEY_URL);
        if (!TextUtils.isEmpty(url)) {
            return;
        }
//        webView.loadUrl("about:blank");
        webView.loadUrl(url);
    }

    public FragmentNormalWebView getInstance() {
        return instance;
    }

    public void onClickBack() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            if (!TextUtils.isEmpty(getTitle()) && NativeHelper.getInstnace().checkActivity(getActivity())
                    && getActivity() instanceof NormaWebviewActivity) {
                if (getTitle().equalsIgnoreCase(getString(R.string.password_reset))) {
                    SetupAccountActivity.newLoginInstanceForResult(getActivity(),
                            VpnActivity.REQUEST_LOGIN, SetupAccountActivity.ACTION_SIGN_IN);
                    return;
                }
                if (getTitle().equalsIgnoreCase(getString(R.string.end_user_license_agreement))) {
                    SetupAccountActivity.newLoginInstanceForResult(getActivity(),
                            VpnActivity.REQUEST_LOGIN, SetupAccountActivity.ACTION_SIGN_UP);
                }
            } else {
                popCurrentFragment();
            }
//            showHomeFragment(this);
        }
    }

    private class UrlOverrideWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, final String url, Bitmap favicon) {
            llLoader.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            llLoader.setVisibility(View.VISIBLE);
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            llLoader.setVisibility(View.GONE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
//            llLoader.setVisibility(View.GONE);
        }
    }
}
