package com.finjan.securebrowser.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.finjan.securebrowser.adapter.TrackerAdapter;
import com.finjan.securebrowser.adapter.TrackerAdapterOld;
import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.AppLog;
import com.finjan.securebrowser.Constants;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.DbHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.model.TrackerCount;
import com.finjan.securebrowser.model.TrackerFoundModel;
import com.finjan.securebrowser.model.TrackerListModel;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import webHistoryDatabase.TrackerDao;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * fragment to show trackers from browser.
 */

public class FragmentTrackerList extends ParentFragment implements
       TrackerAdapter.TrackerExpendableListener,TrackerAdapterOld.UpdateTrackerDatabaseListener
{


    private View parentView;
    private ExpandableListView trackerExpList;
    private ArrayList<TrackerFoundModel> trackerFoundList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentView=inflater.inflate(R.layout.layout_tracker,container,false);
        setReferences(parentView);
        return parentView;
    }



    @Override
    public void onSwitchChildIndicatorClick(List<TrackerFoundModel> webhistoryList,ArrayList<TrackerListModel> urlList,
                                            int groupPosition,int childPosition) {
        boolean shouldAdd=urlList.get(0).isCheck();
        TrackerFoundModel trackerFoundModel=webhistoryList.get(groupPosition);

        if(urlList != null && urlList.size() > 0) {

            if (shouldAdd) {
                for (TrackerListModel trackerListModel:urlList){
                    trackerFoundModel.setCheckedString(trackerFoundModel.getCheckedString() +
                            Constants.URL_SEPRATOR + trackerListModel.getTrackerName());
                }

                DbHelper.getInstnace().addAllTrackerDB(urlList);

            } else {

                for (TrackerListModel trackerListModel:urlList){
                    String targetString=trackerFoundModel.getCheckedString();
                    targetString=targetString.replace(trackerListModel.getTrackerName(),"");
                    trackerFoundModel.setCheckedString(targetString);
                }

                DbHelper.getInstnace().removeAllTrackerDB(urlList);
            }
            if (trackerAdapter!=null){
                trackerAdapter.updateList(webhistoryList);
//                trackerAdapter.updateList(getUpdatedTrackerList(trackerFoundList));
            }
        }


//        onUpdateTrackerDatabase(webhistoryList,urlList, !urlList.get(0).isCheck());
    }

    @Override
    public void onCheckGroupIndicatorClick(List<TrackerFoundModel> webhistoryList,ArrayList<TrackerListModel> urlList,
                                           boolean shouldAdd,int groupPosition) {

        TrackerFoundModel trackerFoundModel=webhistoryList.get(groupPosition);
        if (urlList != null && urlList.size() > 0) {
            if (shouldAdd) {
                for (TrackerListModel trackerListModel:urlList){
                    trackerFoundModel.setCheckedString(trackerFoundModel.getCheckedString() +
                            Constants.URL_SEPRATOR + trackerListModel.getTrackerName());
                }

                DbHelper.getInstnace().addAllTrackerDB(urlList);

            } else {
                for (TrackerListModel trackerListModel:urlList){
                    String targetString=trackerFoundModel.getCheckedString();
                    targetString=targetString.replace(trackerListModel.getTrackerName(),"");
                    trackerFoundModel.setCheckedString(targetString);
                }
                DbHelper.getInstnace().removeAllTrackerDB(urlList);
            }
            if (trackerAdapter!=null){
                trackerAdapter.updateList(webhistoryList);
//                trackerAdapter.updateList(getUpdatedTrackerList(trackerFoundList));
            }
        }

//        onUpdateTrackerDatabase(webhistoryList,trackerURL, isChecked,groupPosition);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    protected void setFragmentData(ArrayList<TrackerFoundModel> trackerFoundList){
        this.trackerFoundList=trackerFoundList;
    }

    private void sortTrackerList(List<TrackerFoundModel> trackerFoundList){
        Collections.sort(trackerFoundList, new Comparator<TrackerFoundModel>() {
            @Override
            public int compare(TrackerFoundModel lhs, TrackerFoundModel rhs) {
                return lhs.getTrackerType().compareToIgnoreCase(rhs.getTrackerType());
//                return lhs.getTrackerType()./compareTo("Advertising");
            }
        });
    }


    private void setData(){
        setTrackerList(trackerExpList);
//        setTrackerList(rvTracker);
    }

    private void setReferences(View parentView) {
        trackerExpList=(ExpandableListView)parentView.findViewById(R.id.tracker_exp_List);
//
//        rvTracker=(RecyclerView) parentView.findViewById(R.id.tracker_recycler_view);
    }
    private TrackerAdapter trackerAdapter;

////    old
//    private void setTrackerList(RecyclerView trackerList) {
//        if(NativeHelper.getInstnace().checkActivity(getActivity()) && trackerFoundList!=null && trackerFoundList.size()>0){
//            TrackerAdapterOld adapter = new TrackerAdapterOld(getActivity(), getUpdatedTrackerList(trackerFoundList));
//            adapter.setListener(this);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
//            trackerList.setLayoutManager(mLayoutManager);
//            trackerList.setItemAnimator(new DefaultItemAnimator());
//            trackerList.setAdapter(adapter);
//        }
//    }
    //new

    private ArrayList<TrackerListModel> getTrackerList(String tackerName, ArrayList<TrackerListModel> trackerURL, boolean isChecked) {
        ArrayList<TrackerListModel> trackerList = new ArrayList<>();
        for (TrackerListModel url : trackerURL) {
            if (url.getTrackerName().equalsIgnoreCase(tackerName)) {
                trackerList.add(new TrackerListModel(url.getTrackerName(), url.getTrackerURl(), isChecked));
            }
        }
        return trackerList;
    }

    private ArrayList<TrackerListModel> getTrakerListFromAdapterList(List<TrackerFoundModel> fraTrackerFoundModels){

        ArrayList<TrackerListModel> trackerUpdatePreList=new ArrayList<>();
        for (TrackerFoundModel trackerFoundModel:fraTrackerFoundModels){
            for (TrackerCount trackerCount:trackerFoundModel.getTrackerCounts()){
                String trackerName=trackerCount.getName();
                ArrayList<TrackerListModel> trackerListModels=getTrackerList(trackerName,
                        trackerFoundModel.getTrackerURL(),
                        trackerFoundModel.getCheckedString().contains(trackerName));
                if(trackerListModels!=null && trackerListModels.size()>0){
                    trackerUpdatePreList.addAll(trackerListModels);
                }
            }
        }
        return trackerUpdatePreList;
    }
    private void setTrackerList(ExpandableListView trackerExpList) {
        if(NativeHelper.getInstnace().checkActivity(getActivity()) && trackerFoundList!=null && trackerFoundList.size()>0){

            trackerUpdatePreList=new ArrayList<>();
//        RecyclerView trackerList = (RecyclerView) findViewById(R.id.tracker_recycler_view);
//        TrackerAdapterOld trackerAdapter = new TrackerAdapterOld(UrlScannerActivity.this, getUpdatedTrackerList(trackerFoundList));
            List<TrackerFoundModel> listTemp = getUpdatedTrackerList(trackerFoundList);
            if(listTemp!=null && listTemp.size()>0){
                sortTrackerList(listTemp);
            }
            trackerAdapter = new TrackerAdapter(getContext(), listTemp);
            trackerAdapter.setListener(this);
            trackerUpdatePreList= getTrakerListFromAdapterList(listTemp);





//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//        trackerList.setLayoutManager(mLayoutManager);
//        trackerList.setItemAnimator(new DefaultItemAnimator());
//        trackerList.setAdapter(trackerAdapter);
            trackerExpList.setAdapter(trackerAdapter);
            for (int i=0;i<listTemp.size();i++){
                trackerExpList.expandGroup(i);
            }
            trackerExpList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                    return true;
                }
            });
//                TrackerAdapterOld adapter = new TrackerAdapterOld(getActivity(), getUpdatedTrackerList(trackerFoundList));
//                adapter.setListener(this);
//                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
//                trackerList.setLayoutManager(mLayoutManager);
//                trackerList.setItemAnimator(new DefaultItemAnimator());
//                trackerList.setAdapter(adapter);
        }
    }

    private boolean isReloadTrackerPage(ArrayList<TrackerListModel> trackerUpdatePreList,ArrayList<TrackerListModel> newTrackerList) {
        if(trackerUpdatePreList!=null){
            if ((trackerUpdatePreList.size()>newTrackerList.size() || trackerUpdatePreList.size()<newTrackerList.size())){
                return true;
            }
            boolean isContain=false;
            for (TrackerListModel trackerData : newTrackerList){
                isContain=false;
                for (TrackerListModel trackerPreData : trackerUpdatePreList){
                    if (trackerData.getTrackerURl().equalsIgnoreCase(trackerPreData.getTrackerURl())){
                        isContain=true;
                        if (trackerData.isCheck()!=trackerPreData.isCheck()){
                            return true;
                        }
                    }
                }
                if (!isContain){
                    return true;
                }
            }
        }
        return false;
    }

    private ArrayList<TrackerListModel> trackerUpdatePreList;
    private ArrayList<TrackerListModel> getDatabaseTracker() {
        ArrayList<TrackerListModel> modelList=new ArrayList<>();
        try {
            TrackerDao trackerDao = AnalyticsApplication.daoSession.getTrackerDao();
            List<webHistoryDatabase.Tracker> dataBaseTable = trackerDao.loadAll();
            if (dataBaseTable != null && dataBaseTable.size() > 0) {
                for (int j = 0; j < dataBaseTable.size(); j++) {
                    modelList.add(new TrackerListModel("",dataBaseTable.get(j).getUrl(),true));
                }
            }
        } catch (Exception e) {
            AppLog.printLog(e.getMessage());
        }
        return modelList;
    }

    private List<TrackerFoundModel> getUpdatedTrackerList(ArrayList<TrackerFoundModel> trackerFoundList) {

        ArrayList<TrackerFoundModel> tempTrackerList = new ArrayList<>();
        if (trackerFoundList==null || trackerFoundList.size() == 0)
            return tempTrackerList;

        HashMap<String, ArrayList<TrackerListModel>> trackerHashMap = new HashMap<>();

        for (TrackerFoundModel trackerModel:trackerFoundList){
            ArrayList<TrackerListModel> nameList = trackerHashMap.get(trackerModel.getTrackerType());
            if (nameList == null) {
                nameList = new ArrayList<>();
            }
            nameList.add(new TrackerListModel(trackerModel.getTrackerName(), trackerModel.getTrackerScript()));
            trackerHashMap.put(trackerModel.getTrackerType(), nameList);
        }
        if (trackerHashMap.containsKey("Disconnect")) {
            trackerHashMap = manageDisconnectCatagory(trackerHashMap);
        }
        Set<String> trackerTypeList = trackerHashMap.keySet();
        for (String trackerType : trackerTypeList) {
            ArrayList<TrackerListModel> trackerNameList = trackerHashMap.get(trackerType);
            TrackerFoundModel model = new TrackerFoundModel();
            model.setTrackerType(trackerType);
            model.setTrackerCounts(getTrackerCount(trackerNameList));
            model.setTrackerURL(trackerNameList);
            tempTrackerList.add(model);
        }

        return checkTrackerDatabase(tempTrackerList);
    }
    private List<TrackerFoundModel> checkTrackerDatabase(ArrayList<TrackerFoundModel> tempTrackerList) {
        TrackerDao trackerDao = AnalyticsApplication.daoSession.getTrackerDao();
        if (trackerDao != null && trackerDao.loadAll() != null && trackerDao.loadAll().size() > 0) {
            List<webHistoryDatabase.Tracker> trackerDataList = trackerDao.loadAll();
            if (trackerDataList == null || trackerDataList.size() <= 0) {
                return tempTrackerList;
            }
            for (TrackerFoundModel data : tempTrackerList) {
                ArrayList<TrackerListModel> url = data.getTrackerURL();
                for (TrackerListModel urlData : url) {
                    for (webHistoryDatabase.Tracker dbData : trackerDataList) {
                        if (dbData.getUrl().equalsIgnoreCase(urlData.getTrackerURl())) {
                            if (TextUtils.isEmpty(data.getCheckedString())) {
                                data.setCheckedString(urlData.getTrackerName());
                            } else {
                                data.setCheckedString(data.getCheckedString() + Constants.URL_SEPRATOR + urlData.getTrackerName());
                            }
                            break;
                        }
                    }
                }
            }

        }
        return tempTrackerList;
    }
    private ArrayList<TrackerCount> getTrackerCount(ArrayList<TrackerListModel> trackerNameList) {
        ArrayList<TrackerCount> trackerList = new ArrayList<>();
        HashMap<String, Integer> trackerCountMap = new HashMap<>();
        ArrayList<String> nameList = new ArrayList<>();
        for (int i = 0; i < trackerNameList.size(); i++) {
            nameList.add(trackerNameList.get(i).getTrackerName());
        }
        for (int i = 0; i < trackerNameList.size(); i++) {
            String name = trackerNameList.get(i).getTrackerName();
            int count = Collections.frequency(nameList, name);
            trackerCountMap.put(name, count);
        }
        Set<String> keys = trackerCountMap.keySet();
        for (String name : keys) {
            Integer count = trackerCountMap.get(name);
            trackerList.add(new TrackerCount(name, count));
        }

        return trackerList;
    }


    private HashMap<String, ArrayList<TrackerListModel>> manageDisconnectCatagory(HashMap<String, ArrayList<TrackerListModel>> trackerHashMap) {
        String disKey = "Disconnect";
        String contKey = "Content";
        ArrayList<TrackerListModel> disValue = trackerHashMap.get(disKey);
        trackerHashMap.remove(disKey);
        if (trackerHashMap.containsKey(contKey)) {
            ArrayList<TrackerListModel> contValue = trackerHashMap.get(contKey);
            for (int i = 0; i < disValue.size(); i++) {
                contValue.add(disValue.get(i));
            }
        } else {
            trackerHashMap.put(contKey, disValue);
        }
        return trackerHashMap;

    }

    public void onUpdateTrackerDatabase(List<TrackerFoundModel> webhistoryList,ArrayList<TrackerListModel> urlList, boolean shouldAdd) {

//        if (urlList != null && urlList.size() > 0) {
//            if (shouldAdd) {
//                for (TrackerListModel trackerListModel:urlList){
//                    trackerFoundModel.setCheckedString(trackerFoundModel.getCheckedString() +
//                            Constants.URL_SEPRATOR + trackerListModel.getTrackerName());
//                }
//
//                DbHelper.getInstnace().addAllTrackerDB(urlList);
//
//            } else {
//                DbHelper.getInstnace().removeAllTrackerDB(urlList);
//            }
//            if (trackerAdapter!=null){
//                trackerAdapter.updateList(getUpdatedTrackerList(trackerFoundList));
//            }
//        }
    }


//    private void onStopTrackerList(List<TrackerFoundModel> trackerFoundModelList,List<TrackerFoundModel> modified){
//        for (TrackerFoundModel trackerFoundModel: trackerFoundModelList){
//            for (TrackerCount trackerCount: trackerFoundModel.getTrackerCounts()){
//                if(trackerCount.ischecked()==)
//            }
//        }
//    }

    private void removeAllTracker(ArrayList<TrackerListModel> urlList) {
        try {
            TrackerDao trackerDao = AnalyticsApplication.daoSession.getTrackerDao();
            for (int i = 0; i < urlList.size(); i++) {
                List<webHistoryDatabase.Tracker> dataBaseTable = trackerDao.loadAll();
                if (dataBaseTable != null && dataBaseTable.size() > 0) {
                    for (int j = 0; j < dataBaseTable.size(); j++) {
                        if (urlList.get(i).getTrackerURl().equalsIgnoreCase(dataBaseTable.get(j).getUrl())) {
                            trackerDao.deleteByKey(dataBaseTable.get(j).getId());
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
        } catch (Exception e) {
            AppLog.printLog(e.getMessage());
        }
    }

    private void addAllTracker(ArrayList<TrackerListModel> urlList) {
        try {
            TrackerDao trackerDao = AnalyticsApplication.daoSession.getTrackerDao();
            for (TrackerListModel model : urlList) {
                List<webHistoryDatabase.Tracker> dataBaseTable = trackerDao.loadAll();
                if (dataBaseTable != null && !dataBaseTable.contains(model.getTrackerURl())) {
                    webHistoryDatabase.Tracker tracker = new webHistoryDatabase.Tracker();
                    tracker.setUrl(model.getTrackerURl());
                    trackerDao.insert(tracker);
                }
            }
        } catch (Exception e) {
            AppLog.printLog(e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        GoogleTrackers.Companion.setSTrackers();
//        LocalyticsTrackers.Companion.setSTrackers();
//        GoogleTrackers.Companion.setTracking(GoogleTrackers.Companion.getInstance().getSCREEN_BlockedTrackers());
    }

    @Override
    public void onStop() {
        super.onStop();
//        if(rvTracker!=null){
//            boolean shouldReloadWebsite=false;
//            HashMap<String,Boolean> defaultKeyList=((TrackerAdapterOld)rvTracker.getAdapter()).getDefaultKeyList();
//            HashMap<String,Boolean> modifiedKeyList=((TrackerAdapterOld)rvTracker.getAdapter()).getModifiedKeyList();
//            if(modifiedKeyList.size()>0){
//                for (String key:modifiedKeyList.keySet()){
//                    if(defaultKeyList.containsKey(key) && defaultKeyList.get(key)!=modifiedKeyList.get(key)){
//                        shouldReloadWebsite=true;
//                    }
//                }
//            }else {
//                shouldReloadWebsite=true;
//            }
//            if(shouldReloadWebsite && GlobalVariables.getInstnace().currentHomeFragment!=null){
//                GlobalVariables.getInstnace().currentHomeFragment.onReloadClick(null);
//            }
//        }
//        ArrayList<TrackerListModel> newTrackerList = getDatabaseTracker();
        if(trackerAdapter!=null && GlobalVariables.getInstnace().currentHomeFragment!=null
                && isReloadTrackerPage(trackerUpdatePreList,
                getTrakerListFromAdapterList(trackerAdapter.getCurrentList()))){
            GlobalVariables.getInstnace().currentHomeFragment.onReloadClick();
        }
    }
}
