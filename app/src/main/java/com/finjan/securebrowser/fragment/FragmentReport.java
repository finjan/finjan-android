package com.finjan.securebrowser.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.finjan.securebrowser.AppLog;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.ScanResultsAdapter;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.SearchHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.model.ReportCard;
import com.finjan.securebrowser.model.SafetyResult;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * fragment to show report on click bottom bar after safe scan on browser.
 */


public class FragmentReport extends ParentFragment implements View.OnClickListener {

    private View parentView;
    private ViewGroup report, reportButton, domainCategoryBody, llDtailsContainer, reportMainButton;
    private TextView reportButtonText, reportTitle, reportDescription, domainLocationBody,  tv_detectratio,
            tv_categtitle, tv_lastseen, factoid;
    private ImageView reportButtonImg, flag;
    private LinearLayout ll_scanresults_title, ll_dyk, llScans, llDetail,llIndicator;
    private RecyclerView rv_scanresults;
    private LinearLayout rl_country;
    //    private View  view_title;
    //Main model for Report making
    private SafetyResult safetyResult;
    private ScrollView scroll_view;
    private ImageView iv_result_status, ivDetail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.report_view, container, false);
        setReferences(parentView);
        return parentView;
    }

    private void setDefaults() {
        if(getTitleTextview()!=null){
            getTitleTextview().setTextSize(getResources().getDimension(R.dimen._5sdp));
            getTitleTextview().setAllCaps(false);
            if(getTitleTextview()!=null && !TextUtils.isEmpty(getTitleTextview().getText().toString())){
                String title=getTitleTextview().getText().toString().toLowerCase();
                if(title.startsWith("www")){
                    getTitleTextview().setText(title);
                }else {
                    getTitleTextview().setText("www."+title);
                }
            }
        }
        factoid.setText(getRandomFactoid());
    }

    private String title;
    protected void setFragmentData(SafetyResult newSafetyResult,String title) {
        this.safetyResult = newSafetyResult;
        if(!TextUtils.isEmpty(title)){
//            this.title=title.substring(title.indexOf("://")+3);
            this.title=NativeHelper.getInstnace().getUrlDomain(title);
            return;
        }
        this.title=title;
    }

    private void setReferences(View parentView) {

        llScans = (LinearLayout) parentView.findViewById(R.id.ll_scans);
        llDetail = (LinearLayout) parentView.findViewById(R.id.ll_detail);
//        ivDetail = (ImageView) parentView.findViewById(R.id.iv_detail);
        llIndicator=(LinearLayout)parentView.findViewById(R.id.ll_indicator);

        iv_result_status = (ImageView) parentView.findViewById(R.id.iv_result_status);
        report = (ViewGroup) parentView.findViewById(R.id.report);
        ll_dyk = (LinearLayout) parentView.findViewById(R.id.ll_dyk);
        reportButton = (ViewGroup) parentView.findViewById(R.id.reportButton);
        reportMainButton = (ViewGroup) parentView.findViewById(R.id.reportMainButton);
        domainCategoryBody = (ViewGroup) parentView.findViewById(R.id.domainCategoryBody);
        tv_detectratio = (TextView) parentView.findViewById(R.id.tv_detectratio);
        ll_scanresults_title = (LinearLayout) parentView.findViewById(R.id.ll_scanresults_title);
        rv_scanresults = (RecyclerView) parentView.findViewById(R.id.rv_scanresults);
        rl_country = (LinearLayout) parentView.findViewById(R.id.ll_country);
//        view_country = (View) parentView.findViewById(R.id.view_country);
//        view_title = (View) parentView.findViewById(R.id.view_title);
        tv_categtitle = (TextView) parentView.findViewById(R.id.tv_categtitle);
        flag = (ImageView) parentView.findViewById(R.id.flag);
        factoid = (TextView) parentView.findViewById(R.id.factoid);
        scroll_view = (ScrollView) parentView.findViewById(R.id.scroll_view);
        tv_lastseen = (TextView) reportButton.findViewById(R.id.tv_lastseen);


//        tv_moreinfo = (TextView) parentView.findViewById(R.id.tv_moreinfo);
        llDtailsContainer = (ViewGroup) parentView.findViewById(R.id.ll_detail_container);
//        trackerButton = (ViewGroup) parentView.findViewById(R.id.rl_trackerlayout);
        reportButtonText = (TextView) parentView.findViewById(R.id.reportButtonText);
        reportButtonImg = (ImageView) parentView.findViewById(R.id.reportButtonImg);
        reportTitle = (TextView) parentView.findViewById(R.id.reportTitle);
        reportDescription = (TextView) parentView.findViewById(R.id.reportDescription);
        domainLocationBody = (TextView) parentView.findViewById(R.id.domainLocationBody);
        reportTitle.setTypeface(null, Typeface.BOLD);
        reportButtonText.setTypeface(null,Typeface.BOLD);
//        ((RelativeLayout) parentView.findViewById(R.id.rl_toggle_report_btn)).setOnClickListener(this);

//        tv_moreinfo.setOnClickListener(this);
        llDtailsContainer.setOnClickListener(this);


//        reportMainButton.setVisibility(View.GONE);
//        reportButton.setVisibility(View.GONE);
//        report.setVisibility(View.GONE);
    }

    private void setData(String title) {

        if (safetyResult == null) {
            return;
        }
        setDefaultView(title);
        setDefaults();
        setFlag(safetyResult);
        setDomainCategoryBody(safetyResult);
        setReport(safetyResult);
        GoogleTrackers.Companion.setSVirusResults();
//        LocalyticsTrackers.Companion.setSVirusResults();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData(title);
    }

    @Override
    public void onClickParentBack() {
        if (llDetail.getVisibility() == View.VISIBLE) {
            showScans();
            return;
        }
        super.onClickParentBack();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_detail_container:
                showDetail();
                break;
//            case R.id.rl_toggle_report_btn:
//                onClickParentBack();
//                break;
//            case R.id.tv_moreinfo:

//                if (llScans.getVisibility() == View.VISIBLE) {
//                    tv_moreinfo.setText("X");
//                    hideScanResult();
//                    hideScanResults();
//                } else {
//                    tv_moreinfo.setText("?");
//                    showScanResult();
//                    setDefaultView();
//                }
//                break;
            case R.id.lyt_safety_close:
//                onClickReportBarAway(v);
                break;
            case R.id.reportButton:
//                toggleSetting();
                break;
        }
    }


    private void toggleSetting() {
//        Bundle bundle=new Bundle();
//        bundle.putInt(AppConstants.BKEY_REDIRECTED_FUNCTION,AppConstants.RFUC_TOGGLE_SAFE_BAR);
//        setChangeCarrier(bundle);
        popCurrentFragment();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getTitleTextview()!=null){
            getTitleTextview().setTextSize(getResources().getDimension(R.dimen._5sdp));
            getTitleTextview().setAllCaps(true);
        }
    }

    public void onClickReportBarAway(View view) {
//        Bundle bundle=new Bundle();
//        bundle.putInt(AppConstants.BKEY_REDIRECTED_FUNCTION,AppConstants.RFUC_SAFE_BAR_AWAY);
//        setChangeCarrier(bundle);
        popCurrentFragment();
    }


    public void hideScanResults() {
        ll_scanresults_title.setVisibility(View.GONE);
        rv_scanresults.setVisibility(View.GONE);
        ll_dyk.setVisibility(View.VISIBLE);
        domainCategoryBody.setVisibility(View.VISIBLE);
        tv_categtitle.setVisibility(View.VISIBLE);
        rl_country.setVisibility(View.VISIBLE);
//        view_country.setVisibility(View.VISIBLE);
        reportDescription.setVisibility(View.VISIBLE);
        reportTitle.setVisibility(View.VISIBLE);
//        view_title.setVisibility(View.VISIBLE);
    }

    private void showDetail() {
        Animation animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_to_bottom);
        llScans.setAnimation(animation2);
        llScans.setVisibility(View.GONE);

        Animation animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_top);
        llDetail.setAnimation(animation1);
        llDetail.setVisibility(View.VISIBLE);

        if (getToolbar() != null) {
            changeStatusBarColor(safetyResult.getColor());
            int  colour=ResourceHelper.getInstance().getColor(safetyResult.getColor());
            ((RelativeLayout) (getToolbar().findViewById(R.id.rl_title))).setBackgroundColor(colour);
        }
        if(getBackButton()!=null){
            getBackButton().setColorFilter(ResourceHelper.getInstance().getColor(R.color.white));
        }

    }

    private void showScans() {
        Animation animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_to_right);
        llDetail.setAnimation(animation1);
        llDetail.setVisibility(View.GONE);


        Animation animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_left);
        llScans.setAnimation(animation2);
        llScans.setVisibility(View.VISIBLE);

        if (getToolbar() != null) {
            changeStatusBarColor(R.color.col_status);
            int  colour=ResourceHelper.getInstance().getColor(R.color.col_desc_w);
            ((RelativeLayout) (getToolbar().findViewById(R.id.rl_title))).setBackgroundColor(colour);
        }
        if(getBackButton()!=null){
            getBackButton().setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_0));
        }
    }

    public String getRandomFactoid() {
//        String[] factoidArray = getResources().getStringArray(R.array.factoid_array);
//        ArrayList<String> factoidArray = PlistHelper.getInstance().getPlistvalue(Constants.KEY_REPORT_FACTOIDS);
        ArrayList<String> factoidArray = PlistHelper.getInstance().getFactoryArray(PlistHelper.FACTOR_TYPE.TYPE_FACTOR_ID_NEW);
//        return factoidArray[new Random().nextInt(factoidArray.length)];
        if (factoidArray == null || factoidArray.size() == 0)
            return "";
        else
            return factoidArray.get(new Random().nextInt(factoidArray.size()));
    }


    public void setFlag(SafetyResult safetyResult) {
        if (safetyResult == null) {
            return;
        }
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            int flagResource = getResources().getIdentifier(safetyResult.getFlagName(), "drawable", getActivity().getPackageName());
            flagResource = flagResource == 0 ? getResources().getIdentifier("no_country", "drawable", getActivity().getPackageName()) : flagResource;
            flag.setImageResource(flagResource);
        }
        domainLocationBody.setText(safetyResult.country);
    }

    public void setDefaultView(String title) {
        llDetail.setVisibility(View.GONE);
        llScans.setVisibility(View.VISIBLE);

        setTitle(title);
    }


    private void setReport(SafetyResult safetyResult) {
        setReportAdapter(SearchHelper.getInstance().getKeyArrayList(), SearchHelper.getInstance().getValueArrayList());
        setScaningRation(safetyResult);
        setLastScan(safetyResult);
        setReportButtons(safetyResult);
//        reportButtonImg.setBackground(getResources().getDrawable(R.drawable.safe));
//        tv_lastseen.setText("");
//        tv_detectratio.setText("");
        reportButton.setClickable(false);
//        reportButtonText.setText("SAFE");
//        fakeButton.setBackgroundColor(getResources().getColor(R.color.fj_search_green_fg));
//        reportButton.setBackgroundColor(getResources().getColor(R.color.fj_search_green_fg));
//        trackerButton.setBackgroundColor(getResources().getColor(R.color.fj_search_green_fg));
//            if (report.getVisibility()==View.GONE){
        if (reportButton.getHeight() != 0) {
//            setReportButton();
        } else {
            final ViewTreeObserver vto = reportButton.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
//                    setReportButton();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        vto.removeOnGlobalLayoutListener(this);
                    } else {
                        vto.removeGlobalOnLayoutListener(this);
                    }
                }
            });
        }
    }

    private void setReportAdapter(ArrayList<String> keyArrayList, ArrayList<String> valueArrayList) {
        ScanResultsAdapter scanResultsAdapter = new ScanResultsAdapter(getActivity(), keyArrayList, valueArrayList) {
        };
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_scanresults.setLayoutManager(layoutManager);
        rv_scanresults.setAdapter(scanResultsAdapter);
    }

    private void setScaningRation(SafetyResult safetyResult) {
        SpannableString spanString = new SpannableString("" + safetyResult.getPositive() + "/" + safetyResult.getTotal());
//        spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
        tv_detectratio.setText(spanString);
    }

    private void setLastScan(SafetyResult safetyResult) {
        Date date = null;
        try {
            String datestr = safetyResult.getLastSeen();
            AppLog.printLog("LAST Date ", "   " + datestr);
            DateFormat formatter;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            date = (Date) formatter.parse(datestr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String last_scan = "Last Scanned on " + date.getDate() + "/" + (date.getMonth() + 1);
        AppLog.printLog("LAST SCANNED ", "   " + last_scan);
        tv_lastseen.setText("" + (date.getMonth() + 1) + "/" + date.getDate());
    }

    private ReportCard getRepordCard(int titleId) {

        switch (titleId) {
            case R.string.good_site_title:
                return PlistHelper.getInstance().getReportCard().get(PlistHelper.REPORT_TYPE.SAFE);
            case R.string.suspicious_site_title:
                return PlistHelper.getInstance().getReportCard().get(PlistHelper.REPORT_TYPE.SUSPICIOUS);
            case R.string.bad_site_title:
                return PlistHelper.getInstance().getReportCard().get(PlistHelper.REPORT_TYPE.DANGER);
        }
        return null;
    }

    private void setDomainCategoryBody(SafetyResult safetyResult) {
        if (safetyResult.country != null && !safetyResult.country.equals("")) {
            domainLocationBody.setText(safetyResult.country);
//                getFlag(safetyResult);
            domainCategoryBody.removeAllViews();
            String[] categories = safetyResult.categories;
            if (categories != null) {
                for (String category : categories) {
                    TextView categoryView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.category_template, domainCategoryBody, false);
                    categoryView.setText(category);
                    domainCategoryBody.addView(categoryView);
                }
            }
        }
    }

    private void setReportButtons(SafetyResult safetyResult) {

        ReportCard reportCard = getRepordCard(safetyResult.getTitle());
        if (reportCard == null) {
            return;
        }
        reportTitle.setText(reportCard.getHeading());
        reportDescription.setText(reportCard.getDescription());

        reportButtonText.setText(safetyResult.getButtonString());
        reportButtonImg.setImageResource(safetyResult.getButtonImage());

        Drawable drawable = null;
        Drawable statusIcon = null;

        int color = R.color.red_1;
        if (safetyResult.color == R.color.red_1) {
            color = R.color.red_1;
            drawable = ResourceHelper.getInstance().getDrawable(R.drawable.bg_scan_result_dangrous_selector);
//            drawable = ResourceHelper.getInstance().getDrawable(R.drawable.red_drawable);
            statusIcon = ResourceHelper.getInstance().getDrawable(R.drawable.scan_status_big_danger);
        } else if (safetyResult.color == R.color.yellow_1) {
            color = R.color.yellow_1;
            drawable = ResourceHelper.getInstance().getDrawable(R.drawable.bg_scan_result_suspecious_selector);
//            drawable = ResourceHelper.getInstance().getDrawable(R.drawable.red_drawable);
            statusIcon = ResourceHelper.getInstance().getDrawable(R.drawable.scan_status_big_caution);
        } else if (safetyResult.color == R.color.green_colour_1) {
            color = R.color.green_colour_1;
            drawable = ResourceHelper.getInstance().getDrawable(R.drawable.bg_scan_result_safe_selector);
//            drawable = ResourceHelper.getInstance().getDrawable(R.drawable.green_drawable);
            statusIcon = ResourceHelper.getInstance().getDrawable(R.drawable.scan_status_big_safe);
        }
//        if (drawable != null) {
//            if (getToolbar() != null) {
//                changeStatusBarColor(color);
//                ((RelativeLayout) (getToolbar().findViewById(R.id.rl_title))).setBackground(drawable);
//            }
            iv_result_status.setImageDrawable(statusIcon);
//            reportButton.setBackground(drawable);
//            scroll_view.setBackground(drawable);
//        }
        color=ResourceHelper.getInstance().getColor(color);
        reportMainButton.setBackgroundColor(color);
        llIndicator.setBackgroundColor(color);
    }
}
