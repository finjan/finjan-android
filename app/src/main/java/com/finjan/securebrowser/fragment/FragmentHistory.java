package com.finjan.securebrowser.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.BookmarksAdapter;
import com.finjan.securebrowser.CustomPagerAdapter;
import com.finjan.securebrowser.ExpandableListAdapter;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.BaseActivity;
import com.finjan.securebrowser.helpers.DbHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import webHistoryDatabase.bookmarks;
import webHistoryDatabase.tabhistory;
import webHistoryDatabase.webhistory;
import webHistoryDatabase.webhistoryDao;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class FragmentHistory extends ParentFragment implements View.OnClickListener {

    private static final int HISTORY = 0;
    private static final int BOOKMARKS = 1;
    private View parentView;
    private TabLayout tabs;
    private ViewPager pager;
    private ExpandableListView expListView;
    private RecyclerView recyclerView;
    private ExpandableListAdapter listAdapter;
    private HashMap<String, ArrayList<webhistory>> dateWiseHistory;
    private ExpandableListAdapter.ListItemListener listItemListener = new ExpandableListAdapter.ListItemListener() {
        @Override
        public void onChildDelete(View view, String date, int childPosition) {
            ArrayList<webhistory> arrayList = dateWiseHistory.get(date);
            try {
                webhistory webhistory = arrayList.get(childPosition);
                arrayList.remove(childPosition);
                webhistoryDao webhistoryDao = AnalyticsApplication.daoSession.getWebhistoryDao();
                webhistoryDao.delete(webhistory);
                if(arrayList.size()==0){
                    listAdapter.getDaysList().remove(date);
                    if(listAdapter.getDaysList().size()==0){
                        tvHistoryClear.setVisibility(View.GONE);
                    }
                }
                listAdapter.notifyDataSetInvalidated();
                listAdapter.notifyDataSetChanged();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUrlClicks(View view, String date, int childPosition) {
            GlobalVariables.getInstnace().forceLoader=true;
            boolean isPrivate=GlobalVariables.getInstnace().currentHomeFragment.isPrivate;
            Long id=GlobalVariables.getInstnace().currentHomeFragment.id;
            ArrayList<webhistory> arrayList = dateWiseHistory.get(date);
            webhistory webhistory = arrayList.get(childPosition);
            openTab(id,webhistory.getPagetitle(),webhistory.getUrl(),isPrivate,true,false);
        }
    };

    private BookmarksAdapter.ListItemListener bookmarklistItemListener = new BookmarksAdapter.ListItemListener() {
        @Override
        public void onBookMarkUrlDel(View view, Long idUrl, int position, List<bookmarks> bookmarksList) {
            DbHelper.getInstnace().getBookmarksDao().deleteByKey(idUrl);
//            recyclerView.getAdapter().notifyItemRemoved(position);
        }

        @Override
        public void onUrlClicks(View view, int position, List<bookmarks> bookmarksList) {
            bookmarks bookmarks=bookmarksList.get(position);
            tabhistory tabhistory=DbHelper.getInstnace().getLastTab();
            GlobalVariables.getInstnace().forceLoader=true;
            if(tabhistory!=null){
                boolean isPrivate=TextUtils.isEmpty(tabhistory.getType());
                openTab(tabhistory.getId(),bookmarks.getPagetitle(),bookmarks.getUrl(),isPrivate,true,false);
            }

//            Long id=GlobalVariables.getInstnace().currentHomeFragment.id;
//            boolean isPrivate=GlobalVariables.getInstnace().currentHomeFragment.isPrivate;

//            openUrl(bookmarks.getPagetitle(),bookmarks.getUrl(),GlobalVariables.getInstnace().currentHomeFragment.isPrivate,
//                    id);
        }
    };

    private void setDefaultPage(){
        String title=getFragmentTag();
        if(!TextUtils.isEmpty(title)){
            if(title.equalsIgnoreCase(DKEY_Bookmarks)){
                selectBookmarkTab();

            }else {
                selectHistoryTab();
            }
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.hist_book_layout, container, false);
        setReferences(parentView);
        return parentView;
    }

    private ImageView tvHistoryClear,tvBookMarkClear;
    private ViewGroup vgHistory,vgBookmark;
    private void setReferences(View parentView) {
        pager = (ViewPager) parentView.findViewById(R.id.viewpager);
        tabs = (TabLayout) parentView.findViewById(R.id.tabs);

        ImageView moreOption=(ImageView)parentView.findViewById(R.id.iv_more_option);
//        moreOption.setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_0));
        moreOption.setVisibility(View.GONE);
        //for history part
        expListView = (ExpandableListView) parentView.findViewById(R.id.lvExp);
        tvHistoryClear=((ImageView) parentView.findViewById(R.id.clearHistory));
        tvBookMarkClear=((ImageView) parentView.findViewById(R.id.clearButton));
        tvHistoryClear.setOnClickListener(this);
        tvBookMarkClear.setOnClickListener(this);
        vgHistory=(ViewGroup)parentView.findViewById(R.id.vg_hist_exp);
        //for bookmark part
        recyclerView = (RecyclerView) parentView.findViewById(R.id.recycler_view);
        vgBookmark=(ViewGroup)parentView.findViewById(R.id.vg_bookhist);
    }

    private void swith(int page) {
        if (page == HISTORY) {
            recyclerView.setVisibility(View.GONE);
            expListView.setVisibility(View.VISIBLE);
            return;
        }
        expListView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }
    private void setData(){

    }

    private void setBookmarks(RecyclerView recyclerView) {
        List<bookmarks> bookmarksList = DbHelper.getInstnace().getBookMarsList();
        Collections.reverse(bookmarksList);


        BookmarksAdapter bookmarksAdapter = new BookmarksAdapter(bookmarksList, bookmarklistItemListener,getContext());
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(bookmarksAdapter);
        tvBookMarkClear.setVisibility(bookmarksList.size()==0?View.GONE:View.VISIBLE);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setPager(pager);
        setExpListView(expListView);
        setBookmarks(recyclerView);
        setDefaultPage();
//        swith(tabType);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void setExpListView(ExpandableListView expListView) {

        dateWiseHistory =getHistoryDataSet();
        ArrayList<String> dateKeys = new ArrayList<>();
        if (dateWiseHistory != null && dateWiseHistory.size() > 0) {
            Set<String> keys = dateWiseHistory.keySet();
            for (String key : keys) {
                dateKeys.add(key);
            }
            Collections.sort(dateKeys);
            Collections.reverse(dateKeys);
        }
        listAdapter = new ExpandableListAdapter(getActivity(), dateKeys, dateWiseHistory, listItemListener);
        expListView.setAdapter(listAdapter);
        for (int i = 0; i < dateWiseHistory.size(); i++) {
            expListView.expandGroup(i);
        }
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });
        tvHistoryClear.setVisibility(dateWiseHistory.size()==0?View.GONE:View.VISIBLE);
    }

    private void setPager(final ViewPager pager) {
        pager.setAdapter(new CustomPagerAdapter(getActivity(),true));
        tabs.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                   selectHistoryTab();
                } else {
                  selectBookmarkTab();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }
    private void selectHistoryTab(){
        pager.setCurrentItem(0, true);
        // check weather we need to refresh or not as it is costly to use it
        notifyHistoryTab();
        setTitle(getActivity().getString(R.string.history));
        vgBookmark.setVisibility(View.GONE);
        vgHistory.setVisibility(View.VISIBLE);
        LocalyticsTrackers.Companion.setBrowserHistory();
    }
    private void selectBookmarkTab() {
        pager.setCurrentItem(1, true);
        setTitle(getActivity().getString(R.string.bookmarks));
        vgBookmark.setVisibility(View.VISIBLE);
        vgHistory.setVisibility(View.GONE);
        // check weather we need to refresh or not as it is costly to use it
        notifyBookMarkTab();
        LocalyticsTrackers.Companion.setBookmarks();
    }


    private void setTabListener(){
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    tabs.setScrollPosition(0, 0f, true);
                    pager.setCurrentItem(0, true);
                    setTitle(getResources().getString(R.string.history));
                    //check weather if is working fine without referesh adapter
                    notifyHistoryTab();
                } else {
                    tabs.setScrollPosition(1, 0f, true);
                    pager.setCurrentItem(1, true);
                    setTitle(getResources().getString(R.string.bookmarks));
                    //check weather if is working fine without referesh adapter
                    notifyBookMarkTab();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clearHistory:
                showClearHistoryDialog(false);
//                onClickClearHistory();
                break;
            case R.id.clearButton:
                showClearHistoryDialog(true);
//                onClickClearBookMark();
                break;
        }
    }

    private void onClickClearHistory() {
        DbHelper.getInstnace().getWebhistoryDao().deleteAll();
        notifyHistoryTab();
    }

    private void onClickClearBookMark() {
        DbHelper.getInstnace().getBookmarksDao().deleteAll();
        notifyBookMarkTab();
    }

    private void clearHistory(boolean bookmark){
        if(bookmark){
            onClickClearBookMark();
        }else {
            onClickClearHistory();
        }
    }

    private void showClearHistoryDialog(final boolean isBookmark){
        String msg=isBookmark?getString(R.string.delete_all_bookmarks):getString(R.string.delete_all_history_items);

        new FinjanDialogBuilder(getActivity()).setMessage(msg)
                .setPositiveButton(getString(R.string.alert_yes)).setNegativeButton(getString(R.string.alert_no))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        clearHistory(isBookmark);
                    }
                }).show();
    }

    private void notifyHistoryTab() {
        dateWiseHistory = getHistoryDataSet();
        ArrayList<String> dateKeys = new ArrayList<>();
        if (dateWiseHistory != null && dateWiseHistory.size() > 0) {
            Set<String> keys = dateWiseHistory.keySet();
            for (String key : keys) {
                dateKeys.add(key);
            }
            Collections.sort(dateKeys);
            Collections.reverse(dateKeys);
        }
        listAdapter.notifyAdapter(dateKeys, dateWiseHistory);
        if(dateWiseHistory==null || dateWiseHistory.size()==0){
            tvHistoryClear.setVisibility(View.GONE);
        }else {
            if(dateWiseHistory.size()==1 && dateWiseHistory.get(dateKeys.get(0))!=null &&
                    dateWiseHistory.get(dateKeys.get(0)).size()>0){
                tvHistoryClear.setVisibility(View.VISIBLE);
            }
        }
    }

    private void notifyBookMarkTab() {
        List<bookmarks> bookmarksList = DbHelper.getInstnace().getBookMarsList();
        Collections.reverse(bookmarksList);
        ((BookmarksAdapter) recyclerView.getAdapter()).notifyBookmarkTab(bookmarksList);
        if(bookmarksList.size()==0){
            tvBookMarkClear.setVisibility(View.GONE);
        }else {
            tvBookMarkClear.setVisibility(View.VISIBLE);
        }
    }
    public void onClickBack(){
        if(NativeHelper.getInstnace().checkActivity(getActivity())){
            ((BaseActivity)getActivity()).hideNormalFragment(this,GlobalVariables.getInstnace().currentHomeFragment);
        }
    }

    public HashMap<String, ArrayList<webhistory>> getHistoryDataSet() {
        webhistoryDao webhistoryDao = DbHelper.getInstnace().getWebhistoryDao();
        List<webhistory> webhistoryList = webhistoryDao.queryBuilder().orderDesc(webHistoryDatabase.webhistoryDao.Properties.Dtime).list();
        final HashMap<String, ArrayList<webhistory>> dateWiseHistory = new HashMap<>();
        for (int i = 0; i < webhistoryList.size(); i++) {
            Date dTime = webhistoryList.get(i).getDtime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String finalDate = formatter.format(dTime);
            if (dateWiseHistory.containsKey(finalDate)) {
                ArrayList<webhistory> strings = dateWiseHistory.get(finalDate);
                strings.add(webhistoryList.get(i));
            } else {
                ArrayList<webhistory> strings = new ArrayList<>();
                strings.add(webhistoryList.get(i));
                dateWiseHistory.put(finalDate, strings);
            }
        }
        return dateWiseHistory;
    }

}
