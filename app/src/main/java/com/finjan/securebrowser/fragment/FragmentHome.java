package com.finjan.securebrowser.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;

import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * main fragment where all logic of browser is written
 */


public class FragmentHome extends ParentHomeFragment {

    private BroadcastReceiver noThankListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setBottomBars(GlobalVariables.getInstnace().currentHomeFragment,true);
        }
    };
    public static final String TAG=FragmentHome.class.getSimpleName();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        GoogleTrackers.Companion.setSHome();
//        LocalyticsTrackers.Companion.setSHome();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        GlobalVariables.getInstnace().homeFragmentInitiated=true;
        GlobalVariables.getInstnace().currentHomeFragment=this;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setBottomBars(FragmentHome.this,true);
            }
        },1000);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void setDefaults() {
        Bundle bundle = getFragmentBundle();
        if (ifBundleHasUrl() && bundle != null) {
            if (bundle.containsKey(AppConstants.BKEY_IS_PRIVATE)) {
                isPrivate = bundle.getBoolean(AppConstants.BKEY_IS_PRIVATE);
            }
            if (bundle.containsKey(AppConstants.BKEY_ID)) {
                id = bundle.getLong(AppConstants.BKEY_ID);
                newTab = false;
                isIdAvailable = true;
            } else {
                isIdAvailable = false;
            }
        }else {
            currentUrl = getBundleUrl();
        }
    }

    @Override
    protected void defaultLoading() {
        setDefaults();
        defaultUrlLoading(getBundleUrl(),false,false);
    }

    private boolean ifBundleHasUrl() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AppConstants.BKEY_BUNDLE)) {
            Bundle bundle1 = bundle.getBundle(AppConstants.BKEY_BUNDLE);
            if (bundle1 != null && bundle1.containsKey(AppConstants.BKEY_URL)) {
                return true;
            }
        }
        return false;
    }

    private String getBundleUrl() {
        String url = null;
        if (ifBundleHasUrl()) {
            url = getArguments().getBundle(AppConstants.BKEY_BUNDLE).getString(AppConstants.BKEY_URL);
        }
        return url == null? "" : url;
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getActivity()).
                registerReceiver(noThankListener, new IntentFilter(AppConstants.LBCAST_BAR_NO_THANKS_CLICK));
//        LocalBroadcastManager.getInstance(getActivity()).
//                registerReceiver(perchaseResponse, new IntentFilter(AppConstants.LBCAST_AppPurchase_connection));

    }

    @Override
    public void onStop() {
        super.onStop();
        GlobalVariables.getInstnace().homeFragmentInitiated=false;
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(noThankListener);
//        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(perchaseResponse);

    }

}
