package com.finjan.securebrowser.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.widget.SwitchCompat;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.finjan.securebrowser.BuildConfig;
import com.finjan.securebrowser.activity.HomeActivity;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.BaseActivity;
import com.finjan.securebrowser.activity.ConfirmPasscode;
import com.finjan.securebrowser.activity.FingerPrintGeneration;
import com.finjan.securebrowser.activity.Finger_Passcode_Activity;
import com.finjan.securebrowser.activity.PasscodeSet;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.DbHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.helpers.vpn_helper.InitializeVpn;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.util.PermissionUtil;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * fragment to show report on click bottom bar after safe scan from browser.
 */



public class FragmentSetting extends ParentFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private View parentView;
    private SwitchCompat tb_vpn_switch,tb_safescan, tb_tracker, tb_audible, tb_private, tb_touchid, tb_passcode,
    tb_vpn_auto_connect;
    private TextView tv_default_search_alert, tv_secs,  tv_touchid;
    private RelativeLayout tracker_layout, rl_audible, rl_rmscan,
            view_touchid, rl_chngepswd,rl_safe_scan,rl_require_password,rl_app_switch,rl_vpn_autoconnect,rl_default_search,
            rl_homepage,rl_private_browsing_data;
    private boolean browse, cache, cookies;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.settings_layout, container, false);
        setReferences(parentView);
        return parentView;
    }

    private void setVersionName(TextView tvVersion){
        tvVersion.setText(String.format(getString(R.string.version),BuildConfig.VERSION_NAME,BuildConfig.VERSION_CODE+""));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        GlobalVariables.getInstnace().fragmentSetting =this;
        setDefaults();
    }
    private TextView tv_tracker_optn_name,tv_txt_safe_Scan,tvUpgradeTxt,tvVpnSwitch,tvVpnAutoconnect,
    tvVersion;
    private void setReferences(View parentView) {
        tv_txt_safe_Scan=(TextView)parentView.findViewById(R.id.tv_txt_safe_scan);
        tvVpnSwitch=(TextView)parentView.findViewById(R.id.tv_vpn_switch_optn);
        tvVpnAutoconnect=(TextView)parentView.findViewById(R.id.tv_vpn_autoconnect);
        tvUpgradeTxt=(TextView)parentView.findViewById(R.id.txt_upgrade);
        tvVersion=(TextView)parentView.findViewById(R.id.tv_version);


        //SwitchCompats

        tb_vpn_switch = (SwitchCompat) parentView.findViewById(R.id.tb_vpn_switch);
        tb_vpn_switch.setOnCheckedChangeListener(this);
        tb_vpn_auto_connect= (SwitchCompat) parentView.findViewById(R.id.tb_vpn_autoconect);
        tb_vpn_auto_connect.setOnCheckedChangeListener(this);
        tb_safescan = (SwitchCompat) parentView.findViewById(R.id.tb_safescan);
        tb_safescan.setOnCheckedChangeListener(this);
        tb_tracker = (SwitchCompat) parentView.findViewById(R.id.tb_tracker);
        tb_tracker.setOnCheckedChangeListener(this);
        tb_audible = (SwitchCompat) parentView.findViewById(R.id.tb_audible);
        tb_audible.setOnCheckedChangeListener(this);
        tb_private = (SwitchCompat) parentView.findViewById(R.id.tb_private);
        tb_private.setOnCheckedChangeListener(this);
        tb_touchid = (SwitchCompat) parentView.findViewById(R.id.tb_touchid);
        tb_touchid.setOnCheckedChangeListener(this);
        tb_passcode = (SwitchCompat) parentView.findViewById(R.id.tb_passcode);
        tb_passcode.setOnCheckedChangeListener(this);


        //textviews
        tvUpgradeTxtBottom = ((TextView) parentView.findViewById(R.id.txt_upgrade_title));
        tv_tracker_optn_name=(TextView)parentView.findViewById(R.id.tv_tracker_optn_name);
        tv_default_search_alert = (TextView) parentView.findViewById(R.id.tv_default_search_alert);
        tv_secs = (TextView) parentView.findViewById(R.id.tv_secs);
        tv_touchid = (TextView) parentView.findViewById(R.id.tv_touchid);

        //RelativeViews
        tracker_layout = (RelativeLayout) parentView.findViewById(R.id.tracker_layout);
        rl_safe_scan=(RelativeLayout)parentView.findViewById(R.id.rl_safe_scan);
        rl_app_switch=(RelativeLayout)parentView.findViewById(R.id.rl_app_switch);
        rl_vpn_autoconnect=(RelativeLayout)parentView.findViewById(R.id.rl_vpn_autoconnect);
        rl_audible = (RelativeLayout) parentView.findViewById(R.id.rl_audible);
        rl_rmscan = (RelativeLayout) parentView.findViewById(R.id.rl_rmscan);
        view_touchid = (RelativeLayout) parentView.findViewById(R.id.view_touchid);
        rl_chngepswd = (RelativeLayout) parentView.findViewById(R.id.rl_chngepswd);
        rl_chngepswd.setVisibility(UserPref.getInstance().getPasscode() ? View.VISIBLE : View.GONE);

        rl_require_password=(RelativeLayout)parentView.findViewById(R.id.rl_require_password);
        rl_default_search=(RelativeLayout)parentView.findViewById(R.id.rl_default_search);
        rl_homepage=(RelativeLayout)parentView.findViewById(R.id.rl_homepage);
        rl_private_browsing_data=(RelativeLayout)parentView.findViewById(R.id.rl_private_browsing_data);

        rl_default_search.setOnClickListener(this);
        rl_homepage.setOnClickListener(this);
        rl_app_switch.setOnClickListener(this);
        rl_private_browsing_data.setOnClickListener(this);
        ((Button) parentView.findViewById(R.id.btn_layout_upgrade1)).setOnClickListener(this);
        rl_audible.setOnClickListener(this);
        tvUpgradeTxt.setOnClickListener(this);
        setMenuNames(parentView);
    }
    private void setViewForAppSwitch(boolean browser){

        if(!browser){
            setAlpha(rl_audible,.45f);
            setAlpha(tracker_layout,.45f);
            setAlpha(rl_default_search,.45f);
            setAlpha(rl_rmscan,.45f);
            setAlpha(rl_homepage,.45f);
            setAlpha(rl_private_browsing_data,.45f);
        }

        rl_audible.setEnabled(browser);
        tracker_layout.setEnabled(browser);
        rl_default_search.setEnabled(browser);
        rl_rmscan.setEnabled(browser);
        rl_homepage.setEnabled(browser);

        rl_private_browsing_data.setEnabled(browser);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!buttonView.isPressed()) {
            return;
        }
        switch (buttonView.getId()) {

            case R.id.tb_vpn_switch:
                onClickAppSwitch(isChecked);
                break;
            case R.id.tb_vpn_autoconect:
                onClickVpnAutoConnect(isChecked,false);
                break;

            case R.id.tb_safescan:
                UserPref.getInstance().setSafeScanEnabled(isChecked);
                if(UserPref.getInstance().getSafeSearchPurchase()){
                    setSafeScanAlpha(isChecked);
                }
                break;
            case R.id.tb_tracker:
                if(isChecked){
                    trackerStatusChanged=true;
                }
                if(!UserPref.getInstance().getTrackerStatusChanged()){
                    UserPref.getInstance().setTrackerStatusChanged(true);
                }
                UserPref.getInstance().setTracker(isChecked);
                UserPref.getInstance().setTrackerHintVisibility(isChecked);
                UserPref.getInstance().setFirstTrackerHintVisibility(!isChecked);
                setTrackerAlpha(isChecked);
                break;
            case R.id.tb_private:
                UserPref.getInstance().setPrivateBrowsing(isChecked);
                setAlpha((RelativeLayout)parentView.findViewById(R.id.rl_private),isChecked?1:.45f);
                break;
            case R.id.tb_audible:
                UserPref.getInstance().setAudable(isChecked);
                setRlAudibleAlpha(isChecked);
                break;
            case R.id.tb_touchid:
                onSwitchTouchId(isChecked);
                break;
            case R.id.tb_passcode:
                onSwitchPasscode(isChecked);
                break;
        }
    }
    private boolean trackerStatusChanged,lastTrackerStatus,isBrowser;

    private void setMenuNames(View parentView){
        tvVpnSwitch.setText(PlistHelper.getInstance().getSMenuAppSwitch());
        tvVpnAutoconnect.setText(PlistHelper.getInstance().getSMenuVPNAutoConnect());
        tv_txt_safe_Scan.setText(PlistHelper.getInstance().getSMenuSafeScan());
        tv_tracker_optn_name.setText(PlistHelper.getInstance().getSMenuTrackingBlocking());
        ((TextView)parentView.findViewById(R.id.tv_audible_title)).setText(PlistHelper.getInstance().getSMenuAudibleAlert());
        ((TextView)parentView.findViewById(R.id.tv_default_search)).setText(PlistHelper.getInstance().getSMenuDefaultSearch());
        ((TextView)parentView.findViewById(R.id.tv_rmscn)).setText(PlistHelper.getInstance().getSMenuScanResultBar());
        ((TextView)parentView.findViewById(R.id.tv_homepage)).setText(PlistHelper.getInstance().getSMenuHomePage());
        ((TextView)parentView.findViewById(R.id.tv_private_browse)).setText(PlistHelper.getInstance().getSMenuClearPrivatedata());
        ((TextView)parentView.findViewById(R.id.txt_changepassword)).setText(PlistHelper.getInstance().getSMenuChangePasscode());
        ((TextView)parentView.findViewById(R.id.tv_require_passcode)).setText(PlistHelper.getInstance().getSMenuRequirePasscode());
        tv_touchid.setText(PlistHelper.getInstance().getSMenuTouchId());
    }



    private void setDefaults() {
        setTrackerOptnName();
        setTvSecs(tv_secs);
        setAlertToRemove(getActivity());
        manageAppPurchaseBar(parentView);
        setDefaultSwitches();
        manageSafeSearchSelection();
        handleTouchIdVisibility();
        handleTouchIdAndPasscodeSymittry();
        setViewForAppSwitch(UserPref.getInstance().getIsBrowser());
        setVersionName(tvVersion);
        isBrowser=UserPref.getInstance().getIsBrowser();
    }
    private void setTrackerOptnName(){
        tv_tracker_optn_name.setText(GlobalVariables.blockTracking?getString(R.string.tracker_blocking):getString(R.string.show_trackers));
    }
    private void handleTouchIdAndPasscodeSymittry(){
        if(UserPref.getInstance().getTouchId() && !UserPref.getInstance().getPasscode()){
           UserPref.getInstance().setTouchId(false);
            tb_touchid.setChecked(false);

        }
    }

    private void setDefaultSwitches() {

        if(!PlistHelper.getInstance().getIsTrackerEnabled()){
            tracker_layout.setVisibility(View.GONE);
        }else {
            tb_tracker.setChecked(UserPref.getInstance().getTracker());
            setTrackerAlpha(UserPref.getInstance().getTracker());
            lastTrackerStatus=UserPref.getInstance().getTracker();
        }

        tb_vpn_switch.setChecked(!UserPref.getInstance().getIsBrowser());
        setAppSwitchAlpha(!UserPref.getInstance().getIsBrowser());

        tb_vpn_auto_connect.setChecked(UserPref.getInstance().getVPNAutoConnect());
        setVpnAutoConnectAlpha(UserPref.getInstance().getVPNAutoConnect());

        tb_safescan.setChecked(UserPref.getInstance().getSafeScanEnabled());
        setSafeScanAlpha(UserPref.getInstance().getSafeScanEnabled());

        tb_private.setChecked(UserPref.getInstance().getPrivateBrowsing());
        setRlPrivateAlpha(UserPref.getInstance().getPrivateBrowsing());

        tb_audible.setChecked(UserPref.getInstance().getAudable());
        setRlAudibleAlpha(UserPref.getInstance().getAudable());

        tb_touchid.setChecked(UserPref.getInstance().getTouchId());
        setRlTouchIdAlpha(UserPref.getInstance().getTouchId());

        tb_passcode.setChecked(UserPref.getInstance().getPasscode());
        setRlPasswordAlpha(UserPref.getInstance().getPasscode());

        if (UserPref.getInstance().getPasscode()) {
            if (rl_chngepswd != null) {
                rl_chngepswd.setVisibility(View.VISIBLE);
            }
        }
    }
    private void setTrackerAlpha(boolean checked){
        setAlpha(tracker_layout,checked?1f:.45f);

    }
    private void setSafeScanAlpha(boolean checked){
        if(tvUpgradeTxt.getVisibility()==View.VISIBLE){
            setAlpha(rl_safe_scan,.45f);

        }else {
//            setAlpha(tv_txt_safe_Scan,.45f);
            setAlpha(rl_safe_scan,checked?1:.45f);

        }
    }
    private void setAppSwitchAlpha(boolean checked){
//        setAlpha(tvVpnSwitch,checked?1:.45f);
        setAlpha(rl_app_switch,checked?1:.45f);

    }
    private void setVpnAutoConnectAlpha(boolean checked){
        setAlpha(rl_vpn_autoconnect,checked?1:.45f);
//        setAlpha(tvVpnAutoconnect,checked?1:.45f);
    }
    private void setAlpha(View view,float value){
        String tag="";
        if(value==1){
            tag="false";
        }else {
            tag="true";
        }
        if(value<1){
            Object o =view.getTag();
            if(view.getTag()!=null && view.getTag()=="false"){
                view.setAlpha(value);
                view.setTag(tag);
            }  else {
                view.setAlpha(value);
                view.setTag(tag);
            }
        }else {
            view.setAlpha(value);
            view.setTag(tag);
        }
    }

    private void setRlPrivateAlpha(boolean isChecked){
        setAlpha((RelativeLayout)parentView.findViewById(R.id.rl_private),isChecked?1:.45f);

    }
    private void setRlPasswordAlpha(boolean isChecked){
        setAlpha(rl_chngepswd,isChecked?1:.45f);
        setAlpha(rl_require_password,isChecked?1:.45f);
    }
    private void setRlAudibleAlpha(boolean isChecked){
        setAlpha(rl_audible,isChecked?1:.45f);

    }
    private void setRlTouchIdAlpha(boolean isChecked){
        setAlpha(view_touchid,isChecked?1:.45f);
    }

    private void handleTouchIdVisibility() {

        if (NativeHelper.getInstnace().checkActivity(getActivity())) {

            if (NativeHelper.getInstnace().isSupportFingurePrint()) {
                if ((ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.USE_FINGERPRINT}, 225);
                    return;
                }
            }
            if (!NativeHelper.getInstnace().isSupportFingurePrint() ||
                    (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED &&
                            ((FingerprintManager) getContext().getSystemService(Context.FINGERPRINT_SERVICE))!=null &&
                            !((FingerprintManager) getContext().getSystemService(Context.FINGERPRINT_SERVICE)).isHardwareDetected())) {
                // Device doesn't support fingerprint authentication
                tv_touchid.setVisibility(View.GONE);
                tb_touchid.setVisibility(View.GONE);
                view_touchid.setVisibility(View.GONE);
                return;
            }
            tv_touchid.setVisibility(View.VISIBLE);
            tb_touchid.setVisibility(View.VISIBLE);
            view_touchid.setVisibility(View.VISIBLE);
        }
    }

    private void setTvSecs(TextView tv_secs) {
        tv_secs.setText(UserPref.getInstance().getRmSecs() == 0 ? getString(R.string.dont_remove) :
                String.valueOf(UserPref.getInstance().getRmSecs()) +" "+ getString(R.string.seconds));
    }

    private void setAlertToRemove(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.remove_scan);
        dialog.setCanceledOnTouchOutside(false);
        TextView btncancel = (TextView) dialog.findViewById(R.id.btncancel);
        LinearLayout ll_1rsmscn = (LinearLayout) dialog.findViewById(R.id.ll_1rsmscn);
        LinearLayout ll_2rsmscn = (LinearLayout) dialog.findViewById(R.id.ll_2rsmscn);
        LinearLayout ll_3rsmscn = (LinearLayout) dialog.findViewById(R.id.ll_3rsmscn);
        LinearLayout ll_4rsmscn = (LinearLayout) dialog.findViewById(R.id.ll_4rsmscn);
        LinearLayout ll_5rsmscn = (LinearLayout) dialog.findViewById(R.id.ll_5rsmscn);
        final RadioButton rb_1rsmscn = (RadioButton) dialog.findViewById(R.id.rb_1rsmscn);
        final RadioButton rb_2rsmscn = (RadioButton) dialog.findViewById(R.id.rb_2rsmscn);
        final RadioButton rb_3rsmscn = (RadioButton) dialog.findViewById(R.id.rb_3rsmscn);
        final RadioButton rb_4rsmscn = (RadioButton) dialog.findViewById(R.id.rb_4rsmscn);
        final RadioButton rb_5rsmscn = (RadioButton) dialog.findViewById(R.id.rb_5rsmscn);

        switch (UserPref.getInstance().getRmSecs()) {
            case 0:
                rb_1rsmscn.setChecked(true);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(false);
                break;
            case 5:
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(true);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(false);
                break;
            case 10:
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(true);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(false);
                break;
            case 15:
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(true);
                rb_5rsmscn.setChecked(false);
                break;
            case 20:
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(true);
                break;
        }

        ll_1rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(true);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(false);
                UserPref.getInstance().setRmSecs(0);
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        ll_2rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(true);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(false);
                UserPref.getInstance().setRmSecs(5);
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        ll_3rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(true);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(false);
                UserPref.getInstance().setRmSecs(10);
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        ll_4rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(true);
                rb_5rsmscn.setChecked(false);
                UserPref.getInstance().setRmSecs(15);
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        ll_5rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(true);
                UserPref.getInstance().setRmSecs(20);
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        rb_1rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(true);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(false);
                UserPref.getInstance().setRmSecs(0);
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        rb_2rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(true);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(false);
                UserPref.getInstance().setRmSecs(5);
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        rb_3rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(true);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(false);
                UserPref.getInstance().setRmSecs(10);
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        rb_4rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(true);
                rb_5rsmscn.setChecked(false);
                UserPref.getInstance().setRmSecs(15);
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        rb_5rsmscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_1rsmscn.setChecked(false);
                rb_2rsmscn.setChecked(false);
                rb_3rsmscn.setChecked(false);
                rb_4rsmscn.setChecked(false);
                rb_5rsmscn.setChecked(true);
                UserPref.getInstance().setRmSecs(20);
                setTvSecs(tv_secs);
                dialog.dismiss();


            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTvSecs(tv_secs);
                dialog.dismiss();
            }
        });
        rl_rmscan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NativeHelper.getInstnace().getScanRemain() <= 0 && !UserPref.getInstance().getSafeSearchPurchase()) {
                    showAppPurchaseScreen();
                } else {
                    dialog.show();
                }
            }
        });
    }

    private TextView tvUpgradeTxtBottom;
    private void manageAppPurchaseBar(View parentView) {
        if (PlistHelper.getInstance().isSafeScanUpgradeEnable()) {


            tvUpgradeTxtBottom.setVisibility(View.VISIBLE);
            parentView.findViewById(R.id.bottomViewSetting).setVisibility(View.VISIBLE);
            if (UserPref.getInstance().getSafeSearchPurchase()) {
                parentView.findViewById(R.id.bottomViewSetting).setVisibility(View.GONE);
                parentView.findViewById(R.id.tb_safescan).setVisibility(View.VISIBLE);
                tvUpgradeTxt.setVisibility(View.GONE);
                tb_audible.setClickable(true);
                tb_audible.setChecked(UserPref.getInstance().getAudable());
            } else {
                int remainDays = NativeHelper.getInstnace().getScanRemain();
                if (remainDays > 0) {
                    String message = getString(R.string.scan_left_message);
                    message = message.replace("$$", "" + remainDays);
                    tvUpgradeTxtBottom.setText(message);
                    parentView.findViewById(R.id.tb_safescan).setVisibility(View.VISIBLE);
                    tvUpgradeTxt.setVisibility(View.GONE);
                    tb_audible.setClickable(true);
                } else {
                    tvUpgradeTxtBottom.setText(getString(R.string.no_longer_protected));
                    parentView.findViewById(R.id.tb_safescan).setVisibility(View.GONE);
                    tvUpgradeTxt.setVisibility(View.VISIBLE);
                    parentView.findViewById(R.id.tb_audible).setClickable(false);
                    tb_audible.setClickable(false);
                }
            }

        } else {
            parentView.findViewById(R.id.bottomViewSetting).setVisibility(View.GONE);
        }

        if(tvUpgradeTxt.getVisibility()==View.VISIBLE){
            setAlpha(tv_txt_safe_Scan,.45f);
        }
    }

    private void showSafeSearchDialog(Context context) {

        if (NativeHelper.getInstnace().checkContext(context)) {

            final Dialog safeDialog = new Dialog(context);
            safeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            safeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            safeDialog.setContentView(R.layout.layout_safety_search);
            safeDialog.setCanceledOnTouchOutside(false);
            ((TextView) safeDialog.findViewById(R.id.btncancel)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    safeDialog.dismiss();
                }
            });
            RadioGroup searchRG = (RadioGroup) safeDialog.findViewById(R.id.rg_safe_search);
            ((RadioButton) safeDialog.findViewById(R.id.rb_safe_search)).setChecked(false);
            ((RadioButton) safeDialog.findViewById(R.id.rb_google)).setChecked(false);
            ((RadioButton) safeDialog.findViewById(R.id.rb_bing)).setChecked(false);
            ((RadioButton) safeDialog.findViewById(R.id.rb_yahoo)).setChecked(false);
            ((RadioButton) safeDialog.findViewById(R.id.rb_aol)).setChecked(false);
            ((RadioButton) safeDialog.findViewById(R.id.rb_ask)).setChecked(false);
            switch (UserPref.getInstance().getScanType()) {
                case 0:
                    ((RadioButton) safeDialog.findViewById(R.id.rb_safe_search)).setChecked(true);
                    break;
                case 1:
                    ((RadioButton) safeDialog.findViewById(R.id.rb_google)).setChecked(true);
                    break;
                case 2:
                    ((RadioButton) safeDialog.findViewById(R.id.rb_bing)).setChecked(true);
                    break;
                case 3:
                    ((RadioButton) safeDialog.findViewById(R.id.rb_yahoo)).setChecked(true);
                    break;
                case 4:
                    ((RadioButton) safeDialog.findViewById(R.id.rb_aol)).setChecked(true);
                    break;
                case 5:
                    ((RadioButton) safeDialog.findViewById(R.id.rb_ask)).setChecked(true);
                    break;
            }
            searchRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int newId = 0;
                    switch (checkedId) {
                        case R.id.rb_safe_search:
                            newId = 0;
                            break;
                        case R.id.rb_google:
                            newId = 1;
                            break;
                        case R.id.rb_bing:
                            newId = 2;
                            break;
                        case R.id.rb_yahoo:
                            newId = 3;
                            break;
                        case R.id.rb_aol:
                            newId = 4;
                            break;
                        case R.id.rb_ask:
                            newId = 5;
                            break;
                    }
                    UserPref.getInstance().setScanType(newId);
                    if(!UserPref.getInstance().getScanTypeChanged()){
                        UserPref.getInstance().setScanTypeChanged(true);
                    }
                    safeDialog.dismiss();
                    manageSafeSearchSelection();
                }
            });
            safeDialog.show();
        }

    }

    private void manageSafeSearchSelection() {
        String searchName = "";
        switch (UserPref.getInstance().getScanType()) {
            case 0:
                searchName = "Safe Search";
                break;
            case 1:
                searchName = "Google";
                break;
            case 2:
                searchName = "Bing";
                break;
            case 3:
                searchName = "Yahoo!";
                break;
            case 4:
                searchName = "AOL";
                break;
            case 5:
                searchName = "Ask";
                break;
        }
        tv_default_search_alert.setText(searchName);
    }
    private void onClickAppSwitch(boolean checked){
        setViewForAppSwitch(!checked);
        setAppSwitchAlpha(checked);
        UserPref.getInstance().setIsBroswer(!checked);
        switchDefaultApp(!checked);
        UserPref.getInstance().setIsTipShown(!(!isBrowser && UserPref.getInstance().getIsBrowser()));
        LocalyticsTrackers.Companion.setBorrowerUser();
    }

    public void onClickVpnAutoConnect(boolean checked,boolean afterPermissionGranted){
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            PermissionUtil.askLocationPermission(getActivity());
        }else {
            if(afterPermissionGranted){
                tb_vpn_auto_connect.setChecked(true);
            }
            setVpnAutoConnectAlpha(checked);
            UserPref.getInstance().setVPNAutoConnect(checked);
            if(checked && !InitializeVpn.getInstance().isServicesInitiated()){
                InitializeVpn.getInstance().setHomeActivity((HomeActivity) HomeActivity.getInstance());
                InitializeVpn.getInstance().initVpnService(HomeActivity.getInstance());
            }
        }
    }

    @Override
    public void onClickParentBack() {
                    super.onClickParentBack();
        if(!UserPref.getInstance().getIsSetUpDone()){
//            if(UserPref.getInstance().getIsBrowser()){
//                ScreenNavigation.getInstance().setHomeScreenInitiated(true);
//                ScreenNavigation.getInstance().callHomeActivity(getContext(),"");
//            }else {
//                ScreenNavigation.getInstance().callVPNScreen(getContext());
//                ScreenNavigation.getInstance().setHomeScreenInitiated(false);
//            }
            UserPref.getInstance().setIsSetUpDone(true);
            getActivity().finish();
            return;
        }
//        if(shouldCallVPN){
//            ScreenNavigation.getInstance().callVPNScreen(getContext());
//        }else {
//            super.onClickParentBack();
//        }
    }

    private void switchDefaultApp(boolean browser){
        appSwitchPopup(browser);
    }

    private void appSwitchPopup(final boolean browser){
        FinjanDialogBuilder builder=new FinjanDialogBuilder(getContext());
        builder.setMessage(browser?PlistHelper.getInstance().getChooseBrowerMsg():
        PlistHelper.getInstance().getChooseVpnMsg())
                .setPositiveButton("Ok")
                .setDialogClickListener(new FinjanDialogBuilder.DialogClickListener() {
                    @Override
                    public void onNegativeClick() {
                    }

                    @Override
                    public void onPositivClick() {
                    }

                    @Override
                    public void onContentClick(String content) {

                    }
                });
        builder.show();
    }

    private void handleClick(View v) {
        switch (v.getId()) {

            case R.id.btn_layout_upgrade1:
                showAppPurchaseScreen();
                break;
            case R.id.txt_upgrade:
                showAppPurchaseScreen();
                break;

            case R.id.rl_default_search:
                showSafeSearchDialog(getActivity());
                break;
            case R.id.tv_homepage_icon:
                showHomePageDialog();
                break;
            case R.id.rl_homepage:
                showHomePageDialog();
                break;
            case R.id.rl_private_browsing_data:
                showPrivateBrowingData();
                break;
            case R.id.tv_browse_icon:
                showPrivateBrowingData();
                break;
            case R.id.rl_audible:
                if (NativeHelper.getInstnace().getScanRemain() <= 0 && !UserPref.getInstance().getSafeSearchPurchase()) {
                    showAppPurchaseScreen();
                }
                break;
            case R.id.rl_chngepswd:
                onClickChangePassword();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        handleClick(v);
    }

    private void onClickChangePassword() {
        ConfirmPasscode.touch_on = false;
        Intent intnet = new Intent(getActivity(), Finger_Passcode_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString("password", "true");
        intnet.putExtras(bundle);
        startActivity(intnet);
    }

    public void showHomePageDialog() {

        String currentURL = GlobalVariables.getInstnace().currentHomeFragment.getCurrentUrl();
        if (TextUtils.isEmpty(currentURL) || currentURL.equalsIgnoreCase(AppConstants.EMPTY_URL)) {
            showToast(getResources().getString(R.string.please_enter_valid_url), getActivity());
            return;
        }
        final Dialog homeDialog = new Dialog(getActivity());
        homeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        homeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        homeDialog.setCanceledOnTouchOutside(false);
        homeDialog.setContentView(R.layout.layout_home_alert_dialog);
        TextView titleText = (TextView) homeDialog.findViewById(R.id.txt_dialg_title);
        TextView clearText = (TextView) homeDialog.findViewById(R.id.txt_dialog_clear_home);
        TextView setTitleText = (TextView) homeDialog.findViewById(R.id.txt_dialog_use_home);
        LinearLayout clearLayout = (LinearLayout) homeDialog.findViewById(R.id.ll_clear_dialog);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            titleText.setMaxLines(2);
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            titleText.setMaxLines(5);
        }
        titleText.setMovementMethod(new ScrollingMovementMethod());
        String message = "";
        if (!UserPref.getInstance().getHomePage().isEmpty()) {
            titleText.setText(getString(R.string.homepage_is_set_to) + " " + UserPref.getInstance().getHomePage());
            clearLayout.setVisibility(View.VISIBLE);
            clearText.setText(getString(R.string.clear_existing_setting));
            setTitleText.setText(getString(R.string.change_to_current_page));
            message = getString(R.string.home_page_change_successfully);
        } else {
            titleText.setText(getString(R.string.homepage_setting));
            clearLayout.setVisibility(View.GONE);
            setTitleText.setText(getString(R.string.use_current_page));
            message = getString(R.string.home_page_add_successfully);
        }
        ((TextView) homeDialog.findViewById(R.id.btncancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeDialog.dismiss();
            }
        });
        ((TextView) homeDialog.findViewById(R.id.txt_dialog_clear_home)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserPref.getInstance().setHomePage("");
//                setHomeIcon(false);
                showToast(getString(R.string.home_page_removed_successfully), getActivity());
                homeDialog.dismiss();
            }
        });
        final String finalMessage = message;
        ((TextView) homeDialog.findViewById(R.id.txt_dialog_use_home)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String currentURL = GlobalVariables.getInstnace().currentHomeFragment.getCurrentUrl();
                if (!TextUtils.isEmpty(currentURL) && Patterns.WEB_URL.matcher(currentURL).matches()) {
                    UserPref.getInstance().setHomePage(DbHelper.getInstnace().getLastUrl());
                    showToast(finalMessage, getActivity());
                } else {
                    showToast(getResources().getString(R.string.please_enter_valid_url), getActivity());
                }
                homeDialog.dismiss();
            }
        });
        homeDialog.show();
    }

    public void showPrivateBrowingData() {
        GoogleTrackers.Companion.setSPrivateDataPref();
//        LocalyticsTrackers.Companion.setSPrivateDataPref();
        browse = cache = cookies = false;
        final Dialog clearDataDialog = new Dialog(getActivity());
        clearDataDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        clearDataDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        clearDataDialog.setContentView(R.layout.layout_clear_browse_data);
        clearDataDialog.setCanceledOnTouchOutside(true);
        ViewGroup browseHistoryRL = (ViewGroup) clearDataDialog.findViewById(R.id.rl_browserHistory);
        ViewGroup cacheRL = (ViewGroup) clearDataDialog.findViewById(R.id.rl_cache);
        ViewGroup cookieRL = (ViewGroup) clearDataDialog.findViewById(R.id.rl_cookie);
        final CheckBox historyCheckBox = (CheckBox) clearDataDialog.findViewById(R.id.chk_browserhistory);
        final CheckBox cacheCheckBox = (CheckBox) clearDataDialog.findViewById(R.id.chk_cache);
        final CheckBox cookieChecKBox = (CheckBox) clearDataDialog.findViewById(R.id.chk_cookie);
        final TextView clearBtn = (TextView) clearDataDialog.findViewById(R.id.btnclear);
        TextView cancelBtn = (TextView) clearDataDialog.findViewById(R.id.btncancel);
        historyCheckBox.setChecked(browse);
        cacheCheckBox.setChecked(cache);
        cookieChecKBox.setChecked(cookies);
        clearBtn.setEnabled(false);

        browseHistoryRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browse = !browse;
                historyCheckBox.setChecked(browse);
                manageClearButton(browse, cache, cookies, clearBtn);
            }
        });
        cacheRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cache = !cache;
                cacheCheckBox.setChecked(cache);
                manageClearButton(browse, cache, cookies, clearBtn);
            }
        });
        cookieRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cookies = !cookies;
                cookieChecKBox.setChecked(cookies);
                manageClearButton(browse, cache, cookies, clearBtn);
            }
        });

        historyCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    browse = true;
                } else {
                    browse = false;
                }
                manageClearButton(browse, cache, cookies, clearBtn);
            }
        });
        cacheCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cache = true;
                } else {
                    cache = false;
                }
                manageClearButton(browse, cache, cookies, clearBtn);
            }
        });
        cookieChecKBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cookies = true;
                } else {
                    cookies = false;
                }
                manageClearButton(browse, cache, cookies, clearBtn);
            }
        });

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (browse || cache || cookies) {
                    confirmClearHistoryDialog(clearDataDialog);
                }

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearDataDialog.dismiss();
            }
        });
        clearDataDialog.show();
    }

    private void manageClearButton(boolean browse, boolean cache, boolean cookies, TextView clearBtn) {
        if (browse || cache || cookies) {
            setAlpha(clearBtn,1f);
            clearBtn.setEnabled(true);
        } else {
            setAlpha(clearBtn,.45f);
            clearBtn.setEnabled(false);
        }
    }

    private void confirmClearHistoryDialog(final Dialog clearDataDialog) {
        new FinjanDialogBuilder(getActivity()).setMessage(getString(R.string.clear_confirm_message))
                .setPositiveButton(getString(R.string.alert_yes)).setNegativeButton(getString(R.string.alert_cancel))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        broadCastClearHistory(browse, cache, cookies);
                        if (clearDataDialog != null) {
                            clearDataDialog.dismiss();
                        }
                    }
                }).show();
    }

    private void broadCastClearHistory(boolean browse, boolean cache, boolean cookies) {

        String whichDataCleared = "Browser ";
        String[] records = new String[3];
        if (browse) {
            ((BaseActivity) getActivity()).clearAllHistory();
            records[0] = "history";
        }
        if (cache) {
            ((BaseActivity) getActivity()).clearCashe();
            records[1] = "cache";
        }
        if (cookies) {
            ((BaseActivity) getActivity()).clearCookies();
            records[2] = "cookies";
        }
        Toast.makeText(getActivity(), getString(R.string.selected_records_cleared), Toast.LENGTH_SHORT).show();
    }

    private void onSwitchPasscode(boolean isChecked) {
        setRlPasswordAlpha(isChecked);
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {

            String lockCode = UserPref.getInstance().getLockCode();
            if (isChecked) {
                if (TextUtils.isEmpty(lockCode) || lockCode.trim().length() == 0) {
                    ConfirmPasscode.touch_on = false;
                    getActivity().startActivityForResult(new Intent(getActivity(), PasscodeSet.class),AppConstants.AFR_Password_set);
                } else if (lockCode.trim().length() > 0) {
                    showToast(getString(R.string.earlier_password_is_active));
                    tb_passcode.setChecked(true);
                    UserPref.getInstance().setPasscode(true);
                    rl_chngepswd.setVisibility(View.VISIBLE);
                }
                return;
            }
                tb_passcode.setChecked(false);
                rl_chngepswd.setVisibility(View.GONE);
                UserPref.getInstance().setPasscode(isChecked);
            if(tb_touchid.isChecked()){
                tb_touchid.setChecked(false);
                UserPref.getInstance().setTouchId(false);
            }
        }
    }

    public void setPasscordOnResult(boolean success){
        UserPref.getInstance().setPasscode(success);
        tb_passcode.setChecked(success);
    }
    public void setTouchIdOnResult(boolean success){

        UserPref.getInstance().setTouchId(success);
        if(success){
            setPasscordOnResult(true);
            tb_touchid.setChecked(true);
            return;
        }
        tb_touchid.setChecked(false);
    }
    private void onSwitchTouchId(boolean isChecked) {

//        if (NativeHelper.getInstnace().isSupportFingurePrint() && ActivityCompat.checkSelfPermission(getActivity(),
//                Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.USE_FINGERPRINT}, 225);
//            return;
//        }

        setRlTouchIdAlpha(isChecked);
        if(isChecked){
            if (UserPref.getInstance().getPasscode() ||
                    (!TextUtils.isEmpty(UserPref.getInstance().getLockCode())) && UserPref.getInstance().getLockCode().length() == 4) {

                getActivity().startActivityForResult(new Intent(getActivity(), FingerPrintGeneration.class), AppConstants.AFR_REGISTER_FINGER);
                return;
            }
            Intent intent=new Intent(FragmentSetting.this.getActivity(),PasscodeSet.class);
            intent.putExtra(AppConstants.IKEY_ENABLE_TOUCH_ID,true);
            getActivity().startActivityForResult(intent,AppConstants.AFR_Password_set);
            return;
        }

        tb_touchid.setChecked(false);
        UserPref.getInstance().setTouchId(false);
        onSwitchPasscode(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(trackerStatusChanged && !lastTrackerStatus){
            GlobalVariables.getInstnace().shouldPageReload=true;
        }else {
            GlobalVariables.getInstnace().shouldPageReload=false;
        }
        GlobalVariables.getInstnace().appSwitchSettingChanged=!(isBrowser && UserPref.getInstance().getIsBrowser());
    }
}