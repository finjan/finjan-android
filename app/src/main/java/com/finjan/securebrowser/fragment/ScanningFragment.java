package com.finjan.securebrowser.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.finjan.securebrowser.R;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class ScanningFragment extends ParentFragment {

    private View parentView;
    private VideoView scanningVideo;
    private RelativeLayout cancelScanContainer;
    private LinearLayout cancelButton;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentView=inflater.inflate(R.layout.scanning_view,container,false);
        setRefernces(parentView);
        return parentView;
    }

    private void setRefernces(View parentView) {
        scanningVideo=(VideoView)parentView.findViewById(R.id.scanningVideo);
        cancelScanContainer=(RelativeLayout)parentView.findViewById(R.id.cancelScanContainer);
        cancelButton=(LinearLayout) parentView.findViewById(R.id.cancelButton);
    }

}
