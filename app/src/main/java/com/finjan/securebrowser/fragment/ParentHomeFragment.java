package com.finjan.securebrowser.fragment;

import android.Manifest;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.NotificationCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.finjan.securebrowser.AppLog;
import com.finjan.securebrowser.Constants;

import com.finjan.securebrowser.custom.VideoEnabledWebView;
import com.finjan.securebrowser.custom.WebVideoHelper;
import com.finjan.securebrowser.DisplayWebView;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.UrlAdapter;
import com.finjan.securebrowser.UrlEditText;
import com.finjan.securebrowser.UrlPojo;
import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.activity.HomeActivity;
import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.eventbus_pojo.UploadingMsg;
import com.finjan.securebrowser.helpers.AnimationHelper;
import com.finjan.securebrowser.helpers.DbHelper;
import com.finjan.securebrowser.helpers.DisplayHelper;
import com.finjan.securebrowser.helpers.DownloadHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.SearchHelper;
import com.finjan.securebrowser.helpers.TipScreenHelper;
import com.finjan.securebrowser.helpers.TrackerHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.helpers.vpn_helper.InitializeVpn;
import com.finjan.securebrowser.model.BingSearchResponse;
import com.finjan.securebrowser.model.SafetyResult;
import com.finjan.securebrowser.model.SearchResult;
import com.finjan.securebrowser.model.TrackerFoundModel;
import com.finjan.securebrowser.service.BingRestClient;
import com.finjan.securebrowser.service.BingService;
import com.finjan.securebrowser.service.SafetyRestClient;
import com.finjan.securebrowser.service.SafetyService;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.tracking.tune.TuneTracking;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.util.IabBroadcastReceiver;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.eventbus.VpnStatusEvent;
import com.finjan.securebrowser.vpn.ui.iab.UpgradeVpnActivity;
import com.finjan.securebrowser.vpn.util.PermissionUtil;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.VpnUtil;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.blinkt.openvpn.core.VpnStatus;
import de.greenrobot.event.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import webHistoryDatabase.Tracker;
import webHistoryDatabase.bookmarks;
import webHistoryDatabase.tabhistory;
import webHistoryDatabase.webhistory;
import webHistoryDatabase.webhistoryDao;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 * <p>
 * main browser fragment
 */

public abstract class ParentHomeFragment extends ParentFragment implements DisplayWebView, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener,
        IabBroadcastReceiver.IabBroadcastListener, VpnStatus.ByteCountListener {

    private static final int NOTIFICATION_ID = 01;
    private static final String TAG = ParentHomeFragment.class.getSimpleName();
    public Handler safetyHandler = new Handler();
    /**
     * tells weather this tab is private or not
     */
    public boolean isPrivate;
    public String currentUrl = "";
    public boolean isNavigationPresssed;
    public boolean barGoneClicked;
    /**
     * isTrackerReportAvailable shows that is there time to show Tracker FAB
     */
    public boolean isTrackerReportAvailable;
    /**
     * Sets the report card to match the supplied SafetyResult
     *
     * @param safetyResult
     */

    public boolean isReportAvailable;
    public Long id = null;
    public boolean isSafetyResultFetching;
    public boolean vtScan = false;
    public ValueCallback<Uri[]> mFilePathCallback;
    public ValueCallback<Uri> mFilePathCallbackPre;
    protected boolean isIdAvailable;
    protected View parentView;
    protected String backWardUrl, forwarUrl;
    protected int backwardIndex = -1;
    protected int forwardIndex = 1;
    protected VideoEnabledWebView urlWebView;
    protected ViewGroup container;
    boolean sameDomain = false;
    //tell weather is tab is New Tab or old one.
    boolean newTab = true;
    //weather is current status of screen is up or down scroll
    boolean isUpScrolling = false;
    boolean downScroll = false;
    private List<UrlPojo> mList;
    private SafetyRestClient safetyRestClient = new SafetyRestClient();
    final SafetyService safetyService = safetyRestClient.getSafetyService();
    private ViewGroup home,
            header, webViewControls,
            scanningViewContainer, cancelScanContainer, cancelButton;
    private VideoView scanningVideo;
    private UrlEditText urlEditText;
    private ImageView backButton, forwardButton;
    private ImageView iv_refresh, iv_menu_icon;
    //    private TextView tv_url;
    private LinearLayout ll_urlview;
    private RelativeLayout rl_urlview, webViewContainer;
    private TextView btn_cancel;
    //Report controls
    private List<SearchResult> searchResults = new ArrayList<>();
    private String currentSearchTerm;
    private String firstUrl;
    private ImageView searchIcon;
    private ImageView iv_tabsopened, iv_vpn;
    private SafetyResult currentSafetyResult;
    private int reportColor = -1;
    private int reportIndex;
    private int counter_url = 0;
    private String mydomainame = "";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    private ImageView urlCleaner;
    private String urlSeprater = "URL_SEPRATOR";
    private ProgressBar progressbar1, progressbar2;
    private TextView headerCover, txt_webloadError, txt_tabCount;
    private boolean isFailour;
    private ArrayList<TrackerFoundModel> trackerFoundList = new ArrayList<>();
    private String urlTitle = "";
    private CookieSyncManager privateCookie, normalCookie;
    private CookieManager privateCookieManager, normalCookieManager;
    private NotificationManager notificationmanager;
    private int index = 0;
    //changes by ~Anurag
    private int mParallaxImageHeight, drawerBottomBarHeight;
    private int mScrollY = 0; //Keeps track of our scroll.
    private boolean isScrollEndPointReached;
    private boolean mIsToolbarShown = false;
    private int mToolbarHeight;
    //    private TextView tv_url_new;
    //    private ObservableScrollView custom_scrollview;
    private boolean isDefaultDataLoaded;
    /**
     * method where we should make SearchviewFragment able to show result
     */
    private SearchedResultFragment searchedResultFragment;
    private boolean urlHeaderShowing = true;
    private Handler trackerHandler = new Handler();
    private boolean isPagetRedirected;
    private boolean warningFromNavigation;
    private boolean isUrlInEditForm;
    private boolean canBeEditNow;
    private boolean upgradeDialogShowed;
    private TrackerHelper trackerHelper;
    private TextView tvTrackerCount, tvTrackerCountHint;
    private Runnable trackerRunnable = new Runnable() {
        @Override
        public void run() {
//            if (isPagetRedirected) {
//                trackerHandler.postDelayed(trackerRunnable, 200);
//                return;
//            }
            showTracker(urlWebView.getUrl());
        }
    };
    private ImageView iv_menu_bookmark, iv_menu_home, iv_menu_vpn;
    private RelativeLayout rl_menu_tracker;
    private LinearLayout view_drawer_bottom;
    private int oldMScrolly;
    private int displayHeight;
    private WebVideoHelper videoHelpder;
    private FragmentWarning.WarningCallback warningCallback = new FragmentWarning.WarningCallback() {
        @Override
        public void continuePage(boolean continuepage) {
            if (continuepage) {
                continueLoading();
            } else {
                setWarningBackPress();
            }
        }
    };

    public void clearCaches() {
        if (urlWebView != null) {
            urlWebView.clearCache(true);
        }
    }

    public void clearHistory() {
        urlWebView.clearHistory();
    }

    public Long getWebviewId() {
        return id;
    }

    public String getWebviewUrl() {
        return urlWebView.getUrl();
    }

    private void continueLoading() {
        if (currentSafetyResult != null) {
            setReportResponse(currentSafetyResult);
            loadUrl(true, currentSafetyResult.getUrl());
        }
    }

    public void makeDownScrollFalse() {
        downScroll = false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (isDefaultDataLoaded) {
            return parentView;
        }
        if (!UserPref.getInstance().getUserVisitsBrowser()) {
            UserPref.getInstance().setUserVisitsBrowser(true);
            LocalyticsTrackers.Companion.setEBrowser_Page_First_Time();
        }
        LocalyticsTrackers.Companion.setBrowserPage();
        parentView = inflater.inflate(R.layout.fragment_browser, container, false);
        setReferences(parentView);
        setViewListeners();
        setBackgroundImage();
        return parentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Logger.logE(TAG, "onActivityCreated");
        setData();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.logE(TAG, "onCreate");
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
        if (parentView != null) {
            ViewGroup parent = (ViewGroup) parentView.getParent();
            if (parent != null) {
                parent.removeAllViews();
            }
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final VpnStatusEvent event) {
        if (isAdded() && isVisible() && event.getStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTED) {
            setVpnIcon();
        }
    }

    @Override
    protected void setHomeIcon(boolean isSet) {
        iv_menu_home.setImageDrawable(isSet ? ResourceHelper.getInstance().getDrawable(R.drawable.tab_home_on)
                : ResourceHelper.getInstance().getDrawable(R.drawable.menu_home));
    }

    public void drawerStatusChanged(boolean showing) {
        if (NativeHelper.getInstnace().checkContext(getContext())) {
            iv_menu_home.setAlpha(showing ? .2f : 1);
            iv_menu_home.setEnabled(!showing);

            iv_menu_vpn.setAlpha(showing ? .2f : 1);
            iv_menu_vpn.setEnabled(!showing);

            iv_menu_bookmark.setAlpha(showing ? .2f : 1);
            iv_menu_bookmark.setEnabled(!showing);

            if (PlistHelper.getInstance().getIsTrackerEnabled()) {
                rl_menu_tracker.setAlpha(showing ? .2f : 1);
                rl_menu_tracker.setEnabled(!showing);
                rl_menu_tracker.setClickable(true);

            } else {
                rl_menu_tracker.setAlpha(.5f);
                rl_menu_tracker.setClickable(false);
            }

            Drawable menuDrawable = showing ? ResourceHelper.getInstance().getDrawable(R.drawable.close) :
                    ResourceHelper.getInstance().getDrawable(R.drawable.menu);
            if (menuDrawable != null) {
                iv_menu_icon.setImageDrawable(menuDrawable);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void setViewListeners() {
        urlEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!canBeEditNow || isLoaderVisible()) {
                    if (urlEditText.hasFocus()) {
                        urlEditText.clearFocus();
                    }
                    return;
                }
                if (urlEditText.hasFocus()) {
                    makeUrlForEditing();
//                    iv_refresh.setVisibility(View.GONE);
                } else {
                    urlCleaner.setVisibility(View.GONE);
                }
            }
        });

        urlWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.urlWebView) {
                    hideDrawer();
                }
                return false;
            }
        });


        scanningVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            // video started; hide the placeholder.
                            scanningVideo.setBackgroundResource(0);
                            return true;
                        }
                        return false;
                    }
                });
            }
        });
        urlEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!canBeEditNow || isLoaderVisible()) {
                    if (urlEditText.hasFocus()) {
                        urlEditText.clearFocus();
                    }
                    return;
                }
                vtScan = false;
                if (hasFocus) {
                    GlobalVariables.getInstnace().isKeyboardShowing = true;
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            GlobalVariables.getInstnace().isKeyboardShowing = false;
                        }
                    }, 500);
                }
                isUrlInEditForm = hasFocus;
                if (!hasFocus) {
                    onUrlFocusChange(v);
                } else {
                    makeUrlForEditing();
                }
            }
        });
        urlEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                setHomeIcon(s.length() > 0);
                if (s.length() > 0) {
                    if (urlEditText.hasFocus()) {
                        urlCleaner.setVisibility(View.VISIBLE);
                    }
                } else {
                    urlCleaner.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        urlEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mList != null && mList.size() > 0) {
                    isNavigationPresssed = false;
                    omnibarResults(mList.get(position).getUrl());
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vtScan = false;
                setBottomBars((FragmentHome) ParentHomeFragment.this, true);
                urlEditText.clearFocus();
                refreshTabCount();
            }

        });
        home.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && TextUtils.isEmpty(urlEditText.getText().toString())) {
                    showLastUrl();
                }
            }
        });
        if (!AppConfig.Companion.getUpAndDownBarShowing()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                urlWebView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                    @Override
                    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                        Log.e("y", "" + scrollY);
                        Log.e("oldy", "" + oldScrollY);


                        if ((mScrollY <= mParallaxImageHeight /*&& mScrollY >=0*/ && scrollY > oldScrollY) || (/*mScrollY<=mParallaxImageHeight &&*/ mScrollY > 0 && scrollY < oldScrollY)) {
                            mScrollY = mScrollY + scrollY - oldScrollY;
                            if (mScrollY > 0 && mScrollY <= mParallaxImageHeight) {

                                header.setY(-mScrollY);
                                view_drawer_bottom.setTranslationY(mScrollY);

                            } else if (scrollY > oldScrollY) {
                                mScrollY = mParallaxImageHeight;
//
                                header.setY(-mScrollY);
                                view_drawer_bottom.setTranslationY(mScrollY);

                            } else if (scrollY < oldScrollY) {

                                mScrollY = 0;
                                header.setY(mScrollY);
                                view_drawer_bottom.setTranslationY(mScrollY);

                            }
                        }
                        if (scrollY > oldScrollY) {

                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                            layoutParams.setMargins(0, 0, 0, 0);
                            webViewContainer.setLayoutParams(layoutParams);


                        } else if (scrollY < oldScrollY) {
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                            layoutParams.setMargins(0, 0, 0, -mScrollY);
                            webViewContainer.setLayoutParams(layoutParams);


                        }


                    }
                });
            }
        }else {

            urlWebView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
                @Override
                public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
                    if (!GlobalVariables.getInstnace().orientationIsJustChanged) {
                        hideDrawer();
                        hideKeyboard(parentView);
                    }
                    mScrollY = scrollY;


                    // Grab the last child placed in the ScrollView, we need it to determinate the bottom position.
//                View view = (View) custom_scrollview.getChildAt(0);

                    // Calculate the scrolldiff
                    int diff = (urlWebView.getBottom() - (urlWebView.getHeight() + urlWebView.getScrollY()));

                    // if diff is zero, then the bottom has been reached
                    isScrollEndPointReached = diff == 0;
                    if (diff == 0) {
                        // notify that we have reached the bottom
                        Logger.logE(TAG, "webview:Bottom has been reached");
                    }

//                AnimationHelper.getInstance().moveWebview(getContext(),mScrollY-scrollY,
//                        mScrollY-scrollY<0,urlWebView,header,mParallaxImageHeight);


                    if (mScrollY < 150) {
                        setBottomBars((FragmentHome) ParentHomeFragment.this, false);
                    }
                }

                @Override
                public void onDownMotionEvent() {
//                Logger.logE("Anurag", "onDownMotionEvent");
                }

                @Override
                public void onUpOrCancelMotionEvent(ScrollState scrollState) {

//                If we're scrolling up, and are too far away from toolbar, hide it:
                    if (scrollState == ScrollState.UP) {
//                    if (mScrollY > mParallaxImageHeight) {
                        if (urlHeaderShowing) {
                            hideUrlHeader();
                        }
//                    }

                        if (urlEditText.hasFocus()) {
                            urlEditText.clearFocus();
                        }
                        isUpScrolling = true;
                        downScroll = false;
                    } else if (scrollState == ScrollState.DOWN) {
                        //Show toolbar as fast as we're starting to scroll down
                        if (!urlHeaderShowing) {
                            showHeader();
                        }
                        isUpScrolling = false;
                        downScroll = true;
                    }
                    setBottomBars((FragmentHome) ParentHomeFragment.this, false);
                }
            });
        }
    }

    protected void onClickSearchUrl(final String keyward) {

        isReportAvailable = GlobalVariables.getInstnace().tempIsReportAvailable;
        isTrackerReportAvailable = GlobalVariables.getInstnace().tempIsTrackerAvailable;
        if (GlobalVariables.getInstnace().tempSafetyResult != null) {
            try {
                currentSafetyResult = GlobalVariables.getInstnace().tempSafetyResult.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                currentSafetyResult = GlobalVariables.getInstnace().tempSafetyResult;
            }
        }

        if (GlobalVariables.getInstnace().tempTrackerFoundList != null && trackerHelper != null) {
            trackerHelper.makeCloneOf(GlobalVariables.getInstnace().tempTrackerFoundList);
        }
        GlobalVariables.getInstnace().tempTrackerFoundList = null;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                urlEditText.requestFocus();
                NativeHelper.getInstnace().showKeyBoard(ParentHomeFragment.this.getActivity());
                setUrlEditText(keyward, true);
                urlEditText.setSelection(TextUtils.isEmpty(keyward) ? 0 : keyward.length());
            }
        }, 500);
    }

    public String getCurrentUrl() {
        String url = "";
        if (urlWebView != null) {
            url = urlWebView.getUrl();
        }
        return url;
    }

    private void defaultToRefreshScreen() {
        GlobalVariables.getInstnace().currentHomeFragment = (FragmentHome) this;
        refreshTabCount();
        if (isTrackerReportAvailable && !TextUtils.isEmpty(urlWebView.getUrl()) &&
                !urlWebView.getUrl().equals(AppConstants.EMPTY_URL)) {
            hintVisibility();
        }
        setBookMark(ifBookMark());
        setTheme();
        setUrlEditText(urlWebView.getUrl(), false);
        setVpnIcon();
        setHomeIcon(urlEditText != null && urlEditText.getText().toString().trim().length() > 0
                && urlEditText.getText().toString().equals(UserPref.getInstance().getHomePage()));
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        setBottomBars((FragmentHome) ParentHomeFragment.this, true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            container = (ViewGroup) getView().findViewById(R.id.rl_container);
//            setAnim(container,LayoutTransition.APPEARING);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isSafetyResultFetching) {
                    showLoader();
                }
            }
        }, 1000);

    }

    private void setData() {
        defaultToRefreshScreen();
        if (isDefaultDataLoaded && !GlobalVariables.getInstnace().isHomeActivityCalled) {
            setBottomBars((FragmentHome) ParentHomeFragment.this, true);

            return;
        }
        setDefaults();
        defaultLoading();
        isDefaultDataLoaded = true;
        GlobalVariables.getInstnace().isHomeActivityCalled = false;
    }

    private void setDefaultHeaderPosition() {

        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen._50sdp);
        drawerBottomBarHeight = DisplayHelper.getInstance().getDrawerBottomBar(getContext());
        mToolbarHeight = getResources().getDimensionPixelSize(R.dimen._20sdp);

        final AnimatorSet animatorSet2 = AnimationHelper.getInstance().buildAnimationSet(getActivity(), 250,
                AnimationHelper.getInstance().buildAnimation(urlWebView, 0, mParallaxImageHeight));
        animatorSet2.start();

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(0, 0, 0, drawerBottomBarHeight);
        webViewContainer.setLayoutParams(layoutParams);
    }

    private void setDefaults() {
        urlDataSet();
        defaultUrlLayout();
        setupWebView();
        setDefaultHeaderPosition();
        startCookie();
        showOptionalMsg();
        GoogleTrackers.Companion.setSBrowser();
//        LocalyticsTrackers.Companion.setSBrowser();
    }

    private void setTrackerVisibility() {
        tvTrackerCount.setVisibility(UserPref.getInstance().getTracker() ? View.VISIBLE : View.GONE);
    }

    private void defaultUrlLayout() {
        Logger.logE(TAG, "defaultUrlLayout");

        if (isUrlInEditForm) {
            return;
        }
        RelativeLayout.LayoutParams llpm = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        llpm.addRule(RelativeLayout.START_OF, R.id.iv_vpn);
        llpm.addRule(RelativeLayout.END_OF, R.id.webViewControls);
        llpm.setMargins(0, 15, 15, 15); // llp.setMargins(left, top, right, bottom);
        ll_urlview.setLayoutParams(llpm);
    }

    protected abstract void defaultLoading();

    protected void defaultUrlLoading(String url, boolean fromHistory, boolean fromWarning) {

        if (fromHistory) {
            if (!TextUtils.isEmpty(url)) {
                currentUrl = url;
            } else {
                setUrlEditText("", false);
            }

        } else {
            if (!TextUtils.isEmpty(url)) {
                if (url.equalsIgnoreCase(AppConstants.EMPTY_URL)) {
//                    isPrivate = false;
                    id = null;

                    if (!TextUtils.isEmpty(UserPref.getInstance().getHomePage())) {
                        currentUrl = UserPref.getInstance().getHomePage();

                    } else if (!TextUtils.isEmpty(PlistHelper.getInstance().getHomePage())) {
                        currentUrl = PlistHelper.getInstance().getHomePage();
                    } else {
                        currentUrl = AppConstants.EMPTY_URL;
                    }
                } else {
                    currentUrl = url;
                }
            } else {
                if (!TextUtils.isEmpty(GlobalVariables.getInstnace().lastDirectUrl)) {
                    id = null;
                    currentUrl = GlobalVariables.getInstnace().lastDirectUrl;
                    GlobalVariables.getInstnace().lastDirectUrl = "";
                    GlobalVariables.getInstnace().lastDirectConsumedUrl = currentUrl;
//                    isPrivate = false;
                } else {
                    List<tabhistory> tabhistories = DbHelper.getInstnace().getTabList();
                    Collections.reverse(tabhistories);
                    if (tabhistories.size() > 0) {
                        tabhistory tabhistory = tabhistories.get(0);
                        id = tabhistory.getId();
                        currentUrl = tabhistory.getUrl();
                        isPrivate = (tabhistory.getType() != null && tabhistory.getType().equalsIgnoreCase(AppConstants.PRIVATE));
                        newTab = false;

                        if (TextUtils.isEmpty(currentUrl)) {

                            if (!TextUtils.isEmpty(UserPref.getInstance().getHomePage())) {
                                currentUrl = UserPref.getInstance().getHomePage();

                            } else if (!TextUtils.isEmpty(PlistHelper.getInstance().getHomePage())) {
                                currentUrl = PlistHelper.getInstance().getHomePage();
                            }

                        }

                    } else {

//                        isPrivate = false;
                        id = null;

                        if (!TextUtils.isEmpty(UserPref.getInstance().getHomePage())) {
                            currentUrl = UserPref.getInstance().getHomePage();

                        } else if (!TextUtils.isEmpty(PlistHelper.getInstance().getHomePage())) {
                            currentUrl = PlistHelper.getInstance().getHomePage();
                        } else {
                            currentUrl = AppConstants.EMPTY_URL;
                        }


                    }
                }
            }
        }
        setBackForwordClick(false, false);
        isNavigationPresssed = false;
        omnibarResults(currentUrl);
        defaultToRefreshScreen();
    }

    private String getUrlAtIndex(int index) {
        try {
            List<tabhistory> tabhistories = AnalyticsApplication.daoSession.getTabhistoryDao().loadAll();
            if (tabhistories.size() > index) {
                Collections.reverse(tabhistories);
                return tabhistories.get(index).getUrl();
//                tabloadhistory tempData = UrlScannerHelper.getInstance().getTabLoadData(tabhistories.get(index).getRegion(), null);
//                if (tempData != null) {
//                    return tempData.getCurrenturl();
//                }
            }
        } catch (Exception e) {
        }
        return "";
    }

    private void startCookie() {
        CookieSyncManager.createInstance(getActivity());
        privateCookie = CookieSyncManager.getInstance();
        normalCookie = CookieSyncManager.getInstance();
        privateCookieManager = CookieManager.getInstance();
        normalCookieManager = CookieManager.getInstance();

        if (isPrivate) {
            privateCookie.startSync();
            privateCookieManager.setAcceptCookie(true);
        } else {
            normalCookie.startSync();
            normalCookieManager.setAcceptCookie(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            privateCookieManager.setAcceptThirdPartyCookies(urlWebView, true);
            normalCookieManager.setAcceptThirdPartyCookies(urlWebView, true);
        }
    }

    private int getTabsCount() {
        return AnalyticsApplication.daoSession.getTabhistoryDao().loadAll().size();
    }

    private void showTabOverSizeDialog() {
        String message = getString(R.string.tab_oversize_message_1) + " " +
                PlistHelper.getInstance().getMaxTabCount() + " " + getString(R.string.tab_oversize_message_2);

        new FinjanDialogBuilder(getActivity()).setMessage(message).show();
    }

    private void showLastUrl() {
        List<tabhistory> tempTabhistories = AnalyticsApplication.daoSession.getTabhistoryDao().loadAll();
        if (tempTabhistories != null && tempTabhistories.size() > 0 && !newTab) {
            Collections.reverse(tempTabhistories);
            setUrlEditText(tempTabhistories.get(0).getUrl(), false);
        }
    }

    public void setVpnIcon() {

        if (FinjanVPNApplication.getInstance() != null &&
                VpnUtil.isVpnActive(FinjanVPNApplication.getInstance().getApplicationContext())) {
            iv_vpn.setImageDrawable(InitializeVpn.getInstance().getFlag(getActivity()));
            return;
        }
        iv_vpn.setImageDrawable(ResourceHelper.getInstance().getDrawable(R.drawable.tab_vpn_off));
    }

    private void onClickTrackHint() {
        UserPref.getInstance().setFirstTrackerHintVisibility(true);
        hintVisibility();
    }

    private void reSacanURL() {
        String url = getCurrentUrl();
        if (!TextUtils.isEmpty(url) && !url.equalsIgnoreCase(AppConstants.EMPTY_URL)) {
            if (UserPref.getInstance().getSafeSearchPurchase() ||
                    NativeHelper.getInstnace().getScanRemain() > 0) {
                showVirusCheckAlertDialog();
            } else if (NativeHelper.getInstnace().getScanRemain() <= 0) {
                showAlertForceUpgrade();
            } else {
                showAlertForceUpgrade();
            }
            return;
        }
        Toast.makeText(getContext(), getString(R.string.please_enter_valid_url), Toast.LENGTH_SHORT).show();
    }

    public void onvtclick() {
        GoogleTrackers.Companion.setEVirus_Scan_Button();
//        LocalyticsTrackers.Companion.setEVirus_Scan_Button();
        if (!UserPref.getInstance().getSafeScanEnabled()) {
            TipScreenHelper.getDefaultSafeScanAlert(ParentHomeFragment.this.getContext(), new TipScreenHelper.SafeScanTurnOnLisnter() {
                @Override
                public void onSafeScanTurnOn() {
                    reSacanURL();
                }
            });
        } else {
            reSacanURL();
        }

    }

    public void onBookmarkPage() {
        GoogleTrackers.Companion.setEBookmark_Button();
//        LocalyticsTrackers.Companion.setEBookmark_Button();
        if (TextUtils.isEmpty(getCurrentUrl())) {
            Toast.makeText(getContext(), getString(R.string.please_wait_while_page_is_loading), Toast.LENGTH_SHORT).show();
            return;
        }
        if (isPrivate) {
            if (isCurrenPageBookMarked()) {
                DbHelper.getInstnace().deleteBookMark(GlobalVariables.getInstnace().currentHomeFragment.getCurrentUrl());
                Toast.makeText(getContext(), getString(R.string.bookmark_deleted_successfully), Toast.LENGTH_SHORT).show();
                setBookMark
                        (false);
            } else {
                showBookmarkWithPriavteTabDialog();
            }
        } else {
            checkToAddOrDelBookMark();
        }
    }

    private void checkToAddOrDelBookMark() {
        if (isCurrenPageBookMarked()) {
            DbHelper.getInstnace().deleteBookMark(GlobalVariables.getInstnace().currentHomeFragment.getCurrentUrl());
            Toast.makeText(getContext(), getString(R.string.bookmark_deleted_successfully), Toast.LENGTH_SHORT).show();
            setBookMark(false);

        } else {
            DbHelper.getInstnace().createBookMark(GlobalVariables.getInstnace().currentHomeFragment.getCurrentUrl());
            Toast.makeText(getContext(), getString(R.string.bookmark_added_successfully), Toast.LENGTH_SHORT).show();
            setBookMark(true);
        }
    }

    private void showBookmarkWithPriavteTabDialog() {
        new FinjanDialogBuilder(getContext())
                .setMessage(getString(R.string.you_are_in_a_private_tab_Are_you_sure_that_you_want_to_save_this_bookmark))
                .setPositiveButton(getString(R.string.alert_yes))
                .setNegativeButton(getString(R.string.alert_no))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        DbHelper.getInstnace().createBookMark(GlobalVariables.getInstnace().currentHomeFragment.currentUrl);
                        Toast.makeText(getContext(), getString(R.string.bookmark_added_successfully), Toast.LENGTH_SHORT).show();
                        setBookMark(true);
                    }
                }).show();
    }

    public void setBookMark(boolean isBookMark) {
        if (NativeHelper.getInstnace().checkContext(getContext())) {
            Drawable drawable = ResourceHelper.getInstance().getDrawable(R.drawable.tab_bookmark_off_1);
            if (isBookMark) {
                iv_menu_bookmark.setImageDrawable(drawable);
//            iv_bookmark_land.setImageDrawable(drawable);
                iv_menu_bookmark.setTag(true);
//            iv_bookmark_land.setTag(true);
                return;
            }
            drawable = ResourceHelper.getInstance().getDrawable(R.drawable.tab_bookmark_off);
            iv_menu_bookmark.setImageDrawable(drawable);
//        iv_bookmark_land.setImageDrawable(drawable);
            iv_menu_bookmark.setTag(false);
//        iv_bookmark_land.setTag(false);
        }
    }

    private boolean isCurrenPageBookMarked() {
        if (iv_menu_bookmark.getTag() != null && iv_menu_bookmark.getTag().equals(true)) {
            return true;
        }
//        if (isLandscapedOrientation()) {
//            if(iv_bookmark_land.getTag()!=null && iv_bookmark_land.getTag().equals(true)){
//                return true;
//            }
//        }
        return false;
    }

    private void temp() {
        if (com.finjan.securebrowser.BuildConfig.IsDebuging && TrafficController.getInstance(ParentHomeFragment.this.getContext()).isRegistered()) {
            TuneTracking.getInstance().trackSubsRenew("finjan_vpn_monthly_single_device");
        }

    }

    public void onHomeclick() {
        GoogleTrackers.Companion.setEHomeButton();
//        LocalyticsTrackers.Companion.setEHomeButton();
        String currentURL = getCurrentUrl();
        if (!TextUtils.isEmpty(currentURL) && Patterns.WEB_URL.matcher(currentURL).matches()) {
            if (TextUtils.isEmpty(UserPref.getInstance().getHomePage())) {
                setHomePageDialog(currentURL);
            } else {
                isNavigationPresssed = false;
                omnibarResults(UserPref.getInstance().getHomePage());
            }
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.please_enter_valid_url), Toast.LENGTH_SHORT).show();
        }
    }

    private void setHomePageDialog(final String currentURL) {
        new FinjanDialogBuilder(getActivity()).setMessage(getString(R.string.use_current_page_as_home))
                .setPositiveButton(getString(R.string.alert_ok)).setNegativeButton(getString(R.string.alert_cancel))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        UserPref.getInstance().setHomePage(currentURL);
                        Toast.makeText(getContext(), getString(R.string.home_page_add_successfully), Toast.LENGTH_SHORT).show();
                        setHomeIcon(true);
                    }
                }).show();
    }

    public void showUpgradeBarIfRequired() {
        if (upgradeDialogShowed) {
            return;
        }
        upgradeDialogShowed = true;
        String msg = "";
        if (TrafficController.getInstance(getContext()).isRegistered()) {
            msg = getString(R.string.traffic_limit_bound_message);
        } else {
            msg = getString(R.string.traffic_limit_bound_message_register);
        }
        new FinjanDialogBuilder(getContext()).setMessage(msg).setPositiveButton(getString(R.string.upgrade_small))
                .setNegativeButton(getString(R.string.alert_cancel))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        upgradeDialogShowed = false;
                        startActivity(new Intent(getActivity(), UpgradeVpnActivity.class));
                    }
                }).show();
    }

    private void handleClick(View v) {
        if (v.getId() != R.id.iv_settings) {
            hideDrawer();
        }
        if (v.getId() != R.id.img_clear_url) {
            hideKeyboard(v);
        }
        switch (v.getId()) {

            case R.id.tracker_hint_layout:
                onClickTrackHint();
//                setBottomBars();
                break;

//            case R.id.ll_vt_land:
            case R.id.iv_connection:
                onvtclick();
                break;

//            case R.id.ll_top_bookmark_land:
            case R.id.iv_bookmark:
                onBookmarkPage();
                break;

//            case R.id.ll_home_land:
            case R.id.iv_home:
                onHomeclick();
                break;

            case R.id.rl_tracker:
                onClickTrackerFab();
                break;

            case R.id.iv_vpn:
                onClickVPN();
                break;
            case R.id.backButtonUrl:
                getActivity().onBackPressed();
                break;
            case R.id.img_clear_url:
                setUrlEditText("", true);
//                urlWebView.stopLoading();
                break;
            case R.id.iv_refresh:
                onReloadClick();
                break;
            case R.id.iv_tabsopened:
                onClickTabs();
                break;
            case R.id.iv_settings:
                toggleSettings(v);
                break;
            case R.id.backButton:
                showWebview();
                onClickBack();
                break;
            case R.id.forwardButton:
                showWebview();
                onClickForward();
                break;
//            case R.id.iv_info:
//                oninfoClick(v);
//                break;
        }
    }

    public void onClickTrackerFab() {
        GoogleTrackers.Companion.setSTrackers();
        if (UserPref.getInstance().getTracker()) {
            ArrayList<TrackerFoundModel> trackerFoundList = null;
            if (trackerHelper != null) {
                trackerFoundList = trackerHelper.getTrackerList();
            }

            if (trackerFoundList == null || trackerFoundList.size() == 0) {
                showToast(R.string.no_tracker_found);
            } else {
                showTrackerList(trackerFoundList);
            }
        } else {
            if (!TextUtils.isEmpty(PlistHelper.getInstance().getTrackerDisableMessage())) {
                TrackerHelper.getDefaultTrackerAlert(ParentHomeFragment.this.getContext(), new TrackerHelper.TrackerTurnOnListener() {
                    @Override
                    public void onTurnOn() {
                        setTrackerVisibility();
                        onReloadClick();
                    }
                });
            }
        }
    }

    @Override
    public void onClick(View v) {
        handleClick(v);
    }

    private void showTrackerList(ArrayList<TrackerFoundModel> trackerFoundList) {

        stopProgressBar();
        String TAG = FragmentTrackerList.class.getSimpleName();
//        FragmentTrackerList fragment=(FragmentTrackerList)getActivity().getSupportFragmentManager().findFragmentByTag(TAG);
//        if(fragment==null){
        FragmentTrackerList fragment = (FragmentTrackerList) newInstance("Trackers", false, DKEY_Tracker_Fragemnt);
//        }
        fragment.setFragmentData(trackerFoundList);
        changeFragment(fragment, true, TAG, TAG);
    }

    private boolean isTabSizeUnderLimit() {
        try {
            return getTabsCount() >= Integer.parseInt(PlistHelper.getInstance().getMaxTabCount());
//            return getTabsCount() >= Integer.parseInt(Utils.getPlistvalue(Constants.KEY_MAX_TAB_COUNT, ""));
        } catch (Exception e) {
            return false;
        }
    }

    private void refreshTabCount() {
        int count = getTabsCount();
        txt_tabCount.setText(count == 0 ? "0" : "" + count);
        txt_tabCount.setVisibility(View.VISIBLE);
        iv_tabsopened.setVisibility(View.VISIBLE);
    }

    @Override
    public void receivedBroadcast() {
        manageAppPurchaseUI();
    }

    private void manageAppPurchaseUI() {
        List additionalSkuList = new ArrayList();
        additionalSkuList.add(getResources().getString(R.string.app_purchase_key_monthly));
        additionalSkuList.add(getResources().getString(R.string.app_purchase_key_yearly));
    }

    @Override
    public void onResume() {
        super.onResume();
//        Logger.logE(TAG,
//                "WifiSecurityHelper"+(WifiSecurityHelper.getInstance().getNetworkType(this.getContext())==null?"":
//                        WifiSecurityHelper.getInstance().getNetworkType(this.getContext())).toString());
        this.displayHeight = ((HomeActivity) getActivity()).displayHeight;
        ScreenNavigation.getInstance().saveLastScreen(this);
        TipScreenHelper.getInstance().setTipScreen(this, parentView);
//        testBoolean();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        VpnStatus.addByteCountListener(this);

        setProgressbarColor(UserPref.getInstance().getSafeScanEnabled());
        GlobalVariables.getInstnace().currentFragmentTag = getFragmentTag();
        setSearchedUrl();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                canBeEditNow = true;
            }
        }, 1000);
        setVpnIcon();
        setTrackerVisibility();
        if (GlobalVariables.getInstnace().shouldPageReload) {
            onReloadClick();
        }
        if (!TextUtils.isEmpty(GlobalVariables.getInstnace().lastDirectUrl) && !GlobalVariables.getInstnace().lastDirectUrl
                .equals(GlobalVariables.getInstnace().lastDirectConsumedUrl)) {
            defaultLoading();
        }
    }

    private void setSearchedUrl() {
        Bundle bundle = getChangeCarrier();
        if (bundle != null) {
            if (bundle.containsKey(AppConstants.BKEY_REDIRECTED_FUNCTION)) {
                switch (bundle.getInt(AppConstants.BKEY_REDIRECTED_FUNCTION)) {
                    case AppConstants.RFUC_URL_NON_SAFTY_RESULT:
                        isNavigationPresssed = false;
                        omnibarResults(bundle.getString(AppConstants.BKEY_REDIRECT_URL));
                        newTab = false;
                        break;
                }
            }
            setChangeCarrier(null);
        }
    }

    private void showLoader() {
        ParentHomeFragment.this.finzenLoaderVisibility(View.VISIBLE, (FragmentHome) ParentHomeFragment.this);
    }

    private void dismissLoader() {
        if (isSafetyResultFetching) {
            return;
        }
        finzenLoaderVisibility(View.GONE, null);
    }

    private void setBackgroundImage() {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            try {
                getActivity().getWindow().getDecorView().
                        setBackgroundColor(ResourceHelper.getInstance().getColor(R.color.white));
            } catch (OutOfMemoryError e) {
                System.gc();
            } catch (Exception e) {
                System.gc();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
            Webview Utilities
         */
    public void setupWebView() {
//        resetWebView();
        urlWebView.getSettings().setJavaScriptEnabled(true);
        urlWebView.getSettings().setDomStorageEnabled(true);
        urlWebView.getSettings().setBuiltInZoomControls(true);
        urlWebView.getSettings().setDisplayZoomControls(false);
        urlWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        urlWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        urlWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        urlWebView.getSettings().setSupportZoom(true);
        String userAgent = "Mozilla/5.0 (Linux; Android 4.4; Nexus 5 Build/_BuildID_) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36";
        urlWebView.getSettings().setUserAgentString(userAgent);


        if (UserPref.getInstance().getPrivateBrowsing()) {
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(false);
            urlWebView.getSettings().setSaveFormData(false);
            urlWebView.getSettings().setSavePassword(false);
            urlWebView.getSettings().setAppCacheEnabled(false);
            urlWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        }
        // Prevent the copy / paste / share popup from appearing
        urlWebView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                WebView.HitTestResult result = urlWebView.getHitTestResult();
                if (result != null && result.getExtra() != null && !result.getExtra().isEmpty()) {
                    showNewTabCustomDialog(result.getExtra());
                }
                return true;
            }
        });
        urlWebView.setLongClickable(true);
        resetWebView();
    }

    private void showNewTabCustomDialog(final String extra) {
        final Dialog HomeDialog = new Dialog(getActivity());
        HomeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        HomeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        HomeDialog.setContentView(R.layout.layout_home_alert_dialog);
        HomeDialog.setCanceledOnTouchOutside(false);
        ((TextView) HomeDialog.findViewById(R.id.btncancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeDialog.dismiss();
            }
        });
        ((TextView) HomeDialog.findViewById(R.id.txt_dialg_title)).setText(extra);
        ((TextView) HomeDialog.findViewById(R.id.txt_dialog_clear_home)).setText(getResources().getString(R.string.copy_link_url));
        ((TextView) HomeDialog.findViewById(R.id.txt_dialog_clear_home)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyUrlToClipBoard(extra);
                HomeDialog.dismiss();
            }
        });
        ((TextView) HomeDialog.findViewById(R.id.txt_dialog_use_home)).setText(getResources().getString(R.string.open_in_ew_tab));
        ((TextView) HomeDialog.findViewById(R.id.txt_dialog_use_home)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openUrl("", extra, false, null);
                HomeDialog.dismiss();
            }
        });
        HomeDialog.show();
    }

    private void copyUrlToClipBoard(String extra) {
        int sdk = Build.VERSION.SDK_INT;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(extra);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(getString(R.string.copied_text), extra);
            clipboard.setPrimaryClip(clip);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        WebView.HitTestResult result = null;
        WebView webview = (WebView) v;
        if (webview != null) {
            result = urlWebView.getHitTestResult();
        }

        MenuItem.OnMenuItemClickListener handler = new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                return true;
            }
        };

        if (result != null && (result.getType() == WebView.HitTestResult.ANCHOR_TYPE ||
                result.getType() == WebView.HitTestResult.SRC_ANCHOR_TYPE)) {
            menu.setHeaderTitle(result.getExtra());
            menu.add(0, Constants.ID_OPEN_IN_NEW_TAB_MENU, 0, getString(R.string.open_in_new_tab)).setOnMenuItemClickListener(handler);
            menu.add(0, Constants.ID_COPY_URL_MENU, 0, "Copy Link URL").setOnMenuItemClickListener(handler);
            menu.add(0, Constants.ID_CANCEL_MENU, 0, "Cancel").setOnMenuItemClickListener(handler);
        } else {
            ParentHomeFragment.this.showToast("Please try again later!");
        }
    }

    @SuppressLint("JavascriptInterface")
    public void resetWebView() {
        reportIndex = -1;
        urlWebView.setWebViewClient(new UrlOverrideWebViewClient());
//        urlWebView.getSettings().setSupportMultipleWindows(true);
        urlWebView.getSettings().setJavaScriptEnabled(true);
//        urlWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        urlWebView.getSettings().setAllowFileAccessFromFileURLs(true);
        urlWebView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                GlobalVariables.getInstnace().downloadURL = url;
                GlobalVariables.getInstnace().downloadfileName = URLUtil.guessFileName(url, contentDisposition, mimetype);
                if (isStoragePermissionGranted()) {
                    DownloadHelper.getInstance().downloadFile(getActivity(), userAgent, GlobalVariables.getInstnace().downloadURL,
                            GlobalVariables.getInstnace().downloadfileName);
                } else {
                    if (NativeHelper.getInstnace().checkActivity(getActivity())) {
                        PermissionUtil.askFileWritePermission(getActivity());
//                        ActivityCompat.requestPermissions(getActivity(),
//                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.STORAGE_PERMISSION);
                    }
                }
            }
        });

        setWebChromeClient(parentView);
    }

    private void setWebChromeClient(View parentView) {
        videoHelpder = WebVideoHelper.getInstnace().initWebVideo(this, parentView,
                (RelativeLayout) webViewContainer, urlWebView, new WebVideoHelper.WebChromeListener() {
                    @Override
                    public void onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
                        if (isTabSizeUnderLimit()) {
                            showTabOverSizeDialog();
                        } else {
//                    newTab = true;
                            urlWebView.setWebViewClient(new UrlOverrideWebViewClient());
                            urlWebView.requestFocusNodeHref(resultMsg);
                            String url = resultMsg.getData().getString("url");
                            isNavigationPresssed = false;
                            omnibarResults(url);
                        }
                    }

                    @Override
                    public void onProgressChanged(WebView view, int newProgress) {
                        if (progressbar1 != null) {
                            progressbar1.setProgress(newProgress);
                            if (newProgress == 100) {
                                progressbar1.setVisibility(View.GONE);
                            } else {
                                progressbar1.setVisibility(View.VISIBLE);
                            }
                        }
                        if (progressbar2 != null) {
                            progressbar2.setProgress(newProgress);
                            if (newProgress == 100) {
                                progressbar2.setVisibility(View.GONE);
                            } else {
                                progressbar2.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    @Override
                    public void onExitFullScreen() {
                        header.setVisibility(View.VISIBLE);
                        view_drawer_bottom.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFullScreen() {
                        header.setVisibility(View.GONE);
                        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
                            ((HomeActivity) getActivity()).onClickReportBarAway();
                        }
                        view_drawer_bottom.setVisibility(View.GONE);
                    }

                    @Override
                    public void filechooserPre(ValueCallback<Uri> filePathCallback) {
                        if (mFilePathCallbackPre != null) {
                            mFilePathCallbackPre.onReceiveValue(null);
                            mFilePathCallbackPre = null;
                        }
                        ParentHomeFragment.this.mFilePathCallbackPre = filePathCallback;
                    }

                    @Override
                    public void filechooser(ValueCallback<Uri[]> filePathCallback) {
                        if (mFilePathCallback != null) {
                            mFilePathCallback.onReceiveValue(null);
                            mFilePathCallback = null;
                        }
                        ParentHomeFragment.this.mFilePathCallback = filePathCallback;
                    }
                });
        urlWebView.setWebChromeClient(videoHelpder.getWebChromeClient());
    }

    private void generateNotification(boolean success, Intent fileIntent) {
        if (success) {
            fileDownloadSuccessNotification(fileIntent);
        } else {
            fileDownloadFailureNotification();
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final UploadingMsg event) {
        if (mFilePathCallbackPre != null) {
            mFilePathCallbackPre.onReceiveValue(event.getUploadingUriPre());
            mFilePathCallbackPre = null;
        } else if (mFilePathCallback != null) {

            mFilePathCallback.onReceiveValue(event.getUploadingUri());
            mFilePathCallback = null;
        }
    }

    private void fileDownloadFailureNotification() {
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(getActivity())
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.no_handler));
        NotificationManager manager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    private void fileDownloadSuccessNotification(Intent fileIntent) {
        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(getActivity())
                // Set Icon
                .setSmallIcon(R.drawable.notification_icon)
                // Set Ticker Message
                // Dismiss Notification
                .setAutoCancel(true);
        // Set PendingIntent into Notification


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            builder = (NotificationCompat.Builder) builder.setContent(getComplexNotificationView(fileIntent));
//        } else {
        builder = (NotificationCompat.Builder) builder.setContentTitle(getTitle())
                .setContentText(getString(R.string.download_complete))
                .setSmallIcon(R.drawable.notification_icon);
        PendingIntent pIntent = PendingIntent.getActivity(
                getActivity(),
                NOTIFICATION_ID,
                fileIntent,
                FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pIntent);
//        }
        // Create Notification Manager
        notificationmanager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify((int) GlobalVariables.getInstnace().mDownloadedFileID, builder.build());
    }

    private void setWarningBackPress() {
        if (newTab || urlWebView.getUrl() == null) {
//            urlEditText.setText("");
            isReportAvailable = false;
            isTrackerReportAvailable = false;
            currentSafetyResult = null;

            if (!TextUtils.isEmpty(UserPref.getInstance().getHomePage())) {
                currentUrl = UserPref.getInstance().getHomePage();

            } else if (!TextUtils.isEmpty(PlistHelper.getInstance().getHomePage())) {
                currentUrl = PlistHelper.getInstance().getHomePage();
            } else {
                currentUrl = AppConstants.EMPTY_URL;
            }
            omnibarResults(currentUrl);
        } else {
            if (!(currentSafetyResult != null && urlWebView.getUrl().equalsIgnoreCase(currentSafetyResult.getUrl()))) {

                omnibarResults(urlWebView.getUrl());
            } else {
//                if(isNavigationPresssed){
                if (backWardUrl != null) {
                    onClickBack();
                } else {
                    this.isNavigationPresssed = false;
                    isReportAvailable = false;
                    isTrackerReportAvailable = false;
                    currentSafetyResult = null;
                    if (!TextUtils.isEmpty(UserPref.getInstance().getHomePage())) {
                        currentUrl = UserPref.getInstance().getHomePage();

                    } else if (!TextUtils.isEmpty(PlistHelper.getInstance().getHomePage())) {
                        currentUrl = PlistHelper.getInstance().getHomePage();
                    } else {
                        currentUrl = AppConstants.EMPTY_URL;
                    }
                    omnibarResults(currentUrl);
                }
                warningFromNavigation = false;
//                }
            }
        }
    }

    protected void onClickBack() {

        try {
            if (!TextUtils.isEmpty(backWardUrl)) {
                isNavigationPresssed = true;
                warningFromNavigation = true;
                urlWebView.goBackOrForward(backwardIndex);
                setBackForwordClick(false, false);
//                omnibarResults(backWardUrl);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClickForward() {

        if (!TextUtils.isEmpty(forwarUrl)) {
            isNavigationPresssed = true;
            warningFromNavigation = true;
            urlWebView.goBackOrForward(forwardIndex);
            omnibarResults(forwarUrl);
            setBackForwordClick(false, false);
        }
    }

    public String getDomainName(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

    private void toggleErrorScreen(boolean showError) {
        if (showError) {
            if (NativeHelper.getInstnace().checkActivity(getActivity())) {
                ((HomeActivity) getActivity()).onClickReportBarAway();
            }
        }
        showHeader();
        isFailour = showError;
        int textVisibility = showError ? View.VISIBLE : View.GONE;
        int otherVisibility = (!showError) ? View.VISIBLE : View.GONE;
        txt_webloadError.setVisibility(textVisibility);
//        urlWebView.setVisibility(otherVisibility);
        webViewContainer.setVisibility(otherVisibility);
    }

    private void removeUrlCleaner() {
        if (!urlEditText.hasFocus()) {
            urlCleaner.setVisibility(View.GONE);
        }
    }

    private boolean canSafeSearched() {

        return
//                isDefaultSafeSearch() &&
//        return isDefaultSafeSearch() &&
                UserPref.getInstance().getSafeScanEnabled() &&
                        (UserPref.getInstance().getSafeSearchPurchase() ||
                                NativeHelper.getInstnace().getScanRemain() > 0) || vtScan;
    }

    private boolean shouldShowPurchaseBar() {
        return !(UserPref.getInstance().getSafeSearchPurchase() ||
                (NativeHelper.getInstnace().getScanRemain() > 0));
    }

    /*
        Utility functions
     */

    private void findTrackerUsingScript(String url) {
        if (UserPref.getInstance().getTracker()) {
            isTrackerReportAvailable = true;
            index = 0;
            updateTrackerCount(0);
            trackerHandler.removeCallbacks(trackerRunnable);
            trackerHandler.postDelayed(trackerRunnable, 400);
        }
    }

    @Override
    protected void updateTrackerCount(final int size) {
        if (!NativeHelper.getInstnace().checkActivity(getActivity())) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvTrackerCount.setText(String.valueOf(size));
            }
        });
    }

    private void showTracker(String url) {
        if (trackerHelper != null) {
            trackerHelper = null;
            System.gc();
        }
        trackerHelper = new TrackerHelper(this, new TrackerHelper.TrackerListener() {
            @Override
            public void updateTrackerCount(int count) {

                if (NativeHelper.getInstnace().checkActivity(getActivity())) {
                    ParentHomeFragment.this.updateTrackerCount(count);
                    if (isVisible()) {
                        setTrackFab(count);
                    }
                }
            }

            @Override
            public void trackerFound(int count, ArrayList<TrackerFoundModel> trackerFoundList) {
            }

            @Override
            public void hideLoader() {
                hideLoader();
            }

            @Override
            public void showLoader() {
                showLoader();
            }
        });
        trackerHelper.initializeTrackerFinding(url, urlWebView);
    }

    private void showLoginAlertDialog(final HttpAuthHandler handler, final String host) {
        final Dialog loginDialog = new Dialog(getActivity());
        loginDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loginDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loginDialog.setContentView(R.layout.layout_login);
        loginDialog.setCanceledOnTouchOutside(true);
        final TextInputLayout inputLayoutName = (TextInputLayout) loginDialog.findViewById(R.id.input_layout_username);
        final TextInputLayout inputLayoutPassword = (TextInputLayout) loginDialog.findViewById(R.id.input_layout_password);
        final EditText inputName = (EditText) loginDialog.findViewById(R.id.input_username);
        final EditText inputPassword = (EditText) loginDialog.findViewById(R.id.input_password);
        TextView okButton = (TextView) loginDialog.findViewById(R.id.btnclear);
        TextView cancelButton = (TextView) loginDialog.findViewById(R.id.btncancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.cancel();
                loginDialog.dismiss();
            }
        });
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = inputName.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();
                if (userName.length() == 0) {
                    inputLayoutName.setError(getString(R.string.err_msg_email));
                    inputName.requestFocus();
                } else if (password.length() == 0) {
                    inputLayoutName.setErrorEnabled(false);
                    inputLayoutPassword.setError(getString(R.string.err_msg_password));
                    inputPassword.requestFocus();
                } else {
                    loginDialog.dismiss();
                    handler.proceed(userName, password);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadUrl(true, host);
//                            urlWebView.loadUrl(host);
                        }
                    }, 1000);
                }
            }
        });
        loginDialog.show();
    }

    private void manageCookie(String url) {
        if (isPrivate) {
            privateCookie.startSync();
            normalCookie.stopSync();
            privateCookieManager.setAcceptCookie(true);
            normalCookieManager.setAcceptCookie(false);
        } else {
            privateCookie.stopSync();
            normalCookie.startSync();
            normalCookieManager.setAcceptCookie(true);
        }
    }

    private boolean shouldShowTracker() {
        boolean isTrackerHintShowing = UserPref.getInstance().getTrackerHintVisibility();
        boolean isFirackerHintShown = UserPref.getInstance().getFirstTrackerHintVisibility();
        isTrackerHintShowing = isTrackerHintShowing && !isFirackerHintShown;
//
        String currentUrl = urlWebView.getUrl();
        return !GlobalVariables.getInstnace().isDrawerShowing && !isTrackerHintShowing && UserPref.getInstance().getTracker()
                && !TextUtils.isEmpty(currentUrl) && !currentUrl.equalsIgnoreCase(AppConstants.EMPTY_URL);
    }

    private boolean shouldShowSafeBar() {
        return !GlobalVariables.getInstnace().isDrawerShowing && !barGoneClicked && canSafeSearched();
    }

    private boolean checkApplaunch(String url) {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            Uri checkURL = Uri.parse(url);
            Intent appIntent = new Intent(Intent.ACTION_VIEW, checkURL);
            PackageManager packageManager = getActivity().getPackageManager();
            List activities = packageManager.queryIntentActivities(appIntent,
                    PackageManager.MATCH_DEFAULT_ONLY);
            return activities.size() > 0;
        }
        return false;
    }

    private void setBackForwordButton() {
        if (urlWebView.canGoBack() && urlWebView.canGoForward()) {
            setBackForwordClick(true, true);
        } else {
            if (urlWebView.canGoBack()) {
                setBackForwordClick(true, false);
            } else if (urlWebView.canGoForward()) {
                setBackForwordClick(false, true);
            } else {
                setBackForwordClick(false, false);
            }
        }
    }

    private void setBackForwordClick(boolean back, boolean forword) {

        if (!NativeHelper.getInstnace().checkContext(getContext())) {
            return;
        }
        backButton.setVisibility(View.VISIBLE);
        forwardButton.setVisibility(View.VISIBLE);

        if (back) {
            backButton.setColorFilter(ResourceHelper.getInstance().getColor(R.color.white));
            backButton.setClickable(true);
        } else {
            backButton.setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_2));
            backButton.setClickable(false);
        }
        if (forword) {
            forwardButton.setColorFilter(ResourceHelper.getInstance().getColor(R.color.white));
            forwardButton.setClickable(true);
        } else {
            forwardButton.setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_2));
            forwardButton.setClickable(false);
        }
    }

    public void setReportResponse(SafetyResult safetyResult) {
//        rating = safetyResult.rating;
        isReportAvailable = true;
        reportColor = safetyResult.getColor();
        setProgressbarColor(true);
        ArrayList<String> maliKey = new ArrayList<>();
        ArrayList<String> malwKey = new ArrayList<>();
        ArrayList<String> phisKey = new ArrayList<>();
        ArrayList<String> suspiciousKey = new ArrayList<>();
        ArrayList<String> cleanKey = new ArrayList<>();
        ArrayList<String> unratedKey = new ArrayList<>();
        ArrayList<String> maliVal = new ArrayList<>();
        ArrayList<String> malwVal = new ArrayList<>();
        ArrayList<String> phisVal = new ArrayList<>();
        ArrayList<String> suspiciousVal = new ArrayList<>();
        ArrayList<String> cleanVal = new ArrayList<>();
        ArrayList<String> unratedVal = new ArrayList<>();
        ArrayList<String> keyArrayList = new ArrayList<>();
        ArrayList<String> valueArrayList = new ArrayList<>();
        try {
            Set<Map.Entry<String, JsonElement>> entries = safetyResult.getScans().entrySet();//will return members of your object
            for (Map.Entry<String, JsonElement> entry : entries) {
                if (entry.getValue().toString().toLowerCase().contains("mali")) {
                    maliKey.add(entry.getKey());
//                    String maliware = Utils.getPlistvalue(Constants.KEY_SCANNER_DATA_FILTER, Constants.KEY_SCANNER_MALICIOUS, "");
                    String maliware = PlistHelper.getInstance().getMalicousValue();
                    maliVal.add(TextUtils.isEmpty(maliware) ? entry.getValue().toString() : maliware);
                } else if (entry.getValue().toString().toLowerCase().contains("malw")) {
                    malwKey.add(entry.getKey());
//                    String maliware = Utils.getPlistvalue(Constants.KEY_SCANNER_DATA_FILTER, Constants.KEY_SCANNER_MALWARE, "");
                    String maliware = PlistHelper.getInstance().getMalwareValue();
                    malwVal.add(TextUtils.isEmpty(maliware) ? entry.getValue().toString() : maliware);
//                    malwVal.add(entry.getValue().toString());
                } else if (entry.getValue().toString().toLowerCase().contains("phi")) {
                    phisKey.add(entry.getKey());
                    phisVal.add(entry.getValue().toString());
                } else if (entry.getValue().toString().toLowerCase().contains("sus")) {
                    suspiciousKey.add(entry.getKey());
                    suspiciousVal.add(entry.getValue().toString());
                } else if (entry.getValue().toString().toLowerCase().contains("clean")) {
                    cleanKey.add(entry.getKey());
                    cleanVal.add(entry.getValue().toString());
                } else if (entry.getValue().toString().toLowerCase().contains("unr")) {
                    unratedKey.add(entry.getKey());
                    unratedVal.add(entry.getValue().toString());
                } else {
                    keyArrayList.add(entry.getKey());
                    valueArrayList.add(entry.getValue().toString());
                }
            }
        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        Collections.sort(unratedKey, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(cleanKey, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(suspiciousKey, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(phisKey, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(malwKey, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(maliKey, String.CASE_INSENSITIVE_ORDER);
        Collections.reverse(unratedKey);
        Collections.reverse(cleanKey);
        Collections.reverse(suspiciousKey);
        Collections.reverse(phisKey);
        Collections.reverse(malwKey);
        Collections.reverse(maliKey);
        keyArrayList.addAll(unratedKey);
        keyArrayList.addAll(cleanKey);
        keyArrayList.addAll(suspiciousKey);
        keyArrayList.addAll(phisKey);
        keyArrayList.addAll(malwKey);
        keyArrayList.addAll(maliKey);
        valueArrayList.addAll(unratedVal);
        valueArrayList.addAll(cleanVal);
        valueArrayList.addAll(suspiciousVal);
        valueArrayList.addAll(phisVal);
        valueArrayList.addAll(malwVal);
        valueArrayList.addAll(maliVal);
        Collections.reverse(keyArrayList);
        Collections.reverse(valueArrayList);
        SearchHelper.getInstance().setKeyArrayList(keyArrayList);
        SearchHelper.getInstance().setValueArrayList(valueArrayList);
        setSafetyReport(safetyResult);
    }

    /**
     * progress bar will show default colors if safe scan is not enabled
     * i.e. --> R.color.fj_menu_bg
     *
     * @param isSafeScan
     */
    private void setProgressbarColor(boolean isSafeScan) {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            int defaultColor = ResourceHelper.getInstance().getColor(R.color.accent_color_0);
            int customColor = ResourceHelper.getInstance().getColor(R.color.accent_color_0);
            if (reportColor != -1) {
                customColor = ResourceHelper.getInstance().getColor(reportColor);
            }
            if (progressbar1 != null) {
                progressbar1.setBackgroundColor(isSafeScan ? customColor : defaultColor);
                progressbar1.getProgressDrawable().setColorFilter(
                        isSafeScan ? customColor : defaultColor, PorterDuff.Mode.SRC_IN);
            }
            if (progressbar2 != null) {
                progressbar2.setBackgroundColor(isSafeScan ? customColor : defaultColor);
                progressbar2.getProgressDrawable().setColorFilter(
                        isSafeScan ? customColor : defaultColor, PorterDuff.Mode.SRC_IN);
            }
        }
    }

    private void setTheme() {

        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            int color = isPrivate ? R.color.content_color : R.color.col_desc_w;
            color = ResourceHelper.getInstance().getColor(color);

            if (isPrivate) {
                header.setBackgroundColor(color);
                changeStatusBarColor(R.color.content_color);
                rl_urlview.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_edittext_private));
                view_drawer_bottom.setBackgroundColor(ResourceHelper.getInstance().getColor(R.color.content_color));
            } else {
                header.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.top_title_background_bg));
                changeStatusBarColor(R.color.col_desc_w);
                rl_urlview.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_edittext_general));
                view_drawer_bottom.setBackgroundColor(ResourceHelper.getInstance().getColor(R.color.col_desc_w));
            }
            btn_cancel.setTextColor(getResources().getColor(R.color.white));
            webViewControls.setBackgroundColor(color);
            btn_cancel.setBackgroundColor(color);
//            iv_tabsopened.setBackgroundColor(color);
            searchIcon.setBackgroundColor(color);
            headerCover.setBackgroundColor(color);
//            tv_url.setBackgroundColor(color);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                iv_tabsopened.setBackgroundTintList(ColorStateList.valueOf(color));
                searchIcon.setBackgroundTintList(ColorStateList.valueOf(color));
            }
        }
    }

    public void setLoopingVideo(VideoView videoView, String pathToVideo) {
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });
        videoView.setVideoPath(pathToVideo);
    }

    /*
        TRANSITIONS: Header
     */
    public void toggleUrlAsTitle() {
//        popCurrentFragment();
    }

    public void showWebViewControls() {
        if (isUrlInEditForm) {
            return;
        }
        Logger.logE(TAG, "showWebViewControls");
        webViewControls.setVisibility(View.VISIBLE);
//        iv_menu_icon.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        forwardButton.setVisibility(View.VISIBLE);
        iv_tabsopened.setVisibility(View.VISIBLE);
//        iv_refresh.setVisibility(View.VISIBLE);
        if (ll_urlview.getVisibility() == View.GONE) {
            ll_urlview.setVisibility(View.VISIBLE);
        }
        btn_cancel.setVisibility(View.GONE);
        RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        llp.addRule(RelativeLayout.START_OF, R.id.iv_vpn);
        llp.addRule(RelativeLayout.END_OF, R.id.webViewControls);
        llp.setMargins(0, 15, 0, 15); // llp.setMargins(left, top, right, bottom);
        ll_urlview.setLayoutParams(llp);
    }

    protected void makeUrlForEditing() {
        if (parentView != null) {
            Utils.showKeyboard(parentView);
        }
        ll_urlview.setVisibility(View.VISIBLE);
        if (urlEditText.getText().toString().trim().length() > 0) {
            urlCleaner.setVisibility(View.VISIBLE);
        }
        webViewControls.setVisibility(View.GONE);
//        iv_menu_icon.setVisibility(View.GONE);
        backButton.setVisibility(View.GONE);
        forwardButton.setVisibility(View.GONE);
        iv_tabsopened.setVisibility(View.INVISIBLE);
        txt_tabCount.setVisibility(View.GONE);
        iv_refresh.setVisibility(View.GONE);
        iv_vpn.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) btn_cancel.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_END);
        params.setMargins(5, 2, 0, 10);
        btn_cancel.setLayoutParams(params);
        RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        llp.addRule(RelativeLayout.START_OF, R.id.btn_cancel);
        llp.addRule(RelativeLayout.END_OF, 0);
        llp.setMargins(15, 15, 15, 15); // llp.setMargins(left, top, right, bottom);
        ll_urlview.setLayoutParams(llp);
        hideDrawer();
    }

    public void onUrlFocusChange(View v) {
        if (isUrlInEditForm) {
            return;
        }
        refreshTabCount();
        hideKeyboard(v);
        iv_tabsopened.setVisibility(View.VISIBLE);
        webViewControls.setVisibility(View.VISIBLE);
//        iv_menu_icon.setVisibility(View.VISIBLE);
        ll_urlview.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        forwardButton.setVisibility(View.VISIBLE);
        iv_vpn.setVisibility(View.VISIBLE);
        btn_cancel.setVisibility(View.GONE);
        urlCleaner.setVisibility(View.GONE);
        RelativeLayout.LayoutParams llpm = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        llpm.addRule(RelativeLayout.START_OF, R.id.iv_vpn);
        llpm.addRule(RelativeLayout.END_OF, R.id.webViewControls);
        llpm.setMargins(0, 15, 15, 15); // llp.setMargins(left, top, right, bottom);
        ll_urlview.setLayoutParams(llpm);
        if (TextUtils.isEmpty(urlEditText.getText().toString())) {
            showLastUrl();
        }
        Logger.logE(TAG, "onUrlFocusChange");
        setBottomBars((FragmentHome) ParentHomeFragment.this, true);
    }

    /*
        TRANSITIONS: Scanning Screen
     */
    public void showScanningView() {
//        makeUrlTitle((RelativeLayout.LayoutParams) header.getLayoutParams());
        makeUrlForEditing();
        scanningViewContainer.setVisibility(View.VISIBLE);
        // Play video if we're not in the emulator ("generic" implies emulated)
        if (!Build.FINGERPRINT.contains("generic")) {
            //setLoopingVideo(scanningVideo, "android.resource://" + getPackageName() + "/" + R.raw.code);
            scanningVideo.start();
        }
        webViewContainer.setVisibility(View.GONE);
    }

    public void hideScanningView() {
        scanningViewContainer.setVisibility(View.GONE);
    }

    /**
     * TRANSITIONS: warning Screen
     * <p>
     * make header of warning fragment as red and make url red
     */
    public void showWarnigFragmnet(String url, SslError sslError, FragmentWarning.WarningCallback warningCallback) {

        if (sslError == null) {
            urlWebView.stopLoading();
        }
        String TAG = FragmentWarning.class.getSimpleName();
        Bundle bundle = new Bundle();
        setUrlEditText("", true);
        bundle.putString(AppConstants.BKEY_URL, url);
        bundle.putBoolean(FragmentWarning.WARNING_FROM_NAVIGATION, warningFromNavigation);
        bundle.putString(FragmentWarning.WARNING_TYPE, sslError == null ? FragmentWarning.WARNING_TYPE_SAFETY_RESULT :
                FragmentWarning.WARNING_TYPE_SSL);
        FragmentWarning fragmentWarning = (FragmentWarning) newInstance("", false, DKEY_Worning, bundle);
        fragmentWarning.setSslError(sslError);
        fragmentWarning.setWarningCallback(warningCallback);
        changeFragment(fragmentWarning, true, TAG, TAG);
    }

    /*
        TRANSITIONS: Web View Screen
         for showing worning screen
     */
    public void loadNew(SafetyResult newSafetyResult) {
        if ((newSafetyResult.getRating().equals("dangerous"))) {
            if (UserPref.getInstance().getAudable()) {
                playWarningMusic();
            }
            currentSafetyResult = newSafetyResult;
            dismissLoader();
            showWarnigFragmnet(newSafetyResult.url, null, warningCallback);
            return;
        } else if (newSafetyResult.getRating().equals("suspicious")) {
            if (UserPref.getInstance().getAudable()) {
                playWarningMusic();
            }
        }
        currentSafetyResult = newSafetyResult;
        loadUrl(true, currentSafetyResult.url);
    }

    public void loadNewWithoutSafetyCheck(String url) {

        finzenLoaderVisibility(View.VISIBLE, (FragmentHome) ParentHomeFragment.this);
        loadUrl(true, url);
    }

    public void popCurrentView() {

    }

    public void showWebView() {
        if (webViewContainer.getVisibility() != View.VISIBLE) {
            webViewContainer.setVisibility(View.VISIBLE);
            // Prevent double-push
//            resetWebView();
        }
    }

    private void setTrackFab(int count) {
        if (shouldShowTracker() && (!downScroll || mScrollY < 50 || isScrollEndPointReached) && isTrackerReportAvailable) {
//            if (!isTrakerFabVisible()) {
            updateTrackerCount(count);
            showTrackFab();
//            }
        } else {
            hideTrackerFab();
        }
    }

    private void setReportBar(boolean isFromScrolling) {

        if (isSafeBarShouldShowOnScroll(isFromScrolling) && (shouldShowSafeBar()) && (downScroll || mScrollY < 50 || isScrollEndPointReached) && isReportAvailable &&
                urlWebView.getUrl() != null && !urlWebView.getUrl().equalsIgnoreCase(AppConstants.EMPTY_URL) &&
                !UserPref.getInstance().getTrackerHintVisibility() && !TipScreenHelper.getInstance().isTipShowing) {
            if (currentSafetyResult != null) {
                setSafetyReport(currentSafetyResult);
            }
            showReportBar();
            return;
        }
        hideReportBar();
    }

    private boolean isSafeBarShouldShowOnScroll(boolean isFromScroll) {
        if (UserPref.getInstance().getRmSecs() == 0) {
            return true;
        }
        if (!isFromScroll) {
            return false;
        }
        return true;
    }

    public void setBottomBars(FragmentHome fragmentHome, boolean safeShouldShow) {
        if (fragmentHome.isVisible()) {
            setReportBar(safeShouldShow);
            manageAppPurchaseBar();
            setTrackFab(trackerHelper != null ? trackerHelper.getTrackerCount() : 0);
            setUrlEditText(urlWebView.getUrl(), false);
        }
    }

    private void onOrientationChanged() {
        setBottomBars((FragmentHome) ParentHomeFragment.this, true);
    }

    /*
        TRANSITIONS: Search Results Screen
     */
    // From DisplayWebView interface

    public void toggleSettings(View view) {

        if (TextUtils.isEmpty(getCurrentUrl())) {
            showToast("Please wait while page is loading");
            return;
        }

        if (GlobalVariables.getInstnace().isDrawerShowing) {
            hideDrawer();
            downScroll = false;
            setBottomBars((FragmentHome) ParentHomeFragment.this, true);
            return;
        }
        showDrawer();
        hideBarsAndFab();
    }

    public void showAlertForceUpgrade() {
        new FinjanDialogBuilder(getActivity()).setMessage(getString(R.string.scan_force_update))
                .setPositiveButton(getString(R.string.alert_upgrade))
                .setNegativeButton(getString(R.string.alert_cancel))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        showAppPurchaseScreen();
                    }
                }).show();
    }

    public void showVirusCheckAlertDialog() {

        new FinjanDialogBuilder(getActivity()).setMessage(getString(R.string.would_you_like_rescan))
                .setPositiveButton(getString(R.string.alert_yes))
                .setNegativeButton(getString(R.string.alert_no))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        vtScan = true;
                        showLoader();
                        cleanUrl(urlEditText.getText().toString());
                        barGoneClicked = false;
                        isNavigationPresssed = true;
                        checkSafetyVirus(currentUrl);
                    }
                }).show();
    }

    void urlDataSet() {
        urlEditText.setAnchor(header);
        webhistoryDao webhistoryDao = AnalyticsApplication.daoSession.getWebhistoryDao();
        List<webhistory> urlhistoryList = webhistoryDao.queryBuilder().orderDesc(webHistoryDatabase.webhistoryDao.Properties.Dtime).list();
        String[] arr = new String[urlhistoryList.size()];
        boolean urlPresent = false;
        mList = new ArrayList<>();
        if (urlhistoryList.size() > 0) {
            for (int h = 0; h < urlhistoryList.size(); h++) {
                urlPresent = false;
                arr[h] = urlhistoryList.get(h).getUrl();
                UrlPojo urlPojo = new UrlPojo();
                urlPojo.setUrl(urlhistoryList.get(h).getUrl());
                if (mList.size() > 0) {
                    for (int j = 0; j < mList.size(); j++) {
                        if (mList.get(j).getUrl().trim().equalsIgnoreCase(urlhistoryList.get(h).getUrl().trim())) {
                            urlPresent = true;
                            break;
                        }
                    }
                    if (urlPresent) {
                        continue;
                    }
                }
                mList.add(urlPojo);
            }
        }
        urlEditText.setThreshold(1);
        UrlAdapter urlAdapter = new UrlAdapter(getActivity(), R.layout.fragment_browser, R.id.lbl_name, mList);
        urlEditText.setAdapter(urlAdapter);
    }

    public void onReloadClick() {
        if (urlEditText.getText().toString().trim().length() > 0) {
            isNavigationPresssed = false;
            loadUrl(true, getCurrentUrl());
//            omnibarResults(urlEditText.getText().toString().trim());
        }
    }

    private boolean isUrlEmpty() {
        return urlEditText.getText().toString().trim().isEmpty();
    }

    private boolean ifBookMark() {
        if (!TextUtils.isEmpty(currentUrl)) {
//            if (currentUrl.equalsIgnoreCase(AppConstants.EMPTY_URL)) {
            List<bookmarks> bookmarksList = DbHelper.getInstnace().getBookMarsList();
            if (bookmarksList.size() > 0) {
                for (bookmarks bookmarks : bookmarksList) {
                    if (currentUrl.equalsIgnoreCase(bookmarks.getUrl())) {
                        return true;
                    }
                }
//                }
            }
        }
        return false;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        onOrientationChanged();
    }

    private void manageAppPurchaseBar() {
        if (!isShowUpgradebar() || canSafeSearched() || UserPref.getInstance().getTrackerHintVisibility()) {
            hidePurchaseBar();
            return;
        }

        if (shouldShowPurchaseBar() && !TextUtils.isEmpty(urlWebView.getUrl())
                && !(urlWebView.getUrl().equalsIgnoreCase(AppConstants.EMPTY_URL))) {
            currentSafetyResult = null;
            showPurchaseBar();
            return;
        }
        hidePurchaseBar();
    }

    private boolean isShowUpgradebar() {
        String setDate = UserPref.getInstance().getNoThanksRemain();
        if (TextUtils.isEmpty(setDate)) {
            return true;
        } else {
            long date;
            try {
                date = Long.parseLong(setDate);
//            strDate = formatter.parse(setDate);
//            formatter.format(setDate);
                if (date != 0 && System.currentTimeMillis() > date)
                    return true;
                else
                    return false;
            } catch (Exception e) {
            }
        }
        return false;
    }

    private String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);
//        return new Date(dateStr);
    }

    private void stopProgressBar() {
        if (progressbar1 != null && progressbar1.getVisibility() == View.VISIBLE) {
            urlWebView.stopLoading();
            progressbar1.setProgress(100);
            progressbar1.setVisibility(View.GONE);
        }
        if (progressbar2 != null && progressbar2.getVisibility() == View.VISIBLE) {
            urlWebView.stopLoading();
            progressbar2.setProgress(100);
            progressbar2.setVisibility(View.GONE);
        }
    }

    public void toggleReport() {
//        if (isNewURlLoad)
//            return;
        GoogleTrackers.Companion.setEVirus_Results();
//        LocalyticsTrackers.Companion.setEVirus_Results();
        if (currentSafetyResult == null) {
            return;
        }
        String TAG = FragmentReport.class.getSimpleName();
//        FragmentReport fragment=(FragmentReport) getActivity().getSupportFragmentManager().findFragmentByTag(TAG);
//        if(fragment==null){
        FragmentReport fragment = (FragmentReport) newInstance(urlWebView.getTitle(), false, DKEY_Report_Fragment);
//        }
        fragment.setFragmentData(currentSafetyResult, getCurrentUrl());
        changeFragment(fragment, true, TAG, TAG);
    }

    /*
        Handle Errors
     */
    public void failedToGetReport() {
        ParentHomeFragment.this.showToast(getResources().getString(R.string.api_error), Toast.LENGTH_LONG);
        dismissLoader();
        toggleUrlAsTitle();
        setUrlEditText(urlWebView.getUrl(), false);
    }

    public void allowCancel(Handler handler) {
        if (cancelScanContainer.getVisibility() == View.GONE) {
            cancelScanContainer.setVisibility(View.VISIBLE);
            cancelButton.setOnClickListener(cancelRunningScan(handler));
        }
    }

    View.OnClickListener cancelRunningScan(final Handler handler) {
        return new View.OnClickListener() {
            public void onClick(View view) {
                handler.removeCallbacksAndMessages(null);
                dismissLoader();
                toggleUrlAsTitle();
                makeUrlForEditing();
            }
        };
    }

    @Override
    public void onRefresh() {
    }

    public void showSearchResultsView() {

        searchedResultFragment = (SearchedResultFragment) newInstance("", false, DKEY_Search_Fragment);
        String TAG = SearchedResultFragment.class.getSimpleName();
        searchedResultFragment.setFragmentData(this, currentSearchTerm, backButton.isClickable(), forwardButton.isClickable());
        changeFragment(searchedResultFragment, true, TAG, TAG);
        setUrlEditText(currentSearchTerm, false);
        dismissLoader();
    }

    private void updateSafeScanCount() {
        int days = UserPref.getInstance().getSafeScanRemain();
        days++;
        UserPref.getInstance().setSafeScanRemain(days);
    }

    public void hideSearchResultsView() {
        if (searchedResultFragment != null) {
            searchedResultFragment.popCurrentFragment();
        }
    }

    private void setDefaultOnClickSearch() {
        urlEditText.clearFocus();
        downScroll = false;
        removeUrlCleaner();
        clearTrackerList();
        showWebview();
        UserPref.getInstance().setDeletedUrl("");

        webViewContainer.setVisibility(View.VISIBLE);
        hideKeyboard(urlEditText);
        setTitle("");
        isFailour = false;
    }

    private void showWebview() {
        if (txt_webloadError.getVisibility() == View.VISIBLE) {
            txt_webloadError.setVisibility(View.GONE);
        }
    }

    private void hideKeyboard(View view) {
        if (isUrlInEditForm) {
            return;
        }
        Utils.hideKeyboard(view);
    }

    public void omnibarResults(String contents) {

        setUrlEditText(contents, false);
        GlobalVariables.getInstnace().tempIsReportAvailable = isReportAvailable;
        GlobalVariables.getInstnace().tempIsTrackerAvailable = isTrackerReportAvailable;
        if (currentSafetyResult != null) {
            try {
                GlobalVariables.getInstnace().tempSafetyResult = currentSafetyResult.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                GlobalVariables.getInstnace().tempSafetyResult = currentSafetyResult.copyMe();
            }
        }


        isReportAvailable = false;
        isTrackerReportAvailable = false;
        currentSafetyResult = null;
        setDefaultOnClickSearch();

        currentUrl = contents;

        if (Utils.isNetworkConnected(getActivity())) {

            if (TextUtils.isEmpty(contents) || contents.equalsIgnoreCase(AppConstants.EMPTY_URL)) {
                if (!TextUtils.isEmpty(UserPref.getInstance().getHomePage())) {
                    currentUrl = UserPref.getInstance().getHomePage();

                } else if (!TextUtils.isEmpty(PlistHelper.getInstance().getHomePage())) {
                    currentUrl = PlistHelper.getInstance().getHomePage();
                }
                loadUrl(true, currentUrl);
                return;
            }

            counter_url = 0;
            barGoneClicked = false;
            mydomainame = "";
            sameDomain = false;
            callSafeSearch(contents, UserPref.getInstance().getScanType());
        } else {
            showNetworkErrorDialog();
        }
    }

    private void clearTrackerList() {
        if (trackerHelper != null && trackerHelper.getTrackerCount() > 0) {
            GlobalVariables.getInstnace().tempTrackerFoundList = new ArrayList<>();
            for (TrackerFoundModel trackerFoundModel : trackerHelper.getTrackerList()) {
                if (trackerFoundModel != null) {
                    GlobalVariables.getInstnace().tempTrackerFoundList.add(trackerFoundModel.clone());
                }
            }
            trackerHelper.clearTrackerList();
            updateTrackerCount(0);
        }
    }

    public void showToast(String i) {
        showToast(i, getActivity());
    }

    public void showToast(int i) {
        showToast(getString(i), getActivity());
    }

    private void callSafeSearch(String contents, int searchType) {
        if (Patterns.WEB_URL.matcher(contents).matches()) {
            currentUrl = contents.trim();
            currentSearchTerm = null;
            cleanUrl(contents);
            cleanUrlFirst(contents);
            showLoader();

            if (canSafeSearched()) {
                checkSafety(currentUrl);
                return;
            }
            loadNewWithoutSafetyCheck(currentUrl);
        } else {
            makeDefaultSearch(contents, searchType);
        }
    }

    private boolean isUrlMatchesSearchEngine(String url) {
        return url.contains("google.com/search?")
                || url.contains("bing.com/search?")
                || url.contains("search.yahoo.com/search?")
                || url.contains("search.aol.com/aol/search?")
                || url.contains("ask.com/web?");
    }

    private void makeDefaultSearch(String contents, int searchType) {
        boolean isOtherSearch = false;
        String queryURL = "";
        switch (searchType) {
            case 0:
                currentSearchTerm = contents;
                // Clear search results
                showLoader();
                search(contents, false);
                break;
            case 1:
                isOtherSearch = true;
                queryURL = "https://www.google.com/search?q=";
                break;
            case 2:
                isOtherSearch = true;
                queryURL = "http://www.bing.com/search?q=";
                break;
            case 3:
                isOtherSearch = true;
                queryURL = "https://search.yahoo.com/search?p=";
                break;
            case 4:
                isOtherSearch = true;
                queryURL = "http://search.aol.com/aol/search?q=";
                break;
            case 5:
                isOtherSearch = true;
                queryURL = "http://www.ask.com/web?q=";
                break;
        }
        if (isOtherSearch) {
            String charset = "UTF-8";
            try {
                URL completeUrl = new URL(queryURL + URLEncoder.encode(contents, charset));
                showLoader();
                loadNewWithoutSafetyCheck(completeUrl.toString());
                if (urlWebView.getVisibility() == View.GONE) {
                    urlWebView.setVisibility(View.VISIBLE);
                }
            } catch (MalformedURLException e) {

            } catch (UnsupportedEncodingException e) {

            } catch (Exception e) {

            }
        }
    }

    private void showNetworkErrorDialog() {
        new FinjanDialogBuilder(getActivity()).setMessage(getString(R.string.network_message))
                .setPositiveButton(getString(R.string.alert_yes))
                .setNegativeButton(getString(R.string.alert_no)).show();
    }

    public void searchResultDisplayed(int currentDisplayIndex) {
        // Request additional search results if we're getting close to the end
        final int minResultBuffer = 4;
        if (currentDisplayIndex + minResultBuffer >= searchResults.size()) {
            // Time to request more results!
            search(currentSearchTerm, true);
        }
    }

    public SearchResult resultAtIndex(int index) {
        SearchResult result;
        try {
            result = searchResults.get(index);
        } catch (IndexOutOfBoundsException e) {
            result = null;
        }
        return result;
    }

    public void loadSearchResult(int index) {
        isNavigationPresssed = false;
        if (isTabSizeUnderLimit()) {
            showTabOverSizeDialog();
        } else if (index >= 0 && index < searchResults.size()) {

            SafetyResult safetyResult = searchResults.get(index).safetyResult;
            if (safetyResult != null && !safetyResult.status.equals("overloaded")) {

                setReportResponse(safetyResult);
                hideSearchResultsView();
//                showLoader();
                loadNew(safetyResult);
            } else {

                hideSearchResultsView();
                SearchResult searchvalue = searchResults.get(index);
                if (searchvalue != null && !TextUtils.isEmpty(searchvalue.displayUrl)) {
//                    omnibarResults(searchvalue.displayUrl);

                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.BKEY_REDIRECTED_FUNCTION, AppConstants.RFUC_URL_NON_SAFTY_RESULT);
                    bundle.putString(AppConstants.BKEY_REDIRECT_URL, searchvalue.displayUrl);
                    setChangeCarrier(bundle);
                }
            }
        }
    }

    public void showReportAtSearchIndex(int index) {
        if (index >= 0 && index < searchResults.size()) {
            SearchResult searchResult = searchResults.get(index);
            SafetyResult safetyResult = searchResult.safetyResult;
            if (safetyResult != null && !safetyResult.status.equals("overloaded")) {
                setReportResponse(safetyResult);
                setUrlEditText(searchResult.displayUrl, false);
            }
        }
    }

    /*
        Searching
     */
    public void search(String searchString, boolean append) {
//        String bingSearchApiKey = Utils.getPlistvalue(Constants.KEY_SEARCH_API_KEY, Constants.KEY_BING);
        String bingSearchApiKey = PlistHelper.getInstance().getBingKey();
//      String authCredentials = Constants.subscriptions_key;
        String authCredentials = bingSearchApiKey;
        String encodedApiKey = "Basic " + Base64.encodeToString(authCredentials.getBytes(), Base64.NO_WRAP);
        BingRestClient bingRestClient = new BingRestClient(encodedApiKey);
        final BingService bingService = bingRestClient.getBingService();
        int resultStart = 0;
        if (append) {
            resultStart = searchResults.size();
        }
        new Handler().post(getSearchReport(bingService, searchString, resultStart));
    }

    private Runnable getSearchReport(final BingService bingService, final String searchString, final int resultStart) {
        return new Runnable() {
            @Override
            public void run() {
                String filter = "webpages";
//                String bingSearchApiKey = Utils.getPlistvalue(Constants.KEY_SEARCH_API_KEY, Constants.KEY_BING);
//                String authCredentials = Constants.subscriptions_key;
//                String authCredentials = Utils.getPlistvalue(Constants.KEY_SEARCH_API_KEY, Constants.KEY_BING);
                String authCredentials = PlistHelper.getInstance().getBingKey();

                String encodedApiKey = authCredentials;
                String urlEncodedSearchString;
                try {
                    urlEncodedSearchString = URLEncoder.encode(searchString, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    return;
                }
                urlEncodedSearchString = "bing/v5.0/search?q=" + urlEncodedSearchString;
                try {
                    Call<BingSearchResponse> searchQuery = bingService.search(encodedApiKey, urlEncodedSearchString, resultStart, 12, filter);
                    searchQuery.enqueue(new Callback<BingSearchResponse>() {
                        @Override
                        public void onResponse(Call<BingSearchResponse> call, Response<BingSearchResponse> response) {

                            if (response != null && response.isSuccessful() && response.code() == 200 && response.body() != null && response.body().toString().length() > 0) {
                                BingSearchResponse searchResult = response.body();
                                searchResults.clear();
                                try {
                                    SearchResult[] temp = searchResult.webPages.value;
                                    if (temp != null && temp.length > 0) {
                                        searchResults.addAll(Arrays.asList(temp));
                                        showSearchResultsView();
                                    } else {
                                        startGoogleSearch();
                                    }
                                } catch (Exception e) {
                                    startGoogleSearch();
                                }
                            } else {
                                startGoogleSearch();
                            }
                        }

                        @Override
                        public void onFailure(Call<BingSearchResponse> call, Throwable t) {
                            startGoogleSearch();
                        }
                    });
                } catch (Exception e) {
                    startGoogleSearch();
                    e.printStackTrace();
                }
            }
        };
    }

    private void startGoogleSearch() {
        if (getCurrentFragmentName().equalsIgnoreCase(SearchedResultFragment.class.getSimpleName())) {
            popCurrentFragment();
        }
        String queryURL = "https://www.google.com/search?q=";
        String search = urlEditText.getText().toString().trim();
        String charset = "UTF-8";
        try {
            URL completeUrl = new URL(queryURL + URLEncoder.encode(search, charset));
//            newTab = false;
            loadUrl(true, completeUrl.toString());
            if (urlWebView.getVisibility() == View.GONE) {
                urlWebView.setVisibility(View.VISIBLE);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void checkSafety(int index) {
        SearchResult result;
        try {
            result = searchResults.get(index);
        } catch (IndexOutOfBoundsException e) {
            // Something must have gotten out of sync, we don't even have a result at that index.
            return;
        }
        if (result.safetyResult == null) {
//            String localDB = Utils.getPlistvalue(Constants.KEY_USE_LOCAL_DB, "");
            String localDB = PlistHelper.getInstance().getUserLocalDb();
            if (UserPref.getInstance().getSafeSearchPurchase() || NativeHelper.getInstnace().getScanRemain() > 0) {
                safetyHandler.post(getSafetyReport(safetyService, result.url, index, 0, 1, TextUtils.isEmpty(localDB) ? 0 : Integer.parseInt(localDB)));
            } else {
                if (searchedResultFragment != null) {
                    searchedResultFragment.updateSafetyRatingAppPurchaseFalse(index);
                }
            }
        }
    }

    private void loadUrl(boolean shouldLoad, String url) {
        if (shouldLoad && !isNavigationPresssed) {
            currentUrl = url;
            Logger.logE(TAG, "Loading url ");
            urlWebView.loadUrl(currentUrl);
//            showLoader();
        }
        setBottomBars((FragmentHome) ParentHomeFragment.this, true);
        setUrlEditText(urlWebView.getUrl(), false);
        isNavigationPresssed = false;
    }

    private void setUrlEditText(String url, boolean forcefully) {

        if (TextUtils.isEmpty(url) || url.equalsIgnoreCase(AppConstants.EMPTY_URL)) {
            url = "";
        }
        if (forcefully) {
            urlEditText.setText(url);
            return;
        }
        if (!isUrlInEditForm) {
            urlEditText.setText(url);
        }
    }

    @Override
    public void hideSoftKeyBoard() {
        removeUrlCleaner();
        urlEditText.clearFocus();
        showWebViewControls();
    }

    public void checkSafety(String url) {
        if (!NativeHelper.getInstnace().checkActivity(getActivity())) {
            return;
        }
        if (isUrlMatchesSearchEngine(url)) {
            loadNewWithoutSafetyCheck(url);
            return;
        }
//        if (isPhoneReadStatePermissionGranted()) {
//            Intent intent = new Intent(getActivity(), UpdateSafeCountService.class);
//            Bundle bundle = new Bundle();
//            bundle.putString("type", Constants.UPDATE_PARAM);
//            bundle.putString("uuid", UserPref.getInstance().getDeviceID());
//            intent.putExtras(bundle);
//            getActivity().startService(intent);
//            String localDB = Utils.getPlistvalue(Constants.KEY_USE_LOCAL_DB, "");
        String localDB = PlistHelper.getInstance().getUserLocalDb();
        barGoneClicked = false;
        safetyHandler.post(getSafetyReport(safetyService, url, -1, 0, 1, TextUtils.isEmpty(localDB) ? 0 : Integer.parseInt(localDB)));
//        } else {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, AppConstants.READ_PHONE_STATE_PERMISSION);
//        }
    }

    public void checkSafetyVirus(String url) {
        safetyHandler.post(getSafetyReport(safetyService, url, -1, 0, 1, 0));
    }

    /**
     * @param safetyService The SafetyService to use
     * @param url           The url to check.  Must not be an invalid url.
     * @param index         The index within the searchResults, used for validation that the results properly
     *                      match up with the original url searched for.  Use -1 if this report is not from
     *                      a search.
     * @param parent        Redirect parent.
     * @param version       New api version.
     * @param use_local_db  Whether to use DB at api end or not.
     * @return Runnable, that can be posted to a Handler to be run asynchronously.
     */
    private Runnable getSafetyReport(final SafetyService safetyService, final String url, final int index, final int parent, final int version, final int use_local_db) {
        return new Runnable() {
            @Override
            public void run() {
                // Make sure parentString is null if parent is 0, so server knows to disregard.
                String parentString = null;
                isSafetyResultFetching = true;

                if (parent != 0) {
                    parentString = Integer.toString(parent);
                }

                Call<SafetyResult> safetyResult = safetyService.getSafetyReport(url, parentString, version, use_local_db);
                Logger.logE(TAG, "safety url " + safetyResult.request().url().toString() + url);
                safetyResult.enqueue(new Callback<SafetyResult>() {
                    @Override
                    public void onResponse(Call<SafetyResult> call, Response<SafetyResult> response) {
                        isSafetyResultFetching = false;
                        dismissLoader();

                        if (response != null && response.isSuccessful() && response.code() == 200) {
                            if (response.body() != null && response.body().toString().length() > 0) {
                                if (vtScan) {
                                    DbHelper.getInstnace().updateOldTab(response.body(), urlWebView.getTitle(), url, isPrivate, id);
                                }
                                successfulSafetyResultReceived(response.body(), index);
                                updateSafeScanCount();
                            } else {
                                loadUrl(true, url);
                                setReportReferences();
                            }
                        } else {
                            loadUrl(true, url);
                            setReportReferences();
                        }
                        if (vtScan) {
                            vtScan = false;
                        }
                    }

                    @Override
                    public void onFailure(Call<SafetyResult> call, Throwable t) {
//                        urlEditText.setAsEditable();
                        isSafetyResultFetching = false;
                        currentSafetyResult = null;
                        isReportAvailable = false;
                        if (!vtScan) {
                            loadNewWithoutSafetyCheck(url);
                        } else {
                            vtScan = false;
                            DbHelper.getInstnace().updateOldTab(null, urlWebView.getTitle(), url, isPrivate, id);
                            setTrackFab(trackerHelper != null ? trackerHelper.getTrackerCount() : 0);
                        }
//                        ParentHomeFragment.this.showToast("Unable to load Safety Result...");
                        dismissLoader();
//                        makeDefaultSearch(url, UserPref.getInstance().getScanType());
                    }
                });
            }
        };
    }
//    private boolean goingUp = false;

    public void successfulSafetyResultReceived(final SafetyResult localSafetyResult, final int index) {
        if (localSafetyResult != null) {
            localSafetyResult.setAttributes();
            dismissLoader();
            if (localSafetyResult.rating.equals("invalid") && !localSafetyResult.status.equals("overloaded")) {
                // Invalid for some reason.  Determine what went wrong and handle appropriately
                if (localSafetyResult.status.toLowerCase().equalsIgnoreCase("invalid api key")
                        || localSafetyResult.status.toLowerCase().contains("invalid")) {
                    if (TextUtils.isEmpty(localSafetyResult.url) || localSafetyResult.url.equalsIgnoreCase(AppConstants.EMPTY_URL)) {
                        showWebViewControls();
                    } else {
                        makeDefaultSearch(localSafetyResult.url, UserPref.getInstance().getScanType());
                    }
                } else if (localSafetyResult.status.equals("queued")) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadUrl(true, localSafetyResult.url);
                            ParentHomeFragment.this.showToast("Taking more than expected,please try again later.");
                            showWebViewControls();
                            if (isUpScrolling) {

                                showHeaderTitle(false);
                            }
                        }
                    }, 1000);
                }
            } else if (index != -1) {
                // Verify that the url we looked up is still the url we're storing in that location
                SearchResult searchResult;
                try {
                    searchResult = searchResults.get(index);
                } catch (IndexOutOfBoundsException e) {
                    // Just do nothing, I guess we cleared out our search results since making this request
                    return;
                }
//                if (searchResult.value[0].url.equals(localSafetyResult.url)) {
                if (searchResult.url.equals(localSafetyResult.url)) {
                    // Everything checks out.  Update the SafetyResult of this SearchResult.
                    searchResult.safetyResult = localSafetyResult;
                    if (searchedResultFragment != null) {
                        searchedResultFragment.safetyResultReceiver(index);
                    }
                }
            } else if (!localSafetyResult.status.equals("overloaded")) {
                // We're not searching, we simply have a score for the url.
                setReportResponse(localSafetyResult);
                loadNew(localSafetyResult);
            } else {
                // We got an empty response body
                failedToGetReport();
            }
        } else {
            // We got an empty response body
            dismissLoader();
            failedToGetReport();
        }
        refreshTabCount();
    }

    private void redirectUrl(String url) {
        if (url.startsWith("intent:/")) {
            openAppFromURl(url);
        } else if (url.startsWith("market:/")) {
            openplayStore(url);
        }
//        urlEditText.clearFocus();
    }

    private void openplayStore(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        setUrlEditText("", false);
        setBackForwordClick(false, false);
        urlEditText.clearFocus();
    }

    private void openAppFromURl(String url) {
        try {
            if (url.contains(AppConstants.PACKAGE) || url.contains(AppConstants.SCHEMA)) {
                Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage(url);
                if (intent != null) {
                    startActivity(intent);
                } else {
                    makeDefaultSearch(url, UserPref.getInstance().getScanType());
                }
            }

        } catch (ActivityNotFoundException e) {
            makeDefaultSearch(url, UserPref.getInstance().getScanType());
        }

        setBackForwordClick(false, false);

        if (!TextUtils.isEmpty(UserPref.getInstance().getHomePage())) {
            currentUrl = UserPref.getInstance().getHomePage();

        } else if (!TextUtils.isEmpty(PlistHelper.getInstance().getHomePage())) {
            currentUrl = PlistHelper.getInstance().getHomePage();
        } else {
            currentUrl = AppConstants.EMPTY_URL;
        }

        loadUrl(true, currentUrl);
    }

    private Intent getLauncherIntent(String url) {
        Intent intent = null;
        String packageName = "";
        String searchQuery = "";
        if (url.contains(AppConstants.PACKAGE)) {
            searchQuery = AppConstants.PACKAGE;
        } else if (url.contains(AppConstants.SCHEMA)) {
            searchQuery = AppConstants.SCHEMA;
        }
        if (url.contains(";")) {
            String[] values = url.split(";");
            if (values.length > 0) {
                for (String urls : values) {
                    if (urls.startsWith(searchQuery)) {
                        packageName = urls;
                        break;
                    }
                }
                if (packageName.contains("=")) {
                    int index = packageName.indexOf("=");
                    packageName = packageName.substring(index + 1);
                }
                if (!packageName.isEmpty()) {
                    intent = getActivity().getPackageManager().getLaunchIntentForPackage(packageName);
                }
            }
        }
        return intent;
    }

    /*
     Utility functions
     */
    public void cleanUrlFirst(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        firstUrl = url;
    }

    public void cleanUrl(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        currentUrl = url;
    }

    public String addWwwIfRequired(String url) {
        if (url.startsWith("http://")) {
            if (!url.startsWith("http://www.")) {
                currentUrl = url.replace("http://", "http://www.");
            }
        } else if (url.startsWith("https://")) {
            if (!url.startsWith("https://www.")) {
                currentUrl = url.replace("https://", "https://www.");
            }
        }
        return currentUrl;
    }

    public void clearCookies() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
            privateCookieManager.removeAllCookies(null);
            normalCookieManager.removeAllCookies(null);
            normalCookieManager.removeSessionCookies(null);
            privateCookieManager.removeSessionCookies(null);
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(getActivity());
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
            privateCookieManager.removeAllCookie();
            normalCookieManager.removeAllCookie();
            normalCookieManager.removeSessionCookie();
            privateCookieManager.removeSessionCookie();
        }
        privateCookie = CookieSyncManager.createInstance(getContext());
        normalCookie = CookieSyncManager.createInstance(getContext());
        WebViewDatabase.getInstance(getContext()).clearHttpAuthUsernamePassword();
        WebViewDatabase.getInstance(getContext()).clearUsernamePassword();
        WebViewDatabase.getInstance(getContext()).clearFormData();
    }

    public boolean isStoragePermissionGranted() {

        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                (getActivity() != null && getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED);

    }

    public boolean isPhoneReadStatePermissionGranted() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                getActivity().checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
                        == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void showOptionalMsg() {
        String optionalMessage = Utils.getNotificationPlistvalue(Constants.KEY_NOTIFICATION, Constants.KEY_CHANNELS, Constants.KEY_OPTIONAL_MESSAGE);
        if (!TextUtils.isEmpty(optionalMessage)) {
            new FinjanDialogBuilder(getActivity()).setMessage(optionalMessage).show();
        }
    }

    private void setReferences(View parentView) {

        home = (ViewGroup) parentView.findViewById(R.id.home);

        //main webview of home fragment
        urlWebView = (VideoEnabledWebView) parentView.findViewById(R.id.urlWebView);


        //url header
        header = (ViewGroup) parentView.findViewById(R.id.header);
        searchIcon = (ImageView) parentView.findViewById(R.id.searchIcon);
        iv_tabsopened = (ImageView) parentView.findViewById(R.id.iv_tabsopened);
        iv_vpn = (ImageView) parentView.findViewById(R.id.iv_vpn);

        btn_cancel = (TextView) parentView.findViewById(R.id.btn_cancel);
        progressbar1 = (ProgressBar) parentView.findViewById(R.id.progressbar1);
        progressbar2 = (ProgressBar) parentView.findViewById(R.id.progressbar2);

        webViewControls = (ViewGroup) parentView.findViewById(R.id.webViewControls);
        headerCover = (TextView) parentView.findViewById(R.id.header_cover);
        ll_urlview = (LinearLayout) parentView.findViewById(R.id.ll_urlview);
        rl_urlview = (RelativeLayout) parentView.findViewById(R.id.rl_urlview);
        urlEditText = (UrlEditText) parentView.findViewById(R.id.urlEditText);
        iv_refresh = (ImageView) parentView.findViewById(R.id.iv_refresh);
//        tv_url = (TextView) parentView.findViewById(R.id.tv_url);
        urlCleaner = (ImageView) parentView.findViewById(R.id.img_clear_url);
        iv_menu_icon = (ImageView) parentView.findViewById(R.id.iv_settings);
//        iv_backsettings = (ImageView) parentView.findViewById(R.id.iv_backsettings);
        txt_tabCount = (TextView) parentView.findViewById(R.id.txt_tabcount);
        backButton = (ImageView) parentView.findViewById(R.id.backButton);
        forwardButton = (ImageView) parentView.findViewById(R.id.forwardButton);
        urlEditText.delegate = this;
        iv_vpn.setOnClickListener(this);
        urlCleaner.setOnClickListener(this);
        txt_tabCount.setVisibility(View.VISIBLE);

        //webview container
        webViewContainer = (RelativeLayout) parentView.findViewById(R.id.webViewContainer);

        //what if page unable to load
        txt_webloadError = (TextView) parentView.findViewById(R.id.txt_webload_error);

        //first understood what is it.
        scanningViewContainer = (ViewGroup) parentView.findViewById(R.id.scanningViewContainer);
        scanningVideo = (VideoView) parentView.findViewById(R.id.scanningVideo);
        cancelScanContainer = (ViewGroup) parentView.findViewById(R.id.cancelScanContainer);
        cancelButton = (ViewGroup) parentView.findViewById(R.id.cancelButton);


        iv_menu_bookmark = (ImageView) parentView.findViewById(R.id.iv_bookmark);
        iv_menu_vpn = (ImageView) parentView.findViewById(R.id.iv_connection);
        rl_menu_tracker = (RelativeLayout) parentView.findViewById(R.id.rl_tracker);

        view_drawer_bottom = (LinearLayout) parentView.findViewById(R.id.view_drawer_bottom);

        //tracker count
        tvTrackerCount = (TextView) parentView.findViewById(R.id.tv_tracker);
        tvTrackerCount.setTypeface(null, Typeface.BOLD);

        iv_menu_home = (ImageView) parentView.findViewById(R.id.iv_home);
        iv_menu_home.setOnClickListener(this);

        rl_menu_tracker.setOnClickListener(this);
        setTrackerLayout();


        parentView.findViewById(R.id.iv_bookmark).setOnClickListener(this);
        parentView.findViewById(R.id.iv_connection).setOnClickListener(this);


        //click listeners
//        (parentView.findViewById(R.id.iv_refresh)).setOnClickListener(this);
        (parentView.findViewById(R.id.iv_tabsopened)).setOnClickListener(this);
        (parentView.findViewById(R.id.iv_settings)).setOnClickListener(this);
        (parentView.findViewById(R.id.backButton)).setOnClickListener(this);
        (parentView.findViewById(R.id.forwardButton)).setOnClickListener(this);
    }

    private void setTrackerLayout() {
        if (PlistHelper.getInstance().getIsTrackerEnabled()) {
            rl_menu_tracker.setAlpha(1f);
            rl_menu_tracker.setClickable(true);
        } else {

            rl_menu_tracker.setAlpha(.5f);
            rl_menu_tracker.setClickable(false);
        }
    }

    private void showHeaderTitle(boolean visibility) {

        int headerVisibility = visibility ? View.VISIBLE : View.GONE;
        int otherVisibility = (!visibility) ? View.VISIBLE : View.GONE;


        iv_tabsopened.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
//        tv_url_new.setVisibility(headerVisibility);
        webViewControls.setVisibility(otherVisibility);
        ll_urlview.setVisibility(otherVisibility);
        txt_tabCount.setVisibility(otherVisibility);

//        tv_url_new.setTextSize(18.0f);
    }




    public void showHeader() {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            progressbar2.setVisibility(View.GONE);
            progressbar1.setVisibility(View.VISIBLE);
            final AnimatorSet animatorSet = AnimationHelper.getInstance().buildAnimationSet(getActivity(), 250,
                    AnimationHelper.getInstance().buildAnimation(header, -mParallaxImageHeight, 0));
            animatorSet.start();
            urlHeaderShowing = true;

            final AnimatorSet animatorSet1 = AnimationHelper.getInstance().buildAnimationSet(getActivity(), 250,
                    AnimationHelper.getInstance().buildAnimation(view_drawer_bottom, drawerBottomBarHeight, 0));
            animatorSet1.start();

            final AnimatorSet animatorSet2 = AnimationHelper.getInstance().buildAnimationSet(getActivity(), 250,
                    AnimationHelper.getInstance().buildAnimation(urlWebView, 0, mParallaxImageHeight));
            animatorSet2.start();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    layoutParams.setMargins(0, 0, 0, drawerBottomBarHeight);
                    webViewContainer.setLayoutParams(layoutParams);
                }
            }, 500);


            GlobalVariables.getInstnace().isDrawerBottomShowing = true;
            refreshDisplayHeight();
        }
    }

    public void hideUrlHeader() {
        if (mScrollY < mParallaxImageHeight) {
            return;
        }
        progressbar1.setVisibility(View.GONE);
        progressbar2.setVisibility(View.VISIBLE);

//        setAnim(container,LayoutTransition.CHANGE_APPEARING);
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
/*
            if (mScrollY >= mParallaxImageHeight)
*/
            urlHeaderShowing = false;

         /*  if (mScrollY < mParallaxImageHeight) {
               final AnimatorSet animatorSet = AnimationHelper.getInstance().buildAnimationSet(getActivity(), 0,
                       AnimationHelper.getInstance().buildAnimation(header, 0,
                               -mScrollY));
           }
           else {
               final AnimatorSet animatorSet = AnimationHelper.getInstance().buildAnimationSet(getActivity(), 0,
                       AnimationHelper.getInstance().buildAnimation(header, 0,
                               -mParallaxImageHeight));
               animatorSet.start();
           }*/
//            header.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                }
            }, 10);

            final AnimatorSet animatorSet = AnimationHelper.getInstance().buildAnimationSet(getActivity(), 250,
                    AnimationHelper.getInstance().buildAnimation(header, 0,
                            -mParallaxImageHeight));
            animatorSet.start();
            final AnimatorSet animatorSet2 = AnimationHelper.getInstance().buildAnimationSet(getActivity(), 250,
                    AnimationHelper.getInstance().buildAnimation(urlWebView, mParallaxImageHeight,
                            0));
            animatorSet2.start();


            final AnimatorSet animatorSet1 = AnimationHelper.getInstance().buildAnimationSet(getActivity(), 250,
                    AnimationHelper.getInstance().buildAnimation(view_drawer_bottom, 0,
                            drawerBottomBarHeight));
            animatorSet1.start();

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 0);
            webViewContainer.setLayoutParams(layoutParams);

            GlobalVariables.getInstnace().isDrawerBottomShowing = false;
            refreshDisplayHeight();
        }
    }


    private void defaultOnPageFinished(String url) {

        removeUrlCleaner();
        dismissLoader();

        if (!url.equals(AppConstants.EMPTY_URL)) {
            hintVisibility();
            setBottomBars((FragmentHome) this, true);
//            firstLaunch = false;
            if (isFailour) {
                webViewContainer.setVisibility(View.GONE);
                urlWebView.setVisibility(View.GONE);
            }
        }
    }

    private void fixTabId() {
        if (id == null && newTab) {
            id = DbHelper.getInstnace().getNewDefaultId();
        }
        if (!GlobalVariables.getInstnace().isFirstFragmentIdFixed) {
            GlobalVariables.getInstnace().isFirstFragmentIdFixed = true;
            updateFragmentMap(id, (FragmentHome) ParentHomeFragment.this);
        } else {
            if (getFragmentMap() != null) {
                addFragmentToMap(id, (FragmentHome) ParentHomeFragment.this);
            }
        }
    }

    private void addToFragmentMap() {
        if (getFragmentMap() != null && id != null) {
            addFragmentToMap(id, (FragmentHome) ParentHomeFragment.this);
        }
    }

    private void setWebViewBackforward() {
        WebBackForwardList history = urlWebView.copyBackForwardList();
        int backwardIndex = -1;
        backWardUrl = null;

        while (urlWebView.canGoBackOrForward(backwardIndex)) {
            if (history.getItemAtIndex(history.getCurrentIndex() + backwardIndex) != null &&
                    !(history.getItemAtIndex(history.getCurrentIndex() + backwardIndex).getUrl().equals(AppConstants.EMPTY_URL))) {
//                urlWebView.goBackOrForward(backwardIndex);
                backWardUrl = history.getItemAtIndex(history.getCurrentIndex() + backwardIndex).getUrl();
                Logger.logE("tag", "first non empty" + backWardUrl);
                this.backwardIndex = backwardIndex;
                break;
            }
            backwardIndex--;
        }
        // no history found that is not empty
        int forwardIndex = 1;
        forwarUrl = null;

        while (urlWebView.canGoBackOrForward(forwardIndex)) {
            if ((history.getItemAtIndex(history.getCurrentIndex() + forwardIndex) != null) &&
                    !(history.getItemAtIndex(history.getCurrentIndex() + forwardIndex).getUrl().equals(AppConstants.EMPTY_URL))) {
//                urlWebView.goBackOrForward(forwardIndex);
                forwarUrl = history.getItemAtIndex(history.getCurrentIndex() + forwardIndex).getUrl();
                this.forwardIndex = forwardIndex;
                Logger.logE("tag", "first non empty" + forwarUrl);
                break;
            }
            forwardIndex++;

        }
        // no history found that is not empty
        if (forwarUrl == null && backWardUrl == null) {
            setBackForwordClick(false, false);
        } else if (!TextUtils.isEmpty(backWardUrl) && TextUtils.isEmpty(forwarUrl)) {
            setBackForwordClick(true, false);
        } else if (!TextUtils.isEmpty(forwarUrl) && TextUtils.isEmpty(backWardUrl)) {
            setBackForwordClick(false, true);
        } else if (!TextUtils.isEmpty(backWardUrl) && !TextUtils.isEmpty(forwarUrl)) {
            setBackForwordClick(true, true);
        }
    }

    private void setHttpsLock(String url) {
        if (NativeHelper.getInstnace().checkContext(getContext())) {
            if (url.startsWith("https")) {
                Drawable drawable = ResourceHelper.getInstance().getDrawable(R.drawable.password);
                drawable.setAlpha(170);
//                drawable.setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_1), PorterDuff.Mode.ADD);
                urlEditText.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                urlEditText.setCompoundDrawablePadding(25);
            } else {
                urlEditText.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                urlEditText.setCompoundDrawablePadding(0);
            }
        }
        urlEditText.setTextColor(Color.WHITE);

    }

    @Override
    public void updateByteCount(long in, long out, long diffIn, long diffOut) {
        TrafficController trafficController = TrafficController.getInstance(FinjanVPNApplication.getInstance());
        final long traffic = in + out + trafficController.getTraffic();
//            final long  traffic = in + trafficController.getTraffic();
        final long trafficLimit = trafficController.getTrafficLimit();

        if (FinjanVPNApplication.getInstance() != null &&
                VpnUtil.isVpnActive(FinjanVPNApplication.getInstance().getApplicationContext())) {
//            if(testBoolean){
//                checkIfLimitExceds(trafficLimit,trafficLimit);
//                return;
//            }
            checkIfLimitExceds(traffic, trafficLimit);
        }
    }

    private void stopVpn() {
        if (GlobalVariables.getInstnace().mainActivityFragment != null) {
            NativeHelper.getInstnace().showToast(ParentHomeFragment.this.getContext(), "Disconnecting VPN, Data limit reached", Toast.LENGTH_SHORT);
            GlobalVariables.getInstnace().mainActivityFragment.disconnectService();
        }
    }

    private void checkIfLimitExceds(long traffic, long trafficLimit) {
        if (trafficLimit > 0) {
            double percentage = (traffic * 100d) / trafficLimit;
            percentage = Math.floor(percentage * 100) / 100;

            if (percentage >= 100) {
                if (NativeHelper.getInstnace().checkActivity(ParentHomeFragment.this.getActivity())) {
                    ParentHomeFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setVpnIcon();
                            stopVpn();
                            showUpgradeBarIfRequired();
                        }
                    });
                }
            }
        }
    }

    private class UrlOverrideWebViewClient extends WebViewClient {


        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            showLoginAlertDialog(handler, view.getUrl().toString());
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            Logger.logE("qwerty", "" + view.getHitTestResult());
            isPagetRedirected = true;
            isNavigationPresssed = false;
            removeUrlCleaner();
//            isPagetRedirected=true;
//            redirect = false;
            counter_url += 1;
            hideKeyboard(home);
            cleanUrl(url);
            String urlDomain = "";
            try {
                if (counter_url == 1) {
                    cleanUrlFirst(url);
                    mydomainame = getDomainName(firstUrl);
                }
                urlDomain = getDomainName(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // if(counter_url >= 1 && mydomainame.trim().equalsIgnoreCase(urlDomain) && tb_skipscan.isChecked()){
            if (counter_url >= 1 && mydomainame.trim().equalsIgnoreCase(urlDomain)) {
                AppLog.printLog("Same Domain", "Same Domain");
                sameDomain = true;
            }
//            showLoader();
            if (!Patterns.WEB_URL.matcher(url).matches() && checkApplaunch(url)) {
                redirectUrl(url);
            } else {
                if (Patterns.WEB_URL.matcher(url).matches()) {
                    if (canSafeSearched()) {
                        checkSafety(url);
                    } else {
                        loadUrl(true, url);
                    }
                }
            }
            return true;
        }


        @Override
        public void onPageStarted(WebView view, final String url, Bitmap favicon) {
            Logger.logE(TAG, "Timer  onPageStarted " + System.currentTimeMillis());
            isPagetRedirected = true;
            setHomeIcon(!TextUtils.isEmpty(url) && url.equals(UserPref.getInstance().getHomePage()));
            setUrlEditText(url, false);
            setHttpsLock(url);
//            findTrackerUsingScript(url);
//            newTrackerHandler.postDelayed(getTrackerRunnable,1000);
            setBookMark(ifBookMark());
            setTitle(view.getTitle());
            String title = view.getTitle() != null ? view.getTitle() : "";
            fixTabId();

            if (newTab) {
                id = DbHelper.getInstnace().addNewTab(currentSafetyResult, title, isPrivate);
                newTab = false;
                updateFragmentId((FragmentHome) ParentHomeFragment.this, id);
            } else {
                tabhistory tabhistory = DbHelper.getInstnace().getTabHistoy(id);
                if (tabhistory == null) {
                    id = DbHelper.getInstnace().addNewTab(currentSafetyResult, title, isPrivate);
                    updateFragmentId((FragmentHome) ParentHomeFragment.this, id);
                } else {
                    DbHelper.getInstnace().updateOldTab(currentSafetyResult, title, url, isPrivate, id);
                }

            }
            currentUrl = url;
            urlTitle = view.getTitle();
            manageCookie(url);

            index = 0;
//            findTrackerUsingScript(url);
            if (webViewContainer.getVisibility() == View.GONE || urlWebView.getVisibility() == View.GONE) {
                webViewContainer.setVisibility(View.VISIBLE);
                urlWebView.setVisibility(View.VISIBLE);
            }
//            if (!isUpScrolling) {
//                showWebViewControls();
//            }
//            updateTitle(urlTitle);

            setTheme();
//            urlEditText.clearFocus();
            setUrlEditText(url, false);
            refreshTabCount();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    dismissLoader();
                    setProgressbarColor(UserPref.getInstance().getSafeScanEnabled());
                }
            }, 1500);
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            if (GlobalVariables.blockTracking && UserPref.getInstance().getTracker()) {
                List<Tracker> trackerList = AnalyticsApplication.daoSession.getTrackerDao().loadAll();
                if (trackerList != null && trackerList.size() > 0) {
                    for (webHistoryDatabase.Tracker urlValue : trackerList) {
                        if (url.contains(urlValue.getUrl())) {
                            return new WebResourceResponse("text/css", "UTF-8", null);
                        }
                    }
                }
            }
            return super.shouldInterceptRequest(view, url);
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            Logger.logE(TAG, "Timer  onPageCommitVisible " + System.currentTimeMillis());
            findTrackerUsingScript(url);
            dismissLoader();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Logger.logE(TAG, "Timer  onPageFinished " + System.currentTimeMillis());
            urlTitle = view.getTitle();
//            scrollToTop();
            currentUrl = url;
            isPagetRedirected = false;
//            findTrackerUsingScript();

            defaultOnPageFinished(url);
            if (!url.equals(AppConstants.EMPTY_URL)) {
                setTitle(view.getTitle());
                String title = view.getTitle() != null ? view.getTitle() : "";

                DbHelper.getInstnace().updateOldTab(currentSafetyResult, title, url, isPrivate, id);
                DbHelper.getInstnace().addToWebHistory(currentSafetyResult, title, url, isPrivate, vtScan);

            } else {
//                fixTabId();
                if (newTab) {
                    id = DbHelper.getInstnace().addNewTab(currentSafetyResult, "", isPrivate);
                    newTab = false;
                    addToFragmentMap();
//                    updateFragmentId((FragmentHome) ParentHomeFragment.this, id);

                } else {
                    DbHelper.getInstnace().updateOldTab(currentSafetyResult, "", url, isPrivate, id);
                }
                refreshTabCount();
            }
            setBookMark(ifBookMark());
            Logger.logE(TAG, "onPageStartedFinished");
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            String desc = description;
            Logger.logE(TAG, "onReceivedError " + desc + "error code " + errorCode);
            if (description.contains("ERR_NAME_NOT_RESOLVED") ||
                    description.contains("Couldn't find the URL")) {
                if (!failingUrl.equals(addWwwIfRequired(failingUrl))) {
                    loadUrl(true, addWwwIfRequired(failingUrl));
                    return;
                } else {
//                    super.onReceivedError(view, errorCode, description, failingUrl);
                    toggleErrorScreen(true);
                }
            }
            if (description.contains("ERR_UNKNOWN_URL_SCHEME")
                    || description.contains("ERR_TIMED_OUT")) {

                if (!failingUrl.contains(AppConstants.EMPTY_URL)) {
//                    super.onReceivedError(view, errorCode, description, failingUrl);
                    toggleErrorScreen(true);
                }
            }
            dismissLoader();
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            super.onReceivedSslError(view, handler, error);
            showWarnigFragmnet(error.getUrl(), error, new FragmentWarning.WarningCallback() {
                @Override
                public void continuePage(boolean continuepage) {
                    if (continuepage) {
                        handler.proceed();
                    } else {
                        handler.cancel();

                        WebBackForwardList mWebBackForwardList = urlWebView.copyBackForwardList();
                        try {
                            String historyUrl = mWebBackForwardList.getItemAtIndex(mWebBackForwardList.getCurrentIndex() - 1).getUrl();
                        } catch (Exception e) {

                            if (!TextUtils.isEmpty(UserPref.getInstance().getHomePage())) {
                                omnibarResults(UserPref.getInstance().getHomePage());
                            } else {
                                omnibarResults(PlistHelper.getInstance().getHomePage());
                            }

                        }
                    }
                }
            });
        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {

            String desc = error.getDescription().toString();
            Logger.logE(TAG, "onReceivedError " + desc);
            if (error.getDescription().toString().contains("ERR_NAME_NOT_RESOLVED") ||
                    error.getDescription().toString().contains("Couldn't find the URL")) {
                if (!request.getUrl().toString().equals(addWwwIfRequired(request.getUrl().toString()))) {
                    loadUrl(true, addWwwIfRequired(request.getUrl().toString()));
                    return;
                } else {
//                      super.onReceivedError(view,request,error);
                    toggleErrorScreen(true);
                }
            }

            if (error.getDescription().toString().contains("ERR_UNKNOWN_URL_SCHEME") || error.getDescription().toString().contains("ERR_TIMED_OUT")) {
                if (!request.getUrl().toString().contains(AppConstants.EMPTY_URL)) {
                    toggleErrorScreen(true);
//                    super.onReceivedError(view,request,error);
                    Logger.logE(TAG, "onReceivedError");
                }
            }
            dismissLoader();
        }


        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            try {
                if (!url.equals(AppConstants.EMPTY_URL)) {
                    WebBackForwardList webBackForwardList = view.copyBackForwardList();
                    int currentIndex = webBackForwardList.getCurrentIndex();
                    if (currentIndex == 1 && webBackForwardList.getItemAtIndex(0).getUrl().equals(AppConstants.EMPTY_URL)) {
//                        view.clearHistory();
                        webBackForwardList = view.copyBackForwardList();
                        currentIndex = webBackForwardList.getCurrentIndex();
                    }
                    if (currentIndex > reportIndex) {
                        reportIndex = currentIndex;
                    }
                    setWebViewBackforward();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
