package com.finjan.securebrowser.fragment;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.RemoteException;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finjan.securebrowser.AppLog;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.AppPurchaseActivity;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.model.SkuModel;

import java.util.ArrayList;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * currently not in use was used for showing purchase screen for safe scan
 */


public class FragmentAppPurchase extends ParentFragment implements View.OnClickListener {

    private View parentView;
    private LinearLayout llButtons, lyt_bullet_txt_layout;
    private Button btn_app_purchase_monthly, btn_app_purchase_yearly;
    private TextView txt_app_purchase_title;
    private BroadcastReceiver appPurchase = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && parentView != null) {

                if (intent.getBooleanExtra(AppConstants.IKEY_AppPurchase_Monthly, false)) {
                    Button monthly = ((Button) parentView.findViewById(R.id.btn_app_purchase_monthly));
                    monthly.setText(getString(R.string.renew_monthly));
                    return;
                }
                if (intent.getBooleanExtra(AppConstants.IKEY_AppPurchase_Yearly, false)) {
                    Button renew_yearly = ((Button) parentView.findViewById(R.id.btn_app_purchase_yearly));
                    renew_yearly.setText(getString(R.string.renew_yearly));
                }
            }
        }
    };
    private boolean shouldPopBack;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.layout_app_purchase, container, false);
        setReferences(parentView);
        return parentView;
    }

    private void setReferences(View parentView) {
        llButtons = (LinearLayout) parentView.findViewById(R.id.app_purchase_button_layout);
//        lyt_bullet_txt_layout=(LinearLayout)parentView.findViewById(R.id.lyt_bullet_txt_layout);
//        txt_app_purchase_title=(TextView)parentView.findViewById(R.id.txt_app_purchase_title);
        btn_app_purchase_monthly = (Button) parentView.findViewById(R.id.btn_app_purchase_monthly);
        btn_app_purchase_yearly = (Button) parentView.findViewById(R.id.btn_app_purchase_yearly);
        btn_app_purchase_monthly.setOnClickListener(this);
        btn_app_purchase_yearly.setOnClickListener(this);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setDefaults(parentView);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void setDefaults(View parentView) {
        setDialogTitle(parentView);
        setLayoutContents(parentView);
        setAppPurchaseButtons();
    }

    private void setDialogTitle(View parentView) {
        TextView txt_app_purchase_title = (TextView) parentView.findViewById(R.id.txt_app_purchase_title);
//        String title = Utils.getPlistvalue(Constants.KEY_SCANNER_DATA_FILTER, Constants.KEY_IN_APP_VIEW_STRING, Constants.KEY_HEADER_TEXT);
        String title = PlistHelper.getInstance().getInAppTitle();

//        title = title.replace("%d", Utils.getPlistvalue(Constants.KEY_APP_PURCHASE, Constants.KEY_FREE_SCAN_NUMBER));
        title = title.replace("%d", String.valueOf(PlistHelper.getInstance().getInAppPurchase().getFreeprivacyscan()));
        txt_app_purchase_title.setText(title.trim());
    }

    private void setLayoutContents(View parentView) {
//        ArrayList<String> BulletText = Utils.getPlistvalueArray(Constants.KEY_SCANNER_DATA_FILTER, Constants.KEY_IN_APP_VIEW_STRING, Constants.KEY_BULLET_TEXT);
        ArrayList<String> BulletText = PlistHelper.getInstance().getInAppButtetPoints();
        if (BulletText != null && BulletText.size() > 0) {
            LinearLayout bulletDatalayout = ((LinearLayout) parentView.findViewById(R.id.lyt_bullet_txt_layout));
            bulletDatalayout.removeAllViews();
            for (int i = 0; i < BulletText.size(); i++) {
                try {
                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View appPurchaseChildLayout = inflater.inflate(R.layout.app_purchase_child_text, null);
                    TextView txtView = (TextView) appPurchaseChildLayout.findViewById(R.id.txt_bullet_detail);
                    txtView.setText(BulletText.get(i).trim());
                    bulletDatalayout.addView(txtView);
                } catch (Exception e) {
                    AppLog.printLog(e.getMessage());
                }
            }
        }
    }

    private void setAppPurchaseButtons() {
        ArrayList<SkuModel> responseList = GlobalVariables.getInstnace().responseList;
        if (responseList == null) {
            return;
        }

        for (SkuModel skuModel : responseList) {

            if (skuModel.getSku().equalsIgnoreCase(getString(R.string.app_purchase_key_monthly))) {
//                                AppLog.printLog("******monthly purchase status " + purchaseState);
                final Button monthly = ((Button) parentView.findViewById(R.id.btn_app_purchase_monthly));
                if (!skuModel.getPurchaseState().equalsIgnoreCase("0")) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            monthly.setText(getString(R.string.monthly_charge));
                            monthly.setAlpha(1f);
                            monthly.setClickable(true);
                        }
                    });
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            monthly.setText(getString(R.string.renew_monthly));
                            monthly.setAlpha(0.5f);
                            monthly.setClickable(false);
                        }
                    });
                }
            } else if (skuModel.getSku().equalsIgnoreCase(getString(R.string.app_purchase_key_yearly))) {
                AppLog.printLog("******yearly purchase status " + skuModel.getPurchaseState());
                final Button monthly = ((Button) parentView.findViewById(R.id.btn_app_purchase_yearly));
                if (!skuModel.getPurchaseState().equalsIgnoreCase("0")) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            monthly.setText(getString(R.string.yearly_charges));
                            monthly.setAlpha(1f);
                            monthly.setClickable(true);
                        }
                    });
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            monthly.setText(getString(R.string.renew_yearly));
                            monthly.setAlpha(0.5f);
                            monthly.setClickable(false);
                        }
                    });

                }
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_app_purchase_monthly:
//                if (isPhoneReadStatePermissionGranted()){
                callAppPurchaseMonthly(getString(R.string.app_purchase_key_monthly), AppPurchaseActivity.MONTHLY_KEY);
//                }else{
//                    ActivityCompat.requestPermissions(UrlScannerActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_PERMISSION);
//                }
                break;
            case R.id.btn_app_purchase_yearly:
//                if (isPhoneReadStatePermissionGranted()){
                callAppPurchaseMonthly(getString(R.string.app_purchase_key_yearly), AppPurchaseActivity.YEARLY_KEY);
//                }else{
//                    ActivityCompat.requestPermissions(UrlScannerActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_PERMISSION_YEARLY);
//                }
                break;
        }
    }

    private void callAppPurchaseMonthly(final String purchaseKey, int requestKey) {
        if (getIInAppBillingService() != null && NativeHelper.getInstnace().checkActivity(getActivity())) {
            try {
                Bundle buyIntentBundle = getIInAppBillingService().getBuyIntent(3, getActivity().getPackageName(),
                        purchaseKey, "subs", "");
                int response = buyIntentBundle.getInt("BILLING_RESPONSE_RESULT_OK");
                if (response == 0) {
                    PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
                    if (pendingIntent != null && pendingIntent.getIntentSender() != null) {
                        getActivity().startIntentSenderForResult(pendingIntent.getIntentSender(),
                                requestKey, new Intent(), 0, 0, 0);
                    }
                }

            } catch (RemoteException e) {
                AppLog.printLog("billing crash", e.getMessage());
            } catch (IntentSender.SendIntentException e) {
                AppLog.printLog("billing crash", e.getMessage());
            } catch (Exception e) {
                AppLog.printLog("billing crash", e.getMessage());
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        GlobalVariables.getInstnace().appPurchaseFragment = this;
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(appPurchase, new IntentFilter(AppConstants.LBCAST_AppPurchase_Success));
        if (shouldPopBack) {
            popCurrentFragment();
        }
    }

    public void updateUiAfterPerchase(Bundle bundle) {
//        if (bundle == null) {
//            shouldPopBack=true;
//            return;
//        }

        if (bundle.getBoolean(AppConstants.IKEY_AppPurchase_Monthly)) {
            Button monthly = ((Button) parentView.findViewById(R.id.btn_app_purchase_monthly));
            monthly.setText(getString(R.string.renew_monthly));
            shouldPopBack = true;
            return;
        }
        if (bundle.getBoolean(AppConstants.IKEY_AppPurchase_Yearly)) {
            Button renew_yearly = ((Button) parentView.findViewById(R.id.btn_app_purchase_yearly));
            renew_yearly.setText(getString(R.string.renew_yearly));
            shouldPopBack = true;
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(appPurchase);
    }

    public void onClickBack() {
        popCurrentFragment();
    }
}
