package com.finjan.securebrowser.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.SearchResultPage;
import com.finjan.securebrowser.activity.BaseActivity;
import com.finjan.securebrowser.activity.HomeActivity;
import com.finjan.securebrowser.activity.NormaWebviewActivity;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.constants.DrawerConstants;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.SafetyResult;

import java.util.HashMap;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * fragment having all comment method used in inherited fragments.
 */

public class ParentFragment extends Fragment implements DrawerConstants {


    private static final String TAG = ParentFragment.class.getSimpleName();
    private String tag;
    private SearchResultPage searchResultPage;
    private TextView tvTitle;
    private ImageView ivBackButton;
    private ProgressBar progressbar1;
    private LinearLayout llToolbar;

    public static Fragment newInstance(String title, boolean isHome, String dkey) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BKEY_TITLE_NAME, title);
        bundle.putString(AppConstants.BKEY_DKEY_TAG, dkey);
        bundle.putBoolean(AppConstants.BKEY_ISHOME, isHome);
        Fragment fragment = getFragment(dkey);
        ((ParentFragment) fragment).setFragmentTag(dkey);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static Fragment newInstance(String title, boolean isHome, String dkey, Bundle bundle) {
        Fragment fragment = newInstance(title, isHome, dkey);
        Bundle bundle1 = fragment.getArguments();
        bundle1.putBundle(AppConstants.BKEY_BUNDLE, bundle);
        fragment.setArguments(bundle1);
        return fragment;
    }

    private static Fragment getFragment(String dkey) {
        switch (dkey) {
            case DKEY_HOME:
                return new FragmentHome();
            case DKEY_APP_PERCHASE_DIALOG:
                return new FragmentAppPurchase();
            case DKEY_Worning:
                return new FragmentWarning();
            case DKEY_Bookmarks:
                return new FragmentHistory();
            case DKEY_History:
                return new FragmentHistory();
            case DKEY_Report_Fragment:
                return new FragmentReport();
            case DKEY_Scanning_Fragment:
                return new ScanningFragment();
            case DKEY_Search_Fragment:
                return new SearchedResultFragment();
            case DKEY_Settings:
                return new FragmentSetting();
            case DKEY_TABS:
                return new FragmentTabs();
            case DKEY_Tracker_Fragemnt:
                return new FragmentTrackerList();
            case DKEY_PASSWORD_RESET:
                return new FragmentNormalWebView();
            case DKEY_HELP:
                return new FragmentNormalWebView();
            case DKEY_ABOUT:
                return new FragmentNormalWebView();
            case DKEY_EULA:
                return new FragmentNormalWebView();
            case DKEY_PRIVATE_PRIVACY:
                return new FragmentNormalWebView();
        }
        return new FragmentHome();
    }

    public String getFragmentTag() {
        return tag;
    }

    private void setFragmentTag(String tag) {
        this.tag = tag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        GlobalVariables.getInstnace().currentFragmentTag = getFragmentTag();
        setReferences();
        setDefaults();
    }

    private void setDefaults() {
        if(NativeHelper.getInstnace().checkActivity(getActivity())){
            if (!(this instanceof FragmentHome) && getActivity() instanceof HomeActivity) {
                hideBarsAndFab();
            }
            changeStatusBarColor(R.color.col_status);
            if (getToolbar() != null && (this instanceof FragmentHome)) {
                Drawable drawable = ResourceHelper.getInstance().getDrawable(R.color.col_desc_w);
                ((RelativeLayout) (getToolbar().findViewById(R.id.rl_title))).setBackground(drawable);
            }
        }

    }

    protected void hideBarsAndFab() {
        hideReportBar();
        hidePurchaseBar();
        hideTrackerFab();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(GlobalVariables.getInstnace().currentHomeFragment!=null &&
                !GlobalVariables.getInstnace().currentHomeFragment.isSafetyResultFetching){
            finzenLoaderVisibility(View.GONE, null);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        finzenLoaderVisibility(View.GONE, null);
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            return;
        }
    }

    protected String getTitle() {
        return tvTitle.getText().toString().trim();
    }

    protected TextView getTitleTextview(){
        return tvTitle;
    }
    protected ImageView getBackButton(){
        return ivBackButton;
    }


    protected void setTitle(String title) {

        if (tvTitle == null) {
            return;
        }
        tvTitle.setTextSize(getResources().getDimension(R.dimen._6sdp));

//        tvTitle.setTypeface(null, Typeface.BOLD);
        if (!TextUtils.isEmpty(GlobalVariables.getInstnace().currentFragmentTag) &&
                !TextUtils.isEmpty(tag)) {
            if (tag.equalsIgnoreCase(GlobalVariables.getInstnace().currentFragmentTag)) {
                if (NativeHelper.getInstnace().checkActivity(getActivity())) {
                    if (title == null || title.equalsIgnoreCase("") || title.equalsIgnoreCase(AppConstants.EMPTY_URL)) {
                        tvTitle.setText("");
                    } else {
                        tvTitle.setText(title.toUpperCase());
                    }
                }

            } else {
                Logger.logE(TAG, "Title issue strikees, " + tag + " != " + GlobalVariables.getInstnace().currentFragmentTag);
            }
        }
    }

    protected void handleBasicNavigation(boolean isHome) {

        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            if (isHome) {
//                ((HomeActivity)getActivity()).getHomeBtn().setVisibility(View.VISIBLE);
                ivBackButton.setVisibility(View.GONE);
            } else {
//                ((HomeActivity)getActivity()).getHomeBtn().setVisibility(View.GONE);
                ivBackButton.setVisibility(View.VISIBLE);

            }
            ivBackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackClick();
                }
            });
        }
    }
    private void onBackClick(){
        if(NativeHelper.getInstnace().checkActivity(getActivity())){
            if(this instanceof FragmentNormalWebView
                    && ((FragmentNormalWebView)this).getParentActivity()!=null &&
                    ((FragmentNormalWebView)this).getParentActivity() instanceof NormaWebviewActivity){
                    ((((FragmentNormalWebView)this).getParentActivity())).onBackPressed();
                    return;
                }

                GlobalVariables.getInstnace().justBackPressed = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    GlobalVariables.getInstnace().justBackPressed = false;
                }
            }, 500);
            if(NativeHelper.getInstnace().checkActivity(getActivity())
//                    &&
//                    getActivity().getSupportFragmentManager().getBackStackEntryCount()==0
                    && this instanceof FragmentSetting){
                if(UserPref.getInstance().getIsBrowser()){
                    ScreenNavigation.getInstance().callHomeActivity(ParentFragment.this.getContext(),"");
                }else {
                    ScreenNavigation.getInstance().callVPNScreen(ParentFragment.this.getContext());
                    ParentFragment.this.getActivity().finish();
                }
            }
            HomeActivity.getInstance().onBackPressed();
        }
    }
    private boolean ifFromNormalWebviewActivity(){
        return (this instanceof FragmentNormalWebView && ((FragmentNormalWebView)this).getParentActivity()!=null &&
                ((FragmentNormalWebView)this).getParentActivity() instanceof NormaWebviewActivity);
    }

    public void finzenLoaderVisibility(int visibility, FragmentHome fragment) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            try {
                ((HomeActivity) getActivity()).finzenLoaderVisibility(visibility, fragment);
            }catch (ClassCastException e){}
        }
    }
    protected boolean isLoaderVisible(){
        if(NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()){
            ((HomeActivity)getActivity()).isLoaderVisible();
        }
        return false;
    }

    protected void nonSecureLoaderVisibility(int visibility) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).nonSecureLoaderVisibility(visibility);
        }
    }

    protected void changeFragment(Fragment fragment, boolean addToBackStack, String backStackTag, String fragmentTag) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).changeFragment(fragment, addToBackStack, backStackTag, fragmentTag);
        }
    }

    protected void replaceWithCurrentFragment(Fragment fragment) {
        changeFragment(fragment, false, FragmentHome.TAG, FragmentHome.TAG);
    }

    protected void showToast(String msg, Context context) {
        if (NativeHelper.getInstnace().checkContext(context)) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }
    }

    protected void setProgressbar1(int progress) {
        if (progressbar1 != null) {
            progressbar1.setProgress(progress);
        }
    }

    protected void setProgressbarBackGround(String condition) {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            if (progressbar1 != null) {
                int colour = ResourceHelper.getInstance().getColor(getBackGroundUponConditions(condition));
                progressbar1.setBackgroundColor(colour);
                progressbar1.getProgressDrawable().setColorFilter(
                        ResourceHelper.getInstance().getColor(R.color.fj_menu_bg), PorterDuff.Mode.SRC_IN);
            }
        }
    }

    public int getBackGroundUponConditions(String condition) {
        switch (condition) {
            case NativeHelper.ProgressBarTypes.NORMAL:
                return R.color.fj_menu_bg;
        }
        return R.color.fj_menu_bg;
    }

    protected ProgressBar getProgressBar() {
        return progressbar1;
    }

    protected void showProggessBar() {
        if (progressbar1 != null && progressbar1.getVisibility() == View.GONE) {
            progressbar1.setVisibility(View.VISIBLE);
        }
    }

    protected void hideProggessBar() {
        if (progressbar1 != null && progressbar1.getVisibility() == View.VISIBLE) {
            progressbar1.setVisibility(View.GONE);
        }
    }

    protected void showAppPurchaseScreen() {

        Fragment fragment = FragmentAppPurchase.newInstance(getString(R.string.app_purchase_title),
                false, DrawerConstants.DKEY_APP_PERCHASE_DIALOG);
        String tag = FragmentAppPurchase.class.getSimpleName();
        changeFragment(fragment, true, tag, tag);
    }

    protected void popCurrentFragment() {
        if(this instanceof FragmentTabs){
            return;
        }
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    protected IInAppBillingService getIInAppBillingService() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            return ((HomeActivity) getActivity()).getIInAppBillingService();
        }
        return null;
    }

    protected void changeStatusBarColor(int color) {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            NativeHelper.getInstnace().changeStatusBarColor(color, getActivity());
        }
    }

    protected SearchResultPage getSearchResultPage() {
        return this.searchResultPage;
    }

    protected void setSearchResultPage(SearchResultPage searchResultPage) {
        this.searchResultPage = searchResultPage;
    }

    protected String getCurrentFragmentName() {
        if (NativeHelper.getInstnace().checkActivity(getActivity())  && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).getCurrentFragmentName();
        }
        return "";
    }

    protected boolean isPortraitOrientation() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    protected boolean isLandscapedOrientation() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    protected Bundle getChangeCarrier() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            return ((HomeActivity) getActivity()).getChangeCarrier();
        }
        return null;
    }

    protected void setChangeCarrier(Bundle bundle) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).setChangeCarrier(bundle);
        }
    }

    protected void updateFragmentMap(Long id, Fragment fragment) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            if (id != null && fragment != null) {
                ((HomeActivity) getActivity()).updateFragmentMap(id, fragment);
            }
        }
    }

    protected boolean addFragmentToMap(Long id, Fragment fragment) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            if (id != null && fragment != null) {
                ((HomeActivity) getActivity()).addFragment(id, fragment);
                return true;
            }
        }
        return false;
    }

    protected HashMap<Long, Fragment> getFragmentMap() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            return ((HomeActivity) getActivity()).getFragmentMap();
        }
        return null;
    }

    protected void clearAllFragments() {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            ((BaseActivity) getActivity()).clearAllFragments();
        }
    }
    protected void clearAllPrivateFragments(){
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            ((BaseActivity) getActivity()).clearAllPrivateFragments();
        }
    }
    protected void clearAllNonPrivateFragment(){
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            ((BaseActivity) getActivity()).clearAllNonPrivateFragment();
        }
    }

//    protected void updateCurrentFragmentId(FragmentHome fragmentHome) {
//        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
//            ((BaseActivity) getActivity()).updateCurrentFragmentId(fragmentHome);
//        }
//    }

    protected void removeFragment(Fragment fragment) {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.remove(fragment);
            ft.commit();
        }
    }

    protected void showDrawer() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).showDrawer();
        }
    }

    protected void hideDrawer() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).hideDrawer();
        }
    }

    protected void hideReportBar() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).hideReportBar();
        }
    }

    protected void hidePurchaseBar() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).hidePurchaseBar();
        }
    }

    protected void showReportBar() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).showReportBar();
        }
    }
    protected void setForShowReport(){
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).setForShowReport();
        }
    }

    protected void showPurchaseBar() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).showPurchaseBar();
        }
    }

    protected void showTrackFab() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).showTrackFab();
        }
    }

    protected void hideTrackerFab() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).hideTrackerFab();
        }
    }

    protected void setSafetyReport(SafetyResult safetyResult) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).setReport(safetyResult);
        }
    }

    protected void setReportReferences() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).setreportResources();
        }
    }

    protected void onClickTabs() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).openTabsScreen();
        }
    }

    protected void setHomeIcon(boolean isSet) {
//        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
//            ((HomeActivity) getActivity()).setHomeIcon(isSet);
//        }
    }

    protected void updateTrackerCount(int size) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).updateTrackerCount(size);
        }
    }

    protected void hintVisibility() {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).hintVisibility();
        }
    }

    protected void changeBookMarkIcon(String url) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && !ifFromNormalWebviewActivity()) {
            ((HomeActivity) getActivity()).changeBookMarkIcon(url);
        }
    }

    protected Bundle getFragmentBundle() {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            Bundle bundle = getArguments();
            if (bundle.containsKey(AppConstants.BKEY_BUNDLE)) {
                return bundle.getBundle(AppConstants.BKEY_BUNDLE);
            }
        }
        return null;
    }
    protected void updateFragmentId(FragmentHome fragmentHome,Long id){
        if(NativeHelper.getInstnace().checkActivity(getActivity())){
            ((BaseActivity)getActivity()).updateFragmentID(fragmentHome,id);
        }
    }

    public void openUrl(String title, String url, boolean isPrivate, Long id) {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {

            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.BKEY_URL, url);
            bundle.putBoolean(AppConstants.BKEY_IS_PRIVATE, isPrivate);
            if (id != null) {
                bundle.putLong(AppConstants.BKEY_ID, id);
            }
            Fragment fragment = newInstance(title, false, DKEY_HOME, bundle);
            changeFragment(fragment, false, "", "");
        }
    }

    protected void switchTo(Long id, String url, boolean fromWarning) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && id != null) {
            if (getFragmentMap() != null && getFragmentMap().containsKey(id)) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Fragment fragment = getFragmentMap().get(id);
                if (fm != null && fragment != null) {
                    if (!TextUtils.isEmpty(url)) {
                        ((FragmentHome) fragment).defaultUrlLoading(url, true, fromWarning);
                    }
//                    fm.beginTransaction().remove(fragment).commit();
                    changeFragment(getFragmentMap().get(id), false, FragmentHome.TAG, FragmentHome.TAG);
                }
            }
        }
    }

    protected void openTab(Long id, String title, String url, boolean isPrivate, boolean fromHistory, boolean fromWarning) {

        if(GlobalVariables.getInstnace().currentHomeFragment!=null && fromHistory){
           id= GlobalVariables.getInstnace().currentHomeFragment.getWebviewId();
        }
        if (isFragmentOpened(id)) {
            if (!fromHistory) {
                url = null;
            }
            switchTo(id, url, fromWarning);
        } else {
            openUrl(title, url, isPrivate, id);
        }
    }

    private boolean isFragmentOpened(Long id) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && id != null) {
            if (getFragmentMap() != null && getFragmentMap().containsKey(id)) {
                return true;
            }
        }
        return false;
    }

    protected void updateFragmentId(Long oldId, Long newId) {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            ((BaseActivity) getActivity()).updateFragmentId(oldId, newId);
        }
    }

    protected void setBookMark(boolean isBookMark) {
//        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
//            ((HomeActivity) getActivity()).setBookMarks(isBookMark);
//        }
    }

    /**
     * parent back click that will effect all fragment as Default back blick
     */
    public void onClickParentBack() {
        popCurrentFragment();
    }

    /**
     * setting toolbar view and back navigation with title references
     */
    private void setReferences() {
        if (getView() != null) {
            llToolbar = (LinearLayout) getView().findViewById(R.id.base_toolbar);
            progressbar1 = (ProgressBar) getView().findViewById(R.id.progressbar1);

            if (getArguments() != null) {
                if (getArguments().containsKey(AppConstants.BKEY_ISHOME)) {
                    if (getView() != null) {
                        tvTitle = (TextView) getView().findViewById(R.id.tv_title);
                        ivBackButton = (ImageView) getView().findViewById(R.id.iv_backButton);
                        if(ivBackButton != null){
                            handleBasicNavigation(getArguments().getBoolean(AppConstants.BKEY_ISHOME));
                        }
                    }
                }

                if (getArguments().containsKey(AppConstants.BKEY_TITLE_NAME)) {
                    setTitle(getArguments().getString(AppConstants.BKEY_TITLE_NAME));
                }
            }


        }
    }

    //    private MediaPlayer mPlay, mPlay2;
    protected void playWarningMusic() {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            ((BaseActivity) getActivity()).playWarningMusic();
        }
    }

    protected void stopWarningMusic() {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            ((BaseActivity) getActivity()).stopWarningMusic();
        }
    }

    /**
     * @return Title bar of Fragments
     */
    protected LinearLayout getToolbar() {
        return llToolbar;
    }

    protected void showToast(String msg) {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        }
    }
    protected void showToast(String msg,int lenght) {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            Toast.makeText(getActivity(), msg, lenght).show();
        }
    }
    protected void onClickVPN(){
        if(NativeHelper.getInstnace().checkActivity(getActivity())){
            ((HomeActivity)getActivity()).openVPNScreen();
        }
    }
    protected void refreshDisplayHeight(){
        if(NativeHelper.getInstnace().checkActivity(getActivity())){
            ((HomeActivity)getActivity()).refreshDisplayHeight();
        }
    }

}
