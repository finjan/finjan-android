package com.finjan.securebrowser.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import android.view.View;
import android.view.Window;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.logger.Logger;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class TouchIdDialog extends Dialog implements View.OnClickListener {


    public TouchIdDialog(@NonNull Context context) {
        super(context);
    }

    public TouchIdDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }


    protected TouchIdDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.touch_id);
        this.setCancelable(false);
        this.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_canceltouchid:
                Logger.logE("TAG","cancel touch id ");
                this.dismiss();
                break;

            case R.id.tv_enterpass:
                Logger.logE("TAG","enter pass");
                this.dismiss();
                break;
        }
    }
}
