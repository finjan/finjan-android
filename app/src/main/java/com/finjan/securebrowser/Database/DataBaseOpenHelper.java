package com.finjan.securebrowser.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.finjan.securebrowser.helpers.logger.Logger;

import org.greenrobot.greendao.database.Database;

import webHistoryDatabase.DaoMaster;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 */


public class DataBaseOpenHelper extends DaoMaster.OpenHelper {
    public Database db=null;
    public DataBaseOpenHelper(Context context, String name) {
        super(context, name);
    }

    public DataBaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        Logger.logI("DataBaseOpenHelper", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by MigrateHelper");
        this.db=db;
        MigrateHelper.getInstance().migrateData(db,newVersion);
    }
}
