package com.finjan.securebrowser.Database;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

//import com.crashlytics.android.Crashlytics;
import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.internal.DaoConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import webHistoryDatabase.DaoMaster;
import webHistoryDatabase.bookmarks;
import webHistoryDatabase.bookmarksDao;
import webHistoryDatabase.tabhistory;
import webHistoryDatabase.tabhistoryDao;
import webHistoryDatabase.webhistory;
import webHistoryDatabase.webhistoryDao;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 */


public class MigrateHelper  {
    private static MigrateHelper instance;
    private List<bookmarks> bookmarkList;
    private List<webhistory> webHistoryList;
    private List<tabhistory> tabHistoryList;
    private static final String CONVERSION_CLASS_NOT_FOUND_EXCEPTION = "MIGRATION HELPER - CLASS DOESN'T MATCH WITH THE CURRENT PARAMETERS";

    public static MigrateHelper getInstance(){
        if (instance==null){
            instance=new MigrateHelper();
        }
        return instance;
    }

    public void migrateData(Database db, int newVersion){
        DaoMaster daoMaster = new DaoMaster(db);
        AnalyticsApplication.daoSession = daoMaster.newSession();
        if (AnalyticsApplication.daoSession==null)
            return;
        recoverData(db,newVersion);
        dropAlltable(db);
        createTable(db);
        restoreData();
    }

    private void createTable(Database db) {
        DaoMaster.createAllTables(db,false);
    }

    private void restoreData() {
        restoreTabData();
        restoreWebData();
        restoreBookMarkData();
    }

    private void restoreTabData() {
        tabhistoryDao tabDao = AnalyticsApplication.daoSession.getTabhistoryDao();
        tabDao.deleteAll();
        if (tabHistoryList!=null && tabHistoryList.size()>0){
            Collections.reverse(tabHistoryList);
            for (int i=0;i<tabHistoryList.size();i++){
                tabhistory tabData=new tabhistory();
                tabData.setId(tabHistoryList.get(i).getId());
                tabData.setPagetitle(tabHistoryList.get(i).getPagetitle());
                tabData.setUrl(tabHistoryList.get(i).getUrl());
                tabData.setRating(tabHistoryList.get(i).getRating());
                tabData.setCountry(tabHistoryList.get(i).getCountry());
                tabData.setStatus(tabHistoryList.get(i).getStatus());
                tabData.setCategories(tabHistoryList.get(i).getCategories());
                tabData.setType("");
                tabDao.insert(tabData);
            }
        }
    }

    private void restoreWebData() {
        webhistoryDao WebDao = AnalyticsApplication.daoSession.getWebhistoryDao();
        WebDao.deleteAll();
        if (webHistoryList!=null && webHistoryList.size()>0){
            Collections.reverse(webHistoryList);
            for (int i=0;i<webHistoryList.size();i++){
                webhistory history=new webhistory();
                history.setId(webHistoryList.get(i).getId());
                history.setPagetitle(webHistoryList.get(i).getPagetitle());
                history.setUrl(webHistoryList.get(i).getUrl());
                history.setRating(webHistoryList.get(i).getRating());
                history.setCategories(webHistoryList.get(i).getCategories());
                history.setCountry(webHistoryList.get(i).getCountry());
                history.setStatus(webHistoryList.get(i).getStatus());
                history.setDtime(webHistoryList.get(i).getDtime());
                WebDao.insert(history);
            }
        }
    }

    private void restoreBookMarkData() {
        bookmarksDao bookmarkDao = AnalyticsApplication.daoSession.getBookmarksDao();
        bookmarkDao.deleteAll();
        if (bookmarkList!=null && bookmarkList.size()>0){
            Collections.reverse(bookmarkList);
            for (int i=0;i<bookmarkList.size();i++){
                bookmarks bookmark=new bookmarks();
                bookmark.setId(bookmarkList.get(i).getId());
                bookmark.setPagetitle(bookmarkList.get(i).getPagetitle());
                bookmark.setUrl(bookmarkList.get(i).getUrl());
                bookmark.setRating(bookmarkList.get(i).getRating());
                bookmark.setCountry(bookmarkList.get(i).getCountry());
                bookmark.setStatus(bookmarkList.get(i).getStatus());
                bookmark.setCategories(bookmarkList.get(i).getCategories());
                bookmarkDao.insert(bookmark);
            }
        }

    }

    private void generateTempTables(Database db, Class<? extends AbstractDao<?, ?>>... daoClasses) {
        for(int i = 0; i < daoClasses.length; i++) {
            DaoConfig daoConfig = new DaoConfig(db, daoClasses[i]);

            String divider = "";
            String tableName = daoConfig.tablename;
            String tempTableName = daoConfig.tablename.concat("_TEMP");
            ArrayList<String> properties = new ArrayList<>();

            StringBuilder createTableStringBuilder = new StringBuilder();

            createTableStringBuilder.append("CREATE TABLE ").append(tempTableName).append(" (");

            for(int j = 0; j < daoConfig.properties.length; j++) {
                String columnName = daoConfig.properties[j].columnName;

                if(getColumns(db, tableName).contains(columnName)) {
                    properties.add(columnName);

                    String type = null;

                    try {
                        type = getTypeByClass(daoConfig.properties[j].type);
                    } catch (Exception exception) {
                        FirebaseCrashlytics.getInstance().recordException(exception);
//                        Crashlytics.logException(exception);
                    }

                    createTableStringBuilder.append(divider).append(columnName).append(" ").append(type);

                    if(daoConfig.properties[j].primaryKey) {
                        createTableStringBuilder.append(" PRIMARY KEY");
                    }

                    divider = ",";
                }
            }
            createTableStringBuilder.append(");");

            db.execSQL(createTableStringBuilder.toString());

            StringBuilder insertTableStringBuilder = new StringBuilder();

            insertTableStringBuilder.append("INSERT INTO ").append(tempTableName).append(" (");
            insertTableStringBuilder.append(TextUtils.join(",", properties));
            insertTableStringBuilder.append(") SELECT ");
            insertTableStringBuilder.append(TextUtils.join(",", properties));
            insertTableStringBuilder.append(" FROM ").append(tableName).append(";");

            db.execSQL(insertTableStringBuilder.toString());
        }
    }

    private String getTypeByClass(Class<?> type) throws Exception {
        if(type.equals(String.class)) {
            return "TEXT";
        }
        if(type.equals(Long.class) || type.equals(Integer.class) || type.equals(long.class)) {
            return "INTEGER";
        }
        if(type.equals(Boolean.class)) {
            return "BOOLEAN";
        }

        Exception exception = new Exception(CONVERSION_CLASS_NOT_FOUND_EXCEPTION.concat(" - Class: ").concat(type.toString()));
        FirebaseCrashlytics.getInstance().recordException(exception);
        throw exception;
    }

    private static List<String> getColumns(Database db, String tableName) {
        List<String> columns = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM " + tableName + " limit 1", null);
            if (cursor != null) {
                columns = new ArrayList<>(Arrays.asList(cursor.getColumnNames()));
            }
        } catch (Exception e) {
            Logger.logE(tableName, e.getMessage()+ e);
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return columns;
    }
    private void dropAlltable(Database db) {
        DaoMaster.dropAllTables(db,true);
    }

    private void recoverData(Database db, int newVersion) {
        Logger.logI("MigrateHelper","db "+db+" version "+newVersion);
        Logger.logI("MigrateHelper","daoSession "+AnalyticsApplication.daoSession);
        if (AnalyticsApplication.daoSession != null) {
            bookmarkList = AnalyticsApplication.daoSession.getBookmarksDao().loadAll();
            if (newVersion==2){
                tabhistoryDao tabhistorytemp = AnalyticsApplication.daoSession.getTabhistoryDao();
                String tableName = tabhistorytemp.getTablename();
                db.execSQL("ALTER TABLE "+tableName+" ADD COLUMN type VARCHAR;");
                try {
                    tabHistoryList = AnalyticsApplication.daoSession.getTabhistoryDao().loadAll();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }else{
                tabHistoryList = AnalyticsApplication.daoSession.getTabhistoryDao().loadAll();
            }
            webHistoryList = AnalyticsApplication.daoSession.getWebhistoryDao().loadAll();
        }
    }
}
