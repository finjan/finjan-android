package com.finjan.securebrowser;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.model.ModelManager;
import com.finjan.securebrowser.parser.PList;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.VpnUtil;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class Utils {
    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
    public static void showKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isAssetExists(String filename,String ext){
        try {
        if(FinjanVPNApplication.getInstance().getResources().getAssets().list("")!=null){
             return Arrays.asList(FinjanVPNApplication.getInstance().getResources().getAssets().list("")).contains(filename+"."+ext);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getPlistvalue(String dict, String key) {
        String value = "";
        PList plist = ModelManager.getInstance().getPList();
        if (plist == null) {
            return value;
        } else {
            try {
                Map<String, PList> mapValue = plist.getMap();
                if (mapValue.containsKey(dict)) {
                    PList dictValue = mapValue.get(dict);
                    if (dictValue != null) {
                        if (TextUtils.isEmpty(key)) {
                            value = dictValue.data.toString();
                        } else {
                            value = dictValue.get(key).data.toString();
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
        return value;
    }

    public static String getPlistvalue(String dict, String key, String key1) {
        String value = "";
        ArrayList<PList> array = getPlistArray();
        if (array != null && array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).qname.equalsIgnoreCase(dict)) {
                    PList dictValue = array.get(i);
                    if (dictValue != null) {
                        PList tempPlist = dictValue.get(key);
                        if(tempPlist !=null){
                            if (TextUtils.isEmpty(key1)) {
                                value = tempPlist.data;
                            } else {
                                if(tempPlist.get(key1)!=null){
                                    value = tempPlist.get(key1).data;
                                }
                            }
                            break;
                        }
                    }
                }

            }
        }
        return value;

    }

    public static ArrayList<String> getPlistvalueArray(String dict, String key, String key1) {
        ArrayList<String> value = new ArrayList<>();
        ArrayList<PList> array = getPlistArray();
        if (array != null && array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).qname.equalsIgnoreCase(dict)) {
                    PList dictValue = array.get(i);
                    if (dictValue != null && dictValue.get(key)!=null) {
                        ArrayList<PList> tempPlist = dictValue.get(key).array;
                        if (tempPlist != null && tempPlist.size() > 0 && !TextUtils.isEmpty(key1)) {
                            for (int j = 0; j < tempPlist.size(); j++) {
                                if (tempPlist.get(j).qname.equalsIgnoreCase(key1)) {
                                    ArrayList<PList> key1value = tempPlist.get(j).array;
                                    if (key1value != null && key1value.size() > 0) {
                                        for (int k = 0; k < key1value.size(); k++) {
                                            value.add(key1value.get(k).data);
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

            }
        }
        return value;
    }

    public static String getNotificationPlistvalue(String dict, String key, String key1) {

        String value = "";
        PList plist = ModelManager.getInstance().getPList();
        if (plist == null) {
            return null;
        } else {
            ArrayList<PList> array;
            try {
                Map<String, PList> mapValue = plist.getMap();
                if (mapValue.containsKey(dict)) {
                    ArrayList<PList> notificationArray = mapValue.get(dict).array;
                    if (notificationArray != null && notificationArray.size() > 0) {
                        for (int j = 0; j < notificationArray.size(); j++) {
                            ArrayList<PList> dictArray = notificationArray.get(j).array;
                            if (dictArray != null && dictArray.size() > 0) {
                                for (int i = 0; i < dictArray.size(); i++) {
                                    ArrayList<PList> tempDictArray = dictArray.get(i).array;
                                    if (tempDictArray != null && tempDictArray.size() > 0) {
                                        for (int k = 0; k < tempDictArray.size(); k++) {
                                            if (tempDictArray.get(k).qname.equalsIgnoreCase(key1)) {
                                                value = tempDictArray.get(k).data.toString();
                                                return value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        }

        return value;

    }

    public static ArrayList<String> getPlistvalue(String dict) {
        ArrayList<String> factId = new ArrayList<>();
        ArrayList<PList> array = getPlistArray();
        if (array != null && array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).qname.equalsIgnoreCase(dict)) {
                    ArrayList<PList> values = array.get(i).array;
                    if (values != null && values.size() > 0) {
                        for (int j = 0; j < values.size(); j++) {
                            try {
                                factId.add(values.get(j).data.toString());
                            } catch (Exception e) {
                            }
                        }
                    }
                }
            }
        }
        return factId;
    }

    public static ArrayList<PList> getPlistArray() {
        PList plist = ModelManager.getInstance().getPList();
        if (plist == null) {
            return null;
        } else {
            try {
                Map<String, PList> mapValue = plist.getMap();
                if (mapValue.containsKey(Constants.KEY_APP_STRING)) {
                    return mapValue.get(Constants.KEY_APP_STRING).array;
                }
            } catch (Exception e) {
            }
        }
        return null;
    }

    public static float convertToFloat(String value) {
        if (value == null && value.isEmpty())
            return 0;
        return Float.parseFloat(value.trim());
    }

    public static String getRecommendedValue() {
        StringBuffer value = new StringBuffer();
//        value.append(getPlistvalue(Constants.KEY_RECOMMEND, ""));
//        value.append(getPlistvalue(Constants.KEY_APP_URL, Constants.KEY_ANDROID));
        value.append(PlistHelper.getInstance().getRecommend());
        value.append(PlistHelper.getInstance().getAboutUrl());
        return value.toString();
    }

    public static boolean isNetworkConnected(Context mContext) {
        if(NativeHelper.getInstnace().checkContext(mContext)) {
            ConnectivityManager ConnectionManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnected();
        }
        return false;
    }

    public static boolean isValidURL(String url) {
        String patternString = "^((https?:[/][/])?(\\\\w+[.])+com|((https?:[/][/])?(\\\\w+[.])+com[/]|[.][/])?\\\\w+([/]\\\\w+)*([/]|[.]html|[.]php|[.]gif|[.]jpg|[.]png)?)$";
        try {
            Pattern patt = Pattern.compile(patternString);
            Matcher matcher = patt.matcher(url);
            return matcher.matches();
        } catch (RuntimeException e) {
            return false;
        }
    }

    public static int getNotificationId() {
        Calendar cal = Calendar.getInstance();
        int seconds = cal.get(Calendar.SECOND);
        int minute = cal.get(Calendar.MINUTE);
        int hour = cal.get(Calendar.HOUR);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return Integer.parseInt(day + "" + hour + "" + minute + "" + seconds);
    }

    public static int get_count_of_days(String Created_date_String, String Expire_date_String) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        Date Created_convertedDate = null, Expire_CovertedDate = null, todayWithZeroTime = null;
        try {
            Created_convertedDate = dateFormat.parse(Created_date_String);
            Expire_CovertedDate = dateFormat.parse(Expire_date_String);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int c_year = 0, c_month = 0, c_day = 0;

        if (Created_convertedDate.after(todayWithZeroTime)) {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(Created_convertedDate);
            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);
        } else if (Created_convertedDate.before(todayWithZeroTime)) {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(Created_convertedDate);
            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);
        } else {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(todayWithZeroTime);
            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);
        }


    /*Calendar today_cal = Calendar.getInstance();
    int today_year = today_cal.get(Calendar.YEAR);
    int today = today_cal.get(Calendar.MONTH);
    int today_day = today_cal.get(Calendar.DAY_OF_MONTH);
    */

        Calendar e_cal = Calendar.getInstance();
        e_cal.setTime(Expire_CovertedDate);

        int e_year = e_cal.get(Calendar.YEAR);
        int e_month = e_cal.get(Calendar.MONTH);
        int e_day = e_cal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(c_year, c_month, c_day);
        date2.clear();
        date2.set(e_year, e_month, e_day);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return (int) dayCount;
    }

    public static String getFilePath() {
        return getDownlaodDirectory().getAbsolutePath();
    }


    public static File getDownlaodDirectory() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    }


    public static void connectToVpn(Context context,String serverRegion) {

        // get last used server id

        if (TextUtils.isEmpty(serverRegion))
        {
            serverRegion = FinjanVpnPrefs.getLastUsedServer(context);
        }

        long serverId = new ContentProviderClient().getServerId(context, serverRegion);
        TrafficController trafficController = TrafficController.getInstance(context);
        long trafficLimit = trafficController.getTrafficLimit();
        long remainingTraffic = trafficLimit - trafficController.getTraffic();
        boolean hasVpnTraffic = (remainingTraffic > 0) || /*(trafficLimit == 0)*/TrafficController.getInstance(context).isPaid(); // limit 0 = limitless traffic


        // connect to vpn
        if (hasVpnTraffic)
            VpnUtil.startVpnConnection(context,serverId,true);
        else
            Toast.makeText(context, "You have run out of your monthly free VPN data and are no longer protected. Update now to our unlimited plan to remain protected!", Toast.LENGTH_LONG).show();
    }
}

