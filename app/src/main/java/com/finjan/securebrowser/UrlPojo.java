package com.finjan.securebrowser;
/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class UrlPojo {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
