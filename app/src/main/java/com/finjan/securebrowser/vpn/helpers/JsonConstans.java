package com.finjan.securebrowser.vpn.helpers;

/**
 * Created by anurag on 14/07/17.
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class JsonConstans {

    public static final String DEVICE_NAME = "device_name";
    public static final String DEVICE_ID = "device_id";
    public static final String TYPE = "type";
    public static final String IP_ADDRESS = "ip_address";
    public static final String ATTRIBUTES = "attributes";
    public static final String DATA = "data";
    public static final String STATUS = "status";
    public static final String SUBSCRIPTION_ID = "subscription_id";
    public static final String TRANSACTION_ID = "transaction_id";
    public static final String NAME = "name";
    public static final String SOURCE = "source";
    public static final String RENEWED_AT = "renewed_at";
    public static final String EXPIRES_AT = "expires_at";
    public static final String CREATED_AT = "created_at";
    public static final String PLAN_INTERVAL = "plan_interval";
    public static final String AMOUNT = "amount";
    public static final String COMP_FLAG = "comp_flg";
    public static final String RENEWAL_FLG = "renewal_flg";
    public static final String PARENT_TRANSACTION_ID = "parent_transaction_id";

    public static final String DEVICE_TYPE = "device_type";
    public static final String EXPIRATION_DATE = "expiration_date";
    public static final String SUBSCRIPTION = "subscription";
    public static final String ACTION = "action";

    public static final String ANDROID = "android";
    public static final String GOOGLE = "google";
    public static final String PROMO = "promo";
    public static final String ID = "id";
    public static final String CANCELLED = "cancelled";
    public static final String MONTH = "month";
    public static final String YEAR = "year";
    public static final String MESSAGE = "message";


}
