package com.finjan.securebrowser.vpn.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.model.RegionModel;
import com.finjan.securebrowser.vpn.util.Util;

import java.util.List;

/**
 * Created by Illia.Klimov on 11/26/2015.
 */
public class RegionAdapter extends ArrayAdapter<RegionModel> {

    private List<RegionModel> mData = null;

    class RegionViewHolder {
        private RadioButton radioButton;
//        protected ImageView mRegionFlag;
        protected TextView mTitle;

        public RegionViewHolder(View view) {
            this.radioButton=(RadioButton)view.findViewById(R.id.rb_regional_dialog);
//            this.mRegionFlag = (ImageView) view.findViewById(R.id.imgItemRegionFlag);
            this.mTitle = (TextView) view.findViewById(R.id.txtItemRegionTitle);
//            if (TrafficController.getInstance(FinjanVPNApplication.getInstance()).isPaid()) {
//                this.mTitle.setTextColor(Util.getColor(FinjanVPNApplication.getInstance(), R.color.pro_text_normal_color));
//            }
        }
    }

    private RegionDialogFragment regionDialogFragment;
    public RegionAdapter(Context context, List<RegionModel> feedItemList,RegionDialogFragment regionDialogFragment) {
        super(context, R.layout.region_list_item);
        mData = feedItemList;
        this.regionDialogFragment=regionDialogFragment;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final RegionViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.region_list_item, null);
        }

        if (convertView.getTag() != null) {
            viewHolder = (RegionViewHolder) convertView.getTag();
        } else {
            viewHolder = new RegionViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        RegionModel item = getItem(position);
        if(item!=null){
            viewHolder.mTitle.setText(item.getTitle());
            int iconForRegion = Util.getIconForRegion(FinjanVPNApplication.getInstance(), item.getRegion());
            if (iconForRegion > 0) {
//            viewHolder.mRegionFlag.setImageResource(iconForRegion);
//            viewHolder.mRegionFlag.setVisibility(View.VISIBLE);
            } else {
//            viewHolder.mRegionFlag.setVisibility(View.INVISIBLE);
            }
            viewHolder.radioButton.setChecked(item.isSelected());
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regionDialogFragment.refreshDialogList(position);
            }
        });

        return convertView;
    }

    public RegionModel getItem(int position) {
        return mData.get(position);
    }

    public void clear() {
        super.clear();
        mData.clear();
    }
    public void notifyAdapter( List<RegionModel> feedItemList){
        this.mData=feedItemList;
        notifyDataSetChanged();
    }


}
