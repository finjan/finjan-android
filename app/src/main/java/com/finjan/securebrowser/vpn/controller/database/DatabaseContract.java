/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.controller.database;

import android.provider.BaseColumns;

public class DatabaseContract {

    private DatabaseContract() {
    }

    public static final class ServerTable implements BaseColumns {
        private ServerTable() {
        }

        // Table name
        public static final String TABLE = "server";
        // Columns
        public static final String NAME = "name";
        public static final String HOST = "host";
        public static final String PORT = "port";
        public static final String PROTOCOL = "protocol";
        public static final String REGION = "region";
        public static final String UUID = "uuid";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE +
                " (" + _ID + " INTEGER PRIMARY KEY," + UUID + " TEXT," + HOST +
                " TEXT," + PORT + " TEXT," + NAME + " TEXT," + REGION + " TEXT,"+ PROTOCOL + " TEXT)";
    }
}
