/*
 * *
 *  * <p>
 *  * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 *  * Finjan Mobile Vital Security Browser
 *  * <p>
 *  * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *  * Developed by NewOfferings, LLC.
 *  *
 *  *
 *
 */

/***
 * 
 * @author Pavitra
 * @email-id pavitra.yadav@trignodev.com
 *@description cache entry class to handle data while putting and getting to and from disk lru cache
 */
package com.finjan.securebrowser.vpn.controller.network.cache;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

public class CacheEntry
      {
          private Map<String, String> cacheHeaders;
          public byte[] responseData;
          private static final int DISK_CACHE_ENTRY_METADATA = 0;
          private static final int DISK_CACHE_ENTRY_BODY = 0;

          public CacheEntry(final byte[] responseData) {

              this.responseData = responseData;
          }

          public CacheEntry(final DiskLruCache.Snapshot snapshot) throws IOException
          {


              ByteArrayOutputStream baos = new ByteArrayOutputStream();
              byte[] buffer = new byte[1024];
              int read = 0;

              InputStream in = snapshot.getInputStream(DISK_CACHE_ENTRY_BODY);

              while ((read = in.read(buffer, 0, buffer.length)) != -1) {
                  baos.write(buffer, 0, read);
              }

              in.close();

              responseData = baos.toByteArray();
          }

          public void writeTo(final DiskLruCache.Editor editor) throws IOException {

              InputStream in = new ByteArrayInputStream(responseData);
              byte[] buffer = new byte[1024];
              int read = 0;

              OutputStream out = editor.newOutputStream(DISK_CACHE_ENTRY_BODY);

              while ((read = in.read(buffer, 0, buffer.length)) != -1) {
                  out.write(buffer, 0, read);
              }

              out.close();
              in.close();
          }

          public Map<String, String> getCacheHeaders() {
              return cacheHeaders;
          }

          public void setCacheHeaders(final Map<String, String> cacheHeaders) {
              this.cacheHeaders = cacheHeaders;
          }

          public byte[] getResponseData() {
              return responseData;
          }

          public void setResponseData(final byte[] responseData) {
              this.responseData = responseData;
          }

          public int size() {
              return responseData.length;
          }
      }
       