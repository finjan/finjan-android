///*
// * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
// */
//
//
//import android.util.Log;
//
////import com.avira.common.tracking.EventProperties;
////import com.avira.common.tracking.TrackingEvent;
////import com.avira.common.tracking.TrackingManager;
//import com.avira.common.utils.SharedPreferencesUtilities;
//import com.finjan.securebrowser.a_vpn.FinjanVPNApplication;
//import com.finjan.securebrowser.a_vpn.ui.main.VPNSettingsActivity;
//
//import org.json.JSONObject;
//
//public class Tracking {
//
//    private static final String TAG = Tracking.class.getName();
//
//    //app name
//    public static final String APP_NAME = "AviraVPN";
//
//
//    //feature name
//    private static final String FEATURE_APP = "App";
//    private static final String FEATURE_LOGIN = "Login";
//    private static final String FEATURE_REGISTER = "Register";
//    private static final String FEATURE_SECURE_CONNECTION = "SecureConnection";
//    private static final String FEATURE_CHOICE_COUNTRY = "ChoiceCountry";
//    private static final String FEATURE_TRAFFIC_LIMIT_REACHED = "TrafficLimitReached";
//    private static final String FEATURE_FTU = "Ftu";
//    private static final String FEATURE_TRACKING = "TrackingMixpanel";
//    private static final String FEATURE_SETTINGS = "Settings";
//    private static final String FEATURE_NOTIFICATION = "Notification";
//    private static final String FEATURE_DRAWER = "Drawer";
//
//    public static final String FEATURE_PURCHASE_STARTED = "PurchaseStarted";
//    public static final String FEATURE_PURCHASE_COMPLETED = "PurchaseCompleted";
//    public static final String FEATURE_PURCHASE_GAVE_UP = "PurchaseGaveUp";
//    public static final String FEATURE_PRODUCT_UPGRADE = "ProductUpdate";
//    public static final String FEATURE_RATE_DIALOG = "RateDialog";
//
//
//    public static final TrackingEvent EVENT_OPEN = new TrackingEvent(APP_NAME, FEATURE_APP, "Open");
//    public static final TrackingEvent EVENT_LOGGEDIN = new TrackingEvent(APP_NAME, FEATURE_LOGIN, "Login");
//    public static final TrackingEvent EVENT_REGISTER = new TrackingEvent(APP_NAME, FEATURE_REGISTER, "Register");
//    public static final TrackingEvent EVENT_SECURE_CONNECTION = new TrackingEvent(APP_NAME, FEATURE_SECURE_CONNECTION, "SecureConnection");
//    private static final TrackingEvent EVENT_CHOICE_COUNTRY = new TrackingEvent(APP_NAME, FEATURE_CHOICE_COUNTRY, "ChoiceCountry");
//    public static final TrackingEvent EVENT_TRAFFIC_LIMIT_REACHED = new TrackingEvent(APP_NAME, FEATURE_TRAFFIC_LIMIT_REACHED, "TrafficLimitReached");
//    private static final TrackingEvent EVENT_FTU_TIMESPENT = new TrackingEvent(APP_NAME, FEATURE_FTU, "TimeSpent");
//    public static final TrackingEvent EVENT_TRACKING_ON = new TrackingEvent(APP_NAME, FEATURE_TRACKING, "trackingOn");
//    public static final TrackingEvent EVENT_TRACKING_OFF = new TrackingEvent(APP_NAME, FEATURE_TRACKING, "trackingOff");
//    private static final TrackingEvent EVENT_SMART_MONITOR = new TrackingEvent(APP_NAME, FEATURE_SETTINGS, "SmartMonitor_Action");
//    private static final TrackingEvent EVENT_NOTIF_SHOW = new TrackingEvent(APP_NAME, FEATURE_NOTIFICATION, "Notification_Show");
//    private static final TrackingEvent EVENT_NOTIF_ACTION = new TrackingEvent(APP_NAME, FEATURE_NOTIFICATION, "Notification_Action");
//    private static final TrackingEvent EVENT_DRAWER_SELECTION = new TrackingEvent(APP_NAME, FEATURE_DRAWER, "Item_Selected");
//
//    public static final TrackingEvent EVENT_PURCHASE_STARTED = new TrackingEvent(APP_NAME, FEATURE_PURCHASE_STARTED, "PurchaseStarted");
//    public static final TrackingEvent EVENT_PURCHASE_COMPLETED = new TrackingEvent(APP_NAME, FEATURE_PURCHASE_COMPLETED, "PurchaseCompleted");
//    public static final TrackingEvent EVENT_PURCHASE_GAVE_UP = new TrackingEvent(APP_NAME, FEATURE_PURCHASE_GAVE_UP, "PurchaseGaveUp");
//    public static final TrackingEvent EVENT_UPGRADE = new TrackingEvent(APP_NAME, FEATURE_PRODUCT_UPGRADE, "ProductUpdate");
//
//    public static final TrackingEvent EVENT_RATE_ME_DIALOG_SHOW = new TrackingEvent(APP_NAME, FEATURE_RATE_DIALOG, "RateMeDialog_Show");
//
//    private static final String PROP_OLDCOUNTRYNAME = "OldCountryName";
//    private static final String PROP_NEWCOUNTRYNAME = "NewCountryName";
//    private static final String PROP_TIMESPENT = "TimeSpent";
//    private static final String PROP_ACTION_TYPE = "actionType";
//    private static final String PROP_NOTIF_TITLE = "notificationTitle";
//    private static final String PROP_CAMPAIGN_ID = "campaignId";
//    private static final String PROP_ITEM_SELECTED = "itemSelected";
//    private static final String PROP_TRAFFIC_VALUE = "traffic";
//    private static final String PROP_PURCHASE_RUNTIME = "purchasedRuntime";
//    private static final String PROP_PURCHASE_PLATFORM = "purchasedPlatform";
//    private static final String PROP_PURCHASE_SEEN_OTHER_OFFER = "seenOtherOffer";
//
//    public static final String VALUE_NEW_WIFI = "newSsid";
//    public static final String VALUE_INSECURE_WIFI = "unprotectedWiFi";
//
//    public static final String ID_NEW_WIFI_ID = "phantom_newssid";
//    public static final String ID_INSECURE_WIFI_ID = "phantom_unprotectedwifi";
//
//    public static void trackEvent(TrackingEvent event) {
//
//        if (trackingEnabled()) {
//            Logger.logD(TAG, "MixPanel tracking " + event.getEvent());
////            TrackingManager.getInstance().trackFeature(event);
//        } else {
//            Logger.logD(TAG, "MixPanel tracking DISABLED " + event.getEvent());
//        }
//    }
//
//    public static void trackEvent(TrackingEvent event, JSONObject attr) {
//        if (trackingEnabled()) {
////            TrackingManager.getInstance().trackFeature(event, attr);
//        }
//    }
//
//    public static void trackChoiceCountry(String oldCountry, String newCountry) {
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_OLDCOUNTRYNAME, oldCountry);
//        attr.putProperty(PROP_NEWCOUNTRYNAME, newCountry);
//        trackEvent(EVENT_CHOICE_COUNTRY, attr);
//    }
//
//    public static void trackPurchaseComplete(int purchasedRuntime, String purchasedPlatform, boolean seenOtherOffer) {
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_PURCHASE_RUNTIME, purchasedRuntime);
//        attr.putProperty(PROP_PURCHASE_PLATFORM, purchasedPlatform);
//        attr.putProperty(PROP_PURCHASE_SEEN_OTHER_OFFER, seenOtherOffer);
//        trackEvent(EVENT_PURCHASE_COMPLETED, attr);
//    }
//
//    public static void trackPurchaseGaveUp(int purchasedRuntime, String purchasedPlatform) {
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_PURCHASE_RUNTIME, purchasedRuntime);
//        attr.putProperty(PROP_PURCHASE_PLATFORM, purchasedPlatform);
//        trackEvent(EVENT_PURCHASE_GAVE_UP, attr);
//    }
//
//    public static void trackPurchaseStarted(int purchasedRuntime, String purchasedPlatform) {
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_PURCHASE_RUNTIME, purchasedRuntime);
//        attr.putProperty(PROP_PURCHASE_PLATFORM, purchasedPlatform);
//        trackEvent(EVENT_PURCHASE_STARTED, attr);
//    }
//
//    public static void trackFtuTimeSpent(long timeSpentInSec) {
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_TIMESPENT, timeSpentInSec);
//        trackEvent(EVENT_FTU_TIMESPENT, attr);
//    }
//
//    /**
//     * Tracks whenever user toggles the smart monitor setting. This setting determines
//     * whether our app will show warning notification whenever user is connected to an insecure wifi.
//     * @param enabled True is smart monitor setting is enabled, otherwise false
//     */
//    public static void trackSmartMonitorSetting(boolean enabled) {
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_ACTION_TYPE, enabled ? "enable" : "disable");
//        trackEvent(EVENT_SMART_MONITOR, attr);
//    }
//
//    public static void trackNotificationShown(String notifTitle) {
//        String campaignId = null;
//        switch (notifTitle) {
//            case VALUE_NEW_WIFI:
//                campaignId = ID_NEW_WIFI_ID;
//                break;
//            case VALUE_INSECURE_WIFI:
//                campaignId = ID_INSECURE_WIFI_ID;
//                break;
//        }
//
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_NOTIF_TITLE, notifTitle);
//        attr.putProperty(PROP_CAMPAIGN_ID, campaignId);
//        trackEvent(EVENT_NOTIF_SHOW, attr);
//    }
//
//    public static void trackNotificationAction(String notifTitle, boolean actionPositive) {
//        String campaignId = null;
//        switch (notifTitle) {
//            case VALUE_NEW_WIFI:
//                campaignId = ID_NEW_WIFI_ID;
//                break;
//            case VALUE_INSECURE_WIFI:
//                campaignId = ID_INSECURE_WIFI_ID;
//                break;
//        }
//
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_NOTIF_TITLE, notifTitle);
//        attr.putProperty(PROP_CAMPAIGN_ID, campaignId);
//        attr.putProperty(PROP_ACTION_TYPE, actionPositive ? "positiveClick" : "negativeClick");
//        trackEvent(EVENT_NOTIF_ACTION, attr);
//    }
//
//    /**
//     * Tracks when a drawer item has been selected.
//     * @param menuItem The name of the selected item in the drawer.
//     */
//    public static void trackDrawerSelection(String menuItem) {
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_ITEM_SELECTED, menuItem);
//        trackEvent(EVENT_DRAWER_SELECTION, attr);
//    }
//
//    private static boolean trackingEnabled() {
//        return SharedPreferencesUtilities.getBoolean(FinjanVPNApplication.getInstance(),
//                VPNSettingsActivity.PREFERENCES_ALLOW_TRACKING, true);
//    }
//
//    public static void trackRateMeDialogShown(long traffic) {
//        EventProperties attr = new EventProperties();
//        attr.putProperty(PROP_TRAFFIC_VALUE, traffic);
//        trackEvent(EVENT_RATE_ME_DIALOG_SHOW, attr);
//    }
//}