/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.ui.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.widget.SwitchCompat;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.avira.common.backend.WebUtility;
import com.avira.common.dialogs.RateMeDialogUtils;
import com.avira.common.utils.DrawableUtils;
import com.avira.common.utils.SharedPreferencesUtilities;
import com.electrolyte.sociallogin.socialLogin.FbSignInHandler;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.NormaWebviewActivity;
import com.finjan.securebrowser.activity.RedeemActivity;
import com.finjan.securebrowser.activity.WatchContentActivity;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.constants.DrawerConstants;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.ToastHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.security.WifiSecurityHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.helpers.vpn_helper.CountryFlags;
import com.finjan.securebrowser.helpers.vpn_helper.InitializeVpn;
import com.finjan.securebrowser.model.RedeemListData;
import com.finjan.securebrowser.service.NotifyUnSecureNetwork;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.util.dialogs.NativeDialogs;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.auto_connect.AutoConnectAcitivity;
import com.finjan.securebrowser.vpn.auto_connect.AutoConnectHelper;
import com.finjan.securebrowser.vpn.controller.JsonConfigParser;
import com.finjan.securebrowser.vpn.controller.JsonParser;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
import com.finjan.securebrowser.vpn.controller.database.DatabaseContract;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.vpn.eventbus.AllPromoFetched;
import com.finjan.securebrowser.vpn.eventbus.AllSubscFetched;
import com.finjan.securebrowser.vpn.eventbus.CancelConnectingEvent;
import com.finjan.securebrowser.vpn.eventbus.CancelledsubsFetched;
import com.finjan.securebrowser.vpn.eventbus.DiscountedPromoAvailable;
import com.finjan.securebrowser.vpn.eventbus.FinjanUserFetched;
import com.finjan.securebrowser.vpn.eventbus.LicenceFetched;
import com.finjan.securebrowser.vpn.eventbus.LicenceFetchedFailed;
import com.finjan.securebrowser.vpn.eventbus.LicenceFetchedVPN;
import com.finjan.securebrowser.vpn.eventbus.LoginSuccess;
import com.finjan.securebrowser.vpn.eventbus.SubscriptionUpdated;
import com.finjan.securebrowser.vpn.eventbus.VpnStatusEvent;
import com.finjan.securebrowser.vpn.helpers.JsonConstans;
import com.finjan.securebrowser.vpn.licensing.LicensesRefreshedEvent;
import com.finjan.securebrowser.vpn.service.AviraOpenVpnService;
import com.finjan.securebrowser.vpn.social.SocialUserProfile;
import com.finjan.securebrowser.vpn.subscriptions.SubscriptionHelper;
import com.finjan.securebrowser.vpn.subscriptions.Subscriptions;
import com.finjan.securebrowser.vpn.ui.authentication.SetupAccountActivity;
import com.finjan.securebrowser.vpn.ui.iab.UpgradeVpnActivity;
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper;
import com.finjan.securebrowser.vpn.util.AutoClose;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.Util;
import com.finjan.securebrowser.vpn.util.VpnUtil;
import com.finjan.securebrowser.vpn.util.log.FileLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.OnClick;
import de.blinkt.openvpn.core.VpnStatus;
import de.greenrobot.event.EventBus;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 */
public class VpnHomeFragment extends Fragment implements VpnStatus.ByteCountListener {

    public static final String TAG = VpnHomeFragment.class.getSimpleName();

    public static final String VPN_SERVER_LIST = "VpnServerListJson";
    public static boolean isToHitCanclePromo = false;
    public static final long HOURS_24 = 1000 * 60 * 60 * 24;
    private static final long RATE_ME_DIALOG_AGO = 300000;
    public static boolean userCalledDisconnect;

    //
    public static boolean userJustLogin = false;
    private static VpnHomeFragment instance;
    protected long mSelectedServerId = 0;
    protected AviraOpenVpnService mService;
    View mPageLayout, ll_private_policy;
    TextView mUpgradeButton;
    TextView mTrafficCounter;
    TextView mTrafficDescription;
    LottieAnimationView mConnectionStatus;
    TextView mVpnLocationDescriptionValue;
    ImageView iv_arrow_down, iv_flag;
    //    ImageView statusRipple1;
//    ImageView statusRipple2;
    ImageView ivPlaceHolderFill;
    TextView mVpnLocationDescription;
    ImageView imVitalSecurity;
    private TextView tv_privacy_msg, tv_connect_status;
    private TextView loginRegisterButton;
    private String mSelectedServerRegion = null;
    private boolean isReachedTrafficLimit = false;
    private boolean mStartedNewConnection = false;
    private boolean mStartButtonPressed = false;
    private boolean mCanSwitchRegion = false;
    private boolean isConnected = false;
    private Random random = new Random();
    private boolean dontShowRippleAnim = false;
    //    private ProgressBar vpnProgressBar;
    private ImageView vpnProgressBar;
    private boolean updatingServerList;
    private FinjanDialogBuilder dialog;
    private boolean connetingToVpn;
    private ProgressDialog progressDialog;
    private ImageView ivMoreOption;
    private boolean showingPrivacy;
    private RegionDialogFragment regionDialogFragment;
    private boolean reconnectingToVpn;
    private int licenFailedCount = 0;
    //endregion
    private String connectedRegion;
    private FirebaseAnalytics mFirebaseAnalytics;
    private LottieAnimationView lottieAnimationView;
    private RelativeLayout lottieContainer;
    private TextView tv_start_trial;
    private LinearLayout vg_loader, buttonUpgradeVpn;
    private ProgressBar pb_upgrade;
    private String pointinAnim = setLottieFile("vince_pointing_up");
    private String currentVinceAnimation = setLottieFile("bull_airplane");
    private boolean vincePlaying = false;
    private String currentConnectingAnim = setLottieFile("vince_pointing_up");
    private long upgradeDialogShowTime;
    private int vinceAlreadyPointedFingerCount = PlistHelper.getInstance().getVinceAnimationCount();
    private boolean isBullpointing = false;
    private ImageView iv_lock;


    private boolean shouldShowUpdateBtn = true;
    //    private boolean isPromoInfoGone = false;
    private boolean isDrawerResponse;
    private boolean isLowDataReached = true;
    private boolean isLoderShowing = true;
    private boolean islastDayPromo = true;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            AviraOpenVpnService.LocalBinder binder = (AviraOpenVpnService.LocalBinder) service;
            mService = binder.getService();
            if (mService != null && isAdded()) {
                initStatus(mService.getConnectionStatus());
                // check did user press disconnect button via notification
                VpnActivity activity = (VpnActivity) getActivity();
                if (activity.mIsDisconnectPressed) {
                    VpnUtil.stopVpnConnection(getActivity(), mService);
                    activity.mIsDisconnectPressed = false;
                    if (activity.userClickedDisFromNot) {
                        activity.userClickedDisFromNot = false;
//                    activity.finish();
                    }
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }

    };



    public VpnHomeFragment() {
    }

    public static VpnHomeFragment newInstance() {
        return new VpnHomeFragment();
    }

    public static Intent upgradeIntentDeepLink(Context context, String redeemCode) {
        Intent intent = upgradeIntent(context, false, false, true);
        intent.putExtra("redeem_code", redeemCode);
        return intent;
    }

    public static Intent upgradeIntent(Context context, boolean throughOther, boolean showPopUp, boolean shouldbCheckDiscount) {
        Intent intent = getUpgradeIntent(context);
        intent = getDiscountedIntent(intent,/* (ArrayList<String>) Arrays.asList(arra)*/PromoHelper.Companion.getDiscountedArray(75),
                "Become InvinciBull Now!",
                "Save 75%- Limited Time",
                0 + "");
        if (!PlistHelper.getInstance().getUpgrade_discount().equals("0") && PromoHelper.Companion.isPromoActive()) {
            /*String[] arra =  new String[]{LicenseUtil.getSkuAllDevicesYearly(),
                    LicenseUtil.getSkuAllDevicesMonthly()};*/

            try {
                intent = getDiscountedIntent(intent,/* (ArrayList<String>) Arrays.asList(arra)*/PromoHelper.Companion.getDiscountedArray(Integer.parseInt(PlistHelper.getInstance().getUpgrade_discount())),
                        PromoHelper.Companion.getCurrentActivePromo().getAttributes().getTitle(),
                        PromoHelper.Companion.getCurrentActivePromo().getAttributes().getTitle_2(),
                        PromoHelper.Companion.getCurrentActivePromo().getId());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (shouldbCheckDiscount) {
            if (PromoHelper.Companion.getDiscountedRedeemListData() != null) {
                RedeemListData redeemListData = PromoHelper.Companion.getDiscountedRedeemListData();
                intent = getDiscountedIntent(intent, redeemListData.getAttributes().getIapDiscount().getAndroid(),
                        redeemListData.getAttributes().getTitle(),
                        redeemListData.getAttributes().getTitle_2(),
                        redeemListData.getId());
            } else {

            }

        } else {
            intent.putExtra("showPopUp", showPopUp);
            intent.putExtra("trialmsgTitle", PlistHelper.getInstance().getUpgradeAlertAflterLoginTitle());
            intent.putExtra("trialmsg", throughOther ? PlistHelper.getInstance().getUpgradeAlertAflterLogin()
                    : PlistHelper.getInstance().getUpgradeDefaultAlert());
            intent.putExtra("okBtn", throughOther ? PlistHelper.upgradeAlertAflterLoginOkBtn
                    : PlistHelper.upgradeDefaultAlertOkBtn);
            if (throughOther) {
                intent.putExtra("cancelBtn", PlistHelper.upgradeAlertAflterLoginCBtn);
            }
            intent.putExtra("userClickUpgrade", true);
        }

        GoogleTrackers.Companion.setEUpgrade_Button();
        LocalyticsTrackers.Companion.setEUpgrade_Button();
        return intent;
    }

    public static Intent upgradeIntent(Context context, boolean throughOther, boolean showPopUp) {
        return upgradeIntent(context, throughOther, showPopUp, false);
    }

    public static Intent getUpgradeIntent(Context context) {
        Intent intent = new Intent(context, UpgradeVpnActivity.class);
        return intent;
    }

    public static Intent getDiscountedIntent(Intent intent, ArrayList<String> skus, String title,
                                             String title_2,
                                             String id) {
        String[] array = new String[skus.size()];
        array = skus.toArray(array);
        intent.putExtra("isDiscountedPurchase", true);
        intent.putExtra("isDiscountedPurchaseTitle", title);
        intent.putExtra("isDiscountedPurchaseTitle_2", title_2);
        intent.putExtra("isDiscountedPurchaseId", id);
        intent.putExtra("skus", array);
        return intent;
    }

    public static VpnHomeFragment getInstance() {
        return instance;
    }

    public static boolean isDeepLinkPromo(Activity activity) {
        return activity.getIntent() != null && !TextUtils.isEmpty(activity.getIntent().getStringExtra("promo_id"));
    }

    private static long getLastSelectedServer(@NonNull Context context) {
        String serverRegion = FinjanVpnPrefs.getLastUsedServer(context);
        String localyticsRegion = serverRegion;
        if (serverRegion.equals(FinjanVpnPrefs.getDefaultServer())) {
            localyticsRegion = "Nearest Location";
            LocalyticsTrackers.Companion.setLastCountryConnected(localyticsRegion);
        }

        long id = VpnHomeFragment.getDefaultServerVPNId(context);
        if (!TextUtils.isEmpty(serverRegion)) {
            ContentProviderClient contentProviderClient = new ContentProviderClient();
            Cursor cursor = contentProviderClient.getServer(context, serverRegion);
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    id = cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID));

                }
                AutoClose.closeSilently(cursor);
            }
        }
        return id;
    }

    public static void onClickVpnAppShare() {
        GoogleTrackers.Companion.setEShare();
        LocalyticsTrackers.Companion.setEShare();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                NativeDialogs.getVPNShareLinkDialog(NativeHelper.getInstnace().currentAcitvity,
                        PlistHelper.getInstance().getVpnsharepopup());
            }
        }, 100);

    }

    public static void runVPN(Context context) {
        if (context != null) {
            boolean isRunned = VpnUtil.startVpnConnection(context,
                    getLastSelectedServer(context), false);
        }
    }

    private static long getDefaultServerVPNId(@NonNull Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String defaultServer = prefs.getString(JsonConfigParser.DEFAULT_SERVER, null);

        long server = 0;
        if (!TextUtils.isEmpty(defaultServer)) {
            ContentProviderClient contentProviderClient = new ContentProviderClient();
            Cursor cursor = contentProviderClient.getServer(context, defaultServer);
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    server = cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID));
                }
                AutoClose.closeSilently(cursor);
            }
        }
        return server;
    }

//    public static boolean shouldAutoLogin(@NonNull Context context) {
//        return TrafficController.getInstance(context).isRegistered() && UserPref.getInstance().isTokenExpires();
//    }

    public static void showLoginScreen() {
        if (NativeHelper.getInstnace().checkActivity(NativeHelper.getInstnace().currentAcitvity)) {
            if (NativeHelper.getInstnace().currentAcitvity instanceof SetupAccountActivity) {
                return;
            }
            ToastHelper.showToast(FinjanVPNApplication.getInstance().getString(R.string.token_expire_msg), Toast.LENGTH_SHORT);
            SetupAccountActivity.newLoginInstanceForResult(NativeHelper.getInstnace().currentAcitvity,
                    VpnActivity.REQUEST_NAV, SetupAccountActivity.ACTION_SIGN_NAV);
            preLogoutPref(FinjanVPNApplication.getInstance());
        } else {
            AutoConnectHelper.Companion.getInstance().createAutoConnectNofication(
                    ResourceHelper.getInstance().getString(R.string.you_are_in_public_wifi_msg),
                    ResourceHelper.getInstance().getString(R.string.you_are_in_public_wifi_msg));
        }
    }

    public static void preLogoutPref(@NonNull Context context) {
        VpnApisHelper.Companion.getInstance().refreshApis();
        SubscriptionHelper.getInstance().logout();
        PromoHelper.Companion.setPromoChecked(false);
        if (UserPref.getInstance().getCachedUserForAutoLogin() != null &&
                UserPref.getInstance().getCachedUserForAutoLogin().length > 0 &&
                UserPref.getInstance().getCachedUserForAutoLogin()[0].equals(SocialUserProfile.FB_USER)) {
            FbSignInHandler.Companion.LogoutUser();
        }
        UserPref.getInstance().clearUserCorrespondingPrefOnLogout();
        AuthenticationHelper.getInstnace().stopRefreshing();
        TrafficController.getInstance(context).clearHistory();
        FinjanVpnPrefs.saveEmptyServer(context);
        LocalyticsTrackers.Companion.setELogout();
        SubscriptionHelper.getInstance().updatingSubs = false;
    }

    public static void onClickAutoConnect(Context context) {
        String v = ResourceHelper.getInstance().getString(R.string.auto_connect);
        if (PlistHelper.getInstance().getVpnMenuMap().get("trustednetworks") != null) {
            v = PlistHelper.getInstance().getVpnMenuMap().get("trustednetworks").getName();
        }
        AutoConnectAcitivity.Companion.getAutoConnectActivityIntent(context, v);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PromoHelper.Companion.setPromoChecked(false);
        if (!UserPref.getInstance().getUserVisitsVPN()) {
            UserPref.getInstance().setUserVisitsVPN(true);
            LocalyticsTrackers.Companion.setEVPN_Page_First_Time();
        }
        initOpenService();


      /*  if (UserPref.getInstance().getInappMassege() >= 0)
        {
            UserPref.getInstance().setInappMassege(-1);
            HashMap map = new HashMap<String, String>();
            map.put("result", String.valueOf(UserPref.getInstance().getInappMassege()));
            LocalyticsTrackers.Companion.setTrafficExhausted(map);
        }*/

        // Obtain the FirebaseAnalytics instance.

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_activity_2, container, false);

        ButterKnife.bind(this, view);
        bindView(view);
        new Thread(new Runnable() {
            @Override
            public void run() {
                fetchTraffic();
                PromoHelper.Companion.getInstance().getAllRedeemOffers(getContext());
                VpnApisHelper.Companion.getInstance().updateLicence(false,false,VpnHomeFragment.this.getContext());

                selectLastServer(FinjanVPNApplication.getInstance());
//        VpnUtil.startVpnConnection(getContext(), 0000,false);
                GoogleTrackers.Companion.setSVPNMain();
                LocalyticsTrackers.Companion.setEVpnMain();
            }
        }).start();


        return view;
    }

    public void disconnectService() {
        VpnUtil.stopVpnConnection(getActivity(), mService);
    }

    public void showLoader() {
        if (isAdded()) {
            vg_loader.setVisibility(View.VISIBLE);
        }
    }

    public void hideLoader() {
        if (isAdded()) {
            vg_loader.setVisibility(View.GONE);
        }
    }
    private TextView tvpromoname,tvpromo,tvpromoexpirydate;
    private  LinearLayout llPromoInfo;
//    private ImageView imswitch;
    private SwitchCompat igswitch;
    private void setAutoProtectUi(){
        if (UserPref.getInstance().getAutoConnect())
//            imswitch.setBackgroundResource(R.drawable.switch_on);
        igswitch.setChecked(true);
           // Picasso.with(getActivity()).load(R.drawable.switch_on).into(imswitch);
        else
//            imswitch.setBackgroundResource(R.drawable.switch_off);
        igswitch.setChecked(false);
//            Picasso.with(getActivity()).load(R.drawable.switch_off).into(imswitch);
    }
    private void bindView(View view) {
//         imswitch = (ImageView)view.findViewById(R.id.imswitch);
         igswitch = view.findViewById(R.id.tgswitch);
         setAutoProtectUi();
         igswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 UserPref.getInstance().setAutoConnect(isChecked);
             }
         });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            igswitch.setThumbTintList(getContext().getColorStateList(R.color.switch_thumb_color));
            igswitch.setTrackTintList(getContext().getColorStateList(R.color.switch_track_color));
        }
//         imswitch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (UserPref.getInstance().getAutoConnect()) {
//                    imswitch.setBackgroundResource(R.drawable.switch_off);
////                    Picasso.with(getActivity()).load(R.drawable.switch_off).into(imswitch);
//                    UserPref.getInstance().setAutoConnect(false);
//                }
//                else {
//                    imswitch.setBackgroundResource(R.drawable.switch_on);
////                    Picasso.with(getActivity()).load(R.drawable.switch_on).into(imswitch);
//                    UserPref.getInstance().setAutoConnect(true);
//                }
//            }
//        });
        llPromoInfo = (LinearLayout)  view.findViewById(R.id.llpromoInfo);
        tvpromoname = (TextView) view.findViewById(R.id.tvpromoname);
        tvpromo = (TextView) view.findViewById(R.id.tvpromo);
        tvpromoexpirydate = (TextView) view.findViewById(R.id.tvpromoexpirydate);
        mPageLayout = view.findViewById(R.id.layout_top);
        pb_upgrade = view.findViewById(R.id.pb_upgrade);
        vg_loader = view.findViewById(R.id.vg_loader);
        tv_connect_status = (TextView) view.findViewById(R.id.tv_connect_status);
        tv_start_trial = (TextView) view.findViewById(R.id.tv_start_trial);
        tv_start_trial.setTypeface(null, Typeface.BOLD);
        tv_connect_status.setTypeface(null, Typeface.BOLD);
        ll_private_policy = view.findViewById(R.id.ll_private_policy);
        buttonUpgradeVpn = (LinearLayout) view.findViewById(R.id.buttonUpgradeVpn);
        mUpgradeButton = (TextView) view.findViewById(R.id.tv_upgrade);
        tv_privacy_msg = (TextView) view.findViewById(R.id.tv_privacy_msg);
        vpnProgressBar = (ImageView) view.findViewById(R.id.iv_guage);
        mTrafficCounter = (TextView) view.findViewById(R.id.txtTrafficCounter);
        mTrafficCounter.setTypeface(null, Typeface.BOLD);
        mTrafficDescription = (TextView) view.findViewById(R.id.txtTrafficDescription);
        mConnectionStatus = (LottieAnimationView) view.findViewById(R.id.txtConnectionStatus);
        mConnectionStatus.setAnimation(pointinAnim);
        mConnectionStatus.playAnimation();
        mConnectionStatus.setVisibility(View.VISIBLE);
        mVpnLocationDescriptionValue = (TextView) view.findViewById(R.id.txtVpnLocationDescriptionValue);
        loginRegisterButton = (TextView) view.findViewById(R.id.buttonLogin_or_register);
        lottieAnimationView = (LottieAnimationView) view.findViewById(R.id.iv_vpn_btn_holder);
        lottieContainer = (RelativeLayout) view.findViewById(R.id.rl_lottie);
        SpannableString spannableString = new SpannableString(getString(R.string.or_get_extra_500_mb));
        int start = getString(R.string.or_get_extra_500_mb).length() - 9;
        spannableString.setSpan(new ForegroundColorSpan(Util.getColor(FinjanVPNApplication.getInstance(), R.color.col_content)),
                start, getString(R.string.or_get_extra_500_mb).length(), 0);// set color
        loginRegisterButton.setText(spannableString);

        iv_arrow_down = (ImageView) view.findViewById(R.id.iv_arrow_down);
        iv_flag = (ImageView) view.findViewById(R.id.iv_flag);
        mVpnLocationDescription = (TextView) view.findViewById(R.id.txtVpnLocationDescription);
        ivPlaceHolderFill = (ImageView) view.findViewById(R.id.iv_switch_fill);
        imVitalSecurity = (ImageView) view.findViewById(R.id.imgVpnStatus_2);
        iv_lock = (ImageView) view.findViewById(R.id.iv_lock);

        setupRippleAnimation();

        if (TrafficController.getInstance(getActivity()).isPaid()) {
            if (shouldShowUpdateBtn) {
                buttonUpgradeVpn.setVisibility(View.GONE);
            }
        } else {
            buttonUpgradeVpn.setVisibility(View.VISIBLE);
            mUpgradeButton.setText(getResources().getString(R.string.upgrade_for_unlimited_traffic));
        }
        ((TextView) view.findViewById(R.id.tv_click_here_to_read_privacy)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VpnHomeFragment.this.getContext(), NormaWebviewActivity.class);
                intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_PRIVATE_PRIVACY);
                startActivity(intent);
                GoogleTrackers.Companion.setSPrivacy();
//                LocalyticsTrackers.Companion.setSPrivacy();
            }
        });
        ((TextView) view.findViewById(R.id.tv_i_agree)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserPref.getInstance().setPrivacyReadingDate(DateHelper.getInstnace().getServerFormatDate(Calendar.getInstance().getTimeInMillis()));
                showVpnScreen();
            }
        });
        mVpnLocationDescriptionValue.setTypeface(null, Typeface.BOLD);
    }

    private void makeVinceAnimate() {
        String currentVince = getVinceAnimation();
        currentVinceAnimation = currentVince;
        currentConnectingAnim = currentVince;
        mConnectionStatus.setAnimation(currentVince);
    }

    private String setLottieFile(String filename) {
        return PlistHelper.getInstance().getShowFestiveAnim() ?
                filename + "_" + PlistHelper.getInstance().showLottieKey() + ".json" :
                filename + ".json";
    }

    private String getVinceAnimation() {
        switch (currentVinceAnimation) {
            case "bull_airplane_Christmas.json":
                return "bull_behind_Christmas.json";
            case "bull_behind_Christmas.json":
                return "bull_popcorn_Christmas.json";
            case "bull_popcorn_Christmas.json":
                return "bull_running_Christmas.json";
            case "bull_running_Christmas.json":
                return "bull_superman_no_spinning_Christmas.json";
            case "bull_superman_no_spinning_Christmas.json":
                return "bull_superman_spinning_Christmas.json";
            case "bull_superman_spinning_Christmas.json":
                return "bull_airplane_Christmas.json";
            case "bull_airplane.json":
                return "bull_behind.json";
            case "bull_behind.json":
                return "bull_popcorn.json";
            case "bull_popcorn.json":
                return "bull_running.json";
            case "bull_running.json":
                return "bull_superman_no_spinning.json";
            case "bull_superman_no_spinning.json":
                return "bull_superman_spinning.json";
            case "bull_superman_spinning.json":
                return "bull_airplane.json";
            default:
                return "bull_airplane.json";
        }
    }

    private void animateVince(int status) {
        switch (status) {
            //connecting
            case 0:
                if (vincePlaying) {
                    return;
                }
                userTapVpnInit();
                makeVinceAnimate();
                mConnectionStatus.setRepeatCount(-1);
                mConnectionStatus.playAnimation();
                if (!vincePlaying) {
                    vincePlaying = true;
                }
                break;
            //connected
            case 1:
                mConnectionStatus.pauseAnimation();
                vincePlaying = false;
                break;
            case 2:
                //disconnected
                if (!currentConnectingAnim.equalsIgnoreCase(pointinAnim)) {
                    vincePlaying = false;
                    mConnectionStatus.setRepeatCount(0);
                    mConnectionStatus.setAnimation(pointinAnim);
                    if (!isBullpointing)
                        mConnectionStatus.playAnimation();
                }
                break;
        }
    }

    //region the RIPPLE animation when connected
    private void setupRippleAnimation() {

        if (dontShowRippleAnim) {
            return;
        }
        // need the same drawable as the status imageview but i'm mutating it so it doesn't share the state because i'll
        // do things to it - override color inside it
        Drawable rippleBackground = getResources().getDrawable(R.drawable.vpn_intermediate).mutate();
        DrawableUtils.colorizeDrawable(rippleBackground, getResources().getColor(R.color.accent_color_1));
        ivPlaceHolderFill.setVisibility(View.GONE);
    }

    private void stopRippleAnimation() {
        if (dontShowRippleAnim) {
            return;
        }
        stopRippleAnim();
    }

    private void updateTrafficUI() {
        disableButtonsIfRequired();
        Logger.logD(TAG, "updateTrafficUI");
        if (isAdded() && !isVpnConnecting()) {

            TrafficController trafficController = TrafficController.getInstance(FinjanVPNApplication.getInstance());
            long traffic = trafficController.getTraffic() + trafficController.getNewTraffic();
            long trafficLimit = trafficController.getTrafficLimit();
            // do not update if its first time run and no traffic consumed yet
            if (traffic != 0 || trafficLimit != 0 || trafficController.isRegistered()) {
                setNewTrafficUI(traffic, trafficLimit);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (UserPref.getInstance().getInappMassege() >= 0)
                {

                    HashMap map = new HashMap<String, String>();
                    map.put("result", String.valueOf(UserPref.getInstance().getInappMassege()));
                    LocalyticsTrackers.Companion.setTrafficExhausted(map);
                    UserPref.getInstance().setInappMassege(-1);
                }
            }
        },1000);
        instance = this;
        isBullpointing = false;
        if (isDeepLinkPromo(getActivity()) && PromoHelper.Companion.getInstance().getFailedDeepLinkingTry()) {
            PromoHelper.Companion.getInstance().checkIfDeepLinking(VpnActivity.getInstance());
        }

//        if(!VpnApisHelper.Companion.getInstance().shouldCheckPromoValidaity(getContext())){
//            showLoader();
//        }
//        if(!VpnApisHelper.Companion.isPromoListFetched() && PromoHelper.Companion.getInstance().getAllPromoList(getContext())==null){
//            showLoader();
//        }
        AviraOpenVpnService.noProcessRunning = false;
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(getActivity(), false);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        licenFailedCount = 0;
        vinceAlreadyPointedFingerCount = PlistHelper.getInstance().getVinceAnimationCount();
        GlobalVariables.getInstnace().mainActivityFragment = this;
        VpnStatus.addByteCountListener(this);
        LocalyticsTrackers.Companion.setIsAutoConnectUser();
        updateTrafficUI();
        initOpenService();
        onResumeRefresh();
        setAutoProtectUi();
        //  TrafficController.getInstance(getContext()).applyNewTraffic();

    }

    private void initOpenService() {
        Intent intent = new Intent(getActivity(), AviraOpenVpnService.class);
        intent.setAction(AviraOpenVpnService.START_SERVICE);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void onResumeRefresh() {
        setMenuNew();
        setDefaults();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (AutoConnectHelper.Companion.getInstance().getWaitingHomeForAutoConnect()) {
            AutoConnectHelper.Companion.getInstance().setWaitingHomeForAutoConnect(false);
            AutoConnectHelper.Companion.getInstance().whenStatusChanged(getContext());
        }

        fetchServer();
        if (getActivity() != null && VpnApisHelper.Companion.getInstance().shouldAutoLogin(getActivity())) {
            doAutoLoginIfRequested(getContext());
        } else {
            if (mStartButtonPressed || isDrawerResponse) {
                isDrawerResponse = false;
                return;
            }

//            VpnApisHelper.Companion.getInstance().updateLicence(false,false,VpnHomeFragment.this.getContext());

            // VpnApisHelper.Companion.getInstance().updateLicence(false, getActivity());
            doIfTokenValid();

            updateTrafficUI();
            if (UserPref.getInstance().getIsMyAppWasInBackground()) {
                SubscriptionHelper.getInstance().setSubscriptionFetched(false);
            }
        }
    }
    private void fetchTraffic(){
        NetworkManager.getInstance(FinjanVPNApplication.getInstance()).fetchTrafficConsumption(FinjanVPNApplication.getInstance(),
                new FetchTrafficConsumption());
    }

    private void fetchServer() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (VpnHomeFragment.this.isAdded()) {
                    if (mVpnLocationDescriptionValue != null && !TextUtils.isEmpty(mVpnLocationDescriptionValue.getText().toString())
                            && mVpnLocationDescriptionValue.getText().toString().startsWith(getString(R.string.updating))) {
                        selectLastServer(FinjanVPNApplication.getInstance());
                    }
                }

                if (AuthenticationHelper.getInstnace().getFinjanUser() == null) {
                    AuthenticationHelper.getInstnace().fetchUser(VpnHomeFragment.this.getContext());
                }
            }
        }).start();

    }

    private void setDefaults() {

        setAppSwitchViews();
    }

    private void checkWeatherShowPrivacyScreen() {
        if (!TextUtils.isEmpty(PlistHelper.getInstance().getUpdated_policy())) {
            Calendar updatePolicyCal = DateHelper.getInstnace().getServerDateCalender(PlistHelper.getInstance().getUpdated_policy());
            Calendar registeredDate = DateHelper.getInstnace().
                    getServerDateCalender(AuthenticationHelper.getInstnace().getFinjanUser().getUser_registered());
            Calendar previousReadingDate = null;
            if (!TextUtils.isEmpty(UserPref.getInstance().getPrivacyReadingDate())) {
                previousReadingDate = DateHelper.getInstnace().getServerDateCalender(UserPref.getInstance().getPrivacyReadingDate());
            }
            if (updatePolicyCal.after(registeredDate)) {
                if (previousReadingDate == null || updatePolicyCal.after(previousReadingDate)) {
                    showPrivacyScreen();
                } else {
                    showVpnScreen();
                }
            }
        }
    }

    private void showPrivacyScreen() {
        switchAnimation(true);
        if (TextUtils.isEmpty(tv_privacy_msg.getText().toString())) {
            tv_privacy_msg.setText(PlistHelper.getInstance().getPrivacyPolicyUpdateText());
        }
        if (mService != null) {
            switch (mService.getConnectionStatus()) {
                case LEVEL_CONNECTING_NO_SERVER_REPLY_YET:
                case LEVEL_CONNECTING_SERVER_REPLIED:
                case LEVEL_NONETWORK:
                case LEVEL_START:
                case LEVEL_CONNECTED: {
                    VpnUtil.stopVpnConnection(getActivity(), mService);
                }
            }
        }
    }

    private void showVpnScreen() {
        switchAnimation(false);
    }

    private void switchAnimation(boolean showPrivacy) {
        if (showingPrivacy) {
            if (showPrivacy) {
                return;
            }
        } else if (!showPrivacy) {
            return;
        }
        showingPrivacy = showPrivacy;
        if (NativeHelper.getInstnace().checkContext(getContext())) {
            if (showPrivacy) {
                mPageLayout.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.enter_to_right));
                mPageLayout.setVisibility(View.GONE);
                ll_private_policy.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.enter_from_left));
                ll_private_policy.setVisibility(View.VISIBLE);
                return;
            }
            mPageLayout.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.enter_from_right));
            mPageLayout.setVisibility(View.VISIBLE);
            ll_private_policy.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.enter_to_left));
            ll_private_policy.setVisibility(View.GONE);
        }
    }

    /**
     * Event whenever Licence is fetched
     */
    @SuppressWarnings("unused")
    public void onEventMainThread(LicenceFetchedFailed event) {
        PromoHelper.Companion.getInstance().checkIfDeepLinking(VpnActivity.getInstance());
        if (VpnApisHelper.Companion.getInstance().getWaitingLicenceForVpnConnect()) {
            changeStatusDisconnect();
        }
    }

    /**
     * Event whenever Licence is fetched
     */
    @SuppressWarnings("unused")
    public void onEventMainThread(LicenceFetchedVPN event) {
        if (VpnApisHelper.Companion.getInstance().getWaitingLicenceForVpnConnect()) {
            runVPN(getContext());
//            VpnApisHelper.Companion.getInstance().setWaitingLicenceForVpnConnect(false);
            return;
        }
    }

    /**
     * Event whenever Licence is fetched
     */
    @SuppressWarnings("unused")
    public void onEventMainThread(LicenceFetched event) {
        PromoHelper.Companion.getInstance().checkForUserValidity(this.getContext());
        if (event.getShouldRefresUi()) {
            if (event != null && event.getJsonObject() != null) {

                if (TextUtils.isEmpty(UserPref.getInstance().getTempPurchaseToken())) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (VpnHomeFragment.this.isAdded())
                                updateTrafficUI();
                        }
                    }, 100);
                }

                if (this.isAdded()) {
                    if (mVpnLocationDescriptionValue != null && !TextUtils.isEmpty(mVpnLocationDescriptionValue.getText().toString())
                            && mVpnLocationDescriptionValue.getText().toString().startsWith(getString(R.string.updating))) {
                        selectLastServer(FinjanVPNApplication.getInstance());
                    }
                }
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onResumeRefresh();
                }
            }, 200);
        }
    }

    /**
     * Event whenever Licence is fetched
     */
    @SuppressWarnings("unused")
    public void onEventMainThread(CancelledsubsFetched event) {
        if (isDeepLinkPromo(getActivity())) {
            PromoHelper.Companion.getInstance().checkIfDeepLinking(VpnActivity.getInstance());
        } else {
            PromoHelper.Companion.getInstance().checkForUserValidity(this.getContext());
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(DiscountedPromoAvailable event) {
        pb_upgrade.setVisibility(View.GONE);
        disableButtonsIfRequired();
        PromoHelper.Companion.setDiscountedRedeemListData(event.getRedeemListData());
    }

    @Override
    public void onPause() {
        super.onPause();
//        if(GlobalVariables.getInstnace().isLaunchVpnAcivityCalled ){return;}
        getActivity().unbindService(mConnection);
        VpnStatus.removeByteCountListener(this);

        if (isConnected) {
            stopRippleAnimation();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Hi guys, please, never ever remove this part of code from onDestroy
        // additionally do not forget to check isAdded()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final VpnStatusEvent event) {
        if (isAdded() && mService != null) {
            initStatus(event.getStatus());
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final FinjanUserFetched event) {

        VpnApisHelper.Companion.setUserFetched(true);
        PromoHelper.Companion.getInstance().checkForUserValidity(this.getContext());
        if (isAdded()) {
            LocalyticsTrackers.Companion.setUserRegisteredType();
            LocalyticsTrackers.Companion.setUserRegisteredPlateformType();
            LocalyticsTrackers.Companion.setPaidSource();
            LocalyticsTrackers.Companion.setDevicesRegistered();
            LocalyticsTrackers.Companion.setIsDesktopUser();
            LocalyticsTrackers.Companion.setIsIosUser();
            LocalyticsTrackers.Companion.setIsAndroidUser();
            LocalyticsTrackers.Companion.getInstance().subscribePaidUser(FinjanVPNApplication.getInstance());
            checkWeatherShowPrivacyScreen();
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final CancelConnectingEvent event) {
        if (isAdded()) {
            mStartButtonPressed = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    userCalledDisconnect = false;
                }
            }, 2000);
            initStatus(VpnStatus.ConnectionStatus.LEVEL_NOTCONNECTED);
        }
    }

    /**
     * Event whenever there's a change in the licenses
     */
    @SuppressWarnings("unused")
    public void onEventMainThread(LicensesRefreshedEvent event) {
        boolean pro = TrafficController.getInstance(getActivity()).isPaid();
        Logger.logD(TAG, "LicensesRefreshedEvent pro? " + pro);

        if (isAdded()) {
            updateTrafficUI();
//            updateLicense();

            if (mService != null) {
                if (!VpnApisHelper.Companion.getInstance().getWaitingLicenceForVpnConnect()) {
                    initStatus(mService.getConnectionStatus());
                }
            }
        }
    }

    @MainThread
    protected void initStatus(VpnStatus.ConnectionStatus status) {

        switch (status) {
            case LEVEL_NOTCONNECTED:
                if (mStartButtonPressed) {
                    changeStatusConnecting();
                } else {
//                    if(VpnApisHelper.Companion.getInstance().getWaitingLicenceForVpnConnect())
                    changeStatusDisconnect();
                }
                break;
            case LEVEL_NONETWORK:
                if (mStartButtonPressed) {
                    changeStatusConnecting();
                } else {
                    changeStatusDisconnect();
                }
                break;
            case LEVEL_AUTH_FAILED: {
                if (mStartButtonPressed) {
                    // after user pressed start button
                    // change state immediately
                    changeStatusConnecting();
                } else {
//                    if(connectVpnWasPressed){
                    Logger.logI(TAG, "unable to authenticate user, Verfication failed");
                    VpnActivity.showSnackBar(getString(R.string.unable_to_authenticate_you_try_again), getView());
//                    }
                    changeStatusDisconnect();
                }
                break;
            }
            case LEVEL_CONNECTING_NO_SERVER_REPLY_YET:
            case LEVEL_CONNECTING_SERVER_REPLIED: {
                if (mStartButtonPressed) {
                    mStartButtonPressed = false;
                }
                changeStatusConnecting();
                break;
            }
            case LEVEL_CONNECTED: {
                changeStatusConnected();
                break;
            }
            case LEVEL_CONNECTION_CANCELED:
                changeStatusDisconnect();
                break;
        }
    }

    public void setSelectedRegion(long id, String region, final String title, final int flag) {
        if (isAdded()) {
            GoogleTrackers.Companion.setELocationChange();
            connectedRegion = title;
            InitializeVpn.getInstance().setCurrentLocation(title);
            InitializeVpn.getInstance().setCurrentLocationId(region);
            mSelectedServerId = id;
            mSelectedServerRegion = region;

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mVpnLocationDescriptionValue.setText(Html.fromHtml(title));
                        if (flag != 0) {
                            iv_flag.setImageResource(flag);
                        } else
                            iv_flag.setImageResource(CountryFlags.getInstance().getCountryFlag(title));
                    }
                });
            }

            LocalyticsTrackers.Companion.setLastCountryConnected(title);
//            mVpnLocationDescriptionValue.setCompoundDrawablesWithIntrinsicBounds(
//                    CountryFlags.getInstance().getCountryFlag(title), 0, 0, 0);
//            mVpnLocationDescriptionValue.setCompoundDrawablePadding((int)getContext().getResources().getDimension(R.dimen._10sdp));
        }
    }

    @Override
    public void updateByteCount(long in, long out, long diffIn, long diffOut) {
        TrafficController trafficController = TrafficController.getInstance(FinjanVPNApplication.getInstance());
        final long traffic = in + out + trafficController.getTraffic();
//            final long  traffic = trafficController.getNewTraffic()+ trafficController.getTraffic();
        final long trafficLimit = trafficController.getTrafficLimit();

        if (isAdded() && isVpnConnected()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setNewTrafficUI(traffic, trafficLimit);
                }
            });
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        upgradeDialogShowTime = 0;
    }

    private void showUpgradeDialog() {
        if (!isAdded()) {
            return;
        }
        upgradeDialogShowTime = System.currentTimeMillis();
        boolean registered = TrafficController.getInstance(getContext()).isRegistered();

        String[] content = {registered ? "" : getString(R.string.register), getString(R.string.upgrade)};

        if (dialog != null) {

            if (TextUtils.isEmpty(dialog.getContents()[0]) == TextUtils.isEmpty(content[0])) {
                if (!dialog.isShowing()) {
                    dialog.show();
                }
                return;
            } else {
                dialog.dismiss();
            }
        }
//        String msg = registered ? TrafficController.getInstance(FinjanVPNApplication.getInstance()).isLicenseDateValid()?
//                getString(R.string.register_vpn_alert): getString(R.string.register_vpn_alert_license) : getString(R.string.unregister_vpn_alert);
        String msg = registered ? getString(R.string.register_vpn_alert_license) : getString(R.string.unregister_vpn_alert);
        dialog = new FinjanDialogBuilder(getContext())
                .outSideClickable(false)
                .setMessage(msg)
                .setContent(content)
                .setPositiveButton(getString(R.string.no_thanks_for_now))
                .setDialogClickListener(new FinjanDialogBuilder.DialogClickListener() {
                    @Override
                    public void onNegativeClick() {
                        VpnHomeFragment.this.getActivity().onBackPressed();
                    }

                    @Override
                    public void onPositivClick() {
                    }

                    @Override
                    public void onContentClick(String content) {
                        if (content.toLowerCase().equalsIgnoreCase(getString(R.string.register))) {
                            onButtonLoginOrRegister();

                        } else if (content.toLowerCase().equalsIgnoreCase(getString(R.string.upgrade))) {
                            onClickUpgrade(false, true);

                        } else if (content.toLowerCase().equalsIgnoreCase(getString(R.string.upgrade))) {
                            onPositivClick();

                        }
                    }
                });
        if (this.isAdded())
            dialog.show();
//        ((VpnActivity)getActivity()).loginToContinue();
    }

    /**
     * Run only from UI thread!!!
     */
    private void setNewTrafficUI(long traffic, long trafficLimit) {
        Logger.logD(TAG, "setNewTrafficUI traffic " + traffic + " trafficLimit " + trafficLimit);

        if (!isAdded()) {
            // do not perform any data on non added fragment
            return;
        }

        final String trafficDescription;
        int progress = 0;
        SpannableString spannableString = null;
        String trafficConsumeed = null;
        String readableTraficLimit = TrafficController.getInstance(getContext()).getOnlyMbs(Util.humanReadableByteCount(trafficLimit, false));
//        String readableTraficLimit = NativeHelper.getInstnace().getRoundOffString(Util.humanReadableByteCount(trafficLimit, false));

        if (!TrafficController.getInstance(getContext()).isPaid()/*trafficLimit>0*/) {
            if (!UserPref.getInstance().getStatus().equals("free"))
            {
                UserPref.getInstance().setStatus("free");
//                Toast.makeText(getActivity(), "free", Toast.LENGTH_SHORT).show();
                HashMap map = new HashMap<String, String>();
                map.put("Status", String.valueOf("free"));
                LocalyticsTrackers.Companion.setCurrentSubscriptionStatus(map);
            }
            trafficDescription = getString(R.string.txt_traffic_description_modified, readableTraficLimit);

            double percentage = (traffic * 100d) / trafficLimit;
            percentage = Math.floor(percentage * 100) / 100;
            progress = (int) percentage;
//            String readableRemainingTrafic = (NativeHelper.getInstnace().getRoundOffString(Util.humanReadableByteCount(traffic, false)));
            String readableRemainingTrafic = TrafficController.getInstance(getContext()).getRemainingTraffic();
            isReachedTrafficLimit = false;
//            tv_start_trial.setVisibility(View.VISIBLE);
            if (!AppConfig.Companion.getMakeVpnConnectEventOnTrafficReached()) {
                try {
                    if (percentage >= (100 - Double.parseDouble(PlistHelper.getInstance().getData_tank_min())) && isLowDataReached) {
                        if (PromoHelper.Companion.getInstance().lowDataPromoActivation(getActivity())) {
                            isLowDataReached = false;
                            Logger.logE("low_data", "Activated");
                        } else {
                            isLowDataReached = false;
                            Logger.logE("low_data", "unavailable");
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                if (percentage >= 100) {
                    isReachedTrafficLimit = true;
                    reachedTrafficLimit();
                }else {
                  //  UserPref.getInstance().setAutoConnect(true);

                }
            }


            trafficConsumeed = readableRemainingTrafic;
            spannableString = new SpannableString(trafficDescription);
            int end = 3 + readableTraficLimit.length();
            spannableString.setSpan(new ForegroundColorSpan(Util.getColor(FinjanVPNApplication.getInstance(), R.color.col_content)), 3, end, 0);// set color
        } else {
           // UserPref.getInstance().setAutoConnect(true);

            isReachedTrafficLimit = false;
            trafficDescription = getActivity().getString(R.string.txt_no_traffic_limit_description);

            if (TrafficController.getInstance(getContext()).isPaid()) {
                trafficConsumeed = getActivity().getString(R.string.txt_no_traffic_limit);
                checkIfAndSetDaysLeft();
                hideProgressBar();
            } else if (!TrafficController.getInstance(getContext()).isRegistered()) {
                trafficConsumeed = "0";
                showProgressBar();
            }else if (TrafficController.getInstance(getActivity()).isRegistered())
            {
                if (!UserPref.getInstance().getStatus().equals("free"))
                {
                    UserPref.getInstance().setStatus("free");
//                    Toast.makeText(getActivity(), "free", Toast.LENGTH_SHORT).show();
                    HashMap map = new HashMap<String, String>();
                    map.put("Status", String.valueOf("free"));
                    LocalyticsTrackers.Companion.setCurrentSubscriptionStatus(map);
                }
            }
            spannableString = new SpannableString(trafficDescription);
            spannableString.setSpan(new ForegroundColorSpan(Util.getColor(FinjanVPNApplication.getInstance(), R.color.col_content)), 0, trafficDescription.length(), 0);// set color
        }

        // prevent change UI during app in Connecting stage
        if (!mStartButtonPressed && !isVpnConnecting()) {
//            showProgressBar();
            mTrafficCounter.setVisibility(View.VISIBLE);
            mTrafficDescription.setVisibility(View.VISIBLE);
            updateProgressBar(progress);
            if (isSecureUnlimitedAvailable) {
                mTrafficCounter.setText(trafficConsumeed);
//                mTrafficDescription.setVisibility(View.VISIBLE);

                mTrafficDescription.setText(trafficDescription);

            }
        }
        if (!isReachedTrafficLimit) {
            if (!TextUtils.isEmpty(mVpnLocationDescription.getText()) &&
                    mVpnLocationDescription.getText().toString().equals(getString(R.string.txt_no_traffic_limit_location))) {
                mVpnLocationDescription.setText(getString(R.string.server_location));
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }
    }

    private void hideProgressBar() {
        if (getView() != null) {
            {
                getView().findViewById(R.id.ll_progressbar).setVisibility(View.GONE);
                getView().findViewById(R.id.iv_guage).setVisibility(View.GONE);
//                if (isPromoInfoGone)
//                {
//                    mTrafficDescription.setVisibility(View.VISIBLE);
//                }
            }

        }
    }

    private void updatePromoDaysLeftText(RedeemListData redeemListData, Subscriptions subscription){
        if (redeemListData != null) {
            if (!UserPref.getInstance().isPromoConsumed(redeemListData.getId()) )
            {
                UserPref.getInstance().setPromoConsumed(redeemListData.getId());
                HashMap map =new HashMap<String, String>();
                map.put("Promo_ID", redeemListData.getId());
                LocalyticsTrackers.Companion.setEPromoStart(map);
                Logger.logD("triggerLocalitic","Start Promo"+redeemListData.getId());
            }
            shouldShowUpdateBtn = false;
            PromoHelper.Companion.setCurrentActivePromo(redeemListData);

            llPromoInfo.setVisibility(View.VISIBLE);
//                 tvpromoname = (TextView) getView().findViewById(R.id.tvpromoname);
//                 tvpromo = (TextView) getView().findViewById(R.id.tvpromo);
//                TextView tvpromoexpirydate = (TextView) getView().findViewById(R.id.tvpromoexpirydate);
            tvpromoname.setVisibility(View.VISIBLE);
            tvpromo.setVisibility(View.VISIBLE);
            tvpromoexpirydate.setVisibility(View.VISIBLE);
            buttonUpgradeVpn.setVisibility(View.VISIBLE);
            /*  if (!isPromoInfoGone) {*/
            //mTrafficDescription.setVisibility(View.INVISIBLE);



            mUpgradeButton.setText(redeemListData.getAttributes().getPopup_button_text());

//            }
            tvpromoname.setText(redeemListData.getAttributes().getTitle());
            tvpromo.setText(redeemListData.getAttributes().getTitle_2());
            Logger.logD("createddate",PlistHelper.getInstance().getCurrent_date());
            Logger.logD("subscriptionExpiry",subscription.getExpires_at());

            int noOfDays = com.electrolyte.utils.DateHelper.getInstnace().getNoOfDaysLeft(subscription.getExpires_at(), PromoHelper.Companion.getInstance().getCurrentDateStr());
            if (noOfDays>0) {
                tvpromoexpirydate.setText("- " + noOfDays + "" + " Days Left -");
                if (islastDayPromo) {
                    //firelocalitics
                    islastDayPromo = false;
                    HashMap map = new HashMap<String, String>();
                    map.put("Promo_ID", redeemListData.getId());
                    map.put("Days_Left", String.valueOf(noOfDays));
                    LocalyticsTrackers.Companion.setLastDayPromoWarning(map);
                }
            }
        }
    }
    private RedeemListData curentRedeemData;
    private  boolean isSecureUnlimitedAvailable =true;
    public void checkIfAndSetDaysLeft() {
        if (getView() != null && getView().findViewById(R.id.loadergif) != null && getView().findViewById(R.id.loadergif).getVisibility() == View.GONE && isLoderShowing) {
            isLoderShowing = false;
            llPromoInfo.setVisibility(View.GONE);
            getView().findViewById(R.id.tvpromoname).setVisibility(View.GONE);
            getView().findViewById(R.id.tvpromo).setVisibility(View.GONE);

            getView().findViewById(R.id.tvpromoexpirydate).setVisibility(View.GONE);
            getView().findViewById(R.id.ll_progressbar).setVisibility(View.GONE);
            getView().findViewById(R.id.iv_guage).setVisibility(View.GONE);
            getView().findViewById(R.id.loadergif).setVisibility(View.VISIBLE);
        }
        HashMap<String, ArrayList<Subscriptions>> allSubscriptions = SubscriptionHelper.getInstance().getSubscriptionsHashMap();
        ArrayList<Subscriptions> promoSubscriptionList = null;
        //Logger.logE("gautam", "hitsubscription5");
        if (allSubscriptions.size() > 0) {
            ArrayList<Subscriptions> googleSubscriptionList = allSubscriptions.get(JsonConstans.GOOGLE);
            if (googleSubscriptionList!=null && googleSubscriptionList.size()>0)
            {
//                if (isToHitCanclePromo) {
//                        isToHitCanclePromo = false;
//                    SubscriptionHelper.getInstance().cancelAllPromos(getContext(), false);
//                }
                isSecureUnlimitedAvailable =true;
                shouldShowUpdateBtn = true;
                if (getView() != null && getView().findViewById(R.id.llpromoInfo) != null && getView().findViewById(R.id.llpromoInfo).getVisibility() == View.VISIBLE)

                    getView().findViewById(R.id.llpromoInfo).setVisibility(View.GONE);

                if (getView() != null && getView().findViewById(R.id.loadergif) != null && getView().findViewById(R.id.loadergif).getVisibility() == View.VISIBLE)
                    getView().findViewById(R.id.loadergif).setVisibility(View.GONE);
//                disableButtonsIfRequired();
                buttonUpgradeVpn.setVisibility(View.GONE);
                if (!UserPref.getInstance().getStatus().equals("paid"))
                {
                    UserPref.getInstance().clearPrefOnPrimium();
                    UserPref.getInstance().setStatus("paid");
//                    Toast.makeText(getActivity(), "paid", Toast.LENGTH_SHORT).show();
                    HashMap map = new HashMap<String, String>();
                    map.put("Status", String.valueOf("paid"));
                    LocalyticsTrackers.Companion.setCurrentSubscriptionStatus(map);
                }
                //mTrafficCounter.setText("UNLIMITED");
//                mTrafficDescription.setVisibility(View.VISIBLE);

                //mTrafficDescription.setText("Secure Browser");

                return;
            }
            promoSubscriptionList = allSubscriptions.get(JsonConstans.PROMO);
        } else {
            return;
        }

        if (promoSubscriptionList != null) {
            // Logger.logE("gautam", "hitsubscription3");
            for (final Subscriptions subscription : promoSubscriptionList) {
                //    Logger.logE("gautam", "hitsubscription2");

                if (subscription.getCancelled().equals("")) {
                    //  Logger.logE("gautam", "hitsubscription");
                    if (subscription.getComp_flg()!=null && !subscription.getComp_flg().equals("")) {

                        PromoHelper.Companion.setPromoActive(true);
                        isSecureUnlimitedAvailable = false;
                        mTrafficCounter.setText("");
                        mTrafficDescription.setText("");
                        if (!UserPref.getInstance().getStatus().equals("promo"))
                        {
                            UserPref.getInstance().clearPrefOnPrimium();
                            UserPref.getInstance().setStatus("promo");
//                           Toast.makeText(getActivity(), "promo", Toast.LENGTH_SHORT).show();
                            HashMap map = new HashMap<String, String>();
                            map.put("Status", String.valueOf("promo"));
                            LocalyticsTrackers.Companion.setCurrentSubscriptionStatus(map);
                        }
                    }

                    if(curentRedeemData!=null && curentRedeemData.getId().equals(subscription.getComp_flg())){
                        updatePromoDaysLeftText(curentRedeemData,subscription);
                        if (getView() != null && getView().findViewById(R.id.loadergif) != null && getView().findViewById(R.id.loadergif).getVisibility() == View.VISIBLE)
                            getView().findViewById(R.id.loadergif).setVisibility(View.GONE);
                    }else {


                        NetworkManager.getInstance(getActivity()).homeRedeem(getActivity(), new NetworkResultListener() {
                            @Override
                            public void executeOnSuccess(JSONObject response) {
                                if (getView() != null && getView().findViewById(R.id.loadergif) != null && getView().findViewById(R.id.loadergif).getVisibility() == View.VISIBLE)
                                    getView().findViewById(R.id.loadergif).setVisibility(View.GONE);
                                if (response != null) {
                                    ArrayList<RedeemListData> redeemListDataArrayList = RedeemListData.parseRedeemList(response);
                                    if (redeemListDataArrayList != null && redeemListDataArrayList.size() > 0) {
                                        boolean isPromoFound = false;
                                        for (RedeemListData redeemListData : redeemListDataArrayList)
                                        {
                                            if (redeemListData.getId().equals(subscription.getComp_flg()))
                                            {

                                                if (curentRedeemData == null || !curentRedeemData.getId().equals(subscription.getComp_flg())) {
                                                    curentRedeemData = redeemListData;
                                                }
                                                updatePromoDaysLeftText(redeemListData, subscription);
                                                isPromoFound = true;
                                                break;
                                            }
                                        }
                                        if (!isPromoFound){
                                            SubscriptionHelper.getInstance().cancelAllPromos(getActivity(),false);
                                        }
                                    }
                                }
                            }

                            @Override
                            public void executeOnError(VolleyError error) {
                                if (getView() != null && getView().findViewById(R.id.loadergif) != null && getView().findViewById(R.id.loadergif).getVisibility() == View.VISIBLE)
                                    getView().findViewById(R.id.loadergif).setVisibility(View.GONE);
                            }
                        });

                      /*  NetworkManager.getInstance(getActivity()).getSinglePromo(getActivity(), subscription.getComp_flg(), new NetworkResultListener() {
                            @Override
                            public void executeOnSuccess(JSONObject response) {

                                if (getView() != null && getView().findViewById(R.id.loadergif) != null && getView().findViewById(R.id.loadergif).getVisibility() == View.VISIBLE)
                                    getView().findViewById(R.id.loadergif).setVisibility(View.GONE);

                                if (response != null) {
                                    ArrayList<RedeemListData> redeemListDataArrayList = RedeemListData.parseRedeemList(response);
                                    if (redeemListDataArrayList != null && redeemListDataArrayList.size() > 0) {
                                        RedeemListData redeemListData = redeemListDataArrayList.get(0);
                                        if (curentRedeemData == null || !curentRedeemData.getId().equals(subscription.getComp_flg())) {
                                            curentRedeemData = redeemListData;
                                        }
                                        updatePromoDaysLeftText(redeemListData, subscription);
                                    }
                                }
                            }

                            @Override
                            public void executeOnError(VolleyError error) {


                            }
                        });*/
                     /* ArrayList<RedeemListData> reedeemlist = PromoHelper.Companion.getInstance().getAllPromoList(getActivity());
                      for (RedeemListData redeemListData : reedeemlist)
                      {
                          if (redeemListData.getId().equals(subscription.getComp_flg()))
                          {
                              if (getView() != null && getView().findViewById(R.id.loadergif) != null && getView().findViewById(R.id.loadergif).getVisibility() == View.VISIBLE)
                                  getView().findViewById(R.id.loadergif).setVisibility(View.GONE);

                              if (curentRedeemData == null || !curentRedeemData.getId().equals(subscription.getComp_flg())) {
                                  curentRedeemData = redeemListData;
                              }
                              updatePromoDaysLeftText(redeemListData, subscription);
                              break;
                          }
                      }*/
                    }

                    break;

                } else {
                    if (getView() != null && getView().findViewById(R.id.loadergif) != null && getView().findViewById(R.id.loadergif).getVisibility() == View.VISIBLE)
                        getView().findViewById(R.id.loadergif).setVisibility(View.GONE);
                }
            }
        } else {
            if (getView() != null && getView().findViewById(R.id.loadergif) != null && getView().findViewById(R.id.loadergif).getVisibility() == View.VISIBLE)
                getView().findViewById(R.id.loadergif).setVisibility(View.GONE);
        }
    }

    private void showProgressBar() {
        if (getView() != null) {
            getView().findViewById(R.id.llpromoInfo).setVisibility(View.GONE);
            getView().findViewById(R.id.tvpromoname).setVisibility(View.GONE);
            getView().findViewById(R.id.tvpromo).setVisibility(View.GONE);
//            isPromoInfoGone = true;
            getView().findViewById(R.id.tvpromoexpirydate).setVisibility(View.GONE);
            getView().findViewById(R.id.loadergif).setVisibility(View.GONE);
            getView().findViewById(R.id.ll_progressbar).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.iv_guage).setVisibility(View.VISIBLE);



        }
    }

    private void reachedTrafficLimit() {
        if (upgradeDialogShowTime + 5000 < System.currentTimeMillis())
            // showUpgradeDialog();
            if (isVpnConnected()) {
                VpnUtil.stopVpnConnection(getActivity(), mService);
            }

        if (!isVpnConnected() && !isVpnConnecting()) {
            if (TrafficController.getInstance(getActivity()).isRegistered()) {
                loginRegisterButton.setVisibility(View.GONE);
            } else {
                loginRegisterButton.setVisibility(View.GONE);
            }
//            mVpnLocationDescriptionValue.setVisibility(View.GONE);
            mVpnLocationDescription.setText(R.string.txt_no_traffic_limit_location);
        }
    }

    public boolean isVpnConnected() {
        return mService != null && mService.getConnectionStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTED;
    }

    private boolean isVpnConnecting() {
        return (mService != null && mStartButtonPressed && (mService.getConnectionStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTING_SERVER_REPLIED
                || mService.getConnectionStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTING_NO_SERVER_REPLY_YET)) || connetingToVpn;
    }

    private void selectLastServer(@NonNull Context context) {
        String serverRegion = FinjanVpnPrefs.getLastUsedServer(context);
        String localyticsRegion = serverRegion;
        if (serverRegion.equals(FinjanVpnPrefs.getDefaultServer())) {
            localyticsRegion = "Nearest Location";
            LocalyticsTrackers.Companion.setLastCountryConnected(localyticsRegion);
        }

        if (!TextUtils.isEmpty(serverRegion)) {
            ContentProviderClient contentProviderClient = new ContentProviderClient();
            Cursor cursor = contentProviderClient.getServer(context, serverRegion);
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    long id = cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID));
                    String title = cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.NAME));
                    connectedRegion = title;
                    setSelectedRegion(id, serverRegion, title, CountryFlags.getInstance().getCountryFlag(serverRegion));
                }
                AutoClose.closeSilently(cursor);
            }
        }
    }

    protected void changeRegionNew() {
        if (!mStartedNewConnection && !mStartButtonPressed && mCanSwitchRegion) {
            if (updatingServerList) {
                VpnActivity.showSnackBar(getString(R.string.please_wait_while_we_updating_regions), getView());
                return;
            }
            Cursor cursor = new ContentProviderClient().getAllServers(FinjanVPNApplication.getInstance());
            int count = 0;
            if (cursor != null) {
                count = cursor.getCount();
            }
            AutoClose.closeSilently(cursor);

            if (count == 0) {
                if (Util.isNetworkConnected(FinjanVPNApplication.getInstance())) {
                    updateVpnServerList();
                } else {
                    VpnActivity.showSnackBar(R.string.txt_no_internet_connection, getView());
                }
                return;
            }
            try {
                if ( AppConfig.Companion.getLetsMakeRegisteredChangeReg() ||
                        TrafficController.getInstance(FinjanVPNApplication.getInstance().getApplicationContext()).isPaid()) {

                }else
                if (PromoHelper.Companion.getInstance().ifLocationChangePromoOffer(getActivity(),false)) {
                    // Toast.makeText(getActivity(), "Gautam", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    HashMap map = new HashMap<String, String>();
                    map.put("result", "fail");
                    LocalyticsTrackers.Companion.setELocationChange(map);
                }
                RegionAcitivity.Companion.getInstance(getActivity());

            } catch (IllegalStateException e) {
                Logger.logE(TAG, "add fragment 2 times" + e);
            }
        } else {
            if (!mCanSwitchRegion) {
                VpnUtil.stopVpnConnection(getActivity(), mService);
                mStartedNewConnection = false;
                mStartButtonPressed = false;
                mCanSwitchRegion = true;
                reconnectingToVpn = true;
                changeRegionNew();
            }
        }
    }

    private void onRegionSelet(long id, String region, String title, int flag) {

        if (region.equals("nearest") || AppConfig.Companion.getLetsMakeRegisteredChangeReg() ||
                TrafficController.getInstance(FinjanVPNApplication.getInstance().getApplicationContext()).isPaid()) {
            if (id > 0) {
                HashMap map = new HashMap<String, String>();
                map.put("result", "success");
                LocalyticsTrackers.Companion.setELocationChange(map);
                setSelectedRegion(id, region, title, flag);
            }
        } else if (PromoHelper.Companion.getInstance().ifLocationChangePromoOffer(getActivity(),true)) {
//            ToastHelper.showToast(getActivity(),"Success",Toast.LENGTH_LONG);
            /*if (id > 0) {
                HashMap map = new HashMap<String, String>();
                map.put("result", "success");
                LocalyticsTrackers.Companion.setELocationChange(map);

            }*/
            setSelectedRegion(id, region, title, flag);

        } else {
//            HashMap map = new HashMap<String, String>();
//            map.put("result", "fail");
//            LocalyticsTrackers.Companion.setELocationChange(map);
            if (!PlistHelper.getInstance().getShowFreeUserLocationChangePopup()) {
                new FinjanDialogBuilder(getActivity())
                        .setMessage(PlistHelper.getInstance().vpnLocationChangeAlert)
                        .shouldHaveCancelButton(true)
                        .setPositiveButton(PlistHelper.vpnLocationChangeAlertOkBtn)
                        .setCustomPositiveBtnColour(PlistHelper.IAPOKButtonBg)
//                    .setCustomNegativeBtnColour(PlistHelper.IAPLaterButtonBg)
//                    .setNegativeButton(PlistHelper.vpnLocationChangeAlertCBtn)
                        .setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                            @Override
                            public void onNegativeClick() {
                            }
                        })
                        .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                            @Override
                            public void onPositivClick() {
                                LocalyticsTrackers.Companion.setEUpgrade_from_location_change();
                                onClickUpgrade();
                            }
                        }).show();
            }

        }
    }

    private RegionDialogFragment getRegionDialogFragment() {
        return (RegionDialogFragment) getChildFragmentManager().findFragmentByTag(RegionDialogFragment.TAG);
    }

    @OnClick(R.id.tv_start_trial)
    public void onClickTrial() {
        onClickUpgrade();
    }

    @OnClick(R.id.buttonUpgradeVpn)
    public void onClickUpgrade() {
        onClickUpgrade(false, true);
    }

    public void onClickUpgrade(boolean throughOther, boolean showPopup) {

        if (TrafficController.getInstance(getContext()).isRegistered()) {
            startActivity(upgradeIntent(getContext(), throughOther, showPopup, true));
        } else {
            SetupAccountActivity.newLoginInstanceForResult((Activity) getContext(),
                    VpnActivity.REQUEST_Login_For_Upgrade, SetupAccountActivity.ACTION_SIGN_NAV);
        }
    }

    private void hideButtonWithFade(ImageView imageView) {
        hideButtonWithFade(imageView, 300);
    }

    private void hideButtonWithFade(ImageView imageView, int duration) {
        AlphaAnimation anim = new AlphaAnimation(1f, 0f);
        anim.setDuration(duration);
        imageView.startAnimation(anim);
        imageView.setVisibility(View.GONE);
        imageView.setAlpha(1f);
    }

    private void showButtonWithFade(ImageView imageView, int duration) {
        imageView.setAlpha(0f);
        imageView.setVisibility(View.VISIBLE);
        AlphaAnimation anim = new AlphaAnimation(0f, 1f);
        anim.setDuration(duration);
        imageView.startAnimation(anim);
        imageView.setAlpha(1f);
    }

    @OnClick(R.id.iv_vpn_btn)
    public void onButtonConnectClick() {


        /*if (isVpnConnected()) {
            if (UserPref.getInstance().getAutoConnect() && AutoConnectHelper.Companion.isAutoConnection()) {
                if (WifiSecurityHelper.getInstance().isWifiNetwork(getContext()) &&
                        !AutoConnectHelper.Companion.getInstance().checkIfTrusted(getContext())) {
                    AutoConnectHelper.Companion.getInstance().onManuallDisconnectAutoConnect(getContext(), this);
                    return;
                }
            }
//          AutoConnectHelper.Companion.getInstance().setUserDisconnectedAutoConnection(true);
        }*/

        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(getActivity(), false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FinjanVPNApplication.getInstance().setIsMyAppInBackGround(getActivity(), false);

            }
        }, 500);
        onClickVpnButton(true);
    }

    public void clickVPNBtnOnUi() {
        VpnHomeFragment.this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isVpnConnected() || isVpnConnecting()) {
                    return;
                }
                AutoConnectHelper.Companion.setAutoConnection(true);
                onClickVpnButton(false);
                ToastHelper.showToast(VpnHomeFragment.this.getContext(),
                        ResourceHelper.getInstance().getString(R.string.Auto_connecting_VPN), Toast.LENGTH_SHORT);
            }
        });
    }

    public void onClickVpnButton(boolean isPressedByUser) {
        //if the user turns the VPN off while on Wifi and auto protect is on, you should a) turn auto protect off, and b) trigger a localytics event called "Auto Protect Alert".
        if (isPressedByUser && isVpnConnected() && UserPref.getInstance().getAutoConnect() && WifiSecurityHelper.getInstance().isWifiNetwork(getContext())) {
            AutoConnectHelper.Companion.getInstance().onManuallDisconnectAutoConnect(getContext(), this);
            setAutoProtectUi();
            FileLogger.logToFile(VpnHomeFragment.class, "turn off auto protect and trigger Auto Protect Alert");
        }
        onClickVpnButton2();
    }

    public void onClickVpnButton2() {
        FinjanVpnPrefs.saveLastUsedServer(FinjanVPNApplication.getInstance(), mSelectedServerRegion);
        FileLogger.logToFile(VpnHomeFragment.class, "runVPN " + mSelectedServerRegion);

        if (mStartButtonPressed) {
            mStartButtonPressed = false;
            hideButtonWithFade(ivPlaceHolderFill);
        }

        // if VPN is connected give possibility to disconnect
        if (!isVpnConnected() && isReachedTrafficLimit) {
            // showUpgradeDialog();
        } else if (!mStartedNewConnection) {
            mStartedNewConnection = true;
            if (mService != null) {
                switch (mService.getConnectionStatus()) {
                    case LEVEL_CONNECTING_NO_SERVER_REPLY_YET:
                    case LEVEL_CONNECTING_SERVER_REPLIED:
                    case LEVEL_NONETWORK:
                    case LEVEL_START:
                    case LEVEL_CONNECTED: {
                        AutoConnectHelper.Companion.setAutoConnection(false);
                        VpnUtil.stopVpnConnection(getActivity(), mService);
                        return;
                    }
                }
            }

            if (Util.isNetworkConnected(FinjanVPNApplication.getInstance())) {
                if (TrafficController.getInstance(getActivity()).isPaid() || hasEnoughTraffic(FinjanVPNApplication.getInstance()) || AppConfig.Companion.getMakeVpnConnectEventOnTrafficReached()) {
                    VpnApisHelper.Companion.getInstance().setWaitingLicenceForVpnConnect(true);
                    changeStatusConnecting();
                    //         runVPN(FinjanVPNApplication.getInstance(), mSelectedServerId);
                   /* VpnApisHelper.Companion.getInstance().updateLicence(true,true, this.getContext(), new NetworkResultListener() {
                        @Override
                        public void executeOnSuccess(JSONObject response) {

 runVPN(FinjanVPNApplication.getInstance(), mSelectedServerId);
                        }

                        @Override
                        public void executeOnError(VolleyError error) {

                        }
                    });*/
                    NetworkManager.getInstance(getActivity()).fetchAviraLicence(getActivity(), new NetworkResultListener() {
                        @Override
                        public void executeOnSuccess(JSONObject response) {

                        }

                        @Override
                        public void executeOnError(VolleyError error) {

                        }
                    });
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runVPN(FinjanVPNApplication.getInstance(), mSelectedServerId);
                        }
                    },8000);
                } else {
                    VpnActivity.showSnackBar(R.string.snackbar_notification_no_available_traffic, getView());
                    mStartedNewConnection = false;
                }
            } else {
                VpnActivity.showSnackBar(R.string.txt_no_internet_connection, getView());
                mStartedNewConnection = false;
            }
        }
    }

    private boolean hasEnoughTraffic(@NonNull Context context) {
        TrafficController trafficController = TrafficController.getInstance(context);
        long trafficLimit = trafficController.getTrafficLimit();
        return trafficLimit == 0 || trafficLimit > trafficController.getTraffic();
//        return true;
    }

    /**
     * update only once per day
     *
     * @param context
     */
    private void checkVpnServerList(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        long lastUpdateTime = prefs.getLong(VPN_SERVER_LIST, 0l);
        String defaultServer = prefs.getString(JsonConfigParser.DEFAULT_SERVER, null);

        if (lastUpdateTime + HOURS_24 <= System.currentTimeMillis() || TextUtils.isEmpty(defaultServer)) {
            updateVpnServerList();
        }
    }

    protected void updateVpnServerList() {
        if (this.isAdded()) {
            updatingServerList = true;
            mVpnLocationDescriptionValue.setText(getString(R.string.updating_regions));
            NetworkManager.getInstance(FinjanVPNApplication.getInstance())
                    .fetchRegionsList(FinjanVPNApplication.getInstance(), new FetchServerList());
        }
    }

    /**
     * method will called after every successful fetch of license
     * and but run VPN only if initially tried to connect VPN, tried to fetch license before connecting
     * make variable  "waitingLicenceForVpnConnect" false so that
     *
     * @param context
     * @param serverVpnId
     */
    protected void runVPN(Context context, long serverVpnId) {
        if (!VpnApisHelper.Companion.getInstance().getWaitingLicenceForVpnConnect()) {
            return;
        }

        VpnApisHelper.Companion.getInstance().setWaitingLicenceForVpnConnect(false);
        if (serverVpnId <= 0) {
            serverVpnId = getDefaultServerVpnId(context);
        }

        if (mVpnLocationDescriptionValue != null && (TextUtils.isEmpty(mVpnLocationDescriptionValue.getText().toString())
                || (!TextUtils.isEmpty(mVpnLocationDescriptionValue.getText().toString())))) {
            if (this.isAdded()) {
                if (mVpnLocationDescriptionValue.getText().toString().startsWith(getString(R.string.updating))) {
                    selectLastServer(FinjanVPNApplication.getInstance());
                }
            } else {
                selectLastServer(FinjanVPNApplication.getInstance());
            }
        }



        if (getContext() != null) {
            boolean isRunned = VpnUtil.startVpnConnection(getContext(), serverVpnId, false);
            GlobalVariables.getInstnace().isLaunchVpnAcivityCalled = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    GlobalVariables.getInstnace().isLaunchVpnAcivityCalled = false;
                }
            }, 2000);

            if (isRunned) {
                mStartButtonPressed = true;
                connetingToVpn = true;
                changeStatusConnecting();
            } else {
                VpnActivity.showSnackBar(R.string.can_not_connect_to_region, getView());
                mStartedNewConnection = false;
            }
        }
    }

    private void userTapVpnInit() {
        startRippleAnim();
    }

    private long getDefaultServerVpnId(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String defaultServer = prefs.getString(JsonConfigParser.DEFAULT_SERVER, null);

        if (!TextUtils.isEmpty(defaultServer)) {
            ContentProviderClient contentProviderClient = new ContentProviderClient();
            Cursor cursor = contentProviderClient.getServer(context, defaultServer);
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    mSelectedServerRegion = defaultServer;
                    mSelectedServerId = cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID));
                }
                AutoClose.closeSilently(cursor);
            }
        }
        return mSelectedServerId;
    }

    private void vinceFingerPointing() {
        if (progressDialog != null && progressDialog.isShowing() || isVpnConnecting()) {
            return;
        }
        mConnectionStatus.setVisibility(View.VISIBLE);
        if (isBullpointing) {
            return;
        }
        isBullpointing = true;
        if (vinceAlreadyPointedFingerCount < 0) {
            mConnectionStatus.setRepeatCount(-1);
            mConnectionStatus.playAnimation();
            return;
        }
        if (vinceAlreadyPointedFingerCount > 0) {
            if (vinceAlreadyPointedFingerCount > 1) {
                mConnectionStatus.setRepeatCount(vinceAlreadyPointedFingerCount - 1);
            }
            vinceAlreadyPointedFingerCount--;
            mConnectionStatus.playAnimation();
        }
    }

    protected void changeStatusDisconnect() {
        NotifyUnSecureNetwork.startServiceWhenRequired(FinjanVPNApplication.getInstance());
        isConnected = false;
        connetingToVpn = false;
        LocalyticsTrackers.Companion.getInstance().addVPNOFFEvent();
        if (isAdded()) {
            updateTrafficUI();
            animateVince(2);
            stopRippleAnimation();
            iv_lock.setVisibility(View.GONE);
            tv_connect_status.setText(R.string.not_connected);
            tv_connect_status.setTextColor(ResourceHelper.getInstance().getColor(R.color.col_content));
            mStartedNewConnection = false;
            mCanSwitchRegion = true;

            ivPlaceHolderFill.setVisibility(View.GONE);
            imVitalSecurity.setVisibility(View.VISIBLE);
            imVitalSecurity.setImageResource(R.drawable.vpn_button_off);
            mUpgradeButton.refreshDrawableState();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mTrafficDescription.setVisibility(View.VISIBLE);
                }
            }, 1000);
            vinceFingerPointing();
            mTrafficCounter.setVisibility(View.VISIBLE);
            if (!TrafficController.getInstance(getContext()).isPaid())
                showProgressBar();
            mVpnLocationDescription.setText(getString(R.string.server_location));
            FileLogger.logToFile(VpnHomeFragment.class, "VPN Disconnected");
        }
    }

    protected void changeStatusConnecting() {
        isConnected = false;
        if (isAdded()) {
            animateVince(0);
            mStartedNewConnection = false;
            mCanSwitchRegion = false;

            tv_connect_status.setText(R.string.connecting);
            tv_connect_status.setTextColor(ResourceHelper.getInstance().getColor(R.color.col_content));
            imVitalSecurity.setImageResource(R.drawable.vpn_button_off);
//            mConnectionStatus.setVisibility(View.GONE);
            mVpnLocationDescription.setText(R.string.txt_vpn_location_description_connecting);
        }
    }

    private void stopRippleAnim() {
        lottieContainer.setVisibility(View.GONE);
    }

    private void changeRippleToConnectedAnim() {
        lottieContainer.setVisibility(View.VISIBLE);
        lottieAnimationView.setAnimation("vpn_connected_animation.json");
        lottieAnimationView.playAnimation();
    }

    private void startRippleAnim() {
        lottieContainer.setVisibility(View.VISIBLE);
        lottieAnimationView.setAnimation("vpn_connecting.json");
        lottieAnimationView.playAnimation();
    }

    protected void changeStatusConnected() {
        NotifyUnSecureNetwork.stopService();
        GoogleTrackers.Companion.setEVPNConnect();
        LocalyticsTrackers.Companion.setEVPNConnect();
        isConnected = true;
        connetingToVpn = false;
        LocalyticsTrackers.Companion.getInstance().addVPNONEvent();
        if (isAdded()) {
            changeRippleToConnectedAnim();
            animateVince(1);
            tv_connect_status.setText(R.string.connected);
            tv_connect_status.setTextColor(ResourceHelper.getInstance().getColor(R.color.col_green));
            if (vinceAlreadyPointedFingerCount > 0) {
                vinceAlreadyPointedFingerCount--;
            }
            iv_lock.setVisibility(View.VISIBLE);
            mStartedNewConnection = false;
            mCanSwitchRegion = false;
            hideButtonWithFade(ivPlaceHolderFill);
            imVitalSecurity.setImageResource(R.drawable.vpn_button_on);
            imVitalSecurity.setVisibility(View.GONE);
            mTrafficDescription.setVisibility(View.VISIBLE);
//            mConnectionStatus.setVisibility(View.GONE);
            mTrafficCounter.setVisibility(View.VISIBLE);
            if (!TrafficController.getInstance(getContext()).isPaid())
                showProgressBar();
            mVpnLocationDescription.setText(getString(R.string.connected_to));
            FileLogger.logToFile(VpnHomeFragment.class, "VPN Connected " + mSelectedServerRegion);
            updateTrafficUI();
        }
    }

    @OnClick(R.id.buttonLogin_or_register)
    public void onButtonLoginOrRegister() {
        SetupAccountActivity.newLoginInstanceForResult(getActivity(), VpnActivity.REQUEST_SIGNUP, SetupAccountActivity.ACTION_SIGN_NAV);
    }

    @OnClick(R.id.layVpnLocation)
    public void onLocationTitleClick() {
//        changeRegion();
        changeRegionNew();
    }

    private void showProgressDialog(Context context, String msg) {
        if (NativeHelper.getInstnace().checkContext(context)) {
            if (progressDialog == null || !progressDialog.isShowing()) {
                progressDialog = new ProgressDialog(context, R.style.VpnProgressDialog);
                progressDialog.setMessage(msg);
            }
//            else {
//                progressDialog.setMessage(msg);
//            }
            if (this.isAdded())
                progressDialog.show();
        }
    }

    private void hideProgress(Context context) {
        if (NativeHelper.getInstnace().checkContext(context)) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        progressDialog = null;
    }

    private void doIfTokenValid() {
        checkVpnServerList(FinjanVPNApplication.getInstance());
        vinceFingerPointing();
        disableButtonsIfRequired();
        if (!TextUtils.isEmpty(VpnActivity.PENDING_ACTION) && VpnActivity.PENDING_ACTION.equals(VpnActivity.ACTION_CONNECT)) {
            onClickVpnButton(false);
            VpnActivity.PENDING_ACTION = "";
        }
    }

    protected void doAutoLoginIfRequested(@NonNull final Context context) {
        VpnActivity.UserLogout = false;
        showProgressDialog(getActivity(), getString(R.string.refreshing_session));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideProgress(getActivity());
            }
        }, 5000);
        AuthenticationHelper.getInstnace().initRefreshToken(new AuthenticationHelper.AutoLoginResult() {
            @Override
            public void autoLoginResults(boolean success) {
                hideProgress(getActivity());
                if (success) {
                    VpnApisHelper.Companion.getInstance().updateLicence(false,true, VpnHomeFragment.this.getContext());
                    onResumeRefresh();
                } else {
                    preLogoutWork(context, true);
                    if (VpnActivity.getInstance() != null) {
                        showLoginScreen();
                    }
                    ToastHelper.showToast(getString(R.string.token_expire_msg), Toast.LENGTH_LONG);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final LoginSuccess event) {
        Logger.logD(TAG, "onEvent LoginSuccess");
        if (isAdded()) {
            disableButtonsIfRequired();
        }
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        AuthenticationHelper.getInstnace().fetchUser(VpnHomeFragment.this.getContext());
        SubscriptionHelper.getInstance().setSubscriptionFetched(false);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final AllSubscFetched event) {
        checkIfAndSetDaysLeft();
        // fireLastDayLocalitics();


    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final SubscriptionUpdated event) {
        Logger.logD(TAG, "onEvent SubscriptionUpdated");

        if (!event.start) {
//islastDayPromo = true;

            shouldShowUpdateBtn = true;
            isSecureUnlimitedAvailable =true;
            isLoderShowing =true;
            onRegionSelet(1,
                    "nearest", "Nearest location",
                    2131231356);

        } /*else {
            isPromoInfoGone = false;
        }*/
        //  updateTrafficUI();


        VpnApisHelper.Companion.getInstance().refreshApis();
        if (isAdded()) {
            if (event.isPromoPurchase) {
                userJustLogin = false;
                if(event.isToShowPromoEndPopUp)
                    PromoHelper.Companion.getInstance().showPopupOnPromoEndStart(getContext(), event.promoId, event.start);
            }
            disableButtonsIfRequired();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isAdded()) {
                        disableButtonsIfRequired();
                    }
                }
            }, 1000);
        }
        if (!TextUtils.isEmpty(event.promoId) && !event.start)
        {
            SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(getActivity(),true);
        }
        SubscriptionHelper.getInstance().setSubscriptionFetched(false);
        PromoHelper.Companion.setPromoChecked(false);
    }

    public void disableButtonsIfRequired() {

        if (!this.isAdded()) {
            return;
        }
        if (TrafficController.getInstance(getActivity()).isRegistered()) {
            loginRegisterButton.setVisibility(View.GONE);
        } else {
            loginRegisterButton.setVisibility(View.GONE);
        }
        if (TrafficController.getInstance(getActivity()).isPaid()) {
            loginRegisterButton.setVisibility(View.GONE);
            if (shouldShowUpdateBtn)
                buttonUpgradeVpn.setVisibility(View.GONE);
        } else {
            if (PromoHelper.Companion.getDiscountedRedeemListData() != null) {
                String txt = PromoHelper.Companion.getDiscountedRedeemListData().getAttributes().getIap_button_text().toUpperCase();
                mUpgradeButton.setText(txt);
                mUpgradeButton.setTextSize(getUpgradeButtonTextsize(txt));
            } else
                mUpgradeButton.setText(getResources().getString(R.string.upgrade_for_unlimited_traffic));

            buttonUpgradeVpn.setVisibility(View.VISIBLE);
        }
    }

    private float getUpgradeButtonTextsize(String updateBtnText) {
        if (!TextUtils.isEmpty(updateBtnText)) {
            int size = updateBtnText.length();
            if (size > 35) {
                return getResources().getDimension(R.dimen._3sdp);
            } else if (size > 15 && size < 25) {
                return getResources().getDimension(R.dimen._5sdp);
            } else if (size <= 15) {
                return getResources().getDimension(R.dimen._6sdp);
            }
        }
        return getResources().getDimension(R.dimen._4sdp);
    }

    private void showRateMeDialog(int descId) {
        FragmentActivity context = getActivity();
        if (context != null && !context.isFinishing() && shouldDisplayRateMeDialog()) {
            long traffic = SharedPreferencesUtilities.getLong(context, AviraOpenVpnService.SHOW_RATE_DIALOG_TRAFFIC);
            if (traffic > 0) {
//                Tracking.trackRateMeDialogShown(traffic);
                String desc = context.getString(descId, Util.humanReadableByteCount(traffic * AviraOpenVpnService.BYTE, false));
//                RateMeDialogUtils.showRateMeDialog(context, R.string.rate_app_title, 0, desc, Tracking.APP_NAME, Tracking.FEATURE_RATE_DIALOG);
                SharedPreferencesUtilities.remove(context, AviraOpenVpnService.SHOW_RATE_DIALOG_FOR_TRAFFIC_LIMIT);
                SharedPreferencesUtilities.remove(context, AviraOpenVpnService.SHOW_RATE_DIALOG_TRAFFIC);
            }
        }
    }

    private boolean shouldDisplayRateMeDialog() {
        Context context = getActivity();
        if (RateMeDialogUtils.isAppRated(context)) {
            return false;
        }
        long lastDisplayedTime = SharedPreferencesUtilities.getLong(context,
                RateMeDialogUtils.LAST_DIALOG_DISPLAY_TIME_PREF, 0);
        return (isReachedTrafficLimit(context) && (lastDisplayedTime == 0
                || SystemClock.elapsedRealtime() - lastDisplayedTime >= RATE_ME_DIALOG_AGO));
    }

    private boolean isReachedTrafficLimit(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, AviraOpenVpnService.SHOW_RATE_DIALOG_FOR_TRAFFIC_LIMIT, false);
    }

    protected String getCurrentLocation() {
        if (mVpnLocationDescriptionValue != null) {
            return mVpnLocationDescriptionValue.getText().toString();
        }
        return "";
    }

    private void updateProgressBar(int progress) {
//        vpnProgressBar.setProgress(progress);
        updateGuage(progress);
    }

    private void updateGuage(int progress) {

        progress = 100 - progress;
//        progress = progress * PlistHelper.getInstance().getDataGaugeMultiplier();
//        progress = progress * PlistHelper.getInstance().getDataGaugeMultiplier();
        int imageRes = R.drawable.guage_full;
        if (progress > 87) {

        } else if (progress > 75) {
            imageRes = R.drawable.guage_7_8;
        } else if (progress > 62) {
            imageRes = R.drawable.guage_6_8;

        } else if (progress > 50) {
            imageRes = R.drawable.guage_5_8;
        } else if (progress > 37) {
            imageRes = R.drawable.guage_4_8;
        } else if (progress > 24) {
            imageRes = R.drawable.guage_3_8;
        } else if (progress > 12) {
            imageRes = R.drawable.guage_2_8;
        } else if (1 < progress && progress < 12) {
            imageRes = R.drawable.guage_1_8;
        } else {
            imageRes = R.drawable.guage_0;
        }
        vpnProgressBar.setImageResource(imageRes);
    }

    private void setAppSwitchViews() {
        if (getView() != null) {
            getView().findViewById(R.id.ll_back).setVisibility(UserPref.getInstance().getIsBrowser() ? View.VISIBLE : View.GONE);
        }
    }

    public void preLogoutWork(final Context context, final boolean autoLogout) {
        UserPref.getInstance().setTempPurchaseToken(null);
        AuthenticationHelper.getInstnace().setFinjanUser(null);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                preLogoutPref(context);
                if (!autoLogout)
                    Toast.makeText(context, getString(R.string.user_logout_successfully), Toast.LENGTH_SHORT).show();
                if (VpnHomeFragment.this.isAdded()) {
                    selectLastServer(context);
                }
                VpnActivity.UserLogout = true;

            }
        }, 100);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                handleMenuClick(VpnHomeFragment.this.getContext(), VPNDrawer.MoIdentifier.menu_login);
            }
        }, 50);
    }

    void onClickLogout() {
        if (!isVpnConnected()) {
            new FinjanDialogBuilder(getContext())
                    .setMessage("Are you sure you want to logout?")
                    .setPositiveButton(getString(R.string.alert_confirm))
                    .setNegativeButton(getString(R.string.alert_cancel))
                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                        @Override
                        public void onPositivClick() {
                            preLogoutWork(getContext(), false);
                        }
                    }).
                    setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                        @Override
                        public void onNegativeClick() {

                        }
                    }).show();

        } else {
            VpnUtil.stopVpnConnection(getContext(), mService);
            changeStatusDisconnect();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onClickLogout();
                }
            }, 500);
//            VpnActivity.showSnackBar(R.string.txt_region_switch_forbidden, getView());
        }
    }

    private void onClickUpgradeFromMoreOption() {
        if (TrafficController.getInstance(getContext()).isRegistered()) {
            startActivity(new Intent(getContext(), UpgradeVpnActivity.class));
        } else {
            VpnActivity.showSnackBar(R.string.txt_region_switch_forbidden, getView());
        }
    }

    protected void setMenuNew() {
        if (getView() == null) {
            return;
        }
        ivMoreOption = (ImageView) getView().findViewById(R.id.iv_more_option);

        getView().findViewById(R.id.ll_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        ivMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                VPNDrawer.Companion.getInstance(getActivity());
//                menuDrawerFragment = MenuDrawerFragment.getInstance();
//                android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                ft.setCustomAnimations(R.anim.enter_from_right, R.anim.enter_to_right, R.anim.enter_from_left, R.anim.enter_to_right);
//                ft.add(R.id.frame_container, menuDrawerFragment, MenuDrawerFragment.TAG).commitAllowingStateLoss();
            }
        });
    }

    protected void handleMenuClick(final Context context, int itemId) {
        switch (itemId) {
            case VPNDrawer.MoIdentifier.menue_watch:
                NetworkManager.getInstance(context).fetchAviraLicence(context, new NetworkResultListener() {
                    @Override
                    public void executeOnSuccess(JSONObject response) {

                    }

                    @Override
                    public void executeOnError(VolleyError error) {

                    }
                });
                Intent intent1 = new Intent(context, WatchContentActivity.class);
                startActivity(intent1);
                break;
            case VPNDrawer.MoIdentifier.menu_change_pwd:
                Intent intent = new Intent(context, NormaWebviewActivity.class);
                intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_PASSWORD_RESET);
                startActivity(intent);
                break;
            case VPNDrawer.MoIdentifier.menu_login:
                SetupAccountActivity.newLoginInstanceForResult(getActivity(), VpnActivity.REQUEST_LOGIN, SetupAccountActivity.ACTION_SIGN_NAV);
                break;
            case VPNDrawer.MoIdentifier.menu_logout:
                onClickLogout();
                break;
            case VPNDrawer.MoIdentifier.menu_upgrade:
                onClickUpgradeFromMoreOption();
                break;
            case VPNDrawer.MoIdentifier.menu_about:
                ScreenNavigation.getInstance().callHomeActivity
                        (context, DrawerConstants.DKEY_ABOUT);
                break;
            case VPNDrawer.MoIdentifier.menu_help:
                ScreenNavigation.getInstance().callHomeActivity
                        (context, DrawerConstants.DKEY_HELP);
                break;
            case VPNDrawer.MoIdentifier.menu_rate:
                Intent i = new Intent(Intent.ACTION_VIEW);
                String url = PlistHelper.getInstance().getAppUrl();
                if (url != null && !url.isEmpty()) {
                    i.setData(Uri.parse(url));
                    startActivityForResult(i, 1234);
                }
                break;
            case VPNDrawer.MoIdentifier.menu_settings:
                ScreenNavigation.getInstance().callHomeActivity
                        (context, DrawerConstants.DKEY_Settings);
                break;
            case VPNDrawer.MoIdentifier.menu_share:
                onClickVpnAppShare();
//                NativeDialogs.getShareDesktopAppDialog(context, PlistHelper.getInstance().getVpnScreenPopUpText());
                break;
            case VPNDrawer.MoIdentifier.menu_my_ip:
                onClickMyId();
                break;
            case VPNDrawer.MoIdentifier.menu_secure_browser:
                switchApp();
                break;
            case VPNDrawer.MoIdentifier.menu_register:
                SetupAccountActivity.newLoginInstanceForResult(getActivity(),
                        VpnActivity.REQUEST_SIGNUP, SetupAccountActivity.ACTION_SIGN_NAV);
                break;
            case VPNDrawer.MoIdentifier.menu_vs_desktop:
//                onClickVpnDesktopShare();
                NativeDialogs.getShareDesktopAppDialog(getActivity(), PlistHelper.getInstance().getVSScreenPopUpText());
                break;

            case VPNDrawer.MoIdentifier.menu_auto_connect:
                onClickAutoConnect(getContext());
                break;
            case VPNDrawer.MoIdentifier.menu_trustednetworks:
                onClickAutoConnect(getContext());
                break;
            case VPNDrawer.MoIdentifier.menu_submit_debug_report:
                Logger.sendEmailOfLog(getContext());
                break;
            case VPNDrawer.MoIdentifier.menu_make_you_token_expire:
                FinjanVpnPrefs.setAuthToken(getContext(), "3buwcxozx7ih84exmawyziephbitbeoabuyq1qlt");
                FinjanVpnPrefs.setRefereshToken(getContext(), "3buwcxozx7ih84exmawyziephbitbeoabuyq1qlt");
                ToastHelper.showToast("Forcefully made your token is expired, and refreshing in background", Toast.LENGTH_SHORT);
                break;
            case VPNDrawer.MoIdentifier.menu_make_you_time_expire:
                UserPref.getInstance().setTokenExpiresIn(0);
                ToastHelper.showToast("Forcefully made your token expire time 0, refreshing token now", Toast.LENGTH_SHORT);
                break;

            case VPNDrawer.MoIdentifier.menu_redeem_offer:
                onClickRedeemOffer();

                // ToastHelper.showToast("Working on Redeem offer screen",Toast.LENGTH_SHORT);
                break;
            case VPNDrawer.MoIdentifier.cancel_all_promos:
                SubscriptionHelper.getInstance().cancelAllPromos(getContext(),true);
                // ToastHelper.showToast("Working on Redeem offer screen",Toast.LENGTH_SHORT);
                break;
           /* case VPNDrawer.MoIdentifier.testing_Location_promo:
               PlistHelper.VpnMenu  vpnMenu =  PlistHelper.getInstance().getVpnMenuMap().get("testing_Location_promo");
                if (vpnMenu.getName().contains("softcoded")) {
                   String name =  vpnMenu.getName().replace("softcoded","Hardcoded");
                    vpnMenu.setName(name);
//                    AppConfig.Companion.setCoding("Hardcoded");
                    AppConfig.Companion.setHardCodedPromoResponse(true);
                    PromoHelper.Companion.getInstance().getAllRedeemOffers(getActivity());
                } else {
                    String name =  vpnMenu.getName().replace("Hardcoded","softcoded");
                    vpnMenu.setName(name);
//                    AppConfig.Companion.setCoding("softcoded");
                    AppConfig.Companion.setHardCodedPromoResponse(false);
//                    AppConfig.Companion.isHardCodedPromoResponse()
                    PromoHelper.Companion.getInstance().getAllRedeemOffers(getActivity());
                }
                break;*/
            case 100:
//                if(!TrafficController.getInstance(context).isRegistered()){
//                    getActivity().onBackPressed();
//                }
                break;
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final AllPromoFetched event) {

        hideLoader();
    }

    private void onClickRedeemOffer() {
        Intent intent = new Intent(getContext(), RedeemActivity.class);
        intent.putExtra("redeemcode", "CAOK65517");
//        ActivityOptions options =
//                ActivityOptions.makeCustomAnimation(getActivity(), R.anim.enter_from_right,R.anim.enter_to_left);
        startActivity(intent/*, options.toBundle()*/);
    }

    private void onClickMyId() {
        mVpnLocationDescriptionValue.setText(getString(R.string.fetching_location));
        String ipAddress = NativeHelper.getInstnace().getIpAddressOfDevice();
        NetworkManager.getInstance(getContext()).fetchLocation(getContext(), ipAddress, new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                selectLastServer(VpnHomeFragment.this.getContext());
                String location = "Your IP Address locate at '";
                location = location + " " + JsonParser.getInstance().getIpAdressLocation(response) + "'";
                location = location + "\n \nYour VPN connected to '" + connectedRegion + "'";


                FinjanDialogBuilder builder = new FinjanDialogBuilder(getContext());
                builder.setMessage(location)
                        .setPositiveButton("Ok")
                        .setDialogClickListener(new FinjanDialogBuilder.DialogClickListener() {
                            @Override
                            public void onNegativeClick() {
                            }

                            @Override
                            public void onPositivClick() {
                            }

                            @Override
                            public void onContentClick(String content) {

                            }
                        });
                if (VpnHomeFragment.this.isAdded())
                    builder.show();
            }

            @Override
            public void executeOnError(VolleyError error) {
                selectLastServer(VpnHomeFragment.this.getContext());
            }
        });
    }

    private void switchApp() {

        final boolean isCurrentBrowser = UserPref.getInstance().getIsBrowser();
        FinjanDialogBuilder builder = new FinjanDialogBuilder(getContext());
        builder.setMessage(
                UserPref.getInstance().getIsBrowser() ? PlistHelper.getInstance().getChooseVpnMsg() :
                        PlistHelper.getInstance().getChooseBrowerMsg())
                .setNegativeButton(getString(R.string.alert_cancel))
                .setPositiveButton(getString(R.string.alert_confirm))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        UserPref.getInstance().setIsTipShown(UserPref.getInstance().getIsBrowser());
                        UserPref.getInstance().setIsBroswer(!UserPref.getInstance().getIsBrowser());
//                       switchDefaultApp(!checked);
                        setAppSwitchViews();
                        LocalyticsTrackers.Companion.setBorrowerUser();
                        if (!isCurrentBrowser) {
                            getActivity().onBackPressed();
                        }

                    }
                }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
            @Override
            public void onNegativeClick() {

            }
        });
        if (VpnHomeFragment.this.isAdded())
            builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case VPNDrawer.Drawer_Result:
                handleMenuClick(getContext(), resultCode);
                isDrawerResponse = true;
                break;
            case RegionAcitivity.Region_Result:
                if (data != null) {
                    isDrawerResponse = true;
                    onRegionSelet(data.getLongExtra("id", 0),
                            data.getStringExtra("region"), data.getStringExtra("title"),
                            data.getIntExtra("flag", 0));
                }
                break;

        }
    }


    private void onNetworkFetch(JSONObject response) {
        updatingServerList = false;
        JsonConfigParser.parseJsonConfig(FinjanVPNApplication.getInstance(), response);

        if (isAdded() && getRegionDialogFragment() != null) {
            getRegionDialogFragment().initRegions(FinjanVPNApplication.getInstance());
        }
        selectLastServer(FinjanVPNApplication.getInstance());
    }

    private void onFetchingTraffic(JSONObject response) {
        if (TextUtils.isEmpty(UserPref.getInstance().getTempPurchaseToken())) {
            JsonConfigParser.parseJsonTrafficConsumption(FinjanVPNApplication.getInstance(), response);
        }
    }

    private class FetchServerList implements NetworkResultListener {

        @Override
        public void executeOnSuccess(JSONObject response) {
            onNetworkFetch(response);
            AppConfig.LocalDataHelper.Companion.updateRegions(response);
        }

        @Override
        public void executeOnError(VolleyError error) {
            updatingServerList = false;
            if (AppConfig.Companion.getSupportOfLocalJsons() && Util.isNetworkConnected(getContext()) && (error instanceof NoConnectionError) || error instanceof TimeoutError) {
                onNetworkFetch(AppConfig.LocalDataHelper.Companion.getLocalRegions());
                return;
            }
            if (VpnHomeFragment.this.isAdded()) {
                if (error instanceof NetworkError || error instanceof NoConnectionError) {
                    mVpnLocationDescriptionValue.setText(getString(R.string.regions_update_failed_network_error));

                } else if (error instanceof TimeoutError) {
                    mVpnLocationDescriptionValue.setText(getString(R.string.regions_update_failed));
                } else {
                    mVpnLocationDescriptionValue.setText(getString(R.string.regions_update_failed));
                }
                if (!TextUtils.isEmpty(WebUtility.errorMessageForUser(error))) {
                    ToastHelper.showToast(WebUtility.errorMessageForUser(error), Toast.LENGTH_LONG);
                }
            }
            error.printStackTrace();
        }
    }

    private class FetchTrafficConsumption implements NetworkResultListener {

        @Override
        public void executeOnSuccess(final JSONObject response) {
            onFetchingTraffic(response);
            AppConfig.LocalDataHelper.Companion.updateTrfficJson(response);
            updateTrafficUI();
        }

        @Override
        public void executeOnError(VolleyError error) {
            error.printStackTrace();
            if (AppConfig.Companion.getSupportOfLocalJsons() && Util.isNetworkConnected(FinjanVPNApplication.getInstance()) && (error instanceof NoConnectionError) || error instanceof TimeoutError) {
                onFetchingTraffic(AppConfig.LocalDataHelper.Companion.getOnGBTrafficUsage());
                if (!UserPref.getInstance().getLastUpdateTrafficDate().equals(DateHelper.getInstnace().getCurrentDateMonth())) {
                    TrafficController.getInstance(getContext()).clearConsumedTraffic();
                }
                updateTrafficUI();
                return;
            }
            if (VpnHomeFragment.this.isAdded() && !TextUtils.isEmpty(WebUtility.errorMessageForUser(error))) {
                ToastHelper.showToast(WebUtility.errorMessageForUser(error), Toast.LENGTH_LONG);
            }
        }
    }
}
