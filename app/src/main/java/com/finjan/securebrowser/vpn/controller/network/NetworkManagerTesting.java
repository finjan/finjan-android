package com.finjan.securebrowser.vpn.controller.network;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.avira.common.backend.WebUtility;
import com.finjan.securebrowser.custom_exceptions.CustomExceptionConfig;
import com.finjan.securebrowser.custom_exceptions.UserFetchApiException;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anurag on 23/11/17.
 */

public class NetworkManagerTesting {

    public static final String TAG = NetworkManager.class.getSimpleName();

    private static final String AUTHORIZATION = "Authorization";
    private static NetworkManagerTesting mInstance;
    private final String mUser;
    private RequestQueue mRequestQueue;

    private NetworkManagerTesting(@NonNull Context context) {
        mUser ="http://staging.fintechlabs.in/api/v1/borrower/stepOne";
        mRequestQueue = getRequestQueue(context);
    }

    public static synchronized NetworkManagerTesting getInstance(@NonNull Context context) {
        if (mInstance == null) {
            mInstance = new NetworkManagerTesting(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue(@NonNull Context context) {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    public void fetchUser(@NonNull Context context, @NonNull NetworkResultListener networkResultListener) {
        try {
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("emailAddress","tester_a1@td.com");
            jsonObject.put("password","123456");
            jsonObject.put("confirmPassword","123456");
            fetchJsonPost(context, networkResultListener, mUser,jsonObject, UserFetchApiException.class.getSimpleName());
        }catch (Exception e){}
    }

    private void fetchJson(@NonNull Context context, @NonNull final NetworkResultListener networkResultListener, final @NonNull String url,
                           @NonNull final String correspondingExceptionClass) {
        Logger.logD(TAG, "fetchJson url " + url);
        final JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.logD(TAG, "onResponse(JSONObject response) " + response.toString());
                        // the response is already constructed as a JSONObject!
//                        new CreateDeviceApiException("Testing Url","","");
                        networkResultListener.executeOnSuccess(response);

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json=null;
                        if(error==null){
                            json="Volly error Unknown response";
                        }else {
                            json= WebUtility.getMessage(error);
                        }
                        CustomExceptionConfig.getInstance().logException(url,"",json,correspondingExceptionClass);
                        networkResultListener.executeOnError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
//                return super.getHeaders();
                return getHeaderss();
            }
        };
        getRequestQueue(context).add(jsonRequest);
    }

    private void fetchJsonPost(@NonNull Context context, @NonNull final NetworkResultListener networkResultListener,
                               @NonNull final String url,final  @NonNull JSONObject jsonObject,final @NonNull String correspondingExceptionClass) {
        Logger.logE(TAG, "fetchJson url " + url);
        Logger.logE(TAG, "Json url " + jsonObject.toString());
        int method = Request.Method.POST;
        if (url.contains("devices/")) {
            method = Request.Method.PATCH;
        } else if (url.contains("/license")) {
            method = Request.Method.PUT;
        }else if(url.contains("/subscription")){
            if(url.endsWith("/subscription")){
                method = Request.Method.POST;
            }else {
                method = Request.Method.PUT;
            }
        }

//        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        Logger.logE(TAG, "onResponse(JSONObject response) " + response);
//
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Logger.logE(TAG, "onResponse(error) " + error.getMessage());
//                error.printStackTrace();
//            }
//        }) {
//
//            @Override
//            public String getBodyContentType() {
//                return "application/x-www-form-urlencoded; charset=UTF-8";
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//
//                params.put("emailAddress", "tester_a1@td.com");
//                params.put("password", "123456");
//                params.put("confirmPassword", "123456");
//                return params;
//            }
//
//        };


        final JsonObjectRequest jsonRequest = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Logger.logD(TAG, "onResponse(JSONObject response) " + response.toString());
                // the response is already constructed as a JSONObject!
                networkResultListener.executeOnSuccess(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String json=null;
                if(error==null){
                    json="Volly error Unknown response";
                }else {
                    json= WebUtility.getMessage(error);
                }
                networkResultListener.executeOnError(error);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        getRequestQueue(context).add(jsonRequest);
    }

    private HashMap<String, String> getHeaderss() {
        HashMap<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        // read user authenticate token and add to a request
        String authToken = "Bearer " + FinjanVpnPrefs.getAuthToken(FinjanVPNApplication.getInstance());
//        String authToken = "Bearer "+ UserPref.getInstance().getCurrent_Access_Token();
        Logger.logD(TAG, "AUTHORIZATION " + authToken);
        if (!TextUtils.isEmpty(authToken)) {
            params.put(AUTHORIZATION, authToken);
        }
        return params;
    }

}
