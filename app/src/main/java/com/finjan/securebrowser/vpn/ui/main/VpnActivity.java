/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avira.common.ui.DrawerBuilder;
import com.avira.common.utils.WhatsNewDialog;
import com.finjan.securebrowser.Appirater;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.HomeActivity;
import com.finjan.securebrowser.activity.ParentActivity;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.service.NotifyUnSecureNetwork;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.NotificationHandler;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.auto_connect.AutoConnectHelper;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.eventbus.ActionNotificationEvent;
import com.finjan.securebrowser.vpn.eventbus.AutoLoginEvent;
import com.finjan.securebrowser.vpn.eventbus.LicenceFetched;
import com.finjan.securebrowser.vpn.eventbus.LoginSuccess;
import com.finjan.securebrowser.vpn.eventbus.SubscriptionUpdated;
import com.finjan.securebrowser.vpn.licensing.LicensesRefreshedEvent;
import com.finjan.securebrowser.vpn.service.AviraOpenVpnService;
import com.finjan.securebrowser.vpn.subscriptions.SubscriptionHelper;
import com.finjan.securebrowser.vpn.ui.authentication.SetupAccountActivity;
import com.finjan.securebrowser.vpn.ui.iab.LicenseUtil;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.vpn.util.VpnUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
//import com.localytics.android.Localytics;

import java.util.ArrayList;

import de.blinkt.openvpn.core.OpenVPNService;
import de.greenrobot.event.EventBus;


public class VpnActivity extends ParentActivity implements DrawerBuilder.DrawerClickListener, View.OnClickListener {

    public static final String TAG = VpnActivity.class.getSimpleName();
    public static final String ACTION_DISCONNECTED = "com.avira.vpn.ui.main.ACTION_DISCONNECTED";
    public static final String ACTION_CONNECT = "com.avira.vpn.ui.main.ACTION_CONNECT";
    public static final int WIFI_PERMISSION_NOTIFICATION_COUNT = 5;
    public static final int WIFI_PERMISSION_NOTIFICATION_DELAY_HOURS = 1;
    public static final int REQUEST_LOGIN = 115;
    public static final int REQUEST_Login_For_Region = 117;
    public static final int REQUEST_Login_For_Upgrade = 118;
    public static final int REQUEST_SIGNUP = 116;
    public static final int REQUEST_NAV = 119;

    public static final int RESULT_SIGNUP = 1;
    public static final int RESULT_LOGIN = 2;

    public static boolean I_AM_ALIVE = false;
    public static boolean UserLogout = false;
    public static boolean IsUserLoggin = false;
    private static VpnActivity instance;
    protected boolean mIsDisconnectPressed = false;
    protected boolean userClickedDisFromNot = false;
    private DrawerLayout mDrawerLayout;
    private DrawerBuilder mDrawerLayoutBuilder;
    private View mLicensingInfoLayout;
    private FrameLayout mMenuLayout;
    private Button mUpgradeButton;
//    private RemoteConfig mRemoteConfig;
    private ImageView ivMoreOption;
    public static String PENDING_ACTION = "";

    public static void showSnackBar(int msg, View view) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView tv = (TextView) snackbarView.findViewById(R.id.snackbar_text);
        tv.setTextColor(ResourceHelper.getInstance().getColor(R.color.accent_color_0));
        snackbar.show();
    }

    public static void showSnackBar(String msg, View view) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView tv = (TextView) snackbarView.findViewById(R.id.snackbar_text);
        tv.setTextColor(ResourceHelper.getInstance().getColor(R.color.accent_color_0));
        snackbar.show();
    }

    public static VpnActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.logD(TAG, "onCreate");

        super.onCreate(savedInstanceState);

//        if (TrafficController.getInstance(this.getApplicationContext()).isPaid()) {
//            setTheme(R.style.Pro_AppTheme_NoActionBar);
//        }
//        String token = FirebaseInstanceId.getInstance().getToken();
//        Localytics.setPushRegistrationId(token);
//        Localytics.registerPush();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                Logger.logE("token", token);
//                Localytics.setPushRegistrationId(token);
//                Localytics.registerPush();
//                Logger.logE("Localytics version using", " " + Localytics.getLibraryVersion());

            }
        });


        setContentView(R.layout.activity_vpn);
        ScreenNavigation.pushActivApp("SCREEN_VPN");
        PlistHelper.setUpdatePlist();
        setReferences();
        NativeHelper.getInstnace().changeStatusBarColor(R.color.col_status, this);
        setDefaults();
        VpnActivity.instance = this;
        if(getActionBar()!=null){
            getActionBar().setTitle(getString(R.string.app_name));
        }

        initFragment();

        new Thread(new Runnable() {
            @Override
            public void run() {
                FinjanVpnPrefs.setFTUCompleted(VpnActivity.this);
            }
        }).start();


        String action = getIntent().getAction();
        if(!TextUtils.isEmpty(action)){
            PENDING_ACTION = action;
        }
        if (ACTION_DISCONNECTED.equals(action)) {

            showSnackBar(R.string.traffic_limit_bound_message, mDrawerLayout);
        } else if (AviraOpenVpnService.DISCONNECT_VPN.equals(action)) {
            mIsDisconnectPressed = true;
            userClickedDisFromNot = true;
        }

//        mRemoteConfig = new RemoteConfig();

        WhatsNewDialog.showWhatsNewDialog(this, R.mipmap.app_icon,
                R.string.whats_new_title, R.string.whats_new_title, R.array.whats_new_content);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Appirater.appLaunched(VpnActivity.this);
            }
        },500);
        setScreenNavigation(getIntent());
    }
    private void setScreenNavigation(Intent intent){
        if(intent.hasExtra("ll_deep_link_url")){
            NotificationHandler.Companion.getInstance().init(this,intent);
            NotificationHandler.Companion.getInstance().processIntent();
        }
    }

    private void setReferences() {initDrawerItems(); }

    private VpnHomeFragment vpnHomeFragment;
    private void initFragment() {
        Logger.logD(TAG, "initFragment");
        vpnHomeFragment= VpnHomeFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.frame_container, vpnHomeFragment, VpnHomeFragment.TAG).commitAllowingStateLoss();
        SubscriptionHelper.getInstance().getDeviceSubscriptions(this, new SubscriptionHelper.SubscriptionAvailable() {
            @Override
            public void subscriptionAvailable(ArrayList<String> skuList) {
                if(skuList.size()==0){
                    UserPref.getInstance().setTempPurchaseToken(null);
                    EventBus.getDefault().post(new SubscriptionUpdated());
                }else {
                    int type = 2;
                    if(skuList.contains(LicenseUtil.getSkuAllDevicesMonthly())){
                        type =4;
                    }else if(skuList.contains(LicenseUtil.getSkuAllDevicesYearly())){
                        type = 5;
                    }else if(skuList.contains(LicenseUtil.getSkuThisDevicesMonthly())){
                        type =2;
                    }else if(skuList.contains(LicenseUtil.getSkuThisDevicesYearly())){
                        type = 3;
                    }
                    LocalyticsTrackers.Companion.setUserPaidType(type);
                }
            }
        });
    }
    public VpnHomeFragment getVpnHomeFragment(){
        return vpnHomeFragment;
    }

    private void initDrawerItems() {
        mMenuLayout = ((FrameLayout) findViewById(R.id.drawer_content));
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    private void setDefaults() {
//        TextView tvTitle = (TextView)findViewById(R.id.tv_title);
//        tvTitle.setText("VPN");
    }

    private void refreshDrawerLayout() {
        Logger.logD(TAG, "initDrawerLayout");
//
//        mDrawerLayoutBuilder = new DrawerBuilder(this)
//                .setHeaderBackground(R.drawable.)
//                .setIcon(R.drawable.drawer_logo);
//
//        TrafficController trafficController = TrafficController.getInstance(this.getApplicationContext());
//        int aboutIcon = trafficController.isPaid() ? R.drawable.ic_about_dark : R.drawable.ic_about;
//
//
//        mDrawerLayoutBuilder.addMenuItem(R.drawable.ic_change_location, R.string.drawer_location);
//        mDrawerLayoutBuilder.addMenuItem(R.drawable.ic_drawer_settings, R.string.Settings);
//        mDrawerLayoutBuilder.addMenuItem(aboutIcon, R.string.About);
//
//
//        UserProfile userProfile = UserProfile.load();
//        if (userProfile == null || !trafficController.isRegistered()) {
//            mDrawerLayoutBuilder.addMenuItem(R.drawable.ic_login, R.string.drawer_login);
//        }
//
//        if (userProfile != null && !trafficController.isRegistered()) {
//            Toast.makeText(this, R.string.txt_toast_relogin, Toast.LENGTH_LONG).show();
//        }
//
//        if (!trafficController.isPaid()) {
//            makeBuyVisible();
//        }
//
//        updateProfileInfo();
//
//        mDrawerLayoutBuilder.build(mMenuLayout);
//
//        hideShare(mDrawerLayout);
//
//        if (trafficController.isPaid()) {
//            initProTheme();
//        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if(intent!=null && !TextUtils.isEmpty(intent.getAction())){
            String action = intent.getAction();
            switch (action){
                case OpenVPNService.DISCONNECT_VPN:
                    onDisconnectButtonClick(true);
                    finish();
                    break;
                case AppConstants.SERVICE_VPN_Open:
                    break;
                case AppConstants.SERVICE_VPN_Connect:
                    if(!VpnUtil.isVpnActive(getApplicationContext())){
                        onDisconnectButtonClick(false);
                        finish();
                    }
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        Logger.logD(TAG, "onResume");
        super.onResume();
        NativeHelper.getInstnace().currentAcitvity=this;
        I_AM_ALIVE = true;
        userNeedToLoginCalled=false;
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        LocalyticsTrackers.Companion.getInstance().removeFromBackground();
        ScreenNavigation.getInstance().saveLastScreen(this);
        refreshMenu();
//        if(GlobalVariables.user_registered){
//            GlobalVariables.user_registered=false;
//            NativeDialogs.getShareDesktopAppDialog(NativeHelper.getInstnace().currentAcitvity,
//                    PlistHelper.getInstance().getRegistrationPopUpText());
//        }
        if(!VpnActivity.IsUserLoggin)
        userNeedToLogin();
    }

    public void refreshMenu() {
        if (mMenuLayout != null) {
            mMenuLayout.removeAllViews();
        }
    }

    public void onEventMainThread(final ActionNotificationEvent event) {
        Logger.logD(TAG, "onEvent: " + event.getMessage());
        showSnackBar(event.getMessage(), mDrawerLayout);
    }

    public void onDisconnectButtonClick(boolean isPressedByUser) {
        AutoConnectHelper.Companion.setAutoConnection(false);
        Fragment mainActivityFragment = getSupportFragmentManager().findFragmentByTag(VpnHomeFragment.TAG);
        if (mainActivityFragment != null && mainActivityFragment.isAdded()) {
            ((VpnHomeFragment) mainActivityFragment).onClickVpnButton(isPressedByUser);
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(final AutoLoginEvent event) {
        Logger.logD(TAG, "onEvent AutoLoginEvent");
        final Fragment mainActivityFragment = getSupportFragmentManager().findFragmentByTag(VpnHomeFragment.TAG);
        if (mainActivityFragment != null && mainActivityFragment.isAdded()) {
            ((VpnHomeFragment) mainActivityFragment).doAutoLoginIfRequested(getApplicationContext());
        }
    }


    @SuppressWarnings("unused")
    public void onEventMainThread(final LoginSuccess event) {
        Logger.logD(TAG, "onEvent LoginSuccess");
        refreshMenu();
//        Fragment mainActivityFragment = getSupportFragmentManager().findFragmentByTag(VpnHomeFragment.TAG);
//        if (mainActivityFragment != null && mainActivityFragment.isAdded()) {
//            ((VpnHomeFragment) mainActivityFragment).updateLicence();
//            ((VpnHomeFragment) mainActivityFragment).disableButtonsIfRequired();
//        }
    }

    /**
     * Event whenever Licence is fetched
     */
    @SuppressWarnings("unused")
    public void onEventMainThread(LicenceFetched event) {
        if(getIntent()!=null && getIntent().getAction()!=null &&
                getIntent().getAction().equals(AppConstants.SERVICE_VPN_Connect)
                && !VpnUtil.isVpnActive(getApplicationContext())){
            onDisconnectButtonClick(false);
            getIntent().setAction(null);
        }
    }

    /**
     * Event whenever there's a change in the licenses
     */
    @SuppressWarnings("unused")
    public void onEventMainThread(LicensesRefreshedEvent event) {
        Logger.logD(TAG, "LicensesRefreshedEvent pro? " + TrafficController.getInstance(this).isPaid());
        refreshMenu();
        userNeedToLogin();
    }


    @Override
    protected void onStop() {
        Logger.logD(TAG, "onStop");
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        NotifyUnSecureNetwork.stopService();
//        if(GlobalVariables.getInstnace().isLaunchVpnAcivityCalled ){return;}
        LocalyticsTrackers.Companion.getInstance().addToBackgroundChannels();
        I_AM_ALIVE = false;
    }

    @Override
    public void onDrawerClickListener(View view, int i) {
        Logger.logD(TAG, "onDrawerClickListener");
        // always close drawer when an action takes place inside it
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onClick(View view) {
        Logger.logD(TAG, "onClick");
    }

    @Override
    public void onBackPressed() {
        if(!UserPref.getInstance().getIsBrowser()){

            exitAlert(this);

//            if(vpnHomeFragment.isAdded() && vpnHomeFragment.menuDrawerFragment!=null && vpnHomeFragment.menuDrawerFragment.isResumed()){
//                vpnHomeFragment.menuDrawerFragment.onBackClick();
//            }else {
//            }
        }else {
            if(GlobalVariables.getInstnace().appSwitchSettingChanged ||
                    !ScreenNavigation.getInstance().getIsHomeScreenInitiated()){
                GlobalVariables.getInstnace().appSwitchSettingChanged=false;
                ScreenNavigation.getInstance().callHomeActivity(VpnActivity.this,"");
                finish();
            }else {
                super.onBackPressed();
            }
        }
    }
    public boolean loginToContinueCalled=false;

    protected void loginToContinue(){

        if(TrafficController.getInstance(this).isRegistered()){return;}

        userNeedToLogin();

//        if (NativeHelper.getInstnace().checkActivity(this)) {
//            new FinjanDialogBuilder(this).setMessage(getString(R.string.login_to_continue_message))
//                    .setPositiveButton(getString(R.string.alert_ok))
//                    .setNegativeButton(getString(R.string.alert_cancel))
//                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
//                        @Override
//                        public void onPositivClick() {
//                            loginToContinueCalled=false;
//                            showLoginScreen();
//
//                        }
//                    }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
//                @Override
//                public void onNegativeClick() {
//                    loginToContinueCalled=false;
////                    if(UserPref.getInstance().getIsBrowser()){
//////                        NativeHelper.getInstnace().showToast(getString(R.string.exiting),Toast.LENGTH_SHORT);
////                        finish();
////                    }else {
//                        exitAlert();
////                    }
//                }
//            })
//                    .show();
//            loginToContinueCalled=true;
//        }
    }

    public static void exitAlert(final Activity activity) {

        if (NativeHelper.getInstnace().checkActivity(activity)) {
            String msg = String.format(ResourceHelper.getInstance().getString(R.string.exit_message_vpn), ResourceHelper.getInstance().getString(R.string.app_name));
            new FinjanDialogBuilder(activity).setMessage(msg)
                    .setPositiveButton(ResourceHelper.getInstance().getString(R.string.alert_ok))
                    .setNegativeButton(ResourceHelper.getInstance().getString(R.string.alert_cancel))
                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                        @Override
                        public void onPositivClick() {
                            if(UserPref.getInstance().getIsBrowser()){
                                if(NativeHelper.getInstnace().checkActivity(activity))
                                activity.finish();
                            }else {
                                if(NativeHelper.getInstnace().checkActivity(activity)){
                                    try {
                                        activity.finishAffinity();
                                            if(HomeActivity.getInstance()!=null){
                                            HomeActivity.getInstance().finish();
                                        }
                                    } catch (Exception e) {
                                        Process.killProcess(Process.myPid());
                                    }
                                }

                            }
                        }
                    }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                @Override
                public void onNegativeClick() {
//                    if(!TrafficController.getInstance(VpnActivity.this).isRegistered()){
//                       loginToContinue();
//                    }
                }
            })
                    .show();
        }
    }
    private boolean userNeedToLoginCalled=false;
    private void userNeedToLogin(){
        if(!I_AM_ALIVE ){
            return;
        }
        if(!TrafficController.getInstance(VpnActivity.this).isRegistered()) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (userNeedToLoginCalled) {
                        return;
                    }
                    userNeedToLoginCalled = true;
                    Toast.makeText(FinjanVPNApplication.getInstance(), getString(R.string.login_to_continue_message), Toast.LENGTH_SHORT).show();
                    showLoginScreen();

                }
            },100);
        }


    }
    private void showLoginScreen(){
        SetupAccountActivity.newLoginInstanceForResult(VpnActivity.this,
                VpnActivity.REQUEST_LOGIN, SetupAccountActivity.ACTION_SIGN_NAV);
    }
//    private void licenceRefershMsg(){
//        VpnHomeFragment mainActivityFragment = (VpnHomeFragment) getSupportFragmentManager().findFragmentByTag(VpnHomeFragment.TAG);
//        if(mainActivityFragment!=null){
//            mainActivityFragment.onClickLogout();
//        }
//        new FinjanDialogBuilder(this).setMessage(getString(R.string.msg_connecting_prob))
//                .setPositiveButton(getString(R.string.alert_ok)).show();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_SIGNUP:
                if(resultCode==RESULT_OK){
                    GlobalVariables.user_registered=true;
                }
                break;

//            case REQUEST_NAV:
//                if(resultCode==RESULT_OK){
//                    if(NetworkManager.getInstance(VpnActivity.this).failedRequest!=null){
//                        NetworkManager.getInstance(VpnActivity.this).failedRequest.hitReqest(VpnActivity.this);
//                    }
//                }
//                break;

            case REQUEST_LOGIN:
                if(resultCode!=RESULT_OK || !TrafficController.getInstance(this).isRegistered()){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(GlobalVariables.getInstnace().lastNetworkConnection != null){
                                NetworkManager.getInstance(FinjanVPNApplication.getInstance())
                                        .hitCashedRequest(GlobalVariables.getInstnace().lastNetworkConnection);
                            }
//                            loginToContinue();
                        }
                    },100);
                }
                break;
            case REQUEST_Login_For_Region:
                if(resultCode==RESULT_OK){
                    VpnHomeFragment mainActivityFragment = (VpnHomeFragment) getSupportFragmentManager().findFragmentByTag(VpnHomeFragment.TAG);
                    mainActivityFragment.onClickUpgrade(false,false);
                }
                break;
            case REQUEST_Login_For_Upgrade:
                if(resultCode==RESULT_OK){
                    if(TrafficController.getInstance(this).isPaid()) {
                        Toast.makeText(this, getString(R.string.you_are_already_a_paid_user), Toast.LENGTH_SHORT).show();
                    }else if(TrafficController.getInstance(this).isRegistered()){
                        startActivity(VpnHomeFragment.upgradeIntent(this,false,true,true));
                    }else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
//                                loginToContinue();
                            }
                        },100);
                    }
                }
            case VPNDrawer.Drawer_Result:
                VpnActivity.this.vpnHomeFragment.onActivityResult(requestCode, resultCode, data);
                break;
            case RegionAcitivity.Region_Result:
                VpnActivity.this.vpnHomeFragment.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
