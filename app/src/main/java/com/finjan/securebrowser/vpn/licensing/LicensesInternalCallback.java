package com.finjan.securebrowser.vpn.licensing;

import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.avira.common.backend.WebUtility;
import com.finjan.securebrowser.vpn.licensing.models.restful.License;
import com.finjan.securebrowser.vpn.licensing.models.restful.LicenseArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * callback to a licenses query to the backend that does some processing on the result (searches
 * through the response for the best license for each product)
 *
 * @author ovidiu.buleandra
 * @since 20.11.2015
 */
/*package */ class LicensesInternalCallback implements Response.Listener<LicenseArray>, ErrorListener {

    private LicensesCallback mCallback;

    public LicensesInternalCallback(LicensesCallback callback) {
        if (callback == null)
            throw new IllegalArgumentException("passed callback shouldn't be null");
        mCallback = callback;
    }

    @Override
    public void onResponse(LicenseArray response) {
        if (response != null) {
            List<License> licenses = response.getLicenses();
            // process licenses so that we have only one licenses per product
            // the order of importance is paid > eval > free (if 2 or more of same type they will be merged and only
            // the oldest expire date will be considered)
            mCallback.onLicenseQuerySuccess(processLicenses(licenses));
        } else {
            mCallback.onLicenseQueryError(-1, "response is null");
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mCallback.onLicenseQueryError(WebUtility.getHTTPErrorCode(error), WebUtility.getMessage(error));
    }

    private List<License> processLicenses(List<License> licenses) {
        HashMap<String, License> resultMap = new HashMap<>();

        for (License license : licenses) {
            String product = license.getProductAcronym();
            if (resultMap.containsKey(product)) {
                // if the current license is less or equally valuable then ignore it
                if (license.compareTo(resultMap.get(product)) <= 0) {
                    continue;
                }
            }

            // make sure the stored license doesn't contain the awfully useless long key inside attributes property
            license.clearKey();
            // make sure the date is maximized for that day for all validation and comparison purposes
            // eg server returns 2016-01-01T00:00:00Z and we process that in 2016-01-01T23:59:59Z
            license.adjustExpirationDate();
            resultMap.put(product, license);
        }

        return new ArrayList<>(resultMap.values());
    }
}
