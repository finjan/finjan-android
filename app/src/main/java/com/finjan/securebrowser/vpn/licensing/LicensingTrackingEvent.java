package com.finjan.securebrowser.vpn.licensing;

import com.finjan.securebrowser.vpn.licensing.models.restful.License;

import java.util.List;

/**
 * @author ovidiu.buleandra
 * @since 05.04.2016
 */
public class LicensingTrackingEvent {
    public List<License> previousLicenses;
    public List<License> currentLicenses;
    public String requestUrl;
    public long queryDuration;

    public LicensingTrackingEvent(List<License> prev, List<License> current, String url, long duration) {
        previousLicenses = prev;
        currentLicenses = current;
        requestUrl = url;
        queryDuration = duration;
    }
}
