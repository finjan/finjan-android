package com.finjan.securebrowser.vpn.ui.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.VpnService;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.avira.common.authentication.models.User;
import com.avira.common.id.HardwareId;
import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.ProfileManager;
import de.blinkt.openvpn.core.VpnStatus;
import de.greenrobot.event.EventBus;

import com.avira.common.utils.HashUtility;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.auto_connect.AutoConnectAcitivity;
import com.finjan.securebrowser.vpn.eventbus.CancelConnectingEvent;
import com.finjan.securebrowser.vpn.service.AviraOpenVpnService;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.log.FileLogger;


/**
 * This class was a clone from LaunchVPN.java as an entry point to start open vpn service
 * it prompts the user for VPN permission and starts the thread to run open vpn service
 *
 *
 *
 *
 *  Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 *  Finjan Mobile Vital Security Browser
 *
 *  Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *  Developed by NewOfferings, LLC
 *
 *
 */
public class LaunchAviraVpnActivity extends Activity {

    private static final String TAG = LaunchAviraVpnActivity.class.getSimpleName();
    public static final String EXTRA_KEY = "com.avira.openvpnwrapper.shortcutProfileUUID";
    public static final String EXTRA_NAME = "com.avira.openvpnwrapper.shortcutProfileName";


    private static final int START_VPN_PROFILE = 70;

    private VpnProfile mSelectedProfile;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
//        if(!UserPref.getInstance().getIsMyAppWasInBackground()){
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    GlobalVariables.getInstnace().isLaunchVpnAcivityCalled = false;
//                }
//            },4000);
//        }
//        if(UserPref.getInstance().getIsMyAppWasInBackground()){
//            moveTaskToBack(true);
//        }else {
//            moveTaskToBack(false);
//        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Resolve the intent

            final Intent intent = getIntent();
            final String action = intent.getAction();

            if (Intent.ACTION_MAIN.equals(action)) {

                // we got called to be the starting point, most likely a shortcut
                String shortcutUUID = intent.getStringExtra(EXTRA_KEY);
                String shortcutName = intent.getStringExtra(EXTRA_NAME);

                VpnProfile profileToConnect = ProfileManager.get(this, shortcutUUID);
                if (shortcutName != null && profileToConnect == null)
                    profileToConnect = ProfileManager.getInstance(this).getProfileByName(shortcutName);

                if (profileToConnect == null) {
                    Logger.logE(TAG, "unable to retrieve profile to connect");
                    // show Log window to display error
                    finish();
                    return;
                }

                mSelectedProfile = profileToConnect;
                launchVPN();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
            GlobalVariables.getInstnace().isLaunchVpnAcivityCalled = true;
        if(!FinjanVPNApplication.isMyAppInBackground){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    GlobalVariables.getInstnace().isLaunchVpnAcivityCalled = false;
                }
            },1500);
        }
    }

//    private static

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == START_VPN_PROFILE) {
            if (resultCode == Activity.RESULT_OK) {
                UserPref.getInstance().isUserHadGivenVPNPermission(true);
                String hardwareId = "0211-"+HashUtility.sha1(HardwareId.get(getApplicationContext()));
                mSelectedProfile.mUsername = hardwareId;
                TrafficController trafficController = TrafficController.getInstance(getApplicationContext());
                if (trafficController.isRegistered()) {
                    mSelectedProfile.mTransientPW = FinjanVpnPrefs.getAuthToken(FinjanVPNApplication.getInstance());
//                    mSelectedProfile.mTransientPW = UserPref.getInstance().getCurrent_Access_Token();
                } else {
                    mSelectedProfile.mTransientPW = hardwareId;
                }
                new startOpenVpnThread().start();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                EventBus.getDefault().post(new CancelConnectingEvent());
                // User does not want us to start, so we just vanish
                VpnStatus.updateStateString("USER_VPN_PERMISSION_CANCELLED", "", R.string.state_user_vpn_permission_cancelled,
                        VpnStatus.ConnectionStatus.LEVEL_CONNECTION_CANCELED);
                finish();
            }
        }
    }


    void showConfigErrorDialog(int vpnok) {
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setTitle(R.string.config_error_found);
        d.setMessage(vpnok);
        d.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();

            }
        });
        d.show();
    }

    void launchVPN() {
        FileLogger.logToFile(LaunchAviraVpnActivity.class, "launchVPN");
        int vpnok = mSelectedProfile.checkProfile(this);
        if (vpnok != R.string.no_error_found) {
            showConfigErrorDialog(vpnok);
            return;
        }

        Intent intent = VpnService.prepare(this);

        if (intent != null) {
            // Start the query
            try {
                startActivityForResult(intent, START_VPN_PROFILE);
            } catch (ActivityNotFoundException ane) {
                // Shame on you Sony! At least one user reported that
                // an official Sony Xperia Arc S image triggers this exception
                Logger.logI(TAG, "vpn not supported");
            }
        } else {
            onActivityResult(START_VPN_PROFILE, Activity.RESULT_OK, null);
        }

    }

    private class startOpenVpnThread extends Thread {
        @Override
        public void run() {
            try {
                Context context = getBaseContext();
                Intent startVPN = mSelectedProfile.prepareStartService(context);
                startVPN.setClass(context, AviraOpenVpnService.class);
                context.startService(startVPN);
                finish();
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }


}
