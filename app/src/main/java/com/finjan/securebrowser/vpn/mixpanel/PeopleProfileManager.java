/*
 * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.mixpanel;

import android.content.Context;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.avira.common.GeneralPrefs;
import com.avira.common.id.HardwareId;
import com.avira.common.utils.HashUtility;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.tracking.mixpanel.TrackingManager;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import java.util.Locale;

/**
 * Provides access to modify MixPanel people profile attributes
 */
public class PeopleProfileManager {

    private static final String TAG = PeopleProfileManager.class.getSimpleName();
    private static final String LOCALE = "deviceLocale";


    /**
     * https://mixpanel.com/help/questions/articles/special-or-reserved-properties
     */
    private static final String NAME = "$name";

    /**
     * 12 digit ID retrieved from our Google API console URL. Described here:
     * https://mixpanel.com/help/reference/android-push-notifications#enabling
     */
//    private static final String GOOGLE_SENDER_ID = "152981558767";

    private static PeopleProfileManager INSTANCE;

    private PeopleProfileManager() {
//        peopleApi = TrackingManager.getInstance().getPeople();
//        peopleApi.initPushHandling(GOOGLE_SENDER_ID); // to receive MixPanel push notifications
    }

    public static PeopleProfileManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PeopleProfileManager();
        }
        return INSTANCE;
    }

//    public void initializeProfile(Context context) {
////        String currentId = peopleApi.getDistinctId();
//
//        if (Authentication.isRegistered(context)) {

//            boolean usingRegisteredId = (currentId != null && currentId.equals(getIdForRegisteredUser(context)));
//            if (!usingRegisteredId) { // user is registered but not the correct id

                // update to a registered user id via identify instead of aliasing
                // https://mixpanel.com/help/questions/articles/how-should-i-use-alias-and-identify-for-my-existing-users-who-have-already-signed-up
//                updateAnonymousToLoggedInProfile();
//            }
//
//        } else if (TextUtils.isEmpty(currentId)){
//            createAnonymousProfile();
//        }
//
//        updateLocale(context);
//        updateInstalledAviraAppsList(context);
//    }

    public void updateLocale(Context context) {
        Locale locale = context.getResources().getConfiguration().locale;
        Logger.logD(TAG, "updateLocale " + locale);
//        peopleApi.set(LOCALE, locale);
    }

    public void updateInstalledAviraAppsList(Context context) {
//        List<String> installedApps = AviraApps.getInstalledAviraApps(context);

        String installedAppsName = "VitalSecurity";


        Logger.logD(TAG, "updateInstalledAviraAppsList " + installedAppsName);
//        peopleApi.set(/"app_name", installedAppsName);
    }

    /**
     * Forces MixPanel to apply latest A/B test changes, if they are present.
     */
    public void joinExperiments() {
//        peopleApi.joinExperimentIfAvailable();
    }

    /**
     * Create a people profile property for anonymous user (not registered/logged in)
     */
    private void createAnonymousProfile() {
        Logger.logI(TAG, "Create profile for anonymous user");
        String distinctId = getIdForAnonymousUser();

        TrackingManager.getInstance().identify(distinctId); // this will replace the auto-generated MixPanel distinct id
//        peopleApi.set(NAME, distinctId);
    }

    /**
     * After user logs in, start identifying the user using his/her own unique id which is the same across devices
     * as recommended by MixPanel:
     * https://mixpanel.com/docs/integration-libraries/using-mixpanel-alias
     */
    public void updateAnonymousToLoggedInProfile() {
        String distinctId = getIdForRegisteredUser(FinjanVPNApplication.getInstance());
        TrackingManager.getInstance().identify(distinctId);
        Logger.logI(TAG, "Updated anonymous profile to logged in profile. Identify with distinct id: " + distinctId);
    }

    /**
     * After user signs up, alias a new id (unique user identification which will be the same across devices - in our case the hash of user email)
     * to the previously assigned distinct id in {@link PeopleProfileManager#createAnonymousProfile()}.
     *
     * Alias does NOT replace the original distinct id but simply just informs MixPanel to map this new id to the original id.
     * With this, the next time user logs in from another device, we will only need to call {@link MixpanelAPI#identify(String)} with the new id,
     * and MixPanel will map its activities to the profile of the original id.
     *
     * https://mixpanel.com/docs/integration-libraries/using-mixpanel-alias
     */
    public void updateAnonymousToRegisteredProfile() {
        String distinctId = getIdForRegisteredUser(FinjanVPNApplication.getInstance());

//        TrackingManager.getInstance().alias(distinctId, peopleApi.getDistinctId());
//        peopleApi.set(NAME, distinctId);

        Logger.logI(TAG, "Updated anonymous profile to registered profile. Aliased distinct id: " + distinctId);
    }

    /**
     * @return The distinct ID to represent anonymous user
     */
    private String getIdForAnonymousUser() {
        return HardwareId.get(FinjanVPNApplication.getInstance());
    }

    /**
     * @return The distinct ID to represent the registered user (same across devices)
     */
    @Nullable
    private String getIdForRegisteredUser(Context context) {
//        UserProfile userProfile = UserProfile.load();

        if (TextUtils.isEmpty(GeneralPrefs.getEmailId(context))) {
            String email = GeneralPrefs.getEmailId(context);

            if (!TextUtils.isEmpty(email)) {
                return HashUtility.sha1(email);
            }
        }

        Logger.logE(TAG, "Failed to generate ID for registered user. Fallback to re-use id for anonymous user");
        return getIdForAnonymousUser();

    }
}
