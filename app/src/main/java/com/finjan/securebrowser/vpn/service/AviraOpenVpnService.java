package com.finjan.securebrowser.vpn.service;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.avira.common.dialogs.RateMeDialogUtils;
import com.avira.common.utils.SharedPreferencesUtilities;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.util.JsonUtils;
import com.finjan.securebrowser.util.NotificationHandler;
import com.finjan.securebrowser.vpn.eventbus.VpnStatusEvent;
import com.finjan.securebrowser.vpn.receiver.NotificationActionReceiver;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;

import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.Util;
import com.finjan.securebrowser.helpers.GlobalVariables;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.OpenVPNService;
import de.blinkt.openvpn.core.ProfileManager;
import de.blinkt.openvpn.core.VpnStatus;
import de.greenrobot.event.EventBus;

/**
 * Inherited from OpenVpnService to override the UI related logics
 */
public class AviraOpenVpnService extends OpenVPNService {

    private static final int OPENVPN_STATUS = 1;
    private VpnProfile mProfile;
    private VpnStatus.ConnectionStatus connectionStatus = VpnStatus.ConnectionStatus.LEVEL_NOTCONNECTED;
    private boolean mDisplayBytecount = false;
    private long mConnecttime;
    private final IBinder mBinder = new LocalBinder();

    public static final long BYTE = 1048576;
    private static final long FIRST_LIMIT_TRAFFIC_L = 145 * BYTE;
    private static final long FIRST_LIMIT_TRAFFIC_G = 155 * BYTE;

    private static final long SECOND_LIMIT_TRAFFIC_L = 195 * BYTE;
    private static final long SECOND_LIMIT_TRAFFIC_G = 205 * BYTE;

    private static final long THIRD_LIMIT_TRAFFIC_L = 245 * BYTE;
    private static final long THIRD_LIMIT_TRAFFIC_G = 255 * BYTE;

    public static final String SHOW_RATE_DIALOG_FOR_TRAFFIC_LIMIT = "show_rate_dialog_for_traffic";
    public static final String SHOW_RATE_DIALOG_TRAFFIC = "show_rate_dialog_traffic";


    public class LocalBinder extends Binder {
        public AviraOpenVpnService getService() {
            // Return this instance of LocalService so clients can call public methods
            return AviraOpenVpnService.this;
        }
    }

    public VpnStatus.ConnectionStatus getConnectionStatus() {
        return connectionStatus;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.hasExtra(getPackageName() + ".profileUUID")) {
            String profileUUID = intent.getStringExtra(getPackageName() + ".profileUUID");
            mProfile = ProfileManager.get(this, profileUUID);
        } else {
            /* The intent is null when we are set as always-on or the service has been restarted. */
            mProfile = ProfileManager.getLastConnectedProfile(this, false);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onRevoke() {
        //save the used traffic when connection status changed because of permission revoke
        TrafficController.getInstance(getApplicationContext()).applyNewTraffic();
        super.onRevoke();
    }

    // Similar to revoke but do not try to stop process
    public void processDied() {
        //save the traffic usage when process died
        TrafficController.getInstance(getApplicationContext()).applyNewTraffic();
        super.processDied();
    }

    @SuppressWarnings("deprecation")
    public void showNotification(final String msg, String tickerText, boolean lowpriority, long when, VpnStatus.ConnectionStatus status) {
       if (status == VpnStatus.ConnectionStatus.LEVEL_CONNECTED )
       {
           LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
           Intent intent = new Intent(AppConstants.CONNECTED_ACTION);
           localBroadcastManager.sendBroadcast(intent);
       }
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
        String channelName = "Invincibill VPN";
        String channelId = "com.invincibull.vpn";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);

            mNotificationManager.createNotificationChannel(mChannel);
        }

        int icon = getIconByConnectionStatus(status);

        android.app.Notification.Builder nbuilder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            nbuilder = new Notification.Builder(this, channelId);
        } else {
            nbuilder = new Notification.Builder(this);
        }

        if (mProfile != null)
            nbuilder.setContentTitle(getString(R.string.app_name));
        else
            nbuilder.setContentTitle(getString(R.string.notifcation_title_notconnect));

        nbuilder.setContentText(msg);
        nbuilder.setOnlyAlertOnce(true);
        nbuilder.setOngoing(true);
        nbuilder.setContentIntent(getMainActivityPendingIntent());
        nbuilder.setSmallIcon(icon);


        if (when != 0)
            nbuilder.setWhen(when);


        // Try to set the priority available since API 16 (Jellybean)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            jbNotificationExtras(lowpriority, nbuilder);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            lpNotificationExtras(nbuilder);

        if (tickerText != null && !tickerText.equals(""))
            nbuilder.setTicker(tickerText);

        Notification notification = nbuilder.getNotification();


        mNotificationManager.notify(OPENVPN_STATUS, notification);
        startForeground(OPENVPN_STATUS, notification);
    }


    private int getIconByConnectionStatus(VpnStatus.ConnectionStatus level) {
        switch (level) {
            case LEVEL_CONNECTED:
                return R.drawable.ic_stat_vpn;
            case LEVEL_AUTH_FAILED:
            case LEVEL_NONETWORK:
            case LEVEL_NOTCONNECTED:
                return R.drawable.ic_stat_vpn_offline;
            case LEVEL_CONNECTING_NO_SERVER_REPLY_YET:
            case LEVEL_WAITING_FOR_USER_INPUT:
                return R.drawable.ic_stat_vpn_outline;
            case LEVEL_CONNECTING_SERVER_REPLIED:
                return R.drawable.ic_stat_vpn_empty_halo;
            case LEVEL_VPNPAUSED:
                return android.R.drawable.ic_media_pause;
            case UNKNOWN_LEVEL:
            default:
                return R.drawable.ic_stat_vpn;

        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void lpNotificationExtras(Notification.Builder nbuilder) {
        nbuilder.setCategory(Notification.CATEGORY_SERVICE);
        nbuilder.setLocalOnly(true);

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void jbNotificationExtras(boolean lowpriority,
                                      Notification.Builder nbuilder) {
        try {
            if (lowpriority) {
                Method setpriority = nbuilder.getClass().getMethod("setPriority", int.class);
                // PRIORITY_MIN == -2
                setpriority.invoke(nbuilder, -2);

                Method setUsesChronometer = nbuilder.getClass().getMethod("setUsesChronometer", boolean.class);
                setUsesChronometer.invoke(nbuilder, true);

            }

//            if(VpnActivity.I_AM_ALIVE && VpnActivity.getInstance()!=null){
//                VpnActivity.getInstance().onDisconnectButtonClick();
//                return;
//            }
            Intent disconnectVPN = new Intent(this, VpnActivity.class);
            disconnectVPN.setAction(DISCONNECT_VPN);
            PendingIntent disconnectPendingIntent = PendingIntent.getActivity(this, 0, disconnectVPN, 0);

            nbuilder.addAction(android.R.drawable.ic_menu_close_clear_cancel,
                    getString(R.string.cancel_connection), disconnectPendingIntent);

            //ignore exception
        } catch (NoSuchMethodException | IllegalArgumentException |
                InvocationTargetException | IllegalAccessException e) {
            VpnStatus.logException(e);
        }
    }

    // Finjan header flag ~Anurag
    private void setVpnIcon() {
        if (GlobalVariables.getInstnace().currentHomeFragment != null &&
                GlobalVariables.getInstnace().currentHomeFragment.isAdded() &&
                GlobalVariables.getInstnace().currentHomeFragment.isVisible()) {
            GlobalVariables.getInstnace().currentHomeFragment.setVpnIcon();
        }
    }


    public static boolean noProcessRunning;

    @Override
    public void updateState(String state, String logmessage, int resid, VpnStatus.ConnectionStatus level) {
        // If the process is not running, ignore any state,
        // Notification should be invisible in this state

        connectionStatus = level;
        Logger.logI(AviraOpenVpnService.class.getSimpleName(), logmessage + " Connection level -- " + level);
        FinjanVpnPrefs.saveVpnConnectionStatus(this, level);
//        setVpnIcon();
        EventBus.getDefault().post(new VpnStatusEvent(level));

        doSendBroadcast(state, level);
        Log.e("State", level.toString());

        boolean lowpriority = false;
        // Display byte count only after being connected

        {
            if (level == VpnStatus.ConnectionStatus.LEVEL_WAITING_FOR_USER_INPUT) {
                // The user is presented a dialog of some kind, no need to inform the user
                // with a notifcation
                return;
            } else if (level == VpnStatus.ConnectionStatus.LEVEL_CONNECTED) {
                mDisplayBytecount = true;
                mConnecttime = System.currentTimeMillis();
            } else {
                mDisplayBytecount = false;
            }

            // Other notifications are shown,
            // This also mean we are no longer connected, ignore bytecount messages until next
            // CONNECTED
            // Does not work :(
            String msg = getString(resid);
            if (msg.equals(getString(R.string.state_noprocess))) {
                noProcessRunning = true;
            } else if (msg.equals(getString(R.string.state_connected))) {
                noProcessRunning = false;
            }
            showNotification(VpnStatus.getLastCleanLogMessage(this),
                    msg, lowpriority, 0, level);

        }
    }

    private void doSendBroadcast(String state, VpnStatus.ConnectionStatus level) {
        Intent vpnstatus = new Intent();
        vpnstatus.setAction("com.avira.openvpnwrapper.VPN_STATUS");
        vpnstatus.putExtra("status", level.toString());
        vpnstatus.putExtra("detailstatus", state);
        sendBroadcast(vpnstatus, Manifest.permission.ACCESS_NETWORK_STATE);
    }


    synchronized  private ArrayList<PlistHelper.OutOfDataNotification> onRegistered(long traffic,long trafficLimit) {
        int progress;
        HashMap<PlistHelper.OutOfDataNotification, Integer> hashMapNotification = new HashMap<>();
        double percentage = ((trafficLimit - traffic) * 100d) / trafficLimit;
        percentage = Math.floor(percentage * 100) / 100;

        progress = (int) percentage;
//        progress = 45;
        String jsonNotificationArray = UserPref.getInstance().getNotificatioJson();
        String jsonPersentNotificationArray = UserPref.getInstance().getNotificatioPersentageJson();
        ArrayList<PlistHelper.OutOfDataNotification> outofDataArrayList = null;

        if (!TextUtils.isEmpty(jsonNotificationArray)) {
//                    outofDataArrayList = (ArrayList<PlistHelper.OutOfDataNotification>) JsonUtils.convertJsonToModel(jsonNotificationArray, ArrayList.class);
            PlistHelper.OutOfDataNotificationModel   outOfDataNotificationModel = (PlistHelper.OutOfDataNotificationModel) JsonUtils.convertJsonToModel(jsonNotificationArray, PlistHelper.OutOfDataNotificationModel.class);
            outofDataArrayList = outOfDataNotificationModel.getOutOfDataNotifications();
        }
        Integer[] arrayPersent = new Integer[outofDataArrayList.size()];
        if (!TextUtils.isEmpty(jsonPersentNotificationArray))
            arrayPersent = (Integer[]) JsonUtils.convertJsonToModel(jsonPersentNotificationArray, Integer[].class);
        int possition = -1;
        if (arrayPersent != null && arrayPersent.length > 0) {
            if (Arrays.binarySearch(arrayPersent, progress) > 0) {
                possition = Arrays.binarySearch(arrayPersent, progress) + 1;
            } else if (Arrays.binarySearch(arrayPersent, progress) < 0) {
                possition = Integer.parseInt(String.valueOf(Arrays.binarySearch(arrayPersent, progress)).replace("-", ""));
            }
        }
        if (possition != -1 && possition <= arrayPersent.length) {
            int persentage = arrayPersent[possition - 1];
            for (int i=0;i<outofDataArrayList.size();i++) {
                if (outofDataArrayList.get(i).isEnable() && UserPref.getInstance().getcurrentRange() > persentage && persentage == outofDataArrayList.get(i).getPercent_remaining()) {
                    UserPref.getInstance().setcurrentRange(persentage);
                    UserPref.getInstance().setInappMassege(persentage);
                    if (outofDataArrayList.get(i).getAlert_type().equals("both")) {

                        UserPref.getInstance().setPushMassege(true);
                        showInsecureWifiNotification(this, outofDataArrayList.get(i).getDeep_link_url(), outofDataArrayList.get(i).getMsg());
                        if (!VpnActivity.I_AM_ALIVE) {

                            Intent intent = new Intent(this, VpnActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            this.startActivity(intent);
                        }
                    } else if (outofDataArrayList.get(i).getAlert_type().equals("notification")) {
                        UserPref.getInstance().setPushMassege(true);
                        showInsecureWifiNotification(this, outofDataArrayList.get(i).getDeep_link_url(), outofDataArrayList.get(i).getMsg());

                    } else if (outofDataArrayList.get(i).getAlert_type().equals("inapp")) {
                        if (!VpnActivity.I_AM_ALIVE) {

                            Intent intent = new Intent(this, VpnActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            this.startActivity(intent);
                        }

                    }
                    break;

                }
            }
        }
        return outofDataArrayList;
    }

    @Override
    public void updateByteCount(long in, long out, long diffIn, long diffOut) {
        if (mDisplayBytecount) {
            TrafficController trafficController = TrafficController.getInstance(getApplicationContext());
            long traffic = in + out + trafficController.getTraffic();
//            long traffic = 1000;
            checkAndSetTrafficLimitReached(traffic);
            trafficController.updateNewTraffic(in + out);
            //TrafficController.getInstance(getApplicationContext()).applyNewTraffic();
            long trafficLimit = trafficController.getTrafficLimit();
//            long trafficLimit = 1000;
            // String licenceType = trafficController.getLicenseType();
            String netstat;


            if (/*trafficLimit > 0*/!TrafficController.getInstance(this).isPaid()) {

                ArrayList<PlistHelper.OutOfDataNotification> outofDataArrayList =     onRegistered(traffic,trafficLimit);
               /* int progress;
                HashMap<PlistHelper.OutOfDataNotification, Integer> hashMapNotification = new HashMap<>();
                double percentage = ((trafficLimit - traffic) * 100d) / trafficLimit;
                percentage = Math.floor(percentage * 100) / 100;

                progress = (int) percentage;
                progress = 0;
                String jsonNotificationArray = UserPref.getInstance().getNotificatioJson();
                String jsonPersentNotificationArray = UserPref.getInstance().getNotificatioPersentageJson();
                ArrayList<PlistHelper.OutOfDataNotification> outofDataArrayList = null;

                if (!TextUtils.isEmpty(jsonNotificationArray)) {
//                    outofDataArrayList = (ArrayList<PlistHelper.OutOfDataNotification>) JsonUtils.convertJsonToModel(jsonNotificationArray, ArrayList.class);
                    PlistHelper.OutOfDataNotificationModel   outOfDataNotificationModel = (PlistHelper.OutOfDataNotificationModel) JsonUtils.convertJsonToModel(jsonNotificationArray, PlistHelper.OutOfDataNotificationModel.class);
                    outofDataArrayList = outOfDataNotificationModel.getOutOfDataNotifications();
                }
                Integer[] arrayPersent = new Integer[outofDataArrayList.size()];
                if (!TextUtils.isEmpty(jsonPersentNotificationArray))
                    arrayPersent = (Integer[]) JsonUtils.convertJsonToModel(jsonPersentNotificationArray, Integer[].class);
                int possition = -1;
                if (arrayPersent != null && arrayPersent.length > 0) {
                    if (Arrays.binarySearch(arrayPersent, progress) > 0) {
                        possition = Arrays.binarySearch(arrayPersent, progress) + 1;
                    } else if (Arrays.binarySearch(arrayPersent, progress) < 0) {
                        possition = Integer.parseInt(String.valueOf(Arrays.binarySearch(arrayPersent, progress)).replace("-", ""));
                    }
                }
                if (possition != -1 && possition <= arrayPersent.length) {
                    int persentage = arrayPersent[possition - 1];
                    for (int i=0;i<outofDataArrayList.size();i++) {
                        if (UserPref.getInstance().getcurrentRange() != persentage && persentage == outofDataArrayList.get(i).getPercent_remaining()) {
                            UserPref.getInstance().setcurrentRange(persentage);
                            UserPref.getInstance().setInappMassege(persentage);
                            if (outofDataArrayList.get(i).getAlert_type().equals("both")) {

                                UserPref.getInstance().setPushMassege(true);
                                showInsecureWifiNotification(this, outofDataArrayList.get(i).getDeep_link_url(), outofDataArrayList.get(i).getMsg());
                                if (!VpnActivity.I_AM_ALIVE) {

                                    Intent intent = new Intent(this, VpnActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    this.startActivity(intent);
                                }
                            } else if (outofDataArrayList.get(i).getAlert_type().equals("notification")) {
                                UserPref.getInstance().setPushMassege(true);
                                showInsecureWifiNotification(this, outofDataArrayList.get(i).getDeep_link_url(), outofDataArrayList.get(i).getMsg());

                            } else if (outofDataArrayList.get(i).getAlert_type().equals("inapp")) {
                                if (!VpnActivity.I_AM_ALIVE) {

                                    Intent intent = new Intent(this, VpnActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    this.startActivity(intent);
                                }

                            }
                            break;

                        }
                    }
                }*/
//                for (PlistHelper.OutOfDataNotification outOfDataNotification : outofDataArrayList)
//                {
//                    if (outOfDataNotification.isEnable() && !UserPref.getInstance().getNotificationAvilable(outOfDataNotification.getPercent_remaining()) &&  progress - outOfDataNotification.getPercent_remaining() < 0)
//                    {
//                       hashMapNotification.put(outOfDataNotification, progress - outOfDataNotification.getPercent_remaining());
//                    }else
//                        continue;
//                }
//
//                if (hashMapNotification.size()>0) {
//                    List<Map.Entry<PlistHelper.OutOfDataNotification, Integer>> list =
//                            new LinkedList<Map.Entry<PlistHelper.OutOfDataNotification, Integer>>(hashMapNotification.entrySet());
//
//                    // Sort the list
//                    Collections.sort(list, new Comparator<Map.Entry<PlistHelper.OutOfDataNotification, Integer>>() {
//                        public int compare(Map.Entry<PlistHelper.OutOfDataNotification, Integer> o1,
//                                           Map.Entry<PlistHelper.OutOfDataNotification, Integer> o2) {
//                            return (o1.getValue()).compareTo(o2.getValue());
//                        }
//                    });
//                  PlistHelper.OutOfDataNotification outOfDataNotification =   ((LinkedList<Map.Entry<PlistHelper.OutOfDataNotification, Integer>>) list).getLast().getKey();
//                  if (outOfDataNotification !=null)
//                  {
//                      if (outOfDataNotification.getAlert_type().equals("both"))
//                      {
//                          showInsecureWifiNotification(this,outOfDataNotification.getDeep_link_url(),outOfDataNotification.getMsg());
//                          if (!VpnActivity.I_AM_ALIVE) {
//
//                              Intent intent = new Intent(this, VpnActivity.class);
//                              this.startActivity(intent);
//                          }
//                      }else if (outOfDataNotification.getAlert_type().equals("notification"))
//                      {
//                          showInsecureWifiNotification(this,outOfDataNotification.getDeep_link_url(),outOfDataNotification.getMsg());
//
//                      }else if (outOfDataNotification.getAlert_type().equals("inapp"))
//                      {
//                          if (!VpnActivity.I_AM_ALIVE) {
//
//                          Intent intent = new Intent(this, VpnActivity.class);
//                          this.startActivity(intent);
//                          }
//
//                      }
//
//                  }
//                }


                if (traffic >= trafficLimit) {

                    for (int i=0;i<outofDataArrayList.size();i++) {
                        if (0 == outofDataArrayList.get(i).getPercent_remaining()) {
                            UserPref.getInstance().setcurrentRange(0);
                            UserPref.getInstance().setInappMassege(0);
                            if (outofDataArrayList.get(i).getAlert_type().equals("both")) {

                                UserPref.getInstance().setPushMassege(true);
                                showInsecureWifiNotification(this, outofDataArrayList.get(i).getDeep_link_url(), outofDataArrayList.get(i).getMsg());

                            } else if (outofDataArrayList.get(i).getAlert_type().equals("notification")) {
                                UserPref.getInstance().setPushMassege(true);
                                showInsecureWifiNotification(this, outofDataArrayList.get(i).getDeep_link_url(), outofDataArrayList.get(i).getMsg());

                            } /*else if (outofDataArrayList.get(i).getAlert_type().equals("inapp")) {
                                if (!VpnActivity.I_AM_ALIVE) {

                                    Intent intent = new Intent(this, VpnActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    this.startActivity(intent);
                                }

                            }*/
                            break;

                        }
                    }
                    //showInsecureWifiNotification(this);
//                    showNotification(this,"Gautam","Trignodev",true);
                    trafficController.runTrafficLimitTimer(this, getApplicationContext(), traffic);
//                    VpnUtil.stopVpnConnection(this, this);
                    UserPref.getInstance().setAutoConnect(false);


                    // showInsecureWifiNotification(this);


                    return; // do no show any more notification
                }
                netstat = getString(R.string.traffic_notification_description_limited, Util.humanReadableByteCount(trafficLimit - traffic, false));
            } else {
                netstat = getString(R.string.traffic_notification_description_unlimited);
                //VpnUtil.stopVpnConnection(this, this);
            }

            boolean lowpriority = true;
            showNotification(netstat, null, lowpriority, mConnecttime, VpnStatus.ConnectionStatus.LEVEL_CONNECTED);
        }

    }


    private void showInsecureWifiNotification(Context context, String deep_link_url, String desc) {


        /* positiveIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(NotificationActionReceiver.ACTION_INSECURE_NOTIF_POSITIVE),
                PendingIntent.FLAG_CANCEL_CURRENT);*/
      /*  PendingIntent positiveIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(context,NotificationActionReceiver.class),
                PendingIntent.FLAG_CANCEL_CURRENT);*/

       /* PendingIntent negativeIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(NotificationActionReceiver.ACTION_INSECURE_NOTIF_NEGATIVE),
                PendingIntent.FLAG_CANCEL_CURRENT);*/
        NotificationHandler.Companion.getInstance().init(context);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, NotificationHandler.Companion.getInstance().processIntent(deep_link_url),
                PendingIntent.FLAG_UPDATE_CURRENT);

        //String desc = PlistHelper.getInstance().getNotification_action_msg();
        String channelName = "Invincibill VPN Exaust";
        String channelId = "com.invincibull.exaustedtraffic";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);
        }
        // build notification
        Notification notification = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.info_icon)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.app_icon))
                .setContentText(desc)
                .setContentIntent(contentIntent) // for < API 16 devices where there's no action buttons
                .setStyle(new NotificationCompat.BigTextStyle().bigText(desc)) // prevents desc to be ellipsized
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat.from(context).notify(NotificationActionReceiver.NOTIF_TRAFIC_EXAUSTED, notification);
    }


    @Override
    public IBinder onBind(Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals(START_SERVICE))
            return mBinder;
        else
            return super.onBind(intent);
    }

    private PendingIntent getMainActivityPendingIntent() {
        // Let the configure Button show the Log
        Intent intent = new Intent(getBaseContext(), VpnActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        PendingIntent startMainActivity = PendingIntent.getActivity(this, 0, intent, 0);
        return startMainActivity;
    }

    PendingIntent getLogPendingIntent() {
        return getMainActivityPendingIntent();
    }

    private void checkAndSetTrafficLimitReached(long traffic) {
        Context context = this;
        if (!RateMeDialogUtils.isAppRated(context)) {
            if (SharedPreferencesUtilities.getBoolean(context, SHOW_RATE_DIALOG_FOR_TRAFFIC_LIMIT, false)) {
                long trafficMb = traffic / BYTE;
                SharedPreferencesUtilities.putLong(context, SHOW_RATE_DIALOG_TRAFFIC, trafficMb);
            } else if ((traffic >= FIRST_LIMIT_TRAFFIC_L && traffic <= FIRST_LIMIT_TRAFFIC_G) ||
                    (traffic >= SECOND_LIMIT_TRAFFIC_L && traffic <= SECOND_LIMIT_TRAFFIC_G) ||
                    (traffic >= THIRD_LIMIT_TRAFFIC_L && traffic <= THIRD_LIMIT_TRAFFIC_G)) {

                SharedPreferencesUtilities.putBoolean(context, SHOW_RATE_DIALOG_FOR_TRAFFIC_LIMIT, true);
                long trafficMb = traffic / BYTE;
                SharedPreferencesUtilities.putLong(context, SHOW_RATE_DIALOG_TRAFFIC, trafficMb);
            }
        }
    }
}
