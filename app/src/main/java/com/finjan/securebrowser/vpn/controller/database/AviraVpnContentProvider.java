/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.controller.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.finjan.securebrowser.helpers.logger.Logger;

import java.util.ArrayList;

public class AviraVpnContentProvider extends ContentProvider {

    private static final String LOG_TAG = AviraVpnContentProvider.class.getSimpleName();

    public static final String AUTHORITY = "com.finjan.vpn";
    public static final String URI_CONTENT_PREFIX = "content://";
    public static final String DEFAULT_VALUE_STRING = "1";

    private AviraVpnDbHelper mLocalDatabase;

    // sms constants
    public static final int ALL_SERVERS = 1;
    public static final int SINGLE_SERVER = 2;

    private static Uri SERVER_URI = Uri.parse("/" + DatabaseContract.ServerTable.TABLE);

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    /**
     * counter to be used as unique ID for newly created loader,
     * should be changed on each use to be unique
     */
    private static int loaderId = 0;

    /**
     * get a unique ID for a newly created loader
     *
     * @return unique id to be used as a loader id
     */
    public static synchronized int getUniqueLoaderId() {
        loaderId++;
        return loaderId;
    }

    public AviraVpnContentProvider() {
    }

    @Override
    public boolean onCreate() {
        uriMatcher.addURI(AUTHORITY, DatabaseContract.ServerTable.TABLE, ALL_SERVERS);
        uriMatcher.addURI(AUTHORITY, DatabaseContract.ServerTable.TABLE + "/#", SINGLE_SERVER);


        mLocalDatabase = new AviraVpnDbHelper(getContext());
        return true;
    }

    public static Uri getServerUri() {
        return Uri.parse(URI_CONTENT_PREFIX + AUTHORITY + SERVER_URI);
    }

    public static Uri getSingleServerUri(final long serverId) {
        return Uri.parse(URI_CONTENT_PREFIX + AUTHORITY + SERVER_URI + "/" + serverId);
    }

    @Nullable
    private Uri getContentUriFromUri(@NonNull Uri uri) {
        // Only for actual tables
        switch (uriMatcher.match(uri)) {
            case ALL_SERVERS:
            case SINGLE_SERVER:
                return SERVER_URI;
            default:
                break;
        }

        return null;
    }

    @Nullable
    private String getTableNameFromUri(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case ALL_SERVERS:
            case SINGLE_SERVER:
                return DatabaseContract.ServerTable.TABLE;
            default:
                break;
        }

        return null;
    }

    public void sendNotification(@NonNull Uri uri) {
        if (getContext() != null && getContext().getContentResolver() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
    }

    @NonNull
    private ArrayList<Uri> getAssociatedViewUris(@NonNull Uri uri) {
        // Only for actual views
        ArrayList<Uri> viewUris = new ArrayList<>();
        switch (uriMatcher.match(uri)) {
            case ALL_SERVERS:
            case SINGLE_SERVER:
                viewUris.add(getServerUri());
                break;
            default:
                break;
        }

        return viewUris;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        // Return a string that identifies the MIME type for a Content Provider URI
        switch (uriMatcher.match(uri)) {
            case ALL_SERVERS:
            case SINGLE_SERVER:
                return "vnd.android.cursor.dir/vnd." + AUTHORITY + "." + DatabaseContract.ServerTable.TABLE;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        // Replace these with valid SQL statements if necessary.
        String groupBy = null;
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // If this is a row query, limit the result set to the passed in row.
        String rowID;
        switch (uriMatcher.match(uri)) {
            case SINGLE_SERVER:
                rowID = uri.getPathSegments().get(1);
                queryBuilder.appendWhere(BaseColumns._ID + "=" + rowID);
                break;
            default:
                break;
        }
        // Specify the table on which to perform the query. This can be a specific table or a join as required.
        queryBuilder.setTables(getTableNameFromUri(uri));

        // Execute...
        Cursor cursor = queryBuilder.query(mLocalDatabase.getReadableDatabase(), projection, selection, selectionArgs, groupBy, null, sortOrder);

        if (cursor != null && getContext() != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        } else {
            Logger.logE(LOG_TAG, "Cursor is null, uri " + uri.toString());
        }

        return cursor;
    }

    @Override
    @Nullable
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        // Try to do an insert as per usual
        long id = mLocalDatabase.getWritableDatabase().insert(getTableNameFromUri(uri), null, values);

        if (id > -1) {
            // the insert was successful
            sendNotification(uri);
            // For non-query statements, we also check if we need to notify any view urls. If we update/insert/remove something from a table used by a view, the view must know.
            ArrayList<Uri> viewUris = getAssociatedViewUris(uri);
            for (Uri viewUri : viewUris) {
                sendNotification(viewUri);

            }

            Uri contentUri = getContentUriFromUri(uri);
            if (contentUri != null) {
                return ContentUris.withAppendedId(contentUri, id);
            } else {
                Logger.logE(LOG_TAG, "ContentUri is null, uri " + uri.toString());
            }
        } else {
            Logger.logE(LOG_TAG, "Database Insert failed, uri " + uri.toString());
        }
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mLocalDatabase.getWritableDatabase();
        String rowId;
        switch (uriMatcher.match(uri)) {
            case SINGLE_SERVER:
                rowId = uri.getPathSegments().get(1);
                selection = BaseColumns._ID + "=" + rowId + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
            default:
                break;
        }

        if (selection == null) {
            selection = DEFAULT_VALUE_STRING;
        }

        int deleteCount = db.delete(getTableNameFromUri(uri), selection, selectionArgs);
        sendNotification(uri);

        // For non-query statements, we also check if we need to notify any view urls. If we update/insert/remove something from a table used by a view, the view must know.
        ArrayList<Uri> viewUris = getAssociatedViewUris(uri);
        for (Uri viewUri : viewUris) {
            sendNotification(viewUri);
        }

        return deleteCount;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String tableName = getTableNameFromUri(uri);
        // Make it safe because UPDATE null ... is not a valid statement
        if (tableName != null) {
            int updateCount = mLocalDatabase.getWritableDatabase().update(tableName, values, selection, selectionArgs);
            sendNotification(uri);

            // For non-query statements, we also check if we need to notify any view urls. If we update/insert/remove something from a table used by a view, the view must know.
            ArrayList<Uri> viewUris = getAssociatedViewUris(uri);
            for (Uri viewUri : viewUris) {
                sendNotification(viewUri);
            }

            return updateCount;
        } else {
            Logger.logE(LOG_TAG, "Can not find a table by uri " + uri.toString());
            return 0;
        }
    }
}
