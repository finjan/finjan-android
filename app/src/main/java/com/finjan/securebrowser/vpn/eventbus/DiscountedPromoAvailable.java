package com.finjan.securebrowser.vpn.eventbus;

import com.finjan.securebrowser.model.RedeemListData;

/**
 * Created by Illia.Klimov on 4/8/2016.
 */
public class DiscountedPromoAvailable {

    private RedeemListData redeemListData;
    public DiscountedPromoAvailable(RedeemListData redeemListData){
        this.redeemListData= redeemListData;
    }

    public RedeemListData getRedeemListData() {
        return redeemListData;
    }
}
