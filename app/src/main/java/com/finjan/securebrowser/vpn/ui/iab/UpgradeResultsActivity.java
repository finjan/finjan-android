/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */
package com.finjan.securebrowser.vpn.ui.iab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.ParentActivity;

/**
 * a simple activity that displays the result of an upgrade attempt instead of a dialog
 *
 * @author ovidiu.buleandra
 * @since 11.12.2015
 */
public class UpgradeResultsActivity extends ParentActivity implements OnClickListener {

    private static final String EXTRA_SUCCESS = "extra_success";
    private static final String EXTRA_MESSAGE = "extra_message";

    private ImageView mCentralImage;
    private TextView mTitle, mDesc;
    private Button mGoBtn;

    private boolean mIsSuccess;

    private static Intent getNewIntent(Context context, boolean isSuccess, String message) {
        Intent newIntent = new Intent(context, UpgradeResultsActivity.class);
        newIntent.putExtra(EXTRA_SUCCESS, isSuccess);
        newIntent.putExtra(EXTRA_MESSAGE, message);
        return newIntent;
    }

    public static void newInstanceForResult(Activity activity, boolean isSuccess, String message, int requestCode) {
        activity.startActivityForResult(getNewIntent(activity, isSuccess, message), requestCode);
    }

    public static void newInstance(Context context, boolean isSuccess, String message) {
        context.startActivity(getNewIntent(context, isSuccess, message));
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        mIsSuccess = getIntent().getBooleanExtra(EXTRA_SUCCESS, true);

        setContentView(R.layout.activity_upgrade_results);
        bindViews();
    }

    private void bindViews() {
        mCentralImage = (ImageView) findViewById(R.id.result_image);
        mTitle = (TextView) findViewById(R.id.title);
        mDesc = (TextView) findViewById(R.id.desc);
        mGoBtn = (Button) findViewById(R.id.btn_go);

        String message = getIntent().getStringExtra(EXTRA_MESSAGE);

//        mCentralImage.setImageResource(mIsSuccess ? R.drawable.congrats : R.drawable.sorry);
        mTitle.setText(mIsSuccess ? R.string.congrats : R.string.sorry);
        mDesc.setText(message);

        mGoBtn.setText(mIsSuccess ? R.string.go : R.string.OK);
        mGoBtn.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        setResult(RESULT_OK);
        finish();
    }
}
