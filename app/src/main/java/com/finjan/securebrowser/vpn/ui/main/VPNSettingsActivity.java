package com.finjan.securebrowser.vpn.ui.main;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.appcompat.widget.SwitchCompat;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.avira.common.activities.ParallaxDashboardActivity;
import com.avira.common.dialogs.AviraDialog;
import com.avira.common.utils.SharedPreferencesUtilities;
import com.finjan.securebrowser.R;
//import com.finjan.securebrowser.a_vpn.controller.mixpanel.Tracking;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.PermissionUtil;

public class VPNSettingsActivity extends ParallaxDashboardActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    public static final String TAG = VPNSettingsActivity.class.getSimpleName();
    private SwitchCompat mSwitchMixPanelStatus;
    private SwitchCompat smartMonitorSwitch;
    public static final String PREFERENCES_ALLOW_TRACKING = "com.avira.vpn.ui.main.VPNSettingsActivity.PREFERENCES_ALLOW_TRACKING";
    private static final int NO_RES = -1;
    private static final int REQ_CODE_LOCATION = 132;
    public static final String NOTIFICATION_PERMISSION = "WIFI_PERNISSION_NOTIFICATION_ONCLICK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();

        boolean isFromNotificationPermission = getIntent().getBooleanExtra(NOTIFICATION_PERMISSION, false);
        Logger.logD(TAG, "onCreate isFromNotificationPermission " + isFromNotificationPermission);
        if (isFromNotificationPermission) {
            Logger.logD(TAG, "opened from notification");
            onSmartMonitorChecked(true);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.settings_enable_mixpanel:
                mSwitchMixPanelStatus.toggle(); // triggers onCheckedChanged
                break;
            case R.id.settings_smart_monitor:
                smartMonitorSwitch.toggle(); // triggers onCheckedChanged
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch ((int) buttonView.getTag()) {
            case R.id.settings_enable_mixpanel:
                SharedPreferencesUtilities.putBoolean(this, PREFERENCES_ALLOW_TRACKING, isChecked);
//                Tracking.trackEvent(isChecked ? Tracking.EVENT_TRACKING_ON : Tracking.EVENT_TRACKING_OFF);
                break;
            case R.id.settings_smart_monitor:
                onSmartMonitorChecked(isChecked);
                break;
        }
    }

    private void onSmartMonitorChecked(boolean isChecked) {
        if (isChecked && !PermissionUtil.hasLocationPermission(this)) {

            smartMonitorSwitch.setChecked(false); // disable until permission has been granted

            if (PermissionUtil.isLocationRequestBlocked(this)) {

                // user clicked "never ask again" previously, so system permission dialog will not be shown
                // we need to guide user to enable permission manually from native Settings
                showGrantLocationManuallyDialog();

            } else {

                // this dialog will lead to system permission dialog
                showSmartMonitorRationaleDialog();
            }

        } else { // has required permission
            enableSmartMonitorSetting(isChecked);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQ_CODE_LOCATION: {
                boolean granted = grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    smartMonitorSwitch.setChecked(true); // triggers nCheckedChanged

                } else {
                    // permission denied
                }
                break;
            }
        }
    }

    /**
     * Method to initialize UI elements
     */
    private void initializeUI() {
        // Configure parallax dashboard
        setParallaxContentLayout(R.layout.vpn_settings_activity_content);
        setParallaxHeaderLayout(R.layout.vpn_settings_activity_header);
        setParallaxBackgroundLayout(R.layout.features_settings_activity_background);
        setContentView(R.layout.parallax_scrollview_dashboard);

        // Setup MixPanel Tracking Switch settings item
        ViewGroup toggleMixpanelLayout = (ViewGroup) findViewById(R.id.settings_enable_mixpanel);
        boolean mixpanelEnabled = SharedPreferencesUtilities.getBoolean(this, PREFERENCES_ALLOW_TRACKING, true);
        mSwitchMixPanelStatus = setupSettingsItem(toggleMixpanelLayout, NO_RES, R.string.txt_mixpanel_settings, mixpanelEnabled);

        // setup smart monitor settings item
        ViewGroup smartMonitorLayout = (ViewGroup) findViewById(R.id.settings_smart_monitor);
        boolean monitorEnabled = FinjanVpnPrefs.isSmartMonitorEnabled(this);
        smartMonitorSwitch = setupSettingsItem(smartMonitorLayout, R.string.smart_monitor, R.string.smart_monitor_desc, monitorEnabled);
    }

    private SwitchCompat setupSettingsItem(ViewGroup settingsLayout, int titleId, int descId, boolean checked) {

        settingsLayout.setOnClickListener(this);
        setText(settingsLayout, R.id.settings_item_text, titleId);
        setText(settingsLayout, R.id.settings_item_desc, descId);

        SwitchCompat settingsSwitch = (SwitchCompat) settingsLayout.findViewById(R.id.settings_item_toggle);
        settingsSwitch.setChecked(checked);
        settingsSwitch.setTag(settingsLayout.getId()); // identify switch by parent layout id
        settingsSwitch.setOnCheckedChangeListener(this);

        return settingsSwitch;
    }

    private void setText(ViewGroup settingsLayout, int textViewId, int textId)
    {
        TextView textView = ((TextView) settingsLayout.findViewById(textViewId));
        if (textId == NO_RES) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setText(textId);
        }
    }

    private void enableSmartMonitorSetting(boolean enabled) {

        if (FinjanVpnPrefs.isSmartMonitorEnabled(this) != enabled) { // prevent tracking duplicate event

            FinjanVpnPrefs.saveSmartMonitorSetting(this, enabled);
//            Tracking.trackSmartMonitorSetting(enabled);
        }
    }

    /**
     * Shows dialog to explain to the user why we require location access permission
     */
    private void showSmartMonitorRationaleDialog() {
        PermissionUtil.showRationaleDialog(this,
                R.string.smart_monitor_rationale_desc, new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        if (v.getId() == R.id.btn_positive) {
                            PermissionUtil.requestLocationPermission(VPNSettingsActivity.this, REQ_CODE_LOCATION);
                        }
                    }
                });
    }

    /**
     * Shows dialog to guide user to enable location access permission for our app manually via native Settings.
     * This dialog is useful when the user clicks on "Never ask again" on the system location permission dialog (causing it to never show again)
     */
    private void showGrantLocationManuallyDialog() {
        new AviraDialog.Builder(this)
                .setTitle(R.string.permission_required)
                .setDesc(R.string.grant_location_permission_desc)
                .setPositiveButton(R.string.goto_settings, R.drawable.settings_icon, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .setCancelable(false)
                .show(getSupportFragmentManager());
    }

    @Override
    protected void onResume() {
        super.onResume();
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,false);
    }

    @Override
    protected void onStop() {
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,true);
        super.onStop();
    }
}
