//package com.finjan.securebrowser.vpn.social;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.util.Log;
//
//import com.facebook.AccessToken;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.FacebookSdk;
//import com.facebook.GraphRequest;
//import com.facebook.GraphResponse;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//import com.facebook.login.widget.LoginButton;
//import com.finjan.securebrowser.R;
//import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
//import com.finjan.securebrowser.helpers.logger.Logger;
//import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
//import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
//import com.finjan.securebrowser.vpn.FinjanVPNApplication;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.Arrays;
//
///**
// * Created by navdeep on 27/2/18.
// */
//
//public class FbSignInHandler {
//    private static final String EMAIL = "email";
//    private CallbackManager callbackManager;
//    private boolean loggedIn = false;
//    private LoginButton loginButton;
//    private Activity activity;
//    private LoginManager loginManager;
//    private SocialUserProfile socialUserProfile;
//
//    public void handle_initialization(Activity activity) {
//        this.activity=activity;
//        FacebookSdk.sdkInitialize(FinjanVPNApplication.getInstance().getApplicationContext());
//        callbackManager = CallbackManager.Factory.create();
//        loggedIn = AccessToken.getCurrentAccessToken() == null;
//    }
//
//    public interface FBResultListener{
//        void onResult(SocialUserProfile socialUserProfiles, boolean success, String msg);
//    }
//
//    public void onClickSignInButton(LoginButton loginButton,final FBResultListener listener) {
//
////        LocalyticsTrackers.Companion.setEFacebook_login();
//        Logger.logE("Fb", "login clicked...." + loggedIn + loginButton);
//        LoginManager.getInstance().logInWithReadPermissions(loginButton.getFragment(),
//                Arrays.asList("public_profile", "email"));
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Logger.logE("fb", "log in successful");
//                getUserDetails(loginResult,listener);
//            }
//
//            @Override
//            public void onCancel() {
//                listener.onResult(null,false, ResourceHelper.getInstance().getString(R.string.user_cancel_authentication));
//                // App code
//                Logger.logE("fb", "cancelled......");
//            }
//
//            @Override
//            public void onError(FacebookException exception) {
//                if(!TextUtils.isEmpty(exception.getMessage())){
//                    listener.onResult(null,false, exception.getMessage());
//                }else {
//                    listener.onResult(null,false, ResourceHelper.getInstance().getString(R.string.error_while_parsing_user_detail));
//                }
//                // App code
//                Logger.logE("fb", "error" + exception);
//            }
//        });
//    }
//
//    public void performTaskOnActivityResult(int requestCode, int resultCode, Intent data) {
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//    }
//
//    private void getUserDetails(final LoginResult loginResult, final FBResultListener listener) {
//        GraphRequest data_request = GraphRequest.newMeRequest(
//                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject object, GraphResponse response) {
//                        try {
//                            socialUserProfile = getFacebookData(object,loginResult.getAccessToken().getToken(),loginResult.getAccessToken().getUserId());
//                            listener.onResult(socialUserProfile,true,"");
//                        } catch (Exception e) {
//                            listener.onResult(socialUserProfile,false,FinjanVPNApplication.getInstance().getString(R.string.error_while_parsing_user_detail));
//                            e.printStackTrace();
//                        }
//                    }
//
//                });
//        Bundle permission_param = new Bundle();
//        permission_param.putString("fields", "id,name,email");
//        data_request.setParameters(permission_param);
//        data_request.executeAsync();
//    }
//
//    private SocialUserProfile getFacebookData(JSONObject object,
//                                              String token,String userId) {
//        String fbEmail = "", fbId, fname = "", lname = "";
//        try {
//            String id = object.getString("id");
//
//            fbEmail = object.getString("email");
//            fbId = object.getString("id");
//            if (object.has("first_name"))
//                fname = object.getString("first_name");
//            if (object.has("last_name"))
//                lname = object.getString("last_name");
//            if(object.has("name")){
//                fname=object.getString("name");
//            }
//            if(TextUtils.isEmpty(fbEmail)){
//                fbEmail=userId+"@facebook-social.com";
//            }
//            socialUserProfile=new SocialUserProfile();
//            socialUserProfile.setSocialType("fb_id");
//
//            socialUserProfile.setSocialId(userId);
////            socialUserProfile.setSocialId("429552714162272");
//            socialUserProfile.setToken_id(token);
//            socialUserProfile.setFirstName(fname);
//            socialUserProfile.setLastName(lname);
//            socialUserProfile.setEmail(fbEmail);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return socialUserProfile;
//    }
//}
