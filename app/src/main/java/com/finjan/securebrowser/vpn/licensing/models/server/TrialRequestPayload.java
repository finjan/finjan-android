package com.finjan.securebrowser.vpn.licensing.models.server;

import android.content.Context;

import com.avira.common.backend.models.BasePayload;

/**
 * @author ovidiu.buleandra
 * @since 23.11.2015
 */
public class TrialRequestPayload extends BasePayload {

    public TrialRequestPayload(Context context, String productAcronym) {
        super(context);

        info.addLicenseType("eval");

        id.addDeviceId();
        id.setAppId(productAcronym); // make sure the correct Application Id is in the request
    }
}
