/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.licensing.models.server;

import android.content.Context;

import com.avira.common.backend.models.BasePayload;
import com.avira.common.backend.models.Info;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.models.billing.SkuDetails;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper;
import com.finjan.securebrowser.vpn.licensing.utils.SubscriptionVPN;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovidiu.buleandra
 * @since 04.11.2015
 */
public class ProcessPurchasePayload extends BasePayload {

    @SerializedName("_debugPurchase")
    private Integer debugPurchase;

    public ProcessPurchasePayload(Context context, Purchase purchase, SkuDetails skuDetails,
                                  String productAcronym) {
        this(context, purchase, skuDetails, productAcronym, null);
    }

    public ProcessPurchasePayload(Context context, Purchase purchase, SkuDetails skuDetails,
                                  String productAcronym, SubscriptionVPN subscriptionVPN) {
        super(context);

        info = new Info(); // delete the part from
        String order_id = purchase.getOrderId();
        if (order_id != null && order_id.length() > 0) {
            info.addOrderId(order_id);
            info.setIsTest(false);
        } else {
            info.setIsTest(true);
        }
        info.addPackageName(purchase.getPackageName());
        info.addProductId(purchase.getSku());
        info.addPurchaseTime(purchase.getPurchaseTime());
        info.addPurchaseState(purchase.getPurchaseState());
        info.addPurchaseToken(purchase.getToken());

        info.addDeveloperPayload(purchase.getDeveloperPayload());

        info.addPrice(skuDetails.getPriceValue());
        info.addCurrency(skuDetails.getCurrencyCode());

        if (subscriptionVPN != null) {
            info.addSubscriptionType(subscriptionVPN.getSubscriptionType());
            info.addRuntime(subscriptionVPN.getRuntime());
            info.addPurchaseType("subscriptions");
        } else {
            // do not change the strings => backend uses them when making in app billing calls to google
            info.addPurchaseType(purchase.getItemType().equals(IabHelper.ITEM_TYPE_INAPP)
                    ? "products"
                    : "subscriptions");
        }

        id.addDeviceId();
        id.setAppId(productAcronym); // make sure the correct Application Id is in the request
    }

    public void debug(int requestErrorCode) {
        debugPurchase = requestErrorCode;
    }
}
