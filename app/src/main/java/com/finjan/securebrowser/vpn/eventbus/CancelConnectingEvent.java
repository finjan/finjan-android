package com.finjan.securebrowser.vpn.eventbus;

/**
 * this event needs only to stop VPN connection if user decided to cancel connection process
 */
public class CancelConnectingEvent {
}
