package com.finjan.securebrowser.vpn.auto_connect

import android.annotation.TargetApi
import android.app.*
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.preference.PreferenceManager
import androidx.core.app.NotificationCompat
import android.text.TextUtils
import android.widget.Toast
import com.android.volley.VolleyError
import com.avira.common.id.HardwareId
import com.avira.common.utils.HashUtility
import com.finjan.securebrowser.R
import com.finjan.securebrowser.activity.SplashScreen
import com.finjan.securebrowser.activity.TutorialActivity
import com.finjan.securebrowser.helpers.AuthenticationHelper
import com.finjan.securebrowser.helpers.NativeHelper
import com.finjan.securebrowser.helpers.PlistHelper
import com.finjan.securebrowser.helpers.ToastHelper
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper
import com.finjan.securebrowser.helpers.logger.Logger
import com.finjan.securebrowser.helpers.security.WifiSecurityHelper
import com.finjan.securebrowser.helpers.sharedpref.UserPref
import com.finjan.securebrowser.jobScheduler.ScheduleJobService
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers
import com.finjan.securebrowser.util.NotificationHandler
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder
import com.finjan.securebrowser.vpn.FinjanVPNApplication
import com.finjan.securebrowser.vpn.controller.JsonConfigParser
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient
import com.finjan.securebrowser.vpn.controller.database.DatabaseContract
import com.finjan.securebrowser.vpn.controller.network.NetworkManager
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener
import com.finjan.securebrowser.vpn.eventbus.LicenceFetched
import com.finjan.securebrowser.vpn.service.AviraOpenVpnService
import com.finjan.securebrowser.vpn.ui.authentication.SetupAccountActivity
import com.finjan.securebrowser.vpn.ui.main.VpnActivity
import com.finjan.securebrowser.vpn.ui.main.VpnApisHelper
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper
import com.finjan.securebrowser.vpn.util.AutoClose
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs
import com.finjan.securebrowser.vpn.util.TrafficController
import com.finjan.securebrowser.vpn.util.VpnUtil
import com.finjan.securebrowser.vpn.util.log.FileLogger
import de.blinkt.openvpn.VpnProfile
import de.blinkt.openvpn.core.ProfileManager
import de.blinkt.openvpn.core.VpnStatus
import de.greenrobot.event.EventBus
import org.jetbrains.annotations.NotNull
import org.json.JSONObject
import java.util.HashMap

class AutoConnectHelper:VpnStatus.ByteCountListener {


    var currentSsid:String = ""
    override fun updateByteCount(`in`: Long, out: Long, diffIn: Long, diffOut: Long) {
        val trafficController = TrafficController.getInstance(FinjanVPNApplication.getInstance())
        val traffic = `in` + out + trafficController.traffic
//            final long  traffic = in + trafficController.getTraffic();
        val trafficLimit = trafficController.trafficLimit
    }
    val TAG = AutoConnectHelper::class.java.simpleName
    var isUserDisconnectedAutoConnection: Boolean = false


    companion object {

        public var startedAutoConnecting = false

        private object HOLDER {
            var instance = AutoConnectHelper()
        }
        var isAutoConnection: Boolean = false
        val instance: AutoConnectHelper by lazy { HOLDER.instance }
    }
    var currentContext:Context? = null

    private fun getMainActivityPendingIntent(context: Context): PendingIntent {
        // Let the configure Button show the Log
        val intent = Intent(context, VpnActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        return PendingIntent.getActivity(context, 0, intent, 0)
    }
    fun createAutoConnectNofication(msg:String,msgIfNotCalledByService:String){
        if(jobService== null){
//            ToastHelper.showToast(msgIfNotCalledByService, Toast.LENGTH_SHORT)
            return
        }
        val ns = Context.NOTIFICATION_SERVICE
        val mNotificationManager = jobService!!.getSystemService(ns) as NotificationManager?
        val icon = R.drawable.notification_icon

        val channelName = "Invincibill VPN"
        val channelId = "com.invincibull.vpn"

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)

            mNotificationManager?.createNotificationChannel(mChannel)
        }

        val nbuilder = NotificationCompat.Builder(jobService!!,channelId)
        nbuilder.setContentTitle(ResourceHelper.getInstance().getString(R.string.notifcation_title_notconnect))
//            nbuilder.setContentTitle(ResourceHelper.getInstance().getString(R.string.you_are_in_public_wifi_msg))

        nbuilder.setContentText(msg)
        nbuilder.setStyle(NotificationCompat.BigTextStyle().bigText(msg))
        nbuilder.setLargeIcon(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(FinjanVPNApplication.getInstance().getResources(),
                R.mipmap.app_icon), 128, 128, false))
        nbuilder.setOnlyAlertOnce(true)
        nbuilder.setOngoing(false)
        nbuilder.setAutoCancel(true)
        nbuilder.setContentIntent(getMainActivityPendingIntent(jobService!!.baseContext))
        nbuilder.setSmallIcon(icon)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            lpNotificationExtras(nbuilder)

        val notification = nbuilder.build()


        mNotificationManager!!.notify(1, notification)
        jobService?.startForeground(1, notification)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun lpNotificationExtras(nbuilder: NotificationCompat.Builder) {
        nbuilder.setCategory(Notification.CATEGORY_SERVICE)
        nbuilder.setLocalOnly(true)

    }


    fun onManuallDisconnectAutoConnect(context: Context,vpnHomeFragment: VpnHomeFragment){
        /* functionality changed to just send auto protect alert
         val ssid = WifiSecurityHelper.getInstance().getSsid(context)
        val title = String.format(PlistHelper.getInstance().autoConnect_title, ssid)
        val msg = String.format(PlistHelper.getInstance().autoConnect_msg)
        FinjanDialogBuilder(context)
                .setMessage(msg)
                .setTitle(title)
                .setNegativeButton(PlistHelper.getInstance().autoConnect_n_btn)
                .setPositiveButton(PlistHelper.getInstance().autoConnect_p_btn)
                .setDialogClickListener(FinjanDialogBuilder.NegativeClickListener {  })
                .setDialogClickListener(FinjanDialogBuilder.PositiveClickListener {
                    UserPref.getInstance().addTrustedNetwork(ssid)
                    vpnHomeFragment.onClickVpnButton()
                }).show()
           LocalyticsTrackers.setVpnOffWithAutoProtect();
         */

        UserPref.getInstance().autoConnect = false

        val map = HashMap<String, String>()
        map["value"] = "off"
        LocalyticsTrackers.setAuto_Protect_Alert(map)
    }

    private var jobService:ScheduleJobService ?= null
    fun whenStatusChanged(context: Context){
        if(WifiSecurityHelper.getInstance().isWifiNetwork(context)){
            if (!startedAutoConnecting && UserPref.getInstance().autoConnect &&
                    UserPref.getInstance().isTutorialScreenShowed &&
                    !AutoConnectHelper.instance.checkIfTrusted(context)) {
                if(context is ScheduleJobService){
                    jobService = context
                }
                startAutoConnect(context)
            }
        }
    }

    fun checkIfTrusted(@NotNull context: Context):Boolean{
        this@AutoConnectHelper.currentSsid = WifiSecurityHelper.getInstance().getSsid(context)
        if(TextUtils.isEmpty(currentSsid)){
            ToastHelper.showToast("ssid is null ", Toast.LENGTH_SHORT)
            return false
        }
        return UserPref.getInstance().trustedNetworks.contains(currentSsid)
    }

    var waitingHomeForAutoConnect = false

    private fun startAutoConnect(@NotNull context: Context){
        if(NativeHelper.getInstnace().currentAcitvity!=null && (NativeHelper.getInstnace().currentAcitvity is SplashScreen
                        || NativeHelper.getInstnace().currentAcitvity is TutorialActivity)){
            waitingHomeForAutoConnect= true
            return
        }
        currentContext = context
        startedAutoConnecting = true
        Handler().postDelayed({ startedAutoConnecting =false},3000)
        initVpnIfNot(context)
        makeLocalyticsEvent(context)
    }
    private fun initVpnIfNot(@NotNull context: Context){

        licenFailedCount = 0
        if(!(NativeHelper.getInstnace().checkActivity(VpnActivity.getInstance()) && VpnActivity.getInstance().vpnHomeFragment.isAdded)){
            val intent = Intent(context, AviraOpenVpnService::class.java)
            intent.action = AviraOpenVpnService.START_SERVICE
            context.bindService(intent, mConnection, Context.BIND_AUTO_CREATE)
            onResumeRefresh(context)
        }else if(!SetupAccountActivity.isSetupAccountLive){
            if(VpnActivity.getInstance().vpnHomeFragment.isAdded
                 && !isVpnConnected() && !isUserDisconnectedAutoConnection){
                VpnActivity.getInstance().vpnHomeFragment.clickVPNBtnOnUi()
            }
        }
    }

    private lateinit var mSelectedProfile:VpnProfile
    fun initVpn(context: Context,key:String){

            // we got called to be the starting point, most likely a shortcut
            val shortcutUUID = key
            val shortcutName = ""

            var profileToConnect: VpnProfile? = ProfileManager.get(context, shortcutUUID)
            if (shortcutName != null && profileToConnect == null)
                profileToConnect = ProfileManager.getInstance(context).getProfileByName(shortcutName)

            if (profileToConnect == null) {
                Logger.logE(TAG, "unable to retrieve profile to connect")
                // show Log window to display error
                return
            }

            mSelectedProfile = profileToConnect
            launchVPN(context)
    }

    private fun launchVPN(context: Context){
        if(UserPref.getInstance().isUserHadGivenVPNPermission){
            UserPref.getInstance().isUserHadGivenVPNPermission(true)
            val hardwareId = HashUtility.sha1(HardwareId.get(context))
            mSelectedProfile.mUsername = hardwareId
            val trafficController = TrafficController.getInstance(context)
            if (trafficController.isRegistered) {
                mSelectedProfile.mTransientPW = FinjanVpnPrefs.getAuthToken(FinjanVPNApplication.getInstance())
                //                    mSelectedProfile.mTransientPW = UserPref.getInstance().getCurrent_Access_Token();
            } else {
                mSelectedProfile.mTransientPW = hardwareId
            }
            ToastHelper.showToast(jobService,ResourceHelper.getInstance().getString(R.string.Auto_connecting_VPN),Toast.LENGTH_SHORT)
            startOpenVpnThread().start()
        }
    }

    private inner class startOpenVpnThread : Thread() {
        override fun run() {


            try{
                if(jobService!=null){
                    val startVPN = mSelectedProfile.prepareStartService(jobService)
                    startVPN.setClass(jobService?.baseContext, AviraOpenVpnService::class.java)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        jobService?.baseContext?.startForegroundService(startVPN)
                    }else
                    {
                        jobService?.baseContext?.startService(startVPN)
                    }
                }else if (FinjanVPNApplication.getInstance() !=null){
                    val startVPN = mSelectedProfile.prepareStartService(FinjanVPNApplication.getInstance())
                    startVPN.setClass(FinjanVPNApplication.getInstance(), AviraOpenVpnService::class.java)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        FinjanVPNApplication.getInstance().startForegroundService(startVPN)
                    }else
                    {
                        FinjanVPNApplication.getInstance().startService(startVPN)
                    }
                }

            }catch (expection:IllegalStateException){
                createAutoConnectNofication(
                        ResourceHelper.getInstance().getString(R.string.you_are_in_public_wifi_msg),
                        ResourceHelper.getInstance().getString(R.string.you_are_in_public_wifi_msg))
            }catch (exception: Exception){
                createAutoConnectNofication(
                        ResourceHelper.getInstance().getString(R.string.you_are_in_public_wifi_msg),
                        ResourceHelper.getInstance().getString(R.string.you_are_in_public_wifi_msg))
            }

        }

    }


    private fun runVPN(context: Context) {
        var serverVpnId = getDefaultServerVpnId(context)
        FinjanVpnPrefs.saveLastUsedServer(FinjanVPNApplication.getInstance(), mSelectedServerRegion)
        val isRunned = VpnUtil.startVpnConnection(context,serverVpnId,true)
        isAutoConnection=true

    }

    private var mSelectedServerId:Long = 0
    private var mSelectedServerRegion:String= ""
    private fun getDefaultServerVpnId(context: Context): Long {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val defaultServer = prefs.getString(JsonConfigParser.DEFAULT_SERVER, null)

        if (!TextUtils.isEmpty(defaultServer)) {
            val contentProviderClient = ContentProviderClient()
            val cursor = contentProviderClient.getServer(context, defaultServer)
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    mSelectedServerRegion = defaultServer
                    mSelectedServerId = cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID))
                }
                AutoClose.closeSilently(cursor)
            }
        }
        return mSelectedServerId
    }
    private fun makeVpnConnection(@NotNull context: Context){
        if(AviraOpenVpnService.noProcessRunning){
            createAutoConnectNofication(
                    ResourceHelper.getInstance().getString(R.string.you_are_in_public_wifi_msg),
                    ResourceHelper.getInstance().getString(R.string.you_are_in_public_wifi_msg))
            return
        }
        if(isVpnConnected() || isVpnConnecting() || VpnApisHelper.instance.shouldAutoLogin(context)){return}
        runVPN(context)
    }
    private fun makeLocalyticsEvent(@NotNull context: Context){

    }

    private fun onResumeRefresh(@NotNull context: Context) {
        autoLoginIfRequested(context)
//        updateTrafficUI(context)
    }

    private fun isVpnConnecting(): Boolean {
        return mService != null  && (mService?.getConnectionStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTING_SERVER_REPLIED
                || mService?.getConnectionStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTING_NO_SERVER_REPLY_YET)
    }

    fun isVpnConnected(): Boolean {
        return mService != null && mService?.getConnectionStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTED
    }


    private fun updateTrafficUI(@NotNull context: Context) {
        if (!isVpnConnecting()) {
            val trafficController = TrafficController.getInstance(FinjanVPNApplication.getInstance())
            val traffic = trafficController.traffic + trafficController.newTraffic
            val trafficLimit = trafficController.trafficLimit
            // do not update if its first time run and no traffic consumed yet
            if (traffic != 0L || trafficLimit != 0L || trafficController.isRegistered) {
                checkIfLimitReached(context,traffic, trafficLimit)
                if(!isReachedTrafficLimit){makeVpnConnection(context)}
            }
        }
    }

    protected fun autoLoginIfRequested(@NotNull context: Context) {
        if (VpnApisHelper.instance.shouldAutoLogin(context)) {
            VpnActivity.UserLogout = false
            AuthenticationHelper.getInstnace().initRefreshToken {

                if(it){
                    NetworkManager.getInstance(context).fetchAviraLicence(context, object : NetworkResultListener {
                        override fun executeOnSuccess(response: JSONObject) {
                           // runVPN(FinjanVPNApplication.getInstance(), mSelectedServerId)
                        }

                        override fun executeOnError(error: VolleyError) {
                            //runVPN(FinjanVPNApplication.getInstance(), mSelectedServerId)
                        }
                    })
                  //  VpnApisHelper.instance.updateLicence(false,true,context,networkResultListener)
                }else{
                    VpnHomeFragment.preLogoutPref(context)
                    Toast.makeText(context,ResourceHelper.getInstance().getString(R.string.user_session_expired),Toast.LENGTH_SHORT).show()
                }
            }
        } else {
           // VpnApisHelper.instance.updateLicence(false,true,context,networkResultListener)
            NetworkManager.getInstance(context).fetchAviraLicence(context, object : NetworkResultListener {
                override fun executeOnSuccess(response: JSONObject) {
                    // runVPN(FinjanVPNApplication.getInstance(), mSelectedServerId)
                }

                override fun executeOnError(error: VolleyError) {
                    //runVPN(FinjanVPNApplication.getInstance(), mSelectedServerId)
                }
            })
        }
    }

    val networkResultListener= object : NetworkResultListener {
        override fun executeOnError(error: VolleyError?) {

        }

        override fun executeOnSuccess(response: JSONObject?) {
            if(currentContext!=null){
                updateTrafficUI(currentContext!!)
                if(NativeHelper.getInstnace().currentAcitvity != null && NativeHelper.getInstnace().currentAcitvity is VpnActivity){
                    EventBus.getDefault().post(LicenceFetched(response,true))
                }
            }
        }
    }

    protected var mService: AviraOpenVpnService? = null
    private val mConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as AviraOpenVpnService.LocalBinder
            mService = binder.service
            if (mService != null) {
                // check did user press disconnect button via notification

            }
        }


        override fun onServiceDisconnected(arg0: ComponentName) {
            mService = null
        }

    }

    public fun reachedTrafficLimit(@NotNull context: Context) {
        if (isVpnConnected()) {
            VpnUtil.stopVpnConnection(context, mService)
        }

        if (!isVpnConnected() && !isVpnConnecting()) {
           //show toast of disconnecting
        }
    }

    private  var isReachedTrafficLimit =false
    private  var isLowDataReached =true
    private fun checkIfLimitReached(@NotNull context: Context,
                                    traffic: Long, trafficLimit: Long) {
        if (trafficLimit > 0) {
            var percentage = traffic * 100.0 / trafficLimit
            percentage = Math.floor(percentage * 100) / 100
            isReachedTrafficLimit = false


            try {
                if (percentage >=/*(100- Double.parseDouble(PlistHelper.getInstance().getData_tank_min()))*/ 0 && isLowDataReached) {
                    if (PromoHelper.instance.lowDataPromoActivation(context)) {
                        isLowDataReached = false
                        Logger.logE("low_data", "Activated")
                    } else {
                        isLowDataReached = false
                        Logger.logE("low_data", "unavailable")
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }


            if (percentage >= 100) {
                isReachedTrafficLimit = true
                reachedTrafficLimit(context)
            }

        } else {
            isReachedTrafficLimit = false
        }
    }


    private inner class FetchTrafficConsumption : NetworkResultListener {

        override fun executeOnSuccess(response: JSONObject) {
            if (TextUtils.isEmpty(UserPref.getInstance().tempPurchaseToken)) {
                JsonConfigParser.parseJsonTrafficConsumption(FinjanVPNApplication.getInstance(), response)
            }
        }

        override fun executeOnError(error: VolleyError) {
            error.printStackTrace()
        }
    }

    private var licenFailedCount = 0
}