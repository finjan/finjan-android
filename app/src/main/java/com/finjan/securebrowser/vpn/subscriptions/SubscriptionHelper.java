package com.finjan.securebrowser.vpn.subscriptions;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.avira.common.backend.WebUtility;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.custom_exceptions.ULMSubscriptionCreatedNonFetal;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.jobScheduler.ScheduleHitConfig;
import com.finjan.securebrowser.model.FinjanUser;
import com.finjan.securebrowser.model.RedeemListData;
import com.finjan.securebrowser.tracking.facebook.FbTrackingConfig;
import com.finjan.securebrowser.tracking.kochava.KochavaTracking;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.tracking.mixpanel.SubscriptionTrack;
import com.finjan.securebrowser.tracking.tune.TuneTracking;
import com.finjan.securebrowser.util.AndroidPublisherValidation;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.JsonConfigParser;
import com.finjan.securebrowser.vpn.controller.JsonCreater;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.vpn.eventbus.AllSubscFetched;
import com.finjan.securebrowser.vpn.eventbus.CancelledsubsFetched;
import com.finjan.securebrowser.vpn.eventbus.SubscriptionUpdated;
import com.finjan.securebrowser.vpn.helpers.JsonConstans;
import com.finjan.securebrowser.vpn.licensing.models.billing.IabResult;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper;
import com.finjan.securebrowser.vpn.licensing.utils.PurchaseHelper;
import com.finjan.securebrowser.vpn.mixpanel.Tracking;
import com.finjan.securebrowser.vpn.ui.iab.LicenseUtil;
import com.finjan.securebrowser.vpn.ui.main.VpnApisHelper;
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment;
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
//import com.localytics.android.Localytics;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;

import static com.finjan.securebrowser.helpers.ScreenNavigation.VPN_SCREEN;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *
 * Created by anurag on 14/07/17.
 *
 *
 * Class that is responsible for creating,Renewing and Cancelling
 * we are not modifying Subscription on ULM from anywhere else except this class.
 *
 */

public class SubscriptionHelper extends JsonConstans {

    private static final String RUNNING = "running";
    private static final String CANCELLED = "cancelled";
    private static final String TAG = SubscriptionHelper.class.getSimpleName();
    private static final String ScheduleHItMsg = "We are updating ur earlier purchase, Soon you licence will update too";
    public boolean subscriptionFetched,updatingSubs;
//    private HashMap<String,Subscriptions> subscriptionsHashMap;
    private HashMap<String, ArrayList<Subscriptions>> subscriptionsHashMap = new HashMap<>();
    private HashMap<String, ArrayList<Subscriptions>> cancelledSubscriptionsHashMap = new HashMap<>();

    public HashMap<String, ArrayList<Subscriptions>> getAllPromoSubscriptionsHashMap() {
        return allPromoSubscriptionsHashMap;
    }

    public void clearSubscriptionData()
    {
        subscriptionsHashMap.clear();
    }

    private HashMap<String, ArrayList<Subscriptions>> allPromoSubscriptionsHashMap =new HashMap<>();


    public static SubscriptionHelper getInstance() {
        return SubsCriptionHelperHolder.Instance;
    }

    public static String getGoogleUrlForVarification(Subscriptions subscriptions) {
        String url = "https://www.googleapis.com/androidpublisher/v2/applications/com.finjan.securebrowser" +
                "/purchases/subscriptions/" + subscriptions.getSku_id() + "/tokens/" +
                subscriptions.getSubscription_id() + "?access_token=mkfkxtpuu8e4lpjrorjmoac5ldqngktwdnifm6a8";
        return url;
    }

    public void logout(){
        isCanceledSubscriptionFetched =false;
        subscriptionFetched =false;
        subscriptionsHashMap.clear();
        cancelledSubscriptionsHashMap.clear();
        allPromoSubscriptionsHashMap.clear();

    }
    public boolean ifSubscriptionIsFetched() {
        return subscriptionFetched;
    }

    public void setSubscriptionFetched(boolean subscriptionFetched) {
        this.subscriptionFetched = subscriptionFetched;
    }

    public HashMap<String, ArrayList<Subscriptions>> getSubscriptionsHashMap() {
        return subscriptionsHashMap;
    }

    private boolean isCanceledSubscriptionFetched = false;
    private boolean isCanceledSubscriptionFetching = false;
//    public void setIsCanceledSubscriptionFetched(boolean isCanceledSubscriptionFetched){
//        this.isCanceledSubscriptionFetched= isCanceledSubscriptionFetched;
//    }

    public boolean isCanceledSubscriptionFetched() {
        return isCanceledSubscriptionFetched;
    }

    public ArrayList<Subscriptions> getAllCanceledSubscriptionsList(Context context,boolean forcedFetch) {
        if(isCanceledSubscriptionFetched){
            return cancelledSubscriptionsHashMap.get(PROMO);
        }
        getAllSubsCancelled(context,forcedFetch);
        return null;
    }

    public void setSubscriptionsHashMap(HashMap<String, ArrayList<Subscriptions>> subscriptionsHashMap) {
        this.subscriptionsHashMap = subscriptionsHashMap;
    }
    public void setAllPromoSubscriptionsHashMap(HashMap<String, ArrayList<Subscriptions>> subscriptionsHashMap) {
        this.allPromoSubscriptionsHashMap= subscriptionsHashMap;
    }

    public void setAllCancelledSubscriptionsHashMap(HashMap<String, ArrayList<Subscriptions>> subscriptionsHashMap) {
        this.cancelledSubscriptionsHashMap= subscriptionsHashMap;
    }

    public void initRequiredSubsUpdation(Context context, boolean isToServerHit) {
        if (TrafficController.getInstance(context).isPaid()) {
            SubscriptionHelper.getInstance().setSubscriptionFetched(true);
            updatingSubs = true;
            getAllSubs(context,isToServerHit);
        }
    }

    private void getAllSubs(final Context context, boolean isToserverHit) {

        NetworkManager.getInstance(context).fetchSubs(isToserverHit,context, new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                JsonConfigParser.parseJsonAllSubs(response,true,false);
                EventBus.getDefault().post(new AllSubscFetched());
                getMyDeviceSubs(context);
                updatingSubs = false;
            }

            @Override
            public void executeOnError(VolleyError error) {
                EventBus.getDefault().post(new AllSubscFetched());
                NativeHelper.getInstnace().showToast(context, error);
                updatingSubs = false;
            }
        });
    }
    public void getAllSubsCancelled(final Context context,boolean forcedFetch) {
        if(isCanceledSubscriptionFetching){
            return;
        }
        isCanceledSubscriptionFetching =true;
        NetworkManager.getInstance(context).fetchCancelledSubs(context, new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                JsonConfigParser.parseJsonAllSubs(response,true,true);
                isCanceledSubscriptionFetched =true;
                EventBus.getDefault().post(new CancelledsubsFetched());
//                PromoHelper.Companion.getInstance().checkForUserValidity(context);
                isCanceledSubscriptionFetching =false;
            }

            @Override
            public void executeOnError(VolleyError error) {
                String msg = WebUtility.getMessage(error);
                if(!TextUtils.isEmpty(msg) && msg.contains(":404")){
                    cancelledSubscriptionsHashMap.put(PROMO,new ArrayList<Subscriptions>());
                }
                isCanceledSubscriptionFetched =true;
                EventBus.getDefault().post(new CancelledsubsFetched());
                isCanceledSubscriptionFetching =false;
            }
        },forcedFetch);
    }


    private void getMyDeviceSubs(final Context context) {
        if (subscriptionsHashMap != null) {

            final ArrayList<Subscriptions> subscriptionsArrayList = subscriptionsHashMap.get(GOOGLE);
            final ArrayList<Subscriptions> promoSubscriptionsArrayList = subscriptionsHashMap.get(PROMO);
            if (subscriptionsArrayList == null && promoSubscriptionsArrayList== null) {
                return;
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if(subscriptionsArrayList!=null){
                        for (Subscriptions subscriptions : subscriptionsArrayList) {
                            if (subscriptions != null) {
                                if (!subscriptions.getSubscription_id().endsWith(Purchase.tempPurchase)) {
                                    updateIfRequired(context,
                                            AndroidPublisherValidation.getInstance()
                                                    .getPurchase(context, subscriptions.getSku_id(), subscriptions.getSubscription_id()),
                                            subscriptions);
                                } else if(subscriptions.getStatus().equals(RUNNING)){
                                    updateSubs(context, subscriptions.getSubscription_id(), null, subscriptions.getSku_id());
                                }
                            }
                        }
                    }
                    if(AppConfig.Companion.getPromo_offer() &&  promoSubscriptionsArrayList!=null){
                        shouldCancelPromo(context);
                    }
                }
            }).start();
        }
    }
    private ArrayList<RedeemListData> redeemListDataArrayList;
    public void updateRedeemList(ArrayList<RedeemListData> redeemListDataArrayList){
        this.redeemListDataArrayList = redeemListDataArrayList;
    }

    public void cancelAllPromos(final Context context, final boolean isToshowPopUpOfEndPromo){
        NetworkManager.getInstance(context).fetchAllPromoSubs(context, new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                VpnHomeFragment.isToHitCanclePromo = false;
                JsonConfigParser.parseJsonAllSubs(response,false,false);
                if(allPromoSubscriptionsHashMap.size()>0){
                    if (subscriptionsHashMap!= null && subscriptionsHashMap.size()>0) {
                        final ArrayList<Subscriptions> promoSubscriptionsArrayList = subscriptionsHashMap.get(PROMO);
                        if (promoSubscriptionsArrayList!=null) {
                            for (Subscriptions subscription : promoSubscriptionsArrayList) {
                                updatePromoSubs(context, subscription.getSubscription_id(), subscription.getComp_flg(),isToshowPopUpOfEndPromo);
                            }
                        }
                    }
                }
            }

            @Override
            public void executeOnError(VolleyError error) {

            }
        });
    }

    private void shouldCancelPromo(Context context){
            if(subscriptionsHashMap!=null){
                final ArrayList<Subscriptions> subscriptionsArrayList = subscriptionsHashMap.get(PROMO);

                if(subscriptionsArrayList!= null && subscriptionsArrayList.size()>0){
                    for (Subscriptions subscription: subscriptionsArrayList){

                        if(com.electrolyte.utils.DateHelper.getInstnace().isNotMoreThan1DayDiff(
                                PromoHelper.Companion.getInstance().getCurrentDateStr(),
                                subscription.getExpires_at()
                        )){
                            PromoHelper.Companion.getInstance().onOneDayleftToEndPromo(subscription.getComp_flg());
                        }
                        if(!com.electrolyte.utils.DateHelper.getInstnace().isFutureDate(com.electrolyte.utils.DateHelper.getInstnace().getDateWithoutTime(subscription.getExpires_at()),
                                com.electrolyte.utils.DateHelper.getInstnace().getDateWithoutTime(PromoHelper.Companion.getInstance().getCurrentDateStr()))){
                            updatePromoSubs(context,subscription.getSubscription_id(),subscription.getComp_flg(),true);
                        }
                    }
                }
            }
    }


    private void updateIfRequired(Context context, SubscriptionPurchase purchase, Subscriptions backendSubs) {
        if (purchase == null) {
            Logger.logE(TAG, "purchase is null i.e. ");
            return;
        }
        if ((AndroidPublisherValidation.getInstance().isSubsCancelled(purchase) ||
                AndroidPublisherValidation.getInstance().isSubsExpires(purchase))
                && RUNNING.equalsIgnoreCase(backendSubs.getStatus())) {
            updateSubs(context, backendSubs.getSubscription_id(), null, backendSubs.getSku_id());
            return;
        }
        if (shouldRenewed(purchase, backendSubs)) {
            updateSubs(context, backendSubs.getSubscription_id(), purchase, backendSubs.getSku_id());
            createRenewSubs(context,backendSubs.getSku_id(),backendSubs.getSubscription_id(),
                    purchase, AuthenticationHelper.getInstnace().getUserName(context));
        }
    }

    private boolean shouldRenewed(SubscriptionPurchase pur, Subscriptions backendSubs) {
        if (backendSubs.getRenewed_at().equals("null") || TextUtils.isEmpty(backendSubs.getRenewed_at())) {
            if (DateHelper.getInstnace().isApproxSameTime(pur.getStartTimeMillis(), backendSubs.getCreated_at())
//                    && DateHelper.getInstnace().isFutureDate(pur.getExpiryTimeMillis(), DateHelper.getInstnace().getLongFromDate(backendSubs.getExpires_at()))) {
                    && DateHelper.getInstnace().isMoreThan3DaysDiff(pur.getExpiryTimeMillis(), backendSubs.getExpires_at())) {
                return true;
            }
        } else {
            if (DateHelper.getInstnace().isFutureDate(pur.getExpiryTimeMillis(), DateHelper.getInstnace().getLongFromDate(backendSubs.getExpires_at()))
                    && DateHelper.getInstnace().isMoreThan3DaysDiff(pur.getExpiryTimeMillis(), backendSubs.getExpires_at())) {
                return true;
            }
        }
        return false;
    }

    private void trackRenewSubs(String sku) {
        if (!TextUtils.isEmpty(sku)) {
            SubscriptionTrack attr = new SubscriptionTrack();
            attr.putProperty(Tracking.TRACK_SUBSCRIPTION_AMOUNT, PurchaseHelper.getPurchaseAmount(sku));
            attr.putProperty(Tracking.TRACK_PURCHASE_SKU, sku);
            Tracking.trackEvent(Tracking.EVENT_PURCHASE_RENEW, attr);
        }
    }


    private void updatePromoSubs(final Context context, final String tokenId, final String promoId, final boolean isToShowEndPromoPopUp){
                NetworkManager.getInstance(context).updateSubs(context, JsonCreater.getInstance().getUpdateSubscription(null, true), tokenId, new NetworkResultListener() {
                    @Override
                    public void executeOnSuccess(JSONObject response) {
                        if (isToShowEndPromoPopUp) {
                            HashMap map = new HashMap<String, String>();
                            map.put("Promo_ID", promoId);
                            LocalyticsTrackers.Companion.setEPromoEnd(map);
                        }

                        EventBus.getDefault().post(new SubscriptionUpdated(promoId, false,isToShowEndPromoPopUp));
                        VpnApisHelper.Companion.getInstance().updateLicence(true,true, context);

                    }

                    @Override
                    public void executeOnError(VolleyError error) {

                    }
                });
    }

    private void updateSubs(final Context context, String tokenId, final SubscriptionPurchase subscriptionPurchase,
                            final String sku) {

        if(subscriptionPurchase== null){
            LocalyticsTrackers.Companion.setIsCancellSubscription("yes");
        }
        NetworkManager.getInstance(context).updateSubs(context,
                JsonCreater.getInstance().getUpdateSubscription(subscriptionPurchase, subscriptionPurchase == null), tokenId, new NetworkResultListener() {
                    @Override
                    public void executeOnSuccess(JSONObject response) {
                        EventBus.getDefault().post(new SubscriptionUpdated());
                        VpnApisHelper.Companion.getInstance().updateLicence(true,true, context);

                        NativeHelper.getInstnace().showToast(context, response);
                        if (subscriptionPurchase != null) {
                            trackRenewSubs(sku);
                            TuneTracking.getInstance().trackSubsRenew(sku);
                            KochavaTracking.getInstance().trackSubsRenew(sku);
                            FbTrackingConfig.getInstance().logPurchaseRenew(FinjanVPNApplication.getInstance(), sku);
                        }
                    }

                    @Override
                    public void executeOnError(VolleyError error) {
                        NativeHelper.getInstnace().showToast(context, error);
                    }
                });
    }


    public void createSubc(final Bundle bundle) {
        Logger.logE(TAG, "scheduled job started");
        if (bundle != null) {
            final Purchase purchase = new Purchase(bundle);
            final String userName = bundle.getString(AppConstants.BKEY_USERNAME);
            if (purchase == null) {
                return;
            }
            createSubs(FinjanVPNApplication.getInstance(), purchase, userName, new NetworkResultListener() {
                @Override
                public void executeOnSuccess(JSONObject response) {
                    EventBus.getDefault().post(new SubscriptionUpdated());
                    VpnApisHelper.Companion.getInstance().updateLicence(true,true, FinjanVPNApplication.getInstance());

                    new ULMSubscriptionCreatedNonFetal(false,purchase.getSku(),response.toString(),
                            JsonCreater.getInstance().getCreateSubscription(purchase, userName).toString(),true);
                }

                @Override
                public void executeOnError(VolleyError error) {
                    Logger.logE(TAG, WebUtility.getMessage(error));

                    if (error instanceof NetworkError || error instanceof NoConnectionError || error instanceof TimeoutError) {
                        ScheduleHitConfig.getInstance().schedulePurchase(purchase, userName);
                        NativeHelper.getInstnace().showToast(ScheduleHItMsg, Toast.LENGTH_LONG);
                        return;
                    }
                    NativeHelper.getInstnace().showToast(null, error);
                }
            });
        }
    }

    public void createSubs(final Context context, final Purchase purchase, final String username,
                           final NetworkResultListener listener) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                SubscriptionPurchase purchase1 = AndroidPublisherValidation.getInstance().
                        getPurchase(context, purchase.getSku(), purchase.getToken());
                if (purchase1 != null) {
                    purchase.setmExpiryTimeMillis(purchase1.getExpiryTimeMillis());
                    Tracking.trackEvent(Tracking.EVENT_PURCHASE, new SubscriptionTrack(purchase));
                    TuneTracking.getInstance().trackPurchase(purchase);
                    FbTrackingConfig.getInstance().logPurchase(FinjanVPNApplication.getInstance(), purchase);
                    KochavaTracking.getInstance().trackPurchase(purchase);
                    LocalyticsTrackers.Companion.setEIAP_Success();
                    LocalyticsTrackers.Companion.setERevenue(purchase.getSku(),purchase.getSku(),
                            PurchaseHelper.getExpireInterval(purchase.getSku()),
                            PurchaseHelper.getPurchaseAmount(purchase));

                    NetworkManager.getInstance(context).createSubs(context,
                            JsonCreater.getInstance().getCreateSubscription(purchase, username),
                            listener);
                }else {
                  /*  purchase.setmExpiryTimeMillis(purchase1.getExpiryTimeMillis());
                    Tracking.trackEvent(Tracking.EVENT_PURCHASE, new SubscriptionTrack(purchase));
                    TuneTracking.getInstance().trackPurchase(purchase);
                    FbTrackingConfig.getInstance().logPurchase(FinjanVPNApplication.getInstance(), purchase);
                    KochavaTracking.getInstance().trackPurchase(purchase);
                    LocalyticsTrackers.Companion.setEIAP_Success();
                    LocalyticsTrackers.Companion.setERevenue(purchase.getSku(),purchase.getSku(),
                            PurchaseHelper.getExpireInterval(purchase.getSku()),
                            PurchaseHelper.getPurchaseAmount(purchase));

                    NetworkManager.getInstance(context).createSubs(context,
                            JsonCreater.getInstance().getCreateSubscription(purchase, username),
                            listener);*/
                    listener.executeOnError(new VolleyError("Unable validate Purchase"));
                }
            }
        }).start();
    }

    public void createPromoSubsc(final Context context, final String promoId,final String promoEnd,
                                 final NetworkResultListener listener){
        new Thread(new Runnable() {
            @Override
            public void run() {

                String username =AuthenticationHelper.getInstnace().getUserEmail(context);
                 if(!TextUtils.isEmpty(username)){
                    username = username.replace(".","");
                    username = username.replace("@","");
                  }else{
                     AuthenticationHelper.getInstnace().fetchUser(context, new NetworkResultListener() {
                         @Override
                         public void executeOnSuccess(JSONObject response) {
                             FinjanUser finjanUser = new FinjanUser(response);
                             AuthenticationHelper.getInstnace().setFinjanUser(finjanUser);
                             if (!UserPref.getInstance().getUserSendLocalitics() && finjanUser!=null)
                             {

//                                     if (!TextUtils.isEmpty(finjanUser.getId()))
//                                         Localytics.setCustomerId(finjanUser.getId());
//                                     if (!TextUtils.isEmpty(finjanUser.getEmail()))
//                                         Localytics.setCustomerEmail(finjanUser.getEmail());
//                                     if (!TextUtils.isEmpty(finjanUser.getFirst_name()))
//                                         Localytics.setCustomerFullName(finjanUser.getFirst_name());

                                 UserPref.getInstance().setUserSendLocalitics(true);
                             }

//                             EventBus.getDefault().post(new FinjanUserFetched());
                             NetworkManager.getInstance(context).createSubs(context,
                                     JsonCreater.getInstance().getPromoSubs(promoId, promoEnd,
                                             AuthenticationHelper.getInstnace().getUserName(context)), listener);
                         }

                         @Override
                         public void executeOnError(VolleyError error) {

                         }
                     });
                 }

                NetworkManager.getInstance(context).createSubs(context,
                        JsonCreater.getInstance().getPromoSubs(promoId, promoEnd,username), listener);
            }
        }).start();

    }

    private void createRenewSubs(final Context context, final String sku, final String token,
                                 final SubscriptionPurchase purchase, final String username){

        LocalyticsTrackers.Companion.setERevenue(sku,sku,
                PurchaseHelper.getExpireInterval(sku),
                PurchaseHelper.getPurchaseAmount(sku));
        NetworkManager.getInstance(context).createRenew(context,
                JsonCreater.getInstance().getRenewSubscription(sku, token, username, purchase), new NetworkResultListener() {
                    @Override
                    public void executeOnSuccess(JSONObject response) {
                        Logger.logE(TAG,"createRenewSubs Successful");
                        new ULMSubscriptionCreatedNonFetal(true,sku,response.toString(),
                                JsonCreater.getInstance().getRenewSubscription(sku,token, username,purchase).toString(),false);
                    }

                    @Override
                    public void executeOnError(VolleyError error) {
                        Logger.logE(TAG,"createRenewSubs Error "+WebUtility.getMessage(error));
                    }
                });
    }
    public void getDeviceSubscriptions(final Activity activity, final SubscriptionAvailable subscriptionAvailable){
        if((!UserPref.getInstance().getIsBrowser() ||
                UserPref.getInstance().getMYLastpage()==VPN_SCREEN)
                && NativeHelper.getInstnace().checkActivity(activity)) {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    final IabHelper mIabHelper=new IabHelper(activity);
                    mIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                        @Override
                        public void onIabSetupFinished(IabResult result) {
                            if(result.isSuccess()){
                                if(mIabHelper.getmService()!=null){
                                    IInAppBillingService mService=mIabHelper.getmService();

                                    ArrayList<String> skuList = new ArrayList<String>();
                                    skuList.add(LicenseUtil.getSkuAllDevicesMonthly());
                                    skuList.add(LicenseUtil.getSkuAllDevicesYearly());
                                    skuList.add(LicenseUtil.getSkuThisDevicesMonthly());
                                    skuList.add(LicenseUtil.getSkuThisDevicesYearly());

                                    Bundle querySkus = new Bundle();
                                    querySkus.putStringArrayList("ITEM_ID_LIST", skuList);

                                    try {
                                        Bundle skuDetails = mService.getPurchases(3,
                                                activity.getPackageName(), "subs", null);
                                        int response = skuDetails.getInt("RESPONSE_CODE");

                                        ArrayList<String> responseList = skuDetails.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                                        ArrayList<String> activitySubsList = new ArrayList<String>();

                                        if (responseList == null || responseList.size() == 0) {
                                            subscriptionAvailable.subscriptionAvailable(activitySubsList);
                                            return;
                                        }

                                        for (String thisResponse : responseList) {

                                            JSONObject object = new JSONObject(thisResponse);
                                            String sku = object.getString("productId");
                                            String autoRenewing = object.getString("autoRenewing");
                                            if("true".equalsIgnoreCase(autoRenewing)){
                                                activitySubsList.add(sku);
                                            }
                                        }
                                        subscriptionAvailable.subscriptionAvailable(activitySubsList);

                                    }catch (Exception e){
                                        subscriptionAvailable.subscriptionAvailable(new ArrayList<String>());
                                    }
                                }
                            }
                        }
                    });
                }
            }).start();
        }
    }
    public interface SubscriptionAvailable{
        void subscriptionAvailable(ArrayList<String> skuList);
    }

    private static final class SubsCriptionHelperHolder {
        private static final SubscriptionHelper Instance = new SubscriptionHelper();
    }
}
