/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.controller.network;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface NetworkResultListener {
    void executeOnSuccess(JSONObject response);
    void executeOnError(VolleyError error);
}
