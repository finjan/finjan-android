package com.finjan.securebrowser.vpn.licensing;

import android.content.Context;
import android.text.format.DateUtils;

import com.avira.common.backend.Backend;
import com.avira.common.backend.oe.OeRequest;
import com.avira.common.backend.oe.OeRequestQueue;
import com.avira.common.backend.oe.RESTRequest;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.models.billing.SkuDetails;
import com.finjan.securebrowser.vpn.licensing.models.restful.License;
import com.finjan.securebrowser.vpn.licensing.models.restful.LicenseArray;
import com.finjan.securebrowser.vpn.licensing.models.server.ProcessPurchasePayload;
import com.finjan.securebrowser.vpn.licensing.models.server.ProcessPurchaseResponse;
import com.finjan.securebrowser.vpn.licensing.models.server.TrialRequestPayload;
import com.finjan.securebrowser.vpn.licensing.models.server.TrialResponse;
import com.finjan.securebrowser.vpn.licensing.utils.SubscriptionVPN;
import com.avira.common.utils.AccessTokenUtil;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.text.ParseException;
import java.util.Date;

/**
 * API to deal with purchases processing and licences querying on the server side
 *
 * @author ovidiu.buleandra
 * @since 03.11.2015
 */
@SuppressWarnings("unused")
public class Licensing {
    private static final String LICENSES_RESOURCE = "licenses";
    private static final String PROCESS_PURCHASE = "processWalletSubscription";
    private static final int TRIAL_DURATION = 15;
    private static final int LICENSING_TIMEOUT = 5000;

    /**
     * calls the backend server to retrieve a list of licenses associated with the specified app
     *
     * @param context  valid app context
     * @param acronym  app acronym
     * @param callback {@link LicensesCallback} interface to deal with the response
     */
    public static String queryLicenses(Context context, String acronym,
                                       LicensesCallback callback) {
        String accessToken = AccessTokenUtil.generateToken(context, acronym);

        LicensesInternalCallback internalCallback = new LicensesInternalCallback(callback);

        String requestUrl = String.format("%s%s/?access_token=%s", Backend.BACKEND_URL.replace("/android", ""),
                LICENSES_RESOURCE, accessToken);

        RESTRequest<LicenseArray> request = new RESTRequest<>(requestUrl, LicenseArray.class,
                internalCallback, internalCallback);

        OeRequestQueue.getInstance(context).add(request);

        return requestUrl;
    }

    public static void processPurchase(Context context,
                                       Purchase purchase, SkuDetails skuDetails,
                                       String productAcronym, ProcessPurchaseCallback callback) {
        processPurchase(context, purchase, skuDetails, productAcronym, callback, null);
    }

    /**
     * validates a payment made through google in app purchase system
     *
     * @param context        valid application context
     * @param purchase       purchase details received from google
     * @param skuDetails     bought product details
     * @param productAcronym bought product acronym for association in Avira's MyAccount system
     * @param callback       registered callback to receive the result of the processing
     */
    public static void processPurchase(Context context,
                                       Purchase purchase, SkuDetails skuDetails,
                                       String productAcronym, ProcessPurchaseCallback callback, SubscriptionVPN subscriptionVPN) {
        ProcessPurchaseInternalCallback internalCallback =
                new ProcessPurchaseInternalCallback(context, productAcronym, callback);

        ProcessPurchasePayload postData = new ProcessPurchasePayload(context, purchase, skuDetails,
                productAcronym, subscriptionVPN);

        // Create OE backend request
        String requestUrl = Backend.BACKEND_URL + PROCESS_PURCHASE;
        OeRequest<ProcessPurchasePayload, ProcessPurchaseResponse> request = new OeRequest<>(
                requestUrl, postData, ProcessPurchaseResponse.class,
                internalCallback, internalCallback, LICENSING_TIMEOUT);

        // Add to volley queue
        OeRequestQueue.getInstance(context).add(request);
    }

    /**
     * @param context        valid context
     * @param productAcronym product id to request trial activation for
     * @param callback       callback for success and error scenarios
     */
    public static void requestTrialLicense(Context context, String productAcronym,
                                           TrialRequestCallback callback) {
        TrialRequestInternalCallback internalCallback = new TrialRequestInternalCallback(context,
                productAcronym, callback);

        TrialRequestPayload payload = new TrialRequestPayload(context, productAcronym);

        String requestUrl = Backend.BACKEND_URL + PROCESS_PURCHASE;
        OeRequest<TrialRequestPayload, TrialResponse> request = new OeRequest<>(requestUrl, payload,
                TrialResponse.class,
                internalCallback, internalCallback);

        // Add to volley queue
        OeRequestQueue.getInstance(context).add(request);
    }

    /**
     * convenience method to retrieve licenses stored inside the secure database
     *
     * @return list of {@link License} objects
     */
//    public static List<License> loadCachedLicenses() {
//        return Settings.readLicenses();
//    }

    /**
     * convenience method to cache the list of licenses inside the secure database
     *
     * @param licenses list of current licenses received by calling
     *                 {@link #queryLicenses(Context, String, LicensesCallback)}
     */
//    public static void saveLicensesInCache(List<License> licenses) {
//        Settings.saveLicenses(licenses);
//    }

    public static License generateLicense(String product, boolean isTrial) {
        return generateLicense(product, isTrial, null);
    }

    /**
     * generate a mock license for the specified product
     *
     * @param product product acronym to generate license for
     * @param isTrial specifies if the generated license is for evaluation or paid
     * @param expDate nullable parameter that specify the expiration date for the generated license(default is 30 days)
     */
    public static License generateLicense(String product, boolean isTrial, String expDate) {
        String expirationDate = License.iso8601format.format(
                new Date(System.currentTimeMillis() + 30 * DateUtils.DAY_IN_MILLIS));
        if (expDate != null) {
            try {
                Date paramDate = License.serverFormat.parse(expDate);
                expirationDate = License.iso8601format.format(paramDate.getTime());
            } catch (ParseException e) {
                // ignore
            }
        }

        final String licenseJSON = String.format("{\"relationships\": {" +
                "\"app\": {\"data\": {\"type\": \"apps\", \"id\": \"%s\"}}, " +
                "\"user\": {\"data\": {\"type\": \"users\", \"id\": 0}}}, " +
                "\"attributes\": {\"runtime_unit\": \"months\", \"expiration_date\": \"%s\", \"key\": \"\", " +
                "\"devices_limit\": 1, \"runtime\": 240, \"type\": \"%s\"}, \"type\": \"licenses\", " +
                "\"id\": \"generated-at-purchase\"}", product, expirationDate, isTrial ? "eval" : "paid");
        try {
            return new Gson().fromJson(licenseJSON, License.class);
        } catch (JsonSyntaxException e) {
            return null;
        }
    }
}
