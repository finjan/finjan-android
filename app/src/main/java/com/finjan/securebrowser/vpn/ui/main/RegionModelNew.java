package com.finjan.securebrowser.vpn.ui.main;

import android.text.TextUtils;
import android.view.View;

import com.finjan.securebrowser.R;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by navdeep on 9/3/18.
 */

public class RegionModelNew implements Serializable
{
    private String host;

    @SerializedName("id")
    private String region;

    private String name;
    private String port;
    private String protocol;
    private boolean isCity=false;
    private long serverId;

    public boolean isCity() {
        return isCity;
    }

    public void setIsCity(boolean city) {
        isCity = city;
    }

    // client side decided variables.
    private HashMap<String,RegionModelNew> citiesList=new HashMap<>();
    private boolean selected=false;
    private int showArrow= View.VISIBLE;

    public int getShowArrow() {
        return showArrow;
    }

    public void setShowArrow(int showArrow) {
        this.showArrow = showArrow;
    }


    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    private String countryId="";
    private int countryFlag= R.drawable.tab_vpn_off;

    public RegionModelNew(){}
public RegionModelNew(String regionId,String name){
    this.name=name;
    this.region=regionId;
}
    public RegionModelNew(String host, String id, String name, String port, String protocol) {
        this.host = host;
        this.serverId = 0;
        this.region = id;
        this.name = name;
        this.port = port;
        this.protocol = protocol;
    }

    public HashMap<String, RegionModelNew> getCitiesList() {
        return citiesList;
    }

    public void setCitiesList(HashMap<String, RegionModelNew> citiesList) {
        this.citiesList = citiesList;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public long getServerId() {
        return serverId;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public int getCountryFlag() {
        return countryFlag;
    }

    public void setCountryFlag(int countryFlag) {
        this.countryFlag = countryFlag;
    }
}
