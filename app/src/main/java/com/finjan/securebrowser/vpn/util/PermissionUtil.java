/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */
package com.finjan.securebrowser.vpn.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.core.content.ContextCompat;

import android.view.View;

import com.avira.common.dialogs.AviraDialog;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;

/**
 * Class to store permissions handling utility methods for API 23 that could be re-used.
 * <p/>
 * Created by hui-joo.goh on 03/06/16.
 */
public class PermissionUtil {

    private static final String TAG = PermissionUtil.class.getName();

    public static boolean hasLocationPermission(Context context) {

        // if one dangerous permission has been granted, access to the remaining
        // permissions in the same permission group is immediately granted
        // (but all required permission still needs to be declared in AndroidManifest.xml)
        return hasPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                || hasPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public static boolean hasPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Check if the system has blocked location requests (will not show location request dialog when requested).
     * This could happen if the user has clicked "never ask again" on the system permission dialog previously.
     *
     * @param activity
     * @return True if the location permission request has been blocked, otherwise false.
     */
    public static boolean isLocationRequestBlocked(Activity activity) {
        boolean blocked = !ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                && !hasLocationPermission(activity)
                && FinjanVpnPrefs.hasRequestedLocationPermission(activity);

        Logger.logD(TAG, "isLocationRequestBlocked " + blocked);
        return blocked;
    }

    /**
     * Check if the system has blocked account permission requests (will not show location request dialog when requested).
     * This could happen if the user has clicked "never ask again" on the system permission dialog previously.
     *
     * @param activity
     * @return True if the get accounts permission request has been blocked, otherwise false.
     */
//    public static boolean isAccountPermissionBlocked(Activity activity) {
//        boolean blocked = !ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.GET_ACCOUNTS)
//                && !hasPermission(activity, Manifest.permission.GET_ACCOUNTS)
//                && FinjanVpnPrefs.hasRequestedAccountsPermission(activity);
//
//        Logger.logD(TAG, "isAccountsRequestBlocked " + blocked);
//        return blocked;
//    }

    public static void requestLocationPermission(Activity activity, int requestCode) {
        requestPermission(activity, requestCode,
                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);

        // remember that we have requested
        FinjanVpnPrefs.setRequestedLocationPermission(activity, true);
    }

    public static void requestAccountsPermission(Activity activity, int requestCode) {
        requestPermission(activity, requestCode, Manifest.permission.GET_ACCOUNTS);

        // remember that we have requested
        FinjanVpnPrefs.setRequestedAccountsPermission(activity, true);
    }

    public static void requestPermission(Activity activity, int requestCode, String... permission) {
        ActivityCompat.requestPermissions(activity, permission, requestCode);
    }

    public static void showRationaleDialog(FragmentActivity activity, int descResId, View.OnClickListener listener) {
        new AviraDialog.Builder(activity)
                .setTitle(R.string.permission_required)
                .setDesc(descResId)
                .setNegativeButton(R.string.Cancel, listener)
                .setPositiveButton(R.string.OK, listener)
                .setCancelable(false)
                .show(activity.getSupportFragmentManager());
    }
    public static void askPhoneStatePermission(final Activity activity){
//        FinjanDialogBuilder.showPermissionRequest(activity, R.string.we_need_permission_to_read_phone_state,
//                new FinjanDialogBuilder.NegativeClickListener() {
//                    @Override
//                    public void onNegativeClick() {
//                        FinjanDialogBuilder.showPermissionError(activity);
//                    }
//                }, new FinjanDialogBuilder.PositiveClickListener() {
//                    @Override
//                    public void onPositivClick() {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, AppConstants.READ_PHONE_STATE_PERMISSION);
//                    }
//                });
    }

    public static void askLocationPermission(final Activity activity){
//        FinjanDialogBuilder.showPermissionRequest(activity, R.string.we_need_you_location_in_order_check_weather_connection_secure,
//                new FinjanDialogBuilder.NegativeClickListener() {
//                    @Override
//                    public void onNegativeClick() {
//                        FinjanDialogBuilder.showPermissionError(activity);
//                    }
//                }, new FinjanDialogBuilder.PositiveClickListener() {
//                    @Override
//                    public void onPositivClick() {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, AppConstants.LOCATION_PERMISSION);
//                    }
//                });
    }
    public static void askFileWritePermission(final Activity activity){
        FinjanDialogBuilder.showPermissionRequest(activity, R.string.we_need_permission_to_write_file,
                new FinjanDialogBuilder.NegativeClickListener() {
                    @Override
                    public void onNegativeClick() {
                        FinjanDialogBuilder.showPermissionError(activity);
                    }
                }, new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.STORAGE_PERMISSION);
                    }
                });
    }

    public static boolean isPhoneReadStatePermissionGranted(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}
