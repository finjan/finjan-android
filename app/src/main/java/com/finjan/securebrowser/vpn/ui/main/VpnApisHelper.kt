package com.finjan.securebrowser.vpn.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import android.text.TextUtils
import com.android.volley.VolleyError
import com.finjan.securebrowser.helpers.AuthenticationHelper
import com.finjan.securebrowser.helpers.sharedpref.UserPref
import com.finjan.securebrowser.jobScheduler.ScheduleHitConfig
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers
import com.finjan.securebrowser.util.AppConfig
import com.finjan.securebrowser.vpn.FinjanVPNApplication
import com.finjan.securebrowser.vpn.controller.JsonConfigParser
import com.finjan.securebrowser.vpn.controller.network.NetworkManager
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener
import com.finjan.securebrowser.vpn.eventbus.LicenceFetched
import com.finjan.securebrowser.vpn.eventbus.LicenceFetchedFailed
import com.finjan.securebrowser.vpn.eventbus.LicenceFetchedVPN
import com.finjan.securebrowser.vpn.subscriptions.SubscriptionHelper
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper
import com.finjan.securebrowser.vpn.util.TrafficController
import com.finjan.securebrowser.vpn.util.VpnUtil
import de.greenrobot.event.EventBus
import org.json.JSONObject
import kotlin.math.exp

class VpnApisHelper {

    companion object {
        private object HOLDER {
            var instance = VpnApisHelper()
        }

        var isLicenseFetched = false
        var isPromoListFetched = false
        var isUserFetched = false

        var licenseAlreadyFetched = false
        val instance: VpnApisHelper by lazy { HOLDER.instance }
    }

    fun shouldAutoLogin(context: Context): Boolean {
        return TrafficController.getInstance(context).isRegistered && UserPref.getInstance().isTokenExpires
    }

    fun shouldCheckPromoValidaity(context: Context): Boolean {
        val list = PromoHelper.instance.getAllPromoList(context)
        val promofetched = (list != null && list.size > 0)
        return isLicenseFetched && promofetched && AuthenticationHelper.getInstnace().finjanUser != null
    }


    private fun getLicense(isToServerHit: Boolean, context: Context, networkResultListener: NetworkResultListener) {
        isLicenseFetched = false
        NetworkManager.getInstance(context).fetchLicence(isToServerHit, context, networkResultListener)
    }

    var licenFailedCount = 0


    fun updateLicence(isToServerHit: Boolean, force: Boolean, context: Context) {
        updateLicence(isToServerHit, force, context, null)
    }

    class CurrentLicense(val type: String, val expiry: String) {}

    fun refreshApis() {
        isLicenseFetched = false
        licenseAlreadyFetched = false
        this.currentLicense = null
    }

    var currentLicense: CurrentLicense? = null
    fun ifLicenseSame(type: String, expiry: String): Boolean {
        return this.currentLicense != null && this.currentLicense!!.type.equals(type) &&
                this.currentLicense!!.expiry.equals(expiry)
    }

    fun updateCurrentLicense(type: String, expiry: String) {
        if (!TextUtils.isEmpty(type) && !TextUtils.isEmpty(expiry)) {
            this.currentLicense = CurrentLicense(type, expiry)
        }

    }


    /**
     * should be single window for update license.
     * and impact should be broadscast
     *
     */

    var waitingLicenceForVpnConnect = false


    fun updateLicence(isToServerHit: Boolean, force: Boolean, context: Context, networkResultListener: NetworkResultListener?) {
        if (licenseAlreadyFetched && !force) {
            isLicenseFetched = true
            return
        }
//            if(force){licenFailedCount =0}
        if (shouldAutoLogin(context)) {
            return
        }
//        if (!VpnUtil.isVpnActive(context)) {

        if (licenFailedCount >= 3 && !force) {

            EventBus.getDefault().post(LicenceFetchedFailed())
            return
        }
        licenFailedCount = licenFailedCount + 1
        getLicense(isToServerHit, context, object : NetworkResultListener {
            override fun executeOnSuccess(response: JSONObject) {
                Thread(Runnable {
                    licenseAlreadyFetched = true
                    if (TextUtils.isEmpty(UserPref.getInstance().tempPurchaseToken)) {
                        JsonConfigParser.parseJsonLicense(FinjanVPNApplication.getInstance(), response)
                    }
                    if (waitingLicenceForVpnConnect) {
                        EventBus.getDefault().post(LicenceFetchedVPN())
                        if (networkResultListener != null) {
                            networkResultListener.executeOnSuccess(response)
                            return@Runnable
                        }
                    }
                    var type = TrafficController.getInstance(FinjanVPNApplication.getInstance()).licenseType
                    var license = TrafficController.getInstance(FinjanVPNApplication.getInstance()).licenseExpiry
                    var licenseUpdate = false
                    if (!ifLicenseSame(type!!, license)) {
                        updateCurrentLicense(type, license)
                        licenseUpdate = true
                        AppConfig.LocalDataHelper.updateLicenseJson(response)
                        LocalyticsTrackers.setUserType()
                        if (TrafficController.getInstance(context).isRegistered) {
                            ScheduleHitConfig.getInstance().scheduleWifiStatusChange()
                        }
                        isLicenseFetched = true
                        if (networkResultListener != null) {
                            networkResultListener.executeOnSuccess(response)
                        }

                    }
//                        EventBus.getDefault().post(LicenceFetchedVPN())
                    EventBus.getDefault().post(LicenceFetched(response, (force || licenseUpdate)))
                    if (!SubscriptionHelper.getInstance().ifSubscriptionIsFetched()) {
                        SubscriptionHelper.getInstance().initRequiredSubsUpdation(context, isToServerHit)
                    }
                }).start()

            }

            override fun executeOnError(error: VolleyError) {
                if (networkResultListener != null) {
                    networkResultListener.executeOnError(error)
                }
                if (licenFailedCount < 3) {
                    // updateLicence(true,force,context)
                } else {
                    if (AppConfig.supportOfLocalJsons) {
                        isLicenseFetched = true
                        EventBus.getDefault().post(LicenceFetched(AppConfig.LocalDataHelper.getOneGBUser(), false))
                    }

                }
            }
        })
//        }
    }
}