/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.avira.common.utils.DeviceInfo;
import com.avira.common.utils.WhatsNewDialog;
import com.finjan.securebrowser.helpers.logger.Logger;

/**
 * Broadcast receiver that will be notified whenever a new version
 * of our application has been installed over an existing one.
 * This class would NOT receive update events of other application.
 */
public class MyPackageReplacedReceiver extends BroadcastReceiver {

    private static final String TAG = MyPackageReplacedReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.logD(TAG, "onReceive own application updated to " + DeviceInfo.getApplicationVersion(context));

        WhatsNewDialog.appWasUpdated(context);
    }
}
