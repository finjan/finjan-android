//package com.finjan.securebrowser.vpn.ui.authentication;
//
//import android.util.Log;
//
//import com.android.volley.VolleyError;
//import com.avira.common.authentication.AuthenticationListener;
//import com.avira.common.authentication.models.LoginResponse_1;
//import com.avira.common.authentication.models.Subscription;
//import com.avira.common.authentication.models.User;
//import com.finjan.securebrowser.vpn.FinjanVPNApplication;
//import com.finjan.securebrowser.vpn.controller.JsonConfigParser;
////import com.finjan.securebrowser.a_vpn.controller.mixpanel.PeopleProfileManager;
////import com.finjan.securebrowser.a_vpn.controller.mixpanel.Tracking;
//import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
//import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
//import com.finjan.securebrowser.vpn.eventbus.LoginSuccess;
//
//import org.json.JSONObject;
//
//import de.greenrobot.event.EventBus;
//
///**
// * Created by Illia.Klimov on 4/6/2016.
// */
//public class LoginTokenAuthListener  implements AuthenticationListener {
//
//    public static final String TAG = LoginTokenAuthListener.class.getSimpleName();
//
//    @Override
//    public void onAuthSuccess(User user, Subscription subscription) {
////        String authToken = AccessTokenUtil.generateToken(FinjanVPNApplication.getInstance(), FinjanVPNApplication.getAcronym());
////        Logger.logD(TAG, "authorization successful " + authToken);
////        FinjanVpnPrefs.setAuthToken(FinjanVPNApplication.getInstance(), authToken);
////
////        NetworkManager.getInstance(FinjanVPNApplication.getInstance()).fetchLicence(FinjanVPNApplication.getInstance(), new NetworkResultListener() {
////            @Override
////            public void executeOnSuccess(JSONObject response) {
//////                Tracking.trackEvent(Tracking.EVENT_LOGGEDIN);
////                new JsonConfigParser().parseJsonLicense(FinjanVPNApplication.getInstance(), response);
////                EventBus.getDefault().post(new LoginSuccess());
////            }
////
////            @Override
////            public void executeOnError(VolleyError error) {
////            }
////        });
//
////        PeopleProfileManager.getInstance().updateAnonymousToRegisteredProfile();
//    }
//
//    @Override
//    public void onAuthSuccess(LoginResponse_1 loginResponse_1) {
//
////        FinjanVpnPrefs.setAuthToken(FinjanVPNApplication.getInstance(), loginResponse_1.getAccess_token());
////        FinjanVpnPrefs.setAuthRefereshToken(FinjanVPNApplication.getInstance(), loginResponse_1.getRefresh_token());
//
//        NetworkManager.getInstance(FinjanVPNApplication.getInstance()).fetchLicence(FinjanVPNApplication.getInstance(), new NetworkResultListener() {
//            @Override
//            public void executeOnSuccess(JSONObject response) {
////                Tracking.trackEvent(Tracking.EVENT_LOGGEDIN);
//                JsonConfigParser.parseJsonLicense(FinjanVPNApplication.getInstance(), response);
//                EventBus.getDefault().post(new LoginSuccess());
//            }
//
//            @Override
//            public void executeOnError(VolleyError error) {
//            }
//        });
//
//    }
//
//    @Override
//    public void onAuthError(int i, String s,String request) {
//        Log.e(TAG, "authorization onAuthError " + s);
//    }
//}