/*
 * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.ui.iab;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovidiu.buleandra
 * @since 22.12.2015
 */
public class PurchaseExtraInfo implements GSONModel {

    @SerializedName("mAviraAccount") private String mAviraAccount;
    @SerializedName("mGoogleAccount") private String mGoogleAccount;

    public PurchaseExtraInfo(String aviraEmail, String googleEmail) {
        mAviraAccount = aviraEmail;
        mGoogleAccount = googleEmail;
    }

    public String getAviraAccount() {
        return mAviraAccount;
    }

    public String getGoogleAccount() {
        return mGoogleAccount;
    }
}
