package com.finjan.securebrowser.vpn.ui.authentication

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.avira.common.utils.NetworkConnectionManager
import com.finjan.securebrowser.R
import com.finjan.securebrowser.activity.NormaWebviewActivity
import com.finjan.securebrowser.constants.AppConstants
import com.finjan.securebrowser.constants.DrawerConstants
import com.finjan.securebrowser.helpers.NativeHelper
import com.finjan.securebrowser.helpers.PlistHelper
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder

class PolicyAcivity: AppCompatActivity(){

    private lateinit var cb_eulaSocial: CheckBox
    private lateinit var btn_signup: TextView
    private lateinit var llBack: LinearLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_policy_check)
        cb_eulaSocial= findViewById(R.id.cb_eula)
        btn_signup= findViewById(R.id.btn_signup)
        llBack = findViewById(R.id.ll_back)
        llBack.setOnClickListener { onBackClick() }
        setPrivacyAndEulaClickable(findViewById(R.id.tv_eula) )
        btn_signup.setOnClickListener({ if(validateEulaCheckBox()) {
            setResult(Activity.RESULT_OK)
            finish()
        }
        })
    }
    private fun onBackClick(){
        setResult(Activity.RESULT_CANCELED)
        onBackPressed()
    }

    private fun validateEulaCheckBox(): Boolean {
        if (!cb_eulaSocial.isChecked()) {
            FinjanDialogBuilder(this).setMessage(PlistHelper.getInstance().eulaSkipMsgText)
                    .setPositiveButton("Ok").show()
        }
        return cb_eulaSocial.isChecked()
    }

    private fun setPrivacyAndEulaClickable(textView: TextView) {
        val eulaClick = object : ClickableSpan() {
            override fun onClick(textView: View) {
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoEulaUrl()
                }
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
        }
        val privacyClick = object : ClickableSpan() {
            override fun onClick(textView: View) {
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoPrivacyUrl()
                }
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
        }
        val ss = SpannableString(getString(R.string.by_registering_you_aggre_to_the_eula))
        ss.setSpan(UnderlineSpan(), 33, 66, 0)
        ss.setSpan(UnderlineSpan(), 75, 90, 0)
        ss.setSpan(eulaClick, 33, 66, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        ss.setSpan(privacyClick, 75, 90, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        textView.text = ss
        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.highlightColor = Color.TRANSPARENT
    }

    /**
     * @return false - the dialog was not shown => there is connectivity true - the dialog was shown => no connectivity
     */
    private fun showNoNetworkDialogIfNoConnectivity(): Boolean {
        if (!NetworkConnectionManager.hasNetworkConnectivity(this)) {
            if (NativeHelper.getInstnace().checkActivity(this)) {
                Toast.makeText(this, "Connect to a mobile or Wi-Fi network to continue.", Toast.LENGTH_LONG).show()

                //                CommonDialogs.showNetworkUnavailable(getActivity());
                return true
            }

        }
        return false
    }

    fun gotoEulaUrl() {

        val intent = Intent(this, NormaWebviewActivity::class.java)
        intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_EULA)
        startActivity(intent)
    }

    fun gotoPrivacyUrl() {

        val intent = Intent(this, NormaWebviewActivity::class.java)
        intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_PRIVATE_PRIVACY)
        GoogleTrackers.setSPrivacyPolicy()
        startActivity(intent)
    }


}