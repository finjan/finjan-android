/*
 * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.util;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class containing re-usable methods related to Avira applications
 */
public class AviraApps {

    // REMINDER: When adding a new avira app, please update the aviraAppSet below as well
    public static final String PHANTOM_VPN = "com.avira.vpn";
    public static final String MOBILE_SECURITY = "com.avira.android";
    public static final String OPTIMIZER = "com.avira.optimizer";
    public static final String APPLOCK_PLUS = "com.avira.applockplus";
    public static final String LAUNCHER = "com.avira.launcher";
    public static final String PASSWORD_MGR = "com.avira.passwordmanager";

    private static final Set<String> aviraAppSet = new HashSet<String>() {{
        add(PHANTOM_VPN);
        add(MOBILE_SECURITY);
        add(OPTIMIZER);
        add(APPLOCK_PLUS);
        add(LAUNCHER);
        add(PASSWORD_MGR);
    }};

    public static List<String> getInstalledAviraApps(Context context) {
        List<String> installedApps = new ArrayList<>();

        for (String packageName : aviraAppSet) {

            if (PHANTOM_VPN.equals(packageName)
                    || Util.isPackageInstalled(context, packageName)) {
                installedApps.add(packageName);
            }
        }
        return installedApps;
    }

    public static boolean isAviraApp(String packageName) {
        return aviraAppSet.contains(packageName);
    }
}
