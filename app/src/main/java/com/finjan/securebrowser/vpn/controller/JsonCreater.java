package com.finjan.securebrowser.vpn.controller;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.avira.common.CommonLibrary;
import com.avira.common.id.HardwareId;
import com.electrolyte.sociallogin.socialLogin.models.SocialUserProfile;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.helpers.JsonConstans;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.utils.PurchaseHelper;
//import com.finjan.securebrowser.vpn.social.SocialUserProfile;
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anurag on 16/06/17.
 * <p>
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class JsonCreater extends JsonConstans {

    private static final String RENEW = "renew";
    private static final String CANCEL = "cancel";

    public static JsonCreater getInstance() {
        return JsonCreaterHolder.Innstance;
    }

    public JSONObject getCreateDeviceObject(Context context) {

        JSONObject parent = new JSONObject();
        JSONObject dataJ = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        if (!TextUtils.isEmpty(HardwareId.get(context)) &&
                !TextUtils.isEmpty(NativeHelper.getInstnace().getIpAddressOfDevice())) {
            try {
                attributesJ.put(DEVICE_NAME, Build.MODEL);
                attributesJ.put(DEVICE_ID, HardwareId.get(context));
                attributesJ.put(TYPE, "android");
                attributesJ.put(IP_ADDRESS, NativeHelper.getInstnace().getIpAddressOfDevice());
                dataJ.put(ATTRIBUTES, attributesJ);
                parent.put(DATA, dataJ);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return parent;
    }

    public JSONObject getUpdateDeviceJson(Context context, String device_name, String activeStatus, String idAddress) {

        JSONObject parent = new JSONObject();
        JSONObject dataJ = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        if (!TextUtils.isEmpty(HardwareId.get(context)) &&
                !TextUtils.isEmpty(NativeHelper.getInstnace().getIpAddressOfDevice())) {
            try {
                if (!TextUtils.isEmpty(device_name)) {
                    attributesJ.put(DEVICE_NAME, device_name);
                }
                if (!TextUtils.isEmpty(idAddress)) {
                    attributesJ.put(IP_ADDRESS, idAddress);
                }
                if (!TextUtils.isEmpty(activeStatus)) {
                    attributesJ.put(STATUS, activeStatus);
                }
                attributesJ.put(TYPE, "android");
                dataJ.put(ATTRIBUTES, attributesJ);
                parent.put(DATA, dataJ);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return parent;
    }

    public JSONObject getUpdateLicenceJson(@NonNull Context context) {
        String type = TrafficController.getInstance(context).isPaid() ? "paid" : "registered";
        String deviceType = "android";
        JSONObject parent = new JSONObject();
        JSONObject dataJ = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        if (!TextUtils.isEmpty(HardwareId.get(context)) &&
                !TextUtils.isEmpty(NativeHelper.getInstnace().getIpAddressOfDevice())) {
            try {
                attributesJ.put(TYPE, type);
                dataJ.put(ATTRIBUTES, attributesJ);
                dataJ.put(DEVICE_TYPE, deviceType);
                parent.put(DATA, dataJ);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return parent;
    }

    public JSONObject getUpdateLicenceJson(@NonNull Context context, byte byteDeviceType,
                                           String expirationDate, String type, boolean subscription) {
        String deviceType = "android";
        if (byteDeviceType == 0) {
            deviceType = "android";
        } else if (byteDeviceType == 1) {
            deviceType = "all";
        }
        JSONObject parent = new JSONObject();
        JSONObject dataJ = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        if (!TextUtils.isEmpty(HardwareId.get(context)) &&
                !TextUtils.isEmpty(NativeHelper.getInstnace().getIpAddressOfDevice())) {
            try {
                attributesJ.put(EXPIRATION_DATE, expirationDate);
                attributesJ.put(SUBSCRIPTION, subscription);
                attributesJ.put(TYPE, type);
                dataJ.put(ATTRIBUTES, attributesJ);
                dataJ.put(DEVICE_TYPE, deviceType);
                parent.put(DATA, dataJ);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return parent;
    }

    /**
     * {
     * "data": {
     * "attributes": {
     * "subscription_id": "4079jfwo00",
     * "name": "Barth Chuks",
     * "device_type": "all",
     * "source": "google",
     * "expires_at": "2017-05-31 08:26:14",
     * "plan_interval": "month",
     * "amount": "800"
     * }
     * }
     * }
     *
     * @param purchase
     * @return
     */
    public JSONObject getCreateSubscription(Purchase purchase, String username) {

        String purchaseEnd = purchase.getmExpiryTimeMillis() > 0 ? DateHelper.getInstnace().getServerFormatDate(purchase.getmExpiryTimeMillis())
                : PurchaseHelper.getExpireTime(purchase);

        JSONObject parent = new JSONObject();
        JSONObject dataJ = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        try {
            attributesJ.put(TRANSACTION_ID, purchase.getToken());
            attributesJ.put(PARENT_TRANSACTION_ID, purchase.getToken());
//                attributesJ.put(RENEWAL_FLG,"false");
            attributesJ.put(NAME, username);
            attributesJ.put(DEVICE_TYPE, PurchaseHelper.getDeviceType(purchase));
            attributesJ.put(SOURCE, GOOGLE);
            attributesJ.put(EXPIRES_AT, purchaseEnd);
            attributesJ.put(CREATED_AT, DateHelper.getInstnace().getServerFormatDate(purchase.getPurchaseTime()));
            attributesJ.put(PLAN_INTERVAL, PurchaseHelper.getExpireInterval(purchase));
            attributesJ.put(AMOUNT, PurchaseHelper.getPurchaseAmount(purchase));
            dataJ.put(ATTRIBUTES, attributesJ);
            parent.put(DATA, dataJ);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parent;
    }

    public JSONObject getPromoSubs(String promoId, String purchaseEnd,String username){

        String transaction_id = "sub_"+username+"_"+promoId+"_"+System.currentTimeMillis();
        JSONObject parent = new JSONObject();
        JSONObject dataJ = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        try {
            attributesJ.put(TRANSACTION_ID, transaction_id);
            attributesJ.put(PARENT_TRANSACTION_ID, transaction_id);
//                attributesJ.put(RENEWAL_FLG,"false");
            attributesJ.put(NAME, username);
//            attributesJ.put(DEVICE_TYPE, "android");
            attributesJ.put(DEVICE_TYPE, "all");
//            attributesJ.put(SOURCE, PROMO);all
            attributesJ.put(SOURCE, GOOGLE);
            attributesJ.put(EXPIRES_AT, purchaseEnd);
            attributesJ.put(CREATED_AT, PromoHelper.Companion.getInstance().getCurrentDateStr());
            attributesJ.put(PLAN_INTERVAL, "promo");
            attributesJ.put(AMOUNT, 0);
            attributesJ.put(COMP_FLAG,promoId );
            dataJ.put(ATTRIBUTES, attributesJ);
            parent.put(DATA, dataJ);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.logD("requestOfPromoSubscriptionParent",parent.toString());
        Logger.logD("requestOfPromoSubscriptionAtribute",attributesJ.toString());
        Logger.logD("requestOfPromoSubscriptiondata",dataJ.toString());
        return parent;
    }

    public JSONObject getRenewSubscription(String sku, String token, String username, SubscriptionPurchase purchase) {
        String purchaseEnd = DateHelper.getInstnace().getServerFormatDate(purchase.getExpiryTimeMillis());
        if(!DateHelper.getInstnace().isMoreThanDaysDiff(25,purchase.getExpiryTimeMillis(),"")){
            purchaseEnd = PurchaseHelper.getExpireTime(sku,System.currentTimeMillis());
        }

        JSONObject parent = new JSONObject();
        JSONObject dataJ = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        try {
            attributesJ.put(TRANSACTION_ID, purchase.getOrderId());
            attributesJ.put(PARENT_TRANSACTION_ID, token);
            attributesJ.put(RENEWAL_FLG, "true");
            attributesJ.put(NAME, username);
            attributesJ.put(DEVICE_TYPE, PurchaseHelper.getDeviceType(sku));
            attributesJ.put(SOURCE, GOOGLE);
            attributesJ.put(EXPIRES_AT, purchaseEnd);
            attributesJ.put(CREATED_AT, DateHelper.getInstnace().getServerFormatDate(purchase.getStartTimeMillis()));
            attributesJ.put(PLAN_INTERVAL, PurchaseHelper.getExpireInterval(sku));
            attributesJ.put(AMOUNT, PurchaseHelper.getPurchaseAmount(sku));
            dataJ.put(ATTRIBUTES, attributesJ);
            parent.put(DATA, dataJ);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parent;
    }

    public JSONObject getUpdateSubscription(SubscriptionPurchase purchase, boolean cancel) {
        JSONObject parent = new JSONObject();
        JSONObject dataJ = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        try {
            if (cancel) {
                dataJ.put(ACTION, CANCEL);
                parent.put(DATA, dataJ);
            } else {
                dataJ.put(ACTION, RENEW);
                attributesJ.put(RENEWED_AT, PurchaseHelper.getServerFormatDate(purchase.getStartTimeMillis()));
                attributesJ.put(EXPIRES_AT, PurchaseHelper.getServerFormatDate(purchase.getExpiryTimeMillis()));
                dataJ.put(ATTRIBUTES, attributesJ);
                parent.put(DATA, dataJ);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parent;
    }

    public JSONObject getSocialToken(SocialUserProfile socialUserProfile) {
        JSONObject parent = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        try {
            attributesJ.put("token_id", socialUserProfile.getToken_id());
            attributesJ.put(socialUserProfile.getSocialType(), socialUserProfile.getSocialId());

            data.put("client_id", CommonLibrary.CLIENT_ID);
            data.put("client_secret", CommonLibrary.SECRET_ID);
            data.put("attributes", attributesJ);
            parent.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public JSONObject getSocialRegisterRequest(SocialUserProfile socialUserProfile) {
        JSONObject parent = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject attributesJ = new JSONObject();
        try {
            attributesJ.put("first_name", socialUserProfile.getFirstName());
            attributesJ.put("last_name",socialUserProfile.getLastName());
            attributesJ.put("email",socialUserProfile.getEmail());
            attributesJ.put("email_verified",socialUserProfile.getEmailVerified());
            attributesJ.put("source","android");
            attributesJ.put(socialUserProfile.getSocialType(), socialUserProfile.getSocialId());

            data.put("client_id", CommonLibrary.CLIENT_ID);
            data.put("client_secret", CommonLibrary.SECRET_ID);
            data.put("attributes", attributesJ);
            parent.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parent;
    }

    public JSONObject getNeverBounceRequest(String key) {
        JSONObject parent = new JSONObject();
        try {
            parent.put("key", key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parent;
    }

    private static final class JsonCreaterHolder {
        private static final JsonCreater Innstance = new JsonCreater();
    }
}
