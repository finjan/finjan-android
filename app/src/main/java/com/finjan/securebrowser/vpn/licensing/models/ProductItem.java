package com.finjan.securebrowser.vpn.licensing.models;

import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.models.billing.SkuDetails;
import com.finjan.securebrowser.vpn.licensing.models.restful.License;

/**
 * @author ovidiu.buleandra
 * @since 23.11.2015
 */
public class ProductItem {

    public static final int TRIAL_UNKNOWN = -1;
    public static final int TRIAL_USED = 0;
    public static final int TRIAL_OK = 1;
    public static final int TRIAL_ALREADY_ACTIVE = 2;

    private String mGoogleSKU;
    private String mMYAAcronym;
    private boolean mHasTrial;
    private Purchase mPurchase;
    private SkuDetails mSkuDetails;
    private License mLicense;
    private int trialStatus;

    public ProductItem(String googleSKU, String myaAcronym, boolean trialAvailable) {
        mGoogleSKU = googleSKU;
        mMYAAcronym = myaAcronym;
        mHasTrial = trialAvailable;
        trialStatus = TRIAL_UNKNOWN;
    }

    public String getSKU() {
        return mGoogleSKU;
    }

    public String getAcronym() {
        return mMYAAcronym;
    }

    public boolean hasTrial() {
        return mHasTrial;
    }

    public Purchase getPurchase() {
        return mPurchase;
    }

    public ProductItem setPurchase(Purchase purchase) {
        mPurchase = purchase;
        return this;
    }

    public SkuDetails getSKUDetails() {
        return mSkuDetails;
    }

    public ProductItem setSKUDetails(SkuDetails details) {
        mSkuDetails = details;
        return this;
    }

    public License getLicense() {
        return mLicense;
    }

    public ProductItem setLicense(License license) {
        mLicense = license;
        return this;
    }

    public void setTrialStatus(int status) {
        trialStatus = status;
    }
}
