package com.finjan.securebrowser.vpn.eventbus;

/**
 * Created by illia.klimov on 12/21/2015.
 */
public class ActionNotificationEvent {
    private int mMessage;

    public ActionNotificationEvent(int message) {
        mMessage = message;
    }

    public int getMessage() {
        return mMessage;
    }
}
