/*
 * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
 */
package com.finjan.securebrowser.vpn.ui.iab;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.avira.common.GeneralPrefs;
import com.avira.common.backend.WebUtility;
import com.avira.common.id.HardwareId;
import com.finjan.securebrowser.custom_exceptions.EventsListHelper;
import com.finjan.securebrowser.custom_exceptions.StoreSubscriptionCreatedNonFetal;
import com.finjan.securebrowser.custom_exceptions.ULMSubscriptionCreatedNonFetal;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.ToastHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.jobScheduler.ScheduleHitConfig;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.JsonCreater;
import com.finjan.securebrowser.vpn.eventbus.SubscriptionUpdated;
import com.finjan.securebrowser.vpn.licensing.ProcessPurchaseCallback;
import com.finjan.securebrowser.vpn.licensing.PurchaseLicenseBaseActivity;
import com.finjan.securebrowser.vpn.licensing.models.billing.IabResult;
import com.finjan.securebrowser.vpn.licensing.models.billing.Inventory;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.models.billing.SkuDetails;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper;
import com.finjan.securebrowser.vpn.licensing.utils.PurchaseHelper;
import com.finjan.securebrowser.vpn.licensing.utils.SubscriptionVPN;
import com.avira.common.utils.NetworkConnectionManager;
import com.avira.common.utils.ProgressDialogUtil;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.vpn.controller.JsonConfigParser;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.vpn.subscriptions.SubscriptionHelper;
import com.finjan.securebrowser.vpn.ui.main.VpnApisHelper;
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment;
import com.finjan.securebrowser.vpn.util.AccountUtil;
import com.finjan.securebrowser.helpers.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;

import de.greenrobot.event.EventBus;

/**
 * Abstract class to contain shared upgrade to pro activity logic.
 * This will be extended by different types of upgrade to pro screens that offers different purchase-able products.
 */
public abstract class UpgradeActivity extends PurchaseLicenseBaseActivity
        implements ProcessPurchaseCallback {

    private static final int ERROR_BILLING_UNAVAILABLE = 3;
    private static final int ERROR_QUERY_PRICE = 6;

    private ProgressDialogUtil mProgressDialogUtil;
    protected Inventory mInventory;
    private String mGoogleEmail;
    SubscriptionVPN subscriptionVPN;
    boolean seenOtherOffer = false;
    int runtime;
    byte deviceType = SUBSCRIPTION_ONE_DEVICE;
//    byte deviceType = SUBSCRIPTION_MULTIPLE_DEVICES;
    static final byte SUBSCRIPTION_MONTHLY = 0;
    static final byte SUBSCRIPTION_YEARLY = 1;


    static final byte SUBSCRIPTION_ONE_DEVICE = 0;
    static final byte SUBSCRIPTION_MULTIPLE_DEVICES = 1;
//    byte subscriptionType = SUBSCRIPTION_MONTHLY;
    byte subscriptionType = SUBSCRIPTION_YEARLY;
    private static final String PLATFORM_SPECIFIC = "platformDevice";
    private static final String PLATFORM_ALL = "platformAll";

    /**
     * Get the SKU (unique product ID) of each PRO features that is available to be purchased in the activity
     *
     * @return The SKU of each PRO features that is available to be purchased in the activity
     */
    public abstract List<String> getSkuList();

    /**
     * Triggered whenever the price of the offered product(s) in the page is available to be displayed to the user.
     *
     * @param skuDetails Details that are tied to a particular SKU (unique product ID)
     */
    public abstract void onPriceAvailable(SkuDetails skuDetails);

    /**
     * Get the SKU of the product that the user has selected (not purchased yet).
     *
     * @return The SKU (unique product ID) of the selected product to be purchased.
     */
    public abstract String getSelectedProductSku();

    /**
     * Method to handle views initialization which is called during onCreate
     */
    public abstract void initializeViews();

    protected boolean isPurchaseSuccess;
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        // HJ TODO: Upgrade targetSdkVersion to 23 & deal with app-wide permission handling in another ticket
//        updateGoogleEmail();
        isPurchaseSuccess= false;
        mProgressDialogUtil = new ProgressDialogUtil(this);

        initializeViews();
    }

    protected void updateGoogleEmail() {
        if (TextUtils.isEmpty(mGoogleEmail)) {
            mGoogleEmail = AccountUtil.retrieveUserGoogleEmail(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mProgressDialogUtil.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentOngingTokenId=null;
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,false);
    }
    private void addToEvents(String event,String value){
        EventsListHelper.Companion.getInstance().add(UpgradeActivity.class.getSimpleName(),event,value);
    }

    @Override
    protected void onStop() {
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,true);
        super.onStop();
        EventsListHelper.Companion.getInstance().set();
    }

    @Override
    protected String getDeveloperPayload() {
        Logger.logI(TAG, "getDeveloperPayload");
        String payload="{"+HardwareId.get(UpgradeActivity.this)+","+ GeneralPrefs.getEmailId(UpgradeActivity.this)+"}";
        addToEvents("getDeveloperPayload",payload);
        return payload;
//        updateGoogleEmail();
//        if (TextUtils.isEmpty(mGoogleEmail)) {
//            return null;
//        }
//
//        return LicenseUtil.generatePurchaseExtraInfo(mGoogleEmail);
    }

    @Override
    protected void complain(IabResult result) {
        Logger.logI(TAG, "complain " + result);

        switch (result.getResponse()) {
            case ERROR_BILLING_UNAVAILABLE: // no google account found
            case ERROR_QUERY_PRICE: // error refreshing inventory
                onPriceAvailable(null);
                break;
            default:
                // show purchase failed screen
                UpgradeResultsActivity.newInstance(this, false, getString(R.string.license_buy_error));
                break;
        }
    }

    @Override
    protected void onInventoryAvailable(Inventory inventory) {
        Logger.logI(TAG, "onInventoryAvailable " + inventory);
        if (inventory == null) return;

        // prices for each product is now available
        mInventory = inventory;
        List<String> skuList = getSkuList();
        for (String sku : skuList) {
            onPriceAvailable(mInventory.getSkuDetails(sku));
        }
    }

    @Override
    protected void setWaitScreen(boolean b) {
        Logger.logI(TAG, "setWaitScreen " + b);
    }

    @Override
    protected Collection<String> getManagedSKUs() {
        Logger.logI(TAG, "getManagedSKUs");
        return getSkuList();
    }

    /**
     * Triggered whenever a buy product action is made by the user.
     */
    public void onBuyClick() {
        addToEvents("onBuyClick","");
        Logger.logD(TAG, "onBuyClick ");

        // we registered/logged user in to MYA successfully, start purchase flow
        launchPurchaseItem(getSelectedProductSku());
//        launchPurchaseItem(getSelectedProductSku());
//        Tracking.trackPurchaseStarted(runtime, deviceType == SUBSCRIPTION_ONE_DEVICE ? PLATFORM_SPECIFIC : PLATFORM_ALL);
    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
        addToEvents("onIabPurchaseFinished","IabResult "+(result!=null?result.toString():" null ")+
                " Purchase "+(purchase!=null?purchase.toString():" null purchase "));
        addToEvents("PurchaseToken",""+(purchase!=null?purchase.getToken():" null purchase"));
        if(result!=null && result.getResponse() == IabHelper.IABHELPER_USER_CANCELLED){
            super.onIabPurchaseFinished(result, purchase);
            return;
        }
            if(purchase!=null){
                new StoreSubscriptionCreatedNonFetal(false,purchase.getSku());
                isPurchaseSuccess= true;
                sendPurchaseToBackend(purchase);
        }
    }

    @Override
    protected void onPurchaseCompleted(Purchase purchase) {
        Logger.logD(TAG, "onPurchaseCompleted " + purchase);
        addToEvents("onPurchaseCompleted"," Purchase "+(purchase!=null?purchase.toString():" empty purchase"));


        // user completed purchase, send info to OE backend to process purchase.
        // Response will be received in onProcessPurchaseSuccessful or onProcessPurchaseError
//        mProgressDialogUtil.show(getString(R.string.QueryingInformationFromServer));
//        sendPurchaseToBackend(purchase);
//        Tracking.trackPurchaseComplete(runtime, deviceType == SUBSCRIPTION_ONE_DEVICE ? PLATFORM_SPECIFIC : PLATFORM_ALL, seenOtherOffer);
    }

    private String currentOngingTokenId;
    protected void sendPurchaseToBackend(Purchase purchase) {
        addToEvents("sendPurchaseToBackend"," Purchase "+(purchase!=null?purchase.toString():" empty purchase"));
//        if(purchaseItemStartsConsumed){
//            return;
//        }
//        purchaseItemStartsConsumed=true;
        if(purchase==null){return;}

        Logger.logE("Purchasing",""+purchase.toString());

        if(currentOngingTokenId !=null && currentOngingTokenId.equals(purchase.getToken())){
            return;
        }
        currentOngingTokenId =purchase.getToken();
        mProgressDialogUtil.show(getString(R.string.QueryingInformationFromServer));

        if(purchase.getToken().contains(Purchase.tempPurchase)){
            UserPref.getInstance().setTempPurchaseToken(purchase.getToken());
        }else {
            UserPref.getInstance().setTempPurchaseToken(null);
        }
        createSubc(purchase);
        VpnHomeFragment.isToHitCanclePromo = true;

                      //  isToHitCanclePromo = false;
                    SubscriptionHelper.getInstance().cancelAllPromos(this, false);

    }

    public void createSubc(final Purchase purchase){
        addToEvents("createSubc"," Purchase "+(purchase!=null?purchase.toString():" empty purchase"));
        final String userName=AuthenticationHelper.getInstnace().getUserName(UpgradeActivity.this);
        SubscriptionHelper.getInstance().createSubs(FinjanVPNApplication.getInstance(),
                purchase, userName, new NetworkResultListener() {
                    @Override
                    public void executeOnSuccess(JSONObject response) {


                        addToEvents("ULM create Subsc success"," Response "+(response!=null?response.toString():" empty response"));
                        if(UpgradeActivity.this.currentOngingTokenId !=null){
                            UpgradeActivity.this.currentOngingTokenId =null;
                        }
                        String requst=JsonCreater.getInstance().getCreateSubscription(purchase, userName).toString();
                        new ULMSubscriptionCreatedNonFetal(false,purchase.getSku(),response.toString(), requst,false);
                        if(mProgressDialogUtil!=null){
                            mProgressDialogUtil.dismiss();
                        }

                        try {
                            String msg=response.getString("message");
                            Logger.logE(TAG,msg);
                            backUpdated(true,msg);
                            EventBus.getDefault().post(new SubscriptionUpdated());
                            VpnApisHelper.Companion.getInstance().updateLicence(true,true, UpgradeActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void executeOnError(VolleyError error) {
                        addToEvents("ULM create Subsc failed"," Error "+(error!=null?WebUtility.getMessage(error):" empty error"));

                        Logger.logE(TAG, WebUtility.getMessage(error));

                        if(error instanceof NetworkError|| error instanceof NoConnectionError || error instanceof TimeoutError){
                            ScheduleHitConfig.getInstance().schedulePurchase(purchase,userName);
                            ToastHelper.showToast(UpgradeActivity.this,ScheduleHItMsg,Toast.LENGTH_LONG);
                            return;
                        }else {

                           new  FinjanDialogBuilder(UpgradeActivity.this)
                                   .setTitle("Error")
                                   .setMessage("Your Subscription is not created yet please contact support team")
                                   .setPositiveButton("Retry")
                                   .setNegativeButton(ResourceHelper.getInstance().getString(R.string.alert_cancel))
                                   .setCustomNegativeBtnColour(PlistHelper.IAPLaterButtonBg)
                                   .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                               @Override
                               public void onPositivClick() {
                                   createSubc(purchase);
                               }
                           }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                               @Override
                               public void onNegativeClick() {

                               }
                           }).show();

                        }


                        if(UpgradeActivity.this.currentOngingTokenId !=null){
                            UpgradeActivity.this.currentOngingTokenId =null;
                        }
                        if(mProgressDialogUtil!=null){
                            mProgressDialogUtil.dismiss();
                        }
                        ToastHelper.showToast(UpgradeActivity.this,error,Toast.LENGTH_LONG);
                    }
                });
    }
    private static final String ScheduleHItMsg="We are updating ur purchase, Soon you licence will update too";


    @Override
    public void onProcessPurchaseSuccessful(final boolean success) { // MYA process IAB purchase success
        Logger.logD(TAG, "onProcessPurchaseSuccessful " + success);
        addToEvents("onProcessPurchaseSuccessful"," success "+success);

        mProgressDialogUtil.dismiss();
        setResult(RESULT_OK);

        NetworkManager.getInstance(getActivity()).fetchLicence(true,getActivity(), new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                JsonConfigParser.parseJsonLicense(FinjanVPNApplication.getInstance(), response);
                UpgradeResultsActivity.newInstance(UpgradeActivity.this, success, success
                        ? getString(R.string.license_buy_success)
                        : getString(R.string.license_buy_error));
                finish();
            }

            @Override
            public void executeOnError(VolleyError error) {
                Toast.makeText(UpgradeActivity.this, getString(R.string.iab_error_try_again_later), Toast.LENGTH_LONG).show();
                UpgradeResultsActivity.newInstance(UpgradeActivity.this, false, getString(R.string.iab_error_try_again_later));
                finish();
            }
        });

//        Tracking.trackEvent(Tracking.EVENT_UPGRADE);
    }

    @Override
    public void onProcessPurchaseError(int errorCode, String errorMessage) { // MYA process IAB purchase error
        Logger.logE(TAG, "onProcessPurchaseError " + errorMessage);
        addToEvents("onProcessPurchaseError"," errorMessage "+errorMessage);

        mProgressDialogUtil.dismiss();
        setResult(RESULT_CANCELED);
        UpgradeResultsActivity.newInstance(this, false, getString(R.string.license_buy_error));
        finish();
    }

    @Override
    protected void onConsumeCompleted(Purchase purchase) {
        Logger.logE(TAG, "onConsumeCompleted " + purchase);
        // no consuming takes place - the stuff is bought forever
    }

    public void onDebugClearPurchases() {
        Logger.logD(TAG, "onDebugClearPurchases ");
        if (mInventory != null) {
            consumePurchases(mInventory.getAllPurchases());
        }
    }

    /**
     * @return true - the dialog was shown => no connectivity
     * false - the dialog was not shown => there is connectivity
     */
    protected boolean showNoNetworkDialogIfNoConnectivity() {
        Logger.logD(TAG, "showNoNetworkDialogIfNoConnectivity ");
        if (!NetworkConnectionManager.hasNetworkConnectivity(getActivity())) {
            if(getActivity()!=null){
                Toast.makeText(getActivity(), "Connect to a mobile or Wi-Fi network to continue.",Toast.LENGTH_LONG).show();
            }
//            CommonDialogs.showNetworkUnavailable(getActivity());
            return true;
        }
        return false;
    }
    protected abstract void backUpdated(boolean success,String msg);

}