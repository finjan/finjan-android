//package com.finjan.securebrowser.a_vpn.ui.about;
//
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.os.Bundle;
//import android.support.v4.app.FragmentActivity;
//import android.text.TextUtils;
//import android.view.View;
//import android.widget.TextView;
//
//import com.avira.vpn.R;
//
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//
//public class AboutActivity extends FragmentActivity {
//
//    private static final String TAG = AboutActivity.class.getSimpleName();
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_about);
//        ButterKnife.bind(this);
//
//        TextView tvVersion = (TextView) findViewById(R.id.tv_version);
//
//        try {
//            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//            String version = pInfo.versionName;
//            int buildNo = pInfo.versionCode;
//
//            if(TextUtils.isEmpty(version)||buildNo == 0){
//                tvVersion.setVisibility(View.GONE);
//            }else{
//                tvVersion.setText(getString(R.string.version, version, buildNo));
//            }
//
//        }catch (PackageManager.NameNotFoundException e){
//            tvVersion.setVisibility(View.GONE);
//        }
//
//    }
//
//    @OnClick(R.id.btn_back)
//    public void actionOnViewTapped(View viewTapped) {
//
//        switch (viewTapped.getRegion()) {
//            case R.id.btn_back:
//                finish();
//                break;
//        }
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        ButterKnife.unbind(this);
//    }
//}
