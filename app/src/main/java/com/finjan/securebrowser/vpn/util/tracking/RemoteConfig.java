package com.finjan.securebrowser.vpn.util.tracking;

import androidx.annotation.NonNull;

import com.avira.common.utils.SharedPreferencesUtilities;
import com.finjan.securebrowser.BuildConfig;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.ui.iab.LicenseUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.HashMap;

/**
 * firebase remote configuration wrapper
 * <p>
 * TODO: replace callback with something asynchronous for better thread management
 *
 * @author ovidiu.buleandra
 * @since 23.06.2016
 */

public class RemoteConfig {

    private static String TAG = RemoteConfig.class.getSimpleName();
    private static final String BLACK_LIST_TAG = "trackingBlackList";
    public static final String AVAILABLE_FOR_PURCHASE_TAG = "availableForPurchase";
    private static final String DEFAULT_AVAILABLE_FOR_PURCHASE_TAG = "[{\n" +
            "\t\"mya\": \"vpna0\",\n" +
            "\t\"sku\": \"vpna0\",\n" +
            "\t\"type\": \"monthly\",\n" +
            "\t\"platform\": \"android\"\n" +
            "\n" +
            "}, {\n" +
            "\t\"mya\": \"avpp0\",\n" +
            "\t\"sku\": \"avpp1\",\n" +
            "\t\"type\": \"monthly\",\n" +
            "\t\"platform\": \"all\"\n" +
            "\n" +
            "}, {\n" +
            "\t\"mya\": \"avpp0\",\n" +
            "\t\"sku\": \"avpp0\",\n" +
            "\t\"type\": \"yearly\",\n" +
            "\t\"platform\": \"all\"\n" +
            "}]";
    private static final long REMOTE_CONFIG_CACHE_EXPIRE = 24 * 3600L; // one day


    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    public RemoteConfig() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        final HashMap<String, Object> defaults = new HashMap<>();
        defaults.put(AVAILABLE_FOR_PURCHASE_TAG, DEFAULT_AVAILABLE_FOR_PURCHASE_TAG);
        mFirebaseRemoteConfig.setDefaults(defaults);
    }

    public void update() {
        mFirebaseRemoteConfig
                .fetch(mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled() ? 0 : REMOTE_CONFIG_CACHE_EXPIRE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Logger.logD(TAG, "FirebaseRemoteConfig update success");
                            mFirebaseRemoteConfig.activateFetched();
                            // save black list config
                            String blackListJSON = mFirebaseRemoteConfig.getString(BLACK_LIST_TAG);
//                            saveBlacklistedEvents(blackListJSON);
                            // save available for purchases config
                            saveAvailableForPurchase(mFirebaseRemoteConfig.getString(AVAILABLE_FOR_PURCHASE_TAG));
                            // clean old data for reinitialization
                            LicenseUtil.clearPurchaseConfig();
                        } else {
                            Logger.logE(TAG, "failed update");
                        }
                    }
                });
    }

    /**
     * save all blacklisted events to a shared preferences
     */
    private void saveBlacklistedEvents(String events) {
//        SharedPreferencesUtilities.putString(FinjanVPNApplication.getInstance(),
//                TrackingConfigurations.TRACKING_BLACK_LIST, events);
//        TrackingConfigurations.clearBlackList();
    }

    /**
     * save all saveAvailableForPurchase to a shared preferences
     */
    private void saveAvailableForPurchase(String availableForPurchase) {
        SharedPreferencesUtilities.putString(FinjanVPNApplication.getInstance(), AVAILABLE_FOR_PURCHASE_TAG, availableForPurchase);
    }
}
