package com.finjan.securebrowser.vpn.licensing.models.restful;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovidiu.buleandra
 * @since 05.11.2015
 */
public class DataContainer implements GSONModel {
    @SerializedName("data")
    private Resource data;

    public Resource getData() {
        return data;
    }
}
