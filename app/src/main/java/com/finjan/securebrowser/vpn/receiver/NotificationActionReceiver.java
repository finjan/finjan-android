/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationManagerCompat;

import android.widget.Toast;

import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
//import com.finjan.securebrowser.a_vpn.controller.mixpanel.Tracking;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.VpnUtil;

/**
 * Class to handle the actions performed by the user on our notifications.
 * Created by hui-joo.goh on 18/11/16.
 */
public class NotificationActionReceiver extends BroadcastReceiver {

    private static final String TAG = NotificationActionReceiver.class.getSimpleName();

    public static final String EXTRA_SSID = "extra_ssid";

    public static final String ACTION_WIFI_CHANGED_NOTIF_POSITIVE = "com.avira.vpn.action.WIFI_CHANGED_NOTIF_POSITIVE";
    public static final String ACTION_WIFI_CHANGED_NOTIF_NEGATIVE = "com.avira.vpn.action.WIFI_CHANGED_NOTIF_NEGATIVE";
    public static final String ACTION_INSECURE_NOTIF_POSITIVE = "com.avira.vpn.action.INSECURE_WIFI_NOTIF_POSITIVE";
    public static final String ACTION_INSECURE_NOTIF_NEGATIVE = "com.avira.vpn.action.INSECURE_WIFI_NOTIF_NEGATIVE";

    public static final int NOTIF_WIFI_CHANGED_ID = 12355;
    public static final int NOTIF_INSECURE_WIFI_ID = 12311;
    public static final int NOTIF_TRAFIC_EXAUSTED = 12319;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        switch (action) {
            case ACTION_INSECURE_NOTIF_POSITIVE:
                Logger.logD(TAG, "Insecure wifi notif - Connect VPN clicked");
//                Tracking.trackNotificationAction(Tracking.VALUE_INSECURE_WIFI, true);
                clearNotification(context, NOTIF_INSECURE_WIFI_ID);
                Utils.connectToVpn(context,"");
                break;

            case ACTION_INSECURE_NOTIF_NEGATIVE:
                Logger.logD(TAG, "Insecure wifi notif - Not now clicked");
//                Tracking.trackNotificationAction(Tracking.VALUE_INSECURE_WIFI, false);
                clearNotification(context, NOTIF_INSECURE_WIFI_ID);
                break;

            case ACTION_WIFI_CHANGED_NOTIF_POSITIVE:
                Logger.logD(TAG, "Wifi changed notif - Connect VPN clicked");
//                Tracking.trackNotificationAction(Tracking.VALUE_NEW_WIFI, true);
                clearNotification(context, NOTIF_WIFI_CHANGED_ID);
                Utils.connectToVpn(context,"");

                // since it is a custom button, we need to manually dismiss status bar
                Intent dismissStatusBar = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                context.sendBroadcast(dismissStatusBar);
                break;

            case ACTION_WIFI_CHANGED_NOTIF_NEGATIVE:
                Logger.logD(TAG, "Wifi changed notif - dismissed");
//                Tracking.trackNotificationAction(Tracking.VALUE_NEW_WIFI, false);
                FinjanVpnPrefs.whiteListWifi(context, intent.getStringExtra(EXTRA_SSID));
                clearNotification(context, NOTIF_WIFI_CHANGED_ID);
                break;
        }
    }

    private void connectToVpn(Context context) {

        // get last used server id
        String serverRegion = FinjanVpnPrefs.getLastUsedServer(context);
        long serverId = new ContentProviderClient().getServerId(context, serverRegion);
        TrafficController trafficController = TrafficController.getInstance(context);
        long trafficLimit = trafficController.getTrafficLimit();
        long remainingTraffic = trafficLimit - trafficController.getTraffic();
        boolean hasVpnTraffic = (remainingTraffic > 0) || /*(trafficLimit == 0)*/TrafficController.getInstance(context).isPaid(); // limit 0 = limitless traffic


        // connect to vpn
        if (hasVpnTraffic)
            VpnUtil.startVpnConnection(context,serverId,true);
        else
            Toast.makeText(context, "You have run out of your monthly free VPN data and are no longer protected. Update now to our unlimited plan to remain protected!", Toast.LENGTH_LONG).show();
    }

    private void clearNotification(Context context, int notifId) {
        Logger.logD(TAG, "clearNotification " + notifId);
        NotificationManagerCompat.from(context).cancel(notifId);
    }
}
