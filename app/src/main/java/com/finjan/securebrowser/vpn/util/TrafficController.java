/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.avira.common.database.Settings;
import com.finjan.securebrowser.BuildConfig;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.JsonConfigParser;
//import com.finjan.securebrowser.a_vpn.controller.mixpanel.Tracking;
import com.finjan.securebrowser.vpn.eventbus.ActionNotificationEvent;
import com.finjan.securebrowser.vpn.service.AviraOpenVpnService;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;

import de.blinkt.openvpn.core.VpnStatus;
import de.greenrobot.event.EventBus;

public class TrafficController {

    public static final String TAG = TrafficController.class.getSimpleName();
    private static final int MORE_TRAFFIC_ID = 101;
    public static final String LICENSE_UNREGISTERED = "unregistered";
    public static final String LICENSE_FREE = "free";
    public static final String LICENSE_PAID = "paid";

    private static TrafficController INSTANCE = new TrafficController();
    private static SharedPreferences mSharedPreferences;

    public static final String VPN_TRAFFIC_USED = "VPN_TRAFFIC_USED";
    public static final String VPN_TRAFFIC_LIMIT = "VPN_TRAFFIC_LIMIT";
    private long mNewTotalTraffic = 0l;
    private static final long ONE_MD = 1024 * 1024;
    public static final long DEFAULT_TRAFFIC_LIMIT = 1024 * 1024 * 1024;
    private static final long BOUND_TRAFFIC_LIMIT = BuildConfig.DEBUG ? 500 * 1024 : 60 * ONE_MD;
    private static final long DEFAULT_TIME_SPAN_TRAFFIC_LIMIT = 60 * 1000; // 60 seconds

    private Thread timerThread = null;
    private boolean stopTimer = false;

    private TrafficController() {
    }

    public static TrafficController getInstance(@NonNull Context context) {
        if (mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }

        return INSTANCE;
    }

    public void saveTraffic(long usedTraffic) {
        usedTraffic = usedTraffic  * PlistHelper.getInstance().getDataGaugeMultiplier();
        saveTraffic(VPN_TRAFFIC_USED, usedTraffic);
    }
    public void clearConsumedTraffic(){
        saveTraffic(VPN_TRAFFIC_USED, 0);
    }

    public void applyNewTraffic() {
        saveTraffic(VPN_TRAFFIC_USED, getTraffic() + mNewTotalTraffic);
        mNewTotalTraffic = 0;
        AppConfig.LocalDataHelper.Companion.updateConsumedData(getTraffic());
    }

    public void updateNewTraffic(long traffic) {
        traffic = traffic  * PlistHelper.getInstance().getDataGaugeMultiplier();
        mNewTotalTraffic = traffic;
    }

    public long getNewTraffic() {
        return mNewTotalTraffic;
    }

    public void saveTrafficLimit(long trafficLimit) {
        Logger.logD(TAG, "traffic limit " + trafficLimit);
        saveTraffic(VPN_TRAFFIC_LIMIT, trafficLimit);
    }


    private String getRoundoffValue(String actuallString){
        if (!TextUtils.isEmpty(actuallString) && !actuallString.startsWith("0")) {
//            return actuallString;
            String[] a = actuallString.split(" ");
            if(a[0].contains(".")){
                a[0] = a[0].replace(a[0].substring(a[0].indexOf(".")
                        ,a[0].length()),"");
            }
            if(a.length>1){
                return a[0]+" "+a[1];
            }else return a[0];
        }
        return "0 MB";
    }
    public String getOnlyMbs(String actuallString){
        if (!TextUtils.isEmpty(actuallString) && !actuallString.startsWith("0")) {
//            return actuallString;
            String[] a = actuallString.split(" ");
            if(a[0].contains(".")){
                a[0] = a[0].replace(a[0].substring(a[0].indexOf(".")
                        ,a[0].length()),"");
            }
            if(a.length>1){
                if(a[1].equalsIgnoreCase("gb")){
                    return "1024 MB";
                }
                return a[0]+" "+a[1];
            }else return a[0];
        }
        return "0 MB";
    }
    public String getRemainingTraffic(){

        long bal = getTrafficLimit() - (getTraffic()+ getNewTraffic());
        if(bal>0){
            return getOnlyMbs(Util.humanReadableByteCount(bal,false));
        }
        return "0 MB    ";
    }
    public String getConsumedTraffic(){
        long bal = (getTraffic()+ getNewTraffic());
        if(bal>0){
            return getOnlyMbs(Util.humanReadableByteCount(bal,false));
        }
        return "0 MB    ";
    }
    public String getTotalTraffic(){
        long bal= getTrafficLimit();
        if(bal>0){
            return getOnlyMbs(Util.humanReadableByteCount(bal,false));
        }
        return "0 MB    ";
    }



    public long getTraffic() {
        return mSharedPreferences.getLong(VPN_TRAFFIC_USED, 0l);
    }

    public long getTrafficLimitBound() {
        return BOUND_TRAFFIC_LIMIT;
    }

    public long getTrafficLimitTimeSpan() {
        return mSharedPreferences.getLong(JsonConfigParser.GRACE_PERIOD, DEFAULT_TIME_SPAN_TRAFFIC_LIMIT);
    }

    public void saveTrafficLimitTimeSpan(long gracePeriod) {
        SharedPreferences.Editor prefsedit = mSharedPreferences.edit();
        prefsedit.putLong(JsonConfigParser.GRACE_PERIOD, gracePeriod);
        prefsedit.apply();
    }

    public void saveLicenseType(String type) {
        SharedPreferences.Editor prefsedit = mSharedPreferences.edit();
        prefsedit.putString(JsonConfigParser.TYPE, type);
        prefsedit.commit();
    }

    @Nullable
    public String getLicenseType() {
        return mSharedPreferences.getString(JsonConfigParser.TYPE, null);
    }

    public boolean isRegistered() {
        String license = mSharedPreferences.getString(JsonConfigParser.TYPE, null);
        return license != null && (!LICENSE_UNREGISTERED.equals(license));
    }

    /**
     * check is user has paid license
     */
    public boolean isPaid() {
        String license = mSharedPreferences.getString(JsonConfigParser.TYPE, null);
        return license != null && LICENSE_PAID.equals(license);
    }

    public void saveExpirationDate(String expirationDate) {
        SharedPreferences.Editor prefsedit = mSharedPreferences.edit();
        prefsedit.putString(JsonConfigParser.EXPIRATION_DATE, expirationDate);
        prefsedit.apply();
    }
    public String getLicenseExpiry(){
        return mSharedPreferences.getString(JsonConfigParser.EXPIRATION_DATE,"");
    }


    public long getTrafficLimit() {
        return mSharedPreferences.getLong(VPN_TRAFFIC_LIMIT, DEFAULT_TRAFFIC_LIMIT);
    }

    public void clearHistory(){
        SharedPreferences.Editor prefsedit = mSharedPreferences.edit();
        prefsedit.remove(VPN_TRAFFIC_LIMIT);
        prefsedit.remove(JsonConfigParser.EXPIRATION_DATE);
        prefsedit.remove(JsonConfigParser.TYPE);
        prefsedit.putString(JsonConfigParser.TYPE, LICENSE_UNREGISTERED);
        prefsedit.remove(JsonConfigParser.GRACE_PERIOD);
//        prefsedit.remove(VPN_TRAFFIC_USED);

        prefsedit.remove(FinjanVpnPrefs.AUTHORIZATION_TOKEN);
        prefsedit.remove(FinjanVpnPrefs.REFRESH_TOKEN);

        prefsedit.commit();
        Settings.deleteTable();
    }
    /**
     * Stop traffic limit timer
     */
    public void stopTimer() {
        stopTimer = true;
    }

    public void runTrafficLimitTimer(final AviraOpenVpnService openVPNService, final Context context, long traffic) {
        if (timerThread == null) {
            stopTimer = false;
            timerThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    long future = System.currentTimeMillis() + getTrafficLimitTimeSpan();
                    long delta = future - System.currentTimeMillis();
                    synchronized (this) {
                        while (delta >= 0 && !stopTimer) {
                            openVPNService.showNotification(context.getString(R.string.traffic_limit_notification_disconnect) + (delta / 1000), null, false, 0, VpnStatus.ConnectionStatus.LEVEL_CONNECTED);
                            try {
                                wait(1000);
                            } catch (InterruptedException e) {
                                Logger.logE(TAG, "Time countdown error"+e);
                            }
                            delta = future - System.currentTimeMillis();
                        }
                    }

                    // disconnect vpn
//                    Tracking.trackEvent(Tracking.EVENT_TRAFFIC_LIMIT_REACHED);
                    VpnUtil.stopVpnConnection(context, openVPNService);

                    timerThread = null;
                    showDisconnectMessage(context);
                }
            });
            timerThread.start();
        } else {
            if (traffic > (getTrafficLimit() + getTrafficLimitBound())) {
                stopTimer();
            }
        }
    }

    /**
     * Show Snackbar message for disconnect action. Start VpnActivity if needed
     *
     * @param context
     */
    private void showDisconnectMessage(Context context) {
        if (VpnActivity.I_AM_ALIVE) {
            EventBus.getDefault().post(new ActionNotificationEvent(R.string.traffic_limit_bound_message));
        } else {
            Intent intent = new Intent(context, VpnActivity.class);
            intent.setAction(VpnActivity.ACTION_DISCONNECTED);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    private void saveTraffic(@NonNull String trafficType, long trafficData) {
        SharedPreferences.Editor prefsedit = mSharedPreferences.edit();
        prefsedit.putLong(trafficType, trafficData);
        prefsedit.apply();
    }


    public boolean isLicenseDateValid(){
        long currentDate = System.currentTimeMillis();
        long licenseDate = DateHelper.getInstnace().getServerDateCalender(TrafficController.getInstance(FinjanVPNApplication.getInstance()).getLicenseExpiry(),DateHelper.SERVER_FORMAT_2).getTimeInMillis();
        return AppConfig.Companion.getUseFinjanLicenseApii()? true :DateHelper.getInstnace().isFutureDate(licenseDate+TrafficController.getInstance(FinjanVPNApplication.getInstance()).getTrafficLimitTimeSpan(),currentDate);
    }

//    public void showRegisterUserNotification(Context context) {
//        String ns = Context.NOTIFICATION_SERVICE;
//        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(ns);
//
//        Notification.Builder nbuilder = new Notification.Builder(context);
//        nbuilder.setContentTitle(context.getString(R.string.app_name));
//
//        nbuilder.setContentText(context.getString(R.string.notification_description_increase_traffic_limit));
//        nbuilder.setOnlyAlertOnce(true);
//        Intent intent = new Intent(context, VpnActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//        intent.setAction(VpnActivity.ACTION_REGISTER_DIALOG);
//        PendingIntent startMainActivity = PendingIntent.getActivity(context, 0, intent, 0);
//
//        nbuilder.setContentIntent(startMainActivity);
//        // TODO add another icon
//        nbuilder.setSmallIcon(R.mipmap.ic_launcher);
//
//
//        Notification notification = null;
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            notification = nbuilder.build();
//        } else {
//            notification = nbuilder.getNotification();
//        }
//
//        mNotificationManager.notify(MORE_TRAFFIC_ID, notification);
//    }
}
