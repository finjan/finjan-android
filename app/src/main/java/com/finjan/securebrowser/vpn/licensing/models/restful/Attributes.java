package com.finjan.securebrowser.vpn.licensing.models.restful;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovidiu.buleandra
 * @since 05.11.2015
 */
public class Attributes implements GSONModel {

    @SerializedName("key") private String key;
    @SerializedName("type") private String type;
    @SerializedName("devices_limit") private int devicesLimit;
    @SerializedName("runtime") private int runtime;
    @SerializedName("runtime_unit") private String runtimeUnit;
    @SerializedName("expiration_date") private String expirationDate;

    public String getKey() { return key; }
    public void setKey(String newKey) { key = newKey; }
    public String getType() { return type; }
    public int getDevicesLimit() { return devicesLimit; }
    public int getRuntime() { return runtime; }
    public String getRuntimeUnit() { return runtimeUnit; }
    public String getExpirationDate() { return expirationDate; }
    public void setExpirationDate(String expDate) { expirationDate = expDate; }
}
