package com.finjan.securebrowser.vpn.licensing.utils;

/**
 * Created by Illia.Klimov on 8/5/2016.
 */
public class SubscriptionVPN {
    private String subscriptionType;
    private int runtime;

    private static final String TYPE_MONTHLY = "monthly";
    private static final String TYPE_YEARLY = "yearly";

    public static final int RUNTIME_MONTHLY = 1;
    public static final int RUNTIME_YEARLY = 12;

    public SubscriptionVPN(int runtime) {
        this.runtime = runtime;
        switch (runtime) {
            case RUNTIME_MONTHLY:
                subscriptionType = TYPE_MONTHLY;
                break;
            case RUNTIME_YEARLY:
                subscriptionType = TYPE_YEARLY;
                break;
        }
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public int getRuntime() {
        return runtime;
    }
}
