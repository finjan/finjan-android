package com.finjan.securebrowser.vpn.licensing;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.avira.common.GeneralPrefs;
import com.avira.common.backend.WebUtility;
import com.finjan.securebrowser.vpn.licensing.models.server.TrialResponse;

/**
 * @author ovidiu.buleandra
 * @since 23.11.2015
 */
public class TrialRequestInternalCallback implements Response.Listener<TrialResponse>, Response.ErrorListener {

    private TrialRequestCallback mCallback;
    private String mAcronym;
    private Context mContext;

    public TrialRequestInternalCallback(Context context, String productAcronym, TrialRequestCallback callback) {
        if (callback == null)
            throw new IllegalArgumentException("passed callback shouldn't be null");
        mContext = context;
        mCallback = callback;
        mAcronym = productAcronym;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mCallback.onTrialRequestError(WebUtility.getHTTPErrorCode(error), WebUtility.getMessage(error));
    }

    @Override
    public void onResponse(TrialResponse response) {
        if (response.isSuccess() && response.getSubscription() != null && response.getSubscription().getEnabled()) {
            GeneralPrefs.setAppId(mContext, mAcronym);
            mCallback.onTrialRequestSuccessful(mAcronym, response.getSubscription());
        } else {
            mCallback.onTrialRequestError(response.getStatusCode(),
                    String.format("[%d] %s", response.getStatusCode(), response.getStatus()));
        }
    }
}
