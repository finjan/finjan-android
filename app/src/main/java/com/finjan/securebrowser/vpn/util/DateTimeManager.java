package com.finjan.securebrowser.vpn.util;

import android.util.Log;


import com.finjan.securebrowser.BuildConfig;
import com.finjan.securebrowser.helpers.logger.Logger;

import java.util.concurrent.TimeUnit;

/**
 * Created by illia.klimov on 03.04.17.
 */

public class DateTimeManager {
    private static final String TAG = DateTimeManager.class.getName();
    private static final String DEFAULT_TIMESTAMP_FORMAT = "yyyy-MM-dd hh:mm:ss";

    /*
     * Get timestamp in given format
     *
     * @param format Timestamp format
     *
     * @return Formatted timestamp
     */
    public static String getTimestamp(String format)
    {
        return (String) android.text.format.DateFormat.format(format, new java.util.Date());
    }

    /*
     * Get timestamp in default format
     *
     * @return Formatted timestamp
     */
    public static String getTimestamp()
    {
        return getTimestamp(DEFAULT_TIMESTAMP_FORMAT);
    }

    /**
     * NOTE: In debug mode, the time unit will be returned in MINUTES for quicker testing.
     *
     * @param timeUnit the time unit to apply on the specified duration
     * @param duration the duration
     * @return The calculated duration in millis based on the specified time unit
     */
    public static long getMillis(TimeUnit timeUnit, long duration) {
        long millis = timeUnit.toMillis(duration);

        if (BuildConfig.DEBUG) { // modify hours to minutes for quicker testing

            switch (timeUnit) {
                case DAYS:
                    // convert days to hours then fallthrough HOURS case below
                    duration = TimeUnit.DAYS.toHours(duration); // e.g. 1 day = 24h
                    timeUnit = TimeUnit.HOURS;
                case HOURS:
                    millis = TimeUnit.MINUTES.toMillis(duration);
                    break;
            }
        }

        Logger.logD(TAG, duration + " " + timeUnit + " getMillis: " + millis);
        return millis;
    }
}
