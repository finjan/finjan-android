package com.finjan.securebrowser.vpn.social;

import android.text.TextUtils;

/**
 * Created by navdeep on 28/2/18.
 */

public class SocialUserProfile {

    public static final String FB_USER = "fb";
    public static final String GG_USER = "gg";
    public static final String Email_USER = "email";

    private String firstName,lastName,email,socialType;
    private String socialId;

    public String getToken_id() {
//        if(token_id=="429552714162294"){return "4295527141622941";}
//        return token_id==null?getRandomToken():token_id;
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    private String token_id;

    public String getSocialId() {
        return socialId;
    }

    /**
     *
     429552714162294

     could you please remove these two user
     * @param socialId
     */

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getSocialType() {
        return socialType;
    }

    public void setSocialType(String socialType) {
        this.socialType = socialType;
    }

    public SocialUserProfile(){}
    public SocialUserProfile(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        if(TextUtils.isEmpty(lastName)){
            lastName="";
        }
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
//        return "scot@scotrobinson.com";
    }

    public void setEmail(String email) {
//        if(TextUtils.isEmpty(email)){
//            email=socialType.equals("fb_id")?socialId+"@facebook-social.com":socialId+"@google-social.com";
//        }
        this.email = email;
//        this.email = "scot@scotrobinson.com";
    }
    public void makeSocialEmail(){
        if(!emailEndsWithSocialId()){
            String socialTag=socialType.equals("fb_id")?"@facebook-social.com":"@google-social.com";
            email=socialId+socialTag;
        }
    }

    public boolean emailEndsWithSocialId(){
        return (email.endsWith("@facebook-social.com") || email.endsWith("@google-social.com"));
    }
    public boolean isSocialFb(){
//        return (email.endsWith("@facebook-social.com"));
        return (socialType.equals("fb_id"));
    }
    public boolean isSocialGoogle(){
        return email.endsWith("@google-social.com");
    }


    public String getRandomToken(){
        return "4295527"+System.currentTimeMillis();
    }



    public  SocialUserProfile getScotsUser(){
        SocialUserProfile scotUser= new SocialUserProfile();
        scotUser.setEmail("scot@scotrobinson.com");
        scotUser.setFirstName("Scot Robinson");
        scotUser.setLastName("");
        scotUser.setSocialId("10155208647842074");
        scotUser.setSocialType("fb_id");
        scotUser.setToken_id(this.token_id);
        return scotUser;
    }
}
