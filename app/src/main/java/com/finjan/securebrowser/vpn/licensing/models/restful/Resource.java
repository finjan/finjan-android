package com.finjan.securebrowser.vpn.licensing.models.restful;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovidiu.buleandra
 * @since 05.11.2015
 */
public class Resource implements GSONModel {
    @SerializedName("relationships")
    protected Relationships relationships;
    @SerializedName("attributes")
    protected Attributes attributes;
    @SerializedName("type")
    protected String type;
    @SerializedName("id")
    protected String id;

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }
}
