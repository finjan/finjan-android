package com.finjan.securebrowser.vpn.licensing.utils;

import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.models.restful.License;
import com.finjan.securebrowser.vpn.ui.iab.LicenseUtil;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by anurag on 13/07/17.
 */

public class PurchaseHelper {

    private static final String SERVER_FORMAT = "yyyy-MM-dd hh:mm:ss";
    private static final String INTERVAL_MONTH = "month";
    private static final String DEVICE_ALL = "all";
    private static final String DEVICE_ANDROID = "android";

    //                1. Single Device Monthly - USD 4.49
//            2. Single device Yearly - USD 39.99
//            3. Multiple Device Monthly - USD 7.49
//            4. Multiple Device Yearly - USD 77.99
    private static final String INTERVAL_YEAR = "year";
//    private Inventory inventory;
//    private List<String> purchasedSkus = new ArrayList<>();
    ;

    public static PurchaseHelper getInstance() {
        return PurchaseHelperHolder.INSTANCE;
    }


    public static String getExpireTime(String sku, long purchaseTime) {
        if (sku.equalsIgnoreCase(LicenseUtil.getSkuAllDevicesMonthly())) {
            return com.electrolyte.utils.DateHelper.getFutureExpireTime(true, purchaseTime);
        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuAllDevicesYearly())) {
            return com.electrolyte.utils.DateHelper.getFutureExpireTime(false, purchaseTime);
        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuThisDevicesMonthly())) {
            return com.electrolyte.utils.DateHelper.getFutureExpireTime(true, purchaseTime);
        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuThisDevicesYearly())) {
            return com.electrolyte.utils.DateHelper.getFutureExpireTime(false, purchaseTime);
        }
        return "";
    }

    /**
     * give expire time of purchase depends upon skus we have
     *
     * @param purchase
     * @return
     */
    public static String getExpireTime(Purchase purchase) {
        return getExpireTime(purchase.getSku(), purchase.getPurchaseTime());
    }


//    public static String getStartDatePast(String sku,long expiretime){
//        if (sku.equalsIgnoreCase(LicenseUtil.getSkuAllDevicesMonthly())) {
//            return getPastStartTime(true, expiretime);
//        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuAllDevicesYearly())) {
//            return getPastStartTime(false, expiretime);
//        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuThisDevicesMonthly())) {
//            return getPastStartTime(true, expiretime);
//        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuThisDevicesYearly())) {
//            return getPastStartTime(false, expiretime);
//        }
//        return "";
//    }

    /**
     * give the time with format of SERVER_FORMAT of purchase time of sku
     *
     * @param purchase
     * @return
     */
    public static String getRenewTime(SubscriptionPurchase purchase) {
        return getServerFormatDate(purchase.getExpiryTimeMillis());
    }

    public static String getServerFormatDate(long miliSec) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliSec);
        SimpleDateFormat formatter = new SimpleDateFormat(SERVER_FORMAT, Locale.US);
        return formatter.format(calendar.getTime());
    }

    /**
     * give expire interval i.e. monthly/year depends upon the purchaes
     *
     * @param purchase corrosponding purchase
     * @return give the interval time of the purchase
     */
    public static String getExpireInterval(Purchase purchase) {
        return expireInterval(purchase.getSku());
    }

    public static String getExpireInterval(String sku) {
        return expireInterval(sku);
    }

    private static String expireInterval(String sku) {

        if (sku.contains("yearly")) {
            return INTERVAL_YEAR;
        } else if (sku.contains("monthly")) {
            return INTERVAL_MONTH;
        }
//        if (sku.equalsIgnoreCase(LicenseUtil.getSkuAllDevicesMonthly()) ||
//                sku.equalsIgnoreCase(LicenseUtil.getSkuThisDevicesMonthly())) {
//            return INTERVAL_MONTH;
//        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuAllDevicesYearly()) ||
//                sku.equalsIgnoreCase(LicenseUtil.getSkuThisDevicesYearly())) {
//            return INTERVAL_YEAR;
//        }
        return "";
    }

    public static String getDeviceType(Purchase purchase) {
        return getDeviceType(purchase.getSku());
    }

    public static String getDeviceType(String sku) {
        if (sku.contains("multiple_device")) {
            return DEVICE_ALL;
        } else if (sku.contains("single_device")) {
            return DEVICE_ANDROID;
        }
        return DEVICE_ALL;
    }


//    /**
//     * @param monthly     is monthly purchase
//     * @param orignalTime orignal purchase time
//     * @return future time depends upon monthly/yearly
//     */
//    public static String getFutureExpireTime(boolean monthly, long orignalTime) {
//
//        orignalTime = validateIfCurrentTime(orignalTime);
//        int offset = monthly ? 30 : 365;
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(orignalTime);
//        calendar.add(Calendar.DATE, offset);
//        SimpleDateFormat formatter = new SimpleDateFormat(SERVER_FORMAT,Locale.US);
//        return formatter.format(calendar.getTime());
//    }
////    private static long validateIfFutureExpiryDate(long orignaTime){
////
////    }
//    public static long validateIfCurrentTime(long orignalTime){
//        if(DateHelper.getInstnace().isNotMoreThan1DayDiff(orignalTime)){
//            return orignalTime;
//        }else return System.currentTimeMillis();
//    }

    /**
     * @param monthly     is monthly purchase
     * @param orignalTime orignal purchase time
     * @return future time depends upon monthly/yearly
     */
    private static String getPastStartTime(boolean monthly, long orignalTime) {

        int offset = monthly ? 30 : 365;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(orignalTime);
        calendar.add(Calendar.DATE, -offset);
        SimpleDateFormat formatter = new SimpleDateFormat(SERVER_FORMAT, Locale.US);
        return formatter.format(calendar.getTime());
    }


    public static String getSkuUponAmount(String amount) {
        int value = 0;
        try {
            value = Integer.parseInt(amount);
        } catch (Exception ex) {

        }

        switch (value) {
            case 4499:
                return LicenseUtil.getSkuAllDevicesYearly25();
            case 4020:
                return LicenseUtil.getSkuAllDevicesYearly33();
            case 2999:
                return LicenseUtil.getSkuAllDevicesYearly50();
            case 1499:
                return LicenseUtil.getSkuAllDevicesYearly75();
            case 600:
                return LicenseUtil.getSkuAllDevicesMonthly25();
            case 536:
                return LicenseUtil.getSkuAllDevicesMonthly33();
            case 399:
                return LicenseUtil.getSkuAllDevicesMonthly50();
            case 199:
                return LicenseUtil.getSkuAllDevicesMonthly75();
            case 799:
                return LicenseUtil.getSkuAllDevicesMonthly();
            case 5999:
                return LicenseUtil.getSkuAllDevicesYearly();
            default:
                return "";
        }
    }

    /**
     * give purchase in cents depending upon skus we have
     *
     * @param purchase corresponding purchase
     * @return
     */
    public static int getPurchaseAmount(Purchase purchase) {
        return getPurchaseAmount(purchase.getSku());
    }

    public static int getPurchaseAmount(String sku) {

        if (LicenseUtil.getSkuAllDevicesYearly25().equalsIgnoreCase(sku)) {
            return 4499;
        } else if (LicenseUtil.getSkuAllDevicesYearly33().equalsIgnoreCase(sku)) {
            return 4020;
        } else if (LicenseUtil.getSkuAllDevicesYearly50().equalsIgnoreCase(sku)) {
            return 2999;
        } else if (LicenseUtil.getSkuAllDevicesYearly75().equalsIgnoreCase(sku)) {
            return 1499;
        } else if (LicenseUtil.getSkuAllDevicesMonthly25().equalsIgnoreCase(sku)) {
            return 600;
        } else if (LicenseUtil.getSkuAllDevicesMonthly33().equalsIgnoreCase(sku)) {
            return 536;
        } else if (LicenseUtil.getSkuAllDevicesMonthly50().equalsIgnoreCase(sku)) {
            return 399;
        } else if (LicenseUtil.getSkuAllDevicesMonthly75().equalsIgnoreCase(sku)) {
            return 199;
        } else if (LicenseUtil.getSkuAllDevicesMonthly().equalsIgnoreCase(sku)) {
            return 799;
        } else if (LicenseUtil.getSkuAllDevicesYearly().equalsIgnoreCase(sku)) {
            return 5999;
        }
        return 5999;
//        if (sku.equalsIgnoreCase(LicenseUtil.getSkuAllDevicesMonthly())) {
//            return 799;
//        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuAllDevicesYearly())) {
//            return 5999;
//        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuThisDevicesMonthly())) {
//            return 499;
//        } else if (sku.equalsIgnoreCase(LicenseUtil.getSkuThisDevicesYearly())) {
//            return 3999;
//        }
//        return 0;
    }

//    public Inventory getInventory() {
//        return inventory;
//    }
//
//    public void setInventory(Inventory inventory) {
//        this.inventory = inventory;
//        setPurchasedSkus(inventory);
//    }
//
//    private void setPurchasedSkus(Inventory inventory) {
//        if (inventory != null && inventory.getAllSkuDetailsSubs() != null) {
//            for (SkuDetails skuDetails: inventory.getAllSkuDetailsSubs()) {
//                purchasedSkus.add(skuDetails.getSku());
//            }
//        }
//    }
//    public List<String> getPurchasedSkus(){
//        return purchasedSkus;
//    }

    private static final class PurchaseHelperHolder {
        private static final PurchaseHelper INSTANCE = new PurchaseHelper();
    }


}
