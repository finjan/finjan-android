/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.licensing;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.avira.common.GeneralPrefs;
import com.avira.common.backend.WebUtility;
import com.finjan.securebrowser.vpn.licensing.models.server.ProcessPurchaseResponse;

/**
 * internal class that wraps both response listeners from volley and makes some processing prior to delivery through
 * the supplied callback
 *
 * @author ovidiu.buleandra
 * @since 05.11.2015
 */
/*package*/ class ProcessPurchaseInternalCallback
        implements Response.Listener<ProcessPurchaseResponse>, ErrorListener {

    Context mContext;
    String mAcronym;
    ProcessPurchaseCallback mCallback;

    public ProcessPurchaseInternalCallback(Context context, String productAcronym, ProcessPurchaseCallback callback) {
        if (callback == null)
            throw new IllegalArgumentException("passed callback shouldn't be null");
        mContext = context;
        mAcronym = productAcronym;
        mCallback = callback;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mCallback.onProcessPurchaseError(WebUtility.getHTTPErrorCode(error), WebUtility.getMessage(error));
    }

    @Override
    public void onResponse(ProcessPurchaseResponse response) {
        if (response.isSuccess() && response.getSubscription() != null) {
            boolean purchaseSuccessful = response.getSubscription().getEnabled();
            if(purchaseSuccessful) {
                GeneralPrefs.setAppId(mContext, mAcronym);
            }
            mCallback.onProcessPurchaseSuccessful(purchaseSuccessful);
        } else {
            mCallback.onProcessPurchaseError(response.getStatusCode(),
                    String.format("[%d] %s", response.getStatusCode(), response.getStatus()));
        }
    }
}
