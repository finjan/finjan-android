/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.licensing;

/**
 * @author ovidiu.buleandra
 * @since 09.11.2015
 */
public interface ProcessPurchaseCallback {

    void onProcessPurchaseError(int errorCode, String message);
    void onProcessPurchaseSuccessful(boolean result);
}
