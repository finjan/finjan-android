package com.finjan.securebrowser.vpn.ui.authentication;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.avira.common.authentication.Authentication;
import com.avira.common.authentication.AuthenticationListener;
import com.avira.common.backend.WebUtility;
import com.electrolyte.sociallogin.socialLogin.FbLoginBtn;
import com.electrolyte.sociallogin.socialLogin.FbSignInHandler;
import com.electrolyte.sociallogin.socialLogin.GoogleSignInHandler;
import com.electrolyte.sociallogin.socialLogin.models.SocialUserProfile;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.FinjanUser;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.controller.JsonCreater;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
//import com.finjan.securebrowser.vpn.social.FbSignInHandler;
//import com.finjan.securebrowser.vpn.social.GoogleSignInHandler;
//import com.finjan.securebrowser.vpn.social.SocialUserProfile;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.android.gms.common.SignInButton;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by navdeep on 13/3/18.
 */

public class AuthNavigationHelper {
    private View view;
    private AuthenticationFragment fragment;
    private TextView btCustomFb, btRegisterUsingEmail, btCustomGoogle;
    private TextView alreadyHaveAccount;
    private GoogleSignInHandler googleSignupObj;
    private FbSignInHandler facebookSignInObj;
    private SignInButton btGoogle;
    private FbLoginBtn btFacebook;
    private int RC_SIGNUP = 0;
    private SocialUserProfile socialUserProfile;
    private String TAG = "Social helper";
    private LinearLayout llSocialLogin;
    private RelativeLayout policyCheck;
    private CheckBox cb_eulaSocial;
    private Context context;

    private AuthNavigationHelper() {
    }

    public static AuthNavigationHelper getINSTANCE() {
        return SocialLoginHelperHolder.INSTANCE;
    }
    public CheckBox getCb_eulaSocial(){
        return  cb_eulaSocial;
    }

    protected void setView(View v, AuthenticationFragment fragment, LinearLayout llBack) {
        view = v;
        this.fragment = fragment;
        context = fragment.getContext();
        setReferences();
        setAnimation(llBack, context);
        setDefaultView(fragment.mAction);
    }

    protected void init(FragmentActivity activity) {
        googleSignupObj = new GoogleSignInHandler();
        facebookSignInObj = new FbSignInHandler();
        googleSignupObj.handle_initialization(activity);
        googleSignupObj.hanlde_onStartConfig();
        facebookSignInObj.handle_initialization(activity);
    }

    /**
     * references only for social login
     */
    private void setReferences() {
        llSocialLogin = (LinearLayout) view.findViewById(R.id.include_social);
        policyCheck = (RelativeLayout) view.findViewById(R.id.include_policy);
        btCustomFb = (TextView) view.findViewById(R.id.tv_fb_btn_placeholder);
        btCustomGoogle = (TextView) view.findViewById(R.id.tv_google_btn_placeholder);
        alreadyHaveAccount = (TextView) view.findViewById(R.id.tv_already_have_account_nav);
        btGoogle = (SignInButton) view.findViewById(R.id.btn_google_auth);
//        btFacebook =   view.findViewById(R.id.btn_fb_auth);
        btFacebook =   new FbLoginBtn(context);
        btRegisterUsingEmail = (TextView) view.findViewById(R.id.register_using_email);
        cb_eulaSocial= (CheckBox) policyCheck.findViewById(R.id.cb_eula);
        alreadyHaveAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchAccountSetup(SetupAccountActivity.ACTION_SIGN_IN, true, false);
            }
        });
        btRegisterUsingEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchAccountSetup(SetupAccountActivity.ACTION_SIGN_UP, true, false);
            }
        });
        btCustomFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.rlProgress.setVisibility(View.VISIBLE);
                facebookSignInObj.setFragment(fragment,btFacebook);
                if(!fragment.isAdded()){return;}
                GoogleTrackers.Companion.setEFacebook_login();
                facebookSignInObj.onClickSignInButton(btFacebook, new FbSignInHandler.FBResultListener() {
                    @Override
                    public void onResult(SocialUserProfile socialUserProfil, boolean success, String msg) {
                        fragment.rlProgress.setVisibility(View.GONE);
                        if (success) {
                            if (socialUserProfil!= null) {
                                if (TextUtils.isEmpty(socialUserProfil.getEmail()) && "no".equalsIgnoreCase(PlistHelper.getInstance().getSocialEmailReq()))
                                {
                                    socialUserProfil.setEmail(socialUserProfil.getSocialId()+"@facebook-social.com");
                                }
//                                if (socialUserProfile !=null){
                                    loginSocialUser(socialUserProfil);
//                                }

                                    Logger.logE(TAG, socialUserProfil.getEmail());
                            }
                        }else {
                            new FinjanDialogBuilder(context).setMessage(msg).show();
                        }
                    }
                });
            }
        });

        btCustomGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoogleTrackers.Companion.setEGoogle_login();
                Intent signInIntent = googleSignupObj.onClickSignInButton();
                if(fragment.isAdded()){
                    fragment.startActivityForResult(signInIntent, RC_SIGNUP);
                }
            }
        });

        btGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoogleTrackers.Companion.setEGoogle_login();
                Intent signInIntent = googleSignupObj.onClickSignInButton();
                if(fragment.isAdded()){
                    fragment.startActivityForResult(signInIntent, RC_SIGNUP);
                }
            }
        });
    }
     private void callTokenGeneration(SocialUserProfile socialUserProfile){
         fragment.rlProgress.setVisibility(View.VISIBLE);
         UserPref.getInstance().setEmailLogin(false);
         fragment.loginType = socialUserProfile.isSocialFb()? "fb":"gg";
         loginUser(context,socialUserProfile.isSocialFb()? "fb":"gg",
                 socialUserProfile.getSocialId(),socialUserProfile.getToken_id(),fragment);
//         Authentication.loginSocial(context,socialUserProfile.getToken_id(),socialUserProfile.getSocialId(),
//                 socialUserProfile.getSocialType().equals("fb_id"),fragment);
     }

     public static void loginUser(Context context, String type, String email, String password, AuthenticationListener responseCallback){
        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(type)){
            UserPref.getInstance().cacheUserForAutoLogin(type, email, password);
        }
        Authentication.loginUser(context, type, email, password, responseCallback);
     }
     private void registerSocialUser(SocialUserProfile usr,NetworkResultListener networkResultListener){

        if (TextUtils.isEmpty(usr.getEmail()) && "yes".equalsIgnoreCase(PlistHelper.getInstance().getSocialEmailReq())) {
            this.ursWaitingForPolicy=usr;
            this.networkResultListener= networkResultListener;
            Intent intent= new Intent(context, SocialEmail.class);
            intent.putExtra("email",usr.getEmail());
//            intent.putExtra("name",usr.getFirstName());
            fragment.getActivity().startActivityForResult(intent,102);
        }
        else if(validateEulaCheckBox()){
            if (TextUtils.isEmpty(usr.getEmail()) && "no".equalsIgnoreCase(PlistHelper.getInstance().getSocialEmailReq())) {
              /*usr.setEmail(usr.getSocialId()+"@facebook-social.com");*/
                usr.makeSocialEmail();
            }
            NetworkManager.getInstance(context).registerSocialuserAccessToken(context,
                    JsonCreater.getInstance().getSocialRegisterRequest(usr), networkResultListener);

        }else {
            if (TextUtils.isEmpty(usr.getEmail()) && "no".equalsIgnoreCase(PlistHelper.getInstance().getSocialEmailReq())) {
               /* usr.setEmail(usr.getSocialId()+"@facebook-social.com");*/
                usr.makeSocialEmail();
            }
            this.ursWaitingForPolicy=usr;
            this.networkResultListener= networkResultListener;
            Intent intent= new Intent(context, PolicyAcivity.class);
            fragment.getActivity().startActivityForResult(intent,101);
        }
     }

    private SocialUserProfile ursWaitingForPolicy;
     private  NetworkResultListener networkResultListener;

    public void ifUserUpdatePolicy(){
        if(ursWaitingForPolicy!=null && networkResultListener!=null){
            fragment.rlProgress.setVisibility(View.VISIBLE);
            registerSocialUser(ursWaitingForPolicy,
                    networkResultListener);
        }
    }
    public void userDeniedPolicy(){
        fragment.rlProgress.setVisibility(View.GONE);

    }
    public void ifUserUpdateEmail(String email,String emailVerified){
        if(ursWaitingForPolicy!=null && networkResultListener!=null){
            fragment.rlProgress.setVisibility(View.VISIBLE);
            ursWaitingForPolicy.setEmail(email);
            if (!TextUtils.isEmpty(emailVerified)){
                ursWaitingForPolicy.setEmailVerified(emailVerified);
            }
            registerSocialUser(ursWaitingForPolicy,
                    networkResultListener);
        }
    }
    public void userDeniedEmail(){
        fragment.rlProgress.setVisibility(View.GONE);

    }
    private boolean validateEulaCheckBox() {

        return cb_eulaSocial.isChecked();
    }

    private void loginSocialUser(final SocialUserProfile usr){
        UserPref.getInstance().setLoginBy(usr.getSocialType().equals("fb_id")?"fb":"google");
        fragment.rlProgress.setVisibility(View.VISIBLE);

        NetworkManager.getInstance(context).checkSocialuser(context, usr.getSocialType(),
                usr.getSocialId(), new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                fragment.rlProgress.setVisibility(View.GONE);
                AuthenticationHelper.getInstnace().setFinjanUser(new FinjanUser(response));
                callTokenGeneration(usr);
            }

            @Override
            public void executeOnError(final VolleyError error) {
                fragment.rlProgress.setVisibility(View.GONE);
                //check if user does not exist.
                String response=WebUtility.getMessage(error);
                if(response.contains("status")){
                    try {
                         JSONObject jsonObject=new JSONObject(response);
                        final JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        int status=jsonObject1.getInt("status");
                        //user not found error
                        //move user for register user
                        if(status==404){
                            fragment.rlProgress.setVisibility(View.VISIBLE);
                            registerSocialUser(usr, new NetworkResultListener() {

                                @Override
                                public void executeOnSuccess(JSONObject response) {
                                    LocalyticsTrackers.Companion.setERegistration();
                                    fragment.rlProgress.setVisibility(View.GONE);
                                    GlobalVariables.user_registered=true;
                                    if(usr.isSocialFb()){
                                        GoogleTrackers.Companion.setSFacebookSignUp();
                                    }else if(usr.isSocialGoogle()){GoogleTrackers.Companion.setSGoogleSignUp();}
                                    callTokenGeneration(usr);
                                }

                                @Override
                                public void executeOnError(VolleyError error) {
                                    fragment.rlProgress.setVisibility(View.GONE);
                                    int status = WebUtility.getHTTPErrorCode(error);
                                    if(status!=409){
                                        try {
                                            String msg=new JSONObject(WebUtility.getMessage(error)).getString("message");
                                            fragment.onAuthError(status,msg,"",error.getLocalizedMessage()+"----"+ error.getMessage());
//                                                    new FinjanDialogBuilder(context).setMessage(msg).show();
                                        }catch (Exception e){}
                                        return;}
                                    if(!usr.emailEndsWithSocialId()){
                                        usr.makeSocialEmail();
                                        fragment.rlProgress.setVisibility(View.VISIBLE);
                                        registerSocialUser(usr,                                                new NetworkResultListener() {
                                            @Override
                                            public void executeOnSuccess(JSONObject response) {
                                                LocalyticsTrackers.Companion.setERegistration();
                                                GlobalVariables.user_registered=true;
                                                fragment.rlProgress.setVisibility(View.GONE);
                                                if(usr.isSocialFb()){
                                                    GoogleTrackers.Companion.setSFacebookSignUp();
                                                }else if(usr.isSocialGoogle()){GoogleTrackers.Companion.setSGoogleSignUp();}

                                                callTokenGeneration(usr);
                                            }

                                            @Override
                                            public void executeOnError(VolleyError error) {
                                                fragment.rlProgress.setVisibility(View.GONE);
                                                int status = WebUtility.getHTTPErrorCode(error);
                                                String message = WebUtility.getErrorDesc(error);
                                                try {
                                                    message=new JSONObject(WebUtility.getMessage(error)).getString("message");
//                                                                    new FinjanDialogBuilder(context).setMessage(message).show();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                fragment.onAuthError(status,message,"",error.getLocalizedMessage()+"----"+ error.getMessage());
                                            }
                                        } );
                                    }else {
                                        fragment.rlProgress.setVisibility(View.GONE);
                                        try {
                                            String message = new JSONObject(WebUtility.getMessage(error)).getString("message");
//                                                    new FinjanDialogBuilder(context).setMessage(message).show();
                                            fragment.onAuthError(status,message,"",error.getLocalizedMessage()+"----"+ error.getMessage());

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }
                            });


                        }else {
                            String msg=new JSONObject(WebUtility.getMessage(error)).getString("message");
                            fragment.onAuthError(status,msg,"",error.getLocalizedMessage()+"----"+ error.getMessage());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
//                new FinjanDialogBuilder(context).setMessage(WebUtility.getMessage(error)).show();
            }
        });
    }




    private void setAnimation(final LinearLayout llBack, final Context context) {
        ((ObservableScrollView) llSocialLogin.findViewById(R.id.sv_social)).setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) { }

            @Override
            public void onDownMotionEvent() { }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                if (scrollState == null) {
                    return;
                }
                if (scrollState.equals(ScrollState.DOWN)) {
                    llBack.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_from_top));
                    llBack.setVisibility(View.VISIBLE);
                } else if (scrollState.equals(ScrollState.UP)) {
                    llBack.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_to_top));
                    llBack.setVisibility(View.GONE);
                }
            }
        });
    }

    private void switchAnimation(String action, boolean fromNav, boolean backPress) {
        if (NativeHelper.getInstnace().checkContext(context)) {
            fragment.showingRegister = action.equals(SetupAccountActivity.ACTION_SIGN_UP);
            switch (action) {
                case SetupAccountActivity.ACTION_SIGN_IN:
                    showLogin(fromNav, backPress);
                    break;
                case SetupAccountActivity.ACTION_SIGN_NAV:
                    showAuthNavigation(fromNav, backPress);
                    break;
                case SetupAccountActivity.ACTION_SIGN_UP:
                    showRegister(fromNav, backPress);
                    break;
            }
        }
    }

    private void setDefaultView(String action) {
        switch (action) {
            case SetupAccountActivity.ACTION_SIGN_IN:

                fragment.rlRegister.setVisibility(View.GONE);
                llSocialLogin.setVisibility(View.GONE);
                fragment.llLogin.setVisibility(View.VISIBLE);


                break;
            case SetupAccountActivity.ACTION_SIGN_NAV:

                fragment.llLogin.setVisibility(View.GONE);
                fragment.rlRegister.setVisibility(View.GONE);
                llSocialLogin.setVisibility(View.VISIBLE);
                break;
            case SetupAccountActivity.ACTION_SIGN_UP:

                llSocialLogin.setVisibility(View.GONE);
                fragment.llLogin.setVisibility(View.GONE);
                fragment.rlRegister.setVisibility(View.VISIBLE);
                break;
        }
        fragment.showingRegister = !SetupAccountActivity.ACTION_SIGN_IN.equals(action);
    }

    private void showRegister(boolean fromNav, boolean backPress) {
        GoogleTrackers.Companion.setSSignUp();
//        LocalyticsTrackers.Companion.setSSignUp();

        if (fromNav) {
            llSocialLogin.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_to_left));
            llSocialLogin.setVisibility(View.GONE);
            fragment.rlRegister.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_from_right));
            fragment.rlRegister.setVisibility(View.VISIBLE);
        } else {
            fragment.llLogin.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_to_right));
            fragment.llLogin.setVisibility(View.GONE);
            fragment.rlRegister.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_from_left));
            fragment.rlRegister.setVisibility(View.VISIBLE);

        }

        fragment.currentScreen = SetupAccountActivity.ACTION_SIGN_UP;
    }

    private void showLogin(boolean fromNav, boolean backPress) {
        fragment.currentScreen = SetupAccountActivity.ACTION_SIGN_IN;
        GoogleTrackers.Companion.setSEmailLogin();
//        LocalyticsTrackers.Companion.setSEmailLogin();

        if (fromNav) {
            llSocialLogin.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_to_left));
            llSocialLogin.setVisibility(View.GONE);
            fragment.llLogin.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_from_right));
            fragment.llLogin.setVisibility(View.VISIBLE);
        } else {
            fragment.rlRegister.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_to_left));
            fragment.rlRegister.setVisibility(View.GONE);
           /* fragment.llTermsAndCondition.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_to_left));
            fragment.llTermsAndCondition.setVisibility(View.GONE);
            *//*fragment.rlRegister.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_to_left));*//*
            fragment.llBack.setEnabled(true);

            fragment.isTermsAndConditionScreen = false;*/


            fragment.llLogin.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_from_right));
            fragment.llLogin.setVisibility(View.VISIBLE);
        }
    }

    protected void showAuthNavigation(boolean fromNav, boolean backPress) {
        fragment.currentScreen = SetupAccountActivity.ACTION_SIGN_NAV;
        GoogleTrackers.Companion.setSLogin();
//        LocalyticsTrackers.Companion.setSLogin();
        if (fromNav) {
            fragment.llBack.setVisibility(fragment.backArrow?View.VISIBLE:View.GONE);
            fragment.rlRegister.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_to_right));
            fragment.rlRegister.setVisibility(View.GONE);
            llSocialLogin.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_from_left));
            llSocialLogin.setVisibility(View.VISIBLE);
        } else {
            fragment.llBack.setVisibility(fragment.backArrow?View.VISIBLE:View.GONE);
            fragment.llLogin.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_to_right));
            fragment.llLogin.setVisibility(View.GONE);
            llSocialLogin.setAnimation(AnimationUtils.loadAnimation(context, R.anim.enter_from_left));
            llSocialLogin.setVisibility(View.VISIBLE);
        }
    }

    protected void switchAccountSetup(String action, boolean fromNav, boolean backPress) {
        if(SetupAccountActivity.ACTION_SIGN_NAV == action){
            fragment.llBack.setVisibility(fragment.backArrow?View.VISIBLE:View.GONE);
        }else {fragment.llBack.setVisibility(View.VISIBLE);}
        fragment.lastScreen = fragment.currentScreen;
        switchAnimation(action, fromNav, backPress);

    }

    protected void performOnActivityResult(int requestCode, int resultCode, Intent data) {
        fragment.rlProgress.setVisibility(View.VISIBLE);

        if (requestCode == RC_SIGNUP) {
            socialUserProfile = googleSignupObj.performTaskOnActivityResult(requestCode, resultCode, data);
            if (socialUserProfile!=null) {
                loginSocialUser(socialUserProfile);
                //get details from google profile and continue flow from here
            }else {
                fragment.rlProgress.setVisibility(View.GONE);
                Toast.makeText(context,"There was some issue in login, please try again",Toast.LENGTH_LONG).show();
            }
        } else
            facebookSignInObj.performTaskOnActivityResult(requestCode, resultCode, data);
    }

//    public static String loginVia="";
//    private static final String view_Fb="via Facebook";
//    private static final String view_Google="via Google";
    private static final class SocialLoginHelperHolder {
        private static final AuthNavigationHelper INSTANCE = new AuthNavigationHelper();
    }
}
