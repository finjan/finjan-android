/*
 * *
 *  * <p>
 *  * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 *  * Finjan Mobile Vital Security Browser
 *  * <p>
 *  * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *  * Developed by NewOfferings, LLC.
 *  *
 *  *
 *
 */

package com.finjan.securebrowser.vpn.ui.authentication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.avira.common.utils.NetworkConnectionManager;
import com.avira.common.utils.validation.EmailConstraintChecker;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.NormaWebviewActivity;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.constants.DrawerConstants;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;

import org.json.JSONObject;

public class SocialEmail extends Activity {
    private TextView btn_signup;
    private LinearLayout llBack;
    private CheckBox cb_eulaSocial;
    private EditText mEtEmail_R;
    private TextInputLayout tiEmail_R;
    private String mValidEmail;
    private String emailVerified;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_social_email);
        cb_eulaSocial= findViewById(R.id.cb_eula);
        btn_signup= findViewById(R.id.btn_signup);
        llBack = findViewById(R.id.ll_back);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                onBackPressed();
            }
        });
        TextView mTvEula = (TextView) findViewById(R.id.tv_eula);
        setPrivacyAndEulaClickable(mTvEula);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmailField() && validateEulaCheckBox()){
                    verifyEmailAndSignup();
                }
            }
        });
        mEtEmail_R = (EditText) findViewById(R.id.et_email_R);
        tiEmail_R = (TextInputLayout) findViewById(R.id.ti_email_R);

        mEtEmail_R.setText(getIntent().getExtras().getString("email",""));

    }

    private void signup(){
        Intent bundle = new Intent();
        bundle.putExtra("email",mEtEmail_R.getText().toString());
        bundle.putExtra("email_verified",emailVerified != null ? emailVerified : "");
//                    bundle.putExtra("name",mtName.getText());
        setResult(Activity.RESULT_OK,bundle);
        finish();
    }

    private boolean validateEulaCheckBox() {
        if (!cb_eulaSocial.isChecked()) {
            new FinjanDialogBuilder(this).setMessage(PlistHelper.getInstance().getEulaSkipMsgText())
                    .setPositiveButton("Ok").show();
        }
        return cb_eulaSocial.isChecked();
    }

    private void setPrivacyAndEulaClickable(TextView textView) {
        ClickableSpan eulaClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoEulaUrl();
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ClickableSpan privacyClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoPrivacyUrl();
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        SpannableString ss = new SpannableString(getString(R.string.by_registering_you_aggre_to_the_eula));
        ss.setSpan(new UnderlineSpan(), 33, 66, 0);
        ss.setSpan(new UnderlineSpan(), 75, 90, 0);
        ss.setSpan(eulaClick, 33, 66, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(privacyClick, 75, 90, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    /**
     * @return false - the dialog was not shown => there is connectivity true - the dialog was shown => no connectivity
     */
    private boolean showNoNetworkDialogIfNoConnectivity() {
        if (!NetworkConnectionManager.hasNetworkConnectivity(this)) {
            if (NativeHelper.getInstnace().checkActivity(this)) {
                Toast.makeText(this, "Connect to a mobile or Wi-Fi network to continue.", Toast.LENGTH_LONG).show();

//                CommonDialogs.showNetworkUnavailable(getActivity());
                return true;
            }

        }
        return false;
    }

    public void gotoEulaUrl() {

        Intent intent = new Intent(this, NormaWebviewActivity.class);
        intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_EULA);
        startActivity(intent);
    }

    public void gotoPrivacyUrl() {

        Intent intent = new Intent(this, NormaWebviewActivity.class);
        intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_PRIVATE_PRIVACY);
        GoogleTrackers.Companion.setSPrivacy();
        startActivity(intent);
    }

    private boolean validateEmailField() {

        String email = mEtEmail_R.getText().toString();
        if (!EmailConstraintChecker.isValidEmailFormat(email)) {
            showEmailError(getString(R.string.registration_invalid_email_format));
            mValidEmail = null;
            return false;
        }
        mValidEmail = email;
        return true;
    }

    public void showEmailError(String errorMessage) {
        EditText editText = mEtEmail_R;
        TextInputLayout ti = tiEmail_R;

        editText.setText("");
        editText.requestFocus();
        ti.setError(errorMessage);
        ti.setErrorEnabled(true);
    }

    private boolean isSecondTimeReg = false;
    private void verifyEmailAndSignup() {
        if (!"neverbounce".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationProvider())
                || "no".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption())) {
            emailVerified = null;
            signup();
            return;
        }
        showProgressDialog(getString(R.string.QueryingInformationFromServer));
        NetworkManager.getInstance(this).hitNeverBounceAPI(this, new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                hideProgress();
                Logger.logV("neverbounce",response.toString());
                if (!"valid".equalsIgnoreCase(response.optString("result"))){

                    if ("warning".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption()) && !isSecondTimeReg) {
                        isSecondTimeReg = true;
                        hideProgress();
                        new FinjanDialogBuilder(SocialEmail.this).setMessage(PlistHelper.getInstance().getEmailValidationWarnMsg())
                                .setPositiveButton("Ok").show();
                        return;
                    }
                    else if ("yes".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption())) {
                        hideProgress();
                        new FinjanDialogBuilder(SocialEmail.this).setMessage(PlistHelper.getInstance().getEmailValidationErrMsg())
                                .setPositiveButton("Ok").show();
                        return;
                    }

                }
                emailVerified = response.optString("result",null);
                signup();
            }

            @Override
            public void executeOnError(VolleyError error) {
                Logger.logE("neverbounce",error.toString());
                hideProgress();
                if ("warning".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption()) && !isSecondTimeReg) {
                    isSecondTimeReg = true;
                    hideProgress();
                    new FinjanDialogBuilder(SocialEmail.this).setMessage(PlistHelper.getInstance().getEmailValidationWarnMsg())
                            .setPositiveButton("Ok").show();
                    return;
                }
                else if ("yes".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption())) {
                    hideProgress();
                    new FinjanDialogBuilder(SocialEmail.this).setMessage(PlistHelper.getInstance().getEmailValidationErrMsg())
                            .setPositiveButton("Ok").show();
                    return;
                }
                emailVerified = null;
                signup();
            }
        },mValidEmail);
    }

    private void showProgressDialog(final String msg) {
        if (NativeHelper.getInstnace().checkActivity(this)) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(SocialEmail.this, R.style.VpnProgressDialog);
                    }
                    progressDialog.setMessage(msg);


                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        progressDialog.show();

                }
            });
        }
    }

    private void hideProgress() {
        if (NativeHelper.getInstnace().checkActivity(this)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            });
        }
        progressDialog = null;
    }

}
