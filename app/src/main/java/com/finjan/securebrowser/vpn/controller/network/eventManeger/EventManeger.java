/*
 * *
 *  * <p>
 *  * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 *  * Finjan Mobile Vital Security Browser
 *  * <p>
 *  * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *  * Developed by NewOfferings, LLC.
 *  *
 *  *
 *
 */

package com.finjan.securebrowser.vpn.controller.network.eventManeger;

import android.content.Context;
import android.util.Log;

import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.network.cache.CacheEntry;
import com.finjan.securebrowser.vpn.controller.network.cache.DiskLruCache;
import com.finjan.securebrowser.vpn.controller.network.cache.LruCache;

import java.io.IOException;

public class EventManeger {

    private String TAG = "Class : EventManager";

    private static final int DISK_CACHE_VALUE_COUNT = 1;
    private static final int DISK_CACHE_VERSION = 1;
    private static final int DEFAULT_DISK_CACHE_SIZE = 10 * 1024 * 1024; // 10MB
    LruCache<String, Response> lruCache = null;
    DiskLruCache diskCache = null;
    Context context = null;

    private static EventManeger instance;
    public static EventManeger getInstance()
    {
        if (instance!= null)
            return instance;
        else
        {
            instance = new EventManeger();
            return instance;
        }
    }
    public Response getCacheValue(String cacheKey) {
        cacheKey = cacheKey.replace("_", "");
        cacheKey = cacheKey.replaceAll("\\W", "");
        Response res = null;
        res = getDiskCacheData(cacheKey);
        return res;
    }

    public Response getDiskCacheData(String cacheKey) {
       // Logger.easyLog(Logger.LEVEL_INFO, "EventManager", "getDiskCache");
        Response res = null;
        DiskLruCache.Snapshot snapshot = null;
        try {
            snapshot = diskCache.get(cacheKey);

            if (snapshot != null) {
                CacheEntry entry = new CacheEntry(snapshot);
                snapshot.close();
                String responseText = new String(entry.responseData);
                res = new Response();
                res.setResponseBytes(entry.getResponseData());
                res.setResponseText(responseText);

            }

        }

        catch (Exception e) {
            Log.e(TAG, "Exception in getting disk cache data: " + e.toString());
        }
        return res;
    }

    public void initialisingCache(int type) {
        //Logger.easyLog(Logger.LEVEL_INFO, "EventManager", "initialisingCache");
        /********** if type = 0 means initialise both cache else only lru *********/
        try {
//			lruCache = LruCache.getLRUInstance();
            if (type == 0) {

                diskCache = DiskLruCache.open(
                        FinjanVPNApplication.getInstance().getExternalCacheDir(),
                        DISK_CACHE_VERSION, DISK_CACHE_VALUE_COUNT,
                        DEFAULT_DISK_CACHE_SIZE);

                // Log.e(TAG, "Disk Cache Initilized");
            }
        } catch (Exception e1) {
            // Log.e(TAG, "Disk Cache Error in Initilized");
            e1.printStackTrace();
        }

    }


    public void putValuesInCache(Response response, String cacheKey, int type) {
       // Logger.easyLog(Logger.LEVEL_INFO, "EventManager", "putValuesinCache");
        cacheKey = cacheKey.replace("_", "");
        cacheKey = cacheKey.replaceAll("\\W", "");
//		lruCache.put(cacheKey, response);
        // TheApplication.writeLog1("EventManager putValuesInCache CacheKey LRU = "
        // + cacheKey);
        CacheEntry entry = new CacheEntry(response.getResponseText().getBytes());
        if (diskCache != null) {
            DiskLruCache.Editor editor = null;
            try {
                editor = diskCache.edit(cacheKey);
                if (editor != null) {
                    entry.writeTo(editor);
                    editor.commit();
                    // TheApplication.writeLog1("EventManager putValuesInCache CacheKey DISK = "
                    // + cacheKey);
//					Log.e(TAG, "Response Saved in Disk Cache >> Key = "
//							+ cacheKey);
                }

            } catch (IOException e) {
                editor = null;
                e.printStackTrace();
                // TheApplication.writeLog1("EventManager putValuesInCache CacheKey Not Saved To Disk = "
                // + cacheKey);
                // Log.e(TAG, "Error in Response Saved in Disk Cache");
            }
            // }else {
            // TheApplication.writeLog1("EventManager>putValuesInCache >> CacheKey = "
            // + cacheKey + " : DiskCache == null");
        }
    }



}
