/* Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.finjan.securebrowser.vpn.licensing.models.billing;

import com.avira.common.GSONModel;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * Represents an in-app product's listing details.
 */
public class SkuDetails implements GSONModel {
    @SerializedName("itemType") private String mItemType;
    @SerializedName("sku") private String mSku;
    @SerializedName("orderId") private String mSubscriptionId;
    @SerializedName("type") private String mType;
    @SerializedName("price") private String mPrice;
    @SerializedName("title") private String mTitle;
    @SerializedName("description") private String mDescription;
    @SerializedName("json") private String mJson;
    @SerializedName("currencyCode") private String mCurrencyCode;
    @SerializedName("priceValue") private String mPriceValue;
    @SerializedName("introductoryPriceValue") private String mIntroductoryPriceValue;
    @SerializedName("introductoryPrice") private String mIntroductoryPrice;
    @SerializedName("purchaseTime") private String mPurchaseTime;
    @SerializedName("purchaseState") private String mPurchaseState;
    @SerializedName("subscriptionPeriod") private String mSubscriptionPriod;
    private BigDecimal priceDecimal;
    private BigDecimal priceDecimalIntroductory;

    public SkuDetails(String jsonSkuDetails) throws JSONException {
        this(IabHelper.ITEM_TYPE_INAPP, jsonSkuDetails);
    }

    public SkuDetails(String itemType, String jsonSkuDetails) throws JSONException {
        mItemType = itemType;
        mJson = jsonSkuDetails;
        JSONObject o = new JSONObject(mJson);
        mSku = o.optString("productId");
        mSubscriptionId = o.optString("orderId");
        mType = o.optString("type");
        mPrice = o.optString("price");
        mTitle = o.optString("title");
        mDescription = o.optString("description");
        mIntroductoryPrice = o.optString("introductoryPrice");
        mPurchaseTime= o.optString("purchaseTime");
        mPurchaseState= o.optString("purchaseState");
        mCurrencyCode = o.optString("price_currency_code");
        mSubscriptionPriod = o.optString("subscriptionPeriod");
        final long priceMicros = o.optLong("price_amount_micros", 1);
        final long priceMicrosIntroductory = o.optLong("introductoryPriceAmountMicros", 1);
        priceDecimal = new BigDecimal((double)priceMicros / 1000000f);
        priceDecimalIntroductory = new BigDecimal((double)priceMicrosIntroductory / 1000000f);
        priceDecimal = priceDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
        priceDecimalIntroductory = priceDecimalIntroductory.setScale(2, BigDecimal.ROUND_HALF_UP);
        mPriceValue = priceDecimal.toString();
        mIntroductoryPriceValue = priceDecimalIntroductory.toString();
    }

    public String getSku() { return mSku; }
    public String getType() { return mType; }
    public String getPrice() { return mPrice; }
    public String getTitle() { return mTitle; }
    public String getDescription() { return mDescription; }

    public String getPriceValue() { return mPriceValue; }
    public String getmIntroductoryPriceValue() { return mIntroductoryPriceValue; }
    public BigDecimal getPriceValueBigD() { return priceDecimal; }
    public BigDecimal getIntroductoryPriceValueBigD() { return priceDecimalIntroductory; }
    public String getCurrencyCode() { return mCurrencyCode; }

    public boolean isManagedProduct() { return mItemType.equals(IabHelper.ITEM_TYPE_INAPP); }

    public String getmPurchaseTime() {
        return mPurchaseTime;
    }

    public String getmPurchaseState() {
        return mPurchaseState;
    }

    public String getmSubscriptionPriod() {
        return mSubscriptionPriod;
    }

    public String getmSubscriptionId() {
        return mSubscriptionId;
    }

    public String getmIntroductoryPrice() {
        return mIntroductoryPrice;
    }

    @Override
    public String toString() {
        return "SkuDetails:" + mJson;
    }
}
