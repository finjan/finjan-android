/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import com.avira.common.CommonLibrary;
import com.avira.common.GeneralPrefs;
import com.avira.common.backend.Backend;
import com.avira.common.database.SecureDB;
import com.avira.common.database.Settings;
import com.avira.common.dialogs.AviraDialog;
import com.avira.common.id.HardwareId;
import com.avira.common.utils.SharedPreferencesUtilities;
import com.avira.common.utils.TypefaceUtil;
//import com.crashlytics.android.Crashlytics;
//import com.electrolyte.utils.InitLib;
import com.electrolyte.utils.InitLib;
import com.finjan.securebrowser.BuildConfig;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.vpn.auto_connect.AutoConnectAcitivity;
import com.finjan.securebrowser.vpn.auto_connect.AutoConnectHelper;
import com.finjan.securebrowser.vpn.controller.database.VpnDBInitialization;
import com.finjan.securebrowser.vpn.receiver.WifiConnectionReceiver;
import com.finjan.securebrowser.vpn.service.AviraOpenVpnService;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.VpnUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
//import com.localytics.android.AnalyticsListenerAdapter;
//import com.localytics.android.Localytics;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import de.blinkt.openvpn.core.PRNGFixes;
import de.blinkt.openvpn.core.VpnStatus;
//import io.fabric.sdk.android.Fabric;

//import com.avira.common.tracking.TrackingManager;


public class FinjanVPNApplication extends AnalyticsApplication {

    public static final String APP_FONT = "fonts/sf-ui-regular.ttf";
    private static final String TAG = FinjanVPNApplication.class.getSimpleName();
    private static final String ACRONYM_PRO = "AVPP0";
    private static final String ACRONYM_FREE = "AVPN0";
    private static FinjanVPNApplication instance;
    public static boolean isMyAppInBackground;
    private AlarmManager mAlarmManager = null;
    private Handler backgroundHandler;

    /**
     * Service connection that is used to bind to VPN service
     * in order to stop VPN connection upon {@link FinjanVPNApplication#clearData()}.
     */
    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Logger.logD(TAG, "onServiceConnected " + name.getClassName());

            // stop VPN service
            AviraOpenVpnService vpnService = ((AviraOpenVpnService.LocalBinder) service).getService();
            VpnUtil.stopVpnConnection(FinjanVPNApplication.this, vpnService);

            // work done, unbind vpn service
            unbindService(mServiceConnection);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Logger.logD(TAG, "onServiceDisconnected " + name.getClassName());
        }
    };

    public static FinjanVPNApplication getInstance() {
        return instance;
    }

    private void setAutoconnect(){
        if(AppConfig.Companion.getShowAutoConnect()){

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                connectivityManager.registerNetworkCallback(new NetworkRequest.Builder().build(), new  ConnectivityManager.NetworkCallback() {

                    @Override
                    public void onAvailable(Network network) {
                        super.onAvailable(network);
                        checkForAutoConnectVpn();
//                        setDefaultLayout(UserPref.getInstance().autoConnect)
                    }

                    @Override
                    public void onLost(Network network) {
                        super.onLost(network);
                        checkForAutoConnectVpn();
                    }

                });

            } else {
                registerReceiver(new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        checkForAutoConnectVpn();
                    }

                }, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            }
        }
    }
    private void checkForAutoConnectVpn(){
        if(UserPref.getInstance().getAutoConnect()){

            if(AutoConnectAcitivity.Companion.getInstance() != null &&
                    NativeHelper.getInstnace().checkActivity(AutoConnectAcitivity.Companion.getInstance())){
                AutoConnectAcitivity.Companion.getInstance().setDefaultLayoutOutside();
            }
//            if(VpnActivity.getInstance()!=null &&
//                    NativeHelper.getInstnace().checkActivity(VpnActivity.getInstance())) {
//                AutoConnectHelper.Companion.getInstance().whenStatusChanged(getApplicationContext());
//            }
            AutoConnectHelper.Companion.getInstance().whenStatusChanged(FinjanVPNApplication.getInstance());
        }

    }


    /**
     * get Application Acronym based on user license type
     */
    public static String getAcronym() {
        if (instance != null) {
            if (TrafficController.LICENSE_PAID.equalsIgnoreCase(TrafficController.getInstance(instance).getLicenseType())) {
                return ACRONYM_PRO;
            } else {
                return ACRONYM_FREE;
            }
        } else {
            throw new IllegalStateException("Initialize application first");
        }
    }

    /**
     * Enables https connections
     */
    @SuppressLint("TrulyRandom")
    public static void handleSSLHandshake() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception ignored) {
        }
    }

    /**
     * strong use this only for VPNActivity not for check ismyapp inbackground or no
     *
     * @param parentActivity
     * @param isMyAppInBackGround
     */
    public void setIsMyAppInBackGround(Activity parentActivity, boolean isMyAppInBackGround) {
//        if((GlobalVariables.getInstnace().isLaunchVpnAcivityCalled || VpnHomeFragment.userCalledDisconnect)
//                && isMyAppInBackGround){
//            return;
//        }
        isMyAppInBackground = isMyAppInBackGround;
        UserPref.getInstance().setMyAppWasInBackground( isMyAppInBackground);
    }

    private void initLibs(){
        InitLib.Companion.initLib(this);
        com.electrolyte.logger.InitLib.Companion.initLib(this);
        com.electrolyte.sociallogin.InitLib.Companion.initLib(this);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Logger.logE(TAG,"onCreate called");
        new Thread(new Runnable() {
            @Override
            public void run() {
                initLibs();
                GeneralPrefs.setAppId(FinjanVPNApplication.this, getAcronym());
                initSecureDb();
                VpnStatus.initLogCache(getApplicationContext().getCacheDir());
                // reset saved connection status in case of app terminated abruptly (e.g. user force stopped or low memory state)
                FinjanVpnPrefs.saveVpnConnectionStatus(FinjanVPNApplication.this, VpnStatus.ConnectionStatus.LEVEL_NOTCONNECTED);

                if (!UserPref.getInstance().getFirstOpen()) {
                    UserPref.getInstance().setFirstOpen(true);
//            LocalyticsTrackers.Companion.setFirstOpen();
                }
            }
        }).start();

        CommonLibrary.setDeveloperBuild(BuildConfig.DEBUG);
        Backend.selectServer("SERVER");

//        try {
//            TypefaceUtil.overrideFont(this, "SERIF", APP_FONT);
//        } catch (Exception e) {
//            Logger.logE(TAG, "Error override font, " + e);
//        }


        PRNGFixes.apply();


        AviraDialog.setDefaultIcon(R.mipmap.app_icon);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            WifiConnectionReceiver wifiConnectionReceiver = new WifiConnectionReceiver();
            registerReceiver(wifiConnectionReceiver,
                    new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        }
        setAutoconnect();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

//            Localytics.autoIntegrate(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                Logger.logE("token", token);
//                    Localytics.setPushRegistrationId(token);
//                    Localytics.registerPush();
//                    Logger.logE("Localytics version using", " " + Localytics.getLibraryVersion());

            }
        });




//        Localytics.setAnalyticsListener(new AnalyticsListenerAdapter() {
//
//            @Override
//            public void localyticsSessionWillOpen(boolean isFirst, boolean isUpgrade, boolean isResume) {
//                LocalyticsTrackers.Companion.setIsAutoConnectUser();
//                if (isFirst) {
//
//                    LocalyticsTrackers.Companion.setUserType();
//                    LocalyticsTrackers.Companion.setUserPaidType(6);
//                    LocalyticsTrackers.Companion.setUserRegisteredPlateformType();
//                    LocalyticsTrackers.Companion.setBorrowerUser();
//                    LocalyticsTrackers.Companion.setUserRegisteredType();
//                    LocalyticsTrackers.Companion.setPaidSource();
//                    LocalyticsTrackers.Companion.setLastCountryConnected(FinjanVpnPrefs.getLastUsedServer(FinjanVPNApplication.this));
//                    LocalyticsTrackers.Companion.setIsDesktopUser();
//                    LocalyticsTrackers.Companion.setIsIosUser();
//                    LocalyticsTrackers.Companion.setIsAndroidUser();
//                    Localytics.setCustomDimension(12, "no");
//                    LocalyticsTrackers.Companion.setDevicesRegistered();
//                    LocalyticsTrackers.Companion.getInstance().updateTrafficProfile();
//                }
//                if (isUpgrade) {
//                    LocalyticsTrackers.Companion.setUserType();
//                    LocalyticsTrackers.Companion.setUserPaidType(6);
//                    LocalyticsTrackers.Companion.setUserRegisteredPlateformType();
//                    LocalyticsTrackers.Companion.setBorrowerUser();
//                    LocalyticsTrackers.Companion.setUserRegisteredType();
//                    LocalyticsTrackers.Companion.setPaidSource();
//                    LocalyticsTrackers.Companion.setLastCountryConnected(FinjanVpnPrefs.getLastUsedServer(FinjanVPNApplication.this));
////                    LocalyticsTrackers.Companion.setIsAutoConnectUser();
//                    LocalyticsTrackers.Companion.setIsDesktopUser();
//                    LocalyticsTrackers.Companion.setIsIosUser();
//                    LocalyticsTrackers.Companion.setIsAndroidUser();
//                    Localytics.setCustomDimension(12, "no");
//                    LocalyticsTrackers.Companion.setDevicesRegistered();
//                    LocalyticsTrackers.Companion.getInstance().updateTrafficProfile();
//                }
//            }
//
//        });

    }
    public FirebaseAnalytics mFirebaseAnalytics;




    private void setupAdjust() {

//        String appToken = getString(R.string.adjust_token);
//        String environment = BuildConfig.DEBUG ?
//                AdjustConfig.ENVIRONMENT_SANDBOX :
//                AdjustConfig.ENVIRONMENT_PRODUCTION;
//        AdjustConfig config = new AdjustConfig(this, appToken, environment);
//        Adjust.onCreate(config);

        // todo: do this if you want to track events
        // registerActivityLifecycleCallbacks(this);
    }

    private void initSecureDb() {
        Logger.logD(TAG, "initSecureDb");
        VpnDBInitialization customInit = new VpnDBInitialization();
        String partialSeed = HardwareId.getSalt(getApplicationContext());
        Logger.logD(TAG, "initSecureDb partial seed " + partialSeed);
        SecureDB.init(getApplicationContext(), partialSeed, customInit);
    }

    /**
     * Method to clear application data by:
     * - stop VPN if active
     * - clearing shared preferences file
     * - deleting all databases
     */
    public void clearData() {
        Logger.logD(TAG, "clearData");

        if (VpnUtil.isVpnActive(this)) {
            disconnectFromVpn();
        }

        SharedPreferencesUtilities.clear(this);

        Settings.deleteTable();
        SecureDB.deleteInstance();
        initSecureDb();

        // AAMA-4026 - fix for "Invalid app" returned from server
        GeneralPrefs.setAppId(this, getAcronym());
    }

    private void disconnectFromVpn() {
        // need to first bind to vpn service then we will stop vpn connection upon ServiceConnection#onServiceConnected
        Intent intent = new Intent(this, AviraOpenVpnService.class);
        intent.setAction(AviraOpenVpnService.START_SERVICE);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    /**
     * Get system service AlarmManager
     *
     * @return AlarmManager from system service
     */
    public AlarmManager getAlarmManager() {
        if (mAlarmManager == null) {
            mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        }
        return mAlarmManager;
    }

    public enum Fonts {
        KIEVIT_LIGHT("fonts/KievitWeb-Light.ttf");
        Typeface typeface;

        Fonts(String path) {
            typeface = Typeface.createFromAsset(getInstance().getAssets(), path);
        }

        public Typeface getTypeface() {
            return typeface;
        }
    }
}
