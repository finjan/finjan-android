package com.finjan.securebrowser.vpn.licensing;

import com.finjan.securebrowser.vpn.licensing.models.restful.License;

import java.util.List;

/**
 * @author ovidiu.buleandra
 * @since 22.12.2015
 */
public class LicensesRefreshedEvent {
    List<License> mLicenses;

    public LicensesRefreshedEvent(List<License> licenses) {
        mLicenses = licenses;
    }

    public List<License> getLicenses() {
        return mLicenses;
    }
}
