package com.finjan.securebrowser.vpn.eventbus;

import de.blinkt.openvpn.core.VpnStatus;

public class VpnStatusEvent {

    VpnStatus.ConnectionStatus mConnectionStatus;

    public VpnStatusEvent(VpnStatus.ConnectionStatus connectionStatus) {
        mConnectionStatus = connectionStatus;
    }

    public VpnStatus.ConnectionStatus getStatus() {
        return mConnectionStatus;
    }

}
