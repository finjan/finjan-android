//package com.finjan.securebrowser.vpn.ui.main
//
//import android.content.Context
//import android.content.Intent
//import android.os.Bundle
//import android.support.v4.app.FragmentActivity
//import android.support.v7.app.AppCompatActivity
//import com.facebook.AccessToken
//import com.facebook.login.LoginManager
//import com.google.android.gms.auth.api.Auth
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions
//import com.google.android.gms.common.ConnectionResult
//import com.google.android.gms.common.api.GoogleApiClient
//
///**
// * Created by Electro on 02-04-2018.
// */
//class LoggoutActivity:AppCompatActivity(),GoogleApiClient.OnConnectionFailedListener,
//        GoogleApiClient.ConnectionCallbacks{
//
//    companion object {
//        fun getInstance(context: Context?){
//            val intent =Intent(context,LoggoutActivity::class.java)
//            context?.startActivity(intent)
//        }
//    }
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        moveTaskToBack(true)
//        logoutFb()
//        handle_initialization()
//    }
//
//    fun logoutFb(){
//        if(LoginManager.getInstance()!=null){
//            LoginManager.getInstance().logOut()
//        }
//        // logout
//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            Auth.GoogleSignInApi.signOut(mGoogleApiClient)
//        }
//    }
//    private lateinit var mGoogleApiClient:GoogleApiClient
//    fun handle_initialization() {
//        // Configure sign-in to request the user's ID, email address, and basic
//        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
//        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .requestIdToken("648551950159-qn8djgas54g46ig7l8e0ovr5ub7hmmke.apps.googleusercontent.com")
//                .requestProfile()
//                .build()
//        mGoogleApiClient = GoogleApiClient.Builder(this)
//                .enableAutoManage(this/* FragmentActivity */, this /* OnConnectionFailedListener */)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .addOnConnectionFailedListener(this)
//                .addConnectionCallbacks(this)
//                .build()
//    }
//
//    override fun onConnectionFailed(p0: ConnectionResult) {
//
//    }
//
//    override fun onConnected(p0: Bundle?) {
//        // logout
//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            Auth.GoogleSignInApi.signOut(mGoogleApiClient)
//        }
////        LoggoutActivity@this.finish()
//    }
//
//    override fun onConnectionSuspended(p0: Int) {
//
//    }
//}