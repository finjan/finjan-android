package com.finjan.securebrowser.vpn.licensing.models.server;

import com.avira.common.authentication.models.Subscription;
import com.avira.common.backend.oe.BaseResponse;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovidiu.buleandra
 * @since 06.11.2015
 */
public class ProcessPurchaseResponse extends BaseResponse{
    @SerializedName("subscription") private Subscription subscription;

    public Subscription getSubscription() { return subscription; }
}
