package com.finjan.securebrowser.vpn.ui.main;

import android.content.Context;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.custom_views.BaseTextview;
import com.finjan.securebrowser.helpers.logger.Logger;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by navdeep on 9/3/18.
 */

public class RegionAdapterNew extends RecyclerView.Adapter<RegionAdapterNew.ParentHolder> {
    private static final int CountryType = 0;
    private static final int CountryCityType = 1;
    private ArrayList<RegionModelNew> regionArrayList;
    private ArrayList<RegionModelNew> orignalList = new ArrayList<>();
    private CountryClickListener countryClickListener;
    private Context context;
    private boolean showCountryList;
//    private RegionModelNew nearestLocation;
    private LayoutInflater inflater;

    public RegionAdapterNew(RegionFragment listener, Collection<RegionModelNew> regionArrayList, boolean showCountryList) {
        countryClickListener = listener;
        this.showCountryList = showCountryList;
//        this.nearestLocation = regionModelNew;
        this.regionArrayList = (ArrayList<RegionModelNew>) regionArrayList;
//        regionArrayList.remove(nearestLocation);
//        this.regionArrayList.add(0, nearestLocation);
        this.orignalList.addAll(regionArrayList);
        this.inflater = LayoutInflater.from(listener.getContext());
    }

    @Override
    public ParentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.region_list_item_country, parent, false);
        return viewType == 0?new CountryViewHolder(view):new CountryCityViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {return showCountryList?CountryType:CountryCityType;}

    public void notify(ArrayList<RegionModelNew> list, boolean listType) {
        regionArrayList = list;
        orignalList.clear();
        orignalList.addAll(regionArrayList);
        this.showCountryList = listType;
        notifyDataSetChanged();
    }

    public ArrayList<RegionModelNew> getList() {
        return this.orignalList;
    }

    public void notify(ArrayList<RegionModelNew> list) {
        regionArrayList = list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ParentHolder holder, int position) {

        RegionModelNew regionModelNew = regionArrayList.get(position);
        if (holder instanceof CountryViewHolder) {
            holder.imgCountryFlag.setImageResource(regionModelNew.getCountryFlag());
            holder.tvCountryName.setText(regionModelNew.getName());
            holder.imgRightArrow.setVisibility(regionModelNew.getShowArrow());
            holder.layoutForCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    countryClickListener.onCountrySelected(regionArrayList.get(holder.getAdapterPosition()));
                }
            });
        } else {
            holder.tvCountryName.setText(regionArrayList.get(position).getName());
            holder.imgCountryFlag.setVisibility(View.GONE);
            holder.layoutForCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    countryClickListener.onCitySelected(regionArrayList.get(holder.getAdapterPosition()));
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        Logger.logE("RegionAdapterNew adapter", regionArrayList.size() + "");
        return regionArrayList.size();
    }

    public interface CountryClickListener {
        void onCountrySelected(RegionModelNew regionModelNew);

        void onCitySelected(RegionModelNew regionModelNew);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View view) {
            super((view));
        }
    }

    class ParentHolder extends ViewHolder{
        private ImageView imgCountryFlag;
        private BaseTextview tvCountryName;
        private ImageView imgRightArrow;
        private LinearLayoutCompat layoutForCountry;
        public ParentHolder(View itemView){
            super(itemView);
            layoutForCountry = (LinearLayoutCompat) itemView.findViewById(R.id.layout_for_country);
            imgCountryFlag = (ImageView) itemView.findViewById(R.id.country_flag);
            tvCountryName = (BaseTextview) itemView.findViewById(R.id.country_name);
            imgRightArrow = (ImageView) itemView.findViewById(R.id.arrow_right);
        }
    }
    class CountryViewHolder extends ParentHolder {
        public CountryViewHolder(View itemView) {super(itemView);}
    }

    class CountryCityViewHolder extends ParentHolder {
        public CountryCityViewHolder(View itemView) {super(itemView);}
    }
}
