package com.finjan.securebrowser.vpn.licensing;

import android.text.TextUtils;

import com.avira.common.database.ConstantValue;
import com.avira.common.database.Settings;
import com.finjan.securebrowser.vpn.licensing.models.restful.License;
import com.finjan.securebrowser.vpn.licensing.models.restful.LicenseArray;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anurag on 03/07/17.
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class LicenceSetting {


    public static void saveLicenses(List<License> licenses) {
        LicenseArray array = new LicenseArray();
        array.setLicenses(licenses);
        Settings.writeSetting(Settings.SETTINGS_LICENSES, new Gson().toJson(array));
    }

    public static List<License> readLicenses() {
        List<License> result;

        String licensesJson = Settings.readSetting(Settings.SETTINGS_LICENSES, ConstantValue.SETTINGS_EMPTY);
        if (!TextUtils.isEmpty(licensesJson)) {
            LicenseArray licenseArray = new Gson().fromJson(licensesJson, LicenseArray.class);
            result = licenseArray.getLicenses();
        } else {
            result = new ArrayList<>();
        }

        return result;

    }

}
