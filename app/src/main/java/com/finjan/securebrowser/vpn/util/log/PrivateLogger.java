package com.finjan.securebrowser.vpn.util.log;

import com.finjan.securebrowser.vpn.FinjanVPNApplication;

import de.blinkt.openvpn.core.VpnStatus;

/**
 * Created by Illia.Klimov on 2/3/2016.
 */
public class PrivateLogger implements VpnStatus.LogListener {

    @Override
    public void newLog(VpnStatus.LogItem logItem) {
        FileLogger.logToFile(PrivateLogger.class, logItem.getLogLevel().name() + " " + logItem.getString(FinjanVPNApplication.getInstance()));
    }
}