/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.controller;

import android.content.Context;
import android.database.Cursor;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.ConfigParser;
import de.blinkt.openvpn.core.ProfileManager;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
import com.finjan.securebrowser.vpn.controller.database.DatabaseContract;
import com.finjan.securebrowser.vpn.util.AutoClose;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;

public class ConnectionConfig {

    public static final String TAG = ConnectionConfig.class.getSimpleName();
    private static final String HEADER_CLIENT = "client";
    private static final String HEADER_REMOTE = "remote";

    /**
     * Load a vpn configuration based on a serverVpnId
     *
     * @param context
     * @param serverVpnId
     * @return
     * @throws IOException
     */
    @Nullable
    public String processConfig(@NonNull Context context, long serverVpnId) throws IOException {

        ContentProviderClient contentProviderClient = new ContentProviderClient();
        Cursor cursor = contentProviderClient.getServer(context, serverVpnId);

        String vpnServerUrl = null;
        if (cursor != null) {
            if (cursor.moveToNext()) {
                vpnServerUrl = cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.HOST));
            }
            AutoClose.closeSilently(cursor);
        }

        if (TextUtils.isEmpty(vpnServerUrl)) {
            return null;
        }

        StringBuilder stringBuffer = new StringBuilder();

        stringBuffer.append(HEADER_CLIENT).append("\n");
        stringBuffer.append(HEADER_REMOTE).append(" ").append(vpnServerUrl).append("\n");

        byte[] headers = stringBuffer.toString().getBytes();
        InputStream inputStream = context.getResources().openRawResource(R.raw.avira_vpn);
        int available = inputStream.available();
        byte[] certificate = new byte[available];

        inputStream.read(certificate);
        inputStream.close();

        ByteBuffer byteBuffer = ByteBuffer.allocate(headers.length + available);
        byteBuffer.put(headers);
        byteBuffer.put(certificate);


        InputStream is = new ByteArrayInputStream(byteBuffer.array());
        VpnProfile mResult = parseVpnProfile(is);
        is.close();

        return saveProfile(context, serverVpnId, mResult);
    }

    /**
     * Save {@link VpnProfile} to a database
     *
     * @param context
     * @param serverVpnId
     * @param mResult
     * @return Uuid of a current {@link VpnProfile}
     */
    @NonNull
    private String saveProfile(@NonNull Context context, long serverVpnId, @NonNull VpnProfile mResult) {
        ProfileManager vpl = ProfileManager.getInstance(context);

        vpl.addProfile(mResult);
        vpl.saveProfile(context, mResult);
        vpl.saveProfileList(context);

        int rowUpdated = new ContentProviderClient().updateServerVpnUuid(context, serverVpnId, mResult.getUUIDString());

        if (rowUpdated == 0) {
            Logger.logE(TAG, "can not update server configuration");
        }

        return mResult.getUUID().toString();
    }

    /**
     * Init {@link VpnProfile} from an {@link InputStream}
     *
     * @param is
     * @return {@link VpnProfile} or null
     */
    @Nullable
    private VpnProfile parseVpnProfile(InputStream is) {
        VpnProfile mResult = null;
        ConfigParser cp = new ConfigParser();
        try {
            InputStreamReader isr = new InputStreamReader(is);

            cp.parseConfig(isr);
            mResult = cp.convertProfile();
        } catch (IOException | ConfigParser.ConfigParseError e) {
            Logger.logE(TAG, "doImport"+ e);
        }
        return mResult;
    }
}
