package com.finjan.securebrowser.vpn.auto_connect

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.media.Image
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.Snackbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.ItemTouchHelper
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.finjan.securebrowser.R
import com.finjan.securebrowser.custom_views.BaseTextview
import com.finjan.securebrowser.helpers.NativeHelper
import com.finjan.securebrowser.helpers.security.WifiSecurityHelper
import com.finjan.securebrowser.helpers.sharedpref.UserPref
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers
import com.finjan.securebrowser.vpn.FinjanVPNApplication
import com.finjan.securebrowser.vpn.receiver.WifiConnectionReceiver
import com.finjan.securebrowser.vpn.ui.main.VPNDrawer
import com.google.android.gms.analytics.GoogleAnalytics
//import com.localytics.android.Localytics
import com.squareup.picasso.Picasso
import org.jetbrains.annotations.Nullable

class AutoConnectAcitivity : AppCompatActivity(),RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var ll_obstraction: LinearLayout
    private lateinit var tb_vpn_autoconect: SwitchCompat
     lateinit var tv_current_network: TextView
    private lateinit var tv_trusted: TextView
    private lateinit var tv_trusted_network_msg: TextView
    private lateinit var iv_current_wifi: ImageView
    private lateinit var cv_auto_connect: androidx.coordinatorlayout.widget.CoordinatorLayout
    private var title = ""


    private var trustedNetworks: ArrayList<TrustedNetworks> = ArrayList()

    companion object {
        var instance:AutoConnectAcitivity? = null

        fun getAutoConnectActivityIntent(activity: Context,title:String) {
            val intent = Intent(activity, AutoConnectAcitivity::class.java)
            intent.putExtra("title",title)
            activity.startActivity(intent)
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this@AutoConnectAcitivity
        setUpdateTrustedNetworkList()
        setContentView(R.layout.activity_auto_connect)
        NativeHelper.getInstnace().currentAcitvity = this
        setReferences()
        setAdapter()
        localytics()
        if (UserPref.getInstance().autoConnect)
        {
          askForPermission()
        }
    }

    private val MY_PERMISSIONS_REQUEST_LOCATION = 100

    private fun askForPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION)

        }

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // location-related task you need to do.
                if (ContextCompat.checkSelfPermission(this,
                                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    /* //Request location updates:
                    locationManager.requestLocationUpdates(provider, 400, 1, this);*/
                   // scanWifi()
                } else {
                    onClickBack()
                    Toast.makeText(this, "Please provide the location permission to proceed", Toast.LENGTH_SHORT).show()
                }

            } else {
                onClickBack()
                Toast.makeText(this, "Please provide the location permission to proceed", Toast.LENGTH_SHORT).show()

            }
        }
    }

    private fun localytics(){
        LocalyticsTrackers.setAuto_Connect_Page()
        GoogleTrackers.setSSettingsVPN()
    }
    private fun setUpdateTrustedNetworkList(){
        trustedNetworks = ArrayList()
        val storedArray = UserPref.getInstance().trustedNetworks
        if (storedArray.isNotEmpty()) {
            for (network in storedArray) {
                if(!TextUtils.isEmpty(network))
                trustedNetworks.add(TrustedNetworks(network))
            }
        }else{
            trustedNetworks = ArrayList()
        }
    }
    fun setDefaultLayoutOutside(){
        runOnUiThread {
            setDefaultLayout(UserPref.getInstance().autoConnect)
        }
    }

//    private fun handleConnectivityChange(connected: Boolean, type: Int) {
//        setDefaultLayout(UserPref.getInstance().autoConnect)
//    }



    private fun setReferences() {
        title = getString(R.string.auto_connect)
        if(intent.hasExtra("title")){
            title = intent.getStringExtra("title")
        }
        findViewById<View>(R.id.ll_back).setOnClickListener { onClickBack() }
        val tvToolbarTitle = findViewById<BaseTextview>(R.id.tv_title)
        tvToolbarTitle.setTextSize(resources.getDimension(R.dimen._6sdp))
        tvToolbarTitle.setText(title.toUpperCase())
        recyclerView = findViewById(R.id.rv_menu_list)
        cv_auto_connect= findViewById(R.id.cv_auto_connect)
        tb_vpn_autoconect = (findViewById(R.id.tb_vpn_autoconect))
        tv_trusted_network_msg= (findViewById(R.id.tv_trusted_network_msg))
        val labelCurrentNetwork = findViewById<TextView>(R.id.tv_label_current_network)
        tv_current_network= findViewById(R.id.tv_current_network)
        tv_trusted= findViewById(R.id.tv_trusted)
        iv_current_wifi= findViewById(R.id.iv_current_wifi)
        val labelTrustedNetworks = findViewById<TextView>(R.id.tv_label_trusted_network)
        ll_obstraction= findViewById(R.id.ll_obstraction)
        labelCurrentNetwork.setTypeface(null, Typeface.BOLD)
        labelTrustedNetworks.setTypeface(null, Typeface.BOLD)
        if (UserPref.getInstance().autoConnect)
            tb_vpn_autoconect.isChecked = true
        else
            tb_vpn_autoconect.isChecked = false
        tb_vpn_autoconect.setOnCheckedChangeListener {
            autoConnectToggle, isChecked -> onAutoConnectionChange(isChecked,true)}
        onAutoConnectionChange(UserPref.getInstance().autoConnect,false)
    }
    private fun onAutoConnectionChange(isChecked:Boolean,isManual:Boolean){

        if (isManual && isChecked)
        {
            askForPermission()
        }
        val map = HashMap<String, String>()
        if(isChecked){
            map.put("value","On")
        }else{
            map.put("value","Off")
        }
        LocalyticsTrackers.setAuto_Connect_Change(map)
        UserPref.getInstance().autoConnect =isChecked
        setDefaultLayout(isChecked)
        AutoConnectHelper.instance.whenStatusChanged(this)
    }
    private fun updateCurrentNetwork(){
        if(WifiSecurityHelper.getInstance().isWifiNetwork(this)){
            val ssid = WifiSecurityHelper.getInstance().getSsid(this)
            if(!TextUtils.isEmpty(ssid)){
                tv_current_network.text = ssid
                if(isTrustedNetwork(ssid)){
                    tv_trusted.visibility = View.VISIBLE
                    iv_current_wifi.visibility =View.GONE
                }else{
                    tv_trusted.visibility = View.GONE
                    iv_current_wifi.visibility =View.VISIBLE
                    iv_current_wifi.setImageResource(R.drawable.icon_plus_enabled)
                    iv_current_wifi.setOnClickListener {
                        onAddNetworkToTrusted(ssid)
                    }
                }
                return
            }
        }
        tv_current_network.text = getString(R.string.no_network_detected)
        iv_current_wifi.setImageResource(R.drawable.icon_plus_disabled)
        tv_trusted.visibility = View.GONE
        iv_current_wifi.visibility = View.VISIBLE
        iv_current_wifi.setOnClickListener {}
    }
    private fun setDefaultLayout(isChecked: Boolean){

        runOnUiThread {
            tb_vpn_autoconect.isChecked = isChecked
            if(isChecked){
                ll_obstraction.visibility = View.GONE
                updateCurrentNetwork()
            }else{
                ll_obstraction.visibility = View.VISIBLE
                tv_current_network.text = getString(R.string.no_network_detected)
                iv_current_wifi.setImageResource(R.drawable.icon_plus_disabled)
                tv_trusted.visibility = View.GONE
                iv_current_wifi.visibility = View.VISIBLE
            }
        }

    }
    private fun onAddNetworkToTrusted(ssid: String){
        if(!isTrustedNetwork(ssid)){
            UserPref.getInstance().addTrustedNetwork(ssid)
        }

        setUpdateTrustedNetworkList()
        (recyclerView.adapter as TrustedAdapter).notifyList(trustedNetworks)
        setDefaultLayout(true)
    }
    private fun onRemoveNetworkFromTrusted(ssid: String){
        UserPref.getInstance().removeTrustedNetwork(ssid)
        setUpdateTrustedNetworkList()
        (recyclerView.adapter as TrustedAdapter).notifyList(trustedNetworks)
        setDefaultLayout(true)
        AutoConnectHelper.instance.whenStatusChanged(this)
    }

    override fun onResume() {
        super.onResume()
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,false)
        Handler().postDelayed({
            FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,false)
        },1000)
    }

    override fun onStop() {
        super.onStop()
            FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,true)
    }


    override fun onDestroy() {
        super.onDestroy()
    }

    private fun onClickBack() {
        onBackPressed()
    }
    private fun isTrustedNetwork(ssid:String):Boolean{
        for (networks in trustedNetworks){
            if(networks.name.trim().equals(ssid)){
                return true
            }
        }
        return false
    }

    override fun onBackPressed() {
        setResult(VPNDrawer.MoIdentifier.menu_trustednetworks)
        super.onBackPressed()
        overridePendingTransition(R.anim.enter_from_left,R.anim.enter_to_right)
    }

    private fun setAdapter() {

        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerView.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
        val  itemTouchHelperCallback= RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)

        recyclerView.adapter = TrustedAdapter(this, trustedNetworks)
    }

    class TrustedNetworks(name: String) {
        var name = name
    }

    class TrustedAdapter(val context: AutoConnectAcitivity, var listOfTrustedNetwork: ArrayList<TrustedNetworks>) : androidx.recyclerview.widget.RecyclerView.Adapter<TrustedAdapter.TrustedAdapterHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrustedAdapterHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.view_safe_wifi_item, parent,false)
            return TrustedAdapterHolder(view)
        }

        override fun onBindViewHolder(holder: TrustedAdapterHolder, position: Int) {
            holder?.ssid?.text = listOfTrustedNetwork.get(position).name
            holder?.ssidToggle?.tag = listOfTrustedNetwork.get(position)
            holder?.ssidToggle?.setOnClickListener {
                //                if (it?.tag != null) {
//                    listOfTrustedNetwork.remove(it.tag)
//                    this@TrustedAdapter.notifyDataSetChanged()
                context.onRemoveNetworkFromTrusted(listOfTrustedNetwork.get(position).name)
//                }
            }
        }

        override fun getItemCount(): Int {
            if(listOfTrustedNetwork.size == 0){
                context.tv_trusted_network_msg.visibility =View.GONE
                return  0
            }
            context.tv_trusted_network_msg.visibility =View.VISIBLE
            return listOfTrustedNetwork.size
        }
        fun notifyList(listOfTrustedNetwork: ArrayList<TrustedNetworks>){
            this.listOfTrustedNetwork =listOfTrustedNetwork
            notifyDataSetChanged()
        }

        fun removeItem(position: Int, name:String) {
//            listOfTrustedNetwork.removeAt(position)
            // notify the item removed by position
            // to perform recycler view delete animations
            // NOTE: don't call notifyDataSetChanged()
            context.onRemoveNetworkFromTrusted(name)
        }

        fun restoreItem(item: TrustedNetworks, position: Int) {
//            listOfTrustedNetwork.add(position, item)
            // notify item added by position
//            notifyItemInserted(position)
            context.onAddNetworkToTrusted(item.name)
        }

        class TrustedAdapterHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
            val ssid = view.findViewById<TextView>(R.id.tv_wifi)
            val ssidToggle = view.findViewById<ImageView>(R.id.iv_wifi)

            var viewBackground= view.findViewById<RelativeLayout>(R.id.view_background)
             var viewForeground= view.findViewById<RelativeLayout>(R.id.view_foreground)
        }
    }

    public override fun onSwiped(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, direction: Int, position: Int) {

        if(viewHolder is TrustedAdapter.TrustedAdapterHolder){
            val name = trustedNetworks.get(viewHolder.adapterPosition).name

            // backup of removed item for undo purpose
            val deletedItem = trustedNetworks.get(viewHolder.adapterPosition)
            val deletedIndex = viewHolder.adapterPosition
            FinjanVPNApplication.getInstance().setIsMyAppInBackGround(AutoConnectAcitivity.instance, false)

            // remove the item from recycler view
            (recyclerView.adapter as TrustedAdapter).removeItem(viewHolder.adapterPosition,name)

            //showing snak bar with Undo uption
            val snackBar = Snackbar.make(cv_auto_connect, name + " removed from "+ getString(R.string.trusted_network), Snackbar.LENGTH_LONG)
            snackBar.setAction(getString(R.string.undo), View.OnClickListener {
                (recyclerView.adapter as TrustedAdapter).restoreItem(deletedItem,deletedIndex)
            })
            snackBar.setActionTextColor(Color.YELLOW)
            snackBar.show()
        }
    }
}