package com.finjan.securebrowser.vpn.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.electrolyte.sociallogin.socialLogin.FbSignInHandler
import com.electrolyte.sociallogin.socialLogin.GoogleSignInHandler
import com.finjan.securebrowser.R
import com.finjan.securebrowser.helpers.NativeHelper
import com.finjan.securebrowser.vpn.FinjanVPNApplication
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient

/**
 * Created by anurag on 20/03/18.
 */
open class VPNDrawer:AppCompatActivity(),GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{

    companion object {

        public const final val Drawer_Result=30
        fun  getInstance(activity: Activity){
            val intent=Intent(activity,VPNDrawer::class.java)
            activity.startActivityForResult(intent, Drawer_Result)
        }
    }

    private lateinit var googleSignInHandler: GoogleSignInHandler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vpn_drawer)
        NativeHelper.getInstnace().currentAcitvity = this
        overridePendingTransition(R.anim.enter_from_right,R.anim.enter_to_left)
        googleSignInHandler = GoogleSignInHandler()
        googleSignInHandler.handle_initialization(this)
        supportFragmentManager.beginTransaction()
                .add(R.id.frame_container, MenuDrawerFragment.getInstance())
//                .setCustomAnimations(0, R.anim.enter_to_right, R.anim.enter_from_left, R.anim.enter_to_right)
                .commitAllowingStateLoss()
    }

    private lateinit var mGoogleApiClient:GoogleApiClient

    override fun onResume() {
        super.onResume()
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,false)
    }

    override fun onStop() {
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,true)
        super.onStop()
    }



    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onConnected(p0: Bundle?) {

    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onBackPressed() {
            super.onBackPressed()
            overridePendingTransition(R.anim.enter_from_left,R.anim.enter_to_right)
    }

    fun logoutFb(){
        FbSignInHandler.LogoutUser()
        googleSignInHandler.logout()
    }

    class MoIdentifier{
        companion object {
            const val menu_logout=1
            const val menu_upgrade=2
            const val menu_secure_browser=3
            const val menu_share=4
            const val menu_rate=5
            const val menu_about=6
            const val menu_help=7
            const val menu_my_ip=8
            const val menu_settings=9
            const val menu_change_pwd=10
            const val menu_vs_desktop=11
            const val menu_login=12
            const val menu_register=13
            const val menu_auto_connect=14
            const val menu_trustednetworks=15
            const val menu_redeem_offer=16
            const val menu_submit_debug_report=106
            const val menu_make_you_token_expire=107
            const val menu_make_you_time_expire=108
            const val cancel_all_promos=109
            const val menue_watch=111
            const val testing_Location_promo=110
        }

    }
}