/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.licensing;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.avira.common.activities.BaseFragmentActivity;
import com.finjan.securebrowser.activity.HomeActivity;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.vpn.licensing.models.billing.IabResult;
import com.finjan.securebrowser.vpn.licensing.models.billing.Inventory;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.models.billing.SkuDetails;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper.OnConsumeFinishedListener;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper.OnConsumeMultiFinishedListener;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper.OnIabPurchaseFinishedListener;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper.OnIabSetupFinishedListener;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper.QueryInventoryFinishedListener;
import com.finjan.securebrowser.vpn.ui.iab.UpgradeVpnActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.avira.common.CommonLibrary.DEBUG;

/**
 * base activity to be used when implementing a purchase activity
 * handles the {@link IabHelper} initialization and disposal and other boiler-plate code
 * <br/><br/>
 *
 * !! requires the following line added to the app's manifest<br/>
 * &lt;uses-permssion android:name="com.android.vending.BILLING" &#47;>
 * <br/><br/>
 *
 * extending this abstract class enforces implementing some methods:<br/>
 * {@link PurchaseLicenseBaseActivity#onInventoryAvailable} to be notified when the inventory is available<br/>
 * {@link PurchaseLicenseBaseActivity#onIabPurchaseFinished} to react to a successful purchase<br/>
 * {@link PurchaseLicenseBaseActivity#complain} to display in your custom way the error messages while purchasing<br/>
 * {@link PurchaseLicenseBaseActivity#setWaitScreen} to display a progress or alternative screen while the user waits
 * for a purchase service resolution
 * <br/><br/>
 *
 *
 * @author ovidiu.buleandra
 * @since 03.11.2015
 */
public abstract class PurchaseLicenseBaseActivity extends BaseFragmentActivity
        implements OnIabSetupFinishedListener, QueryInventoryFinishedListener, OnIabPurchaseFinishedListener,
        OnConsumeFinishedListener, OnConsumeMultiFinishedListener {
    protected static final String TAG = PurchaseLicenseBaseActivity.class.getSimpleName();
    protected static final int RC_IAB = 48392;

    private IabHelper mHelper;
    private List<String> mMoreSKUs = new ArrayList<>();
    private String mDeveloperPayload;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        boolean fetchProducts=true;
        if(this instanceof HomeActivity){
            fetchProducts=!TextUtils.isEmpty(UserPref.getInstance().getTempPurchaseToken());
        }
        if(fetchProducts){
            mHelper = new IabHelper(this);
            mHelper.enableDebugLogging(DEBUG);// TODO maybe is necessary also on release builds?

            Collection<String> skus = getManagedSKUs();
            if(skus != null) {
                mMoreSKUs = new ArrayList<>(skus);
            } else {
                mMoreSKUs = new ArrayList<>();
            }

            mHelper.startSetup(this);
        }

        // this comment should stay here for future reference
//        try {
//            Intent intent = AccountPicker.newChooseAccountIntent(null, null,
//                    new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, false, "Necessary to associate payment with an account", null, null, null);
//            startActivityForResult(intent, RC_EMAIL);
//        } catch (ActivityNotFoundException e) {
//        }
//        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
//        Account[] list = manager.getAccounts();
//        for(Account account:list) {
//            if (account.type.equals("com.google")) {
//                String possibleEmail = account.name;
//                Log.i(TAG, "account email=" + possibleEmail);
//            }
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // very important:
        Logger.logD(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.logD(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Logger.logD(TAG, "onActivityResult handled by IabHelper.");
        }
    }

    @Override
    public void onIabSetupFinished(IabResult result) {
        Logger.logD(TAG, "Setup finished.");

        if (!result.isSuccess()) {
            // Oh noes, there was a problem.
            complain(result);
            return;
        }

        // Have we been disposed of in the meantime? If so, quit.
        if (mHelper == null) return;

        // IAB is fully set up. Now, let's get an inventory of stuff we own.
        Logger.logD(TAG, "Setup successful. Querying inventory.");
        setWaitScreen(true);
        mHelper.queryInventoryAsync(true, mMoreSKUs, this);
    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
        Logger.logD(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

        // if we were disposed of in the meantime, quit.
        if (mHelper == null) return;

        if (result.isFailure()) {
            if(result.getResponse() != IabHelper.IABHELPER_USER_CANCELLED) {
                complain(result);
            }
            setWaitScreen(false);
            return;
        }
        if (!verifyDeveloperPayload(purchase)) {
            complain(new IabResult(IabHelper.IABHELPER_VERIFICATION_FAILED, null));
            setWaitScreen(false);
            return;
        }

        Logger.logD(TAG, "Purchase successful.");

        setWaitScreen(false);
        onPurchaseCompleted(purchase);
    }

    private boolean verifyDeveloperPayload(Purchase purchase) {
//        return true;
        return purchase.getDeveloperPayload().equals(mDeveloperPayload);
    }

    @Override
    public void onQueryInventoryFinished(IabResult result, Inventory inv) {
        Logger.logD(TAG, "Query inventory finished.");

        // Have we been disposed of in the meantime? If so, quit.
        if (mHelper == null) return;

        // Is it a failure?
        if (result.isFailure()) {
            complain(result);
            return;
        }

        Logger.logD(TAG, "Query inventory was successful.");

        /*
         * Check for items we own. Notice that for each purchase, we check
         * the developer payload to see if it's correct! See
         * verifyDeveloperPayload().
         */

        onInventoryAvailable(inv);
        setWaitScreen(false);
        Logger.logD(TAG, "Initial inventory query finished; enabling main UI.");
    }

    @Override
    public void onConsumeFinished(Purchase purchase, IabResult result) {
        Logger.logD(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

        // if we were disposed of in the meantime, quit.
        if (mHelper == null) return;

        if (result.isSuccess()) {
            Logger.logD(TAG, "Consumption successful. Provisioning.");
            onConsumeCompleted(purchase);
        }
        else {
            complain(result);
        }
        setWaitScreen(false);
        Logger.logD(TAG, "End consumption flow.");
    }

    @Override
    public void onConsumeMultiFinished(List<Purchase> purchases, List<IabResult> results) {
        Logger.logD(TAG, "Multiple Consumption finished. Purchase: " + purchases + ", result: " + results);

        // if we were disposed of in the meantime, quit.
        if (mHelper == null) return;
        StringBuilder errors = new StringBuilder();
        errors.append("Error while consuming: ");

        for(int i = 0; i < purchases.size() && i < results.size(); i++) {
            Purchase purchase = purchases.get(i);
            IabResult result = results.get(i);
            if (result.isSuccess()) {
                Logger.logD(TAG, "Consumption successful. ");
                onConsumeCompleted(purchase);
            } else {
                errors.append(result);
            }
        }
        setWaitScreen(false);
        Logger.logE(TAG, errors.toString());
        Logger.logD(TAG, "End consumption flow.");
    }

    protected IabHelper getIabHelper() {
        return mHelper;
    }

    protected void launchPurchaseItem(String sku) {
//        Toast.makeText(this,sku,Toast.LENGTH_LONG).show();
        if(mHelper!=null){
         mHelper.flagEndAsync();
        }
        setWaitScreen(true);
        if(mHelper!=null && !mHelper.isAsyncInProgress()){
            mHelper.launchPurchaseFlow(this, sku, IabHelper.ITEM_TYPE_INAPP, RC_IAB, this, developerPayload());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

//    protected boolean purchaseItemStartsConsumed;

    protected void launchPurchaseSubscription(String sku) {
        setWaitScreen(true);
        mHelper.launchPurchaseFlow(this, sku, IabHelper.ITEM_TYPE_SUBS, RC_IAB, this, developerPayload());
    }

    private String developerPayload() {
        if(TextUtils.isEmpty(mDeveloperPayload)) {
            mDeveloperPayload = getDeveloperPayload();
        }
        return mDeveloperPayload;
    }

    protected void launchPurchase(SkuDetails skuDetails) {
        if(skuDetails.isManagedProduct()) {
            launchPurchaseItem(skuDetails.getSku());
        } else {
            launchPurchaseSubscription(skuDetails.getSku());
        }
    }

    protected void consumePurchase(Purchase purchase) {
        setWaitScreen(true);
        mHelper.consumeAsync(purchase, this);
    }

    protected void consumePurchases(List<Purchase> purchases) {
        setWaitScreen(true);
        mHelper.consumeAsync(purchases, this);
    }

    protected abstract String getDeveloperPayload();

    /**
     * must implement this method to display error messages back to user in any way you see fit
     * @param message the IAB result object(contains error code and message)
     */
    protected abstract void complain(IabResult message);

    /**
     * must implement this method to receive the queried inventory for the current user
     * @param inventory an inventory of owned items
     */
    protected abstract void onInventoryAvailable(Inventory inventory);

    /**
     * must implement this method to receive the successful purchase data
     * @param purchase a successful purchase data
     */
    protected abstract void onPurchaseCompleted(Purchase purchase);

    protected abstract void onConsumeCompleted(Purchase purchase);

    /**
     * must implement this method to display a blocking progress dialog or alternate the purchase layout while we wait
     * for responses from Google Play Billing Service
     * @param state true - enable the blocking, false - disable
     */
    protected abstract void setWaitScreen(boolean state);

    protected abstract Collection<String> getManagedSKUs();
}
