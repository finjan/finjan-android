/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.vpn.helpers.JsonConstans;
import com.finjan.securebrowser.vpn.subscriptions.SubscriptionHelper;
import com.finjan.securebrowser.vpn.licensing.LicensesRefreshedEvent;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
import com.finjan.securebrowser.vpn.subscriptions.Subscriptions;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment;
import com.finjan.securebrowser.vpn.util.TrafficController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;

public class JsonConfigParser extends JsonConstans {

    public static final String TAG = JsonConfigParser.class.getSimpleName();

    public static final String DEFAULT_SERVER = "default_server";
    private static final String REGIONS = "regions";
    private static final String DEFAULT = "default";
    private static final String NAME = "name";
    private static final String HOST = "host";
    private static final String PORT = "port";
    private static final String PROTOCOL = "protocol";
    private static final String REGION = "id";
    public static final String TRAFFIC_USED = "used";
    public static final String TRAFFIC_LIMIT = "limit";
    public static final String GRACE_PERIOD = "grace_period";
    public static final String EXPIRATION_DATE = "expiration_date";
    public static final String TYPE = "type";




    // needed for second JSON
    // probably will be modified later
    private static final String TRAFFIC_LIMIT_2 = "traffic_limit";

    /**
     * Parse json config file and save it to database
     *
     * @param context
     * @param jsonConfig
     * @return the default vpn server region, or null if no default server was predefine
     */
    @Nullable
    public static String parseJsonConfig(@NonNull Context context, @NonNull JSONObject jsonConfig) {

        String defaultServer = null;
        try {
            defaultServer = jsonConfig.getString(DEFAULT);
            JSONArray regionsArray = jsonConfig.getJSONArray(REGIONS);
            ContentProviderClient providerClient = new ContentProviderClient();
            providerClient.removeAllServers(context);
            JSONObject item;
            for (int i = 0; i < regionsArray.length(); i++) {
                item = regionsArray.getJSONObject(i);

                String name = item.getString(NAME);
                String host = item.getString(HOST);
                String port = item.getString(PORT);
                String protocol = item.getString(PROTOCOL);
                String region = item.getString(REGION);

                // save to db
                providerClient.insertServer(context, name, region, host, port, protocol);
            }
            //save default server to preference
            saveDefaultServer(context, defaultServer);

            vpnServerListTime(context, System.currentTimeMillis());
        } catch (JSONException e) {
            Logger.logE(TAG, "json parse error"+e);
        }
        return defaultServer;
    }


    /**
     * fetch traffic amount and traffic limit from a json
     *
     * @param context
     * @param jsonTrafficConsumption
     */
    public static void parseJsonTrafficConsumption(@NonNull Context context, @NonNull JSONObject jsonTrafficConsumption) {
        try {
            UserPref.getInstance().setLastTrafficUpdateDate();
            long usedTraffic = jsonTrafficConsumption.getLong(TRAFFIC_USED);
            long limit = jsonTrafficConsumption.getLong(TRAFFIC_LIMIT);
            long gracePeriod = jsonTrafficConsumption.getLong(GRACE_PERIOD);

            Logger.logD(TAG, "usedTraffic " + usedTraffic + " limit " + limit);
                TrafficController.getInstance(context).saveTraffic(usedTraffic);
                if(limit> 0){
                    TrafficController.getInstance(context).saveTrafficLimit(TrafficController.DEFAULT_TRAFFIC_LIMIT);
                }else {
                    TrafficController.getInstance(context).saveTrafficLimit(0);
                }
            TrafficController.getInstance(context).saveTrafficLimitTimeSpan(gracePeriod);
        } catch (JSONException e) {
            Logger.logE(TAG, "json parse error"+ e);
        }
    }

    /**
     * Save a time of a last successful vpn servers list update
     *
     * @param context
     */
    private static void vpnServerListTime(@NonNull Context context, long timeInMillis) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsedit = prefs.edit();
        prefsedit.putLong(VpnHomeFragment.VPN_SERVER_LIST, timeInMillis);
        prefsedit.apply();
    }

    /**
     * Save default vpn server region id in a SharedPreferences
     *
     * @param context
     * @param defaultServer
     */
    private static void saveDefaultServer(@NonNull Context context, @NonNull String defaultServer) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsedit = prefs.edit();
        prefsedit.putString(DEFAULT_SERVER, defaultServer);
        prefsedit.apply();

    }

    public static void parseJsonLicense(Context context, JSONObject response) {
        Logger.logE(TAG,"value of logout "+VpnActivity.UserLogout);
        if(VpnActivity.UserLogout){return;}
        try {
                String expirationDate = response.optString(EXPIRATION_DATE);
                if(!TextUtils.isEmpty(expirationDate)){
                    TrafficController.getInstance(context).saveExpirationDate(expirationDate);
                }
            String type = response.getString(TYPE);
            TrafficController.getInstance(context).saveLicenseType(type);
            GlobalVariables.getInstnace().isLicenseFetched=true;

            EventBus.getDefault().post(new LicensesRefreshedEvent(null));
        } catch (JSONException e) {
            Logger.logE(TAG, "json parse error"+e);
        }
    }
//    public static void makeFreeLicenseLogin(Context context){
//        TrafficController.getInstance(context).saveLicenseType(TrafficController.LICENSE_FREE);
//        GlobalVariables.getInstnace().isLicenseFetched=true;
//        EventBus.getDefault().post(new LicensesRefreshedEvent(null));
//    }
    public static void parseJsonAllSubs(JSONObject response,boolean general, boolean cancelled){
        HashMap<String,ArrayList<Subscriptions>> subscriptionsMap=new HashMap<>();
        if(response!=null && response.has(DATA)){
            try {
                JSONArray jsonArray=response.getJSONArray(DATA);
                ArrayList<Subscriptions> subscriptionses=null;
                for (int i=0;i<jsonArray.length();i++){
                    Subscriptions subscription=new Subscriptions(jsonArray.getJSONObject(i));
                    if(subscriptionsMap.containsKey(subscription.getSource())){
                        subscriptionses=subscriptionsMap.get(subscription.getSource());
                        subscriptionses.add(subscription);
                    }else {
                        subscriptionses= new ArrayList<>();
                        subscriptionses.add(subscription);
                    }
                    if (TextUtils.isEmpty(subscription.getComp_flg()) && (subscription.getDevice_type().equals("all") || subscription.getDevice_type().equals("android")))
                    {
                        subscriptionsMap.put(GOOGLE,subscriptionses);
                    }
                    else if (!TextUtils.isEmpty(subscription.getComp_flg()) && (subscription.getDevice_type().equals("all") || subscription.getDevice_type().equals("android")))
                    {
                        subscriptionsMap.put(PROMO,subscriptionses);
                    }
//                    subscriptionsMap.put(subscription.getSource(),subscriptionses);
                }
            } catch (JSONException e) {
                Logger.logE(TAG, "json parse error"+e);
            }
        }
        if(cancelled){
            SubscriptionHelper.getInstance().setAllCancelledSubscriptionsHashMap(subscriptionsMap);
            return;
        }
        if(general){
            SubscriptionHelper.getInstance().setSubscriptionsHashMap(subscriptionsMap);

          //  VpnHomeFragment.getInstance().checkIfAndSetDaysLeft();
        }else {
            SubscriptionHelper.getInstance().setAllPromoSubscriptionsHashMap(subscriptionsMap);
        }
    }
}
