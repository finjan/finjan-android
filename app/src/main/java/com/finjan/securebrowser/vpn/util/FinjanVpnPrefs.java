package com.finjan.securebrowser.vpn.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.avira.common.utils.SharedPreferencesUtilities;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.JsonConfigParser;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import de.blinkt.openvpn.core.VpnStatus;

/**
 * Created by Illia.Klimov on 2/23/2016.
 */
public class FinjanVpnPrefs {

    private static final String TAG = FinjanVpnPrefs.class.getSimpleName();
    private static final String SETUP_COMPLETE = "prefs_setup_complete";
    public static final String AUTHORIZATION_TOKEN = "prefs_authorization_token";
    public static final String REFRESH_TOKEN = "prefs_referesh_token";
    private static final String LAST_USED_SERVER = "last_used_server";
    private static final String VPN_CONNECTION_STATUS = "vpn_connection_status";
    private static final String PREFS_SMART_MONITOR = "prefs_smart_monitor";
    private static final String PREFS_REQUESTED_LOCATION_PERMISSION = "prefs_requested_location_permission";
    private static final String PREFS_REQUESTED_ACCOUNTS_PERMISSION = "prefs_requested_accounts_permission";
    private static final String KEY_WHITE_LISTED_WIFI_LIST = "white_listed_wifi_list";
    private static final String WIFI_PERMISSION_NOTIFICATION_COUNT = "WIFI_PERMISSION_NOTIFICATION_COUNT";

    /**
     * Cannot enable setting by default for Android-M because we need to handle location permission request
     */
    private static final boolean DEFAULT_SMART_MONITOR_VALUE = true;
//    private static final boolean DEFAULT_SMART_MONITOR_VALUE = Build.VERSION.SDK_INT < Build.VERSION_CODES.M ? true : false;
    private static final String FTU_ENTRY = "pref_ftu_entry";

    public static void setWifiPermissionNotificationCount(Context context, int count) {
        SharedPreferencesUtilities.putInt(context, WIFI_PERMISSION_NOTIFICATION_COUNT, count);
    }

    public static int getWifiPermissionNotificationCount(Context context) {
        return SharedPreferencesUtilities.getInt(context, WIFI_PERMISSION_NOTIFICATION_COUNT);
    }

    public static void setSetupCompleted(Context context) {
        SharedPreferencesUtilities.putBoolean(context, SETUP_COMPLETE, true);
    }

    public static boolean isSetupCompleted(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, SETUP_COMPLETE);
    }

    public static void setAuthToken(Context context, String token) {
        SharedPreferencesUtilities.putString(context, AUTHORIZATION_TOKEN, token);
    }

    public static String getAuthToken(Context context) {
        // token concatenation dfdf+ tdfdfye6m6yimnalsr2mk66v23cfy9r3vxf3z5jpjkg4
//        return "dfdfye6m6yimnalsr2mk66v23cfy9r3vxf3z5jpjkg4";

        // expired token
//        return "3buwcxozx7ih84exmawyziephbitbeoabuyq1qlt";


        if(AppConfig.Companion.getCustomDevice()){
            return AppConfig.CustomUser.Companion.getToken();
        }else{
            return SharedPreferencesUtilities.getString(context, AUTHORIZATION_TOKEN);
        }
    }

    public static void setRefereshToken(Context context, String token) {
        SharedPreferencesUtilities.putString(context, REFRESH_TOKEN, token);
    }

    public static String getRefershToken(Context context) {
        return SharedPreferencesUtilities.getString(context, REFRESH_TOKEN);
    }
    /**
     * Save the last used server region.
     * @param context
     * @param region The most recently used server region
     */
    public static void saveLastUsedServer(Context context, String region) {
        Logger.logD(TAG, "saveLastUsedServer " + region);
        if (!TextUtils.isEmpty(region)) {
            SharedPreferencesUtilities.putString(context, LAST_USED_SERVER, region);
        }
    }

    public static void saveEmptyServer(Context context) {
        String defaultRegion= SharedPreferencesUtilities.getString(context, JsonConfigParser.DEFAULT_SERVER);
        SharedPreferencesUtilities.putString(context, LAST_USED_SERVER, defaultRegion);
    }
    public static String getDefaultServer(){
        return SharedPreferencesUtilities.getString(FinjanVPNApplication.getInstance(),JsonConfigParser.DEFAULT_SERVER);
    }
    /**
     * Get the last used server region.
     * @param context
     * @return The last used server region if available, otherwise returns the default server region.
     */
    public static String getLastUsedServer(Context context) {
        if(context==null){
            context= FinjanVPNApplication.getInstance();
        }
        String region = SharedPreferencesUtilities.getString(context, LAST_USED_SERVER);
        if (TextUtils.isEmpty(region)) { // fall back to default server
            region = SharedPreferencesUtilities.getString(context, JsonConfigParser.DEFAULT_SERVER);
        }
        Logger.logD(TAG, "getLastUsedServer " + region);
        return region;
    }

    public static void saveVpnConnectionStatus(Context context, VpnStatus.ConnectionStatus status) {
        Logger.logD(TAG, "saveVpnConnectionStatus " + status);
        SharedPreferencesUtilities.putInt(context, VPN_CONNECTION_STATUS, status.ordinal());
    }

    public static VpnStatus.ConnectionStatus getVpnConnectionStatus(Context context) {
        VpnStatus.ConnectionStatus status;
        int statusOrdinal = SharedPreferencesUtilities.getInt(context, VPN_CONNECTION_STATUS, -1);

        if (statusOrdinal == -1) {
            status = VpnStatus.ConnectionStatus.UNKNOWN_LEVEL;
        } else {
            status = VpnStatus.ConnectionStatus.values()[statusOrdinal];
        }

        Logger.logD(TAG, "getVpnConnectionStatus " + status);
        return status;
    }

    /**
     * Save smart monitor setting state which determines whether to notify user whenever connected to an insecure wifi.
     * @param context
     * @param enabled True to enable smart monitor setting, otherwise false.
     */
    public static void saveSmartMonitorSetting(Context context, boolean enabled) {
        SharedPreferencesUtilities.putBoolean(context, PREFS_SMART_MONITOR, enabled);
    }

    /**
     * Check the smart monitor setting state which determines whether to notify user whenever connected to an insecure wifi.
     * @param context
     * @return True if smart monitor setting is enabled, otherwise false.
     */
    public static boolean isSmartMonitorEnabled(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, PREFS_SMART_MONITOR, DEFAULT_SMART_MONITOR_VALUE)
                && PermissionUtil.hasLocationPermission(context);
    }

    /**
     * Remember if location permission has been requested before.
     * @param context
     * @param requested True if we have attempted to request for location permission, otherwise false.
     */
    public static void setRequestedLocationPermission(Context context, boolean requested) {
        SharedPreferencesUtilities.putBoolean(context, PREFS_REQUESTED_LOCATION_PERMISSION, requested);
    }

    /**
     * Check if location permission has been requested before.
     * @param context
     * @return True if we have attempted to request for location permission, otherwise false.
     */
    public static boolean hasRequestedLocationPermission(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, PREFS_REQUESTED_LOCATION_PERMISSION, false);
    }

    /**
     * Remember that the get accounts permission has been requested before.
     * @param context
     * @param requested True if we have attempted to request for get accounts permission, otherwise false.
     */
    public static void setRequestedAccountsPermission(Context context, boolean requested) {
        SharedPreferencesUtilities.putBoolean(context, PREFS_REQUESTED_ACCOUNTS_PERMISSION, requested);
    }

    /**
     * Check if the get accounts permission has been requested before.
     * @param context
     * @return True if we have attempted to request for get accounts permission, otherwise false.
     */
    public static boolean hasRequestedAccountsPermission(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, PREFS_REQUESTED_ACCOUNTS_PERMISSION, false);
    }

    public static void setFTUCompleted(Context context) {
        SharedPreferencesUtilities.putBoolean(context, FTU_ENTRY, true);
    }

    public static boolean isFTUCompleted(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, FTU_ENTRY);
    }

    /**
     * Check if we have already shown wifi changed notification for the specified wifi.
     * If yes, we should skip showing notification for the same wifi again.
     *
     * @param context
     * @param ssid The wifi ssid to check
     * @return True if the specified wifi has been white-listed, otherwise false.
     */
    public static boolean isWifiWhiteListed(Context context, String ssid) {
        boolean whiteListed = getWhiteListedWifis(context).contains(ssid);
        Logger.logD(TAG, "isWifiWhiteListed " + ssid + "? " + whiteListed);
        return whiteListed;
    }

    /**
     * White-list a wifi so that we will not show wifi changed notification
     * the next time the user connects to the specified wifi again.
     *
     * @param context
     * @param ssid The wifi ssid to be white-listed
     */
    public static void whiteListWifi(Context context, String ssid) {
        if(context==null){
            return;
        }
        Logger.logD(TAG, "whiteListWifi " + ssid);
        if (TextUtils.isEmpty(ssid)) { return; }

        Set<String> whiteListedWifis = getWhiteListedWifis(context);
        whiteListedWifis.add(ssid);

        SharedPreferences preferences = SharedPreferencesUtilities.getSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet(KEY_WHITE_LISTED_WIFI_LIST, whiteListedWifis);
        editor.apply();
    }

    private static Set<String> getWhiteListedWifis(Context context) {
        SharedPreferences preferences = SharedPreferencesUtilities.getSharedPreferences(context);
        Set<String> whiteListedWifis = preferences.getStringSet(KEY_WHITE_LISTED_WIFI_LIST, new HashSet<String>());
        Logger.logD(TAG, "getWhiteListedWifis: " + Arrays.toString(whiteListedWifis.toArray()));
        return whiteListedWifis;
    }

    public static void setTempDeviceId(Context context,long deviceId){
        SharedPreferencesUtilities.putString(context,KEY_TEMP_DEVICE_ID,deviceId+"");}
    public static String getTempDeviceId(Context context){return
            SharedPreferencesUtilities.getString(context,KEY_TEMP_DEVICE_ID,"0");}
    private static final String KEY_TEMP_DEVICE_ID_SET="temp_device_id_set";

    private static final String KEY_TEMP_DEVICE_ID="temp_device_id";
    private static final boolean Def_unlimited_user=true;
    public static void setTempDeviceIdSet(Context context,boolean set){
        SharedPreferencesUtilities.putBoolean(context,KEY_TEMP_DEVICE_ID_SET,set);}
    public static boolean getTempDeviceIdSet(Context context){return
            SharedPreferencesUtilities.getBoolean(context,KEY_TEMP_DEVICE_ID_SET,false);}
    public static boolean getUnlimitedUser(){return Def_unlimited_user;}
}
