/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.text.TextUtils;

import com.avira.common.utils.SharedPreferencesUtilities;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.receiver.WifiNotificationAlarmReceiver;

import java.util.Date;
import java.util.Locale;

public class Util {

    public static final String TAG = Util.class.getSimpleName();
    private static final Object lock = new Object();

    /**
     * Check internet connection
     *
     * @param context
     * @return true if network is available, false otherwise
     */
    public static boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo(); // AAMA-4433

            return activeNetwork != null && activeNetwork.isConnected();
        }else {
            return false;
        }
    }

    // From: http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
    public static String humanReadableByteCount(long bytes, boolean mbit) {
        if (mbit)
            bytes = bytes * 8;
        int unit = mbit ? 1000 : 1024;
        if (bytes < unit)
            return bytes + (mbit ? " bit" : " B");

        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (mbit ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (mbit ? "" : "");
        if (mbit)
            return String.format(Locale.getDefault(), "%.1f %sbit", bytes / Math.pow(unit, exp), pre);
        else
            return String.format(Locale.getDefault(), "%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }


    public static long humanReadableByteCountLong(String readableValue){
        String value = readableValue.split(" ")[0];
        int v = 0;
        try {
            v = Integer.parseInt(value);
        }catch (Exception e){
            v = 0;
        }
        return v;
    }

    public static int humanReadableByteCount(String readableValue){
     String value = readableValue.split(" ")[0];
     int v = 0;
     try {
         v = Integer.parseInt(value);
     }catch (Exception e){
         v = 0;
     }
     return v;
    }

    public static PackageInfo getPackageInfo(String packageName) throws PackageManager.NameNotFoundException {
        PackageManager packageManager = FinjanVPNApplication.getInstance().getPackageManager();
        synchronized (lock) {
            return packageManager.getPackageInfo(packageName, 0);
        }
    }

    /**
     * Method to check if a package is still installed
     *
     * @param context
     * @param packageName the app package name
     * @return true if package is still installed, otherwise false
     */
    public static boolean isPackageInstalled(Context context, String packageName) {
        if (TextUtils.isEmpty(packageName)) {
            return false;
        }

        try {
            context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public static int getIconForRegion(Context context, String region) {
        if (region.length() < 2)
            return 0;

        region = region.toLowerCase();

        if (region.equals("uk"))
            region = "gb";

        int idx = region.indexOf('_');
        if (idx > 0) {
            // Multiple regions for the same county use '_' as a separator (ex. us_east, us_west)
            region = region.substring(0, idx);
        }

        String uri = "@drawable/ic_flag_" + region;
        return context.getResources().getIdentifier(uri, null, context.getPackageName());
    }

    public static int getColor(Context context, int resourceId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getResources().getColor(resourceId, null);
        }
        return context.getResources().getColor(resourceId);
    }


    public void DEBUG_resetDeviceId(Context context) {
        SharedPreferencesUtilities.remove(context, "pref_device_id");
        SharedPreferencesUtilities.remove(context, "pref_device_id_type");
        SharedPreferencesUtilities.remove(context, "pref_user_id");
    }

//    public static void sendWifiPermissionNotification(Context context) {
//
//        String notificationContent = context.getString(R.string.notification_wifi_permission_content);
//        final Intent intent = new Intent(context, VPNSettingsActivity.class);
//        intent.putExtra(VPNSettingsActivity.NOTIFICATION_PERMISSION, true);
//        final PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);
//
//        // build notification
//        Notification notification = new NotificationCompat.Builder(context)
//                .setSmallIcon(R.drawable.ic_notification)
//                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_vpn_192_launcher))
//                .setContentTitle(context.getString(R.string.app_name))
//                .setContentText(notificationContent)
//                .setTicker(context.getString(R.string.notif_insecure_wifi_ticker))
//                .setContentIntent(contentIntent)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationContent)) // prevents desc to be ellipsized
//                .setAutoCancel(true)
//                .build();
//
//        NotificationManagerCompat.from(context).notify(001, notification);
//    }

    public static void scheduleWifiPermissionNotification(Context context, long delayMs) {
        long triggerAtMillis = System.currentTimeMillis() + delayMs;
        Logger.logD(TAG, "scheduleNotification to " + new Date(triggerAtMillis));

        Intent i = new Intent(context, WifiNotificationAlarmReceiver.class);
        i.setAction(WifiNotificationAlarmReceiver.ACTION_NOTIF_ALARM);

        PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, i, 0);
        AlarmManager am = FinjanVPNApplication.getInstance().getAlarmManager();
        am.set(AlarmManager.RTC_WAKEUP, triggerAtMillis, pIntent);
    }

}
