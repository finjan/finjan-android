
/*
 * *
 *  * <p>
 *  * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 *  * Finjan Mobile Vital Security Browser
 *  * <p>
 *  * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *  * Developed by NewOfferings, LLC.
 *  *
 *  *
 *
 */

package com.finjan.securebrowser.vpn.controller.network.eventManeger;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

//import org.apache.http.HttpEntity;


public class Response 
{
	public static final int OK = 0;
	public static final int EXCEPTION = 1;
	public static int NETWORK_ERROR = 2;
	public static int SERVER_ERROR = 3;
	private String responseText;
//	private IModel payload;
	private String errorText;
	private int errorCode;
	private int requestId;
	private Exception exception;
//	HttpEntity entity = null;
	public boolean hasError = false;
	public byte[] responseBytes = null;

	private boolean isFromCache;

	// Sunny
	private String cacheKey;

	public boolean isFromCache() {
		return isFromCache;
	}

	public void setIsFromCache(boolean isFromCache) {
		this.isFromCache = isFromCache;
	}

	public String getCacheKey() {
		return cacheKey;
	}
	
	public void setCacheKey(String cacheKey) {
		this.cacheKey = cacheKey;
	}
	//

	public byte[] getResponseBytes() 
	{
		return responseBytes;
	}

	public void setResponseBytes(byte[] responseBytes) 
	{
		this.responseBytes = responseBytes;
	}

	public boolean isHasError() 
	{
		return hasError;
	}

	public void setHasError(boolean hasError) 
	{
		this.hasError = hasError;
	}
//
//	public HttpEntity getEntity()
//	{
//		return entity;
//	}
//
//	public void setEntity(HttpEntity entity)
//	{
//		this.entity = entity;
//	}

	public Response() {
	}
	
	/*public Response(IModel payload) {
		this.payload = payload;
	}*/
	
	public void setRequestId(int v) {
		requestId = v;
	}

	public int getRequestId() {
		return requestId;
	}

	public boolean isError() {
		return getErrorCode() > 0;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorText() {
		return errorText;
	}
	
/*	public void setPayload(IModel model){
		this.payload = model;
	}*/

	/*public IModel getPayload() {
		return payload;
	}*/

	public String getResponseText() {
		return responseText;
	}

	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}
	
	public InputStream createInputStream()
		    throws java.io.UnsupportedEncodingException {

		    return new ByteArrayInputStream(getResponseText().getBytes("UTF-8"));
		}
}
