package com.finjan.securebrowser.vpn.ui.main;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.avira.common.id.HardwareId;
import com.finjan.securebrowser.BuildConfig;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.custom_views.BaseTextview;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.ToastHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.RedeemListData;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.vpn.ui.iab.UpgradeVpnActivity;
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.TrafficController;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
/**
 *
 *
 *  Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *
 *
 * Created by anurag on 13/03/18.
 *
 *
 */

public class MenuDrawerFragment extends Fragment implements View.OnClickListener {
    public static String TAG = MenuDrawerFragment.class.getSimpleName();
    private LinearLayout llLogin, llRegister, llSecureBrowser, llShare, llRate, llAbout, llHelp;
    private LinearLayout llMyIp, llSettings, llChangePwd, llUpgrade, llLogout;
    private BaseTextview tvToolbarTitle;
    private LinearLayout llToolbar, llBack;
    private TextView tvVersion;


    private RecyclerView rv_menu_list;
    public static MenuDrawerFragment getInstance() {
        Bundle bundle=new Bundle();
//        bundle.putSerializable("list",vpnMenuArrayList);
        MenuDrawerFragment fragment=new MenuDrawerFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_drawer, container, false);
    }
    int clickCount = 0;
    long timestamp;
    Handler myHandler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            clickCount = 0;
        }
    };

    private void setVersionName(final TextView tvVersion){
        tvVersion.setText(String.format(getString(R.string.version), BuildConfig.VERSION_NAME,BuildConfig.VERSION_CODE+""));
        tvVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCount++;
                if (clickCount == 7){
                    clickCount = 0;
                    Logger.sendEmailOfLog(getContext());
                    ToastHelper.showToast(getContext(),"Sending Logs",Toast.LENGTH_LONG);
                    if (myHandler != null){
                        myHandler.removeCallbacks(runnable);
                    }
                    return;
                }

                if (myHandler != null){
                    myHandler.removeCallbacks(runnable);
                }
                myHandler.postDelayed(runnable,1000);

            }
        });

    }
    private void setDebugTexts(View view){
        if(/*AppConfig.Companion.getDebugReporting()*/PlistHelper.getInstance().isTestmode()){
            ((TextView)view.findViewById(R.id.tv_device)).setText("Device: "+HardwareId.get(getContext()));
            ((TextView)view.findViewById(R.id.tv_access)).setText("Access Token: "+FinjanVpnPrefs.getAuthToken(FinjanVPNApplication.getInstance()));
        }else {
            ((TextView)view.findViewById(R.id.tv_device)).setVisibility(View.GONE);
            ((TextView)view.findViewById(R.id.tv_access)).setVisibility(View.GONE);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        rv_menu_list=(RecyclerView)view.findViewById(R.id.rv_menu_list);
        tvToolbarTitle = (BaseTextview) view.findViewById(R.id.tv_title);
        tvToolbarTitle.setText(getString(R.string.app_name));
        llBack = (LinearLayout) view.findViewById(R.id.ll_back);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackClick();
            }
        });
        llLogin = (LinearLayout) view.findViewById(R.id.menu_login);
        llRegister = (LinearLayout) view.findViewById(R.id.menu_register);
        llAbout = (LinearLayout) view.findViewById(R.id.menu_about);
        llHelp = (LinearLayout) view.findViewById(R.id.menu_help);
        llRate = (LinearLayout) view.findViewById(R.id.menu_rate);
        llMyIp = (LinearLayout) view.findViewById(R.id.menu_my_ip);
        llSecureBrowser = (LinearLayout) view.findViewById(R.id.menu_secure_browser);
        llSettings = (LinearLayout) view.findViewById(R.id.menu_settings);
        llShare = (LinearLayout) view.findViewById(R.id.menu_share);
        llLogout = (LinearLayout) view.findViewById(R.id.menu_logout);
        llChangePwd = (LinearLayout) view.findViewById(R.id.menu_change_pwd);
        llUpgrade = (LinearLayout) view.findViewById(R.id.menu_upgrade);
        tvVersion=(TextView)view.findViewById(R.id.tv_version);
        setVersionName(tvVersion);
        setDebugTexts(view);
    }
    private ArrayList<PlistHelper.VpnMenu> sortMenuList(ArrayList<PlistHelper.VpnMenu> vpnMenuArrayList){
        Collections.sort(vpnMenuArrayList, new Comparator<PlistHelper.VpnMenu>() {
            @Override
            public int compare(PlistHelper.VpnMenu o1, PlistHelper.VpnMenu o2) {
                return o1.getOrder().compareTo(o2.getOrder());
            }
        });
        return vpnMenuArrayList;
    }

    private ArrayList<PlistHelper.VpnMenu> getActuallList(){
        HashMap<String,PlistHelper.VpnMenu> list=PlistHelper.getInstance().getVpnMenuMap();
        if(list == null){return null;}

        PlistHelper.VpnMenu forgotPassword=list.get("forgot_password");
        PlistHelper.VpnMenu logout =list.get("logout");
        PlistHelper.VpnMenu upgrade =list.get("upgrade");
        PlistHelper.VpnMenu browser =list.get("browser");
        PlistHelper.VpnMenu share =list.get("share");
        PlistHelper.VpnMenu rate =list.get("rate");
        PlistHelper.VpnMenu about =list.get("about");
        PlistHelper.VpnMenu help =list.get("help");
        PlistHelper.VpnMenu findmyip =list.get("findmyip");
        PlistHelper.VpnMenu settings =list.get("settings");
        PlistHelper.VpnMenu vs_desktop =list.get("vs_desktop");
        PlistHelper.VpnMenu vs_redeem =list.get("redeem_offer");


        if(TextUtils.isEmpty(GlobalVariables.orignal_brower_option_name)){
            GlobalVariables.orignal_brower_option_name=browser.getName();
        }
        if(UserPref.getInstance().getIsBrowser()){
            browser.setName("Disable Secure Browser");
        }else {
            browser.setName(GlobalVariables.orignal_brower_option_name);
        }
if (vs_redeem != null)
        vs_redeem.setMo_itendifier(16);



        if(TrafficController.getInstance(getContext()).isRegistered()){
            if(upgrade!=null){
                upgrade.setHide(TrafficController.getInstance(getContext()).isPaid()?1:0);
            }
            if(forgotPassword!=null){
                forgotPassword.setHide(UserPref.getInstance().getEmailLogin()?0:1);
            }
                logout.setHide(0);
                logout.setName("Logout");
                logout.setMo_itendifier(VPNDrawer.MoIdentifier.menu_logout);
                String st = getUserName();
                if (st.length() > 6) {
                    SpannableString spannableString = new SpannableString(st);
                    spannableString.setSpan(new RelativeSizeSpan(.50f), 7, st.length() - 1, 0);
                    logout.setName(st);
                }


        }else {
            if(forgotPassword!=null)
            forgotPassword.setHide(1);

            logout.setHide(0);
            logout.setName("Login");
            logout.setMo_itendifier(VPNDrawer.MoIdentifier.menu_login);
        }
      //  vs_redeem.setMo_itendifier(VPNDrawer.MoIdentifier.menu_redeem_offer);

        ArrayList<PlistHelper.VpnMenu> currentMenuList=new ArrayList<>();
        for (PlistHelper.VpnMenu menu: new ArrayList<>(list.values())){
            if(menu.getHide()==0){
                currentMenuList.add(menu);
            }
        }
        return currentMenuList;
    }

    @Override
    public void onResume() {
        super.onResume();
        GoogleTrackers.Companion.setSVPNMenu();
        LocalyticsTrackers.Companion.setEVPNMenu();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_menu_list.setLayoutManager(layoutManager);
        ArrayList<PlistHelper.VpnMenu> list = getActuallList();
        if(list == null){return;}
        rv_menu_list.setAdapter(new VPNDrawerAdapter(getContext(), sortMenuList(list), new VPNDrawerClick() {
            @Override
            public void onDrawerClick(PlistHelper.VpnMenu vpnMenu) {
                if(vpnMenu.getMo_itendifier() == VPNDrawer.MoIdentifier.menu_redeem_offer){
                    showRedeemDialog(MenuDrawerFragment.this.getActivity());
                    return;
                }

                getActivity().setResult(vpnMenu.getMo_itendifier());
                getActivity().onBackPressed();

                if(vpnMenu.getMo_itendifier()==VPNDrawer.MoIdentifier.menu_logout){
                    ((VPNDrawer)(getActivity())).logoutFb();
                }
            }
        }));
    }

    public void onBackClick() {
        getActivity().setResult(100);
        getActivity().onBackPressed();
//        getActivity().overridePendingTransition(R.anim.enter_from_left,R.anim.enter_to_right);
//        getActivity().finish();
    }

    private String getUserName() {

        if (AuthenticationHelper.getInstnace().getUserName(MenuDrawerFragment.this.getContext()) != null) {
            return "Logout ( " + AuthenticationHelper.getInstnace().getUserName(MenuDrawerFragment.this.getContext())
                    + " "+UserPref.getInstance().getLoginVia() + ")";
        }
        return "Logout";
    }


    @Override
    public void onClick(View v) {
        getActivity().setResult(v.getId());
        getActivity().onBackPressed();

        if(v.getId()==VPNDrawer.MoIdentifier.menu_logout){
            ((VPNDrawer)(getActivity())).logoutFb();
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    interface VPNDrawerClick{
        void onDrawerClick(PlistHelper.VpnMenu vpnMenu);
    }

    class VPNDrawerAdapter extends RecyclerView.Adapter<VPNDrawerAdapter.VPNDrawerHolder>{

        private Context context;
        private ArrayList<PlistHelper.VpnMenu> vpnMenuArrayList;
        private VPNDrawerClick click;

        public void notifyAdapter(ArrayList<PlistHelper.VpnMenu> vpnMenuArrayList){
            if(vpnMenuArrayList!=null && vpnMenuArrayList.size()>0){
                this.vpnMenuArrayList = vpnMenuArrayList;
                this.notifyDataSetChanged();
            }
        }
        VPNDrawerAdapter(Context context, ArrayList<PlistHelper.VpnMenu> vpnMenuArrayList, VPNDrawerClick vpnDrawerClick){

            if(TrafficController.getInstance(context).isPaid()){
                PlistHelper.VpnMenu  menuToRemove = null;
                for (PlistHelper.VpnMenu  menu: vpnMenuArrayList){
                    if(menu.getMo_itendifier() == 16){
                        menuToRemove= menu;
                    }
                }
                if(menuToRemove!=null){vpnMenuArrayList.remove(menuToRemove);}
            }


            this.context=context;
            this.vpnMenuArrayList=vpnMenuArrayList;
            this.click=vpnDrawerClick;
        }

        @Override
        public int getItemCount() {
            return vpnMenuArrayList.size();
        }

        @Override
        public void onBindViewHolder(VPNDrawerHolder vpnDrawerHolder, int i) {
            final PlistHelper.VpnMenu vpnMenu=vpnMenuArrayList.get(i);
            if(!TextUtils.isEmpty(vpnMenu.getIcon()) && vpnMenu.getIcon().toLowerCase().startsWith("menu")){
                int native_icon= ResourceHelper.getInstance().getImageResources(vpnMenu.getIcon(), R.drawable.tab_vpn_off);
                vpnDrawerHolder.ivIcon.setImageResource(native_icon);
            }
            if(vpnMenu!=null && vpnMenu.getMo_itendifier()==VPNDrawer.MoIdentifier.menu_logout){
                String st = getUserName();
                if (st.length() > 6) {
                    SpannableString spannableString = new SpannableString(st);
                    spannableString.setSpan(new RelativeSizeSpan(.75f), 7, st.length() - 1, 0);
                    vpnDrawerHolder.title.setText(spannableString);
                }
            }else
            vpnDrawerHolder.title.setText(vpnMenu.getName());
            if(vpnMenu.getKey().equalsIgnoreCase("upgrade") && TrafficController.getInstance(context).isPaid()){
                vpnDrawerHolder.itemView.setVisibility(View.GONE);
            }else {
                vpnDrawerHolder.itemView.setVisibility(View.VISIBLE);
            }
            vpnDrawerHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    VPNDrawerAdapter.this.click.onDrawerClick(vpnMenu);
                }
            });
        }

        @Override
        public VPNDrawerHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view=LayoutInflater.from(context).inflate(R.layout.view_vpn_drawer,viewGroup,false);
            return new VPNDrawerHolder(view);
        }

        class VPNDrawerHolder extends RecyclerView.ViewHolder{

            private ImageView ivIcon;
            private TextView title;

            public VPNDrawerHolder(View view){
                super(view);
                ivIcon=(ImageView) view.findViewById(R.id.iv_icon);
                title=(TextView) view.findViewById(R.id.tv_title);
            }
        }
    }
    public static Object checkCode(String redeemCode,Context context){
        ArrayList<RedeemListData> list = PromoHelper.Companion.getInstance().getAllPromoList(context);
        if(list==null){
            return null;
        }
        for (RedeemListData redeemListData: list){
            if (redeemListData.getAttributes().getRedeem_code().equals(redeemCode)){
                return redeemListData;
            }
        }
        return false;
    }

    public static void showRedeemDialog(final Activity context){
        showRedeemDialog(context,"");
    }

    public static void showRedeemDialog(final Activity context,String redeemCode){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        }
        dialog.setContentView(R.layout.view_redeem_code);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        final EditText edOfferCode = dialog.findViewById(R.id.et_redeem_offer_code);
        final TextView tv_error= dialog.findViewById(R.id.tv_error);
        final View loader= dialog.findViewById(R.id.vg_loader);
        final TextView btnReedeem = dialog.findViewById(R.id.btn_redeem_offer);
        if(!TextUtils.isEmpty(redeemCode)){
            edOfferCode.setText(redeemCode);
            edOfferCode.setSelection(redeemCode.length());
        }else {
            btnReedeem.setBackgroundResource(R.drawable.bg_bottom_btn_dialog_grey);
        }
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(edOfferCode, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        InputFilter[] editFilters = edOfferCode.getFilters();
        InputFilter[] newFilters = new InputFilter[editFilters.length + 1];
        System.arraycopy(editFilters, 0, newFilters, 0, editFilters.length);
        newFilters[editFilters.length] = new InputFilter.AllCaps();
       // edOfferCode.setFilters(newFilters);
        edOfferCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()>0){
                    btnReedeem.setBackgroundResource(R.drawable.bg_bottom_btn_dialog);
                }else {
                    btnReedeem.setBackgroundResource(R.drawable.bg_bottom_btn_dialog_grey);
                }
                tv_error.setText("");
                tv_error.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable s) {
                for (UnderlineSpan span : s.getSpans(0, s.length(), UnderlineSpan.class)) {
                    s.removeSpan(span);
                }
            }
        });
        edOfferCode.requestFocus();
        View skip= dialog.findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnReedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String redeemCode = edOfferCode.getText().toString();
                if(TextUtils.isEmpty(redeemCode)){
                    ToastHelper.showToast(ResourceHelper.getInstance().getString(R.string.please_enter_redeem_code),Toast.LENGTH_LONG);
                    return;
                }
                Object value = checkCode(redeemCode,context);
                if(value== null){
                    loader.setVisibility(View.VISIBLE);
                    PromoHelper.Companion.getInstance().getSingleRedeemOffer(context, new NetworkResultListener() {
                        @Override
                        public void executeOnError(VolleyError error) {
                            loader.setVisibility(View.GONE);
                        }

                        @Override
                        public void executeOnSuccess(JSONObject response) {
                            ArrayList redeemListDataArrayList = RedeemListData.parseRedeemList(response);
                            loader.setVisibility(View.GONE);
                            MenuDrawerFragment.onHavingPromos(redeemListDataArrayList.get(0),dialog,context,edOfferCode,
                                    tv_error);
                        }
                    });
                    edOfferCode.clearFocus();
                    return;}
                    MenuDrawerFragment.onHavingPromos(value,dialog,context,edOfferCode,
                            tv_error);
                //
//                NetworkManager.getInstance(context).redeemCode(context, edOfferCode.getText().toString().trim(), new NetworkResultListener() {
//                    @Override
//                    public void executeOnSuccess(JSONObject response) {
//
//                        Toast.makeText(context, response.toString(), Toast.LENGTH_SHORT).show();
//
//                    }
//
//                    @Override
//                    public void executeOnError(VolleyError error) {
//                        String response = WebUtility.getMessage(error);
//                        if(!TextUtils.isEmpty(response)){
//                            if(response.contains("message")){
//                                try {
//                                    JSONObject jsonObject = new JSONObject(response);
//                                    String msg = jsonObject.getString("message");
//                                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//
//                    }
//                });
            }
        });
        dialog.show();
    }

    private static void onHavingPromos(Object value,Dialog dialog,Activity context,EditText edOfferCode,
                                       TextView tv_error){
        if(value instanceof Boolean && !(Boolean) value){
            edOfferCode.clearFocus();
            edOfferCode.setText("");
//            edOfferCode.setError(context.getString(R.string.you_entered_wrong_redeem_key));
            tv_error.setVisibility(View.VISIBLE);
            tv_error.setText(context.getString(R.string.you_entered_wrong_redeem_key));
//            Toast.makeText(context, context.getString(R.string.you_entered_wrong_redeem_key), Toast.LENGTH_LONG).show();
            return;
        }else if(value instanceof RedeemListData){
            RedeemListData data = (RedeemListData)value;
            ArrayList<String> skuList = (data).getAttributes().getIapDiscount().getAndroid();
            if(context instanceof UpgradeVpnActivity){
                context.finish();
            }
            else
            {
                context.setResult(100);
               context.onBackPressed();
            }
            dialog.dismiss();
            if(PromoHelper.Companion.getInstance().isDiscountedPromo((RedeemListData) value)){
                PromoHelper.Companion.getInstance().checkIfCreateSubs(context,(RedeemListData) value);
                ToastHelper.showToast(ResourceHelper.getInstance().getString(R.string.processing_promo),Toast.LENGTH_LONG);

                return;
            }
            PromoHelper.Companion.setDiscountedRedeemListData(null);
            context.startActivity(VpnHomeFragment.getDiscountedIntent(VpnHomeFragment.getUpgradeIntent(context),
                    skuList,data.getAttributes().getTitle(),data.getAttributes().getTitle_2(),data.getId()));
        }
    }
}
