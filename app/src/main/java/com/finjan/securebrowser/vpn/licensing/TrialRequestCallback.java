package com.finjan.securebrowser.vpn.licensing;

import com.avira.common.authentication.models.Subscription;

/**
 * @author ovidiu.buleandra
 * @since 23.11.2015
 */
public interface TrialRequestCallback {
    void onTrialRequestSuccessful(String productAcronym, Subscription subscription);

    void onTrialRequestError(int errorCode, String errorMessage);
}
