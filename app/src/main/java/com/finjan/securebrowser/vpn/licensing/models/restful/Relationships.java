package com.finjan.securebrowser.vpn.licensing.models.restful;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovidiu.buleandra
 * @since 05.11.2015
 */
@SuppressWarnings("unused")
public class Relationships implements GSONModel {
    @SerializedName("app") private DataContainer app;
    @SerializedName("user") private DataContainer user;
    @SerializedName("device") private DataContainer device;
    @SerializedName("activation-code") private DataContainer activationCode;

    public Resource getAppData() {
        return app != null ? app.getData() : null;
    }

    public Resource getUserData() {
        return user != null ? user.getData() : null;
    }

    public Resource getDeviceData() { return device != null ? device.getData() : null; }

    public Resource getActivationCodeData() {
        return activationCode != null ? activationCode.getData() : null;
    }
}
