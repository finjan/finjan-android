/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.finjan.securebrowser.R;
//import com.finjan.securebrowser.a_vpn.controller.mixpanel.Tracking;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.ConnectionUtil;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.Util;

import de.blinkt.openvpn.core.VpnStatus;

/**
 * Class to receive system events related to changes in connectivity
 * Created by hui-joo.goh on 10/05/16.
 */
public class WifiConnectionReceiver extends BroadcastReceiver {

    private static final String TAG = WifiConnectionReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        initSmartScan(context);

    }
    public static void initSmartScan(Context context){
        if (Util.isNetworkConnected(context)) {
            showEnableVpnNotifIfRequired(context);

        } else { // no connectivity, clear notif if exists because it's invalidated
            clearNotification(context, NotificationActionReceiver.NOTIF_WIFI_CHANGED_ID);
            clearNotification(context, NotificationActionReceiver.NOTIF_INSECURE_WIFI_ID);
        }
    }

    /**
     * Method that determines whether or not to show enable VPN notification based on multiple criteria:
     * - if smart monitor setting is enabled
     * - if VPN is not connected
     * - if the user has remaining VPN traffic
     * @param context
     */
    private static void showEnableVpnNotifIfRequired(Context context) {

        // do not proceed if smart monitor setting is not enabled
        if (!FinjanVpnPrefs.isSmartMonitorEnabled(context)) { return; }

        // get connected wifi ssid if available
        String ssid = ConnectionUtil.getConnectedWifiSsid(context);

        // if vpn is not running & user has remaining traffic, show notification to enable VPN
        if (!isVpnActive(context)) {

            TrafficController trafficController = TrafficController.getInstance(context);
            long trafficLimit = trafficController.getTrafficLimit();
            long remainingTraffic = trafficLimit - trafficController.getTraffic();
            boolean hasVpnTraffic = (remainingTraffic > 0) || (trafficLimit == 0); // limit 0 = limitless traffic

            if (hasVpnTraffic) {
                if (ssid == null) {
                    showWifiChangedNotification(context, ssid);
//                    showInsecureWifiNotification(context, ssid, Util.humanReadableByteCount(remainingTraffic, false));

                    return; }

                if (!ConnectionUtil.isWifiSecure(context, ssid)) {
                    if (!FinjanVpnPrefs.isWifiWhiteListed(context, ssid)) {
                        showWifiChangedNotification(context, ssid);
                    }
                } else {
                    if (remainingTraffic<0)
                    {
                        showInsecureWifiNotification(context, ssid,"Unlimited");

                    }
                    else
                    showInsecureWifiNotification(context, ssid, Util.humanReadableByteCount(remainingTraffic, false));
                }
            }
        }
    }

    /**
     * @return True if VPN has been enabled (does not mean that VPN is connected), otherwise false.
     */
    private static boolean isVpnActive(Context context) {
        boolean vpnActive = (FinjanVpnPrefs.getVpnConnectionStatus(context) != VpnStatus.ConnectionStatus.LEVEL_NOTCONNECTED);
        Logger.logD(TAG, "isVpnActive " + vpnActive);
        return vpnActive;
    }

    /**
     * The notification that is shown whenever the user connects to a wifi,
     * asking user to connect to VPN in order to increase user engagement.
     *
     * @param context
     * @param ssid The current connected wifi ssid.
     */
    private static void showWifiChangedNotification(Context context, String ssid) {
        Logger.logD(TAG, "showWifiChangedNotification");
//        Tracking.trackNotificationShown(Tracking.VALUE_NEW_WIFI);


        PendingIntent positiveIntent;
        PendingIntent negativeIntent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            positiveIntent = PendingIntent.getBroadcast(context, 0,
                    new Intent(context,NotificationActionReceiverForO.class).setAction(NotificationActionReceiver.ACTION_WIFI_CHANGED_NOTIF_POSITIVE),
                    PendingIntent.FLAG_CANCEL_CURRENT);
        }else
        {
            positiveIntent = PendingIntent.getBroadcast(context, 0,
                    new Intent(NotificationActionReceiver.ACTION_WIFI_CHANGED_NOTIF_POSITIVE),
                    PendingIntent.FLAG_CANCEL_CURRENT);
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            negativeIntent = PendingIntent.getBroadcast(context, 0,
                    new Intent(context,NotificationActionReceiverForO.class).setAction(NotificationActionReceiver.ACTION_WIFI_CHANGED_NOTIF_NEGATIVE).putExtra(NotificationActionReceiver.EXTRA_SSID, ssid),
                    PendingIntent.FLAG_CANCEL_CURRENT);
        }else
        {
            negativeIntent = PendingIntent.getBroadcast(context, 0,
                    new Intent(NotificationActionReceiver.ACTION_WIFI_CHANGED_NOTIF_NEGATIVE)
                            .putExtra(NotificationActionReceiver.EXTRA_SSID, ssid), PendingIntent.FLAG_CANCEL_CURRENT);

        }
        /* positiveIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(NotificationActionReceiver.ACTION_WIFI_CHANGED_NOTIF_POSITIVE),
                PendingIntent.FLAG_CANCEL_CURRENT);

         negativeIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(NotificationActionReceiver.ACTION_WIFI_CHANGED_NOTIF_NEGATIVE)
                        .putExtra(NotificationActionReceiver.EXTRA_SSID, ssid), PendingIntent.FLAG_CANCEL_CURRENT);
*/
        String title = context.getString(R.string.notif_wifi_changed_title);
        String desc = context.getString(R.string.notif_wifi_changed_desc);
        String buttonText = context.getString(R.string.notif_wifi_changed_action);

        RemoteViews customView = new RemoteViews(context.getPackageName(), R.layout.notification_custom);
        customView.setImageViewResource(R.id.notification_image, R.drawable.ic_wifi_notification);
        customView.setTextViewText(R.id.notification_title, title);
        customView.setTextViewText(R.id.notification_desc, desc);
        customView.setTextViewText(R.id.notification_button, buttonText);
        customView.setOnClickPendingIntent(R.id.notification_button, positiveIntent);

        String channelName = "Invincibill VPN";
        String channelId = "com.invincibull.wifireceiver";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);
        }

        Notification notification = new NotificationCompat.Builder(context,channelId)
                .setTicker(title)
                .setSmallIcon(R.drawable.info_icon)
                .setContent(customView)
                .setContentIntent(positiveIntent)
                .setDeleteIntent(negativeIntent)
                .setAutoCancel(true)
                .build();

        notification.bigContentView = customView; // available in >= API 16

        NotificationManagerCompat.from(context).notify(NotificationActionReceiver.NOTIF_WIFI_CHANGED_ID, notification);
    }

    public static void showInsecureWifiNotification(Context context, String ssid, String remainingTraffic) {
        try {

            long mb = Long.parseLong(remainingTraffic) / 1048576;
            remainingTraffic = String.valueOf(mb) + " mb";
        }catch (Exception ex)
        {

        }
        Logger.logD(TAG, "showInsecureWifiNotification");
//        Tracking.trackNotificationShown(Tracking.VALUE_INSECURE_WIFI);
        PendingIntent positiveIntent;
        PendingIntent negativeIntent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            positiveIntent = PendingIntent.getBroadcast(context, 0,
                    new Intent(context,NotificationActionReceiverForO.class).setAction(NotificationActionReceiver.ACTION_INSECURE_NOTIF_POSITIVE),
                    PendingIntent.FLAG_CANCEL_CURRENT);
        }else
        {
            positiveIntent = PendingIntent.getBroadcast(context, 0,
                    new Intent(NotificationActionReceiver.ACTION_INSECURE_NOTIF_POSITIVE),
                    PendingIntent.FLAG_CANCEL_CURRENT);
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            negativeIntent = PendingIntent.getBroadcast(context, 0,
                    new Intent(context,NotificationActionReceiverForO.class).setAction(NotificationActionReceiver.ACTION_INSECURE_NOTIF_NEGATIVE),
                    PendingIntent.FLAG_CANCEL_CURRENT);
        }else
        {
            negativeIntent = PendingIntent.getBroadcast(context, 0,
                    new Intent(NotificationActionReceiver.ACTION_INSECURE_NOTIF_NEGATIVE),
                    PendingIntent.FLAG_CANCEL_CURRENT);
        }

        /* positiveIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(NotificationActionReceiver.ACTION_INSECURE_NOTIF_POSITIVE),
                PendingIntent.FLAG_CANCEL_CURRENT);*/
      /*  PendingIntent positiveIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(context,NotificationActionReceiver.class),
                PendingIntent.FLAG_CANCEL_CURRENT);*/

       /* PendingIntent negativeIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(NotificationActionReceiver.ACTION_INSECURE_NOTIF_NEGATIVE),
                PendingIntent.FLAG_CANCEL_CURRENT);*/

        String desc = TextUtils.isEmpty(remainingTraffic) ?
                context.getString(R.string.notif_insecure_wifi_pro_desc, ssid)
                : String.format(context.getString(R.string.notif_insecure_wifi_free_desc), ssid, remainingTraffic);

        String channelName = "Invincibill VPN";
        String channelId = "com.invincibull.wifireceiver";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);
        }
        // build notification
        Notification notification = new NotificationCompat.Builder(context,channelId)
                .setSmallIcon(R.drawable.info_icon)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.app_icon))
                .setContentTitle(context.getString(R.string.notif_insecure_wifi_title))
                .setContentText(desc)
                .setTicker(context.getString(R.string.notif_insecure_wifi_ticker))
                .setContentIntent(positiveIntent) // for < API 16 devices where there's no action buttons
                .addAction(R.drawable.ic_notification_later, context.getString(R.string.not_now), negativeIntent)
                .addAction(R.drawable.ic_notification_ok, context.getString(R.string.connect_vpn), positiveIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(desc)) // prevents desc to be ellipsized
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat.from(context).notify(NotificationActionReceiver.NOTIF_INSECURE_WIFI_ID, notification);
    }

    private static void clearNotification(Context context, int notifId) {
        Logger.logD(TAG, "clearNotification " + notifId);
        NotificationManagerCompat.from(context).cancel(notifId);
    }
}