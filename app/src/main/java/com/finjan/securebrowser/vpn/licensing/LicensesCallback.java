package com.finjan.securebrowser.vpn.licensing;

import com.finjan.securebrowser.vpn.licensing.models.restful.License;

import java.util.List;

/**
 * @author ovidiu.buleandra
 * @since 20.11.2015
 */
public interface LicensesCallback {
    void onLicenseQuerySuccess(List<License> licenses);

    void onLicenseQueryError(int errorCode, String message);
}
