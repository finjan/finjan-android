//package com.finjan.securebrowser.vpn.auto_connect
//
//import android.app.Activity
//import android.app.AlertDialog
//import android.content.ActivityNotFoundException
//import android.content.Context
//import android.content.ContextWrapper
//import android.content.DialogInterface
//import android.net.VpnService
//import com.finjan.securebrowser.R
//import com.finjan.securebrowser.helpers.logger.Logger
//import com.finjan.securebrowser.vpn.ui.main.LaunchAviraVpnActivity
//import com.finjan.securebrowser.vpn.util.log.FileLogger
//import de.blinkt.openvpn.VpnProfile
//import de.blinkt.openvpn.core.ProfileManager
//
//class VpnConnection{
//
//    private lateinit var mSelectedProfile: VpnProfile
//
//    companion object {
//
//        private object HOLDER {
//            var instance = VpnConnection()
//        }
//        val TAG = VpnConnection.javaClass.simpleName
//        val instance: VpnConnection by lazy { HOLDER.instance }
//    }
//    public fun setPromoList(context: ContextWrapper,key:String, name:String){
//        // we got called to be the starting point, most likely a shortcut
//        val shortcutUUID =key
//        val shortcutName = name
//
//        var profileToConnect: VpnProfile? = ProfileManager.get(context, shortcutUUID)
//        if (shortcutName != null && profileToConnect == null)
//            profileToConnect = ProfileManager.getInstance(context).getProfileByName(shortcutName)
//
//        if (profileToConnect == null) {
//            Logger.logE(TAG, "unable to retrieve profile to connect")
//            // show Log window to display error
//            return
//        }
//
//        mSelectedProfile = profileToConnect
//        launchVPN(context)
//    }
//
//    internal fun showConfigErrorDialog(context: ContextWrapper, vpnok: Int) {
//        val d = AlertDialog.Builder(context)
//        d.setTitle(R.string.config_error_found)
//        d.setMessage(vpnok)
//        d.setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss() }
//        d.show()
//    }
//
//    internal fun launchVPN(context: ContextWrapper) {
//        FileLogger.logToFile(LaunchAviraVpnActivity::class.java, "launchVPN")
//        val vpnok = mSelectedProfile.checkProfile(context)
//        if (vpnok != R.string.no_error_found) {
//            showConfigErrorDialog(context,vpnok)
//            return
//        }
//
//        val intent = VpnService.prepare(context)
//
//        if (intent != null) {
//            // Start the query
//            try {
//                context.startac
//                context.startActivityForResult(intent, START_VPN_PROFILE)
//            } catch (ane: ActivityNotFoundException) {
//                // Shame on you Sony! At least one user reported that
//                // an official Sony Xperia Arc S image triggers this exception
//                Logger.logI(TAG, "vpn not supported")
//            }
//
//        } else {
//            onActivityResult(START_VPN_PROFILE, Activity.RESULT_OK, null)
//        }
//
//    }
//
//
//}