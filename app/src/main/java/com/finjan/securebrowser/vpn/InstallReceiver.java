package com.finjan.securebrowser.vpn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.adjust.sdk.AdjustReferrerReceiver;
import com.avira.common.utils.SharedPreferencesUtilities;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.eventbus.AutoLoginEvent;

import de.greenrobot.event.EventBus;

public class InstallReceiver extends BroadcastReceiver {

    public static final String PREFS_REFERRER_TOKEN_KEY = "prefs_referrer_token";
    public static final String PREFS_REFERRER_SOURCE_KEY = "prefs_referrer_source";
    private static final String TAG = "InstallReceiver";
    private static final String EXTRA_REFERRER = "referrer";
    private static final String SPLIT_EXPRESSION = "-";

    @Override
    public void onReceive(Context context, Intent intent) {

        notifyThirdParties(context, intent);

        if (!intent.hasExtra(EXTRA_REFERRER)) {
            Logger.logE(TAG, "INSTALL_REFERRER broadcast came with no extras");
            return;
        }

        String referrer = intent.getStringExtra(EXTRA_REFERRER);
        if (TextUtils.isEmpty(referrer)) {
            Logger.logE(TAG, "INSTALL_REFERRER broadcast came with NULL extras");
            return;
        }
        Logger.logE(TAG, String.format("received broadcast[%s]", referrer));
        Logger.logE(TAG, "referrer.contains(utm_source) " + referrer.contains("utm_source"));
        Logger.logE(TAG, "getToken(referrer) " + getToken(referrer));
        Logger.logE(TAG, "getSource(referrer) " + getSource(referrer));

        if (referrer.contains("utm_source")) {
            // nothing do here
        } else {
            // get token and deployment source from referrer
            SharedPreferencesUtilities.putString(context, PREFS_REFERRER_TOKEN_KEY, getToken(referrer));
            SharedPreferencesUtilities.putString(context, PREFS_REFERRER_SOURCE_KEY, getSource(referrer));
            EventBus.getDefault().post(new AutoLoginEvent());
        }
    }

    //needed for adjust
    private void notifyThirdParties(Context context, Intent intent) {
        // Adjust
        new AdjustReferrerReceiver().onReceive(context, intent);
        // notify Google Analytics campaign tracking receiver
        // new CampaignTrackingReceiver().onReceive(context, intent);
    }


    private String getToken(String referrer) {
        if (referrer != null && referrer.contains(SPLIT_EXPRESSION)) {
            return referrer.split(SPLIT_EXPRESSION)[0];
        } else {
            return referrer;
        }
    }

    /**
     * @param referrer
     * @return referrer - 127asd4Jtdfu-{3 letter code}
     * Device Card Registered via SMS     - dsr
     * Device Card Registered via Email - dmr
     * Notification Registered SMS - nsr
     * Notification Anonymoys SMS - nsa
     * Device Card Anonymous via SMS - dsa
     */
    private String getSource(String referrer) {
        String deploymentSource = "";
        if (referrer != null && referrer.contains(SPLIT_EXPRESSION)) {
            deploymentSource = referrer.split(SPLIT_EXPRESSION)[1];
        }
        return deploymentSource;
    }
}
