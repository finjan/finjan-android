/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */


package com.finjan.securebrowser.vpn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.finjan.securebrowser.vpn.ui.main.LaunchAviraVpnActivity;

import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.ProfileManager;


public class OnBootReceiver extends BroadcastReceiver {

	// Debug: am broadcast -a android.intent.action.BOOT_COMPLETED
	@Override
	public void onReceive(Context context, Intent intent) {

		final String action = intent.getAction();

		if(Intent.ACTION_BOOT_COMPLETED.equals(action) || Intent.ACTION_MY_PACKAGE_REPLACED.equals(action)) {
			VpnProfile bootProfile = ProfileManager.getLastConnectedProfile(context, true);
			if(bootProfile != null) {
				launchVPN(bootProfile, context);
			}		
		}
	}

	void launchVPN(VpnProfile profile, Context context) {
		Intent startVpnIntent = new Intent(Intent.ACTION_MAIN);
		startVpnIntent.setClass(context, LaunchAviraVpnActivity.class);
		startVpnIntent.putExtra(LaunchAviraVpnActivity.EXTRA_KEY,profile.getUUIDString());
		startVpnIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(startVpnIntent);
	}
}
