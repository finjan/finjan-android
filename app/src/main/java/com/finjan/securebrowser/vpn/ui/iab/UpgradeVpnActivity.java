package com.finjan.securebrowser.vpn.ui.iab;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.NormaWebviewActivity;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.constants.DrawerConstants;
import com.finjan.securebrowser.custom_exceptions.EventsListHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.ToastHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.helpers.vpn_helper.TempPurchaseHelper;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.eventbus.SubscriptionUpdated;
import com.finjan.securebrowser.vpn.licensing.models.billing.Inventory;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.models.billing.SkuDetails;
import com.finjan.securebrowser.vpn.licensing.utils.SubscriptionVPN;
import com.finjan.securebrowser.vpn.ui.authentication.SetupAccountActivity;
import com.finjan.securebrowser.vpn.ui.main.MenuDrawerFragment;
import com.finjan.securebrowser.vpn.util.TrafficController;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

public class UpgradeVpnActivity extends UpgradeActivity {

    public static final int REQUEST_LOGIN = 21315;
    public static final int REQ_CODE_ACCOUNTS = 75;

    public static final String TAG = UpgradeVpnActivity.class.getSimpleName();
    Boolean shouldRestore = true;

    @Bind(R.id.tv_pro_title)
    TextView tvTitle;

    @Bind(R.id.ll_monthly)
    LinearLayout ll_monthly;


    @Bind(R.id.tv_click_here_if_have_promotional_code)
    TextView tv_click_here_if_have_promotional_code;

    @Bind(R.id.tv_seven_day_back_guarantee)
    TextView tv_seven_day_back_guarantee;
    @Bind(R.id.txtgreatprice)
    LottieAnimationView lottiegreatprice;

    @Bind(R.id.tv_seven_day_back_guarantee_2)
    TextView tv_seven_day_back_guarantee_2;

    @Bind(R.id.ll_annually)
    LinearLayout ll_annually;

    @Bind(R.id.tv_restore)
    TextView tv_restore;

    @Bind(R.id.vg_loader)
    ViewGroup vg_loader;

    private String selectedSku;
    private TrafficController trafficController;

    @Override
    public List<String> getSkuList() {
        Logger.logI(TAG, "getSkuList");
        // there's only one product in this page
        return Arrays.asList(LicenseUtil.getSkuAllDevicesMonthly(),
                LicenseUtil.getSkuAllDevicesYearly(),
                LicenseUtil.getSkuThisDevicesMonthly(),
                LicenseUtil.getSkuThisDevicesYearly(),
        //discounted iaps
                LicenseUtil.getSkuAllDevicesYearly25(),
                LicenseUtil.getSkuAllDevicesYearly33(),
                LicenseUtil.getSkuAllDevicesYearly50(),
                LicenseUtil.getSkuAllDevicesYearly75(),
                LicenseUtil.getSkuAllDevicesMonthly25(),
                LicenseUtil.getSkuAllDevicesMonthly33(),
                LicenseUtil.getSkuAllDevicesMonthly50(),
                LicenseUtil.getSkuAllDevicesMonthly75());
    }

    @Override
    public void onPriceAvailable(SkuDetails skuDetails) {
        Logger.logI(TAG, "onPriceAvailable");
        if (skuDetails == null) {
            showNoNetworkDialogIfNoConnectivity();
            return;
        }

    }
    private void hideProgress(){
        if(vg_loader!=null){vg_loader.setVisibility(View.GONE);}
    }
    private void showProgress(){
        if(vg_loader!=null){vg_loader.setVisibility(View.VISIBLE);}
    }

    private boolean isTrialMsgShowed=false;
    private void setDefaults() {

        tvTitle.setTypeface(null, Typeface.BOLD);
//        vg_loader.setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_backButton))
                .setColorFilter(ResourceHelper.getInstance().getColor(R.color.white));
        findViewById(R.id.tv_title).setVisibility(View.GONE);
        ((LinearLayout) findViewById(R.id.ll_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToEvents("softBackPressed","");
                onBackPressed();
            }
        });
        findViewById(R.id.ll_annually).setVisibility(View.GONE);
        findViewById(R.id.ll_monthly).setVisibility(View.GONE);
        RecyclerView recyclerView = findViewById(R.id.rv_purchase_btns);
        upgradeAdapter = new UpgradeAdapter(this,getSortedUpgradeList());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
//        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
//        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(upgradeAdapter);
    }

    private UpgradeAdapter upgradeAdapter;
    private ArrayList<UpgradeBtnModel> upgradeBtnList = new ArrayList<>();

    private ArrayList<UpgradeBtnModel> getDefaultPurchaseList(){
        upgradeBtnList.clear();
        UpgradeBtnModel model1 = new UpgradeBtnModel(LicenseUtil.getSkuAllDevicesYearly());
        BigDecimal b1 =  new BigDecimal(59.99);
        BigDecimal b2 =  new BigDecimal(7.99);
        b1= b1.setScale(2, BigDecimal.ROUND_HALF_UP);
        b2= b2.setScale(2, BigDecimal.ROUND_HALF_UP);
        model1.setPrice(b1,"$ 2","2");
        upgradeBtnList.add(model1);
        UpgradeBtnModel model2 = new UpgradeBtnModel(LicenseUtil.getSkuAllDevicesMonthly());
        model2.setPrice(b2,"$ 2","2");
        upgradeBtnList.add(model2);
        return upgradeBtnList;
    }

    private ArrayList<UpgradeBtnModel> getSortedUpgradeList(){
        showProgress();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideProgress();
            }
        },2000);
        ArrayList<UpgradeBtnModel> list = getUpgradeList();
        UpgradeBtnModel yearlyModel = null;
        for (UpgradeBtnModel model: list){
            if(model.sku.contains("year")){
                yearlyModel= model;
            }
        }
        if (yearlyModel != null) {
            list.remove(yearlyModel);
            list.add(0,yearlyModel);
            yearlyModel.setSelected(true);
        }
        return list;
    }
    private ArrayList<UpgradeBtnModel> getUpgradeList(){
//        ArrayList<UpgradeBtnModel> modelArrayList = new ArrayList<>();
        Intent intent=getIntent();
        if(intent!=null && intent.hasExtra("isDiscountedPurchase")){
            String[] skus = intent.getStringArrayExtra("skus");
            if(skus ==null || skus.length==0 || (skus.length==1 && skus[0].equals("") )){
                upgradeBtnList =getDefaultPurchaseList();
            }else {
                for(String sku : skus){
                    upgradeBtnList.add(new UpgradeBtnModel(sku));
                }
            }
        }else {
           upgradeBtnList =getDefaultPurchaseList();
        }
        selectedSku = upgradeBtnList.get(0).sku;
        return upgradeBtnList;
    }

    private void setPromotionalCodeClick(){
        if(/*AppConfig.Companion.getFeature_reddem_offer()*/PlistHelper.getInstance().isEnable_redeem()){
            tv_click_here_if_have_promotional_code.setVisibility(View.VISIBLE);
        }
        SpannableString ss2 = new SpannableString((getString(R.string.click_here_if_you_have_a_promotional_code)));
        ss2.setSpan(new UnderlineSpan(), 0,ss2.length()-1,0);
        tv_click_here_if_have_promotional_code.setText(ss2);
        tv_click_here_if_have_promotional_code.setTextColor(ResourceHelper.getInstance().getColor(R.color.col_desc_w));
        tv_click_here_if_have_promotional_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuDrawerFragment.showRedeemDialog(UpgradeVpnActivity.this);
            }
        });
    }

    private void setBottomMsg(boolean isTrial){


        SpannableString ss = new SpannableString(getString(isTrial?R.string.cancel_you_subs_new_vpn_screen_msg:
                R.string.cancel_you_subs_new_vpn_screen_msg_1));
        ss.setSpan(new UnderlineSpan(), isTrial?319:233, isTrial?351:265, 0);
//        370
        ss.setSpan(new UnderlineSpan(), isTrial?355:269, isTrial?370:284, 0);
        ss.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoEulaUrl();
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        }, isTrial?319:233, isTrial?351:265, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ss.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoPrivacyUrl();
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        }, isTrial?355:269, isTrial?370:284, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView = (TextView) findViewById(R.id.tv_privaly_and_eula);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    public void gotoEulaUrl() {

        Intent intent = new Intent(this, NormaWebviewActivity.class);
        intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_EULA);
        startActivity(intent);
    }

    public void gotoPrivacyUrl() {

        Intent intent = new Intent(this, NormaWebviewActivity.class);
        intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_PRIVATE_PRIVACY);
        GoogleTrackers.Companion.setSPrivacy();
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(!isPurchaseSuccess){
            HashMap<String,String> map = new HashMap();
            map.put("Promo_ID", currentPromoId.toString());
            LocalyticsTrackers.Companion.setEIAP_Leave(map);
        }
        overridePendingTransition(R.anim.enter_from_left, R.anim.enter_to_right);
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        showTrialMsg();
        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_to_left);
        GoogleTrackers.Companion.setSInAppPurchase();
        LocalyticsTrackers.Companion.setEInAppPurchase();
    }


    @Override
    protected void onInventoryAvailable(Inventory inventory) {
        super.onInventoryAvailable(inventory);
        initSubscriptionSettings();
        hideProgress();
    }


    @Override
    public String getSelectedProductSku() {
        Logger.logI(TAG, "getSelectedProductSku");
        return selectedSku;
    }

    private Integer currentPromoId = 0;
    private boolean isDiscounted= false;
    private void checkIfDiscountedView(){
        Intent intent=getIntent();
        setPromotionalCodeClick();
        if(intent!=null && intent.hasExtra("isDiscountedPurchase")){
            isDiscounted =true;
            currentPromoId = Integer.parseInt(intent.getStringExtra("isDiscountedPurchaseId"));
            String title = intent.getStringExtra("isDiscountedPurchaseTitle");
            String title_2 = intent.getStringExtra("isDiscountedPurchaseTitle_2");
            if(!TextUtils.isEmpty(title_2)){
                tv_seven_day_back_guarantee.setTextSize(getResources().getDimension(R.dimen._5sdp));
                tv_seven_day_back_guarantee.setText(title_2);
                tv_seven_day_back_guarantee.setTypeface(null,Typeface.BOLD);
                tv_seven_day_back_guarantee.setTextColor(ResourceHelper.getInstance().getColor(R.color.discounted_inapp_title));
            }else {
                tv_seven_day_back_guarantee.setVisibility(View.GONE);
            }
            if(!TextUtils.isEmpty(title)){
//                tv_seven_day_back_guarantee_2.setTextSize(getResources().getDimension(R.dimen._5sdp));
//                tv_seven_day_back_guarantee_2.setText(title_2);
//                tv_seven_day_back_guarantee_2.setTypeface(null,Typeface.BOLD);
//                tv_seven_day_back_guarantee_2.setTextColor(ResourceHelper.getInstance().getColor(R.color.discounted_inapp_title));
                tvTitle.setText(title);
                tvTitle.setTextColor(ResourceHelper.getInstance().getColor(R.color.discounted_inapp_title));
                tv_seven_day_back_guarantee_2.setVisibility(View.GONE);

            }else {
                tv_seven_day_back_guarantee_2.setVisibility(View.GONE);
            }
            tv_click_here_if_have_promotional_code.setVisibility(View.GONE);
            setBottomMsg(false);
        }else{
            isDiscounted=false;
            tv_seven_day_back_guarantee_2.setVisibility(View.GONE);
//            setBottomMsg(true);
            setBottomMsg(false);
        }
    }
    @Override
    public void initializeViews() {
        Logger.logI(TAG, "initializeViews");
        setContentView(R.layout.activity_upgrade_vpn_2);
        ButterKnife.bind(this);
        trafficController = TrafficController.getInstance(this.getApplicationContext());


        ll_monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectMonthly();
                ll_monthly.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_upgrade_monthly_btn_pressed));
                ll_annually.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_upgrade_monthly_btn_unpressed));
                onCheckAndBuyClick();
            }
        });
        ll_annually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectYearly();
                ll_annually.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_upgrade_monthly_btn_pressed));
                ll_monthly.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_upgrade_monthly_btn_unpressed));
                onCheckAndBuyClick();
            }
        });
        tv_restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restore();
            }
        });
        initSubscriptionSettings();
    }

    private void onCheckAndBuyClick() {
        addToEvents("onCheckAndBuyClick","");
        processBuyClick();
    }


    private void processBuyClick() {
        addToEvents("processBuyClick","");
        if (trafficController != null && trafficController.isRegistered()) {
            startIABOEFlow();
        } else {
            Toast.makeText(UpgradeVpnActivity.this, getString(R.string.log_in_request), Toast.LENGTH_LONG).show();
            SetupAccountActivity.newLoginInstanceForResult(UpgradeVpnActivity.this, REQUEST_LOGIN, SetupAccountActivity.ACTION_SIGN_UP);
        }
    }

    @Override
    protected String getDeveloperPayload() {
        return super.getDeveloperPayload();
    }

    private void restore(){
        String sku = TempPurchaseHelper.getInstance().getExistingSku(mInventory);
        if (!TextUtils.isEmpty(sku)) {

            Purchase purchase=mInventory.getPurchase(sku);
            purchase.setTempToken();
            if(purchase.getToken().contains(Purchase.tempPurchase)){
                UserPref.getInstance().setTempPurchaseToken(purchase.getToken());
            }else {
                UserPref.getInstance().setTempPurchaseToken(null);
            }
//                sendPurchaseToBackend(purchase);
            TrafficController.getInstance(FinjanVPNApplication.getInstance()).saveLicenseType(TrafficController.LICENSE_PAID);
            TrafficController.getInstance(FinjanVPNApplication.getInstance()).saveTrafficLimit(0);
            EventBus.getDefault().post(new SubscriptionUpdated());
            UpgradeVpnActivity.this.finish();
            ToastHelper.showToast(getString(R.string.we_detect_purchase_on_your_device),Toast.LENGTH_LONG);
        }else {
            ToastHelper.showToast(getString(R.string.no_subscription_found_on_current_device),Toast.LENGTH_LONG);
        }
    }
    private void startIABOEFlow() {
        addToEvents("startIABOEFlow","");
        // setPromoList subscription type
        subscriptionVPN = new SubscriptionVPN(runtime);

//        if (shouldRestore) {
//            //restore
//            String sku = TempPurchaseHelper.getInstance().getExistingSku(mInventory);
//            if (!TextUtils.isEmpty(sku)) {
//
//                Purchase purchase=mInventory.getPurchase(sku);
//                purchase.setTempToken();
//                if(purchase.getToken().contains(Purchase.tempPurchase)){
//                    UserPref.getInstance().setTempPurchaseToken(purchase.getToken());
//                }else {
//                    UserPref.getInstance().setTempPurchaseToken(null);
//                }
////                sendPurchaseToBackend(purchase);
//                TrafficController.getInstance(FinjanVPNApplication.getInstance()).saveLicenseType(TrafficController.LICENSE_PAID);
//                TrafficController.getInstance(FinjanVPNApplication.getInstance()).saveTrafficLimit(0);
//                EventBus.getDefault().post(new SubscriptionUpdated());
//                UpgradeVpnActivity.this.finish();
//                ToastHelper.showToast(getString(R.string.we_detect_purchase_on_your_device),Toast.LENGTH_LONG);
//
//            }else {
//                onBuyClick();
//            }
//        } else {
        onBuyClick();

//        showProgress();
//        NetworkManager.getInstance(this).refreshToken(new NetworkResultListener() {
//            @Override
//            public void executeOnSuccess(JSONObject response) {
//                hideProgress();
//                onBuyClick();
//            }
//
//            @Override
//            public void executeOnError(VolleyError error) {
//                hideProgress();
//                VpnHomeFragment.showLoginScreen();
//            }
//        });
//        }
    }


    @Override
    protected void onStop() {
        addToEvents("onStop","");
        Logger.logE(TAG, "onStop ");
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        overridePendingTransition(R.anim.enter_from_left, R.anim.enter_to_right);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        addToEvents("onActivityResult",""+(data!=null?data.toString():" empty data "));
        if (requestCode == REQUEST_LOGIN && resultCode == RESULT_OK) {
//            UserProfile userProfile = UserProfile.load();
            if (TrafficController.getInstance(this).isRegistered()) {
                startIABOEFlow();//TODO this is a quick fix, it is because the UserProfile object dosen't get formed fast enough and the purchase starts without
            }
        }
    }


//    @OnClick(R.id.ll_monthly)
    protected void selectMonthly() {
        addToEvents("selectMonthly","");
        Logger.logI(TAG, "selectMonthly");
        subscriptionType = SUBSCRIPTION_MONTHLY;
        deviceType = SUBSCRIPTION_MULTIPLE_DEVICES;
        seenOtherOffer = true;
        updateSubscriptionType();
    }

//    @OnClick(R.id.ll_annually)
    protected void selectYearly() {
        addToEvents("selectYearly","");
        Logger.logI(TAG, "selectYearly");
        seenOtherOffer = true;
        subscriptionType = SUBSCRIPTION_YEARLY;
        deviceType = SUBSCRIPTION_MULTIPLE_DEVICES;
        updateSubscriptionType();
    }

    private void addToEvents(String event,String value){
        EventsListHelper.Companion.getInstance().add(UpgradeVpnActivity.class.getSimpleName(),event,value);
    }
    private void initSubscriptionSettings() {
        addToEvents("initSubscriptionSettings","");
        updateSubscriptionType();
        updatePrice();
    }

    private void updateSubscriptionType() {
        addToEvents("updateSubscriptionType","");
        if (deviceType == SUBSCRIPTION_MULTIPLE_DEVICES) {
            if (subscriptionType == SUBSCRIPTION_MONTHLY) {
                runtime = SubscriptionVPN.RUNTIME_MONTHLY;
            } else {
                runtime = SubscriptionVPN.RUNTIME_YEARLY;
            }
        } else {
            if (subscriptionType == SUBSCRIPTION_MONTHLY) {
                runtime = SubscriptionVPN.RUNTIME_MONTHLY;
            } else {
                runtime = SubscriptionVPN.RUNTIME_YEARLY;
            }
        }
    }


    private void updatePrice() {
        hideProgress();
        addToEvents("updatePrice","");
        if (mInventory != null) {
            for (UpgradeBtnModel model: upgradeBtnList){
                SkuDetails details = mInventory.getSkuDetails(model.sku);
                SkuDetails detailsOrignal = null;
                if(model.sku.contains("monthly")){
                    detailsOrignal = mInventory.getSkuDetails(LicenseUtil.getSkuAllDevicesMonthly());
                }else {
                    detailsOrignal = mInventory.getSkuDetails(LicenseUtil.getSkuAllDevicesYearly());
                }
                model.setPrice(details.getPriceValueBigD(),
                        details.getPrice(), details.getPriceValue());
                model.setOrignalPrice(detailsOrignal.getPriceValueBigD(),detailsOrignal.getPrice(),
                        detailsOrignal.getPriceValue());
                if(details.getSku().equalsIgnoreCase(detailsOrignal.getSku())){
                    model.isHaveSamePrice =true;
                }
            }

            if(upgradeAdapter!=null){
                upgradeAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onResume() {
        addToEvents("onResume","");
        NativeHelper.getInstnace().currentAcitvity=this;
        super.onResume();
//        lottiegreatprice.setImageAssetsFolder("images/");
        lottiegreatprice.setAnimation("best_price.json");
new Handler().postDelayed(new Runnable() {
    @Override
    public void run() {
        lottiegreatprice.playAnimation();

    }
},1000);

        if(upgradeAdapter==null){
            setDefaults();
        }
        checkIfDiscountedView();
    }

    private void showTrialMsg(){
        Intent intent=getIntent();
        if(intent!=null && intent.hasExtra("trialmsg")){
            if(!isTrialMsgShowed){
                isTrialMsgShowed=true;
                if(intent.hasExtra("showPopUp") && intent.getBooleanExtra("showPopUp",true)){
                    String title= intent.getStringExtra("trialmsgTitle");
                    FinjanDialogBuilder dialogBuilder= new FinjanDialogBuilder(this).setMessage(intent.getStringExtra("trialmsg"));
                    if(!TextUtils.isEmpty(title)){
                        dialogBuilder.setTitle(title);
                    }
                    if(intent.hasExtra("cancelBtn")){
                        dialogBuilder.setNegativeButton(intent.getStringExtra("cancelBtn"));
                        dialogBuilder.setCustomNegativeBtnColour(PlistHelper.IAPInterestedButtonBg);
                        dialogBuilder.setCustomPositiveBtnColour(PlistHelper.IAPLaterButtonBg);
                        dialogBuilder.setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                            @Override
                            public void onNegativeClick() {
                                UpgradeVpnActivity.this.finish();
                            }
                        });
                    }
                    if(intent.hasExtra("okBtn")){
                        dialogBuilder.setCustompOkBtnColour(PlistHelper.IAPOKButtonBg);
                        dialogBuilder.setPositiveButton(intent.getStringExtra("okBtn"));
                        dialogBuilder.setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                            @Override public void onPositivClick() {}});
                    }
                    dialogBuilder.show();
                }else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            MenuDrawerFragment.showRedeemDialog(UpgradeVpnActivity.this,getIntent().getStringExtra("redeem_code"));
                        }
                    },1000);
                }
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        addToEvents("onRequestPermissionsResult",
                "grantResults "+grantResults+" reqestCode "+requestCode +" permissions "+permissions);
        if (REQ_CODE_ACCOUNTS == requestCode) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                updateGoogleEmail();
                processBuyClick();

            } else {
                // permission denied
            }
        }
    }

    @Override
    protected void backUpdated(boolean success, String msg) {
        addToEvents("backUpdated"," success "+success+" msg "+msg);
        GlobalVariables.getInstnace().isAppUpgraded = true;
        NativeHelper.getInstnace().showToast(this, msg, Toast.LENGTH_SHORT);
        this.finish();
    }

    class UpgradeBtnModel{

        public UpgradeBtnModel(String sku){

            this.sku = LicenseUtil.getActualSku(sku);
            if(sku.contains("annual") || sku.contains("yearly")){
                annual = true;
            }
        }
        public void setPrice(BigDecimal price, @NonNull  String actuallPrice, String value ){
            hideProgress();
            String currency = "";
            actuallPrice = actuallPrice.replace(",", "");
            currency = actuallPrice.replace(value,"");
            currency = currency.trim();
            this.price= price.doubleValue();
            title = currency+(annual?(new DecimalFormat("##.##").format((this.price/12))):price)+"/month";
//            desc = currency+price+(annual?" annually":" monthly");
            desc = currency+price+(annual?"/year":"/month");
        }

        public void setOrignalPrice(BigDecimal price, @NonNull  String actuallPrice, String value){
            String currency = "";
            actuallPrice = actuallPrice.replace(",", "");
            currency = actuallPrice.replace(value,"");
            currency = currency.trim();
            orignalPrice = currency+(annual?(new DecimalFormat("##.##").format((price.doubleValue()/12))):price);
            orignalTotalPrice = currency+price;
        }
        public void setSelected(boolean selected){
            this.selected = selected;
        }


        boolean annual,selected, isHaveSamePrice;
        String sku,title, desc, orignalPrice, orignalTotalPrice;
        Double price;
    }
    class UpgradeAdapter extends RecyclerView.Adapter<UpgradeAdapter.UpgradeBtnHolder>{


        private ArrayList<UpgradeBtnModel> upgradeBtnModelArrayList = new ArrayList<>();
        private Context context;
        public UpgradeAdapter(Context context,ArrayList<UpgradeBtnModel> updUpgradeBtnModelArrayList){
            this.context= context;
            if(updUpgradeBtnModelArrayList == null || updUpgradeBtnModelArrayList.size() ==0){
                this.upgradeBtnModelArrayList.clear();
            }else
            this.upgradeBtnModelArrayList= updUpgradeBtnModelArrayList;
        }
        @Override
        public int getItemCount() {
            return upgradeBtnModelArrayList.size();
        }

        @Override
        public void onBindViewHolder(UpgradeBtnHolder holder, final int position) {
            holder.view.setBackgroundResource(upgradeBtnModelArrayList.get(position).selected?
            R.drawable.bg_upgrade_monthly_btn_pressed:R.drawable.bg_upgrade_monthly_btn_unpressed);
            if (upgradeBtnModelArrayList.get(position).selected)
            {
                holder.tv_purchase_desc.setTextColor(ResourceHelper.getInstance().getColor(R.color.white));
                holder.tv_purchase_title_old.setTextColor(ResourceHelper.getInstance().getColor(R.color.white));
            }else
            {
                holder.tv_purchase_desc.setTextColor(ResourceHelper.getInstance().getColor(R.color.col_content_upgrade));
                holder.tv_purchase_title_old.setTextColor(ResourceHelper.getInstance().getColor(R.color.col_content_upgrade));
            }
            if(!TextUtils.isEmpty(upgradeBtnModelArrayList.get(position).title)){
//                holder.tv_billed.setVisibility(View.VISIBLE);
                holder.pb_upgrade.setVisibility(View.GONE);
            }
//            holder.tv_purchase_title.setText(upgradeBtnModelArrayList.get(position).title);
            holder.tv_purchase_title_old.setText(upgradeBtnModelArrayList.get(position).orignalPrice);
            holder.tv_purchase_title_old.setPaintFlags(
                    holder.tv_purchase_title_old.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//            holder.tv_purchase_desc_old.setText(upgradeBtnModelArrayList.get(position).orignalTotalPrice);
//            holder.tv_purchase_desc_old.setPaintFlags(
//                    holder.tv_purchase_desc_old.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tv_purchase_desc.setText(upgradeBtnModelArrayList.get(position).desc);
            holder.tv_purchase_desc.setTypeface(null,Typeface.BOLD);
//            holder.tv_purchase_title.setTypeface(null,Typeface.BOLD);
            if(upgradeBtnModelArrayList.get(position).isHaveSamePrice){
                holder.tv_purchase_title_old.setVisibility(View.GONE);
             //   holder.tv_purchase_desc_old.setVisibility(View.GONE);
            }
            if (PlistHelper.getInstance().isShowActual())
            {
                holder.tv_purchase_title_old.setVisibility(View.GONE);
             //   holder.tv_purchase_desc_old.setVisibility(View.GONE);
            }
            /*if (PlistHelper.getInstance().isSingleLine())
            {
                holder.tv_purchase_title.setText(upgradeBtnModelArrayList.get(position).desc);
                holder.tv_purchase_title_old.setText(upgradeBtnModelArrayList.get(position).orignalTotalPrice);
                holder.ll_Second_Line.setVisibility(View.GONE);
            }*/
//            if (PlistHelper.getInstance().isSingleLine())
//            {
//                holder.tv_purchase_title.setText(upgradeBtnModelArrayList.get(position).desc);
//                holder.tv_purchase_title.setVisibility(View.GONE);
                if (upgradeBtnModelArrayList.get(position).annual)
                holder.tv_purchase_title_old.setText(upgradeBtnModelArrayList.get(position).orignalTotalPrice+"/year");
                else
                    holder.tv_purchase_title_old.setText(upgradeBtnModelArrayList.get(position).orignalTotalPrice+"/month");
               // holder.ll_Second_Line.setVisibility(View.VISIBLE);
                holder.tv_purchase_desc.setText(upgradeBtnModelArrayList.get(position).desc);
//                holder.tv_purchase_desc_old.setVisibility(View.GONE);
//                holder.tv_billed.setVisibility(View.GONE);
//            }
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        for (UpgradeBtnModel data: upgradeBtnModelArrayList){
                            data.setSelected(false);
                        }
                        upgradeBtnModelArrayList.get(position).setSelected(true);
                        notifyDataSetChanged();
                        UpgradeVpnActivity.this.selectedSku = upgradeBtnModelArrayList.get(position).sku;
                        onCheckAndBuyClick();
                    }
            });
        }

        @Override
        public UpgradeBtnHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.item_purchase_buttons, parent, false);
            return new UpgradeBtnHolder(itemView);
        }

        class UpgradeBtnHolder extends RecyclerView.ViewHolder{

            TextView /*tv_purchase_title,*/tv_purchase_title_old, tv_purchase_desc;
                    /*tv_purchase_desc_old*//*,tv_billed*/
            ProgressBar pb_upgrade;
           // LinearLayout ll_Second_Line;
            View view;
            public UpgradeBtnHolder(View view){
                super(view);
                this.view = view.findViewById(R.id.ll_annually);
//                tv_purchase_title = view.findViewById(R.id.tv_purchase_title);
//                ll_Second_Line= view.findViewById(R.id.ll_Second_Line);
//                tv_billed= view.findViewById(R.id.tv_billed);
                pb_upgrade= view.findViewById(R.id.pb_upgrade);
                tv_purchase_title_old = view.findViewById(R.id.tv_purchase_title_old);
                tv_purchase_desc = view.findViewById(R.id.tv_purchase_desc);
//                tv_purchase_desc_old = view.findViewById(R.id.tv_purchase_desc_old);
                if(!UpgradeVpnActivity.this.isDiscounted){
                    tv_purchase_title_old.setVisibility(View.GONE);
                   // tv_purchase_desc_old.setVisibility(View.GONE);
                }
            }
        }
    }
}

