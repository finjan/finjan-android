package com.finjan.securebrowser.vpn.controller.database;

import android.database.sqlite.SQLiteDatabase;

import com.avira.common.database.MobileSecurityDatabaseHelper;

/**
 * Created by illia.klimov on 1/7/2016.
 */
public class VpnDBInitialization implements MobileSecurityDatabaseHelper.CustomInitialization {

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO should be initialization
//        ALPinAndPasswordManager.initialize(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

    }
}
