/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */
package com.finjan.securebrowser.vpn.util;

import android.content.Context;
import android.util.Log;

import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.VpnWorkflow;
import com.finjan.securebrowser.vpn.service.AviraOpenVpnService;

import de.blinkt.openvpn.core.VpnStatus;

/**
 * Class containing utility methods for VPN connection.
 *
 * Created by hui-joo.goh on 20/07/16.
 */
public class VpnUtil {

    private static final String TAG = VpnUtil.class.getSimpleName();

    /**
     * @return True if VPN has been enabled (does not mean that VPN is connected), otherwise false.
     */
    public static boolean isVpnActive(Context context) {
        if(context==null){
            return false;
        }
        boolean vpnActive = (FinjanVpnPrefs.getVpnConnectionStatus(context) != VpnStatus.ConnectionStatus.LEVEL_NOTCONNECTED);
        Logger.logD(TAG, "isVpnActive " + vpnActive);
        return vpnActive;
    }

    /**
     * Method to start the VPN connection
     *
     * @param serverId id of a vpn server to connect
     * @return true if start was successful, false otherwise
     */
    public static boolean startVpnConnection(Context context, long serverId, boolean autoConnection) {
        Logger.logD(TAG, "startVpnConnection serverId " + serverId);

        return new VpnWorkflow().run(context, serverId,autoConnection);
    }

    /**
     * Method to stop the VPN connection
     *
     * @param context
     * @param vpnService Avira OpenVpnService
     */
    public static void stopVpnConnection(Context context, AviraOpenVpnService vpnService) {
        Logger.logD(TAG, "stopVpnConnection");

        if (vpnService != null && vpnService.getManagement() != null) {
            LocalyticsTrackers.Companion.setEVPNDisconnect();
            vpnService.getManagement().stopVPN(false);
        }
        TrafficController.getInstance(context).stopTimer();
    }
}
