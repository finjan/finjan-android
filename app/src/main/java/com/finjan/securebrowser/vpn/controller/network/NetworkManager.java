/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.controller.network;

import android.content.Context;

import androidx.annotation.NonNull;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;

import com.android.volley.ClientError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.avira.common.authentication.Authentication;
import com.avira.common.authentication.RefreshTokenLintener;
import com.avira.common.authentication.models.RefreshTokenResponse;
import com.avira.common.backend.Backend;
import com.avira.common.backend.WebUtility;
import com.avira.common.id.HardwareId;
import com.avira.common.utils.HashUtility;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.custom_exceptions.CreateDeviceApiException;
import com.finjan.securebrowser.custom_exceptions.CreateSubsApiException;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.custom_exceptions.CustomExceptionConfig;
import com.finjan.securebrowser.custom_exceptions.GetDevicesListApiException;
import com.finjan.securebrowser.custom_exceptions.LaunchAppApiException;
import com.finjan.securebrowser.custom_exceptions.LicenceFetchApiException;
import com.finjan.securebrowser.custom_exceptions.NeverBounceApiException;
import com.finjan.securebrowser.custom_exceptions.RegionFectchApiException;
import com.finjan.securebrowser.custom_exceptions.RenewSubsApiException;
import com.finjan.securebrowser.custom_exceptions.SubscriptionFetchApiException;
import com.finjan.securebrowser.custom_exceptions.TokenRefreshApiException;
import com.finjan.securebrowser.custom_exceptions.TrafficConsumptionApiException;
import com.finjan.securebrowser.custom_exceptions.UpdateDeviceApiException;
import com.finjan.securebrowser.custom_exceptions.UpdateSubcApiException;
import com.finjan.securebrowser.custom_exceptions.UserFetchApiException;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.util.FinjanUrls;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.JsonCreater;
import com.finjan.securebrowser.vpn.controller.network.eventManeger.EventManeger;
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.helpers.logger.Logger;

import org.greenrobot.greendao.annotation.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class NetworkManager {
    public static final String TAG = NetworkManager.class.getSimpleName();

    private static final String AUTHORIZATION = "Authorization";
    private static NetworkManager mInstance;
    private final String mNeverBounceUrl;
    private final String mRegionsUrl;
    private final String mTrafficUrl;
    private final String mLicenceUrlULM;
    private final String mLicenceUrlAvia;
    private final String mSubs;
    private final String mDevices;
    private final String mPromos;
    private final String mUser;
    private RequestQueue mRequestQueue;
    private final String mredeemurl;

    private final String myIPLocationURL;
    private final String socialUserCheckUrl;
    private final String socialUserAccessTokenUrl;
    private final String socialUserRegisterUrl;

    private Handler handler;

    private NetworkManager(@NonNull Context context) {
        String mBaseServerUrl = context.getString(R.string.vpn_backend_configuration);
       // String mBaseServerUrlF = context.getString(R.string.vpn_backend_configuration_finjan);
        handler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                ResultData data = (ResultData) msg.obj;
                if (msg.what == 1) {
                    data.listener.executeOnSuccess(data.result);
                }else{
                    data.listener.executeOnError(data.error);
                }
                super.handleMessage(msg);
            }
        };
        String mBaseServerUrlF;
       if (AppConfig.Companion.isStagingUrl())
       {
           mBaseServerUrlF = Backend.stagingurl2;
       }
       else
       {
          mBaseServerUrlF = Backend.liveUrl;
       }

        mRegionsUrl = mBaseServerUrl + context.getString(R.string.vpn_regions_url);
        mTrafficUrl = mBaseServerUrl + context.getString(R.string.vpn_traffic_url);
        mLicenceUrlAvia =  mBaseServerUrl + context.getString(R.string.vpn_license_url);

        if(AppConfig.Companion.getUseFinjanLicenseApii()){
            mLicenceUrlULM = mBaseServerUrlF + context.getString(R.string.vpn_license_url_2);
        }else {
            mLicenceUrlULM = mBaseServerUrl + context.getString(R.string.vpn_license_url);
        }
        mSubs = mBaseServerUrlF + context.getString(R.string.vpn_subs_url) ;
//        mredeemurl =  PlistHelper.getInstance().getLocation_change_testing()?mBaseServerUrlF + context.getString(R.string.vpn_promos_url)+"?status=test":mBaseServerUrlF + context.getString(R.string.vpn_promos_url);
        mredeemurl =  PlistHelper.getInstance().isTestmode()?mBaseServerUrlF + context.getString(R.string.vpn_promos_url)+"?status=test":mBaseServerUrlF + context.getString(R.string.vpn_promos_url);

        mDevices = mBaseServerUrlF + context.getString(R.string.vpn_devices);
        mPromos = mBaseServerUrlF + context.getString(R.string.vpn_promos);
        mUser = mBaseServerUrlF + context.getString(R.string.vpn_user);
        myIPLocationURL = context.getString(R.string.ip_locator);
        socialUserCheckUrl = context.getString(R.string.social_user_check_url);
        socialUserAccessTokenUrl = context.getString(R.string.access_token_social_user_url);
        socialUserRegisterUrl = context.getString(R.string.register_social_user_url);

        mNeverBounceUrl = context.getString(R.string.never_bounce_url);
        mRequestQueue = getRequestQueue(context);

    }

    public FailedRequest failedRequest;
    public class FailedRequest {
        private NetworkResultListener networkResultListener;
        private String url;
        private String correspondingClass;
        private JSONObject jsonObject;
        private boolean isJsonRequest = false;
        public FailedRequest(NetworkResultListener networkResultListener,
                             String url, String correspondingClass){
            this.networkResultListener = networkResultListener;
            this.url = url;
            this.correspondingClass = correspondingClass;
        }
        public FailedRequest(NetworkResultListener networkResultListener,
                             String url, String correspondingClass, JSONObject jsonObject){
            this.networkResultListener = networkResultListener;
            this.url = url;
            this.correspondingClass = correspondingClass;
            this.jsonObject= jsonObject;
            this.isJsonRequest = true;
        }
        public void hitReqest(Context context){
            if(isJsonRequest){
                fetchJsonPost(context,networkResultListener,url,jsonObject,correspondingClass);
            }else {
                fetchJson(context,networkResultListener,url,correspondingClass,false,"");
            }
        }

    }

    public static synchronized NetworkManager getInstance(@NonNull Context context) {
        if (mInstance == null) {
            mInstance = new NetworkManager(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue(@NonNull Context context) {
        if (mRequestQueue == null) {
            Authentication.initializeSSLContext(context);
            HurlStack stack = new HurlStack();
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    public void fetchRegionsList(@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener) {

        new Thread(new Runnable() {
            @Override
            public void run() {
//                if (isFachedFromCacheData(AppConstants.REGIONS,networkResultListener))
//                {
//                    return;
//                }
//                if (isHitTheApiBeforeFewTime(AppConstants.REGIONS,networkResultListener))
//                {
//                    return;
//                }
                UserPref.getInstance().setHitTime(AppConstants.REGIONS);
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fetchJson(context, networkResultListener, String.format(mRegionsUrl, Locale.getDefault().toString())+"&device_id=0211-"+ HashUtility.sha1(deviceId)/*+"&version="+appVersion*/,
                        RegionFectchApiException.class.getSimpleName(),true,AppConstants.REGIONS);
            }
        }).start();

    }

    public void fetchConfigList(@NonNull Context context, @NonNull final NetworkResultListener resultListener) {
        fetchJson(context, resultListener, FinjanUrls.URL_LAUNCH, LaunchAppApiException.class.getSimpleName(),false,"");
    }

    public void fetchFile(@NonNull Context context, @NonNull String url, @NonNull final NetworkResultListener resultListener) {
        fetchJson(context, resultListener, url, LaunchAppApiException.class.getSimpleName(),false,"");
    }

    public void redeemCode(@NonNull Context context, @NonNull String redeemcode, @NonNull final NetworkResultListener resultListener) {
        String deviceId = HardwareId.get(context);
        String appVersion= "";
        try {
            appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        String url = mredeemurl+"/"+redeemcode;
        if(mredeemurl.contains("?status=test")){
            url= url.replace("?status=test","");
        }
        fetchJson(context, resultListener, url+"?device_id="+deviceId+"&appVer="+appVersion, LaunchAppApiException.class.getSimpleName(),false,"");
    }
    public void allRedeemCode(@NonNull Context context, @NonNull final NetworkResultListener resultListener) {
        fetchJson(context, resultListener, mredeemurl, LaunchAppApiException.class.getSimpleName(),false,"");
    }
    public void getSinglePromo(@NonNull Context context,@NonNull String promoId, @NonNull final NetworkResultListener resultListener) {

        String url = mredeemurl+"/"+promoId+"?verify=false";


        if(mredeemurl.contains("?status=test")){
            url= url.replace("?status=test","");
        }
        fetchJson(context, resultListener, url, LaunchAppApiException.class.getSimpleName(),false,"");
    }
    public void getCategory(@NonNull Context context, @NonNull final NetworkResultListener resultListener,String authTocken) {

        String url = "http://vitalulm.staging.wpengine.com/wp-json/api/v2/watch-contents";



        fetchJson(context, resultListener, url, LaunchAppApiException.class.getSimpleName(),false,"",authTocken);
    }
    public void homeRedeem(@NonNull final Context context, @NonNull final NetworkResultListener resultListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isFachedFromCacheData(AppConstants.ALLPROMOLIST,resultListener))
                {
                    return;
                }
                if (isHitTheApiBeforeFewTime(AppConstants.ALLPROMOLIST,resultListener))
                {
                    return;
                }
                UserPref.getInstance().setHitTime(AppConstants.ALLPROMOLIST);
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(mredeemurl.contains("?status=test"))
                    fetchJson(context, resultListener, mredeemurl/*+"&device_id="+deviceId*/+"&appVer="+appVersion, LaunchAppApiException.class.getSimpleName(),true,AppConstants.ALLPROMOLIST);
                else
                    fetchJson(context, resultListener, mredeemurl/*+"?device_id="+deviceId*/+"?appVer="+appVersion, LaunchAppApiException.class.getSimpleName(),true,AppConstants.ALLPROMOLIST);
            }
        }).start();


    }


    public void fetchTrafficConsumption(@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isFachedFromCacheTraffic(AppConstants.TRAFFIC,networkResultListener))
                {
                    return;
                }
                if (isHitTheApiBeforeFewTime(AppConstants.TRAFFIC,networkResultListener))
                {
                    return;
                }
                UserPref.getInstance().setHitTime(AppConstants.TRAFFIC);
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.logD(TAG, "fetchTrafficConsumption deviceId " + deviceId);

                fetchJson(context, networkResultListener, String.format(mTrafficUrl, HashUtility.sha1(deviceId))/*+"&appVer="+appVersion*/, TrafficConsumptionApiException.class.getSimpleName(),true,AppConstants.TRAFFIC);
            }
        }).start();

    }

    public void fetchLicence(final boolean isToServerHit,@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!isToServerHit && isFachedFromCacheData(AppConstants.LICENCE,networkResultListener))
                {
                    return;
                }
       /* if (isHitTheApiBeforeFewTime(AppConstants.LICENCE))
        {
            return;
        }
        UserPref.getInstance().setHitTime(AppConstants.LICENCE);*/
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.logD(TAG, "fetchLicence deviceId " + deviceId);
                if(AppConfig.Companion.getUseFinjanLicenseApii()){
                    fetchJson(context, networkResultListener, String.format(mLicenceUrlULM, deviceId)+"&appVer="+appVersion, LicenceFetchApiException.class.getSimpleName(),true,AppConstants.LICENCE);
                }else{
                    fetchJson(context, networkResultListener, String.format(mLicenceUrlULM, HashUtility.sha1(deviceId))+"&version="+appVersion, LicenceFetchApiException.class.getSimpleName(),true,AppConstants.LICENCE);
                }

                fetchJson(context, new NetworkResultListener() {
                    @Override
                    public void executeOnSuccess(JSONObject response) {
                        Logger.logE("NetworkManager", "avira license api success");
                    }

                    @Override
                    public void executeOnError(VolleyError error) {
                        Logger.logE("NetworkManager", "avira license api failed "+ error.getMessage());
                    }
                }, String.format(mLicenceUrlAvia, HashUtility.sha1(deviceId))+"&version="+appVersion, LicenceFetchApiException.class.getSimpleName(),false,"");
            }
        }).start();

    }
    public void fetchAviraLicence(@NonNull Context context, @NonNull final NetworkResultListener networkResultListener) {
        /* if (isHitTheApiBeforeFewTime(AppConstants.LICENCE))
        {
            return;
        }
        UserPref.getInstance().setHitTime(AppConstants.LICENCE);*/
        String deviceId = HardwareId.get(context);
        String appVersion= "";
        try {
            appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        Logger.logD(TAG, "fetchLicence deviceId " + deviceId);
        fetchJson(context, new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                ResultData data = new ResultData(networkResultListener,response,null);
                Message message = handler.obtainMessage(1,data);
                message.sendToTarget();
//                networkResultListener.executeOnSuccess(response);
                Logger.logE("NetworkManager", "avira license api success");
            }

            @Override
            public void executeOnError(VolleyError error) {
                ResultData data = new ResultData(networkResultListener,null,error);
                Message message = handler.obtainMessage(0,data);
                message.sendToTarget();
//                networkResultListener.executeOnError(error);
                Logger.logE("NetworkManager", "avira license api failed "+ error.getMessage());
            }
        }, String.format(mLicenceUrlAvia, HashUtility.sha1(deviceId)) +"&version="+appVersion, LicenceFetchApiException.class.getSimpleName(),false,"");
    }

    private static SSLSocketFactory createSslSocketFactory() {
        TrustManager[] byPassTrustManagers = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }
        }};

        SSLContext sslContext = null;
        SSLSocketFactory sslSocketFactory = null;
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, byPassTrustManagers, new SecureRandom());
            sslSocketFactory = sslContext.getSocketFactory();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Logger.logE(TAG, ""+e);
        } catch (KeyManagementException e) {
            Logger.logE(TAG, ""+e);
        }

        return sslSocketFactory;
    }


    public void fetchSubs(final boolean isToServerHit,@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!isToServerHit && isFachedFromCacheData(AppConstants.ALLSUBSCRIPTION,networkResultListener))
                {
                    return;
                }
       /* if (isHitTheApiBeforeFewTime(AppConstants.ALLSUBSCRIPTION))
        {
            return;
        }
        UserPref.getInstance().setHitTime(AppConstants.ALLSUBSCRIPTION);*/
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.logD(TAG, "fetchSubscription deviceId " + deviceId);
                fetchJson(context, networkResultListener, mSubs + "?status=running"+"&device_id="+deviceId+"&appVer="+appVersion, SubscriptionFetchApiException.class.getSimpleName(),true,AppConstants.ALLSUBSCRIPTION);
            }
        }).start();

    }

    private boolean isFachedFromCacheData(String cacheKey,NetworkResultListener networkResultListener)
    {
        /*long lastTimeHit = UserPref.getInstance().getChecheAvilable(cacheKey);
        if (System.currentTimeMillis() - lastTimeHit < PlistHelper.getInstance().getTimeIntervalToUpdateCache() * AppConstants.interValHit)
        {
            *//*EventManeger.getInstance().initialisingCache(0);
            com.finjan.securebrowser.vpn.controller.network.eventManeger.Response response = EventManeger.getInstance().getCacheValue(cacheKey);
            if (response!=null)
            {
                try {
                    JSONObject responsJson = new JSONObject(response.getResponseText().toString());
                    networkResultListener.executeOnSuccess(responsJson);
                    return true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }*//*
            return returnDataFromCache(cacheKey,networkResultListener);


        }*/
        return false;
    }
    private boolean returnDataFromCache(String cacheKey,NetworkResultListener networkResultListener ){
        EventManeger.getInstance().initialisingCache(0);
        com.finjan.securebrowser.vpn.controller.network.eventManeger.Response response = EventManeger.getInstance().getCacheValue(cacheKey);
        if (response!=null)
        {
            try {
                JSONObject responsJson = new JSONObject(response.getResponseText().toString());

                ResultData data = new ResultData(networkResultListener,responsJson,null);
                Message message = handler.obtainMessage(1,data);
                message.sendToTarget();
//                networkResultListener.executeOnSuccess(responsJson);
                return true;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
    private boolean isHitTheApiBeforeFewTime(String cacheKey,NetworkResultListener networkResultListener)
    {
        /*long lastHit= UserPref.getInstance().getHitTime(cacheKey);
        if (System.currentTimeMillis() - lastHit < PlistHelper.getInstance().getTimeIntervalToHitApi() * AppConstants.interValHitMINT)
        {

             if (!returnDataFromCache(cacheKey,networkResultListener)){
                 ResultData data = new ResultData(networkResultListener,null,new VolleyError("API Hit blocked as previous hit was too close"));
                 Message message = handler.obtainMessage(0,data);
                 message.sendToTarget();
//                 networkResultListener.executeOnError(new VolleyError("API Hit blocked as previous hit was too close"));
             }
            return true;
        }*/
        return false;
    }
    private boolean isFachedFromCacheTraffic(String cacheKey,NetworkResultListener networkResultListener)
    {
        long lastTimeHit = UserPref.getInstance().getChecheAvilable(cacheKey);
        if (System.currentTimeMillis() - lastTimeHit < PlistHelper.getInstance().getTimeIntervalToUpdateCacheoftraffic() * AppConstants.interValHit)
        {
            EventManeger.getInstance().initialisingCache(0);
            com.finjan.securebrowser.vpn.controller.network.eventManeger.Response response = EventManeger.getInstance().getCacheValue(cacheKey);
            if (response!=null)
            {
                try {
                    JSONObject responsJson = new JSONObject(response.getResponseText().toString());
                    ResultData data = new ResultData(networkResultListener,responsJson,null);
                    Message message = handler.obtainMessage(1,data);
                    message.sendToTarget();
//                    networkResultListener.executeOnSuccess(responsJson);
                    return true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
        return false;
    }
    public void fetchCancelledSubs(@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener,final boolean forcedFetch) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!forcedFetch && isFachedFromCacheData(AppConstants.ALLCANCLEDPROMOSUBSCRIPTIONLIST,networkResultListener))
                {
                    return;
                }
                if (isHitTheApiBeforeFewTime(AppConstants.ALLCANCLEDPROMOSUBSCRIPTIONLIST,networkResultListener))
                {
                    return;
                }
                UserPref.getInstance().setHitTime(AppConstants.ALLCANCLEDPROMOSUBSCRIPTIONLIST);
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.logD(TAG, "fetchSubscription deviceId " + deviceId);
                fetchJson(context, networkResultListener, mSubs + "?status=cancelled&promos=true"+"&device_id="+deviceId+"&appVer="+appVersion, SubscriptionFetchApiException.class.getSimpleName(),true,AppConstants.ALLCANCLEDPROMOSUBSCRIPTIONLIST);
            }
        }).start();

    }
    public void fetchAllPromoSubs(@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.logD(TAG, "fetchSubscription deviceId " + deviceId);
                fetchJson(context, networkResultListener, mSubs + "?status=running&promos=true"+"&device_id="+deviceId+"&appVer="+appVersion, SubscriptionFetchApiException.class.getSimpleName(),false,"");
            }
        }).start();

    }

    public void listDevices(@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fetchJson(context, networkResultListener, mDevices+"?device_id="+deviceId+"&appVer="+appVersion, GetDevicesListApiException.class.getSimpleName(),false,"");
            }
        }).start();

    }

    public void createDevice(@NonNull Context context, @NotNull JSONObject jsonObject, @NonNull NetworkResultListener networkResultListener) {

        if (jsonObject != null && jsonObject.has("data")) {
            fetchJsonPost(context, networkResultListener, mDevices, jsonObject, CreateDeviceApiException.class.getSimpleName());
        }
    }

    public void createSubs(@NonNull Context context, @NotNull JSONObject jsonObject, @NonNull NetworkResultListener networkResultListener) {
        if (jsonObject != null && jsonObject.has("data")) {
            Logger.logE("Purchase Testing", "" + jsonObject.toString());
            fetchJsonPost(context, networkResultListener, mSubs, jsonObject, CreateSubsApiException.class.getSimpleName());
        }
    }
    public void redeemCoupen(@NonNull Context context, @NotNull String redeemCode,@NonNull NetworkResultListener networkResultListener) {
        String url = mPromos+"/"+redeemCode;
        fetchJson(context, networkResultListener, url, GetDevicesListApiException.class.getSimpleName(),false,"");
    }

    public void createRenew(@NonNull Context context, @NotNull JSONObject jsonObject, @NonNull NetworkResultListener networkResultListener) {
        if (jsonObject != null && jsonObject.has("data")) {
            Logger.logE("Purchase Testing", "" + jsonObject.toString());
            fetchJsonPost(context, networkResultListener, mSubs, jsonObject, RenewSubsApiException.class.getSimpleName());
        }
    }

    //    public void createSubsTemp(@NonNull Context context, @NotNull JSONObject jsonObject, @NonNull NetworkResultListener networkResultListener){
//        fetchJsonPost(context, networkResultListener, mSubs, jsonObject, CreateSubsApiException.class.getSimpleName());
//    }
    public void updateSubs(@NonNull Context context, @NotNull JSONObject jsonObject, String subsId, @NonNull NetworkResultListener networkResultListener) {
        if (jsonObject != null && jsonObject.has("data")) {
            fetchJsonPost(context, networkResultListener, mSubs + "/" + subsId, jsonObject, UpdateSubcApiException.class.getSimpleName());
        }
    }

    public void updateDevice(@NonNull Context context, @NotNull JSONObject jsonObject, @NonNull NetworkResultListener networkResultListener) {
        if (jsonObject != null && jsonObject.has("data")) {
            String deviceId = HardwareId.get(context);
            Logger.logD(TAG, "updateDevice deviceId " + deviceId);
            String url = mDevices + "/" + deviceId;
            fetchJsonPost(context, networkResultListener, url, jsonObject, UpdateDeviceApiException.class.getSimpleName());
        }
    }

    public void fetchUser(@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isFachedFromCacheData(AppConstants.USER,networkResultListener))
                {
                    return;
                }
                if (isHitTheApiBeforeFewTime(AppConstants.USER,networkResultListener))
                {
                    return;
                }
                UserPref.getInstance().setHitTime(AppConstants.USER);
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fetchJson(context, networkResultListener, mUser+"?device_id="+deviceId+"&appVer="+appVersion, UserFetchApiException.class.getSimpleName(),true, AppConstants.USER);
            }
        }).start();

    }

    public void fetchLocation(@NonNull final Context context, @NonNull final String ipAddress, @NonNull final NetworkResultListener networkResultListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fetchJson(context, networkResultListener, String.format(myIPLocationURL, ipAddress), "ip_locator",false,"");
            }
        }).start();

    }

    public void checkSocialuser(@NonNull final Context context, @NonNull final String socialType, @NonNull final String userId, @NonNull final NetworkResultListener networkResultListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String deviceId = HardwareId.get(context);
                String appVersion= "";
                try {
                    appVersion =context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fetchJson(context, networkResultListener, String.format(socialUserCheckUrl, socialType, userId,System.currentTimeMillis())+"&device_id="+deviceId+"&appVer="+appVersion, "checkSocialuser",false,"");
            }
        }).start();

    }

    public void checkSocialuserAccessToken(@NonNull Context context, @NotNull JSONObject jsonObject, @NonNull NetworkResultListener networkResultListener) {
//        if (jsonObject != null && jsonObject.has("data")) {
            fetchJsonPost(context, networkResultListener, socialUserAccessTokenUrl, jsonObject, "checkSocialUserAccesToken");
//        }
    }

    public void registerSocialuserAccessToken(@NonNull Context context, @NotNull JSONObject jsonObject, @NonNull NetworkResultListener networkResultListener) {
//        if (jsonObject != null && jsonObject.has("data")) {
            fetchJsonPost(context, networkResultListener, socialUserRegisterUrl, jsonObject, "registerSocialuserAccessToken");
//        }
    }

    public void refreshToken(final NetworkResultListener networkResultListener){
        Authentication.refereshToken(FinjanVPNApplication.getInstance(),
                FinjanVpnPrefs.getRefershToken(FinjanVPNApplication.getInstance()),
                new RefreshTokenLintener() {
                    @Override
                    public void onRefershSuccess(RefreshTokenResponse refreshTokenResponse) {

                        FinjanVpnPrefs.setAuthToken(FinjanVPNApplication.getInstance().getApplicationContext(),
                                refreshTokenResponse.getAccess_token());
                        FinjanVpnPrefs.setRefereshToken(FinjanVPNApplication.getInstance().getApplicationContext(),
                                refreshTokenResponse.getRefresh_token());
                        int expiresIn = refreshTokenResponse.getExpires_in();
                        UserPref.getInstance().setTokenExpiresIn(expiresIn);

                        ResultData data = new ResultData(networkResultListener,null,null);
                        Message message = handler.obtainMessage(1,data);
                        message.sendToTarget();
//                        networkResultListener.executeOnSuccess(null);
                    }

                    @Override
                    public void onErrorResponse(int errorCode, String errorMessage, String request) {

                        CustomExceptionConfig.getInstance().logException("https://ulm.finjanmobile.com/oauth/token",
                                errorMessage,request,TokenRefreshApiException.class.getSimpleName());

                        String[] data = UserPref.getInstance().getCachedUserForAutoLogin();
                        if(data!=null && data.length == 3 && !TextUtils.isEmpty(data[1])){
                            AuthenticationHelper.autoLogin(FinjanVPNApplication.getInstance(),
                                    new AuthenticationHelper.AutoLoginResult() {
                                        @Override
                                        public void autoLoginResults(boolean success) {
                                            if(success){
                                                ResultData data = new ResultData(networkResultListener,null,null);
                                                Message message = handler.obtainMessage(1,data);
                                                message.sendToTarget();
//                                                networkResultListener.executeOnSuccess(null);
                                            }else {
                                                ResultData data = new ResultData(networkResultListener,null,null);
                                                Message message = handler.obtainMessage(0,data);
                                                message.sendToTarget();
//                                                networkResultListener.executeOnError(null);
                                            }
                                        }
                                    });
                        }else {
                            ResultData data1 = new ResultData(networkResultListener,null,null);
                            Message message = handler.obtainMessage(0,data1);
                            message.sendToTarget();
//                            networkResultListener.executeOnError(null);
                        }

                    }
                });
    }


    /*private void fetchJson(final Context context, @NonNull FailedRequest failedRequest){
        final String url = failedRequest.url;
        final NetworkResultListener networkResultListener = failedRequest.networkResultListener;
        final String correspondingExceptionClass = failedRequest.correspondingClass;
        Logger.logD(TAG, "fetchJson url " + url);
        final JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.logD(TAG, "onResponse(JSONObject response) " + response.toString());
                        // the response is already constructed as a JSONObject!
//                        new CreateDeviceApiException("Testing Url","","");
                        networkResultListener.executeOnSuccess(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;
                        if (error == null) {
                            json = "Volly error Unknown response";
                        } else {
                            json = WebUtility.getMessage(error)+ " "+ error.getMessage();
                        }
                        if(error!=null){
                            int status = WebUtility.getHTTPErrorCode(error);
                            if(status == 401 || status == 400){
                                networkResultListener.executeOnError(error);
                                refreshToken(new NetworkResultListener() {
                                    @Override
                                    public void executeOnSuccess(JSONObject response) {
                                        fetchJson(context, networkResultListener, url, correspondingExceptionClass);
                                    }

                                    @Override
                                    public void executeOnError(VolleyError error) {
                                        VpnHomeFragment.showLoginScreen();

                                    }
                                });
                                return;
                            }
                            if(status == 409 || status == 404 || status == 401 || isSkippableError(json)){
                                networkResultListener.executeOnError(error);
                                return;
                            }
                        }

                        CustomExceptionConfig.getInstance().logException(url, "", json+" "+ error.getMessage(), correspondingExceptionClass);
                        networkResultListener.executeOnError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkManager.this.getHeaders();
            }
        };

        jsonRequest.setRetryPolicy(Authentication.getRetryPolicy());
        getRequestQueue(context).add(jsonRequest);
    }*/

    private void fetchJson(@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener, final @NonNull String url,
                           @NonNull final String correspondingExceptionClass, final boolean isToCatchResponse, final String cacheKey, final String staticAuth) {
        Logger.logD(TAG, "fetchJson url " + url);
        final JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (isToCatchResponse && !TextUtils.isEmpty(cacheKey))
                        {
                            EventManeger.getInstance().initialisingCache(0);
                            com.finjan.securebrowser.vpn.controller.network.eventManeger.Response response1 = new com.finjan.securebrowser.vpn.controller.network.eventManeger.Response();
                            response1.setResponseText(response.toString());
                            EventManeger.getInstance().putValuesInCache(response1,cacheKey,0);
                            UserPref.getInstance().setChecheAvilable(cacheKey,System.currentTimeMillis());
                        }

                        Logger.logD(TAG, "onResponse(JSONObject response) " + response.toString());
                        // the response is already constructed as a JSONObject!
//                        new CreateDeviceApiException("Testing Url","","");
                        ResultData data = new ResultData(networkResultListener,response,null);
                        Message message = handler.obtainMessage(1,data);
                        message.sendToTarget();
//                        networkResultListener.executeOnSuccess(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;
                        if (error == null) {
                            json = "Volly error Unknown response";
                        } else {
                            json = WebUtility.getMessage(error)+ " "+ error.getMessage();
                        }
                        if(error!=null){
                            int status = WebUtility.getHTTPErrorCode(error);
                            if(status == 401 || status == 400){
                                ResultData data = new ResultData(networkResultListener,null,error);
                                Message message = handler.obtainMessage(0,data);
                                message.sendToTarget();
//                                networkResultListener.executeOnError(error);
                                refreshToken(new NetworkResultListener() {
                                    @Override
                                    public void executeOnSuccess(JSONObject response) {
                                        fetchJson(context, networkResultListener, url, correspondingExceptionClass,false,"");
                                    }

                                    @Override
                                    public void executeOnError(VolleyError error) {
                                        VpnHomeFragment.showLoginScreen();
//                                       NetworkManager.this.failedRequest = new FailedRequest(jsonObjectRequest);
                                        NetworkManager.this.failedRequest = new FailedRequest(networkResultListener,
                                                url,correspondingExceptionClass);
                                    }
                                });
                                return;
                            }
                            if(status == 409 || status == 404 || status == 401 || isSkippableError(json)){
                                ResultData data = new ResultData(networkResultListener,null,error);
                                Message message = handler.obtainMessage(0,data);
                                message.sendToTarget();
//                                networkResultListener.executeOnError(error);
                                return;
                            }
                        }

                        CustomExceptionConfig.getInstance().logException(url, "", json+" "+ error.getMessage(), correspondingExceptionClass);
                        ResultData data = new ResultData(networkResultListener,null,error);
                        Message message = handler.obtainMessage(0,data);
                        message.sendToTarget();
//                        networkResultListener.executeOnError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkManager.this.getHeadersStatic(staticAuth);
            }
        };
        jsonRequest.setRetryPolicy(Authentication.getRetryPolicy());
        getRequestQueue(context).add(jsonRequest);
    }

    private void fetchJson(@NonNull final Context context, @NonNull final NetworkResultListener networkResultListener, final @NonNull String url,
                           @NonNull final String correspondingExceptionClass, final boolean isToCatchResponse, final String cacheKey) {
        Logger.logD(TAG, "fetchJson url " + url);
        final JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.logD(TAG, "onResponse of url - "+url+" -- "+ response.toString());
                        if (isToCatchResponse && !TextUtils.isEmpty(cacheKey))
                        {
                            EventManeger.getInstance().initialisingCache(0);
                            com.finjan.securebrowser.vpn.controller.network.eventManeger.Response response1 = new com.finjan.securebrowser.vpn.controller.network.eventManeger.Response();
                            response1.setResponseText(response.toString());
                            EventManeger.getInstance().putValuesInCache(response1,cacheKey,0);
                            UserPref.getInstance().setChecheAvilable(cacheKey,System.currentTimeMillis());
                        }
                        // the response is already constructed as a JSONObject!
//                        new CreateDeviceApiException("Testing Url","","");
                        ResultData data = new ResultData(networkResultListener,response,null);
                        Message message = handler.obtainMessage(1,data);
                        message.sendToTarget();
//                        networkResultListener.executeOnSuccess(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            Logger.logD(TAG, "onResponse of url - " + url + " -- " + error.getMessage());
                        }else{
                            Logger.logD(TAG, "onResponse of url - " + url + " -- " + error);
                        }
                        String json = null;
                        if (error == null) {
                            json = "Volly error Unknown response";
                        } else {
                            json = WebUtility.getMessage(error)+ " "+ error.getMessage();
                        }
                        if(error!=null){
                            int status = WebUtility.getHTTPErrorCode(error);
                            if(status == 401 || status == 400){
                                ResultData data = new ResultData(networkResultListener,null,error);
                                Message message = handler.obtainMessage(0,data);
                                message.sendToTarget();
//                                networkResultListener.executeOnError(error);
                                String temp = url;
                               refreshToken(new NetworkResultListener() {
                                   @Override
                                   public void executeOnSuccess(JSONObject response) {
                                       fetchJson(context, networkResultListener, url, correspondingExceptionClass,false,"");
                                   }

                                   @Override
                                   public void executeOnError(VolleyError error) {
                                       VpnHomeFragment.showLoginScreen();
//                                       NetworkManager.this.failedRequest = new FailedRequest(jsonObjectRequest);
                                       NetworkManager.this.failedRequest = new FailedRequest(networkResultListener,
                                               url,correspondingExceptionClass);
                                   }
                               });
                                return;
                            }
                            if(status == 409 || status == 404 || status == 401 || isSkippableError(json)){
                                ResultData data = new ResultData(networkResultListener,null,error);
                                Message message = handler.obtainMessage(0,data);
                                message.sendToTarget();
//                                networkResultListener.executeOnError(error);
                                return;
                            }
                        }

                        CustomExceptionConfig.getInstance().logException(url, "", json+" "+ error.getMessage(), correspondingExceptionClass);
                        ResultData data = new ResultData(networkResultListener,null,error);
                        Message message = handler.obtainMessage(0,data);
                        message.sendToTarget();
//                        networkResultListener.executeOnError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkManager.this.getHeaders();
            }
        };
        jsonRequest.setRetryPolicy(Authentication.getRetryPolicy());
        getRequestQueue(context).add(jsonRequest);
    }



    private boolean isSkippableError(String response){
        if(response.contains("status:401") || response.contains("status:404") ||
                response.contains("status:409")){return true;}
                return false;
    }
    public void hitCashedRequest(@NonNull  GlobalVariables.LastNetworkConnection lastNetworkConnection){
        if(lastNetworkConnection.isJsonRequest){
            fetchJsonPost(lastNetworkConnection.context,lastNetworkConnection.listener,
                    lastNetworkConnection.url,lastNetworkConnection.jsonObject,
                    lastNetworkConnection.correspondingClass);
        }else {
            fetchJson(lastNetworkConnection.context,lastNetworkConnection.listener,lastNetworkConnection.url,
                    lastNetworkConnection.correspondingClass,false,"");
        }
    }

    /*private void fetchJsonPost(@NonNull Context context, FailedRequest failedRequest) {
        final String url=failedRequest.url;
        final JSONObject jsonObject=failedRequest.jsonObject;
        final NetworkResultListener networkResultListener= failedRequest.networkResultListener;
        final String correspondingExceptionClass= failedRequest.correspondingClass;
        Logger.logE(TAG, "fetchJson url " + url);
        Logger.logE(TAG, "Json url " + jsonObject.toString());
        int method = Request.Method.POST;
        if (url.contains("devices/")) {
            method = Request.Method.PATCH;
        } else if (url.contains("/license")) {
            method = Request.Method.PUT;
        } else if (url.contains("/subscription")) {
            if (url.endsWith("/subscription")) {
                method = Request.Method.POST;
            } else {
                method = Request.Method.PUT;
            }
        }


        final JsonObjectRequest jsonRequest = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Logger.logD(TAG, "onResponse(JSONObject response) " + response.toString());
                // the response is already constructed as a JSONObject!
                networkResultListener.executeOnSuccess(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;
                if (error == null) {
                    json = "Volly error Unknown response";
                } else {
                    json = WebUtility.getMessage(error)+ " "+ error.getMessage();
                }
                if(error instanceof ClientError){
                    // type of no route exception
                }
                if(error!=null) {
                    int status = WebUtility.getHTTPErrorCode(error);
                    if (status == 401 || status == 400) {
                        networkResultListener.executeOnError(error);
                        VpnHomeFragment.showLoginScreen();
//                        NetworkManager.this.failedRequest = new FailedRequest(jsonRequest);
                        NetworkManager.this.failedRequest = new FailedRequest(networkResultListener,
                                url,correspondingExceptionClass,jsonObject);
                        return;
                    }
                }
                if(json!=null && !(json.contains("404") || json.contains("409") || json.contains("401"))){
                    CustomExceptionConfig.getInstance().logException(url, json, jsonObject, correspondingExceptionClass);
                }
                networkResultListener.executeOnError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkManager.this.getHeaders();
            }
        };
        jsonRequest.setRetryPolicy(Authentication.getRetryPolicy());
        getRequestQueue(context).add(jsonRequest);
    }*/
    public void getStagingAuthTocken(Context context, NetworkResultListener networkResultListener){
        JSONObject jsonObject = null;
        try {
           jsonObject  = new JSONObject("{\n" +
                    "    \"grant_type\": \"password\",\n" +
                    "    \"username\": \"danny@gmail.com\",\n" +
                    "    \"password\": \"test123\",\n" +
                    "    \"client_id\": \"Evere4yl2dtaTclTP2tYPjjrYP4mir\",\n" +
                    "    \"client_secret\": \"iXXwpbAXxDa1N4d9X3br11KlsJbw5u\"\n" +
                    "}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        fetchJsonPost(context,networkResultListener,"http://vitalulm.staging.wpengine.com/oauth/token",jsonObject,null);
    }

    private void fetchJsonPost(@NonNull Context context, @NonNull final NetworkResultListener networkResultListener,
                               @NonNull final String url, final @NonNull JSONObject jsonObject, final @NonNull String correspondingExceptionClass) {
        Logger.logE(TAG, "fetchJsonPost url " + url);
        Logger.logD(TAG, "fetchJsonPost data " + jsonObject.toString());
        int method = Request.Method.POST;
        if (url.contains("devices/")) {
            method = Request.Method.PATCH;
        } else if (url.contains("/license")) {
            method = Request.Method.PUT;
        } else if (url.contains("/subscription")) {
            if (url.endsWith("/subscription")) {
                method = Request.Method.POST;
            } else {
                method = Request.Method.PUT;
            }
        }


        final JsonObjectRequest jsonRequest = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Logger.logD(TAG, "fetchJsonPost onResponse of url "+url+" -- " + response.toString());
                // the response is already constructed as a JSONObject!
                ResultData data = new ResultData(networkResultListener,response,null);
                Message message = handler.obtainMessage(1,data);
                message.sendToTarget();
//                networkResultListener.executeOnSuccess(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    Logger.logD(TAG, "fetchJsonPost onResponse of url " + url + " -- " + error.getMessage());
                }else{
                    Logger.logD(TAG, "fetchJsonPost onResponse of url "+url+" -- " + error);
                }
                String json = null;
                if (error == null) {
                    json = "Volly error Unknown response";
                } else {
                    json = WebUtility.getMessage(error)+ " "+ error.getMessage();
                }
                if(error instanceof ClientError){
                    // type of no route exception
                }
                if(error!=null) {
                    int status = WebUtility.getHTTPErrorCode(error);
                    if (status == 401 || status == 400) {
                        ResultData data = new ResultData(networkResultListener,null,error);
                        Message message = handler.obtainMessage(0,data);
                        message.sendToTarget();
//                        networkResultListener.executeOnError(error);
                        VpnHomeFragment.showLoginScreen();
//                        NetworkManager.this.failedRequest = new FailedRequest(jsonRequest);
                        NetworkManager.this.failedRequest = new FailedRequest(networkResultListener,
                                url,correspondingExceptionClass,jsonObject);
                        return;
                    }
                }
                if(json!=null && !(json.contains("404") || json.contains("409") || json.contains("401"))){
                    CustomExceptionConfig.getInstance().logException(url, json, jsonObject, correspondingExceptionClass);
                }
                ResultData data = new ResultData(networkResultListener,null,error);
                Message message = handler.obtainMessage(0,data);
                message.sendToTarget();
//                networkResultListener.executeOnError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkManager.this.getHeaders();
            }
        };
        jsonRequest.setRetryPolicy(Authentication.getRetryPolicy());
        getRequestQueue(context).add(jsonRequest);
    }

    public void hitNeverBounceAPI(@NonNull Context context, @NonNull final NetworkResultListener networkResultListener, final @NonNull String emailId) {

        final String url = String.format(context.getString(R.string.never_bounce_url),emailId);
        Logger.logD(TAG, "hitNeverBounceAPI " + url);
        JSONObject jsonObject = JsonCreater.getInstance().getNeverBounceRequest(context.getString(R.string.never_bounce_key));
        int method = Request.Method.POST;


        final JsonObjectRequest jsonRequest = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Logger.logD(TAG, "hitNeverBounceAPI onResponse of url "+url+" -- " + response.toString());
                // the response is already constructed as a JSONObject!
                ResultData data = new ResultData(networkResultListener,response,null);
                Message message = handler.obtainMessage(1,data);
                message.sendToTarget();
//                networkResultListener.executeOnSuccess(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    Logger.logD(TAG, "hitNeverBounceAPI onResponse of url " + url + " -- " + error.getMessage());
                }else{
                    Logger.logD(TAG, "hitNeverBounceAPI onResponse of url "+url+" -- " + error);
                }
                String json = null;
                if (error == null) {
                    json = "Volly error Unknown response";
                } else {
                    json = WebUtility.getMessage(error)+ " "+ error.getMessage();
                }

                if(json!=null){
                    CustomExceptionConfig.getInstance().logException(url, "", json+" "+ error.getMessage(), NeverBounceApiException.class.getSimpleName());
                }

                ResultData data = new ResultData(networkResultListener,null,error);
                Message message = handler.obtainMessage(0,data);
                message.sendToTarget();
//                networkResultListener.executeOnError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkManager.this.getHeaders();
            }
        };
        jsonRequest.setRetryPolicy(Authentication.getRetryPolicy());
        getRequestQueue(context).add(jsonRequest);
    }

    private HashMap<String, String> getHeaders() {
        HashMap<String, String> params = new HashMap<>();
        // read user authenticate token and add to a request
        String authToken = "Bearer " + FinjanVpnPrefs.getAuthToken(FinjanVPNApplication.getInstance());
//        String authToken = "Bearer "+ UserPref.getInstance().getCurrent_Access_Token();
        Logger.logD(TAG, "AUTHORIZATION " + authToken);
        if (!TextUtils.isEmpty(authToken)) {
            params.put(AUTHORIZATION, authToken);
        }
        return params;
    }




    private HashMap<String, String> getHeadersStatic(String staticAuth) {
        HashMap<String, String> params = new HashMap<>();
        // read user authenticate token and add to a request
        String authToken = "Bearer "+staticAuth ;
//        String authToken = "Bearer "+ UserPref.getInstance().getCurrent_Access_Token();
        Logger.logD(TAG, "AUTHORIZATION_staging " + authToken);
        if (!TextUtils.isEmpty(authToken)) {
            params.put(AUTHORIZATION, authToken);
        }
        return params;
    }

    private static class ResultData{
        public NetworkResultListener listener;
        public JSONObject result;
        public VolleyError error;

        public ResultData(NetworkResultListener listener, JSONObject result, VolleyError error){
            this.listener = listener;
            this.result = result;
            this.error = error;
        }
    }

}
