/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.finjan.securebrowser.vpn.util.AviraApps;

/**
 * Receiver class for package changes events
 */
public class PackageChangesReceiver extends BroadcastReceiver {

	private static final String TAG = PackageChangesReceiver.class.getName();

	@Override
	public void onReceive(Context context, Intent intent) {
//		if(context==null){
//			return;
//		}
//		String action = intent.getAction();
//		String packageName = intent.getData().getSchemeSpecificPart();
//		Logger.logD(TAG, "onReceive " + action + ", packageName " + packageName);
//
//		switch (action) {
//			case Intent.ACTION_PACKAGE_ADDED:
//			case Intent.ACTION_PACKAGE_REMOVED:
//				if (AviraApps.isAviraApp(packageName)) {
////					PeopleProfileManager.getInstance().updateInstalledAviraAppsList(context);
//				}
//				break;
//		}
	}
}
