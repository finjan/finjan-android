package com.finjan.securebrowser.vpn.helpers;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.avira.common.backend.WebUtility;
import com.finjan.securebrowser.vpn.controller.JsonCreater;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anurag on 14/07/17.
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class DeviceUpdateHelper {

    private static final String TAG=DeviceUpdateHelper.class.getSimpleName();
    private static final class DeviceUpdateHelperHolder{
        private static final DeviceUpdateHelper Instance=new DeviceUpdateHelper();
    }
    public static DeviceUpdateHelper getInstance(){
        return DeviceUpdateHelperHolder.Instance;
    }


    public static void addCurrentDevice(final Context context, final DeviceUpdateListener listener){
        NetworkManager.getInstance(context)
                .createDevice(context.getApplicationContext(),
                        JsonCreater.getInstance().getCreateDeviceObject(context.getApplicationContext()), new NetworkResultListener() {
                            @Override
                            public void executeOnSuccess(JSONObject response) {
                                try {
                                    String msg=response.getString("message");
                                    Logger.logE(TAG,msg);
                                    NativeHelper.getInstnace().showToast(context,msg, Toast.LENGTH_SHORT);
                                    if(listener!=null)
                                    listener.onDeviceUpdated(true);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void executeOnError(VolleyError error) {
                                Logger.logE(TAG, WebUtility.getMessage(error));
                                NativeHelper.getInstnace().showToast(context,error);
                                if(listener!=null)
                                    listener.onDeviceUpdated(false);
                            }
                        });
    }
    public static void updateCurrentDevice(final Context context,final DeviceUpdateListener listener){

        NetworkManager.getInstance(context)
                .updateDevice(context,
                        JsonCreater.getInstance().getUpdateDeviceJson(context.getApplicationContext(),
                                null,null,NativeHelper.getInstnace().getIpAddressOfDevice()),new NetworkResultListener() {
                            @Override
                            public void executeOnSuccess(JSONObject response) {
                                try {
                                    String msg=response.getString("message");
                                    Logger.logE(TAG,msg);
                                    NativeHelper.getInstnace().showToast(context,msg,Toast.LENGTH_SHORT);
                                    if(listener!=null)
                                        listener.onDeviceUpdated(true);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void executeOnError(VolleyError error) {
                                Logger.logE(TAG, WebUtility.getMessage(error));
                                NativeHelper.getInstnace().showToast(context,error);
                                if(listener!=null)
                                    listener.onDeviceUpdated(false);
                            }
                        });

    }
    public interface DeviceUpdateListener{
        void onDeviceUpdated(Boolean success);
    }
}
