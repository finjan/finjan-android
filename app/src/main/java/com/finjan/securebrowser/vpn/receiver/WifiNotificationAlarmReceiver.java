package com.finjan.securebrowser.vpn.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.PermissionUtil;
import com.finjan.securebrowser.vpn.util.Util;

/**
 * Class to receive custom alarm triggered events, set by us to attempt to show notification at specific times.
 * <p>
 * Created by illia.klimov on 11/17/2016.
 */
public class WifiNotificationAlarmReceiver extends BroadcastReceiver {
    private static final String TAG = WifiNotificationAlarmReceiver.class.getSimpleName();
    public static final String ACTION_NOTIF_ALARM = "android.avira.vpn.postponed.NotificationAlarm";

    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.logD(TAG, "onReceive " + intent.getAction());

        if (intent != null && ACTION_NOTIF_ALARM.equals(intent.getAction())) {

            Context applicationContext = FinjanVPNApplication.getInstance();
            if (!PermissionUtil.hasLocationPermission(applicationContext) && FinjanVpnPrefs.getWifiPermissionNotificationCount(applicationContext) < VpnActivity.WIFI_PERMISSION_NOTIFICATION_COUNT) {
                FinjanVpnPrefs.setWifiPermissionNotificationCount(applicationContext, FinjanVpnPrefs.getWifiPermissionNotificationCount(applicationContext) + 1);
//                Util.sendWifiPermissionNotification(applicationContext);
            }
        }
    }
}
