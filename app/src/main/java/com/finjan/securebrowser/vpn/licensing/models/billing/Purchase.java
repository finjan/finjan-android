/* Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.finjan.securebrowser.vpn.licensing.models.billing;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.avira.common.GSONModel;
import com.finjan.securebrowser.constants.AppConstants;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents an in-app billing purchase.
 */
public class Purchase implements GSONModel, Parcelable {

    public static final String tempPurchase = "_tempP";
    @SerializedName("itemType")
    String mItemType;  // ITEM_TYPE_INAPP or ITEM_TYPE_SUBS
    @SerializedName("orderId")
    String mOrderId;
    @SerializedName("packageName")
    String mPackageName;
    @SerializedName("sku")
    String mSku;
    @SerializedName("purchaseTime")
    long mPurchaseTime;
    @SerializedName("purchaseState")
    int mPurchaseState;
    @SerializedName("developerPayload")
    String mDeveloperPayload;
    @SerializedName("token")
    String mToken;
    @SerializedName("originalJson")
    String mOriginalJson;
    @SerializedName("signature")
    String mSignature;
    @SerializedName("expiryTimeMillis")
    long mExpiryTimeMillis;
    @SerializedName("autoRenewing")
    boolean mautoRenewing;

    public Purchase(Parcel in) {
        String[] stringArray = new String[7];
        in.readStringArray(stringArray);
        mExpiryTimeMillis=in.readLong();
        mPurchaseTime=in.readLong();
        mPurchaseState=in.readInt();
        mautoRenewing=in.readInt()!=0;
        this.mItemType = stringArray[0];
        mOrderId = stringArray[1];
        mPackageName = stringArray[2];
        mSku = stringArray[3];
        mDeveloperPayload = stringArray[4];
        mToken = stringArray[5];
        mOriginalJson = stringArray[6];
        mSignature = stringArray[7];
    }
    public Purchase(Bundle bundle){

        mItemType = bundle.getString(AppConstants.BKEY_ITEMTYPE);
        mOriginalJson = bundle.getString(AppConstants.BKEY_originalJson);
        mOrderId = bundle.getString(AppConstants.BKEY_ORDERID);
        mPackageName = bundle.getString(AppConstants.BKEY_PACKAGENAME);
        mSku = bundle.getString(AppConstants.BKEY_SKU);
        mPurchaseTime = bundle.getLong(AppConstants.BKEY_PURCHASETIME);
        mPurchaseState = bundle.getInt(AppConstants.BKEY_purchaseState);
        mDeveloperPayload = bundle.getString(AppConstants.BKEY_developerPayload);
        mToken = bundle.getString(AppConstants.BKEY_token);
        mExpiryTimeMillis = bundle.getLong(AppConstants.BKEY_expiryTimeMillis);
        mautoRenewing= bundle.getBoolean(AppConstants.BKEY_autoRenewing);
        mSignature = bundle.getString(AppConstants.BKEY_signature);

    }

    public Purchase(String itemType, String jsonPurchaseInfo, String signature) throws JSONException {
        mItemType = itemType;
        mOriginalJson = jsonPurchaseInfo;
        JSONObject o = new JSONObject(mOriginalJson);
        mOrderId = o.optString("orderId");
        mPackageName = o.optString("packageName");
        mSku = o.optString("productId");
        mPurchaseTime = o.optLong("purchaseTime");
        mPurchaseState = o.optInt("purchaseState");
        mDeveloperPayload = o.optString("developerPayload");
        mToken = o.optString("token", o.optString("purchaseToken"));
        mExpiryTimeMillis = o.optLong("expiryTimeMillis");
        mautoRenewing= o.optBoolean("autoRenewing");
        mSignature = signature;
    }

    public String getItemType() {
        return mItemType;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public String getPackageName() {
        return mPackageName;
    }

    public String getSku() {
        return mSku;
    }

    public long getPurchaseTime() {
        return mPurchaseTime;
    }

    public int getPurchaseState() {
        return mPurchaseState;
    }

    public String getDeveloperPayload() {
        return mDeveloperPayload;
    }

    public String getToken() {
        return mToken;
    }

    public String getOriginalJson() {
        return mOriginalJson;
    }

    public String getSignature() {
        return mSignature;
    }

    public void setTempToken() {
        if (mToken != null) {
            mToken = mToken.concat(tempPurchase);
        }
    }

    public long getmExpiryTimeMillis() {
        return mExpiryTimeMillis;
    }

    public void setmExpiryTimeMillis(long mExpiryTimeMillis) {
        this.mExpiryTimeMillis = mExpiryTimeMillis;
    }
    public boolean getAutoRenewing(){return mautoRenewing;}

    @Override
    public String toString() {
        return "PurchaseInfo(type:" + mItemType + "):" + mOriginalJson;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     *
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{mItemType,mOrderId,mPackageName,mSku,mDeveloperPayload,
        mToken,mOriginalJson,mSignature});
        dest.writeLong(mExpiryTimeMillis);
        dest.writeLong(mPurchaseTime);
        dest.writeInt(mPurchaseState);
        dest.writeInt(mautoRenewing?1:0);

    }
    public static final Parcelable.Creator CREATOR= new Parcelable.Creator<Purchase>(){
        @Override
        public Purchase[] newArray(int size) {
            return new Purchase[size];
        }

        @Override
        public Purchase createFromParcel(Parcel source) {
            return new Purchase(source);
        }
    };
}
