package com.finjan.securebrowser.vpn.licensing.models.restful;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ovidiu.buleandra
 * @since 16.11.2015
 */
public class LicenseArray implements GSONModel {
    @SerializedName("data") private List<License> data;

    public List<License> getLicenses() {
        return data;
    }
    public void setLicenses(List<License> licenses) { data = licenses; }

    @Override
    public String toString() {
        return data != null ? data.toString() : "null";
    }
}
