package com.finjan.securebrowser.vpn.ui.authentication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.avira.common.activities.BaseFragmentActivity;
import com.avira.common.authentication.models.Subscription;
import com.finjan.securebrowser.activity.SplashScreen;
import com.finjan.securebrowser.activity.TutorialActivity;
import com.finjan.securebrowser.vpn.licensing.TrialRequestCallback;
import com.avira.common.utils.ProgressDialogUtil;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.vpn.ui.authentication.AuthenticationFragment.AuthenticationListener;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;

public class SetupAccountActivity extends BaseFragmentActivity
        implements AuthenticationListener, TrialRequestCallback {

    public static final String TAG = SetupAccountActivity.class.getSimpleName();

    private ProgressDialogUtil mProgressDialogUtil;

    public static final String ACTION_SIGN_UP = "com.avira.vpn.ui.authentication.SetupAccountActivity.ACTION_SIGN_UP";
    public static final String ACTION_SIGN_IN = "com.avira.vpn.ui.authentication.SetupAccountActivity.ACTION_SIGN_IN";
    public static final String ACTION_SIGN_NAV = "com.avira.vpn.ui.authentication.SetupAccountActivity.ACTION_SIGN_NAV";

    /**
     * create an instance for the {@link SetupAccountActivity} just for login purposes
     *
     * @param activity calling activity used to create this instance
     */
    public static void newLoginInstanceForResult(Activity activity, int requestCode, String action) {
        Intent newIntent = new Intent(activity, SetupAccountActivity.class);
        newIntent.setAction(action);
        if(activity instanceof TutorialActivity || activity instanceof SplashScreen || activity instanceof VpnActivity){
            newIntent.putExtra("backArrow",false);
        }else {
            newIntent.putExtra("backArrow",true);
        }
        activity.startActivityForResult(newIntent, requestCode);
        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.enter_to_left);
    }
    public static boolean isSetupAccountLive = false;

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        authenticationFragment.onBackClick();
//        if(authenticationFragment.userPressedBackSoManyTimes){
//            VpnActivity.exitAlert(this);
//            return;
//        }
//        overridePendingTransition(R.anim.enter_from_left, R.anim.enter_to_right);
    }

    public static void startActivity(Context context, String action) {
        Intent newIntent = new Intent(context, SetupAccountActivity.class);
        newIntent.setAction(action);
        context.startActivity(newIntent);
    }

    private SetupAccountActivity instance;

    public SetupAccountActivity getInstance(){return instance;}


    @Override
    protected void onResume() {
        super.onResume();
        instance= this;
        isSetupAccountLive =true;
        NativeHelper.getInstnace().currentAcitvity=this;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isSetupAccountLive=false;
    }

    private AuthenticationFragment authenticationFragment;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        instance= this;
        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_to_left);
        setContentView(R.layout.activity_setup);
        NativeHelper.getInstnace().changeStatusBarTransparent(this);
        AuthNavigationHelper.getINSTANCE().init(this);
        authenticationFragment= AuthenticationFragment.newInstance(getIntent().getAction());
        Bundle bundle1 = authenticationFragment.getArguments();
        bundle1.putBoolean("backArrow",getIntent().getBooleanExtra("backArrow",false));
        authenticationFragment.setArguments(bundle1);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, authenticationFragment)
.setCustomAnimations(0, R.anim.enter_to_right,R.anim.enter_from_left,R.anim.enter_to_right)
                .commitAllowingStateLoss();

        mProgressDialogUtil = new ProgressDialogUtil(this);
    }

    @Override
    public void onFinishSetup(int response,boolean success) {
        Intent intent=new Intent();
        intent.putExtra("success",success);
        setResult(response,intent);
        finish();
    }

    @Override
    public void onTrialRequestSuccessful(String s, Subscription subscription) {
        mProgressDialogUtil.dismiss();
    }

    @Override
    public void onTrialRequestError(int i, String s) {
        mProgressDialogUtil.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== 101){
            if(authenticationFragment!=null && authenticationFragment.isAdded()){
                if(resultCode == RESULT_OK){
                    if(AuthNavigationHelper.getINSTANCE().getCb_eulaSocial()!=null){
                        AuthNavigationHelper.getINSTANCE().getCb_eulaSocial().setChecked(true);
                        AuthNavigationHelper.getINSTANCE().ifUserUpdatePolicy();
                    }
                }else {
                    AuthNavigationHelper.getINSTANCE().userDeniedPolicy();
                }
            }
        }
        else if(requestCode== 102){
                if(authenticationFragment!=null && authenticationFragment.isAdded()){
                    if(resultCode == RESULT_OK){
                        if(AuthNavigationHelper.getINSTANCE().getCb_eulaSocial()!=null){
                            AuthNavigationHelper.getINSTANCE().getCb_eulaSocial().setChecked(true);
                            AuthNavigationHelper.getINSTANCE().ifUserUpdateEmail(data.getStringExtra("email"),data.getStringExtra("email_verified"));
                        }
                    }else {
                        AuthNavigationHelper.getINSTANCE().userDeniedEmail();
                    }
                }
            }

    }
}
