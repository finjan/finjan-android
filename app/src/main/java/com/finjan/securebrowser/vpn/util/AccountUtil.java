package com.finjan.securebrowser.vpn.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Log;
import android.util.Patterns;

import com.finjan.securebrowser.helpers.logger.Logger;

import java.util.regex.Pattern;

/**
 * device accounts related utilities
 *
 * @author ovidiu.buleandra
 * @since 24.12.2015
 */
public class AccountUtil {
    private static final String TAG = AccountUtil.class.getSimpleName();

    /**
     * retrieves the primary google account email
     * <br />
     * requires GET_ACCOUNTS permission to actually work
     *
     * @param context
     * @return email if it can retrieve it, otherwise empty string
     */
    public static String retrieveUserGoogleEmail(Context context) {
        String email = null;

        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                email = account.name;
                break;
            }
        }
        // should we cache it?
//            ApplockPrefs.saveGoogleEmail(context, email);
        Logger.logD(TAG, "found email=" + email);

        return email;
    }
}
