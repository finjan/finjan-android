/*
 * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

//import com.finjan.securebrowser.a_vpn.controller.mixpanel.PeopleProfileManager;

/**
 * Class to receive system events related to changes in device locale
 */
public class LocaleChangedReceiver extends BroadcastReceiver {
    
    @Override
    public void onReceive(Context context, Intent intent) {
//        PeopleProfileManager.getInstance().updateLocale(context);
    }
}
