package com.finjan.securebrowser.vpn.ui.authentication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;

import android.os.Looper;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.avira.common.GeneralPrefs;
import com.avira.common.authentication.Authentication;
import com.avira.common.authentication.AuthenticationListener;
import com.avira.common.authentication.BasicApiCallback;
import com.avira.common.authentication.BasicApiListener;
import com.avira.common.authentication.models.BasicApiResponse;
import com.avira.common.authentication.models.LoginResponse_1;
import com.avira.common.authentication.models.Subscription;
import com.avira.common.authentication.models.User;
import com.avira.common.authentication.models.UserAttributesPayload;
import com.avira.common.backend.Backend;
import com.avira.common.backend.ErrorCodeDescriptionMapper;
import com.avira.common.backend.ErrorCodeDescriptionMapper.Error;
import com.avira.common.backend.ServerErrorCodeProcessor;
import com.avira.common.id.HardwareId;
import com.avira.common.utils.NetworkConnectionManager;
import com.avira.common.utils.validation.EmailConstraintChecker;
import com.avira.common.utils.validation.MyAccPasswordConstraintChecker;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.NormaWebviewActivity;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.constants.DrawerConstants;
import com.finjan.securebrowser.custom_exceptions.CustomExceptionConfig;
import com.finjan.securebrowser.custom_exceptions.UserLoginApiException;
import com.finjan.securebrowser.custom_exceptions.UserRegisterApiException;
import com.finjan.securebrowser.eventbus_pojo.GoogleSignInSuccessEvent;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.tracking.facebook.FbTrackingConfig;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.kochava.KochavaTracking;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.tracking.tune.TuneTracking;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.JsonParser;
import com.finjan.securebrowser.vpn.controller.model.Device;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.vpn.eventbus.LicenceFetched;
import com.finjan.securebrowser.vpn.eventbus.LoginSuccess;
import com.finjan.securebrowser.vpn.helpers.DeviceUpdateHelper;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.mixpanel.Tracking;
import com.finjan.securebrowser.vpn.subscriptions.SubscriptionHelper;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;
import com.finjan.securebrowser.vpn.ui.main.VpnApisHelper;
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment;
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.common.eventbus.Subscribe;

import org.json.JSONObject;

import java.util.HashMap;

import de.greenrobot.event.EventBus;

/**
 * fragment that deals with user authentication with the OE backend
 * <br />
 * has support for login with facebook and google plus (currently disabled since it actually doesn't work)
 *
 * @author Illia.Klimov
 * @since 13.02.2016
 */
public class AuthenticationFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener,
        TextView.OnEditorActionListener, AuthenticationListener {
    public static final String TAG = AuthenticationFragment.class.getSimpleName();

    private static final String EXTRA_ACTION = "EXTRA_ACTION";
    private static final String EMAIL_EXISTS = "email_exists";
    protected LinearLayout  llToolbar, llBack;
    protected RelativeLayout rlRegister,llLogin,llTermsAndCondition;
    private TextView tvPrivacyMassege;
    private TextView btnIgnore,btnAccept;
    protected String mAction;
    protected boolean showingRegister;
    protected String currentScreen;
    protected String lastScreen;
    RelativeLayout rlProgress;
    private EditText mEtEmail_R, mEtFullName_R, mEtPassword_R, mEtConfirmPassword_R;
    private EditText mEtEmail_L, mEtPassword_L;
    private ImageView mEmailIcon_R, mPasswordIcon_R, mFullUserIcon_R, mConfirmPasswordIcon_R;
    private ImageView mEmailIcon_L, mPasswordIcon_L;
    private TextInputLayout tiEmail_R, tiEmail_L, tiPassword_R, tiPassword_L,
            tiFullUserName, tiConfirmPassword;
    private CheckBox cb_eula;
    private String mValidEmail, mValidPassword, mFullName, emailVerified;
    private AuthenticationListener mCallback;
    private ProgressDialog progressDialog;
    protected boolean isTermsAndConditionScreen;
    private BasicApiListener basicApiListener = new BasicApiListener() {
        @Override
        public void onApiSuccess(BasicApiResponse basicApiResponse) {
            hideProgress();
            GlobalVariables.user_registered=true;
            NativeHelper.getInstnace().showToast(getContext(), basicApiResponse.getMessage(), Toast.LENGTH_SHORT);
            onClickLogin();
            fireRegisterTrackers();
        }

        @Override
        public void onApiFailled(BasicApiCallback.BasicError errorMessage, String request) {
            hideProgress();

            if (!TextUtils.isEmpty(errorMessage.getCode()) && errorMessage.getCode().equals(EMAIL_EXISTS)) {
                AuthNavigationHelper.getINSTANCE().switchAccountSetup(SetupAccountActivity.ACTION_SIGN_IN, false, false);
                mEtEmail_L.setText(mEtEmail_R.getText().toString());
                mEtPassword_L.setText("");
                mEtPassword_L.requestFocus();
            } else {
                CustomExceptionConfig.getInstance().logException(/*"https://ulm.finjanmobile.com/wp-json/api/v1/"*/AppConfig.Companion.isStagingUrl()?Backend.stagingurl2:Backend.liveUrl/*getResources().getString(R.string.vpn_backend_configuration_finjan)*/+"users",
                        errorMessage.getJson(), request, UserRegisterApiException.class.getSimpleName());
            }
            if (NativeHelper.getInstnace().checkContext(getContext())) {
                new FinjanDialogBuilder(getContext()).setMessage(errorMessage.getDesc()).show();
            }

        }
    };
    private int licenFailedCount = 0;
    private DeviceUpdateHelper.DeviceUpdateListener listener = new DeviceUpdateHelper.DeviceUpdateListener() {
        @Override
        public void onDeviceUpdated(Boolean success) {
            updateLicence();
            VpnHomeFragment.userJustLogin=true;
//            authSuccess(true);
        }
    };
    private String orignalAction;

    public static AuthenticationFragment newInstance(String action) {
        AuthenticationFragment fragment = new AuthenticationFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_ACTION, action);
        fragment.setArguments(args);
        return fragment;
    }
    public String loginType = "email";


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
        try {
            mCallback = (AuthenticationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + AuthenticationListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(GoogleSignInSuccessEvent event) {
        if (event.getMessage().equalsIgnoreCase("success")) {
           Logger.logE(TAG, "event bus sent success....");
            //show sign out button
        } else if (event.getMessage().equalsIgnoreCase("error")) {
            //show error message for google sign in error
        } else if (event.getMessage().equalsIgnoreCase("signOut")) {
            //add sign out functionality
        }

    }

    //for google signup process
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        AuthNavigationHelper.getINSTANCE().setPromoList(getActivity());
    }

    //for google signup process
    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth_new, container, false);
        view.findViewById(R.id.progressbar1).setVisibility(View.GONE);
        Bundle args = getArguments();
        if (args != null) {
            mAction = args.getString(EXTRA_ACTION);
            this.orignalAction = mAction;
        }
        setReferences(view);
        currentScreen = mAction;
        AuthNavigationHelper.getINSTANCE().setView(view, this, llBack);
        bindViews(view);
        backArrow = args.getBoolean("backArrow",false);
        llBack.setVisibility(backArrow?View.VISIBLE:View.GONE);
       GoogleTrackers.Companion.setSGET_STARTED();
        return view;
    }

    boolean backArrow;

    private void setReferences(View view) {
        rlProgress = (RelativeLayout) view.findViewById(R.id.rl_progresss);
        rlRegister = (RelativeLayout) view.findViewById(R.id.include_register);
        llLogin = (RelativeLayout) view.findViewById(R.id.include_login);
        llTermsAndCondition = (RelativeLayout) view.findViewById(R.id.include_terms_and_condition);
        tvPrivacyMassege= (TextView)view.findViewById(R.id.tv_privacy_msg);
        btnIgnore= (TextView) view.findViewById(R.id.btn_back);
        btnAccept= (TextView) view.findViewById(R.id.btn_agree);
        btnIgnore.setText(PlistHelper.getInstance().getIgnoreButton());
        btnAccept.setText(PlistHelper.getInstance().getAcceptButton());
        btnAccept.setOnClickListener(this);
        btnIgnore.setOnClickListener(this);
        if (PlistHelper.getInstance().isHtml()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvPrivacyMassege.setText(Html.fromHtml(PlistHelper.getInstance().getPrivacyHtmlMassage(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                tvPrivacyMassege.setText(Html.fromHtml(PlistHelper.getInstance().getPrivacyHtmlMassage()));


            }
        }else {
            String message = "";
            String[] strMassage = PlistHelper.getInstance().getPrivacyMassage().trim().split("%n");
            for (int i = 0; i < strMassage.length; i++) {
                if (i == 0)
                    message = strMassage[i];
                else
                    message = message + "\n\n" + strMassage[i];
            }
            tvPrivacyMassege.setText(message);
        }
        ((TextView)(view.findViewById(R.id.tv_sign_in_title))).setTypeface(null, Typeface.BOLD);
        ((TextView)(view.findViewById(R.id.tv_auth_title))).setTypeface(null, Typeface.BOLD);
        ((TextView)(view.findViewById(R.id.tv_signin_title))).setTypeface(null, Typeface.BOLD);
        llToolbar = (LinearLayout) view.findViewById(R.id.ll_toolbar);
        llToolbar.setBackgroundColor(ResourceHelper.getInstance().getColor(R.color.transparent));
        llBack = (LinearLayout) llToolbar.findViewById(R.id.ll_back);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackClick();
            }
        });

        ((ObservableScrollView) rlRegister.findViewById(R.id.sv_register)).setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
            }

            @Override
            public void onDownMotionEvent() {
            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                if (scrollState == null) {
                    return;
                }
                if (scrollState.equals(ScrollState.DOWN)) {
                    llBack.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.enter_from_top));
                    llBack.setVisibility(View.VISIBLE);
                } else if (scrollState.equals(ScrollState.UP)) {
                    llBack.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.enter_to_top));
                    llBack.setVisibility(View.GONE);

                }
            }
        });
        ((ObservableScrollView) llLogin.findViewById(R.id.sv_login)).setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
            }

            @Override
            public void onDownMotionEvent() {
            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                if (scrollState == null) {
                    return;
                }
                if (scrollState.equals(ScrollState.DOWN)) {
                    llBack.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.enter_from_top));
                    llBack.setVisibility(View.VISIBLE);
                } else if (scrollState.equals(ScrollState.UP)) {
                    llBack.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.enter_to_top));
                    llBack.setVisibility(View.GONE);
                }
            }
        });
    }

    protected boolean userPressedBackSoManyTimes = false;
    protected void onBackClick() {
        NativeHelper.getInstnace().hideKeyBoard(getActivity());
       /* if (isTermsAndConditionScreen)
        {
            rlRegister.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_left));

            rlRegister.setVisibility(View.VISIBLE);
            llTermsAndCondition.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.enter_to_right));
            llTermsAndCondition.setVisibility(View.GONE);

            llBack.setEnabled(true);
            isTermsAndConditionScreen = false;
            return;
        }*/
        if (currentScreen.equals(SetupAccountActivity.ACTION_SIGN_NAV)) {
            if (TrafficController.getInstance(getContext()).isRegistered()) {
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.enter_to_right);
            } else {
                if(userPressedBackSoManyTimes){
                    VpnActivity.exitAlert(getActivity());
                    return;
                }
                userPressedBackSoManyTimes= AppConfig.Companion.getHandleSocialBackPress();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        userPressedBackSoManyTimes = false;
                    }
                },2000);
                Toast.makeText(FinjanVPNApplication.getInstance(), getString(R.string.login_to_continue_message), Toast.LENGTH_SHORT).show();
            }

        } else {
            if (currentScreen.equals(SetupAccountActivity.ACTION_SIGN_UP)) {
                if (orignalAction.equals(SetupAccountActivity.ACTION_SIGN_UP)) {
                    getActivity().onBackPressed();
                    getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.enter_to_right);
                } else
                    //here fromNav treat as is from SignUP Page
                    AuthNavigationHelper.getINSTANCE().switchAccountSetup(SetupAccountActivity.ACTION_SIGN_NAV, true, true);
            } else {
                if (lastScreen == null) {
                    getActivity().onBackPressed();
                    getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.enter_to_right);

                } else {
                    AuthNavigationHelper.getINSTANCE().switchAccountSetup(lastScreen, false, true);
                }
            }
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch (id) {
            case R.id.et_email_L:
                mEmailIcon_L.setAlpha(hasFocus ? 1 : .5f);
                tiEmail_L.setError(null);
                if (!hasFocus && TextUtils.isEmpty(mEtEmail_L.getText().toString())) {
                    tiEmail_L.setErrorEnabled(true);
                } else {
                    tiEmail_L.setErrorEnabled(false);
                }
                break;
            case R.id.et_email_R:
                mEmailIcon_R.setAlpha(hasFocus ? 1 : .5f);
                tiEmail_R.setError(null);
                if (!hasFocus && TextUtils.isEmpty(mEtEmail_R.getText().toString())) {
                    tiEmail_R.setErrorEnabled(true);
                } else {
                    tiEmail_R.setErrorEnabled(false);
                }
//                tiEmail_R.setErrorEnabled(false);
                break;
            case R.id.et_password_L:
                mPasswordIcon_L.setAlpha(hasFocus ? 1 : .5f);
                tiPassword_L.setError(null);

                if (!hasFocus && TextUtils.isEmpty(mEtPassword_L.getText().toString())) {
                    tiPassword_L.setErrorEnabled(true);
                } else {
                    tiPassword_L.setErrorEnabled(false);
                }

//                tiPassword_L.setErrorEnabled(false);
                break;
            case R.id.et_confirm_password_R:
                mConfirmPasswordIcon_R.setAlpha(hasFocus ? 1 : .5f);
                tiConfirmPassword.setError(null);

                if (!hasFocus && TextUtils.isEmpty(mEtConfirmPassword_R.getText().toString())) {
                    tiConfirmPassword.setErrorEnabled(true);
                } else {
                    tiConfirmPassword.setErrorEnabled(false);
                }

//                tiConfirmPassword.setErrorEnabled(false);
                break;
            case R.id.et_fullname_R:
                mFullUserIcon_R.setAlpha(hasFocus ? 1 : .5f);
                tiFullUserName.setError(null);

                if (!hasFocus && TextUtils.isEmpty(mEtFullName_R.getText().toString())) {
                    tiFullUserName.setErrorEnabled(true);
                } else {
                    tiFullUserName.setErrorEnabled(false);
                }


//                tiFullUserName.setErrorEnabled(false);
                break;
            case R.id.et_password_R:
                mPasswordIcon_R.setAlpha(hasFocus ? 1 : .5f);
                tiPassword_R.setError(null);

                if (!hasFocus && TextUtils.isEmpty(mEtPassword_R.getText().toString())) {
                    tiPassword_R.setErrorEnabled(true);
                } else {
                    tiPassword_R.setErrorEnabled(false);
                }


//                tiPassword_R.setErrorEnabled(false);
                break;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_already_have_account:
               // AuthNavigationHelper.getINSTANCE().switchAccountSetup(SetupAccountActivity.ACTION_SIGN_IN, false, false);
                break;
            case R.id.tv_dont_have_account:
                AuthNavigationHelper.getINSTANCE().switchAccountSetup(SetupAccountActivity.ACTION_SIGN_UP, false, false);
                break;
            case R.id.btn_signup:
                NativeHelper.getInstnace().hideKeyBoard(getActivity());
                onClickRegister();
                break;
            case R.id.btn_back:
                rlRegister.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_left));

                rlRegister.setVisibility(View.VISIBLE);
                llTermsAndCondition.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.enter_to_right));
                llTermsAndCondition.setVisibility(View.GONE);

                llBack.setEnabled(true);
                isTermsAndConditionScreen = false;
                break;
            case R.id.btn_agree:
                showProgressDialog(getString(R.string.QueryingInformationFromServer));
                verifyEmailAndSignup();
                break;
            case R.id.btn_login:
                NativeHelper.getInstnace().hideKeyBoard(getActivity());
                mValidPassword = mEtPassword_L.getText().toString();
                onClickLogin();
                break;
            case R.id.ti_email_L:
                tiEmail_L.requestFocus();
                break;
            case R.id.ti_email_R:
                tiEmail_R.requestFocus();
                break;
            case R.id.ti_password_L:
                tiPassword_L.requestFocus();
                break;
            case R.id.ti_password_R:
                tiPassword_R.requestFocus();
                break;
            case R.id.ti_username:
                tiFullUserName.requestFocus();
                break;
            case R.id.ti_confirm_password_R:
                tiConfirmPassword.requestFocus();
                break;


//            case R.id.btn_continue:
////                mIsPasswordInputError = false;
////                mIsEmailInputError = false;
//                onContinueOrDoneClick();
//                break;
            case R.id.tv_forgot_password:
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoPasswordRecoverUrl();
                }
                break;
            case R.id.tv_eula:
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoEulaUrl();
                }
                break;
            case R.id.tv_skip_registration:

                // if user has pressed skip and auto login was not made,
                // do it to prevent user login manualy later
//                doAutoLoginIfRequested(getActivity());
//
//                Activity context = getActivity();
//                FinjanVpnPrefs.setSetupCompleted(context);
//                startActivity(new Intent(context, VpnActivity.class));
//                context.finish();
                break;
//            case R.id.sign_in_button:// on click for google sign in button
//                Intent signInIntent=googleSignupObj.onClickSignInButton();
//                startActivityForResult(signInIntent, RC_SIGNUP);
//                break;
//            case R.id.login_button: //on click for facebook button
//                btFacebook_L.setFragment(this);
//                personalProfileArrayList=facebookSignInObj.onClickSignInButton(btFacebook_L);
//                if(personalProfileArrayList.size()>0)
//                Logger.logE(TAG,personalProfileArrayList.get(0).getEmail());
//                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        AuthNavigationHelper.getINSTANCE().performOnActivityResult(requestCode, resultCode, data);
//        if (requestCode == RC_SIGNUP) {
//         personalProfileArrayList=googleSignupObj.performTaskOnActivityResult(requestCode, resultCode, data);
//         if(personalProfileArrayList.size()>0)
//         {
//             Logger.logE(TAG,"personal profile info recd"+personalProfileArrayList.get(0).getEmail());
//             //get details from google profile and continue flow from here
//         }
//        }
//        else
//        facebookSignInObj.performTaskOnActivityResult(requestCode,resultCode,data);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
//            mIsPasswordInputError = false;
//            mIsEmailInputError = false;
//            onContinueOrDoneClick();
        }
        return false;
    }

    private void bindViews(View root) {

//        ((TextView)root.findViewById(R.id.tv_fb_btn_placeholder)).setTypeface(null,Typeface.BOLD);
//        ((TextView)root.findViewById(R.id.tv_google_btn_placeholder)).setTypeface(null,Typeface.BOLD);
//        ((TextView)root.findViewById(R.id.register_using_email)).setTypeface(null,Typeface.BOLD);
        ((TextView)root.findViewById(R.id.tv_or)).setTypeface(null,Typeface.BOLD);
        tiConfirmPassword = (TextInputLayout) root.findViewById(R.id.ti_confirm_password_R);
        tiConfirmPassword.setOnClickListener(this);

        tiEmail_L = (TextInputLayout) root.findViewById(R.id.ti_email_L);
        tiEmail_L.setOnClickListener(this);

        tiEmail_R = (TextInputLayout) root.findViewById(R.id.ti_email_R);
        tiEmail_R.setOnClickListener(this);

        tiFullUserName = (TextInputLayout) root.findViewById(R.id.ti_username);
        tiFullUserName.setOnClickListener(this);

        tiPassword_L = (TextInputLayout) root.findViewById(R.id.ti_password_L);
        tiPassword_L.setOnClickListener(this);

        tiPassword_R = (TextInputLayout) root.findViewById(R.id.ti_password_R);
        tiPassword_R.setOnClickListener(this);

        cb_eula = (CheckBox) root.findViewById(R.id.cb_eula);

        mEtEmail_R = (EditText) root.findViewById(R.id.et_email_R);
        mEtPassword_R = (EditText) root.findViewById(R.id.et_password_R);
        mEtFullName_R = (EditText) root.findViewById(R.id.et_fullname_R);
        mEtConfirmPassword_R = (EditText) root.findViewById(R.id.et_confirm_password_R);
        mEtEmail_L = (EditText) root.findViewById(R.id.et_email_L);
        mEtPassword_L = (EditText) root.findViewById(R.id.et_password_L);
        /*btGoogle_L=(SignInButton) root.findViewById(R.id.sign_in_button);
        btFacebook_L=(LoginButton) root.findViewById(R.id.login_button);
        btFacebook_L.setOnClickListener(this);
        btGoogle_L.setOnClickListener(this);*/

        mEtEmail_R.setOnFocusChangeListener(this);
        mEtPassword_R.setOnFocusChangeListener(this);
        mEtFullName_R.setOnFocusChangeListener(this);
        mEtConfirmPassword_R.setOnFocusChangeListener(this);
        mEtEmail_L.setOnFocusChangeListener(this);
        mEtPassword_L.setOnFocusChangeListener(this);

        mEtEmail_R.setOnEditorActionListener(this);
        mEtPassword_R.setOnEditorActionListener(this);
        mEtFullName_R.setOnEditorActionListener(this);
        mEtConfirmPassword_R.setOnEditorActionListener(this);
        mEtEmail_L.setOnEditorActionListener(this);
        mEtPassword_L.setOnEditorActionListener(this);


        mEtEmail_L.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        mEtEmail_R.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        mEtFullName_R.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        mEtPassword_R.setImeOptions(EditorInfo.IME_ACTION_NEXT);


        TextView mTvForgotPassword = (TextView) root.findViewById(R.id.tv_forgot_password);
        mTvForgotPassword.setTypeface(null,Typeface.BOLD);
        mTvForgotPassword.setOnClickListener(this);
        mTvForgotPassword.setText(Html.fromHtml( getString(R.string.registration_forgot_password)));

        root.findViewById(R.id.tv_already_have_account).setOnClickListener(this);
        root.findViewById(R.id.tv_dont_have_account).setOnClickListener(this);

        TextView mTvEula = (TextView) root.findViewById(R.id.tv_eula);
        setPrivacyAndEulaClickable(mTvEula);

        mEmailIcon_L = (ImageView) root.findViewById(R.id.email_icon_L);
        mEmailIcon_R = (ImageView) root.findViewById(R.id.email_icon_R);
        mPasswordIcon_L = (ImageView) root.findViewById(R.id.password_icon_L);
        mPasswordIcon_R = (ImageView) root.findViewById(R.id.password_icon_R);
        mConfirmPasswordIcon_R = (ImageView) root.findViewById(R.id.confirm_password_icon);
        mFullUserIcon_R = (ImageView) root.findViewById(R.id.fullname_icon);

        ((TextView)(root.findViewById(R.id.btn_login))).setTypeface(null,Typeface.BOLD);
        ((TextView)(root.findViewById(R.id.btn_signup))).setTypeface(null,Typeface.BOLD);
        ((TextView)(root.findViewById(R.id.register_using_email))).setTypeface(null,Typeface.BOLD);
        root.findViewById(R.id.btn_login).setOnClickListener(this);
        root.findViewById(R.id.btn_signup).setOnClickListener(this);
    }

    private void setPrivacyAndEulaClickable(TextView textView) {
        ClickableSpan eulaClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoEulaUrl();
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ClickableSpan privacyClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    gotoPrivacyUrl();
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        SpannableString ss = new SpannableString(getString(R.string.by_registering_you_aggre_to_the_eula));
        ss.setSpan(new UnderlineSpan(), 33, 66, 0);
        ss.setSpan(new UnderlineSpan(), 75, 90, 0);
        ss.setSpan(eulaClick, 33, 66, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(privacyClick, 75, 90, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    public void showErrorDialog(String errorMsg) {
        new FinjanDialogBuilder(this.getContext())
                .setMessage(errorMsg)
                .setPositiveButton("Ok")
                .show();
    }

    public void showPasswordError(String errorMessage) {
        EditText editText = showingRegister ? mEtPassword_R : mEtPassword_L;
        TextInputLayout ti = showingRegister ? tiPassword_R : tiPassword_L;
        if (showingRegister) {
            mEtConfirmPassword_R.setText("");
            mEtConfirmPassword_R.requestFocus();
            tiConfirmPassword.setError(errorMessage);
        }
        editText.setText("");
        editText.requestFocus();
        ti.setError(errorMessage);
        ti.setErrorEnabled(true);
    }

    public void showEmailError(String errorMessage) {
        EditText editText = showingRegister ? mEtEmail_R : mEtEmail_L;
        TextInputLayout ti = showingRegister ? tiEmail_R : tiEmail_L;

        editText.setText("");
        editText.requestFocus();
        ti.setError(errorMessage);
        ti.setErrorEnabled(true);
    }

    private void showFullNameError(String msg) {
        mEtFullName_R.setText("");
        mEtFullName_R.requestFocus();
        tiFullUserName.setError(msg);
        tiFullUserName.setErrorEnabled(true);
    }

    private void showReEnterError(String msg) {
        mEtConfirmPassword_R.setText("");
        mEtConfirmPassword_R.requestFocus();
        tiConfirmPassword.setError(msg);
        tiConfirmPassword.setErrorEnabled(true);
    }

    private boolean validateEmailField() {

        String email = (showingRegister ? mEtEmail_R : mEtEmail_L).getText().toString();
        if (!EmailConstraintChecker.isValidEmailFormat(email)) {
            showEmailError(getString(R.string.registration_invalid_email_format));
            mValidEmail = null;
            return false;
        }
        mValidEmail = email;
        return true;
    }

    private boolean validateFullName() {
        String fullName = mEtFullName_R.getText().toString();
        if (TextUtils.isEmpty(fullName) || fullName.length() < 5) {
            showFullNameError("Full Name must be at least 5 characters");
            mFullName = null;
            return false;
        }
        mFullName = fullName;
        return true;
    }

    private boolean validatePasswordField() {
        String password = "";
        if (showingRegister) {
            password = mEtPassword_R.getText().toString();

            MyAccPasswordConstraintChecker passwordChecker = new MyAccPasswordConstraintChecker();
            if (!passwordChecker.checkIfPasswordLengthSatisfyMinLength(password)) {
                showPasswordError(getString(R.string.registration_password_min));
                showErrorDialog(getString(R.string.registration_password_constraints));
                mValidPassword = null;
                return false;
            }
            if (!passwordChecker.checkIfPasswordContainUpperChar(password)) {
                showPasswordError(getString(R.string.registration_password_upper));
                mValidPassword = null;
                showErrorDialog(getString(R.string.registration_password_constraints));
                return false;
            }
            if (!passwordChecker.checkIfPasswordContainLogwerChar(password)) {
                showPasswordError(getString(R.string.registration_password_lower));
                mValidPassword = null;
                showErrorDialog(getString(R.string.registration_password_constraints));
                return false;
            }
            if (!passwordChecker.checkIfPasswordContainNumber(password)) {
                showPasswordError(getString(R.string.registration_password_latter));
                mValidPassword = null;
                showErrorDialog(getString(R.string.registration_password_constraints));
                return false;
            }

            String conFirmPassword = mEtConfirmPassword_R.getText().toString();
            if (!password.equals(conFirmPassword)) {
                showReEnterError(getString(R.string.password_and_confirm_password_not_matched));
                return false;
            }
            mValidPassword = password;
            return true;
        } else {
            password = mEtPassword_L.getText().toString();
            MyAccPasswordConstraintChecker passwordChecker = new MyAccPasswordConstraintChecker();
            if (!passwordChecker.checkIfPasswordLengthSatisfyMinLength(password)) {
                showPasswordError(getString(R.string.registration_password_min));
                mValidPassword = null;
                return false;
            }
            mValidPassword = password;
            return true;
        }
    }

    private boolean validateEulaCheckBox() {
        if (!cb_eula.isChecked()) {
            new FinjanDialogBuilder(getActivity()).setMessage(PlistHelper.getInstance().getEulaSkipMsgText())
                    .setPositiveButton("Ok").show();
        }
        return cb_eula.isChecked();
    }

    private void clearAllFocus() {
        mEtEmail_R.clearFocus();
        mEtPassword_R.clearFocus();
        mEtEmail_L.clearFocus();
        mEtPassword_L.clearFocus();
        mEtConfirmPassword_R.clearFocus();
        mEtFullName_R.clearFocus();
    }

    private void onClickLogin() {
        if (this.isAdded()) {
            clearAllFocus();
            // The check fields command will only be executed if it'a a register instance.
//        if (validateEmailField() && validatePasswordField()) {
            //Change to only email validation as asked by trello feedback 7-7-17 build
            if (validateEmailField()) {
                if (!showNoNetworkDialogIfNoConnectivity()) {
                    showProgressDialog(getString(R.string.QueryingInformationFromServer));
                    loginUser();
//                mProgressDialogUtil.show(getString(R.string.QueryingInformationFromServer));
                }
            }
        }
    }

    private void onClickRegister() {
        if (isAdded()) {
            clearAllFocus();

            // The check fields command will only be executed if it'a a register instance.
            if (validateFullName() && validateEmailField() && validatePasswordField() && validateEulaCheckBox()) {
//                if (!showNoNetworkDialogIfNoConnectivity()) {
                showProgressDialog(getString(R.string.QueryingInformationFromServer));
            //    signUpUser();
                verifyEmailAndSignup();


         /*       llTermsAndCondition.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_right));
                llTermsAndCondition.setVisibility(View.VISIBLE);
                rlRegister.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.enter_to_left));
                rlRegister.setVisibility(View.GONE);
                llBack.setEnabled(false);
                isTermsAndConditionScreen = true;*/

            }
        }
    }



    private void loginUser() {
        UserPref.getInstance().setEmailLogin(true);
        UserPref.getInstance().setLoginBy("email");
        GeneralPrefs.setEmailId(getContext(),mValidEmail);
        GeneralPrefs.setPassword(getContext(),mValidPassword);
        loginType= "email";
        AuthNavigationHelper.loginUser(getContext(),"email",mValidEmail,mValidPassword,this);
//        Authentication.loginEmail(getActivity(), mValidEmail, mValidPassword,
//                true, this);
    }

    private void signUpUser() {
        Authentication.registerUser(getActivity(),
                getUserAttributePayload(mValidEmail, mValidPassword, mFullName, emailVerified), basicApiListener);
    }

    private boolean isSecondTimeReg = false;
    private void verifyEmailAndSignup() {
        if (!"neverbounce".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationProvider())
                || "no".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption())) {
            emailVerified = null;
            signUpUser();
            return;
        }
        NetworkManager.getInstance(getContext()).hitNeverBounceAPI(getContext(), new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                Logger.logV("neverbounce",response.toString());
                if (!"valid".equalsIgnoreCase(response.optString("result"))){

                    if ("warning".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption()) && !isSecondTimeReg) {
                        isSecondTimeReg = true;
                        hideProgress();
                        new FinjanDialogBuilder(getActivity()).setMessage(PlistHelper.getInstance().getEmailValidationWarnMsg())
                                .setPositiveButton("Ok").show();
                        return;
                    }
                    else if ("yes".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption())) {
                        hideProgress();
                        new FinjanDialogBuilder(getActivity()).setMessage(PlistHelper.getInstance().getEmailValidationErrMsg())
                                .setPositiveButton("Ok").show();
                        return;
                    }

                }
                emailVerified = response.optString("result",null);
                signUpUser();
            }

            @Override
            public void executeOnError(VolleyError error) {
                Logger.logE("neverbounce",error.toString());
                if ("warning".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption()) && !isSecondTimeReg) {
                    isSecondTimeReg = true;
                    hideProgress();
                    new FinjanDialogBuilder(getActivity()).setMessage(PlistHelper.getInstance().getEmailValidationWarnMsg())
                            .setPositiveButton("Ok").show();
                    return;
                }
                else if ("yes".equalsIgnoreCase(PlistHelper.getInstance().getEmailValidationOption())) {
                    hideProgress();
                    new FinjanDialogBuilder(getActivity()).setMessage(PlistHelper.getInstance().getEmailValidationErrMsg())
                            .setPositiveButton("Ok").show();
                    return;
                }
                emailVerified = null;
                signUpUser();
            }
        },mValidEmail);
    }

    private UserAttributesPayload getUserAttributePayload(String email, String passoword, String fullName, String emailVerified) {
        UserAttributesPayload userAttributesPayload = new UserAttributesPayload();
        userAttributesPayload.setFirst_name(fullName);
        userAttributesPayload.setLast_name("");
        userAttributesPayload.setEmail(email);
        userAttributesPayload.setSource();
        userAttributesPayload.setPassword(passoword);
        userAttributesPayload.setEmail_verified(emailVerified);
        return userAttributesPayload;
    }

    private void showProgressDialog(final String msg) {
        if (NativeHelper.getInstnace().checkActivity(getActivity()) && this.isResumed()) {
            if(!this.isAdded()){return;}
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(getActivity(), R.style.VpnProgressDialog);
                    }
                    progressDialog.setMessage(msg);
                    if (AuthenticationFragment.this.isAdded() && AuthenticationFragment.this.isVisible()) {

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        progressDialog.show();
                    }
                }
            });
        }
    }

    private void hideProgress() {
        if (NativeHelper.getInstnace().checkActivity(getActivity())) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            });
        }
        progressDialog = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(getActivity(), true);
        hideProgress();
    }

    @Override
    public void onAuthSuccess(User user, Subscription subscription) {
    }

    private AuthNavigationHelper authNavigationHelper;

    @Override
    public void onAuthSuccess(LoginResponse_1 loginResponse_1) {
        if (currentScreen == null || currentScreen.equals(SetupAccountActivity.ACTION_SIGN_NAV)) {
            rlProgress.setVisibility(View.VISIBLE);
        }
        VpnActivity.UserLogout=false;
        VpnActivity.IsUserLoggin=true;
        Logger.logD(TAG, "authorization successful " + loginResponse_1.getAccess_token());
        AuthenticationHelper.getInstnace().initRefreshToken(loginResponse_1);
        NetworkManager.getInstance(getActivity()).listDevices(getActivity(), new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                rlProgress.setVisibility(View.GONE);
                HashMap<String, Device> deviceHashMap = JsonParser.getInstance().parseJsonAllDevice(getActivity(), response);
                String hardwareId = HardwareId.get(getContext());
                if (!(deviceHashMap != null && deviceHashMap.size() > 0 && deviceHashMap.containsKey(hardwareId))) {
                    DeviceUpdateHelper.addCurrentDevice(FinjanVPNApplication.getInstance().getApplicationContext(), listener);
                } else {
                    DeviceUpdateHelper.updateCurrentDevice(FinjanVPNApplication.getInstance().getApplicationContext(), listener);
                }
                Logger.logE(TAG, response.toString());
            }

            @Override
            public void executeOnError(VolleyError error) {
                rlProgress.setVisibility(View.GONE);
                String errorMsg = "";
                //updateLicence();
                if (error instanceof com.android.volley.TimeoutError ||
                        error instanceof com.android.volley.NoConnectionError
                        || error instanceof com.android.volley.ServerError) {
                    errorMsg = "Unable to connect, Try again after sometime";
                } else {
                    errorMsg = NativeHelper.getInstnace().getVollyErrorMsg(error);
                }
                Logger.logE(TAG, errorMsg);
                NativeHelper.getInstnace().showToast(getContext(), error);
            }
        });
    }

    /**
     * Event whenever Licence is fetched
     */
    @SuppressWarnings("unused")
    public void onEventMainThread(LicenceFetched event) {
        PromoHelper.Companion.getInstance().checkForUserValidity(this.getContext());
    }

    private void updateLicence() {
        if (getActivity() == null)
        {
            return;
        }
        VpnApisHelper.Companion.getInstance().updateLicence(false,true, getActivity(), new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                VpnApisHelper.Companion.getInstance().setCurrentLicense(null);
                SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(getContext(),false);
                authSuccess(true);
            }

            @Override
            public void executeOnError(VolleyError error) {
                authSuccess(true);
            }
        });

    }

    protected void authSuccess(boolean success) {
        if(success){fireLoginTarckers();}
        UserPref.getInstance().setTempPurchaseToken(null);
        EventBus.getDefault().post(new LoginSuccess());
        VpnActivity.UserLogout = !success;
        switch (mAction) {
            case SetupAccountActivity.ACTION_SIGN_NAV:
                mCallback.onFinishSetup(VpnActivity.RESULT_SIGNUP, success);
                break;
            case SetupAccountActivity.ACTION_SIGN_IN:
                Toast.makeText(FinjanVPNApplication.getInstance().getApplicationContext(), getString(R.string.sign_in_success), Toast.LENGTH_SHORT).show();
                mCallback.onFinishSetup(VpnActivity.REQUEST_LOGIN, success);
                break;
        }

        hideProgress();
        VpnActivity.IsUserLoggin=false;
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                VpnHomeFragment.userJustLogin=false;
            }
        },3000);
//        mProgressDialogUtil.dismiss();
    }

    private void fireRegisterTrackers(){
        Tracking.trackEvent(Tracking.EVENT_REGISTER);
        TuneTracking.getInstance().trackRegister(false);
        KochavaTracking.getInstance().trackRegister(false);
        GoogleTrackers.Companion.setERegistration();
        LocalyticsTrackers.Companion.setERegistration();
        FbTrackingConfig.getInstance().logRegistration(FinjanVPNApplication.getInstance());
    }
    private void fireLoginTarckers(){
        switch (loginType){
            case "fb":
                GoogleTrackers.Companion.setsFacebookLogin();
                break;
            case "gg":
                GoogleTrackers.Companion.setSGoogleLogin();
                break;
            case "email":
                GoogleTrackers.Companion.setSEmailLogin();
                break;
                default:GoogleTrackers.Companion.setSEmailLogin();
        }
        Tracking.trackEvent(Tracking.EVENT_LOGGEDIN);
        TuneTracking.getInstance().trackRegister(true);
        LocalyticsTrackers.Companion.setELogin();
        FbTrackingConfig.getInstance().logLogin(FinjanVPNApplication.getInstance());
//        GoogleTrackers.Companion.setELogin();
    }

    @Override
    public void onAuthError(int errorCode, String errorMessage, String request,String detailError) {
        rlProgress.setVisibility(View.GONE);
        hideProgress();
        // if there is an account with this email reveal the password field and ask for the account password
        if (errorCode == ServerErrorCodeProcessor.ACCOUNT_ALREADY_EXIST) {
            Toast.makeText(getActivity(), getString(R.string.txt_account_setup_existing), Toast.LENGTH_SHORT).show();
            revealPasswordField();
        } else if (errorCode == ServerErrorCodeProcessor.INVALID_EMAIL_PASSWORD) {
            Toast.makeText(getActivity(), getString(R.string.web_error_login_invalid_password), Toast.LENGTH_SHORT).show();
            showPasswordError(getString(R.string.registration_forgot_password));
        } else {
            CustomExceptionConfig.getInstance().logException("https://ulm.finjanmobile.com/oauth/token",
                    request, errorMessage+"----"+detailError ,UserLoginApiException.class.getSimpleName());

            Error error = ErrorCodeDescriptionMapper.getError(errorCode);
            if (error.equals(Error.UNKNOWN_ERROR) && showNoNetworkDialogIfNoConnectivity()) {
                return; // don't go further if the generated error was due to poor or no connectivity
            }
            new FinjanDialogBuilder(getContext()).setMessage(errorMessage).show();
        }
    }

    /**
     * @return false - the dialog was not shown => there is connectivity true - the dialog was shown => no connectivity
     */
    private boolean showNoNetworkDialogIfNoConnectivity() {
        if (!NetworkConnectionManager.hasNetworkConnectivity(getActivity())) {
            if (NativeHelper.getInstnace().checkActivity(getActivity())) {
                Toast.makeText(getActivity(), "Connect to a mobile or Wi-Fi network to continue.", Toast.LENGTH_LONG).show();

//                CommonDialogs.showNetworkUnavailable(getActivity());
                return true;
            }

        }
        return false;
    }

    /**
     * in case the email is already used for an avira account present the user the possibility to log in
     */
    private void revealPasswordField() {
        AuthNavigationHelper.getINSTANCE().switchAccountSetup(SetupAccountActivity.ACTION_SIGN_IN, false, false);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onResume() {
        super.onResume();
        licenFailedCount = 0;
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(getActivity(), false);
    }

    public void gotoPasswordRecoverUrl() {
        Intent intent = new Intent(AuthenticationFragment.this.getContext(), NormaWebviewActivity.class);
        intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_PASSWORD_RESET);
        startActivity(intent);
    }

    public void gotoEulaUrl() {

        Intent intent = new Intent(AuthenticationFragment.this.getContext(), NormaWebviewActivity.class);
        intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_EULA);
        startActivity(intent);
    }

    public void gotoPrivacyUrl() {

        Intent intent = new Intent(AuthenticationFragment.this.getContext(), NormaWebviewActivity.class);
        intent.putExtra(AppConstants.BKEY_SCREEN, DrawerConstants.DKEY_PRIVATE_PRIVACY);
        GoogleTrackers.Companion.setSPrivacy();
        startActivity(intent);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    private void setBackground(View view, int drawableResourceId) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(getResources().getDrawable(drawableResourceId));
        } else {
            view.setBackground(getResources().getDrawable(drawableResourceId));
        }
    }

    public interface AuthenticationListener {
        void onFinishSetup(int response, boolean success);
    }
}