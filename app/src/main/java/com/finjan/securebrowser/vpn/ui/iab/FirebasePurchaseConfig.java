package com.finjan.securebrowser.vpn.ui.iab;

import android.text.TextUtils;
import android.util.Log;

import com.avira.common.utils.SharedPreferencesUtilities;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.util.tracking.RemoteConfig;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Initialisation SKUs and mapping to MYA from Firebase Remote Config
 * <p>
 * Firebase Config Json may look like
 * [{
 * "mya": "vpna0",
 * "sku": "vpna0",
 * "type": "monthly",
 * "platform": "android"
 * }, {
 * "mya": "avpp0",
 * "sku": "avpp1",
 * "type": "monthly",
 * "platform": "all"
 * }, {
 * "mya": "avpp0",
 * "sku": "avpp0",
 * "type": "avpp0",
 * "platform": "all"
 * }]
 */

public class FirebasePurchaseConfig {
    public static final String TAG = FirebasePurchaseConfig.class.getSimpleName();
    private PurchaseConfig[] purchaseConfigsArray = new PurchaseConfig[0];

    private static final String PLATFORM_ANDROID_TAG = "android";
    private static final String PLATFORM_ALL_TAG = "all";

    private static final String TYPE_MONTHLY_TAG = "monthly";
    private static final String TYPE_YEARLY_TAG = "yearly";

    /**
     * read purchase config from a shared preferences
     */
    private void init() {
        String purchaseDetailsStr = SharedPreferencesUtilities.getString(FinjanVPNApplication.getInstance(),
                RemoteConfig.AVAILABLE_FOR_PURCHASE_TAG);
        if (TextUtils.isEmpty(purchaseDetailsStr)) {
            // probably it is a first start since
            Logger.logD(TAG, "there is nothing to fetch and initialize");
            return;
        }
        purchaseConfigsArray = new Gson().fromJson(purchaseDetailsStr, PurchaseConfig[].class);

    }

    /**
     * fetch SKU value and check if there is additional value for a country based sku
     */
    String getSkuAllDevicesYearly() {
        if (purchaseConfigsArray.length == 0) {
            init();
        }

        String sku = "";
        for (PurchaseConfig purchaseConfigItem : purchaseConfigsArray) {
            // filter for All platforms and Yearly renewal
            if (PLATFORM_ALL_TAG.equals(purchaseConfigItem.platform) && TYPE_YEARLY_TAG.equals(purchaseConfigItem.type)) {
                sku = purchaseConfigItem.sku;
            }
        }
        return sku;
    }

    /**
     * fetch SKU value and check if there is additional value for a country based sku
     */
    String getSkuAllDevicesMonthly() {
        if (purchaseConfigsArray.length == 0) {
            init();
        }
        String sku = "";
        for (PurchaseConfig purchaseConfigItem : purchaseConfigsArray) {
            // filter for All platforms and Monthly renewal
            if (PLATFORM_ALL_TAG.equals(purchaseConfigItem.platform) && TYPE_MONTHLY_TAG.equals(purchaseConfigItem.type)) {
                sku = purchaseConfigItem.sku;
            }
        }
        return sku;
    }

    /**
     * fetch SKU value and check if there is additional value for a country based sku
     */
    String getSkuThisDevicesMonthly() {
        if (purchaseConfigsArray.length == 0) {
            init();
        }
        String sku = "";
        for (PurchaseConfig purchaseConfigItem : purchaseConfigsArray) {
            // filter for Only Android platform and Monthly renewal
            if (PLATFORM_ANDROID_TAG.equals(purchaseConfigItem.platform) && TYPE_MONTHLY_TAG.equals(purchaseConfigItem.type)) {
                sku = purchaseConfigItem.sku;
            }
        }
        return sku;
    }

    /**
     * fetch SKU value and check if there is additional value for a country based sku
     */
    String getSkuThisDevicesYearly() {
        if (purchaseConfigsArray.length == 0) {
            init();
        }
        String sku = "";
        for (PurchaseConfig purchaseConfigItem : purchaseConfigsArray) {
            // filter for Only Android platform and Monthly renewal
            if (PLATFORM_ANDROID_TAG.equals(purchaseConfigItem.platform) && TYPE_YEARLY_TAG.equals(purchaseConfigItem.type)) {
                sku = purchaseConfigItem.sku;
            }
        }
        return sku;
    }

    /**
     * fetch MYA value based on SKU.
     */
    String getMYA(String sku) {
        if (purchaseConfigsArray.length == 0) {
            init();
        }
        String mya = "";
        for (PurchaseConfig purchaseConfigItem : purchaseConfigsArray) {
            if (sku.equals(purchaseConfigItem.sku)) {
                mya = purchaseConfigItem.mya;
            }
        }
        return mya;
    }

    /**
     * after new Firebase Remote Config will be downloaded old one should be removed
     */
    void clear() {
        purchaseConfigsArray = new PurchaseConfig[0];
    }

    class PurchaseConfig {
        @SerializedName("mya")
        public String mya;
        @SerializedName("sku")
        public String sku;
        @SerializedName("type")
        public String type;
        @SerializedName("platform")
        public String platform;
    }
}


