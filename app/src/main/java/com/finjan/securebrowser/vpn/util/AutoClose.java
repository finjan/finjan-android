package com.finjan.securebrowser.vpn.util;

import android.database.Cursor;
import androidx.annotation.Nullable;

import com.finjan.securebrowser.helpers.logger.Logger;

import java.io.Closeable;

/**
 * Use this class for safe and silent Closeable.close()
 */
public class AutoClose {

    public static final String LOG_TAG = AutoClose.class.getSimpleName();

    /**
     * Close Closeable silently
     * @param closeable
     */
    public static void closeSilently(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (Exception e) {
            Logger.logE(LOG_TAG, "can not close closeable"+ e);
            // do nothing
        }
    }

    /**
     * Use this method to close {@link Cursor}. <br>
     * In some of HTC devices {@link Cursor} does not implement {@link Closeable}
     * @param cursor to close silently
     */
    public static void closeSilently(@Nullable Cursor cursor) {
        try {
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            Logger.logE(LOG_TAG, "can not close cursor"+ e);
            // do nothing
        }
    }
}
