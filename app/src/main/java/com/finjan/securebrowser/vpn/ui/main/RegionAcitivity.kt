package com.finjan.securebrowser.vpn.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.finjan.securebrowser.R
import com.finjan.securebrowser.helpers.PlistHelper
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder
import com.finjan.securebrowser.vpn.FinjanVPNApplication
import com.finjan.securebrowser.vpn.util.TrafficController

/**
 * Created by anurag on 20/03/18.
 */
class RegionAcitivity:AppCompatActivity(){

    companion object {

        public const final val Region_Result=31
        fun  getInstance(activity: Activity){
            val intent= Intent(activity,RegionAcitivity::class.java)
            activity.startActivityForResult(intent, Region_Result)
        }
    }
    private lateinit var regionFragment:RegionFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vpn_drawer)
        overridePendingTransition(R.anim.enter_from_bottom, R.anim.enter_to_top)
        regionFragment=getFragment()
        supportFragmentManager.beginTransaction()
                .add(R.id.frame_container, regionFragment)
                .commitAllowingStateLoss()
    }
    private fun getFragment():RegionFragment{
        val countryList=RegionFragment.prepareCountryList(this);
        val allRegionList=RegionFragment.prepareAllLocaitonList(countryList)
        val newFragment = RegionFragment.getInstance(countryList,allRegionList, true)
        newFragment.setRegionSelectedListener { id, region, title, flag ->

            val intent=Intent()
            intent.putExtra("id",id)
            intent.putExtra("region",region)
            intent.putExtra("title",title)
            intent.putExtra("flag",flag)
            setResult(Region_Result,intent)
            finish()
        }
        return newFragment
    }

    override fun onBackPressed() {
        regionFragment.onBackPress()
    }

    override fun onStop() {
        super.onStop()
        overridePendingTransition(R.anim.enter_from_top,R.anim.enter_to_bottom)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}