package com.finjan.securebrowser.vpn.subscriptions;

/**
 * Created by anurag on 14/07/17.
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

import com.finjan.securebrowser.vpn.helpers.JsonConstans;
import com.finjan.securebrowser.vpn.licensing.utils.PurchaseHelper;
import com.finjan.securebrowser.vpn.ui.iab.LicenseUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * "subscription_id": "sub_AvAOMXRqR9OCje",
 * "name": "Barth Chuks",
 * "source": "apple",
 * "device_type": "all",
 * "created_at": "2017-06-27 13:48:25",
 * "expires_at": "2017-07-27 23:26:34",
 * "plan_interval": "month",
 * "amount": "800",
 * "renewed_at": null,
 * "status": "running",
 * "cancelled": null
 */
public class Subscriptions extends JsonConstans {
    private static final String TAG = Subscriptions.class.getSimpleName();
    private String subscription_id;
    private String name;
    private String source;
    private String device_type;
    private String created_at;
    private String expires_at;
    private String plan_interval;
    private String amount;
    private String renewed_at;
    private String status;
    private String cancelled;
    private String type;
    private String id;
    private String comp_flg;

    public String getComp_flg() {
        return comp_flg;
    }

    public void setComp_flg(String comp_flg) {
        this.comp_flg = comp_flg;
    }

    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    private String sku_id;

    public Subscriptions(JSONObject jsonObject) {
        setModel(jsonObject);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(String subscription_id) {
        this.subscription_id = subscription_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getExpires_at() {
        return expires_at;
    }

    public void setExpires_at(String expires_at) {
        this.expires_at = expires_at;
    }

    public String getPlan_interval() {
        return plan_interval;
    }

    public void setPlan_interval(String plan_interval) {
        this.plan_interval = plan_interval;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRenewed_at() {
        return renewed_at;
    }

    public void setRenewed_at(String renewed_at) {
        this.renewed_at = renewed_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }


    private void setModel(JSONObject jsonObject) {
        if(jsonObject!=null){
            try {
                jsonObject=jsonObject.getJSONObject(ATTRIBUTES);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        setCancelled(jsonObject.optString(CANCELLED));
        setCreated_at(jsonObject.optString(CREATED_AT));
        setDevice_type(jsonObject.optString(DEVICE_TYPE));
        setId(jsonObject.optString(ID));
        setName(jsonObject.optString(NAME));
        setExpires_at(jsonObject.optString(EXPIRES_AT));
        setPlan_interval(jsonObject.optString(PLAN_INTERVAL));
        setRenewed_at(jsonObject.optString(RENEWED_AT));
        setSource(jsonObject.optString(SOURCE));
        setStatus(jsonObject.optString(STATUS));
        setSubscription_id(jsonObject.optString(TRANSACTION_ID));
        setType(jsonObject.optString(TYPE));
        setAmount(jsonObject.optString(AMOUNT));
        setSku_id(calculateSku_id(jsonObject.optString(AMOUNT)));
        setComp_flg(jsonObject.optString(COMP_FLAG));
    }

    private String calculateSku_id(String amount){
        return PurchaseHelper.getSkuUponAmount(amount);

//        if(getDevice_type().equals(ANDROID)){
//            if(getPlan_interval().equals(MONTH)){
//                return LicenseUtil.getSkuThisDevicesMonthly();
//            }else {
//                return LicenseUtil.getSkuAllDevicesYearly();
//            }
//        }else {
////            if(getPlan_interval().equals(MONTH)){
////                return LicenseUtil.getSkuAllDevicesMonthly();
////            }else {
////                return LicenseUtil.getSkuAllDevicesYearly();
////            }
//        }
    }
}
