package com.finjan.securebrowser.vpn.licensing.models.restful;

import android.text.TextUtils;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * License resource model
 * <br />
 * contains various helper method to retrieve the useful information from the complicated underlying module
 *
 * @author ovidiu.buleandra
 * @since 05.11.2015
 */
public class License extends Resource implements Comparable<License> {

    public static final SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    public static final SimpleDateFormat iso8601format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);

    public String getUserId() {
        if (relationships != null) {
            Resource user = relationships.getUserData();
            if (user != null) {
                return user.getId();
            }
        }
        return null;
    }

    public String getDeviceId() {
        if(relationships != null) {
            Resource device = relationships.getDeviceData();
            if(device != null) {
                return device.getId();
            }
        }
        return null;
    }

    public String getProductAcronym() {
        if (relationships != null) {
            Resource appResource = relationships.getAppData();
            if (appResource != null) {
                return appResource.getId();
            }
        }
        return null;
    }

    @Override
    public int compareTo(License another) {
        if (attributes != null && another.attributes != null) {
            int value = licenseTypeValue(attributes.getType());
            int anotherValue = licenseTypeValue(another.attributes.getType());
            if (value > anotherValue) {
                return 1;
            } else if (value == anotherValue) {
                Date expirationDate;
                Date anotherExpirationDate;
                try {
                    expirationDate = iso8601format.parse(attributes.getExpirationDate());
                } catch (ParseException | NullPointerException e) {
                    expirationDate = new Date(0);
                }
                try {
                    anotherExpirationDate = iso8601format.parse(another.attributes.getExpirationDate());
                } catch (ParseException | NullPointerException e) {
                    anotherExpirationDate = new Date(0);
                }

                return expirationDate.compareTo(anotherExpirationDate);
            } else {
                return -1;
            }
        }

        return 0;
    }

    private int licenseTypeValue(String type) {
        switch (type) {
            case "paid":
                return 2;
            case "eval":
                return 1;
            case "free":
                return 0;
            default:
                return -1;
        }
    }

    public String getLicenseType() {
        if (attributes != null) {
            return attributes.getType();
        }
        return null;
    }

    public String getExpirationDateString() {
        if (attributes != null) {
            return attributes.getExpirationDate();
        }
        return null;
    }

    public Date getExpirationDate() {
        if (attributes != null) {
            try {
                return iso8601format.parse(attributes.getExpirationDate());
            } catch (ParseException | NullPointerException e) {
                // ignore
            }
        }
        return null;
    }

    /**
     * removes the key from attributes since is not used and occupies too much space
     */
    public void clearKey() {
        if(attributes != null) {
            attributes.setKey("");
        }
    }

    @Override
    public String toString() {
        return String.format("License{product='%s', type='%s', expirationDate='%s'}",
                getProductAcronym(),
                getLicenseType(),
                getExpirationDateString());
    }

    public void adjustExpirationDate() {
        if(attributes != null) {
            String expDate = attributes.getExpirationDate();
            if(!TextUtils.isEmpty(expDate)) {
                try {
                    Date res = License.serverFormat.parse(expDate);
                    res.setTime(res.getTime() + DateUtils.DAY_IN_MILLIS - DateUtils.SECOND_IN_MILLIS);
                    attributes.setExpirationDate(License.iso8601format.format(res));
                } catch (ParseException e) {
                    // do nothing
                }
            }
        }
    }
}
