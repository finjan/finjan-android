package com.finjan.securebrowser.vpn.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;

import com.finjan.securebrowser.helpers.logger.Logger;

import java.util.List;

/**
 * Created by hui-joo.goh on 10/05/16.
 */
public class ConnectionUtil {

    private static final String TAG = ConnectionUtil.class.getSimpleName();
    private static final String DOUBLE_QUOTE = "\"";

    /**
     * https://code.google.com/p/android/issues/detail?id=43336
     */
    private static final String UNKNOWN_SSID = "<unknown ssid>";

    /**
     * Get the currently connected wifi ssid.
     *
     * @param context
     * @return The currently connected wifi ssid. Returns null if none.
     */
    public static String getConnectedWifiSsid(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();
        String ssid = null;

         if (activeNetwork != null && activeNetwork.isConnected()
                && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {

             // get connected ssid name
             WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
             if (wifiManager != null) {
                 WifiInfo info = wifiManager.getConnectionInfo();

                 if (info != null) {
                     ssid = info.getSSID().replace(DOUBLE_QUOTE, "");
                     ssid = (UNKNOWN_SSID.equals(ssid) ? null : ssid);
                 }
             }
        }

        Logger.logD(TAG, "getConnectedWifiSsid " + ssid);
        return ssid;
    }

    /**
     * Check if the specified wifi ssid is a secure network.
     * 
     * ATTENTION: This method requires LOCATION permission in order to access wifi scan results.
     *
     * @param context
     * @param ssid The wifi ssid to be checked. It has to be an active ssid that is currently detectable by the device.
     * @return True if the specified wifi is a secure network, otherwise false.
     */
    public static boolean isWifiSecure(Context context, String ssid) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        boolean isSecure = true;

        if (wifiManager != null && !TextUtils.isEmpty(ssid)) {

            // get wifi ssid found in the most recent scan
            List<ScanResult> scanResults = wifiManager.getScanResults();
            if (scanResults != null) {
                for (ScanResult network : scanResults) {

                    // check capabilities of specified wifi
                    if (ssid.equals(network.SSID)) {
                        String capabilities = network.capabilities;
                        isSecure = capabilities.contains("WPA2");
//                                || capabilities.contains("WPA") || capabilities.contains("WEP");

                        Logger.logD(TAG, ssid + " contains WPA2? " + capabilities.contains("WPA2")
                                + "\ncontains WPA? " + capabilities.contains("WPA")
                                + "\ncontains WEP? " + capabilities.contains("WEP"));
                        break;
                    }
                }
            }
        }

        Logger.logD(TAG, ssid + " isWifiSecure? " + isSecure);
        return isSecure;
    }
}
