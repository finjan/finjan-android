/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.controller;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.vpn.auto_connect.AutoConnectHelper;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
import com.finjan.securebrowser.vpn.ui.main.LaunchAviraVpnActivity;
import com.finjan.securebrowser.vpn.util.log.FileLogger;

import java.io.IOException;

public class VpnWorkflow {
    public static final String TAG = VpnWorkflow.class.getSimpleName();

    /**
     * Start vpn connection configuration
     * @param context
     * @param serverVpnId id of a vpn server to connect
     * @return true if start was successful, false otherwise
     */
    public boolean run(@NonNull Context context, long serverVpnId, boolean autoConnection) {
        String uuid = configure(context, serverVpnId);

        if(UserPref.getInstance().getIsUserHadGivenVPNPermission() && autoConnection){
            if(!TextUtils.isEmpty(uuid)){
                AutoConnectHelper.Companion.getInstance().initVpn(context,uuid);
                return true;
            }else {
                return false;
            }
        }else {
            if (!TextUtils.isEmpty(uuid)) {
                startVPN(context, uuid);
                return true;
            } else {
                Logger.logE(TAG, "VPN configuration failed uuid is empty or null");
                return false;
            }
        }
    }

    /**
     * prepare all connection information in VPN SocialUserProfile object.
     * @param context
     * @param serverVpnId id of a selected vpn server
     * @return selected Vpn SocialUserProfile uuid
     */
    @Nullable
    private String configure(@NonNull Context context, long serverVpnId) {
        String uuid = null;
        try {
            uuid = new ContentProviderClient().getServerUuid(context, serverVpnId);
            if (TextUtils.isEmpty(uuid)) {
                ConnectionConfig connectionConfig = new ConnectionConfig();
                uuid = connectionConfig.processConfig(context, serverVpnId);
            }
        } catch (IOException e) {
            Logger.logE(TAG, "VPN configuration failed"+ e);
        }
        return uuid;
    }

    /**
     * Activate Launch vpn logic
     * @param context
     * @param uuid
     */
    private void startVPN(Context context, String uuid) {
        FileLogger.logToFile(VpnWorkflow.class, "startVPN uuid " + uuid);
        Intent intent = new Intent(context, LaunchAviraVpnActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(LaunchAviraVpnActivity.EXTRA_KEY, uuid);
        intent.setAction(Intent.ACTION_MAIN);
        context.startActivity(intent);
    }
}
