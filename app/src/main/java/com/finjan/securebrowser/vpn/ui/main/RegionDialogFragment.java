package com.finjan.securebrowser.vpn.ui.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.database.AviraVpnContentProvider;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
import com.finjan.securebrowser.vpn.controller.database.DatabaseContract;
import com.finjan.securebrowser.vpn.controller.model.RegionModel;
import com.finjan.securebrowser.vpn.ui.authentication.SetupAccountActivity;
import com.finjan.securebrowser.vpn.util.AutoClose;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.TrafficController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Illia Klimov on 11/26/2015.
 */
public class RegionDialogFragment extends DialogFragment implements LoaderManager.LoaderCallbacks<Cursor>,View.OnClickListener {

    public static final String TAG = RegionDialogFragment.class.getSimpleName();
    protected static int LOADER_ID = AviraVpnContentProvider.getUniqueLoaderId();
    List<RegionModel> mRegionList = new ArrayList<>();
    private RegionAdapter mAdapter;
    private String selectedServer;

    public void setSelectedServer(String selectedServer){
        this.selectedServer=selectedServer;
    }
    public interface RegionSelectedLinstener{
        void regionSelected(VPNServer selectedServer);
    }
    public RegionDialogFragment(){}
    private RegionSelectedLinstener linstener;

    @SuppressLint("ValidFragment")
    public RegionDialogFragment(RegionSelectedLinstener regionSelectedLinstener){
        this.linstener=regionSelectedLinstener;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }
    private TextView tvDone,tvUpdrade,tvCancel;
    private LinearLayout ll_two_buttons;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        getDialog().setTitle(R.string.region_dialog_title);
//        setUpgradeBtnVisibility();
        View view = inflater.inflate(R.layout.fragment_dialog_region, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getDialog().getWindow();
        if (window != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(ResourceHelper.getInstance().getColor(R.color.transparent)));

            WindowManager.LayoutParams windowParams = window.getAttributes();
            windowParams.dimAmount = 0.50f;
            windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(windowParams);


            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            window.setLayout((7*width)/8, RelativeLayout.LayoutParams.WRAP_CONTENT);
        }

//        if(getDialog().getWindow()!=null){
//        }
        getDialog().setCanceledOnTouchOutside(false);

//        if (TrafficController.getInstance(getActivity()).isPaid()) {
//            view.setBackgroundColor(Util.getColor(getActivity(), R.color.pro_layout_background_color));
//        }
        ListView listView =(ListView) view.findViewById(R.id.regionList);
        tvCancel=(TextView)view.findViewById(R.id.tv_cancel);
        tvUpdrade=(TextView)view.findViewById(R.id.tv_upgrade);
        tvDone=(TextView)view.findViewById(R.id.tv_reginal_dialog_done);
        ll_two_buttons=(LinearLayout) view.findViewById(R.id.ll_two_buttons);
        tvCancel.setOnClickListener(this);
        tvUpdrade.setOnClickListener(this);
        tvDone.setOnClickListener(this);

        mAdapter = new RegionAdapter(getActivity(), getRegionsList(getActivity()),this);
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RegionModel item = mAdapter.getItem(position);

                VpnHomeFragment mainActivityFragment = getMainActivityFragment();
                if (mainActivityFragment != null) {
                    if(item!=null){
                        mainActivityFragment.setSelectedRegion(item.getId(), item.getRegion(), item.getTitle(),
                                0);
                    }
                    List<RegionModel> list = getRegionsList(getContext());
                    for (int i = 0; i < list.size(); i++) {
                        if (i == position) {
                            list.get(i).setSelected(true);
                        } else {
                            list.get(i).setSelected(false);
                        }
                    }
                    mAdapter.notifyAdapter(list);
//                    RegionDialogFragment.this.dismiss();
                }
            }
        });
        return view;
    }
    private VPNServer vpnServer;
    protected void refreshDialogList(int position){
        RegionModel item = mAdapter.getItem(position);

        VpnHomeFragment mainActivityFragment = getMainActivityFragment();
        if (mainActivityFragment != null) {
            if(item!=null)
            vpnServer=new VPNServer(item.getId(), item.getRegion(), item.getTitle());
            List<RegionModel> list = getRegionsList(getContext());
            for (int i = 0; i < list.size(); i++) {
                if (i == position) {
                    list.get(i).setSelected(true);
                } else {
                    list.get(i).setSelected(false);
                }
            }
            mAdapter.notifyAdapter(list);
//                    RegionDialogFragment.this.dismiss();
        }
    }


    public void initRegions(Context context) {
        if (mAdapter != null) {
            mAdapter.clear();
            List<RegionModel> regionsList = getRegionsList(context);
            mAdapter.addAll(regionsList);
            mAdapter.notifyDataSetChanged();
        }
    }

    private List<RegionModel> getRegionsList(Context context) {
        String serverRegion = FinjanVpnPrefs.getLastUsedServer(getContext());
        long servicerId=0;
        String serverTitle=null;
        if (!TextUtils.isEmpty(serverRegion)) {
            ContentProviderClient contentProviderClient = new ContentProviderClient();
            Cursor cursor = contentProviderClient.getServer(getContext(), serverRegion);
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    servicerId= cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID));
                    serverTitle = cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.NAME));
                }
                AutoClose.closeSilently(cursor);
            }
        }
        if(TextUtils.isEmpty(serverTitle)){
            serverTitle="";
        }

        Cursor cursor;
        cursor = new ContentProviderClient().getAllServers(context);

        if (cursor != null) {
            RegionModel regionModel;
            while (cursor.moveToNext()) {
                regionModel = new RegionModel();
                regionModel.setId(cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID)));
                regionModel.setRegion(cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.REGION)));
                regionModel.setTitle(cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.NAME)));

                if(regionModel.getTitle().equalsIgnoreCase(serverTitle )){
                    regionModel.setSelected(true);
                }else {
                    regionModel.setSelected(false);
                }
                mRegionList.add(regionModel);
            }

            AutoClose.closeSilently(cursor);
        } else {
            Logger.logE(TAG, "Database request failed");
        }

        return mRegionList;
    }

    private VpnHomeFragment getMainActivityFragment() {
        return (VpnHomeFragment) getParentFragment();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), AviraVpnContentProvider.getServerUri(),
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (isAdded()) {
            initRegions(getActivity());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
    class VPNServer{
        Long id;
        String region,title;
        VPNServer(Long id,String region,String title){
            this.id=id;this.region=region;this.title=title;
        }
        VPNServer(RegionModel regionModel){
            this.id=regionModel.getId();this.region=regionModel.getRegion();
            this.title=regionModel.getTitle();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_upgrade:
                onClickUpgrade();
                break;
            case R.id.tv_reginal_dialog_done:
                onClickDone();
                break;
            case R.id.tv_cancel:
                onClickCancel();
                break;
        }
    }
    private void onClickUpgrade(){
        if(TrafficController.getInstance(FinjanVPNApplication.getInstance().getApplicationContext()).isRegistered()){
            getMainActivityFragment().onClickUpgrade(false,false);
        }else {
            SetupAccountActivity.newLoginInstanceForResult(getActivity(), VpnActivity.REQUEST_Login_For_Region, SetupAccountActivity.ACTION_SIGN_UP);    }
        }
    private void onClickCancel(){
        RegionDialogFragment.this.dismiss();
    }

    private void onClickDone(){
        if(TrafficController.getInstance(FinjanVPNApplication.getInstance().getApplicationContext()).isPaid()){
            onClickCancel();
            linstener.regionSelected(vpnServer);
        }else {
            new FinjanDialogBuilder(getContext())
                    .setMessage(PlistHelper.getInstance().getVpnLocationChangeAlert())
                    .setPositiveButton(PlistHelper.vpnLocationChangeAlertOkBtn)
                    .setNegativeButton(PlistHelper.vpnLocationChangeAlertCBtn)
                    .setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                        @Override
                        public void onNegativeClick() {
                            if(mRegionList!=null && mRegionList.size()>0)
                            linstener.regionSelected(new VPNServer(mRegionList.get(0)));
                            onClickCancel();
                        }
                    }).setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                @Override
                public void onPositivClick() {
                    onClickCancel();
                    onClickUpgrade();
                }
            }).show();
        }



    }

    private void setUpgradeBtnVisibility(){
        if(TrafficController.getInstance(FinjanVPNApplication.getInstance().getApplicationContext()).isPaid()){
            ll_two_buttons.setVisibility(View.GONE);
            tvDone.setVisibility(View.VISIBLE);
        }else {
            ll_two_buttons.setVisibility(View.VISIBLE);
            tvDone.setVisibility(View.GONE);
        }
    }
}