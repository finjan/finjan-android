package com.finjan.securebrowser.vpn.ui.promo

import android.graphics.Typeface
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.finjan.securebrowser.activity.ParentActivity
import com.finjan.securebrowser.R
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper


class RedeemOffer:ParentActivity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_redeem_offer)
    }

    override fun onResume() {
        super.onResume()
        setRedeemOffer()
    }

    fun setRedeemOffer(){
        if(intent!=null && !TextUtils.isEmpty(intent.getStringExtra("redeem_offer"))){
            (findViewById<EditText>(R.id.et_redeem_offer)).setText(intent.getStringExtra("redeem_offer"))
        }
    }
    fun setDefaults(){

//        vg_loader.setVisibility(View.VISIBLE);
        (findViewById<View>(R.id.iv_backButton) as ImageView)
                .setColorFilter(ResourceHelper.getInstance().getColor(R.color.white))
        findViewById<View>(R.id.tv_title).visibility = View.GONE
        (findViewById<View>(R.id.ll_back) as LinearLayout).setOnClickListener {
            onBackPressed()
        }
    }
    fun hitCheckValidity(){

    }
}