/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.finjan.securebrowser.vpn.controller.database;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.finjan.securebrowser.vpn.util.AutoClose;

public class ContentProviderClient {
    private static final String LOG_TAG = ContentProviderClient.class.getSimpleName();

    public Uri insertServer(@NonNull Context context, @NonNull String name, @NonNull String region, @NonNull String host, @NonNull String port, @NonNull String protocol) {

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.ServerTable.NAME, name);
        values.put(DatabaseContract.ServerTable.HOST, host);
        values.put(DatabaseContract.ServerTable.PORT, port);
        values.put(DatabaseContract.ServerTable.PROTOCOL, protocol);
        values.put(DatabaseContract.ServerTable.REGION, region);
        return context.getContentResolver().insert(AviraVpnContentProvider.getServerUri(), values);
    }

    @Nullable
    public Cursor getAllServers(@NonNull Context context) {
        ContentResolver cr = context.getContentResolver();
        return cr.query(AviraVpnContentProvider.getServerUri(), null, null, null, null);
    }

    @Nullable
    public Cursor getServer(@NonNull Context context, long serverId) {
        ContentResolver cr = context.getContentResolver();
        return cr.query(AviraVpnContentProvider.getSingleServerUri(serverId), null, null, null, null);
    }

    @Nullable
    public Cursor getServer(@NonNull Context context, String region) {
        ContentResolver cr = context.getContentResolver();
        String[] selectionArg = {region};
        return cr.query(AviraVpnContentProvider.getServerUri(), null, DatabaseContract.ServerTable.REGION + "=?", selectionArg, null);
    }

    public int updateServerVpnUuid(@NonNull Context context, long serverVpnId, @NonNull String uuid) {
        ContentResolver cr = context.getContentResolver();
        String selection = DatabaseContract.ServerTable._ID + "=?";
        String[] selectionArgs = {String.valueOf(serverVpnId)};
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.ServerTable.UUID, uuid);
        return cr.update(AviraVpnContentProvider.getServerUri(), values, selection, selectionArgs);
    }

    @Nullable
    public String getServerUuid(@NonNull Context context, long serverId) {
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(AviraVpnContentProvider.getSingleServerUri(serverId), new String[]{DatabaseContract.ServerTable.UUID}, null, null, null);

        String uuid = null;
        if (cursor != null) {
            if (cursor.moveToNext()) {
                uuid = cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.UUID));
            }

            AutoClose.closeSilently(cursor);
        }

        return uuid;
    }

    /**
     * Get the unique server id of the specified region.
     * @param context
     * @param serverRegion The server region
     * @return The unique server id of the specified region if exists, otherwise -1
     */
    public long getServerId(Context context, String serverRegion) {
        long serverId = -1;

        if (!TextUtils.isEmpty(serverRegion)) {
            Cursor cursor = getServer(context, serverRegion);
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    serverId = cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID));
                }
                AutoClose.closeSilently(cursor);
            }
        }
        return serverId;
    }

    public void removeAllServers(@NonNull Context context) {
        ContentResolver cr = context.getContentResolver();
        cr.delete(AviraVpnContentProvider.getServerUri(), null, null);
    }
}
