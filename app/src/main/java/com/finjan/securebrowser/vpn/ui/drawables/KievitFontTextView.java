package com.finjan.securebrowser.vpn.ui.drawables;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.finjan.securebrowser.vpn.FinjanVPNApplication;

/**
 * Created by Illia.Klimov on 12/4/2015.
 */
public class KievitFontTextView extends TextView {

    public KievitFontTextView(Context context) {
        super(context, null, 0);
        init(null);
    }

    public KievitFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        init(attrs);
    }

    public KievitFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
//			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
//			String fontName = a.getString(R.styleable.MyTextView_fontName);
//			if (fontName!=null) {
//				Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/"+fontName);
//				setTypeface(myTypeface);
//			}
//			a.recycle();
        }
        if (!isInEditMode()) {
            setTypeface(FinjanVPNApplication.Fonts.KIEVIT_LIGHT.getTypeface());
        }
    }
}
