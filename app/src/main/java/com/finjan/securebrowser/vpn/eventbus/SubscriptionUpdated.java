package com.finjan.securebrowser.vpn.eventbus;

import com.finjan.securebrowser.model.RedeemListData;

/**
 * Created by anurag on 03/08/17.
 */

public class SubscriptionUpdated {
    public boolean isPromoPurchase,start,isToShowPromoEndPopUp;
    public String promoId;
    public SubscriptionUpdated(){}
    public SubscriptionUpdated(String promoId, boolean start,boolean isToShowPromoEndPopUp){
        this.isPromoPurchase = true;
        this.start = start;
        this.promoId= promoId;
        this.isToShowPromoEndPopUp= isToShowPromoEndPopUp;
    }
}
