package com.finjan.securebrowser.vpn.controller;

import android.content.Context;
import android.text.TextUtils;

import com.finjan.securebrowser.vpn.controller.model.Device;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by anurag on 16/06/17.
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class JsonParser {

    private static final class JsonParserHolder{
        private static final JsonParser Instance=new JsonParser();
    }
    private JsonParser(){}
    public static JsonParser getInstance(){return JsonParserHolder.Instance;}

    public HashMap<String,Device> parseJsonAllDevice(Context context, JSONObject jsonObject){

        HashMap<String,Device> deviceHashMap=new HashMap<>();
        try {
            JSONArray data=jsonObject.getJSONArray("data");
            for (int i=0;i<data.length();i++){
                Device device=getDevice(data.getJSONObject(i));
                if(device!=null){
                    deviceHashMap.put(device.getDevice_id(),device);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return deviceHashMap;
    }
    private Device getDevice(JSONObject jsonObject){
        Device device=new Device();
        try {
            device.setId(jsonObject.getString("id"));
            device.setType(jsonObject.getString(TYPE));
            JSONObject attribute=jsonObject.getJSONObject(ATTRIBUTES);
            device.setDevice_name(attribute.getString(DEVICE_NAME));
            device.setType(attribute.getString(TYPE));
            device.setDevice_id(attribute.getString(DEVICE_ID));
            device.setStatus(attribute.getString(STATUS));
            device.setIp_address(attribute.getString(IP_ADDRESS));
            device.setLast_seen_date(attribute.getString(LAST_SEEN_DATE));
            device.setRegistration_date(attribute.getString(REGISTRATION_DATE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return device;
    }
    public String getMsgFromError(JSONObject jsonObject){
        String msg="";
        if(jsonObject.has(MESSAGE)){
            try {
                msg=jsonObject.getString(MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return TextUtils.isEmpty(msg)?"":msg;
    }
    private static final String DEVICE_NAME="device_name";
    private static final String DEVICE_ID="device_id";
    private static final String TYPE="type";
    private static final String IP_ADDRESS="ip_address";
    private static final String ATTRIBUTES="attributes";
    private static final String DATA="data";
    private static final String STATUS="status";
    private static final String REGISTRATION_DATE="registration_date";
    private static final String LAST_SEEN_DATE="last_seen_date";
    private static final String MESSAGE="message";


    public String getIpAdressLocation(JSONObject jsonObject){
        try {
            String regionName= jsonObject.getString("regionName");
            String city= jsonObject.getString("city");
            String zip= jsonObject.getString("zip");
            String lat= jsonObject.getString("lat");
            String lon= jsonObject.getString("lon");

            return regionName+", "+city+" "+zip+" with lat, long "+lat+","+lon;
        } catch (JSONException e) {
            e.printStackTrace();
            return "unable to fetch location";
        }
    }
}
