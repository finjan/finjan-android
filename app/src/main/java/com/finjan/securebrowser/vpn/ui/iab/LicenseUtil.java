package com.finjan.securebrowser.vpn.ui.iab;

import androidx.annotation.Nullable;

/**
 * utils class for checking if a feature is enabled or not, trial verification
 * <br /> this is the only place were the mapping from google skus to mya products exists
 *
 * @author Illia.Klimov on 15.02.2016.
 */
public class LicenseUtil {

    private static final String TAG = LicenseUtil.class.getSimpleName();

    static final String SKU_TEST_PURCHASED = "vpn_monthly_single_device";
    static final String SKU_DEVELOP_SUBSCRIPTION = "vpntest1";
    public static final String SKU_DEVELOP_CONSUMABLE = "vpn_test_consumable";

    static String DEBUG_SELECTED_SKU = SKU_TEST_PURCHASED;
    private static FirebasePurchaseConfig purchaseConfig = new FirebasePurchaseConfig();

//    static String generatePurchaseExtraInfo(String googleEmail) {
//        UserProfile userProfile = UserProfile.load();
//        PurchaseExtraInfo extraInfo = new PurchaseExtraInfo(
//                userProfile != null ? userProfile.getEmail() : "",
//                googleEmail
//        );
//        return Base64.encodeToString(new Gson().toJson(extraInfo).getBytes(), Base64.DEFAULT);
//    }

    public static String getActualSku(String sku){
        if(sku.contains("finjan_vpn_monthly_multiple_device") ||
                sku.contains("finjan_vpn_yearly_multiple_device")){
            return sku;
        }
        if(sku.contains("finjan_vpn_monthly")){
            sku = sku.replace("finjan_vpn_monthly","finjan_vpn_monthly_multiple_device");
        }
        if(sku.contains("finjan_vpn_annual")){
            sku = sku.replace("finjan_vpn_annual","finjan_vpn_yearly_multiple_device");
        }
        return sku;
    }
    public static final String getSkuAllDevicesYearly25() {
        return "finjan_vpn_yearly_multiple_device_25off";
//        return "finjan_vpn_monthly_25off";
    }
    public static String getSkuAllDevicesYearly33() {
        return "finjan_vpn_yearly_multiple_device_33off";
//        return "finjan_vpn_monthly_33off";
    }
    public static String getSkuAllDevicesYearly50() {
        return "finjan_vpn_yearly_multiple_device_50off";
//        return "finjan_vpn_monthly_50off";
    }
    public static String getSkuAllDevicesMonthly25() {
        return "finjan_vpn_monthly_multiple_device_25off";
//        return "finjan_vpn_annual_25off";
    }
    public static String getSkuAllDevicesMonthly33() {
        return "finjan_vpn_monthly_multiple_device_33off";
//        return "finjan_vpn_annual_33off";
    }
    public static String getSkuAllDevicesMonthly50() {
        return "finjan_vpn_monthly_multiple_device_50off";
//        return "finjan_vpn_annual_50off";
    }
    public static String getSkuAllDevicesYearly75() {
        return "finjan_vpn_yearly_multiple_device_750ff";
//        return "finjan_vpn_monthly_50off";
    }
    public static String getSkuAllDevicesMonthly75() {
        return "finjan_vpn_monthly_multiple_device_750ff";
//        return "finjan_vpn_annual_50off";
    }
    public static String getSkuAllDevicesYearly() {
//        return "yearly_subscription_5.99";
        return "finjan_vpn_yearly_multiple_device";
//        return "finjan_vpn_yearly_multiple_device_750ff";
//        return purchaseConfig.getSkuAllDevicesYearly();
    }

    public static String getSkuAllDevicesMonthly() {
//        return purchaseConfig.getSkuAllDevicesMonthly();
//        return "monthly_subscription_0.99";
        return "finjan_vpn_monthly_multiple_device";
//        return "finjan_vpn_monthly_multiple_device_750ff";
    }

    public static String getSkuThisDevicesMonthly() {
//        return "monthly_subscription_0.99";
        return "finjan_vpn_monthly_single_device";
//        return purchaseConfig.getSkuThisDevicesMonthly();
    }

    public static String getSkuThisDevicesYearly() {
//        return "yearly_subscription_5.99";
        return "finjan_vpn_yearly_single_device";
//        return purchaseConfig.getSkuThisDevicesYearly();
    }
    public static boolean isVpnSku(String sku){
        if(sku.equals(getSkuThisDevicesMonthly()) ||
                sku.equals(getSkuThisDevicesYearly()) ||
                sku.equals(getSkuAllDevicesMonthly()) ||
                sku.equals(getSkuAllDevicesYearly())){
            return true;
        }
        return false;
    }


    @Nullable
    static String getMYA(String sku) {
        return purchaseConfig.getMYA(sku);
    }

    /**
     * clear previously inited SKUs and MYAs
     */
    public static void clearPurchaseConfig() {
        purchaseConfig.clear();
    }
}