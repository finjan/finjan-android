package com.finjan.securebrowser.vpn.ui.main;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.finjan.securebrowser.DividerItemDecoration;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.custom_views.BaseTextview;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.vpn_helper.CountryFlags;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.vpn.controller.database.AviraVpnContentProvider;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
import com.finjan.securebrowser.vpn.controller.database.DatabaseContract;
import com.finjan.securebrowser.vpn.util.AutoClose;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by navdeep on 9/3/18.
 */

public class RegionFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, RegionAdapterNew.CountryClickListener {

    public static final String TAG = RegionFragment.class.getSimpleName();
    protected static int LOADER_ID = AviraVpnContentProvider.getUniqueLoaderId();
//    private static String countryType;
    private RecyclerView recyclerViewRegions;
    private AppCompatEditText etSearch;
    private ArrayList<RegionModelNew> arrayListCountry, arrayListSearch, arrayListCity;
    private RegionAdapterNew adapter;
    private ImageView imgbackButton;
    private BaseTextview tvToolbarTitle;


//    private HashMap<String,RegionModelNew> regionModelNewHashMap;
    private ArrayList< RegionModelNew> regions,allRegions;
    private boolean countryType;

    public static RegionFragment getInstance(ArrayList< RegionModelNew> regions,
                                             ArrayList< RegionModelNew> allRegions,
                                             boolean listType) {
        Bundle bundle=new Bundle();
        bundle.putSerializable("regions",regions);
        bundle.putSerializable("allRegions",allRegions);
        bundle.putBoolean("listType",listType);
        RegionFragment fragment=new RegionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }
    private RegionSelectedLinstener regionSelectedLinstener;
    public void setRegionSelectedListener(RegionSelectedLinstener regionSelectedListener){
        this.regionSelectedLinstener=regionSelectedListener;
    }
    private void setDefaults(){
        if(getArguments()!=null) {
            if (getArguments().containsKey("regions")) {
                regions = (ArrayList<RegionModelNew>) getArguments().getSerializable("regions");
                allRegions= (ArrayList<RegionModelNew>) getArguments().getSerializable("allRegions");
//                regionModelNewHashMap = (HashMap<String, RegionModelNew>) getArguments().getSerializable("regions");
            }
            if(getArguments().containsKey("listType")){
                countryType =getArguments().getBoolean("listType");
            }
        }
        if(countryType){
            GoogleTrackers.Companion.setSLocationChange();
//            LocalyticsTrackers.Companion.setSLocationChange();
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, null, this);
        setDefaults();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_region, container, false);
        return view;
    }

    private void showCountryList(){
        recyclerViewRegions.setAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.enter_to_right));
        recyclerViewRegions.setVisibility(View.GONE);
        recyclerViewRegions.setAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.enter_from_left));
        recyclerViewRegions.setVisibility(View.VISIBLE);
    }
    private void showCityList(){
        recyclerViewRegions.setAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.enter_to_left));
        recyclerViewRegions.setVisibility(View.GONE);
        recyclerViewRegions.setAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.enter_from_right));
        recyclerViewRegions.setVisibility(View.VISIBLE);
    }

    private void setReferences(View view){
        recyclerViewRegions = (RecyclerView) view.findViewById(R.id.regionList);
        etSearch = (AppCompatEditText) view.findViewById(R.id.tv_reginal_dialog_title);
        tvToolbarTitle = (BaseTextview) view.findViewById(R.id.tv_title);
        tvToolbarTitle.setText(getString(R.string.choose_location));
        imgbackButton = (ImageView) view.findViewById(R.id.iv_backButton);
        imgbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPress();}
        });
    }
    void onBackPress(){
        NativeHelper.getInstnace().hideKeyBoard(getActivity());
        etSearch.setText("");
        recyclerViewRegions.scrollTo(0,0);
        Logger.logE(TAG, "back stack entry count is==" + getActivity().getSupportFragmentManager().getBackStackEntryCount());
        if(countryType){
            getActivity().finish();
            getActivity().overridePendingTransition(R.anim.enter_from_top,R.anim.enter_to_bottom);

        }else {
            countryType=true;
            ArrayList list=new ArrayList<>(regions);
//            RegionModelNew regionModelNew=regionModelNewHashMap.get("nearest");
//            RegionModelNew us=regionModelNewHashMap.get("us");
//            list.remove(regionModelNew);
//            list.remove(us);
//            list.add(0,regionModelNew);
//            list.add(1,us);
            adapter.notify(list,true);
            showCountryList();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setReferences(view);

        arrayListSearch = new ArrayList<>();
        adapter = new RegionAdapterNew(this,new ArrayList<>(regions), countryType);
        recyclerViewRegions.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewRegions.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewRegions.getContext());
        recyclerViewRegions.addItemDecoration(dividerItemDecoration);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String enterName = etSearch.getText().toString().toLowerCase();
                if(countryType){
                    filter(enterName,regions);
                }else
               filter(enterName,adapter.getList());
            }
        });
    }

    private void filter(String s, Collection<RegionModelNew> arrayList) {
        ArrayList<RegionModelNew> filteredList=new ArrayList<>();
        if (s.length() != 0) {
            for (RegionModelNew region: arrayList){
                if (region.getName().toLowerCase().contains(s)) {
                    filteredList.add(region);
                }
            }
            adapter.notify(filteredList);
        } else {
            adapter.notify(adapter.getList());
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), AviraVpnContentProvider.getServerUri(),
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (isAdded()) {
//            initRegions(getActivity());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


    @Override
    public void onCountrySelected(RegionModelNew regionModelNew) {
        if(regionModelNew.getCitiesList()!=null && regionModelNew.getCitiesList().size()>0){
            countryType=false;
            etSearch.setText("");
            recyclerViewRegions.scrollTo(0,0);
            NativeHelper.getInstnace().hideKeyBoard(getActivity());
            adapter.notify(getSortedList(new ArrayList<>(regionModelNew.getCitiesList().values())),false);
            showCityList();
        }else {
//            onBackPress();
            regionSelectedLinstener.regionSelected(regionModelNew.getServerId(),regionModelNew.getRegion(),regionModelNew.getName()
            ,regionModelNew.getCountryFlag());
        }
    }

    @Override
    public void onCitySelected(RegionModelNew regionModelNew) {
//        getActivity().getSupportFragmentManager().popBackStack();
        regionSelectedLinstener.regionSelected(regionModelNew.getServerId(),regionModelNew.getRegion(),regionModelNew.getName(),regionModelNew.getCountryFlag());
    }

    public static ArrayList< RegionModelNew> prepareAllLocaitonList(ArrayList<RegionModelNew> regionModelNewArrayList){
        ArrayList<RegionModelNew> countryList = new ArrayList<>();
        for (RegionModelNew regionModelNew:regionModelNewArrayList){
            if(regionModelNew.getCitiesList()!=null){
                for (RegionModelNew regionModelNew1: regionModelNew.getCitiesList().values()){
                    regionModelNew1.setIsCity(true);
                    regionModelNew1.setShowArrow(View.GONE);
                    regionModelNew1.setCountryFlag(regionModelNew.getCountryFlag());
                    countryList.add(regionModelNew1);
                }
            }else {
                countryList.add(regionModelNew);
            }
        }
        return countryList;
    }
    public static ArrayList< RegionModelNew> prepareCountryList(Context context) {
        /*HashMap<String, RegionModelNew> allRegionMap = getRegionsList(context);
        HashMap<String, RegionModelNew> countryList = new HashMap<>();

        Set<String> countries=new TreeSet<>();
        Set<String> tempCitiesList=new TreeSet<>();

        for (String regionName: allRegionMap.keySet()){
            countries.add(regionName.contains("_")?regionName.split("_")[0]:regionName);
        }
        if(countries.contains("nearest")){
            countryList.put("nearest",allRegionMap.get("nearest"));
            allRegionMap.get("nearest").setCountryFlag(CountryFlags.getInstance().getCountryFlag("nearest"));
            allRegionMap.get("nearest").setShowArrow(View.GONE);
            tempCitiesList.add("nearest");
        }
        for (String country: countries){
            for (String itemName: allRegionMap.keySet()){
                if(!tempCitiesList.contains(itemName)){
                    if (itemName.equals(country)) {
                        countryList.put(country,allRegionMap.get(country));
                        allRegionMap.get(country).setCountryId(country);
                        allRegionMap.get(country).setCountryFlag(CountryFlags.getInstance().getCountryFlag(country));
                        allRegionMap.get(country).setShowArrow(View.GONE);
                        tempCitiesList.add(country);
                        break;
                    }else if(itemName.contains(country+"_")) {
                        if(countryList.containsKey(country)){
                            allRegionMap.get(itemName).setCountryId(country);
                            countryList.get(country).getCitiesList().put(itemName,allRegionMap.get(itemName));
                            countryList.get(country).setCountryFlag(CountryFlags.getInstance().getCountryFlag(country));
                        }else {
                            RegionModelNew regionModel=new RegionModelNew(country,country.toUpperCase());
                            regionModel.setCountryId(country);
                            regionModel.setCountryFlag(CountryFlags.getInstance().getCountryFlag(country));
                            countryList.put(country,regionModel);
                            countryList.get(country).setShowArrow(View.VISIBLE);
                        }
                        tempCitiesList.add(itemName);
                    }
                }
            }
        }*/


        HashMap<String, RegionModelNew> countryList = getRegionsList(context);
        ArrayList list=new ArrayList<>(countryList.values());
        getSortedList(list);
        RegionModelNew regionModelNew = null;
        RegionModelNew us = null;
        if (countryList.containsKey("nearest")) {
            regionModelNew=countryList.get("nearest");
            list.remove(regionModelNew);
            list.add(0,regionModelNew);
        }

        if (countryList.containsKey("us")) {
            us = countryList.get("us");
//            us.setName("United States");
            list.remove(us);
            list.add(1,us);
        }


        return list;
    }
    private static ArrayList<RegionModelNew> getSortedList(ArrayList<RegionModelNew> list){

        Collections.sort(list, new Comparator<RegionModelNew>() {
            @Override
            public int compare(RegionModelNew o1, RegionModelNew o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return list;
    }

    private static HashMap<String, RegionModelNew> getRegionsList(Context context) {
        HashMap<String, RegionModelNew> mRegionList = new HashMap<>();
        String serverRegion = FinjanVpnPrefs.getLastUsedServer(context);
        long servicerId = 0;
        String serverTitle = null;
        if (!TextUtils.isEmpty(serverRegion)) {
            ContentProviderClient contentProviderClient = new ContentProviderClient();
            Cursor cursor = contentProviderClient.getServer(context, serverRegion);
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    servicerId = cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID));
                    serverTitle = cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.NAME));
                }
                AutoClose.closeSilently(cursor);
            }
        }
        if (TextUtils.isEmpty(serverTitle)) {
            serverTitle = "";
        }

        Cursor cursor;
        cursor = new ContentProviderClient().getAllServers(context);

        if (cursor != null) {
            RegionModelNew regionModel;
            while (cursor.moveToNext()) {
                regionModel = new RegionModelNew();
                regionModel.setServerId(cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID)));
                regionModel.setRegion(cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.REGION)));
                regionModel.setName(cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.NAME)));
                regionModel.setPort(cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.PORT)));
                regionModel.setProtocol(cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.PROTOCOL)));
                regionModel.setShowArrow(View.GONE);
                if (regionModel.getName().equalsIgnoreCase(serverTitle)) {
                    regionModel.setSelected(true);
                } else {
                    regionModel.setSelected(false);
                }

//                if(regionModel.getRegion().equals("nearest")){
//                    regionModel.setCountryFlag(CountryFlags.getInstance().getCountryFlag("nearest"));
//                    regionModel.setCountryFlag(CountryFlags.getInstance().getCountryFlag("nearest"));
//                }else{
                String country = regionModel.getRegion().contains("_")?regionModel.getRegion().split("_")[0]:regionModel.getRegion();
                regionModel.setCountryId(country);
                regionModel.setCountryFlag(CountryFlags.getInstance().getCountryFlag(country));
//                }

                if (regionModel.getRegion().contains("_")){
                    String key = regionModel.getRegion().split("_")[0];
                    if (!mRegionList.containsKey(key)){
                        RegionModelNew countryModel = new RegionModelNew();
                        countryModel.setServerId(regionModel.getServerId());
                        countryModel.setRegion(regionModel.getRegion());
                        countryModel.setName(regionModel.getName());
                        countryModel.setPort(regionModel.getPort());
                        countryModel.setProtocol(regionModel.getProtocol());
                        countryModel.setCountryId(regionModel.getCountryId());
                        countryModel.setCountryFlag(regionModel.getCountryFlag());
                        countryModel.setShowArrow(View.VISIBLE);
                        if (key.equalsIgnoreCase("us")){
                            countryModel.setName("United States");
                        }
                        else if (key.equalsIgnoreCase("gb")){
                            countryModel.setName("United Kingdom");
                        }
                        mRegionList.put(key, countryModel);
                    }
                    mRegionList.get(key).getCitiesList().put(regionModel.getRegion(),regionModel);
                }else{
                    mRegionList.put(regionModel.getRegion(), regionModel);
                }



            }

            AutoClose.closeSilently(cursor);
        } else {
            Logger.logE(TAG, "Database request failed");
        }
        return mRegionList;
    }
    public interface RegionSelectedLinstener{
        void regionSelected(long id, String region, String title,int flag);
    }
}
