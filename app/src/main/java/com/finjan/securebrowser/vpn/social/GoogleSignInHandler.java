//package com.finjan.securebrowser.vpn.social;
//
//import android.content.Intent;
//import android.content.IntentSender;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.FragmentActivity;
//import android.text.TextUtils;
//import android.util.Log;
//
//import com.finjan.securebrowser.R;
//import com.finjan.securebrowser.eventbus_pojo.GoogleSignInSuccessEvent;
//import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
//import com.finjan.securebrowser.helpers.logger.Logger;
//import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
//import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
//import com.google.android.gms.auth.api.Auth;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
//import com.google.android.gms.auth.api.signin.GoogleSignInResult;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.common.api.Status;
//import com.google.android.gms.plus.Plus;
//
//import java.util.ArrayList;
//
//import de.greenrobot.event.EventBus;
//
///**
// * Created by navdeep on 26/2/18.
// */
//
//public class GoogleSignInHandler implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
//    private GoogleApiClient mGoogleApiClient;
//    private int RC_SIGN_IN = 0;
//    private int RESULT_OK = 0;
//    private GoogleSignInResult result;
//    private FragmentActivity frag;
//    private SocialUserProfile personProfile;
//    /* Is there a ConnectionResult resolution in progress? */
//    private boolean mIsResolving = false;
//
//    /* Should we automatically resolve ConnectionResults when possible? */
//    private boolean mShouldResolve = false;
//
//    public void handle_initialization(FragmentActivity fragmentActivity) {
//        // Configure sign-in to request the user's ID, email address, and basic
//// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
//        frag = fragmentActivity;
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .requestIdToken("648551950159-qn8djgas54g46ig7l8e0ovr5ub7hmmke.apps.googleusercontent.com")
//                .requestProfile()
//                .build();
//        mGoogleApiClient = new GoogleApiClient.Builder(fragmentActivity)
//                .enableAutoManage(fragmentActivity /* FragmentActivity */, this /* OnConnectionFailedListener */)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .addOnConnectionFailedListener(this)
//                .addConnectionCallbacks(this)
//                .build();
//    }
//
//    public void hanlde_onStartConfig() {
//        mGoogleApiClient.connect();
//    }
//
//    public void handle_onStopConfig() {
//        mGoogleApiClient.disconnect();
//    }
//
//    public Intent onClickSignInButton() {
//        mShouldResolve = true;
//        mGoogleApiClient.connect();
////        LocalyticsTrackers.Companion.setEGoogle_login();
//
//        Logger.logE("google signup", "siging in");
//        // Show a message to the user that we are signing in.
////        mStatus.setText(R.string.signing_in);
//        return  Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//    }
//
//    public boolean onClickSignOutButton() {
//        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
////                        updateUI(false);
//                    }
//                });
//        return false;
//    }
//
//    public SocialUserProfile performTaskOnActivityResult(int requestCode, int resultCode, Intent data) {
//        // The Task returned from this call is always completed, no need to attach
//        // a listener.
//        if (requestCode == RC_SIGN_IN) {
//            // If the error resolution was not successful we should not resolve further.
//            if (resultCode != RESULT_OK) {
//                mShouldResolve = false;
//            }
//            mIsResolving = false;
//            mGoogleApiClient.connect();
//            result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            GoogleSignInAccount acct = result.getSignInAccount();
//            if(acct!=null){
//                personProfile=new SocialUserProfile();
//                personProfile.setSocialType("gg_id");
////                personProfile.setSocialId("106698897187359322472");
//                personProfile.setSocialId(acct.getId());
//
//                personProfile.setToken_id(acct.getIdToken());
//                if(TextUtils.isEmpty(acct.getEmail())){
//                    personProfile.setEmail(acct.getId()+"@google-social.com");
//                }else {
//                    personProfile.setEmail(acct.getEmail());
//                }
//                personProfile.setFirstName(acct.getGivenName());
//                personProfile.setLastName(acct.getFamilyName());
//            }
//        }
//        return personProfile;
//    }
//
//    /**
//     *  if(socialId=="429552714162294"){
//     socialId="429552714162272";
//     }
//     if(socialId=="106698897187359322439"){
//     socialId="106698897187359322472";
//     }
//     * @param connectionResult
//     */
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//// Could not connect to Google Play Services.  The user needs to select an account,
//        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
//        // ConnectionResult to see possible error codes.
//        Logger.logD("signup", "onConnectionFailed:" + connectionResult);
//
//        if (!mIsResolving && mShouldResolve) {
//            if (connectionResult.hasResolution()) {
//                try {
//                    connectionResult.startResolutionForResult(frag, RC_SIGN_IN);
//                    mIsResolving = true;
//                } catch (IntentSender.SendIntentException e) {
//                    Logger.logE("singup handler", "Could not resolve ConnectionResult."+e);
//                    mIsResolving = false;
//                    mGoogleApiClient.connect();
//                }
//            } else {
//                // Could not resolve the connection result, show the user an
//                // error dialog.
//                EventBus.getDefault().post(new GoogleSignInSuccessEvent("error"));
//            }
//        } else {
//            // Show the signed-out UI
//            EventBus.getDefault().post(new GoogleSignInSuccessEvent("signOut"));
////            showSignedOutUI();
//        }
//
//    }
//
//    @Override
//    public void onConnected(@Nullable Bundle bundle) {
//        // onConnected indicates that an account was selected on the device, that the selected
//        // account has granted any requested permissions to our app and that we were able to
//        // establish a service connection to Google Play services.
//        mShouldResolve = false;
//        if (result != null) {
//
//        }
//        EventBus.getDefault().post(new GoogleSignInSuccessEvent("success"));
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    private void onSignOutClicked() {
//        // Clear the default account so that GoogleApiClient will not automatically
//        // connect in the future.
//        if (mGoogleApiClient.isConnected()) {
//
//            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
//            mGoogleApiClient.disconnect();
//        }
////        showSignedOutUI();
//    }
//}
