package com.finjan.securebrowser.vpn.licensing;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.PowerManager;
import androidx.annotation.Nullable;
import android.text.format.DateUtils;

import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.vpn.licensing.models.billing.IabResult;
import com.finjan.securebrowser.vpn.licensing.models.billing.Inventory;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.models.restful.License;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper.OnIabSetupFinishedListener;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper.QueryInventoryFinishedListener;
import com.avira.common.utils.SharedPreferencesUtilities;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;

import static com.avira.common.CommonLibrary.DEBUG;

/**
 *
 * <p/><b>Note:</b> make sure that you also start the service inside a <b>BootCompletedReceiver</b> otherwise it will
 * only be useful until the user restarts his/her device
 *
 * @author ovidiu.buleandra on 12/7/2015.
 */
public class LicensingService extends Service {
    private final static String TAG = LicensingService.class.getSimpleName();

    private static final String PREFS_LAST_TRIGGER_TIME = "licensing_service_last_trigger";
    private static final String PREFS_SERVICE_ENABLED = "licensing_service_enabled";
    private static final String PREFS_SERVICE_PRODUCTS = "licensing_service_products";

    private static final String EXTRA_ACRONYM = "extra_acronym";
    private static final String EXTRA_PRODUCTS = "extra_products";
    private static final String EXTRA_BACKOFF = "extra_backoff";
    private static final String EXTRA_FORCE_START = "extra_force_start";

    private static final int REQUEST_CODE = 1024;
    private static final long WAKE_LOCK_TIMEOUT = 120 * DateUtils.SECOND_IN_MILLIS;
    private static final long LATCH_TIMEOUT = 120 * DateUtils.SECOND_IN_MILLIS;
    private static final long[] BACKOFF_SCHEME = {
            0,
            2 * DateUtils.MINUTE_IN_MILLIS,
            DateUtils.HOUR_IN_MILLIS,
            3 * DateUtils.HOUR_IN_MILLIS,
            6 * DateUtils.HOUR_IN_MILLIS
    };

    private PowerManager.WakeLock mWakeLock;
    private HashMap<String, String> mProducts;
    private String mProductAcronym;
    private boolean isProcessingLicense;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void enableService(Context context) {
        SharedPreferencesUtilities.putBoolean(context, PREFS_SERVICE_ENABLED, true);
    }

    public static boolean isEnabled(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, PREFS_SERVICE_ENABLED);
    }

    private static Intent buildIntent(Context context, HashMap<String, String> products,
                                      String productAcronym, int backoff, boolean forceStart) {
        Intent serviceIntent = new Intent(context, LicensingService.class);
        serviceIntent.putExtra(EXTRA_PRODUCTS, products);
        serviceIntent.putExtra(EXTRA_ACRONYM, productAcronym);
        serviceIntent.putExtra(EXTRA_BACKOFF, backoff);
        serviceIntent.putExtra(EXTRA_FORCE_START, forceStart);
        return serviceIntent;
    }

    private static PendingIntent buildPendingIntent (Context context, Intent intent, boolean noCreate) {
        return PendingIntent.getService(context,
                REQUEST_CODE,
                intent,
                noCreate ? PendingIntent.FLAG_NO_CREATE : PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private static PendingIntent buildPendingIntent (Context context, HashMap<String, String> products,
                                                     String productAcronym, int backoff, boolean noCreate) {
        return buildPendingIntent(context,
                buildIntent(context, products, productAcronym, backoff, false),
                noCreate);
    }

    private static PendingIntent buildPendingIntent(Context context, HashMap<String, String> products,
                                                   String productAcronym, int backoff) {
        return buildPendingIntent(context, products, productAcronym, backoff, false);
    }

    public static void startService(Context context, HashMap<String, String> products, String productAcronym) {
        SharedPreferencesUtilities.putString(context, PREFS_SERVICE_PRODUCTS, toJSON(products));

        context.startService(buildIntent(context, products, productAcronym, 0, false));
    }

    public static void forceStartService(Context context, HashMap<String, String> products, String productAcronym) {
        SharedPreferencesUtilities.putString(context, PREFS_SERVICE_PRODUCTS, toJSON(products));

        context.startService(buildIntent(context, products, productAcronym, 0, true));
    }

    private static String toJSON(HashMap<String, String> map) {
        Logger.logD(TAG, "toJSON " + map);
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<HashMap<String, String>>(){}.getType();
        return gson.toJson(map, stringStringMap);
    }

    private static HashMap<String, String> toHashMap(String json) {
        Logger.logD(TAG, "toHashMap " + json);
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<HashMap<String, String>>(){}.getType();
        return gson.fromJson(json, stringStringMap);
    }

    @SuppressWarnings("unchecked")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isEnabled(this)) {
            createAndAcquirePartialLock();
            try {
                // get products from shared preferences
                mProducts = toHashMap(SharedPreferencesUtilities.getString(getApplicationContext(), PREFS_SERVICE_PRODUCTS));
            } catch (NullPointerException | JsonSyntaxException e) {
                // ignore
            }
            if (intent == null) {
                // something was wrong so we wait for another start service from the implementing app
                stopSelf();
                return START_NOT_STICKY;
            }

            // take the products from  the intent if they don't exist in the sharedprefs (shouldn't happen)
            if((mProducts == null || mProducts.size() == 0) && intent.hasExtra(EXTRA_PRODUCTS)) {
                mProducts = (HashMap<String, String>) intent.getSerializableExtra(EXTRA_PRODUCTS);
                if(mProducts == null) {
                    // something was wrong so we wait for another start service from the implementing app
                    stopSelf();
                    return START_NOT_STICKY;
                }

                // save them to prefs
                SharedPreferencesUtilities.putString(getApplicationContext(), PREFS_SERVICE_PRODUCTS, toJSON(mProducts));
            }

            boolean forceStart = intent.getBooleanExtra(EXTRA_FORCE_START, false);
            if (forceStart) Logger.logD(TAG, "force start requested");

            // check if enough time has past from the last call and is not a forced start event
            if (!canRun() && !forceStart) {
                Logger.logD(TAG, "can't run. reschedule");
                reschedule(getApplicationContext(), 0, intent);
                stopSelf();
                return START_NOT_STICKY;
            }

            mProductAcronym = intent.getStringExtra(EXTRA_ACRONYM);
            int backoff = intent.getIntExtra(EXTRA_BACKOFF, 0);
            startProcessingLicense(backoff);
            return START_STICKY;
        } else {
            stopSelf();
            return START_NOT_STICKY;
        }

    }

    private synchronized boolean checkIsProcessingLicense(){
        return isProcessingLicense;
    }

    private synchronized void setProcessingLicense(boolean processingLicense){
        isProcessingLicense = processingLicense;
    }

    private synchronized void startProcessingLicense(int backoff){
        if(!checkIsProcessingLicense()) {
            setProcessingLicense(true);
            LicensesProcessing worker = new LicensesProcessing(this, mProductAcronym, mProducts, backoff);
            Thread processingThread = new Thread(worker, "ProcessingThread[" + TAG + "]");
            processingThread.start();
        }else{
            Logger.logD(TAG, "Skipping license check because one the job is already in progress");
        }
    }


    @Override
    public void onDestroy() {
        releasePartialLock();
    }

    private void createAndAcquirePartialLock() {
        // avoid creating the lock if the calling app doesn't have permission for a wakelock
        if(checkCallingOrSelfPermission(Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Logger.logD(TAG, "acquire wakelock");
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        mWakeLock.acquire(WAKE_LOCK_TIMEOUT);
    }

    private void releasePartialLock() {
        Logger.logD(TAG, "release lock");
        if(mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    private boolean canRun() {
        final long lastTime = SharedPreferencesUtilities.getLong(this, PREFS_LAST_TRIGGER_TIME, 0);
        final long currentTime = System.currentTimeMillis();
        final boolean result = (currentTime - lastTime) > (24 * DateUtils.HOUR_IN_MILLIS);
        Logger.logD(TAG, String.format("canRun=%s (%d > %d)", result, currentTime, lastTime));
        return result || DEBUG;
    }

    private synchronized static void reschedule(Context context, int backoffLevel, Intent rescheduleIntent) {
        Logger.logD(TAG, "reschedule => backoffLevel=" + backoffLevel);
        long triggerAt = System.currentTimeMillis();
        rescheduleIntent.putExtra(EXTRA_BACKOFF, backoffLevel);

        // if we reached the max limit of the backoff scheme then consider this a failure and we are back to square one
        // so next call will happen in the normal interval
        if(backoffLevel >= BACKOFF_SCHEME.length) {
            backoffLevel = 0;
        }
        if(backoffLevel == 0) {
            // set alarm to trigger in a random time in between 24 and 48 hours from now
            final Random random = new Random();
            final long offset = (long)(24 + random.nextDouble() * 24);
            Logger.logD(TAG, "reschedule offset=" + offset + " hours");
            triggerAt += (!DEBUG ? offset * DateUtils.HOUR_IN_MILLIS : DateUtils.MINUTE_IN_MILLIS);
            SharedPreferencesUtilities.putLong(context, PREFS_LAST_TRIGGER_TIME, System.currentTimeMillis());
        } else {
            // get the backoff offset and reschedule the check with it
            final long backoffOffset = !DEBUG ? BACKOFF_SCHEME[backoffLevel] : 20 * DateUtils.SECOND_IN_MILLIS ;
            Logger.logD(TAG, "reschedule backoff=" + backoffOffset);
            triggerAt += backoffOffset;

            // erase the last trigger time so the backoff can run next time it triggers
            // is necessary for the canRun function since the backoff < 24h
            SharedPreferencesUtilities.putLong(context, PREFS_LAST_TRIGGER_TIME, 0);
        }

        if(DEBUG) {
            final Date nextTriggerDate = new Date(triggerAt);
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Logger.logD(TAG, "reschedule next_trigger_date=" + sdf.format(nextTriggerDate));
        }

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, triggerAt, buildPendingIntent(context, rescheduleIntent, false));
    }

    private class LicensesProcessing implements Runnable,
            OnIabSetupFinishedListener, QueryInventoryFinishedListener, LicensesCallback, ProcessPurchaseCallback {

        private CountDownLatch latch = new CountDownLatch(2);
        private IabHelper mIabHelper;
        private HashMap<String, String> mProducts;
        private Inventory mInventory;
        private List<License> mLicenses;
        private String mProductAcronym;
        private int mBackoffLevel;
        private Context mAppContext;
        private Intent mRescheduleIntent;
        private long mQueryDuration;

        public LicensesProcessing(Context context, String productAcronym, HashMap<String, String> products,
                                  int backoff) {
            mAppContext = context.getApplicationContext();
            mIabHelper = new IabHelper(mAppContext);
            mProductAcronym = productAcronym;
            mProducts = products;
            mBackoffLevel = backoff;
            mRescheduleIntent = buildIntent(mAppContext, products, productAcronym, backoff, false);
        }

        @Override
        public void run() {
            try {
                Logger.logD(TAG, "LicensesProcessing:run");
                // start a setup that starts an inventory query for purchases on google store
                mIabHelper.startSetup(this);
                // query the backend for licenses for this user
                mQueryDuration = System.currentTimeMillis();
                String queryLicensesUrl = Licensing.queryLicenses(mAppContext, mProductAcronym, this);

                try {
                    // wait for the 2 jobs to finish or crash :P
                    latch.await(LATCH_TIMEOUT, TimeUnit.MILLISECONDS);
                    if (mInventory == null || mLicenses == null) {
                        // something bad happened here so start the backoff scheme
                        errorHandling();
                        return;
                    }

                    List<License> existingLicenses = LicenceSetting.readLicenses();

                    processPurchases();

                    // save licenses in secure db
                    LicenceSetting.saveLicenses(mLicenses);

                    // notify live listeners that a change occurred
                    EventBus.getDefault().post(new LicensingTrackingEvent(existingLicenses, mLicenses,
                            queryLicensesUrl, mQueryDuration));
                    EventBus.getDefault().post(new LicensesRefreshedEvent(mLicenses));

                    Logger.logD(TAG, "LicensesProcessing:normalReschedule");
                    LicensingService.reschedule(mAppContext, 0, mRescheduleIntent);
                } catch (InterruptedException e) {
                    errorHandling();
                } finally {
                    if (mIabHelper != null) {
                        mIabHelper.dispose();
                        mIabHelper = null;
                    }
                }
            } catch (NullPointerException e) {
                Logger.logE(TAG, "LicensesProcessing:runNullPointer"+e);
                LicensingService.reschedule(mAppContext, 0, mRescheduleIntent);
            }
            setProcessingLicense(false);
        }

        private void errorHandling() {
            Logger.logD(TAG, "LicensesProcessing:errorHandling");
            LicensingService.reschedule(mAppContext, mBackoffLevel + 1, mRescheduleIntent);
        }

        private void processPurchases() throws InterruptedException {
            Logger.logD(TAG, "LicensesProcessing:processPurchases");
            List<Purchase> purchasesList = mInventory.getAllPurchases();
            boolean shouldRetriveLicenses = false;
            for(Purchase purchase : purchasesList) {
                if(!mProducts.containsKey(purchase.getSku())) {
                    continue;
                }

                final String productAcronym = mProducts.get(purchase.getSku());
                boolean found = false;
                for(License license : mLicenses) {
                    if(productAcronym.equals(license.getProductAcronym())) {
                        found = true;
                        break;
                    }
                }
                if(!found) {
                    // we have a purchase that wasn't processed on the backend side
//                    Licensing.processPurchase(mAppContext,
//                            purchase, mInventory.getSkuDetails(purchase.getSku()),
//                            productAcronym, this);

                }
            }

            // if we have unprocessed purchases relaunch the query of the licenses (hopefully the calls to process are
            // done by now
            // NOT ACTIVATED YET
            if(shouldRetriveLicenses) {
                latch = new CountDownLatch(1);
                Licensing.queryLicenses(mAppContext, mProductAcronym, this);
                latch.await(LATCH_TIMEOUT, TimeUnit.MILLISECONDS);
            }
        }

        @Override
        public void onIabSetupFinished(IabResult result) {
            if(!result.isSuccess() || mIabHelper == null || mProducts == null) {
                Logger.logD(TAG, "LicensesProcessing:setupFinishedWithError");
                latch.countDown();
                return;
            }

            Logger.logD(TAG, "LicensesProcessing:setupFinished");
            mIabHelper.queryInventoryAsync(true, new ArrayList<>(mProducts.keySet()), this);
        }

        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inv) {
            if(result.isSuccess()) {
                Logger.logD(TAG, "LicensesProcessing:inventoryReady");
                mInventory = inv;
            }
            latch.countDown();
        }

        @Override
        public void onLicenseQuerySuccess(List<License> licenses) {
            mQueryDuration = System.currentTimeMillis() - mQueryDuration;
            mLicenses = licenses;
            log(licenses);
            latch.countDown();
        }

        @Override
        public void onLicenseQueryError(int errorCode, String message) {
            mQueryDuration = System.currentTimeMillis() - mQueryDuration;
            latch.countDown();
        }

        @Override
        public void onProcessPurchaseError(int errorCode, String message) {
            // ignore errors of processing
        }

        @Override
        public void onProcessPurchaseSuccessful(boolean result) {
            // ignore result
            // since we don't get a license object we need to wait until next synchronization occurs so we can
            // get a proper license for this purchase
        }

        private void log(List array) {
            for(Object obj: array) {
                Logger.logI(TAG, obj.toString());
            }
        }
    }
}
