package com.finjan.securebrowser.vpn.ui.promo

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.text.TextUtils
import android.widget.Toast
import com.android.volley.VolleyError
import com.avira.common.backend.WebUtility
import com.electrolyte.utils.DateHelper
import com.electrolyte.utils.ResourceHelper
import com.finjan.securebrowser.R
import com.finjan.securebrowser.helpers.*
import com.finjan.securebrowser.helpers.logger.Logger
import com.finjan.securebrowser.helpers.sharedpref.UserPref
import com.finjan.securebrowser.model.RedeemListData
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers
import com.finjan.securebrowser.util.AppConfig
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder
import com.finjan.securebrowser.util.dialogs.NativeDialogs
import com.finjan.securebrowser.vpn.FinjanVPNApplication
import com.finjan.securebrowser.vpn.controller.network.NetworkManager
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener
import com.finjan.securebrowser.vpn.eventbus.AllPromoFetched
import com.finjan.securebrowser.vpn.eventbus.CheckingPromo
import com.finjan.securebrowser.vpn.eventbus.DiscountedPromoAvailable
import com.finjan.securebrowser.vpn.eventbus.SubscriptionUpdated
import com.finjan.securebrowser.vpn.subscriptions.SubscriptionHelper
import com.finjan.securebrowser.vpn.ui.iab.LicenseUtil
import com.finjan.securebrowser.vpn.ui.main.VpnActivity
import com.finjan.securebrowser.vpn.ui.main.VpnApisHelper
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment
import com.finjan.securebrowser.vpn.util.TrafficController
import de.greenrobot.event.EventBus
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class PromoHelper {

    companion object {

        public fun getDiscountedArray(discount: Int): ArrayList<String> {
            var list = ArrayList<String>()
            when (discount) {
                0 -> {
                    list.add(LicenseUtil.getSkuAllDevicesYearly())
                    list.add(LicenseUtil.getSkuAllDevicesMonthly())
                }
                33 -> {
                    list.add(LicenseUtil.getSkuAllDevicesYearly33())
                    list.add(LicenseUtil.getSkuAllDevicesMonthly33())
                }
                25 -> {
                    list.add(LicenseUtil.getSkuAllDevicesYearly25())
                    list.add(LicenseUtil.getSkuAllDevicesMonthly25())
                }
                50 -> {
                    list.add(LicenseUtil.getSkuAllDevicesYearly50())
                    list.add(LicenseUtil.getSkuAllDevicesMonthly50())
                }
                75 -> {
                    list.add(LicenseUtil.getSkuAllDevicesYearly75())
                    list.add(LicenseUtil.getSkuAllDevicesMonthly75())
                }
            }
            return list
        }

        private object HOLDER {
            var instance = PromoHelper()
        }

        var discountedRedeemListData: RedeemListData? = null
        var isPromoActive: Boolean? = false

        var currentActivePromo: RedeemListData? = null
        var isPromoEnded: Boolean? = false
        var isPromoChecked = false
        var pendingDeepLinkingPromo = false
        val NEW_USER = "new_user"
        val DATA_RANGE = "date_range"
        val REDEEM_OFFER = "redeem_code"
        val PROFILE_EVENT = "profile_event"
        val CHANGE_IAPS_IN_APP = "change_iaps"

        val PROMO_TITLE = "We have a surprise for you"
        val PROMO_MSG = "Congrates..! Click ok to accept promo offer"

        val instance: PromoHelper by lazy { HOLDER.instance }

        fun isNewUserRegistrationValid(): Boolean {
            var registeredDate = AuthenticationHelper.getInstnace().finjanUser.user_registered

            if (!TextUtils.isEmpty(registeredDate)) {
                registeredDate = registeredDate.split(" ")[0]
                val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                format.timeZone = TimeZone.getTimeZone("GMT")
                try {
                    val date = format.parse(registeredDate)
                    val cal = Calendar.getInstance()

                    cal.time = date
//                    val longRegisteredValue =  cal.timeInMillis
                    val nowCal = Calendar.getInstance()
                    nowCal.timeZone = TimeZone.getTimeZone("GMT")
                    return nowCal.get(Calendar.YEAR).equals(cal.get(Calendar.YEAR)) &&
                            nowCal.get(Calendar.MONTH).equals(cal.get(Calendar.MONTH))
                            && nowCal.get(Calendar.DAY_OF_MONTH).equals(cal.get(Calendar.DAY_OF_MONTH))
//                    return DateHelper.getInstnace().isFuture(longRegisteredValue)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            }
            return false
        }
    }


    val redeemListDataArrayList = ArrayList<RedeemListData>()
    fun getAllPromoList(context: Context): ArrayList<RedeemListData>? {
        if (VpnApisHelper.isPromoListFetched || redeemListDataArrayList.size > 0) {
            return redeemListDataArrayList
        }
        getAllRedeemOffers(context)
        return null
    }

    fun addRedeemListData(redeemListData: RedeemListData) {
        if (redeemListDataArrayList.size > 0) {
            for (redeemData: RedeemListData in redeemListDataArrayList) {
                if (redeemData.id.equals(redeemListData.id)) {
                    return
                }
            }
        }
        redeemListDataArrayList.add(redeemListData)
    }

    val redeemMap = HashMap<String, ArrayList<RedeemListData>>()
    fun setPromoList(redeemListDataArrayList: ArrayList<RedeemListData>) {
        this.redeemMap.clear()
        for (redeemData in redeemListDataArrayList.listIterator()) {
            var key = redeemData.attributes.promo_type
            if (TextUtils.isEmpty(key)) {
                key = CHANGE_IAPS_IN_APP
            }
            addRedeemDataInMap(key, redeemData)
        }
        this.redeemListDataArrayList.clear()
        this.redeemListDataArrayList.addAll(redeemListDataArrayList)
    }

    private fun addRedeemDataInMap(key: String, redeemListData: RedeemListData) {
        if (redeemMap.get(key) == null) {
            val list = ArrayList<RedeemListData>()
            list.add(redeemListData)
            redeemMap.put(key, list)
        } else {
            redeemMap.get(key)!!.add(redeemListData)
        }
    }

    fun isPromoExpires(redeemListData: RedeemListData): Boolean {
        var endDate = redeemListData.attributes.end_date
        if (!TextUtils.isEmpty(endDate)) {
            endDate = endDate.replace("-", "")
            endDate = endDate.replace(" ", "")
            endDate = endDate.replace(":", "")
            try {
                val endDateLong = endDate.toLong()
                if (endDateLong > 0) {
                    !DateHelper.getInstnace().isFutureDate(redeemListData.attributes.end_date,
                            getCurrentDateStr())
                }
            } catch (numberFormatException: NumberFormatException) {
            }

        }
        return false
    }

    private fun getEpiryDateOfPromo(redeemListData: RedeemListData): String {
        if (!TextUtils.isEmpty(redeemListData.attributes.days)) {
            val days = Integer.parseInt(redeemListData.attributes.days)
            Logger.logD("noOfDaysOfPromo", days.toString())
            if (days > 0) {
                return DateHelper.getInstnace().getServerFormatDate(getCurrentDate() + 1000 * 60 * 60 * 24 * days)
            }
        }
        return redeemListData.getAttributes().end_date
    }

    fun isAnyNewUserPromo(context: Context): Any {
        if (isNewUserRegistrationValid()) {
            val list = SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(context, false)
            val idList = ArrayList<String>()
            for (subscription in list) {
                idList.add(subscription.comp_flg)
            }
            if (redeemMap.get(NEW_USER) != null) {
                for (redeem in redeemMap.get(NEW_USER)!!) {


                    if (!PlistHelper.getInstance().allow_repeat_promos) {
                        if (!idList.contains(redeem.id) &&
                                !UserPref.getInstance().isPromoConsumed(redeem.id)) {
                            return redeem
                        }
                    } else {
                        return redeem
                    }

                }
            }
        }
        return false
    }

    var failedDeepLinkingTry = true

    fun checkIfCreateSubs(activity: Activity, redeemListData: RedeemListData) {
        val list = SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(activity, false)
        if (list == null) {
            failedDeepLinkingTry = true
            return
        }
        val idList = ArrayList<String>()
        for (subscription in list) {
            idList.add(subscription.comp_flg)
        }
        if (!isPromoExpires(redeemListData) &&
                !idList.contains(redeemListData.id) &&
                !UserPref.getInstance().isPromoConsumed(redeemListData.id)) {
            createPromo(activity, redeemListData.id)
            return
        } else if (PlistHelper.getInstance().allow_repeat_promos) {
            createPromo(activity, redeemListData.id)
            return
        } else {
            ToastHelper.showToast(ResourceHelper.getInstance().getString(R.string.you_are_not_valid_user_for_this_promo), Toast.LENGTH_LONG)
        }
    }

    fun checkIfDeepLinking(activity: VpnActivity) {
        if (activity.getIntent() != null && !TextUtils.isEmpty(activity.getIntent().getStringExtra("promo_id"))) {
            if (TrafficController.getInstance(activity).isPaid) {
                return
            }
            if (!failedDeepLinkingTry) {
                return
            }
            failedDeepLinkingTry = false
            activity.vpnHomeFragment.showLoader()
            val promoId = activity.getIntent().getStringExtra("promo_id")
            NetworkManager.getInstance(activity).homeRedeem(activity, object : NetworkResultListener {
                override fun executeOnSuccess(response: JSONObject?) {
                    activity.vpnHomeFragment.hideLoader()
                    if (response != null) {
                        val redeemList = RedeemListData.parseRedeemList(response)
                        if (redeemList != null && redeemList.size > 0) {
//                            val redeemListData = redeemList[0]
                            for (redeemListData in redeemList) {
                                if (redeemListData.id.equals(promoId)) {
                                    addRedeemListData(redeemListData)
                                    val list = SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(activity, false)
                                    if (list == null) {
                                        failedDeepLinkingTry = true
                                        return
                                    }
                                    val idList = ArrayList<String>()
                                    for (subscription in list) {
                                        idList.add(subscription.comp_flg)
                                    }
                                    if (redeemListData.attributes.promo_type == DATA_RANGE) {
                                        if (!isPromoExpires(redeemListData) &&
                                                !idList.contains(redeemListData.id) &&
                                                !UserPref.getInstance().isPromoConsumed(redeemListData.id)) {
                                            createPromo(activity, redeemListData.id)
                                            return
                                        } else {
                                            ToastHelper.showToast(ResourceHelper.getInstance().getString(R.string.you_are_not_valid_user_for_this_promo), Toast.LENGTH_LONG)
                                        }
                                    }
                                    if (redeemListData.attributes.promo_type == NEW_USER) {
                                        if (!isPromoExpires(redeemListData) && isNewUserRegistrationValid()) {
                                            if (!idList.contains(redeemListData.id) &&
                                                    !UserPref.getInstance().isPromoConsumed(redeemListData.id)) {
                                                createPromo(activity, redeemListData.id)
                                            }
                                            return
                                        } else {
                                            ToastHelper.showToast(ResourceHelper.getInstance().getString(R.string.you_are_not_valid_user_for_this_promo), Toast.LENGTH_LONG)
                                        }
                                    }
                                    if (redeemListData.attributes.promo_type == REDEEM_OFFER) {
                                        FinjanVPNApplication.getInstance().startActivity(VpnHomeFragment.upgradeIntentDeepLink(FinjanVPNApplication.getInstance(),
                                                redeemListData.attributes.redeem_code))
                                        return
                                    }
                                    if (redeemListData.attributes.promo_type == CHANGE_IAPS_IN_APP) {
                                        PromoHelper.discountedRedeemListData = redeemListData
                                        FinjanVPNApplication.getInstance().startActivity(VpnHomeFragment.upgradeIntent(activity, false, false, true))
                                    }
                                }
                                break
                            }
                        }
                    }
                }

                override fun executeOnError(error: VolleyError) {
                    activity.vpnHomeFragment.hideLoader()
                    val errormsg = WebUtility.getMessage(error)
                    if (!TextUtils.isEmpty(errormsg) && errormsg.contains("message")) {
                        val jsonObject = JSONObject(errormsg)
                        val msg = jsonObject.getString("message")
                        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.you_entered_wrong_redeem_key), Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }


    fun isDiscountedPromo(redeemCode: RedeemListData): Boolean {
        return (!(redeemCode.attributes.iapDiscount.android == null ||
                redeemCode.attributes.iapDiscount.android.size == 0 ||
                !(redeemCode.attributes.iapDiscount.android.size == 1 &&
                        redeemCode.attributes.iapDiscount.android[0].equals(""))))
    }

    fun isAnyDataRangePromo(context: Context): Any {
        val list = SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(context, false)
        val idList = ArrayList<String>()
        for (subscription in list) {
            idList.add(subscription.comp_flg)
        }
        if (redeemMap.get(DATA_RANGE) != null) {
            for (redeem in redeemMap.get(DATA_RANGE)!!) {
                if (!PlistHelper.getInstance().allow_repeat_promos) {
                    if (!isPromoExpires(redeem) && !idList.contains(redeem.id) &&
                            !UserPref.getInstance().isPromoConsumed(redeem.id)) {
                        return redeem
                    }
                } else {
                    return redeem
                }
            }
        }
        return false
    }

    fun isAnyDiscountPromo(context: Context): Any {
        if (redeemMap.get(CHANGE_IAPS_IN_APP) != null) {
            for (redeem in redeemMap.get(CHANGE_IAPS_IN_APP)!!) {
                if (!PlistHelper.getInstance().allow_repeat_promos) {
                    if (!isPromoExpires(redeem)) {
                        return redeem
                    }
                } else {
                    return redeem
                }
            }
        }
        return false
    }

    //    private fun isAnyNewUserPromo():Any{
//        if(redeemMap.get(NEW_USER)!=null && redeemMap.get(NEW_USER)!!.size>0){
//            return redeemMap.get(NEW_USER)!!.get(0)
//        }
//        return false
//    }
//    private fun isAnyDataRangePromo():Any{
//        if(redeemMap.get(DATA_RANGE)!=null && redeemMap.get(DATA_RANGE)!!.size>0) {
//            for (item in redeemMap.get(DATA_RANGE)!!){
//                if(!isPromoExpires(item)){
//                    return item
//                }
//            }
//        }
//        return false
//    }
    fun getRedeemListData(promoId: String): RedeemListData? {
        for (data in redeemListDataArrayList) {
            if (data.id.equals(promoId)) {
                return data
            }
        }
        return null
    }

    fun getCurrentDate(): Long {
        if (PlistHelper.getInstance().isTestmode && !TextUtils.isEmpty(PlistHelper.getInstance().current_date) && !PlistHelper.getInstance().current_date.equals("ignore")) {
            return DateHelper.getInstnace().getLongFromDate(PlistHelper.getInstance().current_date)
        } else {
            return Calendar.getInstance().timeInMillis;
        }
    }

    fun getCurrentDateStr(): String {
        if (PlistHelper.getInstance().isTestmode && !TextUtils.isEmpty(PlistHelper.getInstance().current_date) && !PlistHelper.getInstance().current_date.equals("ignore")) {
            return PlistHelper.getInstance().current_date/*"2019-01-15 11:30:00"*/
        } else {
            return DateHelper.getInstnace().getServerFormatDate(Calendar.getInstance().timeInMillis)
        }
    }

    private fun createPromo(context: Context, promoId: String) {
        val data = getRedeemListData(promoId)
        if (data != null) {
            SubscriptionHelper.getInstance().clearSubscriptionData()
            val endDate = getEpiryDateOfPromo(data)
            Logger.logD("endDateOfCreation", endDate)
            Logger.logD("createddate", PlistHelper.getInstance().current_date)
            SubscriptionHelper.getInstance().createPromoSubsc(context, promoId, endDate, object : NetworkResultListener {
                override fun executeOnSuccess(response: JSONObject) {
                    val map = HashMap<String, String>()
                    map.put("Promo_ID", promoId)
                    LocalyticsTrackers.setEPromoStart(map)
                    EventBus.getDefault().post(SubscriptionUpdated(promoId, true, true))
                    UserPref.getInstance().setPromoConsumed(promoId)
                    VpnApisHelper.instance.updateLicence(true, true, context)

                }

                override fun executeOnError(error: VolleyError) {
                }
            })
        }
    }

    fun showPopupOnPromoEndStart(context: Context, promoId: String, start: Boolean) {
        if (PlistHelper.getInstance().Show_native_subscription_or_cancellation_alert()) {
            NetworkManager.getInstance(context).homeRedeem(context, object : NetworkResultListener {
                override fun executeOnSuccess(response: JSONObject?) {
                    if (response != null) {
                        val redeemList = RedeemListData.parseRedeemList(response)
                        if (redeemList != null && redeemList.size > 0) {
                            for (redeemListData in redeemList) {
//                                val redeemListData = redeemList.get(0)
                                if (redeemListData.id.equals(promoId)) {
                                    if (start) {
                                        //     VpnHomeFragment.getInstance().checkIfAndSetDaysLeft(redeemListData)
                                        showPromoStartDialog(context, redeemListData)
                                    } else {
                                        showPromoEndDialog(context, redeemListData)
                                    }
                                    break
                                }
                            }
                        }
                    }

                }

                override fun executeOnError(error: VolleyError?) {
                    Logger.logD("something", "" + error.toString())
                }

            })
            /* NetworkManager.getInstance(context).getSinglePromo(context, promoId, object : NetworkResultListener {
                 override fun executeOnError(error: VolleyError?) {
                     Logger.logD("something",""+error.toString())
                 }

                 override fun executeOnSuccess(response: JSONObject?) {
                     if (response != null) {
                         val redeemList = RedeemListData.parseRedeemList(response)
                         if (redeemList.size > 0) {
                             val redeemListData = redeemList.get(0)
                             if (start) {
                            //     VpnHomeFragment.getInstance().checkIfAndSetDaysLeft(redeemListData)
                                 showPromoStartDialog(context, redeemListData)
                             } else {
                                 showPromoEndDialog(context, redeemListData)
                             }
                         }
                     }
                 }
             })*/
        } else {
            showDesktopShare()
        }
    }

    /* fun getSinglePromo(context: Context, promoId: String): RedeemListData? {
         var redeemListData:RedeemListData
             NetworkManager.getInstance(context).getSinglePromo(context, promoId, object : NetworkResultListener {
                 override fun executeOnError(error: VolleyError?) {
                     Logger.logD("something",""+error.toString())
                     //redeemListData = null
                 }

                 override fun executeOnSuccess(response: JSONObject?) {
                     if (response != null) {
                         val redeemList = RedeemListData.parseRedeemList(response)
                         if (redeemList!=null && redeemList.size > 0) {
                              redeemListData = redeemList.get(0)

                         }
                     }

                 }
             })
       return redeemListData

     }*/


    fun onOneDayleftToEndPromo(promoId: String) {
        val map = java.util.HashMap<String, String>()
        map["Promo_ID"] = promoId
        if (!UserPref.getInstance().isPromoOneDayEventConsumed(promoId)) {
            LocalyticsTrackers.setEPromo_one_day_left(map)
            Logger.logE(PromoHelper::class.java.simpleName, "one day left fired")
            UserPref.getInstance().setPromoOneDayEventConsumed(promoId)
        }
    }


    private fun showDesktopShare() {
        if (GlobalVariables.user_registered) {
            GlobalVariables.user_registered = false
            NativeDialogs.getShareDesktopAppDialog(NativeHelper.getInstnace().currentAcitvity,
                    PlistHelper.getInstance().registrationPopUpText)
        }
    }

    fun showPromoStartDialog(context: Context, redeemListData: RedeemListData) {
        if (redeemListData.attributes.localytics_event_no.equals("0")) {
            FinjanDialogBuilder(context).setTitle(redeemListData.attributes.popup_begin_title)
                    .setMessage(redeemListData.attributes.popup_begin_text).setPositiveButton(ResourceHelper.getInstance().getString(R.string.alert_ok))
                    .setCustomPositiveBtnColour(PlistHelper.IAPLaterButtonBg)
                    .setDialogClickListener(object : FinjanDialogBuilder.PositiveClickListener {
                        override fun onPositivClick() {
                            showDesktopShare()
                            PromoHelper.Companion.isPromoActive = true
                            PromoHelper.Companion.isPromoEnded = false
                        }
                    }).show()
        } else {
            PromoHelper.Companion.isPromoActive = true
            PromoHelper.Companion.isPromoEnded = false
        }
    }

    fun showPromoEndDialog(context: Context, redeemListData: RedeemListData) {
        if (redeemListData.attributes.localytics_event_no.equals("0")) {
            FinjanDialogBuilder(context).setTitle(redeemListData.attributes.popup_end_title)
                    .setMessage(redeemListData.attributes.popup_end_text)
                    .setPositiveButton(redeemListData.attributes.popup_button_text)
                    .setNegativeButton(ResourceHelper.getInstance().getString(R.string.alert_cancel))
                    .setCustomNegativeBtnColour(PlistHelper.IAPLaterButtonBg)
                    .setDialogClickListener(object : FinjanDialogBuilder.PositiveClickListener {
                        override fun onPositivClick() {
                            PromoHelper.Companion.isPromoActive = true
//                        context.startActivity(Intent(context, UpgradeVpnActivity::class.java))
                            context.startActivity(VpnHomeFragment.upgradeIntent(context, true, true, true))
                            Handler().postDelayed({
                                PromoHelper.Companion.isPromoActive = false
                                PromoHelper.currentActivePromo = null
                                PromoHelper.Companion.isPromoEnded = true
                            }, 1000)
                        }
                    })
                    .setDialogClickListener(object : FinjanDialogBuilder.NegativeClickListener {
                        override fun onNegativeClick() {
                            PromoHelper.Companion.isPromoActive = false
                            PromoHelper.currentActivePromo = null
                            PromoHelper.Companion.isPromoEnded = true
                        }
                    }).show()
        } else {
            PromoHelper.Companion.isPromoActive = false
            PromoHelper.Companion.isPromoEnded = true

        }
    }

    //     fun getAllRedeemOffers(context: Context, networkManager: NetworkResultListener) {
//         if(AppConfig.promo_offer){
//             if(TrafficController.getInstance(context).isPaid){return}
//             VpnApisHelper.isPromoListFetched =false
//             if(redeemListDataArrayList.size ==0){
//                 NetworkManager.getInstance(context).homeRedeem(context, object : NetworkResultListener {
//                     override fun executeOnError(error: VolleyError?) {
//                         VpnApisHelper.isPromoListFetched = true
//                         if(networkManager!=null){
//                             networkManager.executeOnError(error)
//                         }
//                     }
//
//                     override fun executeOnSuccess(response: JSONObject?) {
//                         VpnApisHelper.isPromoListFetched = true
//                         val redeemListDataArrayList = RedeemListData.parseRedeemList(response)
//                         SubscriptionHelper.getInstance().updateRedeemList(redeemListDataArrayList)
//                         PromoHelper.instance.setPromoList(redeemListDataArrayList)
//                         PromoHelper.instance.checkForUserValidity(context)
//                         EventBus.getDefault().post(AllPromoFetched())
//                         if(networkManager!=null){
//                             networkManager.executeOnSuccess(response)
//                         }
//                     }
//                 })
//             }
//         }
//    }
    fun getSingleRedeemOffer(context: Context, networkManager: NetworkResultListener) {
        if (TrafficController.getInstance(context).isPaid) {
            return
        }
        NetworkManager.getInstance(context).homeRedeem(context, object : NetworkResultListener {
            override fun executeOnError(error: VolleyError?) {
                if (networkManager != null) {
                    networkManager.executeOnError(error)
                }
            }

            override fun executeOnSuccess(response: JSONObject?) {
                if (networkManager != null) {
                    networkManager.executeOnSuccess(response)
                }
            }
        })
    }


    fun ifLocationChangePromoOffer(context: Context, isActivatePromo: Boolean): Boolean {
        var returnval = false

        if (redeemListDataArrayList == null || redeemListDataArrayList.size == 0) {
            NetworkManager.getInstance(context).homeRedeem(context, object : NetworkResultListener {
                override fun executeOnError(error: VolleyError?) {
                    returnval = false
                }

                override fun executeOnSuccess(response2: JSONObject?) {


                    var response = null


                    if (AppConfig.isHardCodedPromoResponse) {
                        val redeemListDataArrayList = RedeemListData.parseRedeemList(AppConfig.response1)
                        setPromoList(redeemListDataArrayList)
                    } else {
                        val redeemListDataArrayList = RedeemListData.parseRedeemList(response2)
                        setPromoList(redeemListDataArrayList)
                    }

                    val profileEventType = redeemMap.get("location_change")
                    if (profileEventType != null && profileEventType.size > 0) {


                        for (redeemlist /*:RedeemListData*/ in profileEventType) {
                            if (redeemlist.attributes.promo_type == "location_change") {
//                                if (redeemlist.attributes.profile_event_type == "location_change") {
                                var list = SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(context, false)
                                if (list == null) {
                                    list = ArrayList()
                                }
                                val idList = ArrayList<String>()
                                for (subscription in list) {
                                    idList.add(subscription.comp_flg)
                                }
                                if (!idList.contains(redeemlist.id) &&
                                        !UserPref.getInstance().isPromoConsumed(redeemlist.id)) {
                                    returnval = true
                                    if (isActivatePromo) {
                                        createPromo(context, redeemlist.id)
                                        break
                                    }

                                } else if (PlistHelper.getInstance().allow_repeat_promos) {
                                    returnval = true
                                    if (isActivatePromo) {
                                        createPromo(context, redeemlist.id)
                                        break
                                    }

                                }
//                                }
                            }
                        }
                    }

                }
            })
        } else {
            //setPromoList(redeemListDataArrayList)

            val profileEventType = redeemMap.get("location_change")
            if (profileEventType != null && profileEventType.size > 0) {


                for (redeemlist: RedeemListData in profileEventType) {
                    if (redeemlist.attributes.promo_type == "location_change") {
                        /* if (redeemlist.attributes.profile_event_type == "location_change") {*/
                        var list = SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(context, false)
                        if (list == null) {
                            list = ArrayList()
                        }
                        val idList = ArrayList<String>()
                        for (subscription in list) {
                            idList.add(subscription.comp_flg)
                        }
                        if (!idList.contains(redeemlist.id) &&
                                !UserPref.getInstance().isPromoConsumed(redeemlist.id)) {
                            returnval = true
                            if (isActivatePromo) {
                                createPromo(context, redeemlist.id)
                                break
                            }
                        } else if (PlistHelper.getInstance().allow_repeat_promos) {
                            returnval = true
                            if (isActivatePromo) {
                                createPromo(context, redeemlist.id)
                                break
                            }
                        }
//                        }
                    }
                }
            }
        }


        return returnval

    }


    fun lowDataPromoActivation(context: Context): Boolean {
        var returnval = false

        if (redeemListDataArrayList == null || redeemListDataArrayList.size == 0) {
            NetworkManager.getInstance(context).homeRedeem(context, object : NetworkResultListener {
                override fun executeOnError(error: VolleyError?) {
                    returnval = false
                }

                override fun executeOnSuccess(response2: JSONObject?) {


                    var response = null


//                    if (AppConfig.isHardCodedPromoResponse) {
//                        val redeemListDataArrayList = RedeemListData.parseRedeemList(AppConfig.response1)
//                        setPromoList(redeemListDataArrayList)
//                    }
//                    else {
                    val redeemListDataArrayList = RedeemListData.parseRedeemList(response2)
                    setPromoList(redeemListDataArrayList)
//                    }

                    val profileEventType = redeemMap.get("low_data")
                    if (profileEventType != null && profileEventType.size > 0) {


                        for (redeemlist /*:RedeemListData*/ in profileEventType) {
                            if (redeemlist.attributes.promo_type == "low_data") {
//                                if (redeemlist.attributes.profile_event_type == "location_change") {
                                var list = SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(context, false)
                                if (list == null) {
                                    list = ArrayList();
                                }
                                val idList = ArrayList<String>()
                                for (subscription in list) {
                                    idList.add(subscription.comp_flg)
                                }
                                if (!idList.contains(redeemlist.id) &&
                                        !UserPref.getInstance().isPromoConsumed(redeemlist.id)) {
                                    createPromo(context, redeemlist.id)
                                    returnval = true
                                } else if (PlistHelper.getInstance().allow_repeat_promos) {
                                    createPromo(context, redeemlist.id)
                                    returnval = true
                                }
//                                }
                            }
                        }
                    }

                }
            })
        } else {
            //setPromoList(redeemListDataArrayList)

            val profileEventType = redeemMap.get("low_data")
            if (profileEventType != null && profileEventType.size > 0) {


                for (redeemlist: RedeemListData in profileEventType) {
                    if (redeemlist.attributes.promo_type == "low_data") {
                        /* if (redeemlist.attributes.profile_event_type == "location_change") {*/
                        var list = SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(context, false)
                        if (list == null) {
                            list = ArrayList()
                        }
                        val idList = ArrayList<String>()
                        for (subscription in list) {
                            idList.add(subscription.comp_flg)
                        }
                        if (!idList.contains(redeemlist.id) &&
                                !UserPref.getInstance().isPromoConsumed(redeemlist.id)) {
                            createPromo(context, redeemlist.id)
                            returnval = true
                        } else if (PlistHelper.getInstance().allow_repeat_promos) {
                            createPromo(context, redeemlist.id)
                            returnval = true
                        }
//                        }
                    }
                }
            }
        }


        return returnval

    }

    fun getAllRedeemOffers(context: Context) {
        if (AppConfig.promo_offer) {
            if (TrafficController.getInstance(context).isPaid) {
                return
            }
            VpnApisHelper.isPromoListFetched = false
            if (AppConfig.isHardCodedPromoResponse) {
                val redeemListDataArrayList = RedeemListData.parseRedeemList(AppConfig.response1)
                PromoHelper.instance.setPromoList(redeemListDataArrayList)
            } else if (redeemListDataArrayList.size == 0) {
                NetworkManager.getInstance(context).homeRedeem(context, object : NetworkResultListener {
                    override fun executeOnError(error: VolleyError?) {
                        VpnApisHelper.isPromoListFetched = true
                    }

                    override fun executeOnSuccess(response: JSONObject?) {
                        VpnApisHelper.isPromoListFetched = true
                        val redeemListDataArrayList = RedeemListData.parseRedeemList(response)
                        SubscriptionHelper.getInstance().updateRedeemList(redeemListDataArrayList)
                        PromoHelper.instance.setPromoList(redeemListDataArrayList)
                        PromoHelper.instance.checkForUserValidity(context)
                        EventBus.getDefault().post(AllPromoFetched())
                    }
                })
            }
        }
    }


    fun checkForUserValidity(context: Context) {
        if (!VpnApisHelper.instance.shouldCheckPromoValidaity(context)) {
            return
        }

        if (TrafficController.getInstance(context).isPaid) {
            return
        }
        if (isPromoChecked) {
            return
        }
        if (TrafficController.getInstance(context).isRegistered) {
            EventBus.getDefault().post(CheckingPromo())
            val list = SubscriptionHelper.getInstance().getAllCanceledSubscriptionsList(context, false)
            if (list == null) {
                return
            }
            var data = isAnyNewUserPromo(context)
            if (data is Boolean) {
                data = isAnyDataRangePromo(context)
            }
            if (data is RedeemListData) {
                isPromoChecked = true
                if (AppConfig.reddem_offer_show_msg_befor_act_promo) {
                    FinjanDialogBuilder(context).setTitle(PROMO_TITLE).setMessage(PROMO_MSG)
                            .setPositiveButton(ResourceHelper.getInstance().getString(R.string.alert_ok))
                            .setNegativeButton(ResourceHelper.getInstance().getString(R.string.alert_cancel))
                            .setDialogClickListener(object : FinjanDialogBuilder.PositiveClickListener {
                                override fun onPositivClick() {
                                    createPromo(context, (data as RedeemListData).id)
                                }
                            }).show()
                } else {
                    if (!pendingDeepLinkingPromo) {
                        createPromo(context, data.id)
                    }
                }
            }
            PromoHelper.discountedRedeemListData = null
            if (data is Boolean) {
                data = isAnyDiscountPromo(context)
                if (data is RedeemListData) {
                    PromoHelper.discountedRedeemListData = data
                    EventBus.getDefault().post(DiscountedPromoAvailable(data))
                }
            }
        }
    }
}