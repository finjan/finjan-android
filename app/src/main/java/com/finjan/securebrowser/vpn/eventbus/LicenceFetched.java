package com.finjan.securebrowser.vpn.eventbus;

import org.json.JSONObject;

/**
 * Created by anurag on 31/10/17.
 */

public class LicenceFetched {
    private JSONObject jsonObject;
    private boolean shouldRefresUi;
    public LicenceFetched(){}
    public LicenceFetched(JSONObject jsonObject, boolean shouldRefresUi){
        this.jsonObject = jsonObject;
        this.shouldRefresUi = shouldRefresUi;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }
    public boolean getShouldRefresUi() {
        return shouldRefresUi;
    }
}
