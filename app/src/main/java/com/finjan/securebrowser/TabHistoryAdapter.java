package com.finjan.securebrowser;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;

import java.util.ArrayList;
import java.util.List;

import webHistoryDatabase.tabhistory;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */



public abstract class TabHistoryAdapter extends RecyclerView.Adapter<TabHistoryAdapter.MyViewHolder> {

    private List<tabhistory> webhistoryList = new ArrayList<>();
    private Integer reportColor,buttonImage;


    private Context _context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_url;
        public ImageView iv_status, iv_delete;
        public LinearLayout ll_views ;
        public ViewGroup rl_child_parent;

        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_url = (TextView) view.findViewById(R.id.tv_url);
            ll_views = (LinearLayout)view.findViewById(R.id.ll_views);
            iv_status = (ImageView) view.findViewById(R.id.iv_status);
            iv_delete = (ImageView) view.findViewById(R.id.iv_delete);
//            tv_type = (TextView) view.findViewById(R.id.tv_url_type);
            rl_child_parent=(ViewGroup)view.findViewById(R.id.rl_tab_child_view);

            //set theme
            rl_child_parent.setBackground(ResourceHelper.getInstance().getDrawable(isPrivate,R.drawable.bg_tab_card_private_selectror,
                    R.drawable.bg_tab_card_normal_selector));
            tv_title.setTextColor(ResourceHelper.getInstance().getColor(isPrivate,R.color.white,R.color.tab_desc));
            tv_url.setTextColor(ResourceHelper.getInstance().getColor(isPrivate,R.color.tab_desc,R.color.content_color));
        }
    }

    private boolean isPrivate;
    public TabHistoryAdapter(Context context , List<tabhistory> webhistoryList,boolean isPrivate) {
        this.webhistoryList = webhistoryList;
        this._context=context;
        this.isPrivate=isPrivate;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tab_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        tabhistory tabhistory = webhistoryList.get(position);
        Long id = tabhistory.getId();
        holder.tv_title.setText(tabhistory.getPagetitle().isEmpty()?"New Tab":tabhistory.getPagetitle());
        holder.tv_url.setText(tabhistory.getUrl().isEmpty()?"":tabhistory.getUrl());
//        holder.tv_type.setText(tabhistory.getType().isEmpty()?"":tabhistory.getType());
        holder.iv_delete.setTag(R.string.view_tag_idUrl,id);
        holder.iv_delete.setTag(R.string.view_tag_position,position);
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDel(v,String.valueOf(v.getTag(R.string.view_tag_idUrl)),holder.getLayoutPosition());
            }
        });
        holder.rl_child_parent.setTag(R.string.view_tag_position,position);
        holder.rl_child_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUrlClicks(v,holder.getLayoutPosition(),String.valueOf(v.getTag(R.string.view_tag_idUrl)));
            }
        });
        NativeHelper.getInstnace().setStatusIcon(_context,tabhistory.getRating(),holder.iv_status);
//        buttonImage = R.drawable.safe;

//        if (TextUtils.isEmpty(tabhistory.getType())){
//            holder.tv_type.setVisibility(View.INVISIBLE);
//            if (tabhistory.getRating().equals("dangerous")) {
//                buttonImage = R.drawable.danger;
//                holder.itemView.setBackground(_context.getResources() .getDrawable(R.drawable.red_tab_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.red_tab_drawable));
//            } else if (tabhistory.getRating().equals("suspicious")) {
//                buttonImage = R.drawable.suspicious;
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.orange_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.orange_drawable));
//            } else if (tabhistory.getRating().equals("safe")) {
//                buttonImage = R.drawable.safe;
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.green_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.green_drawable));
//            } else if (tabhistory.getRating().equals("invalid")) {
//                Logger.logD("Invalid", "Invalid");
//            }else if (tabhistory.getRating().equals("green")){
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.green_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.green_drawable));
//            }else if (tabhistory.getRating().equals("blue")){
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.blue_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.blue_drawable));
//            }else{
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.green_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.green_drawable));
//            }
//        }
//       else{
//            holder.tv_type.setVisibility(View.VISIBLE);
//            if (tabhistory.getRating().equals(AppConstants.RATING_DANGEROUS)) {
//                buttonImage = R.drawable.danger;
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.private_red_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.private_red_drawable));
//            } else if (tabhistory.getRating().equals(AppConstants.RATING_SUSPICIOUS)) {
//                buttonImage = R.drawable.suspicious;
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.private_orange_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.private_orange_drawable));
//            } else if (tabhistory.getRating().equals(AppConstants.RATING_SAFE)) {
//                buttonImage = R.drawable.safe;
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.private_green_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.private_green_drawable));
//            } else if (tabhistory.getRating().equals(AppConstants.RATING_INVALID)) {
//                Logger.logD("Invalid", "Invalid");
//            }else if (tabhistory.getRating().equals(AppConstants.RATING_GREEN)){
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.private_green_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.private_green_drawable));
//            }else if (tabhistory.getRating().equals(AppConstants.RATING_BLUE)){
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.private_blue_draewble));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.private_blue_draewble));
//            }else{
//                holder.itemView.setBackground(_context.getResources().getDrawable(R.drawable.private_green_drawable));
//                holder.rl_child_parent.setBackground(_context.getResources().getDrawable(R.drawable.private_green_drawable));
//            }
//            holder.tv_type.setTextColor(_context.getResources().getColor(R.color.fj_white));
//            holder.tv_type.setBackground(_context.getResources().getDrawable(R.drawable.private_text_drawable));

//        }
//        holder.iv_status.setImageResource(buttonImage);

    }

    @Override
    public int getItemCount() {
        return webhistoryList.size();
    }
    public abstract void onClickDel(View view, String idUrl, int position);
    public abstract void onUrlClicks(View view,int position,String idUrl);
}