package com.finjan.securebrowser;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.model.SafetyResult;
import com.finjan.securebrowser.model.SearchResult;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class SearchResultCell extends LinearLayout {

    private TextView title;
    private TextView url;
    private TextView desc;
    private TextView ratingLabel;

    private View ratingView;
    private View contentView;

    private ImageView ratingImage;

    private ProgressBar spinner;


    public int cellIndex = -1;
    public SearchResultPage searchResultPage = null;

    public SearchResultCell(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        // Content View
        contentView = findViewById(R.id.contentView);
        title = (TextView) findViewById(R.id.title);
        url = (TextView) findViewById(R.id.url);
        desc = (TextView) findViewById(R.id.description);

        // Rating View
        ratingView = findViewById(R.id.ratingView);
        ratingLabel = (TextView) findViewById(R.id.ratingLabel);
        ratingImage = (ImageView) findViewById(R.id.ratingImage);
        spinner = (ProgressBar)findViewById(R.id.loadingIcon);

        // Fonts
        Typeface robotoLight = Typeface.createFromAsset(getContext().getAssets(), "fonts/sf-ui-regular.ttf");
        Typeface robotoMedium = Typeface.createFromAsset(getContext().getAssets(), "fonts/sf-ui-regular.ttf");

        title.setTypeface(robotoMedium);
        url.setTypeface(robotoLight);
        desc.setTypeface(robotoLight);
        ratingLabel.setTypeface(robotoMedium);

        ratingView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // TODO remove the search result listener
                    // searchResultPage.ratingTouched(cellIndex);
                }
                return true;
            }
        });

//        contentView.setOnTouchListener(new OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                    searchResultPage.contentTouched(cellIndex);
//                }
//                return true;
//            }
//        });
        contentView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                searchResultPage.contentTouched(cellIndex);
            }
        });

        // Initialize contents
        updateContents(null);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void updateContents(SearchResult result) {
        if (result != null) {
            title.setText(result.name);
            url.setText(result.displayUrl);
            desc.setText(result.snippet);
//            title.setText(result.name);
//            url.setText(result.displayUrl);
//            desc.setText(result.snippet);
            //TODO search REsult page
//            updateSafetyRating(result.safetyResult);
        } else {
            // TODO: reset the cell to some good empty values
            title.setText("LOADING");
            url.setText("LOADING");
            desc.setText("LOADING");
            ratingLabel.setText("LOADING");
            ratingImage.setImageDrawable(getResources().getDrawable(R.drawable.oval_472_copy_6));
            spinner.setVisibility(View.VISIBLE);
            updateSafetyRating(null);
        }
    }


    public void updateSafetyRating(SafetyResult safetyResult) {
        String ratingDescription = "SCANNING";
        int textColor = R.color.fj_search_grey_fg;
        int backgroundColor = R.color.fj_search_grey_bg;
        Drawable ratingImage = getResources().getDrawable(R.drawable.oval_472_copy_6);
        boolean loadingIndicatorHidden = false;
        if (safetyResult != null) {
            loadingIndicatorHidden = true;
            if (safetyResult.status.equals("overloaded")) {
                ratingDescription = "ERROR";
                ratingImage = getResources().getDrawable(R.drawable.error_search_icon);
            } else if (safetyResult.rating.equals("safe")) {
                ratingDescription = "SAFE";
                textColor = R.color.fj_search_green_fg;
                backgroundColor = R.color.fj_search_green_bg;
                ratingImage = getResources().getDrawable(R.drawable.safe_search_icon);
            } else if (safetyResult.rating.equals("suspicious")) {
                ratingDescription = "CAUTION";
                textColor = R.color.fj_search_yellow_fg;
                backgroundColor = R.color.fj_search_yellow_bg;
                ratingImage = getResources().getDrawable(R.drawable.caution_search_icon);
            } else if (safetyResult.rating.equals("dangerous")) {
                ratingDescription = "DANGER";
                textColor = R.color.fj_search_red_fg;
                backgroundColor = R.color.fj_search_red_bg;
                ratingImage = getResources().getDrawable(R.drawable.danger_search_icon);
            } else {
                Logger.logD("SearchResultCell", "got an invalid safety result!");
                loadingIndicatorHidden = false;
            }
        }

        ratingLabel.setText(ratingDescription);
        ratingLabel.setTextColor(getResources().getColor(textColor));
        ratingView.setBackgroundColor(getResources().getColor(backgroundColor));
        this.ratingImage.setImageDrawable(ratingImage);
        this.ratingImage.setVisibility(VISIBLE);
        spinner.setVisibility(loadingIndicatorHidden ? View.GONE : View.VISIBLE);
    }

    public void updateSafetyRatingAppPurchaseFalse() {
        String ratingDescription = "Upgrade to scan";
        int textColor = R.color.fj_search_grey_fg;
        int backgroundColor = R.color.fj_search_grey_bg;
        boolean loadingIndicatorHidden = false;
        ratingLabel.setText(ratingDescription);
        ratingLabel.setTextColor(getResources().getColor(textColor));
        ratingView.setBackgroundColor(getResources().getColor(backgroundColor));
        this.ratingImage.setVisibility(INVISIBLE);
        spinner.setVisibility( View.GONE );
    }
}
