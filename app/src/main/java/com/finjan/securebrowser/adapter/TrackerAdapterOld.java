package com.finjan.securebrowser.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.model.TrackerCount;
import com.finjan.securebrowser.model.TrackerFoundModel;
import com.finjan.securebrowser.model.TrackerListModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Adapter showing when trackers are enable from browser settings, which shows all enabled disabled
 * trackers for current web page (old one)
 */

public class TrackerAdapterOld extends RecyclerView.Adapter<TrackerAdapterOld.MyViewHolder> {

    public UpdateTrackerDatabaseListener listener;
    private List<TrackerFoundModel> webhistoryList = new ArrayList<>();
    private String name = "";
    private Context mContext;
    public TrackerAdapterOld(Context mContext, List<TrackerFoundModel> webhistoryList) {
        this.webhistoryList = webhistoryList;
        this.mContext = mContext;
    }

    private HashMap<String,Boolean> defaultKeyList;
    private HashMap<String,Boolean> modifiedKeyList;

    public HashMap<String,Boolean> getDefaultKeyList(){return defaultKeyList;}
    public HashMap<String,Boolean> getModifiedKeyList(){return modifiedKeyList;}
    public void setListener(UpdateTrackerDatabaseListener updateTrackerDatabaseListener) {
        listener = updateTrackerDatabaseListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.layout_tracker_child_old, parent, false);
        defaultKeyList=new HashMap<>();
        modifiedKeyList=new HashMap<>();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final TrackerFoundModel webhistory = webhistoryList.get(position);
        holder.tv_title.setText(webhistory.getTrackerType());
        holder.trackerLayout.removeAllViews();
        final ArrayList<TrackerCount> trackerNameList = webhistory.getTrackerCounts();

        for (int i = 0; i < trackerNameList.size(); i++) {
            View trackerNameLayout = LayoutInflater.from(mContext).inflate(R.layout.layout_trackername, null);
            final TextView nameTextView = (TextView) trackerNameLayout.findViewById(R.id.txt_tracker_name);
            TextView tv_count = (TextView) trackerNameLayout.findViewById(R.id.txt_tracker_count);
//            final SwitchCompat trackerSwitch = (SwitchCompat) trackerNameLayout.findViewById(R.id.tracker_on_switch);
            name = trackerNameList.get(i).getName();
            if (webhistory.getCheckedString().contains(name)) {
//                trackerSwitch.setChecked(false);
                nameTextView.setPaintFlags(nameTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
//                trackerSwitch.setChecked(true);
                nameTextView.setPaintFlags(nameTextView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }
            nameTextView.setText(trackerNameList.get(i).getName());
//            defaultKeyList.put(nameTextView.getText().toString(),trackerSwitch.isChecked());


//            trackerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    trackerSwitch.getThumbDrawable().setColorFilter(isChecked ? ResourceHelper.getInstance().getColor(R.color.fj_tracker_button_colort):Color.WHITE, PorterDuff.Mode.MULTIPLY);
//                    if (isChecked) {
//                        String dataString = webhistory.getCheckedString();
//                        dataString.replace(name, "");
//                        nameTextView.setPaintFlags(nameTextView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
//                        if (listener != null) {
//                            listener.onUpdateTrackerDatabase(getTrackerList(nameTextView.getText().toString(), webhistory.getTrackerURL()), !isChecked);
//                        }
//                    } else {
//                        webhistory.setCheckedString(webhistory.getCheckedString() + Constants.URL_SEPRATOR + name);
//                        nameTextView.setPaintFlags(nameTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                        if (listener != null) {
//                            listener.onUpdateTrackerDatabase(getTrackerList(nameTextView.getText().toString(), webhistory.getTrackerURL()), !isChecked);
//                        }
//                    }
//                    modifiedKeyList.put(nameTextView.getText().toString(),isChecked);
//                }
//            });
            int count = trackerNameList.get(i).getCount();
            String countValue = count + "" + (count == 1 ? " request" : " requests");
            tv_count.setText(countValue);
            View bottomLine = (View) trackerNameLayout.findViewById(R.id.view_line);
            if (i == trackerNameList.size() - 1) {
                bottomLine.setVisibility(View.GONE);
            } else {
                bottomLine.setVisibility(View.VISIBLE);
            }

            holder.trackerLayout.addView(trackerNameLayout);

        }
    }

    private ArrayList<TrackerListModel> getTrackerList(String tackerName, ArrayList<TrackerListModel> trackerURL) {
        ArrayList<TrackerListModel> trackerList = new ArrayList<>();
        for (TrackerListModel url : trackerURL) {
            if (url.getTrackerName().equalsIgnoreCase(tackerName)) {
                trackerList.add(url);
            }
        }
        return trackerList;
    }

    @Override
    public int getItemCount() {
        return webhistoryList.size();
    }

    public interface UpdateTrackerDatabaseListener {
        void onUpdateTrackerDatabase(List<TrackerFoundModel> webhistoryList,ArrayList<TrackerListModel> urlList, boolean shouldAdd);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ArrayList<String> URlList = new ArrayList<>();
        private TextView tv_title;
        private ViewGroup trackerLayout;


        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.txt_tracker_header);
                trackerLayout = (LinearLayout) view.findViewById(R.id.lyt_tracker_name);

        }
    }

}