package com.finjan.securebrowser.adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.buildware.widget.indeterm.IndeterminateCheckBox;
import com.finjan.securebrowser.Constants;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.custom_views.CustomIndeterminateCheckBox;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.model.TrackerCount;
import com.finjan.securebrowser.model.TrackerFoundModel;
import com.finjan.securebrowser.model.TrackerListModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Adapter showing when trackers are enable from browser settings, which shows all enabled disabled
 * trackers for current web page
 */


public class TrackerAdapter extends BaseExpandableListAdapter {

    private LayoutInflater layoutInflator;
    private List<TrackerFoundModel> webhistoryList;
    private Context mContext;
    private TrackerExpendableListener listener;

    public TrackerAdapter(Context mContext, List<TrackerFoundModel> webhistoryList) {
        this.webhistoryList = webhistoryList;
        this.mContext = mContext;
        layoutInflator = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListener(TrackerExpendableListener context) {
        listener = context;
    }

    @Override
    public int getGroupCount() {
        int count = 0;
        for (TrackerFoundModel trackerFoundModel : webhistoryList) {
            if (trackerFoundModel.getTrackerCounts() != null &&
                    trackerFoundModel.getTrackerCounts().size() > 0) {
                count = count + 1;
            }
        }
        return count;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return webhistoryList.get(groupPosition).getTrackerCounts().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflator.inflate(R.layout.layout_tracker_group, null);
            if(groupPosition==0){
                convertView.findViewById(R.id.view_line).setVisibility(View.GONE);
            }
        }
        TextView header = (TextView) convertView.findViewById(R.id.txt_tracker_header);
        final CustomIndeterminateCheckBox headerCB = (CustomIndeterminateCheckBox) convertView.findViewById(R.id.indeterm_checkbox);
        final TrackerFoundModel groupData = webhistoryList.get(groupPosition);
        header.setText(groupData.getTrackerType());
        manageHeaderCheckBox(headerCB, groupData, groupPosition);
        return convertView;
    }

    private void manageHeaderCheckBox(final CustomIndeterminateCheckBox headerCB, final TrackerFoundModel groupData, final int groupPosition) {
        if (!GlobalVariables.blockTracking) {
            headerCB.setVisibility(View.GONE);
            return;
        }
        headerCB.setOnStateChangedListener(null);
        Boolean checkMarks = isHeaderChecked(groupData);
        if (checkMarks == null) {
            headerCB.setIndeterminate(true);
        } else if (checkMarks) {
            headerCB.setChecked(false);
        } else {
            headerCB.setChecked(true);
        }
        headerCB.setHighlightColor(mContext.getResources().getColor(R.color.accent_color_1));
        headerCB.setOnStateChangedListener(new IndeterminateCheckBox.OnStateChangedListener() {
            @Override
            public void onStateChanged(IndeterminateCheckBox indeterminateCheckBox, @Nullable Boolean state) {

                listener.onCheckGroupIndicatorClick(webhistoryList, groupData.getTrackerURL(), !state, groupPosition);
                for (TrackerCount trackerCount : groupData.getTrackerCounts()) {
                    if (state == null || state) {
                        trackerCount.setIschecked(false);
                    } else {
                        trackerCount.setIschecked(true);
                    }
                }
            }
        });
    }

    private Boolean isHeaderChecked(TrackerFoundModel groupData) {
        String checkTracker = groupData.getCheckedString();
        if (TextUtils.isEmpty(checkTracker)) {
            return false;
        }
        String[] trackerArray = checkTracker.split(Pattern.quote(Constants.URL_SEPRATOR));
        ArrayList<String> stringArrayList = new ArrayList<String>(Arrays.asList(trackerArray));
        stringArrayList.removeAll(Arrays.asList("", null));

        if (stringArrayList == null || stringArrayList.size() == 0) {
            return false;
        }
        int count = 0;
        for (TrackerCount trackerCount : groupData.getTrackerCounts()) {
            count = count + trackerCount.getCount();
        }
        if (stringArrayList.size() == count) {
            return true;
        } else {
            return null;
        }
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflator.inflate(R.layout.layout_tracker_child, null);
        }
        final TextView trackerNameTV = (TextView) convertView.findViewById(R.id.txt_tracker_name);
        final TextView trackerCountTV = (TextView) convertView.findViewById(R.id.txt_tracker_count);
        SwitchCompat trackerSwitch = (SwitchCompat) convertView.findViewById(R.id.tracker_on_switch);
        try {
            final TrackerFoundModel trackerData = webhistoryList.get(groupPosition);
            final TrackerCount data = trackerData.getTrackerCounts().get(childPosition);
            trackerNameTV.setText(data.getName());

            int count = data.getCount();
            trackerCountTV.setText(count + "" + (count == 1 ? " Request" : " Requests"));
            manageChildSwitch(trackerSwitch,trackerNameTV,trackerData,groupPosition,childPosition,convertView);

        } catch (ArrayIndexOutOfBoundsException e) {
        } catch (Exception e) {
        }
        return convertView;
    }
    private void manageChildSwitch(final SwitchCompat trackerSwitch,final TextView trackerNameTV,
                                   final TrackerFoundModel trackerData,final int groupPosition,
                                   final int childPosition,View convertView){
        if(!GlobalVariables.blockTracking){
            trackerSwitch.setVisibility(View.GONE);
            return;
        }
        trackerSwitch.setOnCheckedChangeListener(null);
        final TrackerCount data = trackerData.getTrackerCounts().get(childPosition);
        final String name = data.getName();

        if (trackerData.getCheckedString().contains(name)) {
            trackerSwitch.setChecked(false);
            convertView.setAlpha(.5f);
//            trackerNameTV.setPaintFlags(trackerNameTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            trackerSwitch.setChecked(true);
            convertView.setAlpha(1f);
//            trackerNameTV.setPaintFlags(trackerNameTV.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
        trackerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

//                        trackerData.setCheckedString(trackerData.getCheckedString() + Constants.URL_SEPRATOR + name);
//                        trackerNameTV.setPaintFlags(trackerNameTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    if (listener != null) {
                        listener.onSwitchChildIndicatorClick(webhistoryList, getTrackerList(trackerNameTV.getText().toString(),
                                trackerData.getTrackerURL(), false), groupPosition, childPosition);
                    }
                } else {

//                        String dataString = trackerData.getCheckedString();
//                        dataString.replace(name, "");
//                        trackerNameTV.setPaintFlags(trackerNameTV.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    if (listener != null) {
                        listener.onSwitchChildIndicatorClick(webhistoryList, getTrackerList(trackerNameTV.getText().toString(),
                                trackerData.getTrackerURL(), true), groupPosition, childPosition);
                    }
                }
            }
        });
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void updateList(List<TrackerFoundModel> webhistoryListNew) {
//        this.webhistoryList.clear();
//        this.webhistoryList.addAll(webhistoryListNew);
        notifyDataSetChanged();
    }

    private ArrayList<TrackerListModel> getTrackerList(String tackerName, ArrayList<TrackerListModel> trackerURL, boolean isChecked) {
        ArrayList<TrackerListModel> trackerList = new ArrayList<>();
        for (TrackerListModel url : trackerURL) {
            if (url.getTrackerName().equalsIgnoreCase(tackerName)) {
                trackerList.add(new TrackerListModel(url.getTrackerName(), url.getTrackerURl(), isChecked));
            }
        }
        return trackerList;
    }

    public List<TrackerFoundModel> getCurrentList() {
        return webhistoryList;
    }

    public interface TrackerExpendableListener {
        void onSwitchChildIndicatorClick(List<TrackerFoundModel> webhistoryList, ArrayList<TrackerListModel> urlList, int groupPosition, int childPosition);

        void onCheckGroupIndicatorClick(List<TrackerFoundModel> webhistoryList, ArrayList<TrackerListModel> trackerURL, boolean isChecked, int groupPosition);
    }
}
