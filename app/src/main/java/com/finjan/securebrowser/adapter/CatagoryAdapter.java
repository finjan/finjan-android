package com.finjan.securebrowser.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.custom.ContentDialog;
import com.finjan.securebrowser.model.WatchCatsModel;

public class CatagoryAdapter extends RecyclerView.Adapter<CatagoryAdapter.CatagoryViewHolder> {

    private Context context;
    private WatchCatsModel watchCatsModel;

    public CatagoryAdapter(Context context, WatchCatsModel watchCatsModel) {
        this.context = context;
        this.watchCatsModel = watchCatsModel;
    }

    @Override
    public CatagoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.view_vpn_drawer,parent,false);
        return new CatagoryViewHolder(view);

    }

    @Override
    public void onBindViewHolder(CatagoryViewHolder holder, int position) {
       holder.illegalImage.setVisibility(View.GONE);
       final WatchCatsModel.Category category  = watchCatsModel.getData().get(position);
       holder.tvCategoryName.setText(category.getAttributes().getTitle());
       holder.tvCategoryName.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ContentDialog contentDialog = new ContentDialog(context,category);
               contentDialog.show();
           }
       });
    }

    @Override
    public int getItemCount() {
        return watchCatsModel.getData().size();
    }


    class CatagoryViewHolder extends RecyclerView.ViewHolder{
        TextView tvCategoryName;
        ImageView illegalImage;
        public CatagoryViewHolder(View itemView) {
            super(itemView);
            tvCategoryName = (TextView)itemView.findViewById(R.id.tv_title);
            illegalImage = (ImageView)itemView.findViewById(R.id.iv_icon);
        }
    }
}
