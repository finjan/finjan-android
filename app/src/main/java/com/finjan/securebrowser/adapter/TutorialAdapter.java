package com.finjan.securebrowser.adapter;

import android.app.Activity;
import android.content.Context;

import androidx.viewpager.widget.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.TutorialActivity;
import com.finjan.securebrowser.vpn.ui.authentication.SetupAccountActivity;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;

import java.util.HashMap;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Screen responsible for showing Tutorial screen
 * Adpater used in TutorialActivity for showing tutorials of application
 */

public class TutorialAdapter extends PagerAdapter {


    private Context mContext;
    private String[] mResources;
    private HashMap<Integer,LottieAnimationView> lottieAnimationViewHashMap=new HashMap<>();

    public TutorialAdapter(Context mContext, String[] mResources) {
        this.mContext = mContext;
        this.mResources = mResources;
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView=null;
//        if(position==4){
//            itemView = LayoutInflater.from(mContext).inflate(R.layout.tutorial_pager_last_item, container, false);
//            UserPref.getInstance().setIsTutorialScreenShowed(true);
//            setClickListener(itemView);
//        }else {
//            itemView = LayoutInflater.from(mContext).inflate(R.layout.tutorial_pager_item, container, false);
////            setSkip(itemView);
//        }
        itemView = LayoutInflater.from(mContext).inflate(R.layout.tutorial_pager_item, container, false);


        LottieAnimationView imageView = (LottieAnimationView) itemView.findViewById(R.id.img_pager_item);
        try {
            imageView.setAnimation(mResources[position]);
        }catch (Exception ex){
            imageView.setAnimation("screen_"+(position+1)+".json");
        }

        container.addView(itemView);
        lottieAnimationViewHashMap.put(position,imageView);
        return itemView;
    }
    public LottieAnimationView getLoggtieImg(int position){return lottieAnimationViewHashMap.get(position);}




    private void setSkip(View view){
        if(view!=null){
            TextView tvSkip= (TextView) view.findViewById(R.id.tv_skip);
            tvSkip.setText(Html.fromHtml("<u>" + "Skip"+ "</u>"));
            tvSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                   TutorialAdapter.this.viewPager.setCurrentItem(4);
                }
            });
        }
    }
    private void setClickListener(View view){
        if(view!=null){
            view.findViewById(R.id.tv_sign_up).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    UserPref.getInstance().setMyLastPage(ScreenNavigation.VPN_SCREEN);
                    SetupAccountActivity.newLoginInstanceForResult((Activity) mContext, VpnActivity.REQUEST_SIGNUP, SetupAccountActivity.ACTION_SIGN_UP);
                }
            });
            view.findViewById(R.id.tv_sign_up_later).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    TutorialActivity.finishTutorial((Activity) mContext);
//                    UserPref.getInstance().setMyLastPage(ScreenNavigation.HOME_SCREEN);
//                    ScreenNavigation.getInstance().startHome(mContext);
                }
            });
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
