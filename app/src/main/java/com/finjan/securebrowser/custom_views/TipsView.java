package com.finjan.securebrowser.custom_views;

import android.content.Context;
import android.graphics.Canvas;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to draw tips screen when user get browser first time after switch to browser
 *
 */

public class TipsView extends View {
    public TipsView(Context context) {
        super(context);
    }

    public TipsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TipsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TipsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
    private void drawVpnTip(Canvas canvas){

    }
    private void drawTabTip(Canvas canvas){

    }
    private void drawHomeTip(Canvas canvas){

    }
    private void drawBookmarkTip(Canvas canvas){

    }
    private void drawDrawerTip(Canvas canvas){

    }
    private void drawScanTip(Canvas canvas){

    }
    private void drawTrackerTip(Canvas canvas){
        
    }
}
