package com.finjan.securebrowser.custom_views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to draw custom passkey on Finger_Passcode_Activity,PasscodeActivity and PasscodeSet
 *
 */

public class PasskeyIconView extends AppCompatImageView {


    public PasskeyIconView(Context context) {
        super(context);
        init(context);
    }

    public PasskeyIconView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PasskeyIconView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(final Context context) {
    }

    public  void setOnTouch(final PasskeyListener listener,final Context context){
        this.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float lastY = 0;
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        PasskeyIconView.this.setTintColour(context, android.R.color.white);
                        PasskeyIconView.this.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.solid_circle));
                        lastY = event.getY();
                        listener.onTouchKey();
                        break;
                    case MotionEvent.ACTION_UP:
                        PasskeyIconView.this.setTintColour(context, R.color.desc_colour);
                        PasskeyIconView.this.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.circle_border));
                        break;
                    case MotionEvent.ACTION_MOVE:
                        float abs = Math.abs(lastY - event.getY());

//                        if(abs > TIME_DELAY) // TIME_DELAY=2
//                            v.setBackgroundResource(R.drawable.normal);
                        break;
                }
                return true;
            }
        });
    }

    private void setTintColour(Context context, int colour) {
        colour =ResourceHelper.getInstance().getColor(colour);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.setImageTintList(ColorStateList.valueOf(colour));
        }
    }
    public interface PasskeyListener{
        void onTouchKey();
    }
}
