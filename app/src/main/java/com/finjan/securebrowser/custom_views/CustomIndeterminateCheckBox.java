package com.finjan.securebrowser.custom_views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;

import com.buildware.widget.indeterm.IndeterminateCheckBox;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 *
 */

public class CustomIndeterminateCheckBox extends IndeterminateCheckBox {

    public CustomIndeterminateCheckBox(Context context) {
        super(context);
        init();
    }

    public CustomIndeterminateCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomIndeterminateCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init(){
        setPadding(20,20,20,20);
    }

    @Override
    public void setButtonDrawable(Drawable buttonDrawable) {
        if(isIndeterminate()){
            super.setButtonDrawable(ResourceHelper.getInstance().getDrawable(R.drawable.check_stripe));
        }else if(isChecked()){
            super.setButtonDrawable(ResourceHelper.getInstance().getDrawable(R.drawable.check_on));
        }else {
            super.setButtonDrawable(ResourceHelper.getInstance().getDrawable(R.drawable.check_blank));
        }
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if(isIndeterminate()){
            super.setButtonDrawable(ResourceHelper.getInstance().getDrawable(R.drawable.check_stripe));
        }else if(isChecked()){
            super.setButtonDrawable(ResourceHelper.getInstance().getDrawable(R.drawable.check_on));
        }else {
            super.setButtonDrawable(ResourceHelper.getInstance().getDrawable(R.drawable.check_blank));
        }
    }
}
