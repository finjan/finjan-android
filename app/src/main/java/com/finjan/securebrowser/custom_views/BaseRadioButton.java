package com.finjan.securebrowser.custom_views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.util.AttributeSet;

import com.finjan.securebrowser.R;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class to custom radio button with own colour theme
 *
 */

public class BaseRadioButton extends AppCompatRadioButton {

    public BaseRadioButton(Context context) {
        super(context);
    }

    public BaseRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BaseRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context,@Nullable AttributeSet attrs){
        setDefaults(context);
        readAttr(context, attrs);
    }
    private void readAttr(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BaseTextview);

        // Read the title and set it if any
//        String fontName = a.getString(R.styleable.BaseTextview_font) ;
        ColorStateList stateList=a.getColorStateList(R.styleable.BaseTextview_text_color);
        if(stateList!=null){
            this.setTextColor(stateList);
        }
//        if (fontName != null) {
//            Typeface font=Typeface.createFromAsset(context.getAssets(), "fonts/"+sf);
//            this.setTypeface(font);
//            // We have a attribute value and set it to proper value as you want
//        }
        a.recycle();
    }
    private static final String sf="sf-ui-regular.ttf";

    private void setDefaults(Context context){
        Typeface font=Typeface.createFromAsset(context.getAssets(), "fonts/"+sf);
        this.setTypeface(font);
    }
}
