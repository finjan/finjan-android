package com.finjan.securebrowser.helpers;

import android.text.TextUtils;

import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.AppLog;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.SafetyResult;
import com.finjan.securebrowser.model.TrackerListModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import webHistoryDatabase.TrackerDao;
import webHistoryDatabase.bookmarks;
import webHistoryDatabase.bookmarksDao;
import webHistoryDatabase.tabhistory;
import webHistoryDatabase.tabhistoryDao;
import webHistoryDatabase.webhistory;
import webHistoryDatabase.webhistoryDao;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 * Helper class used to make connection and changes in db for
 * writing history and bookmarks while user use browser
 */

public class DbHelper {

    public static final String PRIVATE = "PRIVATE";
    private tabhistoryDao tabhistoryDao;
    private webhistoryDao webhistoryDao;
    private TrackerDao trackerDao;

    private bookmarksDao bookmarksDao;

    private DbHelper() {
        tabhistoryDao = AnalyticsApplication.daoSession.getTabhistoryDao();
        webhistoryDao = AnalyticsApplication.daoSession.getWebhistoryDao();
        bookmarksDao = AnalyticsApplication.daoSession.getBookmarksDao();
        trackerDao = AnalyticsApplication.daoSession.getTrackerDao();
    }

    public static DbHelper getInstnace() {
        return DbHelperHolder.INSTANCE;
    }

    public void addTabHistory(tabhistory tabhistory) {
        if (tabhistoryDao != null) {
            tabhistoryDao.insert(tabhistory);
        }
    }

    private void removeItem(Long id) {
        if (id != null && tabhistoryDao != null) {
            tabhistoryDao.deleteByKey(id);
        }
    }

    public void deleteTab(Long id) {
        if (tabhistoryDao != null) {
            tabhistoryDao.deleteByKey(id);
        }
    }

    public Long addNewTab(SafetyResult safetyResult, String pageTitle, boolean isPrivate) {
        tabhistory tabhistory = getTabHistory(safetyResult, pageTitle, AppConstants.EMPTY_URL, isPrivate);
        addTabHistory(tabhistory);
        return tabhistoryDao.getKey(tabhistory);
    }

    public tabhistory getTabHistoy(Long id) {
        if (id != null) {
            List<tabhistory> tabhistories = tabhistoryDao.queryBuilder().
                    where(webHistoryDatabase.tabhistoryDao.Properties.Id.eq(id)).list();
            if (tabhistories != null && tabhistories.size() > 0) {
                return tabhistories.get(0);
            }
        }
        return null;
    }

    public void updateOldTab(SafetyResult safetyResult, String pageTitle, String url, boolean isPrivate, Long id) {
        tabhistory tabhistory = getTabHistoy(id);
        if (tabhistory == null) {
            return;
        }
        tabhistory = updateEntity(safetyResult, tabhistory, pageTitle, url, isPrivate);
//        removeItem(id);
        tabhistoryDao.update(tabhistory);
    }

    public tabhistory updateEntity(SafetyResult currentSafetyResult, tabhistory tabhistory,
                                   String title, String url, boolean isPrivate) {

        String privateTxt = isPrivate ? PRIVATE : "";
//        tabhistory tabhistory = new tabhistory();
        tabhistory.setPagetitle(title);
        tabhistory.setUrl(url);
        tabhistory.setType(privateTxt);

        if (currentSafetyResult != null) {
            if (!UserPref.getInstance().getSafeScanEnabled()) {
                tabhistory.setRating("blue");
            } else if (UserPref.getInstance().getSafeSearchPurchase()) {
                tabhistory.setRating(currentSafetyResult.getRating());
            } else if (getScanRemain() > 0) {
                tabhistory.setRating(currentSafetyResult.getRating());
            } else {
                tabhistory.setRating("blue");
            }
            tabhistory.setCountry(currentSafetyResult.getCountryName());
            tabhistory.setStatus(currentSafetyResult.status);
            String categoriesNew = "";
            if (currentSafetyResult.categories != null) {
                for (int i = 0; i < currentSafetyResult.categories.length; i++) {
                    if (i == (currentSafetyResult.categories.length - 1)) {
                        categoriesNew += currentSafetyResult.categories[i];
                    } else {
                        categoriesNew += currentSafetyResult.categories[i] + ",";
                    }
                }
            }
            tabhistory.setCategories(categoriesNew);
        } else {
//            if (!UserPref.getInstance().getSafeScanEnabled()) {
//                tabhistory.setRating("blue");
//            } else {
//                tabhistory.setRating((getScanRemain() > 0 || UserPref.getInstance().getSafeSearchPurchase()) ? "green" : "blue");
//            }
            tabhistory.setRating("blue");
            tabhistory.setCountry("");
            tabhistory.setStatus("");
            tabhistory.setCategories("");
        }
        return tabhistory;
    }

    public void addToWebHistory(SafetyResult safetyResult, String title, String url, boolean isPrivate, boolean vtScan) {
        webhistory webhistory = getWebHistroyEntity(safetyResult, title, url);
        if (!isPrivate && webhistoryDao != null) {

            String oldUrl = "";
            String oldHistoryUrl = "";
            String oldTitle = "";

            if (!vtScan) {
                List<webhistory> webDataList = webhistoryDao.loadAll();
                Collections.reverse(webDataList);
                if (webDataList.size() > 0) {
                    oldHistoryUrl = webDataList.get(0).getUrl();
                    oldTitle = webDataList.get(0).getPagetitle();
                }
                if (!oldTitle.equalsIgnoreCase(title) && !oldHistoryUrl.equalsIgnoreCase(url)) {
                    webhistoryDao.insert(webhistory);
                }
            }
        }
    }

    private webhistory getWebHistroyEntity(SafetyResult currentSafetyResult, String title, String url) {
        webhistory webhistory = new webhistory();
        webhistory.setPagetitle(title);
        webhistory.setUrl(url);
        String categories = "";
        if (currentSafetyResult != null) {
            webhistory.setRating(currentSafetyResult.getRating());
            webhistory.setCountry(currentSafetyResult.getCountryName());
            webhistory.setStatus(currentSafetyResult.status);

            if (currentSafetyResult.categories != null) {
                for (int i = 0; i < currentSafetyResult.categories.length; i++) {
                    if (i == (currentSafetyResult.categories.length - 1)) {
                        categories += currentSafetyResult.categories[i];
                    } else {
                        categories += currentSafetyResult.categories[i] + ",";
                    }
                }
            }
        } else {
            if (!UserPref.getInstance().getSafeScanEnabled()) {
                webhistory.setRating("blue");
            } else {
                webhistory.setRating((getScanRemain() > 0 || UserPref.getInstance().getSafeSearchPurchase()) ? "green" : "blue");
            }
            webhistory.setCountry("");
            webhistory.setStatus("");
        }
        webhistory.setCategories(categories);
        webhistory.setDtime(new Date());
        return webhistory;
    }

    private tabhistory getTabHistory(SafetyResult currentSafetyResult, String title, String url, boolean isPrivate) {

        String privateTxt = isPrivate ? PRIVATE : "";
        tabhistory tabhistory = new tabhistory();
        tabhistory.setPagetitle(title);
        tabhistory.setUrl(url);
        tabhistory.setType(privateTxt);

        if (currentSafetyResult != null) {
            if (!UserPref.getInstance().getSafeScanEnabled()) {
                tabhistory.setRating("blue");
            } else if (UserPref.getInstance().getSafeSearchPurchase()) {
                tabhistory.setRating(currentSafetyResult.getRating());
            } else if (getScanRemain() > 0) {
                tabhistory.setRating(currentSafetyResult.getRating());
            } else {
                tabhistory.setRating("blue");
            }
            tabhistory.setCountry(currentSafetyResult.getCountryName());
            tabhistory.setStatus(currentSafetyResult.status);
            String categoriesNew = "";
            if (currentSafetyResult.categories != null) {
                for (int i = 0; i < currentSafetyResult.categories.length; i++) {
                    if (i == (currentSafetyResult.categories.length - 1)) {
                        categoriesNew += currentSafetyResult.categories[i];
                    } else {
                        categoriesNew += currentSafetyResult.categories[i] + ",";
                    }
                }
            }
            tabhistory.setCategories(categoriesNew);
        } else {
//            if (!UserPref.getInstance().getSafeScanEnabled()) {
//                tabhistory.setRating("blue");
//            } else {
//                tabhistory.setRating((getScanRemain() > 0 || UserPref.getInstance().getSafeSearchPurchase()) ? "green" : "blue");
//            }
            tabhistory.setRating("blue");
            tabhistory.setCountry("");
            tabhistory.setStatus("");
            tabhistory.setCategories("");
        }
        return tabhistory;
    }

    private int getScanRemain() {
        int spendScans = UserPref.getInstance().getSafeScanRemain();
        int totalScans = getTotalScans();
        return totalScans - spendScans;
    }

    private boolean isSafeScanUpgradeEnable() {
//        return Utils.getPlistvalue(Constants.KEY_APP_PURCHASE, Constants.KEY_APP_PURCHASE_ENABLE).equalsIgnoreCase("true");
        return PlistHelper.getInstance().getInAppPurchase().isEnabled();
    }

    private int getTotalScans() {
        int days = 0;
//        String daysTotal = Utils.getPlistvalue(Constants.KEY_APP_PURCHASE, Constants.KEY_FREE_SCAN_NUMBER);
//        String daysTotal = PlistHelper.getInstance().getInAppPurchase().getFreeprivacyscan();
//        if (!TextUtils.isEmpty(daysTotal)) {
//            days = Integer.parseInt(daysTotal);
//        }
//        return days;
        return PlistHelper.getInstance().getInAppPurchase().getFreeprivacyscan();
//        return 10;
    }

    public int getTabCount() {
        if (tabhistoryDao != null) {
            return tabhistoryDao.loadAll().size();
        }
        return 0;
    }

    public List<tabhistory> getTabList() {
        if (tabhistoryDao != null) {
            return tabhistoryDao.loadAll();
        }
        return null;
    }

    public List<bookmarks> getBookMarsList() {
        return bookmarksDao.loadAll();
    }

    public bookmarksDao getBookmarksDao() {
        return bookmarksDao;
    }

    public webhistoryDao getWebhistoryDao() {
        return webhistoryDao;
    }

    public tabhistoryDao getTabhistoryDao() {
        return tabhistoryDao;
    }

    public tabhistory makeClone(tabhistory tabhistory) {
        if (tabhistory == null) {
            return null;
        }
        tabhistory tabhistory1 = new tabhistory();
        tabhistory1.setCategories(tabhistory.getCategories());
        tabhistory1.setCountry(tabhistory.getCountry());
        tabhistory1.setPagetitle(tabhistory.getPagetitle());
        tabhistory1.setRating(tabhistory.getRating());
        tabhistory1.setStatus(tabhistory.getStatus());
        tabhistory1.setType(tabhistory.getType());
        tabhistory1.setUrl(tabhistory.getUrl());
        return tabhistory1;
    }

    private bookmarks getBookMark(String url) {
        List<bookmarks> bookmarksList = getBookMarsList();
        for (bookmarks bookmarks : bookmarksList) {
            if (url.equalsIgnoreCase(bookmarks.getUrl())) {
                return bookmarks;
            }
        }
        return null;
    }

    public void deleteBookMark(String url) {
        bookmarks bookmarks = getBookMark(url);
        if (bookmarks != null) {
            bookmarksDao.delete(getBookMark(url));
        }
    }

    public void createBookMark(String url) {
        List<tabhistory> tabhistoryList = getTabList();
        if (tabhistoryList.size() == 0) {
            return;
        }
        Collections.reverse(tabhistoryList);

        bookmarksDao bookmarksDao = AnalyticsApplication.daoSession.getBookmarksDao();
        bookmarks bookmarks = new bookmarks();
        String urlTab = tabhistoryList.get(0).getUrl();
        if (url.equalsIgnoreCase(urlTab)) {
            bookmarks.setUrl(TextUtils.isEmpty(url) ? "" : tabhistoryList.get(0).getUrl());
            bookmarks.setRating(TextUtils.isEmpty(url) ? "" : tabhistoryList.get(0).getRating());
            bookmarks.setCountry(TextUtils.isEmpty(url) ? "" : tabhistoryList.get(0).getCountry());
            bookmarks.setStatus(TextUtils.isEmpty(url) ? "" : tabhistoryList.get(0).getStatus());
            bookmarks.setPagetitle(TextUtils.isEmpty(url) ? "" : tabhistoryList.get(0).getPagetitle());
            bookmarks.setCategories(TextUtils.isEmpty(url) ? "" : tabhistoryList.get(0).getCategories());
        } else {
            bookmarks.setUrl(url);
            bookmarks.setRating("");
            bookmarks.setCountry("");
            bookmarks.setStatus("");
            bookmarks.setPagetitle("");
            bookmarks.setCategories("");
        }
        bookmarksDao.insert(bookmarks);

    }

    public void clearWebHistory() {
        if (webhistoryDao != null) {
            webhistoryDao.deleteAll();
        }
    }

    private void clearTabHistory() {
        if (tabhistoryDao != null) {
            tabhistoryDao.deleteAll();
        }
    }

    public void clearHistory() {
        clearWebHistory();
    }

    private void clearBookMarks() {
        if (bookmarksDao != null) {
            bookmarksDao.deleteAll();
        }
    }

    public tabhistory getLastTab() {
        List<tabhistory> tabhistories = getTabList();
        Collections.reverse(tabhistories);
        if (tabhistories.size() > 0) {
            return tabhistories.get(0);
        }
        return null;
    }

    public String getLastUrl() {
        tabhistory tabhistory = getLastTab();
        if (tabhistory != null && !TextUtils.isEmpty(tabhistory.getUrl())) {
            return tabhistory.getUrl();
        }
        return "";
    }

    public long getNewDefaultId() {
        List<tabhistory> deleteTabList = DbHelper.getInstnace().getTabList();
        Collections.reverse(deleteTabList);
        if (deleteTabList.size() > 0) {
            return deleteTabList.get(0).getId() + 1;
        }
        return 1;
    }

    public void addAllTrackerDB(ArrayList<TrackerListModel> urlList) {
        try {
            for (TrackerListModel model : urlList) {
                List<webHistoryDatabase.Tracker> dataBaseTable = trackerDao.loadAll();
                if (dataBaseTable != null && !dataBaseTable.contains(model.getTrackerURl())) {
                    webHistoryDatabase.Tracker tracker = new webHistoryDatabase.Tracker();
                    tracker.setUrl(model.getTrackerURl());
                    trackerDao.insert(tracker);
                }
            }
        } catch (Exception e) {
            AppLog.printLog(e.getMessage());

        }
    }

    public void removeAllTrackerDB(ArrayList<TrackerListModel> urlList) {
        try {
            for (int i = 0; i < urlList.size(); i++) {
                List<webHistoryDatabase.Tracker> dataBaseTable = trackerDao.loadAll();
                if (dataBaseTable != null && dataBaseTable.size() > 0) {
                    for (int j = 0; j < dataBaseTable.size(); j++) {
                        if (urlList.get(i).getTrackerURl().equalsIgnoreCase(dataBaseTable.get(j).getUrl())) {
                            trackerDao.deleteByKey(dataBaseTable.get(j).getId());
                        }
                    }
                } else {
                    break;
                }
            }
        } catch (Exception e) {
            AppLog.printLog(e.getMessage());
        }
    }

    public TrackerDao getTrackerDao() {
        return trackerDao;
    }

    private static final class DbHelperHolder {
        private static final DbHelper INSTANCE = new DbHelper();
    }


}
