package com.finjan.securebrowser.helpers;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableWebView;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class AnimationHelper {
    private static final class AnimationHelperHolder{
        private static final AnimationHelper INSTANCE=new AnimationHelper();
    }
    public static AnimationHelper getInstance(){
        return AnimationHelperHolder.INSTANCE;
    }
    public void setBackgroundAlpha(View view, float alpha, int baseColor) {
        int a = Math.min(255, Math.max(0, (int) (alpha * 255))) << 24;
        int rgb = 0x00ffffff & baseColor;
        view.setBackgroundColor(a + rgb);
    }
    public void setTitleTextAlpha(TextView textView, float alpha, int baseColor) {
//        textView.setAlpha(alpha);
//        AlphaAnimation animation1=null;
//        if(alpha==0){
//            animation1 = new AlphaAnimation(1.0f, 0.0f);
//        }
//        animation1.setDuration(1000);
//        animation1.setFillAfter(true);
//        textView.startAnimation(animation1);


    }
    public float getAlpha(float scrollY) {
        float alpha = scrollY / 100;
        if (alpha >= 1) {
            return 1;
        } else {
            return alpha;
        }
    }
    public AnimatorSet buildAnimationSet(Context context,int duration, ObjectAnimator... objectAnimators) {

        AnimatorSet a = new AnimatorSet();
        a.playTogether(objectAnimators);
        a.setInterpolator(AnimationUtils.loadInterpolator(context, android.R.interpolator.accelerate_decelerate));
        a.setDuration(duration);

        return a;
    }
    public ObjectAnimator buildAnimation(View view, float from, float to) {
        return ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, from, to);
    }
    public ObjectAnimator buildWidthAnimation(View view, float from, float to) {
        return ObjectAnimator
                .ofFloat(view, View.TRANSLATION_X, from, to);
    }
    public void moveWebview(Context context,float delta,boolean scrollUp,View view,View header,float compareSize){
        if(view!=null && (((ObservableWebView) view).getCurrentScrollY()==0 || ((ObservableWebView) view).getCurrentScrollY()<=compareSize))
            if(scrollUp){
                buildAnimationSet(context,0,buildAnimation(view,view.getY(),view.getY()+delta)).start();
                buildAnimationSet(context,0,buildAnimation(header,header.getY(),header.getY()-delta)).start();
            }else {
                buildAnimationSet(context,0,buildAnimation(view,view.getY(),view.getY()-delta)).start();
                buildAnimationSet(context,0,buildAnimation(header,header.getY(),header.getY()+delta)).start();
            }
    }
}
