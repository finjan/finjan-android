package com.finjan.securebrowser.helpers.vpn_helper;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.HomeActivity;
import com.finjan.securebrowser.helpers.AuthenticationHelper;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.ToastHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.security.WifiSecurityHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.JsonConfigParser;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
import com.finjan.securebrowser.vpn.controller.database.DatabaseContract;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.vpn.service.AviraOpenVpnService;
import com.finjan.securebrowser.vpn.ui.main.VpnApisHelper;
import com.finjan.securebrowser.vpn.util.AutoClose;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.TrafficController;
import com.finjan.securebrowser.vpn.util.VpnUtil;

import org.json.JSONObject;

import de.blinkt.openvpn.core.VpnStatus;

/**
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 07/06/17..
 *
 * class developed to initialized VPN independently but not in use right will be removed in some time
 */


public class InitializeVpn {

    public static final String VPN_SERVER_LIST = "VpnServerListJson";
    public static final long HOURS_24 = 1000 * 60 * 60 * 24;
    private static final int ANIMATION_DELAY_MILLIS = 400;
    private static final long RATE_ME_DIALOG_AGO = 300000; //5 min
    private static final String UNSECURE_NETWORK_DETECTED = "Unsecured Network detected,Connecting to VPN";
    private HomeActivity homeActivity;
    private AviraOpenVpnService mService;
    private ServiceConnection mConnection = new ServiceConnection() {


        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            AviraOpenVpnService.LocalBinder binder = (AviraOpenVpnService.LocalBinder) service;
            mService = binder.getService();

            if (mService != null) {
                if (NativeHelper.getInstnace().checkActivity(homeActivity)
                        && UserPref.getInstance().getVPNAutoConnect()
                        && !WifiSecurityHelper.getInstance().getNetworkType(homeActivity).isSecure()) {
                    connectToVpn(homeActivity);
                }
                initStatus(mService.getConnectionStatus());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }

    };
    private boolean servicesInitiated;
    private boolean isServiceRegistered = false;
    private String currentLocation, countryId;

    private InitializeVpn() {
    }

    public static InitializeVpn getInstance() {
        return InitializeVpnHolder.INSTANCE;
    }

//    public void connectVpn(Context context) {
//        if (UserPref.getInstance().getVPNAutoConnect() && mService != null && !VpnUtil.isVpnActive(context))
//            connectToVpn(context);
//    }

    public void disconnectVpn(Context context) {
        if (VpnUtil.isVpnActive(context) && mService != null) {
            VpnUtil.stopVpnConnection(context, mService);
        }
    }

    private void connectToVpn(Context context) {

        // get last used server id
        String serverRegion = FinjanVpnPrefs.getLastUsedServer(context);
        long serverId = new ContentProviderClient().getServerId(context, serverRegion);
        ToastHelper.showToast(context.getString(R.string.unsecured_network_detected_connecting_to_vpn), Toast.LENGTH_LONG);
        // connect to vpn
        VpnUtil.startVpnConnection( context,serverId,true);
    }

    public void setHomeActivity(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
    }

    public boolean isServicesInitiated() {
        return servicesInitiated;
    }

    public void initVpnService(Context context) {
        if (UserPref.getInstance().getIsBrowser()
            &&!WifiSecurityHelper.getInstance().getNetworkType(context).isSecure()
                && !UserPref.getInstance().getVPNAutoConnect()) {
            new FinjanDialogBuilder(context)
                    .setMessage(context.getString(R.string.your_data_at_risk_check_auto_connect_to_vpn_under_settings))
                    .setPositiveButton(context.getString(R.string.action_settings))
                    .setNegativeButton(context.getString(R.string.alert_cancel))
                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                        @Override
                        public void onPositivClick() {
                            ((HomeActivity) HomeActivity.getInstance()).openSettingScreen();
                        }
                    }).show();
        }

        if (!UserPref.getInstance().getVPNAutoConnect() || WifiSecurityHelper.getInstance().getNetworkType(context).isSecure()) {
            return;
        }
        if (servicesInitiated) {
            return;
        }
        servicesInitiated = true;
        if (NativeHelper.getInstnace().checkActivity(homeActivity)) {
            Intent intent = new Intent(homeActivity, AviraOpenVpnService.class);
            intent.setAction(AviraOpenVpnService.START_SERVICE);
            if (NativeHelper.getInstnace().checkActivity(homeActivity)) {
                isServiceRegistered = homeActivity.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            }
            fetchBackendData(context);
        }
    }

    public void removeVpnService() {
        disconnectVpn(homeActivity);
        if (isServiceRegistered) {
            homeActivity.unbindService(mConnection);
            isServiceRegistered = false;
        }

    }


    private void autoLoginIfRequested(@NonNull final Context context) {
        if (TrafficController.getInstance(context).isRegistered()) {
            if (!AuthenticationHelper.isTokenRefereshing()) {
                AuthenticationHelper.getInstnace().initRefreshToken(new AuthenticationHelper.AutoLoginResult() {
                    @Override
                    public void autoLoginResults(boolean success) {
                    }
                });
            }
        }
    }

    public boolean isVpnConnected() {
        return mService != null && mService.getConnectionStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTED;
    }

    private boolean isVpnConnecting() {
        return mService != null && (mService.getConnectionStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTING_SERVER_REPLIED
                || mService.getConnectionStatus() == VpnStatus.ConnectionStatus.LEVEL_CONNECTING_NO_SERVER_REPLY_YET);
    }

    public void stopVpnService() {
        if (NativeHelper.getInstnace().checkActivity(homeActivity)) {
            homeActivity.unbindService(mConnection);
        }
    }

    private void initStatus(VpnStatus.ConnectionStatus status) {
        switch (status) {
            case LEVEL_NOTCONNECTED:
            case LEVEL_NONETWORK:
            case LEVEL_AUTH_FAILED: {
//                VpnHelper.getInstance().setVpnConnected(false);
                break;
            }
            case LEVEL_CONNECTING_NO_SERVER_REPLY_YET:
            case LEVEL_CONNECTING_SERVER_REPLIED: {
                break;
            }
            case LEVEL_CONNECTED: {
//                VpnHelper.getInstance().setVpnConnected(true);
                break;
            }
            case LEVEL_CONNECTION_CANCELED:
//                VpnHelper.getInstance().setVpnConnected(false);
                break;
        }
    }

    private void fetchBackendData(Context context) {
        checkVpnServerList(context);
        VpnApisHelper.Companion.getInstance().updateLicence(false,true,context);
    }

    /**
     * update only once per day
     *
     * @param context
     */
    private void checkVpnServerList(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        long lastUpdateTime = prefs.getLong(VPN_SERVER_LIST, 0l);
        String defaultServer = prefs.getString(JsonConfigParser.DEFAULT_SERVER, null);
        updateVpnServerList();

        if (lastUpdateTime + HOURS_24 <= System.currentTimeMillis() || TextUtils.isEmpty(defaultServer)) {
            updateVpnServerList();
        }
    }

    protected void updateVpnServerList() {
        NetworkManager.getInstance(FinjanVPNApplication.getInstance()).
                fetchRegionsList(FinjanVPNApplication.getInstance(), new FetchServerList());
    }

    private void selectLastServer(@NonNull Context context) {
        String serverRegion = FinjanVpnPrefs.getLastUsedServer(context);

        if (!TextUtils.isEmpty(serverRegion)) {
            ContentProviderClient contentProviderClient = new ContentProviderClient();
            Cursor cursor = contentProviderClient.getServer(context, serverRegion);
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    long id = cursor.getLong(cursor.getColumnIndex(DatabaseContract.ServerTable._ID));
                    String title = cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.NAME));
                    setSelectedRegion(id, serverRegion, title);
                }
                AutoClose.closeSilently(cursor);
            }
        }
    }

    public String getCurrentLocation() {
        return TextUtils.isEmpty(currentLocation) ? "" : currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getCurrentLocationId() {
        return TextUtils.isEmpty(countryId) ? "" : countryId;
    }

    public void setCurrentLocationId(String currentLocationId) {
        this.countryId = currentLocationId;
    }

    public Drawable getFlag(Context context) {
        String currentLocaiton = getCurrentLocationId();
        if (!TextUtils.isEmpty(currentLocaiton)) {
            int flag = CountryFlags.getInstance().getCountyVPNFlag(currentLocaiton);
            if (flag != 0) {
                return ResourceHelper.getInstance().getDrawable(flag);
            }
        }
        return ResourceHelper.getInstance().getDrawable(R.drawable.tab_vpn_off);
    }


    public void setSelectedRegion(long id, String region, String title) {

        setCurrentLocation(title);
        setCurrentLocationId(region);
    }

    private static final class InitializeVpnHolder {
        private static final InitializeVpn INSTANCE = new InitializeVpn();
    }

    private class FetchServerList implements NetworkResultListener {

        @Override
        public void executeOnSuccess(JSONObject response) {
            JsonConfigParser.parseJsonConfig(FinjanVPNApplication.getInstance(), response);
            selectLastServer(FinjanVPNApplication.getInstance());
        }

        @Override
        public void executeOnError(VolleyError error) {
            error.printStackTrace();
        }
    }
}
