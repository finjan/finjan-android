package com.finjan.securebrowser.helpers;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.webkit.ValueCallback;
import android.webkit.WebView;

import com.finjan.securebrowser.Constants;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.fragment.ParentHomeFragment;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.ModelManager;
import com.finjan.securebrowser.model.TrackerCount;
import com.finjan.securebrowser.model.TrackerDataModel;
import com.finjan.securebrowser.model.TrackerFoundModel;
import com.finjan.securebrowser.model.TrackerListModel;
import com.finjan.securebrowser.model.TrackerModel;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import webHistoryDatabase.TrackerDao;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * <p>
 * <p>
 * Class for helping ParentWebview to solve url associated with webview
 * to find trackers
 *
 * Developed by NewOfferings, LLC on 22/02/18.
 */

public class TrackerHelper {

    // tracker list
    private ArrayList<TrackerFoundModel> trackerFoundList;

    private TrackerListener trackerListener;
    private ParentHomeFragment parentHomeFragment;
    private int index;
    private static final String TAG=TrackerHelper.class.getSimpleName();

    /**
     * Default constructor
     *
     * @param trackerListener for listening callbacks
     */
    public TrackerHelper(ParentHomeFragment parentHomeFragment, TrackerListener trackerListener) {
        trackerFoundList = new ArrayList<>();
        this.parentHomeFragment = parentHomeFragment;
        this.trackerListener = trackerListener;
    }

    public static List<TrackerFoundModel> getUpdatedTrackerList(ArrayList<TrackerFoundModel> trackerFoundList) {
        ArrayList<TrackerFoundModel> tempTrackerList = new ArrayList<>();
        tempTrackerList.clear();
        if (trackerFoundList.size() == 0)
            return tempTrackerList;
        HashMap<String, ArrayList<TrackerListModel>> trackerHashMap = new HashMap<>();
        for (int i = 0; i < trackerFoundList.size(); i++) {
            TrackerFoundModel trackerModel = trackerFoundList.get(i);
            ArrayList<TrackerListModel> nameList = trackerHashMap.get(trackerModel.getTrackerType());
            if (nameList == null) {
                nameList = new ArrayList<>();
            }
            nameList.add(new TrackerListModel(trackerModel.getTrackerName(), trackerModel.getTrackerScript()));
            trackerHashMap.put(trackerModel.getTrackerType(), nameList);
        }
        if (trackerHashMap.containsKey("Disconnect")) {
            trackerHashMap = manageDisconnectCatagory(trackerHashMap);
        }
        Set<String> trackerTypeList = trackerHashMap.keySet();
        for (String trackerType : trackerTypeList) {
            ArrayList<TrackerListModel> trackerNameList = trackerHashMap.get(trackerType);
            TrackerFoundModel model = new TrackerFoundModel();
            model.setTrackerType(trackerType);
            model.setTrackerCounts(getTrackerCount(trackerNameList));
            model.setTrackerURL(getTrackerUrlList(trackerNameList));
            tempTrackerList.add(model);
        }

        return checkTrackerDatabase(tempTrackerList);
    }

    private static List<TrackerFoundModel> checkTrackerDatabase(ArrayList<TrackerFoundModel> tempTrackerList) {
        TrackerDao trackerDao = DbHelper.getInstnace().getTrackerDao();
        if (trackerDao != null && trackerDao.loadAll() != null && trackerDao.loadAll().size() > 0) {
            List<webHistoryDatabase.Tracker> trackerDataList = trackerDao.loadAll();
            if (trackerDataList == null || trackerDataList.size() <= 0) {
                return tempTrackerList;
            }
            for (TrackerFoundModel data : tempTrackerList) {
                ArrayList<TrackerListModel> url = data.getTrackerURL();
                for (TrackerListModel urlData : url) {
                    for (webHistoryDatabase.Tracker dbData : trackerDataList) {
                        if (dbData.getUrl().equalsIgnoreCase(urlData.getTrackerURl())) {
                            if (TextUtils.isEmpty(data.getCheckedString())) {
                                data.setCheckedString(urlData.getTrackerName());
                            } else {
                                data.setCheckedString(data.getCheckedString() + Constants.URL_SEPRATOR + urlData.getTrackerName());
                            }
                            break;
                        }
                    }
                }
            }

        }
        return tempTrackerList;
    }

    private static HashMap<String, ArrayList<TrackerListModel>> manageDisconnectCatagory(HashMap<String, ArrayList<TrackerListModel>> trackerHashMap) {
        String disKey = "Disconnect";
        String contKey = "Content";
        ArrayList<TrackerListModel> disValue = trackerHashMap.get(disKey);
        trackerHashMap.remove(disKey);
        if (trackerHashMap.containsKey(contKey)) {
            ArrayList<TrackerListModel> contValue = trackerHashMap.get(contKey);
            for (int i = 0; i < disValue.size(); i++) {
//                if (!contValue.contains(disValue.get(i))) {
                contValue.add(disValue.get(i));
//                }
            }
        } else {
            trackerHashMap.put(contKey, disValue);
        }
        return trackerHashMap;
    }

    private static ArrayList<TrackerListModel> getTrackerUrlList(ArrayList<TrackerListModel> trackerNameList) {
        ArrayList<TrackerListModel> modelList = new ArrayList<>();
        for (TrackerListModel model : trackerNameList) {
            if (!isContainTracker(modelList, model)) {
                modelList.add(model);
            }
        }
        return modelList;
    }

    private static boolean isContainTracker(ArrayList<TrackerListModel> list, TrackerListModel model) {
        for (TrackerListModel dataModel : list) {
            if (model.getTrackerURl().equalsIgnoreCase(dataModel.getTrackerURl())) {
                return true;
            }
        }
        return false;
    }

    private static ArrayList<TrackerCount> getTrackerCount(ArrayList<TrackerListModel> trackerNameList) {
        ArrayList<TrackerCount> trackerList = new ArrayList<>();
        HashMap<String, Integer> trackerCountMap = new HashMap<>();
        ArrayList<String> nameList = new ArrayList<>();
        for (int i = 0; i < trackerNameList.size(); i++) {
            nameList.add(trackerNameList.get(i).getTrackerName());
        }
        for (int i = 0; i < trackerNameList.size(); i++) {
            String name = trackerNameList.get(i).getTrackerName();
            int count = Collections.frequency(nameList, name);
            trackerCountMap.put(name, count);
        }
        Set<String> keys = trackerCountMap.keySet();
        for (String name : keys) {
            Integer count = trackerCountMap.get(name);
            trackerList.add(new TrackerCount(name, count));
        }

        return trackerList;
    }

    /**
     * @return tracker list associated with particular instance of web page
     */
    public ArrayList<TrackerFoundModel> getTrackerList() {
        return trackerFoundList;
    }

    /**
     * @return count of trackers of current web page
     */
    public int getTrackerCount() {
        return (trackerFoundList == null || trackerFoundList.size() == 0) ? 0 : trackerFoundList.size();
    }

    /**
     * Must method to call for initialize tracker functionality
     *
     * @param url        to be scanned for trackers
     * @param urlWebView Webview that will give callback onReceiveValue() for above and equal Kitkat
     */
    public void initializeTrackerFinding(final String url, final WebView urlWebView) {
        trackerFoundList.clear();
        index = 0;
        trackerListener.updateTrackerCount(trackerFoundList == null ? 0 : trackerFoundList.size());
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                showTracker(url, urlWebView);
            }
        });
    }

    /**
     * @param url        to be scanned for trackers
     * @param urlWebView Webview that will give callback onReceiveValue() for above and equal Kitkat
     */
    private void showTracker(String url, final WebView urlWebView) {
        Logger.logE(TAG,"TrackerHelper initializes");
        final StringBuffer searchTag = new StringBuffer();
        searchTag.append("document.getElementsByTagName(");
        searchTag.append("'script')");
        searchTag.append("[(");
        searchTag.append(index);
        searchTag.append(")].outerHTML");
//
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            urlWebView.evaluateJavascript(searchTag.toString(), new ValueCallback<String>() {
                @Override
                public void onReceiveValue(final String value) {
                    Logger.logE(TAG,"TrackerHelper onReceiveValue "+value);
                    if (value != null && !value.equalsIgnoreCase("null")) {
                        Runnable r = new Runnable() {
                            @Override
                            public void run() {
                                findTracker(value.toLowerCase());
                            }
                        };
                        new Thread(r).start();
                        index = index + 1;
                        showTracker("", urlWebView);
                    }
                }
            });
        } else {
            new GetWebPageTracker().execute(url);
        }
    }

    /**
     * method to calculate trackers from script
     *
     * @param script Script associate wil web page
     */
    private void findTracker(String script) {
        TrackerModel trackerModel = ModelManager.getInstance().getTrackerModel();
        if (trackerModel != null) {
            if (trackerModel.getContent() != null && trackerModel.getContent().size() > 0) {
                findAndAddTracker(script, trackerModel.getContent(), trackerModel.getTrackerTag());
            }
            if (trackerModel.getAdvertising() != null && trackerModel.getAdvertising().size() > 0) {
                findAndAddTracker(script, trackerModel.getAdvertising(), trackerModel.getAdvertisingTag());
            }
            if (trackerModel.getAnalytics() != null && trackerModel.getAnalytics().size() > 0) {
                findAndAddTracker(script, trackerModel.getAnalytics(), trackerModel.getAnalyticsTag());
            }
            if (trackerModel.getDisconnect() != null && trackerModel.getDisconnect().size() > 0) {
                findAndAddTracker(script, trackerModel.getDisconnect(), trackerModel.getDisconnectTag());
            }
            if (trackerModel.getSocial() != null && trackerModel.getSocial().size() > 0) {
                findAndAddTracker(script, trackerModel.getSocial(), trackerModel.getSocialTag());
            }
        }

    }

    /**
     * @param scriptingString Script associate with web page
     * @param trackerContent  arraylist that should be result of all tracker found
     */
    private void findAndAddTracker(String scriptingString, ArrayList<TrackerDataModel> trackerContent, String category) {
        for (int i = 0; i < trackerContent.size(); i++) {
            if (i < trackerContent.size()) {
                try {


                    String urlTrackerValue = isContain(scriptingString, trackerContent.get(i), category);
                    if (!TextUtils.isEmpty(urlTrackerValue)) {

                        trackerFoundList.add(new TrackerFoundModel(trackerContent.get(i).getTrackerName(),
                                trackerContent.get(i).getTrackerType(), urlTrackerValue));
                        Logger.logE(TAG,trackerFoundList.size()+"");
                        if (NativeHelper.getInstnace().checkActivity(parentHomeFragment.getActivity())) {
                            parentHomeFragment.getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (NativeHelper.getInstnace().checkActivity(parentHomeFragment.getActivity())) {
                                        int count = trackerFoundList == null ? 0 : trackerFoundList.size();
                                        trackerListener.trackerFound(count, trackerFoundList);
                                        trackerListener.updateTrackerCount(count);
                                    }
                                }
                            });
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public String isContain(String script, TrackerDataModel value, String category) {
        if (value == null || script == null) {
            return "";
        }
        //block for bypassing tracker that came under whitelist from plist.
        if (!TextUtils.isEmpty(category) && PlistHelper.getInstance().getCategoryWhiteList() != null
                && PlistHelper.getInstance().getCategoryWhiteList().containsKey(category)) {
            ArrayList<String> subCatList = PlistHelper.getInstance().getCategoryWhiteList().get(category);
            if (subCatList.contains(value.getTrackerName()) || subCatList.contains("*")) {
                return "";
            }
        }
        return isContain(script, value.getTrackerDataList());
    }

    private String isContain(String script, ArrayList<String> dataModel) {

        for (String data : dataModel) {
            if (script.contains(data)) {
                return data.toLowerCase();
            }
        }
        return "";
    }

    private StringBuffer getWebPagedetail(String url) throws MalformedURLException {
        StringBuffer response = null;
        URL obj = new URL(url);
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
//            con.setDoOutput(true);
            con.setConnectTimeout(60 * 1000);
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            String result = response.toString();

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
        return response;
    }

    private void scanWebPage(final StringBuffer response) {
        if (response == null) {
            trackerListener.updateTrackerCount(trackerFoundList == null ? 0 : trackerFoundList.size());
            return;
        }

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                trackerFoundList.clear();
                trackerListener.updateTrackerCount(0);
                ArrayList<Integer> totalCountList = getAvailityCount(response);
                if (totalCountList.size() > 0) {
                    getAllTracker(response, totalCountList);
                }
            }
        });
    }

    private ArrayList<Integer> getAvailityCount(StringBuffer response) {
        ArrayList<Integer> indexList = new ArrayList<Integer>();
        String word = response.toString();
//        int index = word.indexOf(SCRIPT_TAG);
        int index = 0;
        while (index >= 0) {
            index = word.indexOf(AppConstants.SCRIPT_TAG, index + 1);
            if (index > 0) {
                indexList.add(index);
            }
        }
        return indexList;
    }

    private void getAllTracker(StringBuffer response, ArrayList<Integer> totalCountList) {
        getAllScriptTag(response, totalCountList);
    }

    private void getAllScriptTag(StringBuffer response, ArrayList<Integer> totalCountList) {
        String webPagedetail = response.toString();
        for (int i = 0; i < totalCountList.size(); i++) {
            String newpagedetail = webPagedetail.substring(totalCountList.get(i));
            int closerIndex = newpagedetail.indexOf(AppConstants.SCRIPT_END_TAG);
            String scriptingString = newpagedetail.substring(0, closerIndex);
            findTracker(scriptingString.toLowerCase());
        }
    }

    public void makeCloneOf(ArrayList<TrackerFoundModel> trackerFoundList) {
        this.trackerFoundList = new ArrayList<>();
        for (TrackerFoundModel trackerFoundModel : trackerFoundList) {
            this.trackerFoundList.add(trackerFoundModel.clone());
        }
    }

    public void clearTrackerList() {
        if (trackerFoundList != null && trackerFoundList.size() > 0) {
            trackerFoundList.clear();
        }
    }

    public interface TrackerListener {
        void updateTrackerCount(int count);

        void trackerFound(int count, ArrayList<TrackerFoundModel> trackerFoundList);

        void hideLoader();

        void showLoader();
    }

    private class GetWebPageTracker extends AsyncTask<String, Void, StringBuffer> {
        @Override
        protected StringBuffer doInBackground(String... params) {
            try {
                return getWebPagedetail(params[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
//            if (UserPref.getInstance().getSafeScanEnabled()) {
//                trackerListener.showLoader();
//            }
        }


        @Override
        protected void onPostExecute(StringBuffer value) {
            super.onPostExecute(value);
            trackerListener.hideLoader();
            try {
                scanWebPage(value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        protected void onCancelled() {
            trackerListener.hideLoader();
        }
    }

    public static void getDefaultTrackerAlert(Context context, final TrackerTurnOnListener trackerTurnOnListener){
        if(NativeHelper.getInstnace().checkContext(context)){
            FinjanDialogBuilder builder=new FinjanDialogBuilder(context);
            builder.setMessage(PlistHelper.getInstance().getTrackerDisableMessage())
                    .setPositiveButton("Turn On")
                    .setNegativeButton("Cancel")
                    .setDialogClickListener(new FinjanDialogBuilder.DialogClickListener() {
                        @Override
                        public void onNegativeClick() {

                        }

                        @Override
                        public void onPositivClick() {
                            UserPref.getInstance().setTracker(true);
                            UserPref.getInstance().setTrackerHintVisibility(true);
                            UserPref.getInstance().setFirstTrackerHintVisibility(false);
                            if(trackerTurnOnListener!=null){
                                trackerTurnOnListener.onTurnOn();
                            }
                        }

                        @Override
                        public void onContentClick(String content) {

                        }
                    });
            builder.show();
        }
    }
    public interface TrackerTurnOnListener{
        void onTurnOn();
    }
}
