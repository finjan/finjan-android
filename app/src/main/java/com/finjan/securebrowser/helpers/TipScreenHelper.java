package com.finjan.securebrowser.helpers;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.fragment.FragmentHome;
import com.finjan.securebrowser.fragment.ParentHomeFragment;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 * Class used in designing tips tutorial screen
 */

public class TipScreenHelper {
    private static final class TipScreenHelperHolder{
        private static final TipScreenHelper instance=new TipScreenHelper();
    }
    public static TipScreenHelper getInstance(){
        return TipScreenHelperHolder.instance;
    }

    private int currentOrientation=-1;
    public void setTipScreen(final ParentHomeFragment fragment, View parentView){

        if(parentView!=null){
            final LinearLayout tipContainer=(LinearLayout) parentView.findViewById(R.id.ll_tip_container);
            if(!UserPref.getInstance().getIsTipShown()){
                currentOrientation=fragment.getResources().getConfiguration().orientation;
                (fragment.getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                isTipShowing=true;
                tipContainer.setVisibility(View.VISIBLE);
                final View view= LayoutInflater.from(fragment.getContext()).inflate(R.layout.view_brower_tips_2,null);
                view.setVisibility(View.VISIBLE);
                bindValue(view);
                tipContainer.addView(view);
                view.findViewById(R.id.view_click_listener).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tipContainer.setVisibility(View.GONE);
                        UserPref.getInstance().setIsTipShown(true);
                        (fragment.getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                        if(fragment!=null){fragment.setBottomBars((FragmentHome) fragment,true);}
                        view.setVisibility(View.GONE);
                    }
                });
            }else {
                isTipShowing=false;
                tipContainer.setVisibility(View.GONE);
            }
        }
    }

    private void bindValue(View view){
        ((TextView)view.findViewById(R.id.tv_tip_bookmark)).setText(PlistHelper.getInstance().getTipBookmark());
        ((TextView)view.findViewById(R.id.tv_tip_drawer)).setText(PlistHelper.getInstance().getTipDrawer());
        ((TextView)view.findViewById(R.id.tv_tip_home)).setText(PlistHelper.getInstance().getTipHome());
        ((TextView)view.findViewById(R.id.tv_tip_tracker)).setText(PlistHelper.getInstance().getTipTracker());
        ((TextView)view.findViewById(R.id.tv_tip_vpn)).setText(PlistHelper.getInstance().getTipVPN());
        ((TextView)view.findViewById(R.id.tv_tip_vs)).setText(PlistHelper.getInstance().getTipVS());
        ((TextView)view.findViewById(R.id.tv_tip_tab)).setText(PlistHelper.getInstance().getTipTab());
    }
    public interface SafeScanTurnOnLisnter{
        void onSafeScanTurnOn();
    }
    public boolean isTipShowing;
    public static void getDefaultSafeScanAlert(Context context, final SafeScanTurnOnLisnter safeScanTurnOnLisnter){
        if(NativeHelper.getInstnace().checkContext(context)){
            FinjanDialogBuilder builder=new FinjanDialogBuilder(context);
            builder.setMessage(PlistHelper.getInstance().getSafeScanDisableMsg())
                    .setPositiveButton("Turn On")
                    .setNegativeButton("Cancel")
                    .setDialogClickListener(new FinjanDialogBuilder.DialogClickListener() {
                        @Override
                        public void onNegativeClick() {

                        }

                        @Override
                        public void onPositivClick() {
                            UserPref.getInstance().setSafeScanEnabled(true);
                            if(safeScanTurnOnLisnter!=null){
                                safeScanTurnOnLisnter.onSafeScanTurnOn();
                            }
                        }

                        @Override
                        public void onContentClick(String content) {

                        }
                    });
            builder.show();
        }
    }
}
