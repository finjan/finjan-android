package com.finjan.securebrowser.helpers.sharedpref;

import android.content.Context;
import android.text.TextUtils;

import com.finjan.securebrowser.BuildConfig;
import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.social.SocialUserProfile;
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper;


/**
 * 29/11/16.
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 13/10/17
 *
 * class for setting and getting the shared preferences with key
 */
public class UserPref extends Pref {


    private static final String Email = "Email";
    private static final String Password = "Password";
    private static final String Password_reset = "Password_reset";
    private static final String ShouldLoginAutomatically = "ShouldLoginAutomatically";
    private static final String isLoginShown = "isLoginShown";
    private static final String Default_Email = "Default_Email";
    private static final String Default_Password = "Default_Password";
    private static final boolean Default_password_reset = false;
    private static final boolean Default_shoult_login_automatically = false;
    private static final boolean Default_isLoginShown = false;
    private static final double DEF_My_Car_Latitude = 0;
    private static final double DEF_My_Car_Longitude = 0;
    private static final String booleanValue = "booleanValue";
    private static final boolean Def_BooleanValue = false;
    private static final String KEY_FB_MESSAGE_TOKEN = "KEY_FB_MEDDAGE_TOKEN";
    private static final String KEY_TOKEN = "KEY_TOKEN";
    private static final String DEF_TOKEN = "";
    private static final String KEY_USERNAME = "KEY_USERNAME";
    private static final String DEF_USERNAME = "";
    private static final String KEY_REMEMBER_ME = "KEY_REMEMBER_ME";
    private static final String SafeScan = "safescan";
    private static final boolean DEF_SAFE_SCAN = false;
    private static final String Tracker = "tracker";
    private static final boolean DEF_Tracker = false;
    private static final String PrivateBrowsing = "privatebrowsing";
    private static final boolean DEF_PrivateBrowsing = false;
    private static final String TrackerHindVisibility = "TrackerHindVisibility";
    private static final boolean DEF_TrackerHindVisibility = false;
    private static final String FirstTrackerHindVisibility = "FirstTrackerHindVisibility";
    private static final boolean DEF_FirstTrackerHindVisibility = false;
    private static final String Audable = "audible";
    private static final boolean DEF_Audable = false;
    private static final String Passcode = "passcode";
    private static final boolean DEF_Passcode = false;
    private static final String TouchId = "touchid";
    private static final boolean DEF_TouchId = false;
    private static final String LastUrl = "lastUrl";
    private static final String DEF_LastUrl = AppConstants.EMPTY_URL;
    private static final String DeletedUrl = "deletedUrl";
    private static final String DEF_DeletedUrl = "";
    private static final String RmSecs = "rm_secs";
    private static final int DEF_RmSecs = 10;
    private static final String SCAN_TYPE = "sacn_type";
    private static final int DEF_SCAN_TYPE = 0;
    private static final String HOME_PAGE = "home_page";
    private static final String DEF_HOME_PAGE = "";
    private static final String APP_LAUNCH_URL = "opeAppURL";
    private static final String DEF_APP_LAUNCH_URL = "";
    private static final String SAFE_SCAN_REMAIN = "enabled";
    private static final int DEF_SAFE_SCAN_REMAIN = 0;
    private static final String NO_THANKS_REMAIN = "nothanksRemain";
    private static final String DEF_NO_THANKS_REMAIN = "";
    private static final String AUTO_SUBSCRIBE = "autosubscribe";
    private static final boolean DEF_AUTO_SUBSCRIBE = true;
    private static final String SafeSearchPurchase = "SafeSearchPurchase";
    private static final boolean DEF_SafeSearchPurchase = false;
    private static final String IsPrivateTab = "isPrivateTab";
    private static final boolean DEF_isPrivateTab = false;
    private static final String KEY_LAND_HEIGHT = "KEY_LAND_HEIGHT";
    private static final int Default_LAND_HEIGHT = 0;
    private static final String KEY_PORTRAIT_HEIGHT = "KEY_PORTRAIT_HEIGHT";
    private static final int Default_PORTRAIT_HEIGHT = 0;
    private static final String KEY_SCAN_TYPE_CHANGED="KEY_SCANT_TYPE_CHANGED";
    private static final boolean Default_SCAN_TYPE_CHANGED=false;
    //locking preferences
    private static final String ConfirmLockCode = "confirmlockcode";
    private static final String DEF_ConfarmLockCode = "";
    private static final String SetLockCode = "setlockcode";
    private static final String DEF_SetLockCode = "";
    private static final String LockCode = "lockcode";
    private static final String DEF_LockCode = "";
    private static final String CheckLockCode = "checklockcode";
    private static final String DEF_CheckLockCode = "";
    private static final boolean DEF_REMEMBER_ME = false;
    private static final String LastId = "KEY_LastId";
    private static final Long Def_LastId = Long.valueOf(1);
    private static String def_token = "";
    private static final String STATUS = "status";


    //Authentication
    private static final String Current_Access_Token="Current_Access_Token";
    private static final String Def_Current_Access_Token="";
    private static final String Current_Refresh_Token="Current_Refresh_Token";
    private static final String Def_Current_Refresh_Token="";


    private static String KEY_DEVICE_ID="ANDROID_DEVICE_ID";
    private static String DEF_DEVICE_ID="";
    public UserPref(Context context) {
        super(context);
    }

    public static UserPref getInstance() {
        return UserPrefHolder.INSTANCE;
    }

    public String getEmail() {
        return getString(UserPref.Email, Default_Email);
    }

    public void setEmail(String email) {
        setString(UserPref.Email, email);
    }

    public String getPassword() {
        return getString(UserPref.Password, Default_Password);
    }

    public void setPassword(String password) {
        setString(UserPref.Password, password);
    }

    public void setShouldLoginAutomatically(boolean shouldLoginAutomatically) {
        setBoolean(UserPref.ShouldLoginAutomatically, shouldLoginAutomatically);
    }

    public boolean getSetShouldLoginAutomatically() {
        return getBoolean(UserPref.ShouldLoginAutomatically, Default_shoult_login_automatically);
    }

    public void setPassword_reset(boolean password_reset) {
        setBoolean(UserPref.Password_reset, password_reset);
    }

    public boolean getPasswordReset() {
        return getBoolean(UserPref.Password_reset, Default_password_reset);
    }

    public boolean getIsLoginShown() {
        return getBoolean(UserPref.isLoginShown, Default_isLoginShown);
    }

    public void setIsLoginShown(boolean isLoginShown) {
        setBoolean(UserPref.isLoginShown, isLoginShown);
    }
    public void setChecheAvilable(String CacheKey,long time) {
        setLong(CacheKey, time);
    }
    public long getChecheAvilable(String CacheKey) {
        return getLong(CacheKey,0);
    }
    public void setHitTime(String CacheKey) {
        setLong(CacheKey+"_hit", System.currentTimeMillis());
    }
    public long getHitTime(String CacheKey) {
        return getLong(CacheKey+"_hit",0);
    }
    public void setInappMassege(int range) {
        setInt("InAppMassege", range);
    }
    public int getInappMassege() {
        return getInt("InAppMassege",-1);
    }
    public void setPushMassege(boolean range) {
        setBoolean("PushMassege", range);
    }
    public boolean getPushMassege() {
        return getBoolean("PushMassege",false);
    }

    public void setcurrentRange(int range) {
        setInt("notification_range", range);
    }
    public int getcurrentRange() {
        return getInt("notification_range",100);
    }
    private static final String NOTIFICATION_JSON  = "notification_json";
    private static final String NOTIFICATION_JSON_Array  = "notification_json_array";
    public void setNotificatioJson(String json) {
        setString(NOTIFICATION_JSON, json);
    }
    public String getNotificatioJson() {
        return getString(NOTIFICATION_JSON, "");
    }
    public void setNotificatioPersentageJson(String json) {
        setString(NOTIFICATION_JSON_Array, json);
    }
    public String getNotificatioPersentageJson() {
        return getString(NOTIFICATION_JSON_Array, "");
    }

    public void setSubscrube(String key, boolean value) {
        setBoolean(key, value);
    }

    public boolean getSubcription(String key) {
        return getBoolean(key, true);
    }

    public void setPlayerSubscrube(String key, boolean value) {
        setBoolean(key, value);
    }

    public boolean getPlayerSubcription(String key) {
        return getBoolean(key, false);
    }

    public void setSubsCriptionInput(String key, boolean value) {
        setBoolean(key + "input", value);
    }

    public boolean getSubscriptionInput(String key) {
        return getBoolean(key + "input", false);
    }

    public String getKeyFbMessageToken() {
        return getString(KEY_FB_MESSAGE_TOKEN, def_token);
    }

    public void setKeyFbMessageToken(String token) {
        setString(KEY_FB_MESSAGE_TOKEN, token);
    }

    public String getUsername() {
        return getString(KEY_USERNAME, DEF_USERNAME);
    }

    public void setUsername(String username) {
        setString(KEY_USERNAME, username);
    }

    public String getToken() {
        return getString(KEY_TOKEN, DEF_TOKEN);
    }

    public void setToken(String token) {
        setString(KEY_TOKEN, token);
    }

    public boolean getRememberMe() {
        return getBoolean(KEY_REMEMBER_ME, DEF_REMEMBER_ME);
    }

//    public void setBooleanValue(String key, boolean value){
//        setBoolean(key,value);
//    }
//    public boolean getBooleanValue(String key){
//        return getBoolean(key,Def_BooleanValue);
//    }

    public void setRememberMe(boolean shouldRememberMe) {
        setBoolean(KEY_REMEMBER_ME, shouldRememberMe);
    }

    public boolean getSafeScanEnabled() {
        return getBoolean(SafeScan, DEF_SAFE_SCAN);
    }

    public void setSafeScanEnabled(boolean enabled) {
        setBoolean(SafeScan, enabled);
    }

    public boolean getTracker() {

        return getBoolean(Tracker, DEF_Tracker);
    }

    public void setTracker(boolean enabled) {
        setBoolean(Tracker, enabled);
    }

    public boolean getTrackerHintVisibility() {
        return false;
//        return getBoolean(TrackerHindVisibility, DEF_TrackerHindVisibility);
    }

    public void setTrackerHintVisibility(boolean trackerHintVisibility) {
        setBoolean(TrackerHindVisibility, trackerHintVisibility);
    }

    public boolean getFirstTrackerHintVisibility() {
        return true;
//        return getBoolean(FirstTrackerHindVisibility, DEF_FirstTrackerHindVisibility);
    }

    public void setFirstTrackerHintVisibility(boolean trackerHintVisibility) {
        setBoolean(FirstTrackerHindVisibility, trackerHintVisibility);
    }

    public boolean getPrivateBrowsing() {
        return getBoolean(PrivateBrowsing, DEF_PrivateBrowsing);
    }

    public void setPrivateBrowsing(boolean enabled) {
        setBoolean(PrivateBrowsing, enabled);
    }

    public boolean getAudable() {
        return getBoolean(Audable, DEF_Audable);
    }

    public void setAudable(boolean isAudable) {
        setBoolean(Audable, isAudable);
    }

    public String getConfirmLockCode() {
        return getString(ConfirmLockCode, DEF_ConfarmLockCode);
    }

    public void setConfirmLockCode(String confirmLockCode) {
        setString(ConfirmLockCode, confirmLockCode);
    }

    public String getSetLockCode() {
        return getString(SetLockCode, DEF_SetLockCode);
    }

    public void setSetLockCode(String setLockCode) {
        setString(SetLockCode, setLockCode);
    }

    public String getLockCode() {
        return getString(LockCode, DEF_LockCode);
    }

    public void setLockCode(String lockCode) {
        setString(LockCode, lockCode);
    }

    public boolean getPasscode() {
        return getBoolean(Passcode, DEF_Passcode);
    }

    public void setPasscode(boolean enabled) {
        setBoolean(Passcode, enabled);
    }

    public String getCheckLockCode() {
        return getString(CheckLockCode, DEF_CheckLockCode);
    }

    public void setCheckLockCode(String checkLockCode) {
        setString(CheckLockCode, checkLockCode);
    }

    public boolean getTouchId() {
        return getBoolean(TouchId, DEF_TouchId);
    }

    public void setTouchId(boolean enabled) {
        setBoolean(TouchId, enabled);
    }

    public String getLastUrl() {
        return getString(LastUrl, DEF_LastUrl);
    }

    public void setLastUrl(String lastUrl) {
        if (!TextUtils.isEmpty(lastUrl)) {
            setString(LastUrl, lastUrl);
        }
    }

    public String getDeletedUrl() {
        return getString(DeletedUrl, DEF_DeletedUrl);
    }

    public void setDeletedUrl(String deletedUrl) {
        setString(DeletedUrl, deletedUrl);
    }

    public int getRmSecs() {

//        String countSTr = getString(RmSecs, "");

//        if (!TextUtils.isEmpty(countSTr)) {
//            countSTr = countSTr.trim();
//            try {
//                int count = Integer.parseInt(countSTr);
//                setInt(RmSecs, count);
////                setString(RmSecs, "");
//                return count;
//
//            } catch (NumberFormatException e) {
////                setString(RmSecs, "");
//                setInt(RmSecs, 0);
//                return 0;
//            }
//        }
        return getInt(RmSecs, DEF_RmSecs);
    }

    public void setRmSecs(int rmSecs) {
//        String countSTr = getString(RmSecs, "");
//        if (!TextUtils.isEmpty(countSTr)) {
//            countSTr=countSTr.trim();
//            try {
//                int count = Integer.parseInt(countSTr);

//                setInt(RmSecs, 0);
//                setString(RmSecs, "");
//            } catch (NumberFormatException e) {
//                setString(RmSecs, "");
//            }
//        }
        setInt(RmSecs, rmSecs);
    }

    public int getScanType() {
        return getInt(SCAN_TYPE, DEF_SCAN_TYPE);
    }

    public void setScanType(int scanType) {
        setInt(SCAN_TYPE, scanType);
    }


    public String getHomePage() {
        return getString(HOME_PAGE, DEF_HOME_PAGE);
    }

    public void setHomePage(String homePage) {
        setString(HOME_PAGE, homePage);
    }

    public String getAppLaunchUrl() {
        return getString(APP_LAUNCH_URL, DEF_APP_LAUNCH_URL);
    }

    public void setAppLaunchUrl(String appLaunchUrl) {
        setString(APP_LAUNCH_URL, appLaunchUrl);
    }

    public int getSafeScanRemain() {
        return getInt(SAFE_SCAN_REMAIN, DEF_SAFE_SCAN_REMAIN);
    }

    public void setSafeScanRemain(int safeScanRemain) {
        setInt(SAFE_SCAN_REMAIN, safeScanRemain);
    }

    public String getNoThanksRemain() {
        return getString(NO_THANKS_REMAIN, DEF_NO_THANKS_REMAIN);
    }

    public void setNoThanksRemain(String noThanksRemain) {
        setString(NO_THANKS_REMAIN, noThanksRemain);
    }

    public boolean getAutoSubscribe() {
        return getBoolean(AUTO_SUBSCRIBE, DEF_AUTO_SUBSCRIBE);
    }

    public void setAutoSubscribe(boolean autoSubscribe) {
        setBoolean(AUTO_SUBSCRIBE, autoSubscribe);
    }

    public boolean getSafeSearchPurchase() {
        if(AppConstants.paidScanVersion){
            return getBoolean(SafeSearchPurchase, DEF_SafeSearchPurchase);
        }
        return true;
    }

    public void setSafeSearchPurchase(boolean searchPurchase) {
        setBoolean(SafeSearchPurchase, searchPurchase);
    }

    public boolean getIsPrivateTab() {
        return getBoolean(IsPrivateTab, DEF_isPrivateTab);
    }

    public void setIsPrivateTab(boolean isPrivateTab) {
        setBoolean(IsPrivateTab, isPrivateTab);
    }

    public Long getLastId() {
        return getLong(LastId, Def_LastId);
    }

    public void setLastId(Long id) {
        if (id != null) {
            setLong(LastId, id);
        } else {
            setLong(LastId, Long.valueOf(1));
        }
    }

    public void setPortraitHeight(int portraitHeight) {
        setInt(KEY_PORTRAIT_HEIGHT, portraitHeight);
    }

    public int getLandHeight() {
        return getInt(KEY_LAND_HEIGHT, Default_LAND_HEIGHT);
    }

    public void setLandHeight(int landHeight) {
        setInt(KEY_LAND_HEIGHT, landHeight);
    }

    public int getPortrainHeight() {
        return getInt(KEY_PORTRAIT_HEIGHT, Default_PORTRAIT_HEIGHT);
    }

    private static class UserPrefHolder {
        private static final UserPref INSTANCE = new UserPref(AnalyticsApplication.getMyApplication().getBaseContext());
    }

    public void setDeviceId(String deviceId){
        if(deviceId==null){
            deviceId="";
        }
        setString(KEY_DEVICE_ID,deviceId);
    }
    public String getDeviceID(){
        String deviceId=getString(KEY_DEVICE_ID,DEF_DEVICE_ID);
        if(TextUtils.isEmpty(deviceId)){
            return "";
        }
        return deviceId;
    }
    public void setScanTypeChanged(boolean isChanged){
        setBoolean(KEY_SCAN_TYPE_CHANGED,isChanged);
    }
    public boolean getScanTypeChanged(){return getBoolean(KEY_SCAN_TYPE_CHANGED,Default_SCAN_TYPE_CHANGED);}

    public void setCurrent_Access_Token(String current_Access_Token){setString(Current_Access_Token,current_Access_Token);}
    public String getCurrent_Access_Token(){return  getString(Current_Access_Token,Def_Current_Access_Token);}
    public void setCurrent_Refresh_Token(String current_Refresh_Token){setString(Current_Refresh_Token,current_Refresh_Token);}
    public String getCurrent_Refresh_Token(){return  getString(Current_Refresh_Token,Def_Current_Refresh_Token);}

    private static final String MY_LAST_PAGE="my_last_page";
    private static final int DEFULT_MY_LAST_PAGE=0;
    public void setMyLastPage(int page){setInt(MY_LAST_PAGE,page);}
    public int getMYLastpage(){return getInt(MY_LAST_PAGE,DEFULT_MY_LAST_PAGE);}

    private static final String isTutorialScreenShowedNew="ISTUTORIALSCREENSHOWEDN";
    private static final String isTutorialScreenShowedOld="ISTUTORIALSCREENSHOWED";
    private static final boolean Default_isTutorialScreenShowed=false;
    public void setIsTutorialScreenShowed(boolean showed){setBoolean(isTutorialScreenShowedNew,showed);}
    public boolean getIsTutorialScreenShowed(){return getBoolean(isTutorialScreenShowedNew,Default_isTutorialScreenShowed);}
    public boolean isOldApp(){return getBoolean(isTutorialScreenShowedOld,false);}
    public void setOldApp(){ setBoolean(isTutorialScreenShowedOld,false);
        setBoolean("isAppUpgraded",true);}
    public boolean isAppUpgraded(){return getBoolean("isAppUpgraded",false);}
    public void consumedAppUpgraded(){setBoolean("isAppUpgraded",false);}

    private static final String myAppWasInBackground="myAppWasInBackground";
    private static final boolean Def_myAppWasInBackground=true;
    public void setMyAppWasInBackground(boolean myAppWasInBackground){this.setBoolean(UserPref.myAppWasInBackground,myAppWasInBackground);}
    public boolean getIsMyAppWasInBackground(){return getBoolean(myAppWasInBackground,Def_myAppWasInBackground);}

    private static final String tempPurchase="tempPurchase";
    private static final String Default_tempPurchase="";
    public void setTempPurchaseToken(String tempPurchase){setString(UserPref.tempPurchase,tempPurchase);}
    public String getTempPurchaseToken(){return getString(UserPref.tempPurchase,Default_tempPurchase);}
    private static  final String  appInstalled="appInstalled";
    private static final boolean Default_appInstalled=false;
    public void setAppInstalled(boolean appInstalled){setBoolean(UserPref.appInstalled,appInstalled);}
    public boolean getAppInstalled(){return getBoolean(UserPref.appInstalled,Default_appInstalled);}

    private static final boolean Default_Tracker_Status=false;
    private static final boolean Default_Tracker_Status_Changed=false;
    private static final String Tracker_Status="Tracker_Status";
    private static final String Tracker_Status_Changed="Tracker_Status_Changed";
    public void setTrackerStatusChanged(boolean status){setBoolean(Tracker_Status_Changed,status);}
    public boolean getTrackerStatusChanged(){return getBoolean(Tracker_Status_Changed,Default_Tracker_Status_Changed);}

    public void setTrackerStatus(boolean status){setBoolean(Tracker_Status,status);}
    public boolean getTrackerStatus(){return getBoolean(Tracker_Status,Default_Tracker_Status);}

    private static final String tempUserLogin="tempUserLogin";
    private static final boolean Default_tempUserLogin=false;
    public void setTempUserLogin(boolean tempUserLogin){setBoolean(UserPref.tempUserLogin,tempUserLogin);}
    public boolean getTempUserLogin(){return getBoolean(UserPref.tempUserLogin,Default_tempUserLogin);}

    private static final String vpnScreenHelpMsgCountSaved="vpnScreenHelpMsgCountSaved";
    private static final boolean Def_vpnScreenHelpMsgCountSaved=false;
    public void setVpnScreenHelpMsgCountSaved(boolean saved){setBoolean(vpnScreenHelpMsgCountSaved,saved);}
    public boolean getVpnScreenHelpMsgCountSaved(){return getBoolean(vpnScreenHelpMsgCountSaved,Def_vpnScreenHelpMsgCountSaved);}
    private static final int Default_VPNHelpMsgCount=5;
    private static final String VPNHelpMsgCount="VPNHelpMsgCount";
    public void setVpnHelpMsgCount(int count){setInt(VPNHelpMsgCount,count);}
    public int getVpnHelpMsgCount(){return getInt(VPNHelpMsgCount,Default_VPNHelpMsgCount);}

    private static final String isTipShown="isTipShown";
    private static final boolean Def_IsTipShown=false;
    public void setIsTipShown(boolean isTipShown){setBoolean(UserPref.isTipShown,isTipShown);}
    public boolean getIsTipShown(){return getBoolean(UserPref.isTipShown,Def_IsTipShown);}

    private static final String isBrowserSetting="isBrowserSetting";
    private static final boolean Def_isBroswer=false;
    public void setIsBroswer(boolean isBroswer){setBoolean(isBrowserSetting,isBroswer);}
    public boolean getIsBrowser(){return getBoolean(isBrowserSetting,Def_isBroswer);}

    private static final String isVpnAutoConnect="isVpnAutoConnect";
    private static final boolean Def_isVpnAutoConnect=false;
    public void setVPNAutoConnect(boolean autoConnect){setBoolean(isVpnAutoConnect,autoConnect);}
    public boolean getVPNAutoConnect(){return getBoolean(isVpnAutoConnect,Def_isVpnAutoConnect);}
    private static final String KEY_isSetupDone="KEY_isSetupDone";
    private static final boolean Def_isSetupDone=false;
    public void setIsSetUpDone(boolean setUpDone){setBoolean(KEY_isSetupDone,setUpDone);}
    public boolean getIsSetUpDone(){return getBoolean(KEY_isSetupDone,Def_isSetupDone);}
    public static boolean autoConnectVersion=false;

    private static final String KEY_PRIVACY_READING_DATE="KEY_PRIVACY_READING_DATE";
    private static final String DEF_PRIVACY_READING_DATE="";
    public void  setPrivacyReadingDate(String date){setString(KEY_PRIVACY_READING_DATE,date);}
    public String getPrivacyReadingDate(){return getString(KEY_PRIVACY_READING_DATE,DEF_PRIVACY_READING_DATE);}


    private static final String KEY_EMAIL_LOGIN="KEY_EMAIL_LOGIN";
    private static final boolean DEF_EMAIL_LOGIN=false;
    public void setEmailLogin(boolean emailLogin){setBoolean(KEY_EMAIL_LOGIN,emailLogin);}
    public boolean getEmailLogin(){return getBoolean(KEY_EMAIL_LOGIN,DEF_EMAIL_LOGIN);}

    private static final String KEY_USER_LANG="KEY_USER_LANG";
    private static final String def_USER_LANG="en";
    public void setUserLang(String userLang){
        setString(KEY_USER_LANG,userLang);
    }
    public String getUserLang(){return getString(KEY_USER_LANG,def_USER_LANG);}


    private static final String KEY_TRUSTED_NETWORK= "KEY_TRUSTED_NETWORK";
    public void addTrustedNetwork(String uuid){
        if(TextUtils.isEmpty(uuid)){return;}
        String trustedNetworks = getString(KEY_TRUSTED_NETWORK, "");
        if(TextUtils.isEmpty(trustedNetworks)){
            setString(KEY_TRUSTED_NETWORK,uuid);
        }else {
            setString(KEY_TRUSTED_NETWORK, trustedNetworks + ","+ uuid);
        }
    }
    public void removeTrustedNetwork(String ssid){
        String trustedNetworks = getString(KEY_TRUSTED_NETWORK, "");
        if(!TextUtils.isEmpty(trustedNetworks)){
            if(trustedNetworks.contains(",")){
                trustedNetworks = trustedNetworks.replace(ssid,"");
            }else {
                trustedNetworks = "";
            }
            setString(KEY_TRUSTED_NETWORK,trustedNetworks);
        }
    }

    public String[] getTrustedNetworks(){
        String trustedNetworks = getString(KEY_TRUSTED_NETWORK, "");
        String[] array = new String[]{};
        if(!TextUtils.isEmpty(trustedNetworks)){
            array = trustedNetworks.split(",");
        }
        return array;
    }
    public static final String KEY_AUTO_CONNECT = "KEY_AUTO_CONNECT";
    public void setAutoConnect(boolean autoConnect){
        setBoolean(KEY_AUTO_CONNECT, autoConnect);
    }
    public void setIsDefaultAutoConnectSet(){setBoolean("setIsDefaultAutoConnectSet",true);}
    public boolean getIsDefaultAutoConnectSet(){return getBoolean("setIsDefaultAutoConnectSet",false);}
    public boolean getAutoConnect(){
        return getBoolean(KEY_AUTO_CONNECT,true);
    }

    public static final String KEY_login_by= "KEY_login_by";
    public void setLoginBy(String loginBy){setString(KEY_login_by, loginBy);}
    public String getLoginBy(){return getString(KEY_login_by, "email");}
    public int isNewInstall(){
        if(getInt("app_version",0) == 0){
            return 0;
        }
        if(getInt("app_version",0) > 0){
            return 1;
        }
        setInt("app_version",BuildConfig.VERSION_CODE);
        return 2;
    }
    public void setUserVisitsVPN(boolean visits){setBoolean("setUserVisitsVPN",visits);}
    public void setUserVisitsBrowser(boolean visits){setBoolean("setUserVisitsBrowser",visits);}
    public Boolean getUserVisitsVPN(){return getBoolean("setUserVisitsVPN",false);}
    public Boolean getUserVisitsBrowser(){return getBoolean("setUserVisitsBrowser",false);}
    public Boolean getFirstOpen(){return getBoolean("firstOpen",false);}
    public void setFirstOpen(boolean visits){setBoolean("firstOpen",visits);}
    public Boolean getUserSendLocalitics(){return getBoolean("UserSendLocalitics",false);}
    public void setUserSendLocalitics(boolean visits){setBoolean("UserSendLocalitics",visits);}

    public void setPlistVersion(String plistversion){
        setString("plistVersion",plistversion);
    }
    public String getPlistVersion(){
        return getString("plistVersion","");
    }
    public void setPlistUpdateTime(long timeStamp){
        setLong("setPlistUpdateTime",timeStamp);
    }
    public long getPlistUpdateTime(){
        return getLong("setPlistUpdateTime",0);
    }
    public void setIsLocalyticsReadAutoConnectStatus(boolean isAutoConnect){setBoolean("IsLocalyticsReadAutoConnectStatus",false);}
    public boolean getIsLocalyticsReadAutoConnectStatus(){return getBoolean("IsLocalyticsReadAutoConnectStatus",false);}

    public void setLastTrafficUpdateDate(){
        setString("setLastTrafficUpdateDate", DateHelper.getInstnace().getCurrentDateMonth());
    }
    public String getLastUpdateTrafficDate(){
        return getString("setLastTrafficUpdateDate", DateHelper.getInstnace().getCurrentDateMonth());
    }
    public void cacheUserForAutoLogin(String type, String email, String password){
        setString("cacheUserForAutoLogin_1",type);
        setString("cacheUserForAutoLogin_2",email);
        setString("cacheUserForAutoLogin_3",password);
    }
    public String[] getCachedUserForAutoLogin(){
        String v1= getString("cacheUserForAutoLogin_1","");
        String v2= getString("cacheUserForAutoLogin_2","");
        String v3= getString("cacheUserForAutoLogin_3","");
        if(TextUtils.isEmpty(v2)){
            return new String[]{};
        }else return new String[]{v1,v2,v3};
    }
    public String getLoginVia(){
        String via = "";
        String v1= getString("cacheUserForAutoLogin_1","");
        if(!TextUtils.isEmpty(v1)){
            switch (v1){
                case SocialUserProfile.FB_USER :
                    return via = "via Facebook";
                case SocialUserProfile.GG_USER :
                    return  via = "via Google";
                case SocialUserProfile.Email_USER:
                    return  via = "via Email";
            }
        }
        return via;
    }
    public void setTokenExpiresIn(int expiresIn){
        setLong("TokenExpiresIn",expiresIn);
        setLong("TokenExpiresInCurrentTime",System.currentTimeMillis());

    }
    public boolean isTokenExpires(){
        long expiresIn = getLong("TokenExpiresIn",0);
//        int expiresInUpdateTime = 0;
        long expiresInUpdateTime = getLong("TokenExpiresInCurrentTime",0);
        return !DateHelper.getInstnace().isFuture(expiresInUpdateTime+expiresIn*1000);
    }

    public void isUserHadGivenVPNPermission(boolean permission){
        setBoolean("isUserHadGivenVPNPermission",permission);
    }
    public boolean getIsUserHadGivenVPNPermission(){
        return getBoolean("isUserHadGivenVPNPermission",false);
    }

    public void setConfigListExpiry(long value){setLong("setConfigListExpiry",value);}
    public long getConfigListExpiry(){return  getLong("setConfigListExpiry",24);}

    //    public void setNewPromoConsumed(boolean consumed){ setBoolean("newPromoConsumed",consumed);}
    public void setPromoConsumed(String consumed){
        consumed = consumed.trim();
        String storedIds = getString("setPromoConsumed","");
        storedIds = storedIds+ consumed+"::";
        setString("setPromoConsumed",storedIds);
    }
    public void setPromoOneDayEventConsumed(String consumed){
        consumed = consumed.trim();
        String storedIds = getString("setPromoOneDayEventConsumed","");
        storedIds = storedIds+ consumed+"::";
        setString("setPromoOneDayEventConsumed",storedIds);
    }

    public boolean isPromoOneDayEventConsumed(String id){

        String storedIds = getString("setPromoOneDayEventConsumed","");
        if(TextUtils.isEmpty(storedIds)){
            return false;
        }else {
            String[] array = storedIds.split("::");
            for (String eachId: array){
                if(eachId.equalsIgnoreCase(id)){
                    return true;
                }
            }
            return false;
        }
    }

    public String getPromoConsumed(){
        return getString("setPromoConsumed","");
    }
    public boolean isPromoConsumed(String id){

        String storedIds = getString("setPromoConsumed","");
        if(TextUtils.isEmpty(storedIds)){
            return false;
        }else {
            String[] array = storedIds.split("::");
            for (String eachId: array){
                if(eachId.equalsIgnoreCase(id)){
                    return true;
                }
            }
            return false;
        }
    }

    public void setLastSplashAnim(String splashFile){
        setString("setLastSplashAnim",splashFile);
    }
    public String getLastSplashAnim(){
        return getString("setLastSplashAnim","");
    }
    public void setStatus(String status)
    {
        setString(STATUS,status);
    }
    public String getStatus()
    {
        return getString(STATUS,"");
    }

    public void clearPrefOnPrimium(){
        clearPrefKey("notification_range");
        clearPrefKey("InAppMassege");
        clearPrefKey("PushMassege");
    }
    public void clearUserCorrespondingPrefOnLogout(){
        PromoHelper.Companion.setCurrentActivePromo(null);
        clearPrefOnPrimium();
        clearPrefKey("cacheUserForAutoLogin_1");
        clearPrefKey("cacheUserForAutoLogin_2");
        clearPrefKey("cacheUserForAutoLogin_3");
        clearPrefKey("setLastTrafficUpdateDate");
        clearPrefKey(KEY_login_by);
        clearPrefKey("setIsDefaultAutoConnectSet");
//        clearPrefKey(KEY_AUTO_CONNECT);
        setAutoConnect(true);
        clearPrefKey(KEY_EMAIL_LOGIN);
        clearPrefKey(KEY_PRIVACY_READING_DATE);
        clearPrefKey(isVpnAutoConnect);
        clearPrefKey(tempUserLogin);
        clearPrefKey(tempPurchase);
        clearPrefKey(Email);
        clearPrefKey(Password);
        clearPrefKey("setPromoOneDayEventConsumed");
        clearPrefKey(STATUS);
        clearPrefKey("UserSendLocalitics");
        clearPrefKey(AppConstants.ALLCANCLEDPROMOSUBSCRIPTIONLIST);
        clearPrefKey(AppConstants.USER);
        clearPrefKey(AppConstants.ALLPROMOLIST);
        clearPrefKey(AppConstants.LICENCE);
        clearPrefKey(AppConstants.ALLSUBSCRIPTION);
        clearPrefKey(AppConstants.TRAFFIC);

        clearPrefKey(AppConstants.ALLCANCLEDPROMOSUBSCRIPTIONLIST+"_hit");
        clearPrefKey(AppConstants.USER+"_hit");
        clearPrefKey(AppConstants.ALLPROMOLIST+"_hit");
        clearPrefKey(AppConstants.LICENCE+"_hit");
        clearPrefKey(AppConstants.ALLSUBSCRIPTION+"_hit");
        clearPrefKey(AppConstants.TRAFFIC+"_hit");
        Utils.deleteCache(FinjanVPNApplication.getInstance());
    }

}

