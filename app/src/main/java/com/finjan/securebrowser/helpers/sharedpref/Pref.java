package com.finjan.securebrowser.helpers.sharedpref;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.finjan.securebrowser.helpers.logger.Logger;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 13/10/17
 */


class Pref {

    private Context mContext;
    private SharedPreferences sp;
    private String prefName	=	"cel";
    private static final String TAG=Pref.class.getSimpleName();



    public Pref(Context context) {

        sp= context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
//        sp = PreferenceManager.getDefaultSharedPreferences(context);
        this.mContext = context;
    }
    public SharedPreferences getSp(){return sp;}

    protected void setString(String key, String value) {
        sp.edit().putString(key, value).apply();
    }

    protected String getString(String key, String def) {
        return sp.getString(key, def);
    }

    protected void setBoolean(String key, boolean value) {
        sp.edit().putBoolean(key, value).apply();
    }

    protected boolean getBoolean(String key, boolean def) {
        try{
            return sp.getBoolean(key, def);
        }catch (Exception e){
            Logger.logE(TAG,"Exception in getBoolean()");
            return false;
        }
    }

    protected void setFloat(String key, float value) {
        sp.edit().putFloat(key, value).apply();
    }

    protected float getFloat(String key, float value) {
        try{
            return sp.getFloat(key, value);
        }catch (Exception e){
            Logger.logE(TAG,"Exception in getFload()");
            return 0;
        }
    }

    protected void setInt(String key, int value) {
        sp.edit().putInt(key, value).apply();
    }

    protected int getInt(String key, int value) {
        try{
            return sp.getInt(key, value);
        }catch (Exception e){
            Logger.logE(TAG,"Exception in getInt()");
            return 0;
        }
    }

    protected void setDouble(String key, double value) {
        sp.edit().putString(key, Double.toString(value)).apply();
    }

    protected double getDouble(String key, double value) {
        try{
            return Double.parseDouble(sp.getString(key, Double.toString(value)));
        }catch (Exception e){
            Logger.logE(TAG,"Exception in getDouble()");
            return 0;
        }
    }

    protected void setLong(String key, long value) {
        sp.edit().putLong(key, value).apply();
    }

    protected long getLong(String key, long def) {
        try{
            return sp.getLong(key, def);
        }catch (Exception e){
            Logger.logE(TAG,"Exception in getLong()");
            return 0;
        }
    }
    public void clearPrefKey(String key){
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        editor.apply();
    }

    public void clearPref() {
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
    }

}