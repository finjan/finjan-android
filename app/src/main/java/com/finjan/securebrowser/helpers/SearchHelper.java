package com.finjan.securebrowser.helpers;

import java.util.ArrayList;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 * Class used in searching for browser
 */

public class SearchHelper {
    private ArrayList<String> keyArrayList;
    private ArrayList<String> valueArrayList;

    private SearchHelper() {
    }

    public static SearchHelper getInstance() {
        return SearchHelperHolder.Instance;
    }

    public ArrayList<String> getKeyArrayList() {
        return keyArrayList;
    }

    public void setKeyArrayList(ArrayList<String> keyArrayList) {
        this.keyArrayList = keyArrayList;
    }

    public ArrayList<String> getValueArrayList() {
        return valueArrayList;
    }

    public void setValueArrayList(ArrayList<String> valueArrayList) {
        this.valueArrayList = valueArrayList;
    }

    private static final class SearchHelperHolder {
        private static final SearchHelper Instance = new SearchHelper();
    }
}
