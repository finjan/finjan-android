package com.finjan.securebrowser.helpers.vpn_helper;

import android.text.TextUtils;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;

/**
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 07/06/17.
 *
 * class for setting and getting the shared preferences with key
 */


public class CountryFlags {
    private static final class CountryFlagsHolder{
        private static final CountryFlags instance=new CountryFlags();
    }
    private CountryFlags(){}
    public static CountryFlags getInstance(){
        return CountryFlagsHolder.instance;
    }


    public int getCountryFlag(String countryId){
        if(countryId.equals("uk")){
            countryId="gb";
        }
        int country=0;
        try{
            country= ResourceHelper.getInstance().getImageResources(countryId,R.drawable.nearest);
        }catch (Exception e){
            country= R.drawable.nearest;
        }
        return country==0?R.drawable.nearest:country;
    }

    public int getCountyVPNFlag(String countryCode){

        countryCode=sortCountryCode(countryCode);
        switch (countryCode){

            //temp as on avira apis
            case "nearest":
                return R.drawable.tab_vpn_on;
            case "nearest location":
                return R.drawable.tab_vpn_on;
            case "uk":
                return R.drawable.united_kingdom;











            case "af":
                //afghanistan
                break;
            case "ax":
                //Aland Islands
                break;
            case "al":
                //Albania
                break;
            case "dz":
                //Algeria
                break;
            case "as":
                //American Samoa
                break;
            case "ad":
                //Andorra
                break;
            case "ao":
                //Angola
                break;
            case "ai":
                //Anguilla
                break;
            case "aq":
                //Antarctica
                break;
            case "ag":
                //Antigua and Barbuda
                break;
            case "ar":
                //Argentina
                break;
            case "am":
                //Armenia
                break;
            case "aw":
                //Aruba
                break;
            case "au":
                //Australia
                return R.drawable.australia;
            case "at":
                //Austria
                return R.drawable.austria;
            case "az":
                //Azerbaijan
                break;
            case "bs":
                //Bahamas
                break;
            case "bh":
                //Bahrain
                break;
            case "bd":
                //Bangladesh
                break;
            case "bb":
                //Barbados
                break;
            case "by":
                //Belarus
                break;
            case "be":
                return R.drawable.belgium;
            //Belgium
//                break;
            case "bz":
                //Belize
                break;
            case "bj":
                //Benin
                break;
            case "bm":
                //Bermuda
                break;
            case "bt":
                //Bhutan
                break;
            case "bo":
                //Bolivia, Plurinational State of
                break;
            case "bq":
                //Bonaire, Sint Eustatius and Saba
                break;
            case "ba":
                //Bosnia and Herzegovina
                break;
            case "bw":
                //Botswana
                break;
            case "bv":
                //Bouvet Island
                break;
            case "br":
                //Brazil
                return R.drawable.brazil;
            case "io":
                //British Indian Ocean Territory
                break;
            case "bn":
                //Brunei Darussalam
                break;
            case "bg":
                return R.drawable.bulgaria;

            //Bulgaria
//                break;
            case "bf":
                //Burkina Faso
                break;
            case "bi":
                //Burundi
                break;
            case "kh":
                //Cambodia
                break;
            case "cm":
                //Cameroon
                break;
            case "ca":
                //Canada
                return R.drawable.canaada;
            case "cv":
                //Cape Verde
                break;
            case "ky":
                //Cayman Islands
                break;
            case "cf":
                //Central African Republic
                break;
            case "td":
                //Chad
                break;
            case "cl":
                //Chile
                break;
            case "cn":
                //China
                break;
            case "cx":
                //Christmas Island
                break;
            case "cc":
                //Cocos (Keeling) Islands
                break;
            case "co":
                //Colombia
                break;
            case "km":
                //Comoros
                break;
            case "cg":
                //Congo
                break;
            case "cd":
                //Congo, the Democratic Republic of the
                break;
            case "ck":
                //Cook Islands
                break;
            case "cr":
                //Costa Rica
                break;
            case "ci":
                //Côte d'Ivoire
                break;
            case "hr":
                //Croatia
                break;
            case "cu":
                //Cuba
                break;
            case "cw":
                //Curaçao
                break;
            case "cy":
                //Cyprus
                break;
            case "cz":
                //Czech Republic
                return R.drawable.czech;
            case "dk":
                return R.drawable.denmark;

            //Denmark
//                break;
            case "dj":
                //Djibouti
                break;
            case "dm":
                //Dominica
                break;
            case "do":
                //Dominican Republic
                break;
            case "ec":
                //Ecuador
                break;
            case "eg":
                //Egypt
                break;
            case "sv":
                //El Salvador
                break;
            case "gq":
                //Equatorial Guinea
                break;
            case "er":
                //Eritrea
                break;
            case "ee":
                //Estonia
                break;
            case "et":
                //Ethiopia
                break;
            case "fk":
                //Falkland Islands (Malvinas)
                break;
            case "fo":
                //Faroe Islands
                break;
            case "fj":
                //Fiji
                break;
            case "fi":
                //Finland
                break;
            case "fr":
                //France
                return R.drawable.france;

            case "gf":
                //French Guiana
                break;
            case "pf":
                //French Polynesia
                break;
            case "tf":
                //French Southern Territories
                break;
            case "ga":
                //Gabon
                break;
            case "gm":
                //Gambia
                break;
            case "ge":
                //Georgia
                break;
            case "de":
                //Germany
                return R.drawable.germany;
            case "gh":
                //Ghana
                break;
            case "gi":
                //Gibraltar
                break;
            case "gr":
                //Greece
                break;
            case "gl":
                //Greenland
                break;
            case "gd":
                //Grenada
                break;
            case "gp":
                //Guadeloupe
                break;
            case "gu":
                //Guam
                break;
            case "gt":
                //Guatemala
                break;
            case "gg":
                //Guernsey
                break;
            case "gn":
                //Guinea
                break;
            case "gw":
                //Guinea-Bissau
                break;
            case "gy":
                //Guyana
                break;
            case "ht":
                //Haiti
                break;
            case "hm":
                //Heard Island and McDonald Islands
                break;
            case "va":
                //Holy See (Vatican City State)
                break;
            case "hn":
                //Honduras
                break;
            case "hk":
                //Hong Kong
                return R.drawable.hongkong;
            case "hu":
                //Hungary
                return R.drawable.hungary;

//            break;
            case "is":
                //Iceland
                break;
            case "in":
                //India
                break;
            case "id":
                //Indonesia
                break;
            case "ir":
                //Iran, Islamic Republic of
                break;
            case "iq":
                //Iraq
                break;
            case "ie":
                //Ireland
                break;
            case "im":
                //Isle of Man
                break;
            case "il":
                //Israel
                break;
            case "it":
                //Italy
                return R.drawable.italy;
            case "jm":
                //Jamaica
                break;
            case "jp":
                //Japan
                return R.drawable.japan;
            case "je":
                //Jersey
                break;
            case "jo":
                //Jordan
                break;
            case "kz":
                //Kazakhstan
                break;
            case "ke":
                //Kenya
                break;
            case "ki":
                //Kiribati
                break;
            case "kp":
                //Korea, Democratic People's Republic of
                break;
            case "kr":
                //Korea, Republic of
                break;
            case "kg":
                //Kyrgyzstan
                break;
            case "la":
                //Lao People's Democratic Republic
                break;
            case "lv":
                //Latvia
                break;
            case "lb":
                //Lebanon
                break;
            case "ls":
                //Lesotho
                break;
            case "lr":
                //Liberia
                break;
            case "ly":
                //Libya
                break;
            case "li":
                //Liechtenstein
                break;
            case "lt":
                //Lithuania
                break;
            case "lu":
                //Luxembourg
                break;
            case "mo":
                //Macao
                break;
            case "mk":
                //Macedonia, the Former Yugoslav Republic of
                break;
            case "mg":
                //Macedonia, the Former Yugoslav Republic of
                break;
            case "mw":
                //Malawi
                break;
            case "my":
                //Malaysia
                break;
            case "mv":
                //Maldives
                break;
            case "ml":
                //Mali
                break;
            case "mt":
                //Malta
                break;
            case "mh":
                //Marshall Islands
                break;
            case "mq":
                //Martinique
                break;
            case "mr":
                //FiMauritaniaji
                break;
            case "mu":
                //Mauritius
                break;
            case "yt":
                //Mayotte
                break;
            case "mx":
                //Mexico
                return R.drawable.mexico;
            case "fm":
                //Micronesia, Federated States of
                break;
            case "md":
                //Moldova, Republic of
                break;
            case "mc":
                //Monaco
                break;
            case "mn":
                //Monaco
                break;
            case "me":
                //Montenegro
                break;
            case "ms":
                //Montserrat
                break;
            case "ma":
                //Morocco
                break;
            case "mz":
                //Mozambique
                break;
            case "mm":
                //Myanmar
                break;
            case "na":
                //Namibia
                break;
            case "nr":
                //Nauru
                break;
            case "np":
                //Nepal
                break;
            case "nl":
                //Netherlands
                return R.drawable.netherland;
            case "nc":
                //New Caledonia
                break;
            case "nz":
                //New Zealand
                break;
            case "ni":
                //Nicaragua
                break;
            case "ne":
                //Niger
                break;
            case "ng":
                //Nigeria
                break;
            case "nu":
                //Niue
                break;
            case "nf":
                //Norfolk Island
                break;
            case "mp":
                //Northern Mariana Islands
                break;
            case "no":
                //Norway
//                break;
                return R.drawable.norway;
            case "om":
                //Oman
                break;
            case "pk":
                //Pakistan
                break;
            case "pw":
                //Palau
                break;
            case "ps":
                //Palestine, State of
                break;
            case "pa":
                //Panama
                break;
            case "pg":
                //Papua New Guinea
                break;
            case "py":
                //Paraguay
                break;
            case "pe":
                //Peru
                break;
            case "ph":
                //Philippines
                break;
            case "pn":
                //Pitcairn
                break;
            case "pl":
                //Poland
                return R.drawable.poland;
            case "pt":
                //Portugal
                break;
            case "pr":
                //Puerto Rico
                break;
            case "qa":
                //Qatar
                break;
            case "re":
                //Réunion
                break;
            case "ro":
                //Romania
                return R.drawable.romania;
            case "ru":
                //Russian Federation
                break;
            case "bl":
                //Saint Barthélemy
                break;
            case "sh":
                //Saint Helena, Ascension and Tristan da Cunha
                break;
            case "kn":
                //Saint Kitts and Nevis
                break;
            case "lc":
                //Saint Lucia
                break;
            case "mf":
                //Saint Martin (French part)
                break;
            case "pm":
                //Saint Pierre and Miquelon
                break;
            case "vc":
                //Saint Vincent and the Grenadines
                break;
            case "ws":
                //Samoa
                break;
            case "sm":
                //San Marino
                break;
            case "st":
                //Sao Tome and Principe
                break;
            case "sa":
                //Saudi Arabia
                break;
            case "sn":
                //Senegal
                break;
            case "rs":
                //Serbia
                break;
            case "sc":
                //Seychelles
                break;
            case "sl":
                //Sierra Leone
                break;
            case "sg":
                //Singapore
                return R.drawable.singapore;
            case "sx":
                //Sint Maarten (Dutch part)
                break;
            case "sk":
                //Slovakia
                break;
            case "si":
                //Slovenia
                break;
            case "sb":
                //Solomon Islands
                break;
            case "so":
                //Somalia
                break;
            case "za":
                //South Africa
                break;
            case "gs":
                //South Georgia and the South Sandwich Islands
                break;
            case "ss":
                //South Sudan
                break;
            case "es":
                //Spain
                return R.drawable.spain;
            case "lk":
                //Sri Lanka
                break;
            case "sd":
                //Sudan
                break;
            case "sr":
                //Suriname
                break;
            case "sj":
                //Svalbard and Jan Mayen
                break;
            case "sz":
                //Swaziland
                break;
            case "se":
                //Sweden
                return R.drawable.sweden;
            case "ch":
                //Switzerland
                return R.drawable.switzerland;
            case "sy":
                //Syrian Arab Republic
                break;
            case "tw":
                //Taiwan, Province of China
                break;
            case "tj":
                //Tajikistan
                break;
            case "tz":
                //Tanzania, United Republic of
                break;
            case "th":
                //Thailand
                break;
            case "tl":
                //Timor-Leste
                break;
            case "tg":
                //Togo
                break;
            case "tk":
                //Tokelau
                break;
            case "to":
                //Tonga
                break;
            case "tt":
                //Trinidad and Tobago
                break;
            case "tn":
                //Tunisia
                break;
            case "tr":
                //Turkey
                break;
            case "tm":
                //Turkmenistan
                break;
            case "tc":
                //Turks and Caicos Islands
                break;
            case "tv":
                //Tuvalu
                break;
            case "ug":
                //Uganda
                break;
            case "ua":
                //Ukraine
                break;
            case "ae":
                //United Arab Emirates
                break;
            case "gb":
                //United Kingdom
                return R.drawable.united_kingdom;
            case "us":
                //United States
                return R.drawable.united_states;
            case "um":
                //United States Minor Outlying Islands
                break;
            case "uy":
                //Uruguay
                break;
            case "uz":
                //Uzbekistan
                break;
            case "vu":
                //Vanuatu
                break;
            case "ve":
                //Venezuela, Bolivarian Republic of
                break;
            case "vn":
                //Viet Nam
                break;
            case "vg":
                //Virgin Islands, British
                break;
            case "vi":
                //Virgin Islands, U.S.
                break;
            case "wf":
                //Wallis and Futuna
                break;
            case "eh":
                //Western Sahara
                break;
            case "ye":
                //Yemen
                break;
            case "zm":
                //Zambia
                break;
            case "zw":
                //Zimbabwe
                break;






        }
        return 0;
    }
    private String sortCountryCode(String code){
        if(!TextUtils.isEmpty(code)){
            code=code.toLowerCase();
            if(code.contains("us_")){
                return "us";
            }
        }
        return code;
    }
}
