package com.finjan.securebrowser.helpers.security;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.finjan.securebrowser.helpers.logger.Logger;

import java.util.List;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 13/10/17
 *
 * helper class to check if connected wifi network is password protected or not.
 *
 */

public class WifiSecurityHelper {

    private static final String TAG = WifiSecurityHelper.class.getSimpleName();
    private static final int TYPE_WPA2 = 1;
    private static final int TYPE_WPA = 2;
    private static final int TYPE_WEP = 3;
    private static final int TYPE_NONE = 0;
    private boolean isAutoConnectVersion=false;

    public static WifiSecurityHelper getInstance() {
        return WifiSecurityHelperHolder.instance;
    }

    private NetworkType networkType;

    public NetworkType getNetworkType(Context context) {
//        if(networkType==null){
            setNetworkType(context);
//        }
        return networkType;
    }
    public boolean isWifiNetwork(Context context){
        ConnectivityManager net= (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo cellular=net.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        return (net!=null && (net.getNetworkInfo(ConnectivityManager.TYPE_WIFI)).isConnectedOrConnecting());
    }
    private void setNetworkType(Context context){
        if(isWifiNetwork(context)){
            networkType=generateNetworkType(context);
        }else networkType=new NetworkType(NetworkType.NT_NONE,false,true,"");
    }

    public String getSsid(@NonNull Context context){
        WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        //get current connected SSID for comparison to ScanResult
        return wifi!=null?wifi.getConnectionInfo().getSSID().replaceAll("\\\"",""):"";
    }
    private NetworkType generateNetworkType(Context context) {
//        if(!UserPref.autoConnectVersion){
//            return new NetworkType(NetworkType.NT_WPA2,true,false);
//        }
        WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        List<ScanResult> networkList = wifi.getScanResults();
        NetworkType networkType=new NetworkType();
        networkType.secure=true;
        //get current connected SSID for comparison to ScanResult
        WifiInfo wi = wifi.getConnectionInfo();
        String currentSSID = wi.getSSID().replaceAll("\\\"","");
        networkType.ssid = currentSSID;
        if (networkList != null) {
            for (ScanResult network : networkList) {
                //check if current connected SSID
                if (currentSSID.equals(network.SSID)) {
                    //get capabilities of current connection
                    String Capabilities = network.capabilities;
                    Logger.logD(TAG, network.SSID + " capabilities : " + Capabilities);

                    if (Capabilities.contains("WPA2")) {
                        return new NetworkType(NetworkType.NT_WPA2,true,false,currentSSID);
                    } else if (Capabilities.contains("WPA")) {
                        return new NetworkType(NetworkType.NT_WPA,false,false,currentSSID);
                    } else if (Capabilities.contains("WEP")) {
                        return new NetworkType(NetworkType.NT_WEP,false,false,currentSSID);
                    }else {
                        return new NetworkType(NetworkType.NT_NONE,false,true,currentSSID);
                    }
                }
            }
        }
        return networkType;
    }

    private static final class WifiSecurityHelperHolder {
        private static final WifiSecurityHelper instance = new WifiSecurityHelper();
    }
    public class NetworkType{
        public static final String NT_WEP="WEP";
        public static final String NT_WPA="WPA";
        public static final String NT_WPA2="WPA2";
        public static final String NT_NONE="NONE";
        private String type,ssid;
        private boolean open,secure;

        public NetworkType(String type,boolean secure,boolean open,String ssid){
            this.type=type;this.secure=secure;this.open=open;this.ssid = ssid;
        }
        public NetworkType(){}
        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public boolean isOpen() {
            return open;
        }

        public void setOpen(boolean open) {
            this.open = open;
        }

        public boolean isSecure() {
            return secure;
        }

        public void setSecure(boolean secure) {
            this.secure = secure;
        }

        public String getSsid() {
            return ssid;
        }
    }

}
