package com.finjan.securebrowser.helpers.context_helper;

import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;

import com.finjan.securebrowser.vpn.FinjanVPNApplication;

import org.jetbrains.annotations.NotNull;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * resource helper class to provide direct access to resources like strings, dimen, color etc
 */

public class ResourceHelper {
    private static final class DrawableHelperHolder{
        private static final ResourceHelper instance=new ResourceHelper();
    }
    private ResourceHelper(){}
    public static ResourceHelper getInstance(){
        return DrawableHelperHolder.instance;
    }
    public Drawable getDrawable(int resource){
        return ContextCompat.getDrawable(FinjanVPNApplication.getInstance().getApplicationContext(), resource);
    }
    public int getColor(int resource){
        return ContextCompat.getColor(FinjanVPNApplication.getInstance().getApplicationContext(), resource);
    }
    public Drawable getDrawable(boolean setFirstRes,int firstRes,int secoundRes){
        return setFirstRes?getDrawable(firstRes):getDrawable(secoundRes);
    }
    public int getColor(boolean setFirstRes,int firstRes,int secondRes){
        return setFirstRes?getColor(firstRes):getColor(secondRes);
    }
    public String getString(int resouce){
        return FinjanVPNApplication.getInstance().getString(resouce);
    }
    public int getImageResources(@NotNull String res,int defaultRes){
        res=res.trim().toLowerCase().replace(" ","_");
        res=res.trim().toLowerCase().replace("-","_");
        int country=0;
        try{
            country= FinjanVPNApplication.getInstance().getResources().getIdentifier(res, "drawable",
                    FinjanVPNApplication.getInstance().getPackageName());
        }catch (Exception e){
            country= defaultRes;
        }
        return country;
    }
}
