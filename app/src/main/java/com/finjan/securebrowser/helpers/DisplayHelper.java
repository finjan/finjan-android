package com.finjan.securebrowser.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Window;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.activity.HomeActivity;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 * Display helper to check size of display and then use dimension in browser's safe scan result process
 */

public class DisplayHelper {
    private static final class DisplayHelperHolder{
        private static final DisplayHelper Instance=new DisplayHelper();
    }
    public static DisplayHelper getInstance(){return DisplayHelperHolder.Instance;}

    public void setDisplay(Activity activity){
        if(NativeHelper.getInstnace().checkActivity(activity)){
            Rect rect = new Rect();


            Window win = activity.getWindow();  // Get the Window
            win.getDecorView().getWindowVisibleDisplayFrame(rect);
            // Get the height of Status Bar
            int statusBarHeight = rect.top;


            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int screenHeight = metrics.heightPixels;


            int heightP = metrics.heightPixels;
            int widhtP = metrics.widthPixels;
            if (!(UserPref.getInstance().getPortrainHeight() > 0 && UserPref.getInstance().getLandHeight() > 0)) {
                if (heightP > widhtP) {
                    UserPref.getInstance().setPortraitHeight(heightP - statusBarHeight);
                    UserPref.getInstance().setLandHeight(widhtP - statusBarHeight);
                } else {
                    UserPref.getInstance().setLandHeight(heightP - statusBarHeight);
                    UserPref.getInstance().setPortraitHeight(widhtP - statusBarHeight);
                }
            }
        }
    }
    public int getDrawerBottomBar(Context context){
        if(GlobalVariables.getInstnace().isDrawerBottomShowing){
            Resources r = context.getResources();
            return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, r.getDimension(R.dimen._50sdp), r.getDisplayMetrics()));
        }
        return 0;

    }

    public int getDisplayHeight(boolean isLandscapeOrientation,Context context){
        int displayHeight=0;
        final int drawerBottomBar=getDrawerBottomBar(context);

        if (isLandscapeOrientation) {
            displayHeight = UserPref.getInstance().getLandHeight();
        } else {
            displayHeight = UserPref.getInstance().getPortrainHeight();
        }
        return displayHeight-drawerBottomBar;
    }
    public int getOrignalHeight(boolean isLandscapeOrientation,Context context){
        int displayHeight=0;
        if (isLandscapeOrientation) {
            displayHeight = UserPref.getInstance().getLandHeight();
        } else {
            displayHeight = UserPref.getInstance().getPortrainHeight();
        }
        return displayHeight;
    }
}
