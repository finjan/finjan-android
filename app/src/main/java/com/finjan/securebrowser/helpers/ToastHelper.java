package com.finjan.securebrowser.helpers;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.avira.common.backend.WebUtility;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.JsonParser;
import com.finjan.securebrowser.vpn.helpers.JsonConstans;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 */


public class ToastHelper {
    public static void showToast(final Activity activity, final String msg, final int toastLength){
        if(NativeHelper.getInstnace().checkActivity(activity)){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity,msg, toastLength).show();
                }
            });
        }
    }
    public static void showToast(final Activity activity, final VolleyError error, final int toastLength){
        if(NativeHelper.getInstnace().checkActivity(activity)){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(error==null || WebUtility.getMessage(error)==null){
                        return;
                    }
                    try {
                        JSONObject jsonObject = new JSONObject(WebUtility.getMessage(error));
                        String msg = JsonParser.getInstance().getMsgFromError(jsonObject);
                        if (!TextUtils.isEmpty(msg)) {
                            Toast.makeText(activity,msg, toastLength).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    public static void showToast(Context context, String msg, int toastLength) {
        if (context == null) {
            context = FinjanVPNApplication.getInstance().getApplicationContext();
        }
        Toast.makeText(context, msg, toastLength).show();
    }
    public static void showToast(String msg, int toastLength){
        showToast(FinjanVPNApplication.getInstance().getApplicationContext(),msg,toastLength);
    }

    public void serverErrorToast(Context context) {
        showToast(context, context.getString(R.string.unable_to_connect_try_again_after_some_time));
    }

    public void showToast(Context context, VolleyError error) {
        if(error==null || WebUtility.getMessage(error)==null){
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(WebUtility.getMessage(error));
            String msg = JsonParser.getInstance().getMsgFromError(jsonObject);
            if (!TextUtils.isEmpty(msg)) {
                if (context == null) {
                    context = FinjanVPNApplication.getInstance().getApplicationContext();
                }
                showToast(context, msg, Toast.LENGTH_SHORT);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showToast(Context context, Object object) {
        if (object instanceof VolleyError) {
            showToast(context, (VolleyError) object);
        } else if (object instanceof JSONObject) {
            try {
                JSONObject response = (JSONObject) object;
                if (response.has(JsonConstans.MESSAGE)) {
                    NativeHelper.getInstnace().showToast(context, response.getString(JsonConstans.MESSAGE), Toast.LENGTH_SHORT);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
