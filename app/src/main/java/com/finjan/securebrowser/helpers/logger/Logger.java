package com.finjan.securebrowser.helpers.logger;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.finjan.securebrowser.BuildConfig;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 * <p>
 * class for custom log message depends upon if app is in debug mode or not.
 */
public class Logger extends HandlerThread {

    private static Logger myInstance;
    private Handler handler;

    private Logger() {
        super("LoggerThread");
    }

    public static Logger getInstance() {
        if (myInstance == null) {
            myInstance = new Logger();
        }
        return myInstance;
    }

    private Handler getHandler(Looper looper) {
        return new Handler(looper){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                LogMessage data = (LogMessage) msg.obj;

                saveToFile(data.key,data.message);
            }
        };
    }

    private void sendLogs(LogMessage data){
        if (handler == null){
            return;
        }
        Message message = new Message();
        message.obj = data;

        handler.sendMessage(message);

    }

    @Override
    protected void onLooperPrepared() {
        Log.e("onLooperPrepared","onLooperPrepared");
        super.onLooperPrepared();
        handler = getHandler(getLooper());
    }

    public static void logV(String key, String value) {

        if (BuildConfig.IsDebuging) {
            Log.v(key, "->" + value);
        }
        if (AppConfig.Companion.getDebugReporting()) {
            getInstance().sendLogs(new LogMessage(key,value));
//            saveToFile(key, value);
        }
    }

    public static void logD(String key, String value) {
        if (BuildConfig.IsDebuging) {
            Log.d(key, "->" + value);
        }
        if (AppConfig.Companion.getDebugReporting()) {
//            saveToFile(key, value);
            getInstance().sendLogs(new LogMessage(key,value));
        }
    }

    public static void logE(String key, String value) {
        if (BuildConfig.IsDebuging) {
            Log.e(key, "->" + value);
        }
        if (AppConfig.Companion.getDebugReporting()) {
//            saveToFile(key, value);
            getInstance().sendLogs(new LogMessage(key,value));
        }
    }


    public static void logI(String key, String value) {
        if (BuildConfig.IsDebuging) {
            Log.i(key, "->" + value);
        }
        if (AppConfig.Companion.getDebugReporting()) {
//            saveToFile(key, value);
            getInstance().sendLogs(new LogMessage(key,value));
        }
    }

    private static void saveToFile(String title, String message) {
        String yourFilePath = FinjanVPNApplication.getInstance().getFilesDir() + "/" + "invincibull_logs.txt";
        File file = new File(yourFilePath);
        String exStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(exStorageState)) {

            try {
//                File root = Environment.getExternalStorageDirectory();
//                String path = root + "/InvinciBull/";
                boolean exists = (file.exists());
                if (!exists) {
                    new File(FinjanVPNApplication.getInstance().getFilesDir() + "").mkdirs();
                }
//                File logFile = new File(path + "InvinciBull_traces.txt");
                boolean isNew = file.createNewFile();
                FileWriter logWriter = new FileWriter(file, true);
                BufferedWriter outer = new BufferedWriter(logWriter);
                outer.write("\r\n");
                outer.write("\r\n");
                outer.write("\r\n");
                outer.write("Time: " + DateHelper.getInstnace().getCurrentDate(DateHelper.SERVER_FORMAT_3));
                outer.write("\r\n");
                outer.write("Title: " + title);
                outer.write("\r\n");
                outer.write("Message: " + message);
                outer.write("\r\n");
                outer.write("------------------------------------");

                outer.close();
                logWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
                //          Toast.makeText(context, "Couldn't save", Toast.LENGTH_SHORT);
            }
        } else {
            //FAIL
            System.out.println("File not accessible");
            // Toast.makeText(context, "File not accessible", Toast.LENGTH_SHORT).show();
        }
    }


    public static void sendEmailOfLog(final Context context) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{});
        intent.putExtra(Intent.EXTRA_SUBJECT, "InvinciBull Debug Logs");
        intent.putExtra(Intent.EXTRA_TEXT, "Please find attached Log file");

        String yourFilePath = context.getFilesDir() + "/" + "invincibull_logs.txt";

        File file = new File(yourFilePath);
        if (!file.exists() || !file.canRead()) {
            Toast.makeText(context, "Attachment Error", Toast.LENGTH_SHORT).show();
            return;
        }
        Uri outputFileUri = null;
        outputFileUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);

        if (intent.resolveActivity(context.getPackageManager()) != null) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(Intent.EXTRA_STREAM, outputFileUri);
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                context.grantUriPermission(packageName, outputFileUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            context.startActivity(Intent.createChooser(intent, "Send Logs via..."));
        }
    }

    private static class LogMessage{
        public String key;
        public String message;

        public LogMessage(String key, String message){
            this.key = key;
            this.message = message;
        }
    }

}
