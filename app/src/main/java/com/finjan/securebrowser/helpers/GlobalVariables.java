package com.finjan.securebrowser.helpers;

import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;

import com.finjan.securebrowser.service.NotifyUnSecureNetwork;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.vpn.social.SocialUserProfile;
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment;
import com.finjan.securebrowser.fragment.FragmentAppPurchase;
import com.finjan.securebrowser.fragment.FragmentHome;
import com.finjan.securebrowser.fragment.FragmentSetting;
import com.finjan.securebrowser.model.SafetyResult;
import com.finjan.securebrowser.model.SkuModel;
import com.finjan.securebrowser.model.TrackerFoundModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 */

public class GlobalVariables {

    private static final class GlobalVariablesHolder{
        private static final GlobalVariables INSTNACE=new GlobalVariables();
    }

    //features on off
    public static boolean autoWifiConnect=false;
    public static boolean user_registered=false;

    public SocialUserProfile ursWaitingForPolicy;

    private GlobalVariables(){}
    public static String orignal_brower_option_name="";

    public static GlobalVariables getInstnace(){
        return GlobalVariablesHolder.INSTNACE;
    }
    public boolean isPurchaseResponseReceived;
    public ArrayList<SkuModel> responseList;
    public boolean  isMonthlyAutoRenewal,isYearlyAutoRenewal;
    public String downloadURL,downloadfileName;
    public long mDownloadedFileID=-1;

    public boolean isNewTab,isKeyboardShowing,isDrawingComplete,isDrawerShowing,barGoneClicked,isFirstFragmentIdFixed;
    public String currentFragmentTag,currentUrl;
//    public Long currentId;
    public FragmentHome currentHomeFragment;
    public FragmentAppPurchase appPurchaseFragment;

    public boolean isFinishCalled,isHomeActivityCalled;
    public boolean safeBarVisibility,fabVisibility,appbarVisibility;
    public FragmentSetting fragmentSetting;

    // trick to save from hiding keyborad  while user change orientation
    public boolean orientationIsJustChanged,justBackPressed,isCheckingScreenLock,shouldPageReload;
    public static boolean blockTracking=true;

    public boolean tempIsReportAvailable,tempIsTrackerAvailable;
    public SafetyResult tempSafetyResult;
    public ArrayList<TrackerFoundModel> tempTrackerFoundList;
    public boolean homeFragmentInitiated,forceLoader,isUserNotifiedWithRegiredId,appCodeSetNewly;
    public boolean isDrawerBottomShowing=true;
//    public boolean userStatusChanged=true;
    public boolean isAppUpgraded,isLicenseFetched,showPopPupAfterRegistrationFromHome,appSwitchSettingChanged;
    public VpnHomeFragment mainActivityFragment;
    public NotifyUnSecureNetwork notifyUnSecureNetworkService;
    public String lastDirectUrl ="";
    public String lastDirectConsumedUrl ="";

    public boolean isLaunchVpnAcivityCalled =false;
    /**
     * tell should we show app share popup on VPN Screen
     */
    public void fixForPopPupAfterRegistrationFromHome(){
        showPopPupAfterRegistrationFromHome=true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showPopPupAfterRegistrationFromHome=false;
            }
        },5000);
    }
    public LastNetworkConnection lastNetworkConnection = null;

    public class LastNetworkConnection{
        public Context context;
        public NetworkResultListener listener;
        public String correspondingClass,url;
        public JSONObject jsonObject;
        public boolean isJsonRequest = false;
        LastNetworkConnection(@NonNull Context context, @NonNull final NetworkResultListener networkResultListener, final @NonNull String url,
                              @NonNull final String correspondingExceptionClass){
            this.context= context;
            this.listener= networkResultListener;
            this.url = url;
            this.correspondingClass = correspondingExceptionClass;
        }
        LastNetworkConnection(@NonNull Context context, @NonNull final NetworkResultListener networkResultListener,
                              @NonNull final String url, final @NonNull JSONObject jsonObject, final @NonNull String correspondingExceptionClass){
            this.context=context;
            this.listener=networkResultListener;
            this.url = url;
            this.correspondingClass= correspondingExceptionClass;
            this.jsonObject= jsonObject;
        }
    }
}
