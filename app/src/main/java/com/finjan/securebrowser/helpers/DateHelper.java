package com.finjan.securebrowser.helpers;

import android.text.TextUtils;

import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.util.TrafficController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 * Date helper class used in various classes, however replacing it with other lib.
 *
 */

public class DateHelper {

//    private static final String serverFormat="dd/MM/yyyy hh:mm:ss.SSS";
    private static final String serverFormat="yyyy-MM-dd HH:mm:ss";
    private static final class DateHelperHolder{
        private static final DateHelper INSTANCE=new DateHelper();
    }
    public static DateHelper getInstnace(){
        return DateHelperHolder.INSTANCE;
    }
    private DateHelper(){};
    public int numofdays(Date date) {
        Date today = new Date();
        return (int) ((today.getTime() - date.getTime()) / (1000 * 60 * 60 * 24));
    }

    public boolean isFuture(long timeToCompare){
        Calendar calendarToCompare=Calendar.getInstance();
        calendarToCompare.setTimeInMillis(timeToCompare);

        Calendar currentCalender=Calendar.getInstance();
        return currentCalender.before(calendarToCompare);
    }
    public String getServerFormatDate(long miliSec){
        return getServerFormatDate(miliSec,SERVER_FORMAT);
    }
    public String getCurrentDate(String format){
        return getServerFormatDate(System.currentTimeMillis(),format);
    }

    public String getServerFormatDate(long miliSec, String format){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliSec);
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.US);
        String serverDate=formatter.format(calendar.getTime());
        if(!TextUtils.isEmpty(serverDate) && serverDate.split("-",1)[0].startsWith("-")){
            formatter.setTimeZone(TimeZone.getTimeZone("gmt"));
        }
        return formatter.format(calendar.getTime());
    }


    public long getLongFromDate(String dateString){
        SimpleDateFormat format = new SimpleDateFormat(SERVER_FORMAT,Locale.US);
        try {
            Date date = format.parse(dateString);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
    public boolean isApproxSameTime(long compareDate,String backEndDate){
        long different=compareDate-getLongFromDate(backEndDate);
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;
        return elapsedDays==0 && elapsedHours<10;
    }
    public boolean isFutureDate(long firstDate,long secondDate ){
        Calendar calendarFirst=Calendar.getInstance();
        calendarFirst.setTimeInMillis(firstDate);
        Calendar calendarSecond=Calendar.getInstance();
        calendarSecond.setTimeInMillis(secondDate);
        return calendarFirst.after(calendarSecond);
    }
    public boolean isNotMoreThan1DayDiff(long compareDate){
        return isMoreThanDaysDiff(1,compareDate,"");
    }

    public boolean isMoreThanDaysDiff(int days, long dateToCompare, String stringDateToCompareWith){
        long dateToCompareWith = TextUtils.isEmpty(stringDateToCompareWith)?System.currentTimeMillis():getLongFromDate(stringDateToCompareWith);

        Long difference=dateToCompareWith-dateToCompare;
        difference = Math.abs(difference);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = difference / daysInMilli;
        difference = difference % daysInMilli;
        return elapsedDays>days;
    }

    public boolean isMoreThan3DaysDiff(long dateToCompare,String backEndDate){
        return isMoreThanDaysDiff(3,dateToCompare,backEndDate);
    }
    public static final String SERVER_FORMAT_3= "dd MMM yyyy HH:mm:ss";
    public static final String SERVER_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String SERVER_FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public String getCurrentGMTTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);
        formatter.setTimeZone(TimeZone.getTimeZone("gmt"));
        return formatter.format(new Date());
    }
    public Calendar getServerDateCalender(String serverFormat){
        return getServerDateCalender(serverFormat,SERVER_FORMAT);
    }
    public Calendar getServerDateCalender(String serverFormat,String dateFormat){
        SimpleDateFormat format = new SimpleDateFormat(dateFormat,Locale.US);
        try {
            Date date = format.parse(serverFormat);
            Calendar calendar=Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        } catch (ParseException e) {
            e.printStackTrace();
            return Calendar.getInstance();
        }
    }
    public String getCurrentDateMonth(){
        SimpleDateFormat formatter = new SimpleDateFormat("MMM",Locale.US);
        return formatter.format(new Date());
    }
}
