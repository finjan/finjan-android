package com.finjan.securebrowser.helpers;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.avira.common.GeneralPrefs;
import com.avira.common.authentication.Authentication;
import com.avira.common.authentication.AuthenticationListener;
import com.avira.common.authentication.RefreshTokenLintener;
import com.avira.common.authentication.models.LoginResponse_1;
import com.avira.common.authentication.models.RefreshTokenResponse;
import com.avira.common.authentication.models.Subscription;
import com.avira.common.authentication.models.User;
//import com.crashlytics.android.Crashlytics;
import com.finjan.securebrowser.custom_exceptions.CustomExceptionConfig;
import com.finjan.securebrowser.custom_exceptions.TokenRefreshApiException;
import com.finjan.securebrowser.custom_exceptions.UserLoginApiException;
import com.finjan.securebrowser.custom_exceptions.UserRegisterApiException;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.FinjanUser;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.vpn.eventbus.CancelConnectingEvent;
import com.finjan.securebrowser.vpn.eventbus.FinjanUserFetched;
import com.finjan.securebrowser.vpn.helpers.DeviceUpdateHelper;
import com.finjan.securebrowser.vpn.ui.authentication.AuthNavigationHelper;
import com.finjan.securebrowser.vpn.ui.authentication.SetupAccountActivity;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;
import com.finjan.securebrowser.vpn.ui.main.VpnApisHelper;
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
//import com.localytics.android.Localytics;

import org.json.JSONObject;

import de.greenrobot.event.EventBus;
//import io.fabric.sdk.android.Fabric;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 * <p>
 * class that help pre and post work in all authentication for application
 */

public class AuthenticationHelper {

    private static boolean isTokenRefereshing;
    private static Handler tokenRefereshHandler;
    private static Context applicationContext;
    private static int expiresIn;
    private static String scope, tokenType;


    private static Runnable tokenRefershRunnable = new Runnable() {
        @Override
        public void run() {
            if (applicationContext != null) {
                refreshToken(FinjanVpnPrefs.getRefershToken(applicationContext), false, null);
            }
        }
    };
    private FinjanUser finjanUser;

    public static AuthenticationHelper getInstnace() {
        return AuthenticationHelperHolder.Instance;
    }

    public static boolean isTokenRefereshing() {
        return isTokenRefereshing;
    }

    private static void refreshToken(String refreshToken, final boolean showProgress, final AutoLoginResult autoLoginResult) {
        if (!UserPref.getInstance().isTokenExpires()) {
            return;
        }
        Authentication.refereshToken(applicationContext, refreshToken,
                new RefreshTokenLintener() {
                    @Override
                    public void onRefershSuccess(RefreshTokenResponse refreshTokenResponse) {

                        if (showProgress) {
                            DeviceUpdateHelper.updateCurrentDevice(applicationContext, null);
                            if (autoLoginResult != null) {
                                autoLoginResult.autoLoginResults(true);
                            }
                        }
                        FinjanVpnPrefs.setAuthToken(FinjanVPNApplication.getInstance().getApplicationContext(),
                                refreshTokenResponse.getAccess_token());
                        FinjanVpnPrefs.setRefereshToken(FinjanVPNApplication.getInstance().getApplicationContext(),
                                refreshTokenResponse.getRefresh_token());
                        expiresIn = refreshTokenResponse.getExpires_in();
                        scope = refreshTokenResponse.getScope();
                        tokenType = refreshTokenResponse.getToken_type();
                        UserPref.getInstance().setTokenExpiresIn(expiresIn);
                        updateToken();
//                        VpnHomeFragment.showLoginScreen();
                    }

                    @Override
                    public void onErrorResponse(int errorCode, String errorMessage, String request) {

                        CustomExceptionConfig.getInstance().logException("https://ulm.finjanmobile.com/oauth/token",
                                errorMessage, request, TokenRefreshApiException.class.getSimpleName());

                        String[] data = UserPref.getInstance().getCachedUserForAutoLogin();
                        if (data != null && data.length == 3 && !TextUtils.isEmpty(data[1])) {
                            autoLogin(applicationContext, autoLoginResult);
                        } else {
                            VpnHomeFragment.showLoginScreen();
                        }

                    }
                });
    }

    public void initRefreshToken(AutoLoginResult autoLoginResult) {

        applicationContext = FinjanVPNApplication.getInstance().getApplicationContext();
        if (!TextUtils.isEmpty(FinjanVpnPrefs.getRefershToken(applicationContext))) {
            if (tokenRefereshHandler == null) {
                tokenRefereshHandler = new Handler();
            }
            refreshToken(FinjanVpnPrefs.getRefershToken(applicationContext), true, autoLoginResult);
            isTokenRefereshing = true;
        }
    }

    public String getUserName(Context context) {

        if (finjanUser != null) {
            if (!TextUtils.isEmpty(finjanUser.getUserName())) {
                return finjanUser.getUserName();
            }

        } else {
            if (!TextUtils.isEmpty(GeneralPrefs.getUserName(context))) {
                return GeneralPrefs.getUserName(context);
            }
            if (!TextUtils.isEmpty(GeneralPrefs.getEmailId(context))) {
                return GeneralPrefs.getEmailId(context);
            }
        }
        return "";
    }

    public String getUserEmail(Context context) {
        if (finjanUser != null) {
            if (!TextUtils.isEmpty(finjanUser.getEmail())) {
                return finjanUser.getEmail();
            }
        }
        return GeneralPrefs.getEmailId(context);
    }

    public FinjanUser getFinjanUser() {
        return finjanUser;
    }

    public void setFinjanUser(FinjanUser finjanUser) {
        if (finjanUser != null) {
            VpnApisHelper.Companion.setUserFetched(true);
        }
        this.finjanUser = finjanUser;
    }

    public void fetchUser(final Context context) {
        fetchUser(context, new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                FinjanUser finjanUser = new FinjanUser(response);
                AuthenticationHelper.getInstnace().setFinjanUser(finjanUser);
                if (!UserPref.getInstance().getUserSendLocalitics() && finjanUser != null) {

//                        if (!TextUtils.isEmpty(finjanUser.getId()))
//                            Localytics.setCustomerId(finjanUser.getId());
//                        if (!TextUtils.isEmpty(finjanUser.getEmail()))
//                            Localytics.setCustomerEmail(finjanUser.getEmail());
//                        if (!TextUtils.isEmpty(finjanUser.getFirst_name()))
//                            Localytics.setCustomerFullName(finjanUser.getFirst_name());

                    UserPref.getInstance().setUserSendLocalitics(true);
                }
                EventBus.getDefault().post(new FinjanUserFetched());
            }

            @Override
            public void executeOnError(VolleyError error) {
//                Toast.makeText(context, "Unable to fetch user info.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void fetchUser(final Context context, NetworkResultListener networkResultListener) {
        NetworkManager.getInstance(context).fetchUser(context, networkResultListener);
    }

    public static void initRefreshToken(LoginResponse_1 loginResponse_1) {

        UserPref.getInstance().setTokenExpiresIn(loginResponse_1.getExpires_in());
        applicationContext = FinjanVPNApplication.getInstance().getApplicationContext();
        isTokenRefereshing = true;
        FinjanVpnPrefs.setAuthToken(applicationContext, loginResponse_1.getAccess_token());
        FinjanVpnPrefs.setRefereshToken(applicationContext, loginResponse_1.getRefresh_token());
//        UserPref.getInstance().setCurrent_Access_Token(loginResponse_1.getAccess_token());
//        UserPref.getInstance().setCurrent_Refresh_Token(loginResponse_1.getRefresh_token());
        expiresIn = loginResponse_1.getExpires_in();
        scope = loginResponse_1.getScope();
        tokenType = loginResponse_1.getToken_type();

        tokenRefereshHandler = new Handler();
        tokenRefereshHandler.postDelayed(tokenRefershRunnable, expiresIn * 1000);
    }

    public static void updateToken() {
        if (tokenRefereshHandler != null) {
            tokenRefereshHandler.postDelayed(tokenRefershRunnable, expiresIn * 1000);
        }

    }

    public void stopRefreshing() {
        if (tokenRefereshHandler != null) {
            tokenRefereshHandler.removeCallbacks(tokenRefershRunnable);
        }
        tokenRefereshHandler = null;
    }

    public static void hitAviraForlicense() {

//        NetworkManager.getInstance(FinjanVPNApplication.getInstance()).fetchAviraLicence(FinjanVPNApplication.getInstance(),
//                new NetworkResultListener() {
//            @Override
//            public void executeOnSuccess(JSONObject response) {
//                Logger.logE("NetworkManager", "avira license api success");
//            }
//
//            @Override
//            public void executeOnError(VolleyError error) {
//                Logger.logE("NetworkManager", "avira license api failed "+ error.getMessage());
//            }
//        });
    }

    /**
     * not a right way remove use of this
     */
    public static void autoLogin(final Context context, final AutoLoginResult autoLoginResult) {


        UserPref.getInstance().setEmailLogin(true);
        if (UserPref.getInstance().getCachedUserForAutoLogin() == null) {
            return;
        }
//        if(TextUtils.isEmpty(GeneralPrefs.getEmailId(context)) || TextUtils.isEmpty(GeneralPrefs.getEmailId(context))){return;}
        String[] data = UserPref.getInstance().getCachedUserForAutoLogin();
        AuthNavigationHelper.loginUser(FinjanVPNApplication.getInstance(), data[0],
                data[1], data[2], new AuthenticationListener() {
                    @Override
                    public void onAuthSuccess(User user, Subscription subscription) {

                    }

                    @Override
                    public void onAuthError(int errorCode, String errorMessage, String request, String detailMsg) {

                        if (autoLoginResult != null) {
                            autoLoginResult.autoLoginResults(false);
                        }
                        VpnHomeFragment.showLoginScreen();
//                        Toast.makeText(context, "Something went wrong,Please login again", Toast.LENGTH_SHORT).show();
                        CustomExceptionConfig.getInstance().logException("https://ulm.finjanmobile.com/oauth/token",
                                request, errorMessage + "----" + detailMsg, UserLoginApiException.class.getSimpleName());
                    }

                    @Override
                    public void onAuthSuccess(LoginResponse_1 loginResponse_1) {
                        if (autoLoginResult != null) {
                            autoLoginResult.autoLoginResults(true);
                        }

                        initRefreshToken(loginResponse_1);
                    }
                });
//        Authentication.loginEmail(applicationContext,
//                GeneralPrefs.getEmailId(context),
//                GeneralPrefs.getPassword(context), true,
//                new AuthenticationListener() {
//                    @Override
//                    public void onAuthSuccess(User user, Subscription subscription) {
//
//                    }
//
//                    @Override
//                    public void onAuthError(int errorCode, String errorMessage, String request,String detailMsg) {
//                        if (VpnActivity.getInstance() != null) {
//                            SetupAccountActivity.newLoginInstanceForResult(VpnActivity.getInstance(),
//                                    VpnActivity.REQUEST_LOGIN, SetupAccountActivity.ACTION_SIGN_IN);
//                        }
//                        if (autoLoginResult != null) {
//                            autoLoginResult.autoLoginResults(false);
//                        }
//                        Toast.makeText(context, "Something went wrong,Please login again", Toast.LENGTH_SHORT).show();
//                        CustomExceptionConfig.getInstance().logException("https://ulm.finjanmobile.com/oauth/token",
//                                request,errorMessage+"----"+detailMsg,UserLoginApiException.class.getSimpleName());
//                    }
//
//                    @Override
//                    public void onAuthSuccess(LoginResponse_1 loginResponse_1) {
//                        if (autoLoginResult != null) {
//                            autoLoginResult.autoLoginResults(true);
//                        }
//                        initRefreshToken( loginResponse_1);
//                    }
//                });
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public String getScope() {
        return scope;
    }

    public String getTokenType() {
        return tokenType;
    }

    public interface AutoLoginResult {
        void autoLoginResults(boolean success);
    }

    private static final class AuthenticationHelperHolder {
        private static final AuthenticationHelper Instance = new AuthenticationHelper();
    }

    public interface TokenRefreshed {
        void onTokenRefreshed();
    }

}
