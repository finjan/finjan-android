/*
 * *
 *  * <p>
 *  * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 *  * Finjan Mobile Vital Security Browser
 *  * <p>
 *  * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *  * Developed by NewOfferings, LLC.
 *  *
 *  *
 *
 */

package com.finjan.securebrowser.helpers;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import androidx.annotation.NonNull;

import android.os.Looper;
import android.text.TextUtils;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.InAppPurchase;
import com.finjan.securebrowser.model.ModelManager;
import com.finjan.securebrowser.model.RateUsModel;
import com.finjan.securebrowser.model.RattingOptionPlistModel;
import com.finjan.securebrowser.model.ReportCard;
import com.finjan.securebrowser.parser.PList;
import com.finjan.securebrowser.parser.PListParser;
import com.finjan.securebrowser.tracking.facebook.FbTrackingConfig;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.kochava.KochavaConfig;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.tracking.tune.TuneTracking;
import com.finjan.securebrowser.tracking.tune.TuneTrackingConfig;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.util.FinjanUrls;
import com.finjan.securebrowser.util.JsonUtils;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.ui.main.VPNDrawer;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;



public class PlistHelper {

    private static final String def_lang = "en";
    public static String vpnLocationChangeAlert,
            vpnLocationChangeAlertTitle, vpnLocationChangeAlertOkBtn, vpnLocationChangeAlertCBtn,
            upgradeDefaultAlert, upgradeDefaultAlertTitle, upgradeDefaultAlertOkBtn, upgradeDefaultAlertCBtn, upgradeAlertAflterLogin,
            upgradeAlertAflterLoginOkBtn, upgradeAlertAflterLoginCBtn, interestedbutton, laterbutton, okbutton,
            footertext, IAPInterestedButtonBg, IAPLaterButtonBg, IAPOKButtonBg,
            CLCancelBtn, CLOkButton;
    private PList parentPlist;
    private String homePage;
    private int dataGaugeMultiplier = 0;
    private boolean shouldGenerateRandomId = false;
    private boolean ShouldShowLocalNotificationForTraffic = false;
    private HashMap<String, ArrayList<String>> whiteListMap = new HashMap<>();
    private ArrayList<String> categoryWhiteList = new ArrayList<>();
    private int searchEngineType;
    private InAppPurchase inAppPurchase;
    private PList appStrings, trackerOptions;
    private HashMap<REPORT_TYPE, ReportCard> reportCardHashMap;
    private ArrayList<String> inAppButtetPoints;
    private String inAppTitle;
    private ArrayList<String> factoryArray;
    private String googleEnalyticKey, vinceAnimationCount, appUrl, helpUrl, aboutUrl, passwordResetUrl, eula_url, maxTabCount, bingKey, userLocalDb, recommend,
            showFreeUserLocationChangeEventNotification;
    private boolean isTrackerEnabled;
    private String appVersion, updateMessage, trackerDisableMessage, registrationPopUpText, vpnScreenPopUpText,
            vpndownloadtext, vpntipcount, safeScanDisableMsg,
            chooseVpnMsg, chooseBrowerMsg, vpnsharepopup, invincibull_update, vpnsharetext;

    private String emailValidationProvider,emailValidationOption,emailValidationWarnMsg,emailValidationErrMsg,socialEmailReq;

    private String trackerFileLocation;
    private RattingOptionPlistModel rattingOptionPlistModel;
    private boolean defaultTrackerStatus, shouldShowPopUpMsg;
    private String milicousValue, malwareValue;
    private String tipVPN, tipTab, tipHome, tipBookmark, tipDrawer, tipVS, tipTracker;
    private boolean jugad = false;
    private String privacyPolicyUpdateText, eulaSkipMsgText, privacyStatementLink, updated_policy;
    private HashMap<String, String[]> settingMenu;
    //set vpn menu drawer icons , name and actions
    private HashMap<String, VpnMenu> vpnMenuMap;
    private ArrayList<OutOfDataNotification> outOfDataNotificationArrayList;
    private Integer[] arrayOfnotificationPersentage;

    public ArrayList<OutOfDataNotification> getOutOfDataNotificationArrayList() {
        return outOfDataNotificationArrayList;
    }

    public ArrayList<String> getTutorialScreenList() {
        return tutorialScreenList;
    }

    private ArrayList<String> tutorialScreenList;

    private String privacyMassage;
    private String privacyHtmlMassage;
    private String ignoreButton;
    private String acceptButton;

    public boolean isHtml() {
        return isHtml;
    }

    private boolean isHtml = false;

    public String getPrivacyMassage() {
        return privacyMassage != null ? privacyMassage:"";
    }

    public String getPrivacyHtmlMassage() {
        return privacyHtmlMassage;
    }

    public String getIgnoreButton() {
        return ignoreButton;
    }

    public String getAcceptButton() {
        return acceptButton;
    }



    private PlistHelper() {
    }

    public static PlistHelper getInstance() {
        return PlistHelperHolder.Instance;
    }

    public static String getPlistvalue(String dict, String key) {
        String value = "";
        PList plist = ModelManager.getInstance().getPList();
        if (plist == null) {
            return value;
        } else {
            try {
                Map<String, PList> mapValue = plist.getMap();
                if (mapValue.containsKey(dict)) {
                    PList dictValue = mapValue.get(dict);
                    if (dictValue != null) {
                        if (TextUtils.isEmpty(key)) {
                            value = dictValue.data.toString();
                        } else {
                            value = dictValue.get(key).data.toString();
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
        return value;
    }

    public static String getREGISTRATIONPOPUPTEXT() {
        return "Thanks for registering for VitalSecurityVPN. Our VPN remains active when you leave the app, so once connected you can switch to other apps and continue to hide your location and encrypt your data.\n Our VPN also has free desktop versions for both Windows and Mac to help you stay secure and hidden when on your computer. You can download these programs from finjanmobile.com or click on the SHARE link below to send the download links to yourself or to friends and family members.";
    }

    public static String getVPNDOWNLOADTEXT() {
        return "Download the Windows and Mac desktop versions of VitalSecurityVPN now to stay secure and hidden when on your computer.\n WINDOWS: https://install.avira-update.com/package/finjanvpn/win/int/vpninstaller.exe \n MAC: https://install.avira-update.com/package/finjanvpn/osx/int/vpn.pkg";
    }

//    public static String getVPNSCREENPOPUPTEXT() {
//        return "Our VPN has free desktop versions for both Windows and Mac to help you stay secure and hidden when on your computer. You can download these programs from finjanmobile.com or click on the SHARE link below to send the download links to yourself or to friends and family members.";
//    }

    private static String realStringReplace(final String victim, final String target,
                                            final String replacement) {

        final int skip = target.length();
        final StringBuilder sb = new StringBuilder(victim.length());

        String tmp = victim;
        int index;

        while (!tmp.isEmpty()) {
            index = tmp.indexOf(target);
            if (index == -1)
                break;
            sb.append(tmp.substring(0, index)).append(replacement);
            tmp = tmp.substring(index + skip);
        }

        return sb.append(tmp).toString();
    }

    public void setDefaults() {
        setPlists();
        GoogleTrackers.Companion.getInstance().setPlistData(parentPlist);
        LocalyticsTrackers.Companion.getInstance().setPlistData(parentPlist);
        setScannerDataFiler();
        setRattingOption();
        setInAppSettings();
        setAppStatics();
        setAnalyticalTrackers();
        setInAppPurchaseModel();

        setGoogleEnalyticKey();
        setLocalyticsKey();
        setHomePage();
        setWhiteList();
        setSearchEngineType();
        setTrackerPart();
        setParentLevelValues();
        setDefaultSettings();
        setAlerts();

        setEmailValidationProvider();
    }

    private void setDefaultSettings() {
        setPlistExpirePeriodInHours();
        setTimeIntervalForUpdateCache();
        setTimeIntervalForHitApi();
        setTimeIntervalForUpdateCacheOfTraffic();
        setSafeScanDisableMessage();
        setTrackerDisableMessage();
        setDefaultTrackerStatus();
        setAppshareMessage();
        setPrivacyMassage();
        setPrivacyHtmlMassage();
        setIgnoreButton();
        setAcceptButton();
        setisHtml();
        setVpndownloadtext();
        setVpnScreenPopUpText();
        setVpnSharePopUpText();
        setvpnsharetextText();
        setinvincibull_updateText();
        setShouldShowPopUpMsg();
        setTipsStrings();
        setChooseBrowerMsg();
        setChooseVpnMsg();
        setPrivacyPolicyUpdateText();
        setEulaSkipMsgText();
        setEula_url();
        setPasswordResetUrl();
        setPrivacyStatementLink();
        setSettingScreenMenuOptions();
        setUpdated_policy();
        setTutorialScreen();
        setPromoVar();
        setNotification();
        setVpnMenuDrawer(parentPlist);
        setOutOfDataNotificationArrayList(parentPlist);
        setDataGaugeMultiplier(parentPlist);
        setIfRandomNoGenerate();
        setShouldShowLocalNotificationForTraffic();
        setAuttonConnectionDef();

        setLottiAnim();
    }


    private boolean showFestiveAnim;
    private String localLottieKey = "";

    private void setLottiAnim() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_events + "." + PlistConstants.KEY_animations) != null) {
            if (parentPlist.get("events.animations.show_event_animations") != null &&
                    parentPlist.get("events.animations.show_event_animations").data.equalsIgnoreCase("true")) {
                showFestiveAnim = true;
            }
            if (parentPlist.get("events.animations.loadLocalKey") != null) {
                localLottieKey = parentPlist.get("events.animations.loadLocalKey").data;

            }
        }
    }

    private void setEmailValidationProvider() {
        emailValidationProvider = getDictLevelValue(PlistConstants.KEY_EMAIL_VALIDATION, PlistConstants.KEY_EMAIL_VAL_PROVIDER, parentPlist);
        /*try {


        PList emailRegistration = getDictLevelPlist(PlistConstants.KEY_EMAIL_VALIDATION, PlistConstants.KEY_EMAIL_REG, parentPlist);
        emailValidationOption = emailRegistration.getData(PlistConstants.KEY_EMAIL_VAL_OPT);
        emailValidationWarnMsg = emailRegistration.getData(PlistConstants.KEY_EMAIL_WARN_MSG);
        emailValidationErrMsg = emailRegistration.getData(PlistConstants.KEY_EMAIL_ERR_MSG);
            socialEmailReq = getDictLevelValue(PlistConstants.KEY_SOCIAL_REG, PlistConstants.KEY_SOCIAL_EMAIL_REQ, emailRegistration);
        }catch (Exception ex)
        {*/

        emailValidationOption = getDictLevelValue("emailvalidations.email_registration",PlistConstants.KEY_EMAIL_VAL_OPT,parentPlist);
        emailValidationWarnMsg = getDictLevelValue("emailvalidations.email_registration",PlistConstants.KEY_EMAIL_WARN_MSG,parentPlist);
        emailValidationErrMsg = getDictLevelValue("emailvalidations.email_registration",PlistConstants.KEY_EMAIL_ERR_MSG,parentPlist);
        socialEmailReq = getDictLevelValue("emailvalidations.email_registration.social_registration",PlistConstants.KEY_SOCIAL_EMAIL_REQ,parentPlist);
//        }


    }


    public boolean getShowFestiveAnim() {
        return showFestiveAnim;
    }

    public String showLottieKey() {
        return localLottieKey;
    }

    /**
     * promos
     */
    private String current_date = "";
    private boolean show_native_subscription_or_cancellation_alert = true;
    private boolean allow_repeat_promos = true;


    private String data_tank_min = "25";
    private String upgrade_discount = "0";

    public boolean isTestmode() {
        return testmode;
    }

    public String getData_tank_min() {
        return data_tank_min;
    }

    private boolean testmode = false;
    private boolean enable_redeem = true;
    private String notification_action_msg = "You have run out of your monthly free VPN data and are no longer protected.Update now to our unlimited plan to remain protected!";
    private String notification_action_diplink_url = "invincibull://identifier=inapp";

    private void setTutorialScreen() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_Tutorialscreens) != null) {
            ArrayList<PList> productpList = parentPlist.get(PlistConstants.KEY_Tutorialscreens).array;
            tutorialScreenList = new ArrayList<>();
            for (PList pList : productpList) {
                tutorialScreenList.add(pList.data);
            }

        }

    }

    private void setPromoVar() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_promos) != null) {
            if (parentPlist.get(PlistConstants.KEY_promos + ".enable_redeem") != null) {
                enable_redeem = parentPlist.get(PlistConstants.KEY_promos + ".enable_redeem").data.equals("true");
            }
            if (parentPlist.get(PlistConstants.KEY_promos + ".testmode") != null) {
                testmode = parentPlist.get(PlistConstants.KEY_promos + ".testmode").data.equals("true");
            }
            if (parentPlist.get(PlistConstants.KEY_promos + ".data_tank_min") != null) {
                data_tank_min = parentPlist.get(PlistConstants.KEY_promos + ".data_tank_min").data/*.equals("true")*/;
            }
            if (parentPlist.get(PlistConstants.KEY_promos + ".upgrade_discount") != null) {
                upgrade_discount = parentPlist.get(PlistConstants.KEY_promos + ".upgrade_discount").data/*.equals("true")*/;
            }
            if (parentPlist.get(PlistConstants.KEY_promos + ".show_native_subscription_or_cancellation_alert") != null) {
                show_native_subscription_or_cancellation_alert = parentPlist.get(PlistConstants.KEY_promos + ".show_native_subscription_or_cancellation_alert").data.equals("true");
            }
            if (testmode) {
                if (parentPlist.get(PlistConstants.KEY_promos + ".allow_repeat_promos") != null) {
                    allow_repeat_promos = parentPlist.get(PlistConstants.KEY_promos + ".allow_repeat_promos").data.equals("true");
                }

                if (parentPlist.get(PlistConstants.KEY_promos + ".current_date") != null && !parentPlist.get(PlistConstants.KEY_promos + ".current_date").data.equals("")) {
                    current_date = parentPlist.get(PlistConstants.KEY_promos + ".current_date").data;
                } else {
                    current_date = "ignore";
                }
            } else {
                allow_repeat_promos = false;
                current_date = "ignore";
            }

            if (DateHelper.getInstnace().getLongFromDate(current_date.trim()) == 0) {
                current_date = "ignore";
            }
//            promo_end_date =parentPlist.get(PlistConstants.KEY_promos+".promo_end_date").data;

        }
    }

    private void setNotification() {
        if (parentPlist != null && parentPlist.get(KEY_Notification_Action) != null) {
            if (parentPlist.get(KEY_Notification_Action + ".msg") != null) {
                notification_action_msg = parentPlist.get(PlistConstants.KEY_promos + ".msg").data;
            }
            if (parentPlist.get(KEY_Notification_Action + ".dip_link_url") != null) {
                notification_action_diplink_url = parentPlist.get(PlistConstants.KEY_promos + ".dip_link_url").data;
            }

        }
    }

    public String getCurrent_date() {
        return current_date/*"2019-03-13 11:30:00"*/;
    }

    public boolean getAllow_repeat_promos() {
        return allow_repeat_promos;
    }

    //    public boolean getLocation_change_testing(){return location_change_testing;}
    public boolean Show_native_subscription_or_cancellation_alert() {
        return show_native_subscription_or_cancellation_alert;
    }

    private String autoConnect_msg;
    private String autoConnect_title;
    private String autoConnect_p_btn;
    private String autoConnect_n_btn;
    private String plistExpirePeriodInHours;
    private String timeIntervalToUpdateCache;
    private String timeIntervalTohitApi;

    public long getTimeIntervalToUpdateCacheoftraffic() {
        try {
            return Long.parseLong(timeIntervalToUpdateCacheoftraffic.trim());
        }catch (Exception ex)
        {
            return 1;
        }
    }
    public long getTimeIntervalToHitApi() {
        try {
            return Long.parseLong(timeIntervalTohitApi.trim());
        }catch (Exception ex)
        {
            return 5;
        }
    }

    private String timeIntervalToUpdateCacheoftraffic;

    public long getTimeIntervalToUpdateCache() {
        try {
            return Long.parseLong(timeIntervalToUpdateCache.trim());
        }catch (Exception ex)
        {
            return 24;
        }
    }

    private void setAuttonConnectionDef() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_OnDemand) != null) {
            if (parentPlist.get("OnDemand.autoConnect") != null) {
                defaultAutoConnect = parentPlist.get("OnDemand.autoConnect").data;
                if (!UserPref.getInstance().getIsDefaultAutoConnectSet()) {
                    UserPref.getInstance().setAutoConnect(defaultAutoConnect.equals("true"));
                    UserPref.getInstance().setIsDefaultAutoConnectSet();
                }
            }
            autoConnect_msg = parentPlist.get("OnDemand.disconnectPopup.message.en").data;
            autoConnect_title = parentPlist.get("OnDemand.disconnectPopup.title.en").data;
            autoConnect_p_btn = parentPlist.get("OnDemand.disconnectPopup.positive_button.en").data;
            autoConnect_n_btn = parentPlist.get("OnDemand.disconnectPopup.negative_button.en").data;
        }
    }

    private void setPlistExpirePeriodInHours() {
        if (AppConfig.Companion.getMakeConfigListExpiry()) {
            plistExpirePeriodInHours = "0";
        } else if (parentPlist != null && parentPlist.get(PlistConstants.KEY_PlistExpirePeriodInHours) != null) {
            plistExpirePeriodInHours = parentPlist.get(PlistConstants.KEY_PlistExpirePeriodInHours).data;
        }
        UserPref.getInstance().setConfigListExpiry(getPlistExpirePeriodInHours1());
    }
    private void setTimeIntervalForUpdateCache() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_timeIntervalToUpdateCache) != null) {
            timeIntervalToUpdateCache = parentPlist.get(PlistConstants.KEY_timeIntervalToUpdateCache).data;
        }

    }
    private void setTimeIntervalForHitApi() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_timeIntervalToHitApi) != null) {
            timeIntervalTohitApi = parentPlist.get(PlistConstants.KEY_timeIntervalToHitApi).data;
        }

    }
    private void setTimeIntervalForUpdateCacheOfTraffic() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_timeIntervalToUpdateCacheoftrafficApi) != null) {
            timeIntervalToUpdateCacheoftraffic = parentPlist.get(PlistConstants.KEY_timeIntervalToUpdateCacheoftrafficApi).data;
        }

    }

    public long getPlistExpirePeriodInHours() {
        return UserPref.getInstance().getConfigListExpiry();
    }

    public int getPlistExpirePeriodInHours1() {
        int defaultPeriod = 24;
        if (TextUtils.isEmpty(plistExpirePeriodInHours)) {
            return defaultPeriod;
        }
        try {
            return Integer.parseInt(plistExpirePeriodInHours);
        } catch (NumberFormatException e) {
            return defaultPeriod;
        } catch (Exception e) {
            return defaultPeriod;
        }
    }

    public String getAutoConnect_msg() {
        if (TextUtils.isEmpty(autoConnect_msg)) {
            return ResourceHelper.getInstance().getString(R.string.if_you_dont_want_to_add_the_current_wifi_network);
        }
        return autoConnect_msg;
    }

    public String getAutoConnect_title() {
        if (TextUtils.isEmpty(autoConnect_title)) {
            return ResourceHelper.getInstance().getString(R.string.if_you_want_to_add_the_current_wifi_network);
        }
        if (autoConnect_title.contains("%@")) {
            autoConnect_title = autoConnect_title.replace("%@", "%1$s");
        }
        return autoConnect_title;
    }

    public String getAutoConnect_p_btn() {
        if (TextUtils.isEmpty(autoConnect_p_btn)) {
            return ResourceHelper.getInstance().getString(R.string.add);
        }
        return autoConnect_p_btn;
    }

    public String getAutoConnect_n_btn() {
        if (TextUtils.isEmpty(autoConnect_n_btn)) {
            return ResourceHelper.getInstance().getString(R.string.stay_connected);
        }
        return autoConnect_n_btn;
    }

    private void setEula_url() {
        eula_url = getParentLavelValue(PlistConstants.KEY_EULA_LINK, parentPlist);
    }

    private void setUpdated_policy() {
        updated_policy = getDictLevelValue(PlistConstants.KEY_PRIVACY, PlistConstants.KEY_UPDATED_AT, parentPlist);
    }

    public String getUpdated_policy() {
        return getString(updated_policy);
    }

    private void setPasswordResetUrl() {
        passwordResetUrl = getParentLavelValue(PlistConstants.KEY_PASSWORD_RESET_LINK, parentPlist);
    }

    private void setPrivacyStatementLink() {
        privacyStatementLink = getDictLevelValue(PlistConstants.KEY_PRIVACY, PlistConstants.KEY_URL, parentPlist);
    }

    private void setSettingScreenMenuOptions() {
        PList pList = getDictLevelPlist(PlistConstants.KEY_SETTINGS_OPTIONS, PlistConstants.KEY_Menus, parentPlist);
        settingMenu = new HashMap<>();
        if (pList != null) {
            for (PList item : pList.array) {
                if (item != null && item.getData("name") != null) {
                    settingMenu.put(item.getData("value"), item.getData("name." + def_lang).split("\\|"));
                }
            }
        }
    }

    private String getSettingMenu(String KEY) {
        if (settingMenu != null && settingMenu.size() > 0) {
            String[] value = settingMenu.get(KEY);
            if (value != null && !TextUtils.isEmpty(value[0])) {
                return value[0];
            }
        }
        return "";
    }

    public String getSMenuChangePasscode() {
        return getSettingMenu("changepasscode");
    }

    public String getSMenuAppSwitch() {
        return getSettingMenu("vpnonly");
    }

    public String getSMenuVPNAutoConnect() {
        return getSettingMenu("vpnautoconnect");
    }

    public String getSMenuTouchId() {
        return getSettingMenu("touchid");
    }

    public String getSMenuRequirePasscode() {
        return getSettingMenu("requirepasscode");
    }

    public String getSMenuClearPrivatedata() {
        return getSettingMenu("clearprivatedata");
    }

    public String getSMenuHomePage() {
        return getSettingMenu("homepage");
    }

    public String getSMenuScanResultBar() {
        return getSettingMenu("scanresultbar");
    }

    public String getSMenuSafeScan() {
        return getSettingMenu("safescan");
    }

    public String getSMenuTrackingBlocking() {
        if (settingMenu != null) {
            String[] trackerblocking = settingMenu.get("trackerblocking");
            if (trackerblocking != null && !TextUtils.isEmpty(trackerblocking[0])) {
                return trackerblocking[0];
//            return GlobalVariables.blockTracking ? trackerblocking[0] : trackerblocking[1];
            }
        }

        return "";
    }

    public String getSMenuAudibleAlert() {
        return getSettingMenu("audiblealert");
    }

    public String getSMenuDefaultSearch() {
        return getSettingMenu("defaultsearch");
    }

    private void setIsTrackerBlockingEnabled() {
        if (PlistHelper.getInstance().getParentPlist() != null) {
            PList blockPlist = PlistHelper.getInstance().getParentPlist().get("trackeroptions.blocktrackeroptions");
            if (blockPlist != null && blockPlist.get("enabled") != null) {
                String blockTrackerOption = blockPlist.getData("enabled");
                if (blockTrackerOption.equalsIgnoreCase("true")) {
                    GlobalVariables.blockTracking = true;
                    return;
                }
            }
        } else
            GlobalVariables.blockTracking = false;
    }

    private void setTipsStrings() {
        if (parentPlist != null && parentPlist.get("tips") != null) {
            Map<String, PList> tipsMap = parentPlist.get("tips").getMap();
            tipVPN = tipsMap.get("vpn").get("message." + def_lang).data;
            tipTab = tipsMap.get("tab").get("message." + def_lang).data;
            tipTracker = tipsMap.get("tracker").get("message." + def_lang).data;
            tipVS = tipsMap.get("scan").get("message." + def_lang).data;
            tipDrawer = tipsMap.get("menu").get("message." + def_lang).data;
            tipBookmark = tipsMap.get("bookmark").get("message." + def_lang).data;
            tipHome = tipsMap.get("home").get("message." + def_lang).data;
        }
    }

    public String getTipVPN() {
//        return "Turn on VPN to secure your data & hide your location";
        return getString(tipVPN);
    }

    public String getTipTab() {
        return getString(tipTab);
//        return "Add new tabs, switch tabs, create private tabs";
    }

    public String getTipHome() {
        return getString(tipHome);
//        return "Set the current webpage to Home. Once set, press this icon to quickly bring up the Home webpage.";
    }

    public String getTipBookmark() {
        return getString(tipBookmark);
//        return "Bookmark the current webpage";
    }

    public String getTipDrawer() {
        return getString(tipDrawer);
    }

    public String getTipVS() {
//        return "Display privacy rating of webpages. Are webpages you visit safe?";
        return getString(tipVS);
    }

    public String getTipTracker() {
        return getString(tipTracker);
    }

    private void setScannerDataFiler() {
        milicousValue = getDictLevelValue(PlistConstants.KEY_SCANNERDATAFILTER, PlistConstants.KEY_MALICIOUS_SITE + "." + def_lang, parentPlist);
        malwareValue = getDictLevelValue(PlistConstants.KEY_SCANNERDATAFILTER, PlistConstants.KEY_MALWARE_SITE + "." + def_lang, parentPlist);
    }

    public String getMalicousValue() {
        return TextUtils.isEmpty(milicousValue) ? "" : milicousValue;
    }

    public String getMalwareValue() {
        return TextUtils.isEmpty(malwareValue) ? "" : malwareValue;
    }



    public String getEmailValidationProvider() {
        return emailValidationProvider;
    }

    public String getEmailValidationOption() {
        return emailValidationOption;
    }

    public String getEmailValidationWarnMsg() {
        return emailValidationWarnMsg;
    }

    public String getEmailValidationErrMsg() {
        return emailValidationErrMsg;
    }

    public String getSocialEmailReq() {
        return socialEmailReq;
    }

    private void setAppStatics() {
        setAppUrl();
        setAppVersion();
        setAppUpdateMessage();
    }

    private void setAppVersion() {
        appVersion = getDictLevelValue(PlistConstants.KEY_APPVERSION, PlistConstants.KEY_ANDROID, parentPlist);
    }

    private void setAppUpdateMessage() {
        updateMessage = getDictLevelValue(PlistConstants.KEY_APPVERSION, PlistConstants.KEY_MESSAGE + "." + def_lang, parentPlist);
    }

    private void setSafeScanDisableMessage() {
        safeScanDisableMsg = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_SAFE_SCAN_DISABLE_TEXT + "." + def_lang, parentPlist);
    }

    private void setPrivacyPolicyUpdateText() {
        privacyPolicyUpdateText = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_PRIVACY_POLICY_UPDATE_TEXT + "." + def_lang, parentPlist);
    }

    public String getPrivacyPolicyUpdateText() {
        return getString(privacyPolicyUpdateText);
    }

    private void setEulaSkipMsgText() {
        eulaSkipMsgText = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_EULA_SKIP_MSG_TEXT + "." + def_lang, parentPlist);
    }

    public String getEulaSkipMsgText() {
        return getString(eulaSkipMsgText);
    }

    private void setTrackerDisableMessage() {
        trackerDisableMessage = getDictLevelValue(PlistConstants.KEY_TRACKEROPTIONS, PlistConstants.KEY_DISABLEMESSAGE + "." + def_lang, parentPlist);
    }

    private void setAppshareMessage() {
        registrationPopUpText = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_REGISTRATION_POPUP_TEXT + "." + def_lang, parentPlist);
    }

    private void setPrivacyMassage() {
        privacyMassage = getDictLevelValue(PlistConstants.KEY_REGISTRATION_ALERT, PlistConstants.KEY_REGISTRATION_MESSAGE + "." + def_lang, parentPlist);
    }

    private void setPrivacyHtmlMassage() {
        privacyHtmlMassage = getDictLevelValue(PlistConstants.KEY_REGISTRATION_ALERT, PlistConstants.KEY_REGISTRATION_HTML_MESSAGE + "." + def_lang, parentPlist);

    }

    private void setIgnoreButton() {
        ignoreButton = getDictLevelValue(PlistConstants.KEY_REGISTRATION_ALERT, PlistConstants.KEY_IGNORE_BUTTON + "." + def_lang, parentPlist);

    }

    private void setAcceptButton() {
        acceptButton = getDictLevelValue(PlistConstants.KEY_REGISTRATION_ALERT, PlistConstants.KEY_ACCEPT_BUTTON + "." + def_lang, parentPlist);

    }
    private void setisHtml() {
        isHtml = getDictLevelValue(PlistConstants.KEY_REGISTRATION_ALERT, PlistConstants.KEY_HTML , parentPlist).equals("true");

    }

    private void setVpndownloadtext() {
        vpndownloadtext = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_VPN_DOWNLOAD_TEXT + "." + def_lang, parentPlist);
    }

    private void setChooseVpnMsg() {

        chooseVpnMsg = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_VPN_MODE_ALERT_TEXT + "." + def_lang, parentPlist);

    }

    private void setChooseBrowerMsg() {
        chooseBrowerMsg = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_BROWSER_MODE_ALERT_TEXT + "." + def_lang, parentPlist);
    }

    public String getChooseVpnMsg() {

//        return "If you choose to use the secure browser, please turn it on in settings under the 3 dot menu";
        return getString(chooseVpnMsg);
    }

    public String getChooseBrowerMsg() {
//        return "While using the browser, you can use the VPN at any time to secure your connection & hide your location. You can access the VPN through the menu or the icon on the top-right next to the address bar. If you wish to turn off the browser and just use the VPN you can do so in settings. The VPN remains active both inside and outside of the app";
        return getString(chooseBrowerMsg);
    }

    private void setShouldShowPopUpMsg() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_VPN_TIP_COUNT) != null) {
            vpntipcount = parentPlist.get(PlistConstants.KEY_VPN_TIP_COUNT).data;
        }
        if (!UserPref.getInstance().getVpnScreenHelpMsgCountSaved()) {

            try {
                int count = Integer.parseInt(vpntipcount);
                UserPref.getInstance().setVpnHelpMsgCount(count);
                UserPref.getInstance().setVpnScreenHelpMsgCountSaved(true);
            } catch (NumberFormatException e) {
                UserPref.getInstance().setVpnHelpMsgCount(5);
            }
        }
    }

    public boolean getShouldShowPopMsg() {
        int count = UserPref.getInstance().getVpnHelpMsgCount();
        if (count > 0) {
            UserPref.getInstance().setVpnHelpMsgCount(count - 1);
            return true;
        }
        return false;
    }

    private void setVpnScreenPopUpText() {
        vpnScreenPopUpText = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_VPNSCREEN_POPUP_TEXT + "." + def_lang, parentPlist);
    }

    private void setVpnSharePopUpText() {
        vpnsharepopup = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_VPNSHARE_POPUP_TEXT + "." + def_lang, parentPlist);
    }

    private void setvpnsharetextText() {
        vpnsharetext = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, "vpnsharetext" + "." + def_lang, parentPlist);
    }

    private void setinvincibull_updateText() {
        invincibull_update = getDictLevelValue(PlistConstants.KEY_APPSTRINGS, PlistConstants.KEY_invincibull_update + "." + def_lang, parentPlist);
    }

    public String getVSScreenPopUpText() {
//        return jugad ? getVPNSCREENPOPUPTEXT() : getString(vpnScreenPopUpText);
        return "" + getString(vpnScreenPopUpText);
    }

    public String getVpnsharepopup() {
        String msg = "" + getString(vpnsharepopup);
        if (msg.contains("http://")) {
            msg = msg.replace("http://", getAppUrl());
        }
        return msg;
    }

    public String getvpnsharetext() {
        String msg = "" + getString(vpnsharetext);
        if (msg.contains("http://")) {
            msg = msg.replace("http://", getAppUrl());
        }
        return msg;
    }

    public String getInvincibull_update() {
        String msg = "" + getString(invincibull_update);
        if (msg.contains("http://")) {
            msg = msg.replace("http://", getAppUrl());
        }
        return msg;
    }

    public String getRegistrationPopUpText() {
//        return jugad ? getREGISTRATIONPOPUPTEXT() : getString(registrationPopUpText);
//
        return "" + getString(registrationPopUpText);
    }

    public String getVpndownloadtext() {
//        return jugad ? getVPNDOWNLOADTEXT() : getString(vpndownloadtext);
        return "" + getString(vpndownloadtext);
    }

    public String getTrackerDisableMessage() {
        return getString(trackerDisableMessage);
    }

    public String getSafeScanDisableMsg() {
        return getString(safeScanDisableMsg);
    }

    public void setDefaultTrackerStatus() {
        String trackerStatus = getDictLevelValue(PlistConstants.KEY_TRACKEROPTIONS, PlistConstants.KEY_DISABLEMESSAGE + "." + def_lang, parentPlist);
        defaultTrackerStatus = getBoolean(trackerStatus);
        if (!UserPref.getInstance().getTrackerStatusChanged()) {
            UserPref.getInstance().setTracker(defaultTrackerStatus);
        }
    }

    private String getString(String var) {
        if (!TextUtils.isEmpty(var)) {
            var = Uri.parse(var).toString();
            if (var.contains("\\n")) {
                var = realStringReplace(var, "\\n", "\n");
            }
            var = var.replaceAll("&amp;", "&");
            return var;
        }
        return "";
    }

    private boolean getBoolean(String var) {
        return (!TextUtils.isEmpty(var) && (var.equalsIgnoreCase("true") || var.equalsIgnoreCase("yes")));
    }

    public String getAppVersion() {
        return getString(appVersion);
    }

    public String getUpdateMessage() {
        return getString(updateMessage);
    }

    private void setParentLevelValues() {
        setHelpUrl();
        setAboutUrl();
        setMaxTabCount();
        setUserLocalDb();
        setRecommend();
        setBingKey();
    }

    private void setAppUrl() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_APP_URL + "." +
                PlistConstants.KEY_ANDROID) != null) {
            appUrl = parentPlist.get(PlistConstants.KEY_APP_URL + "." +
                    PlistConstants.KEY_ANDROID).data;
        }
    }

    public String getAppUrl() {
        return TextUtils.isEmpty(appUrl) ? "" : appUrl;
    }

    private String getParentLavelValue(String key, PList pList) {

        if (pList != null && pList.get(key) != null) {
            return pList.get(key).data;
        }
        return "";
    }

    private String getDictLevelValue(String dictKey, String key, PList parentPlist) {
        if (parentPlist != null && parentPlist.get(dictKey + "." + key) != null) {
            return parentPlist.get(dictKey + "." + key).data;
        }
        return "";
    }

    private PList getDictLevelPlist(String dictKey, String key, PList parentPlist) {
        if (parentPlist != null && parentPlist.get(dictKey + "." + key) != null) {
            return parentPlist.get(dictKey + "." + key);
        }
        return null;
    }

    private void setHelpUrl() {
        helpUrl = getParentLavelValue(PlistConstants.KEY_HELP, parentPlist);
    }

    private void setAboutUrl() {
        aboutUrl = getParentLavelValue(PlistConstants.KEY_ABOUT, parentPlist);
    }

    private void setMaxTabCount() {
        maxTabCount = getParentLavelValue(PlistConstants.KEY_MAXTABCOUNT, parentPlist);
    }

    private void setBingKey() {
        bingKey = getDictLevelValue(PlistConstants.KEY_SEARCHAPIKEY, PlistConstants.KEY_BING, parentPlist);
    }

    private void setUserLocalDb() {
        userLocalDb = getParentLavelValue(PlistConstants.KEY_USELOCALDB, parentPlist);
    }

    private void setRecommend() {
        recommend = getParentLavelValue(PlistConstants.KEY_RECOMMEND + "." + def_lang, parentPlist);
    }

    public String getHelpUrl() {
        return TextUtils.isEmpty(helpUrl) ? "" : helpUrl;
    }

    public String getPasswordResetUrl() {
        return TextUtils.isEmpty(passwordResetUrl) ?
                FinjanUrls.URL_PASSWORD_RESET : passwordResetUrl;
    }

    public String getEulaUrl() {
        return TextUtils.isEmpty(eula_url) ?
                FinjanUrls.URL_EULA : eula_url;
    }

    public String getPrivacyStatementLink() {
        return TextUtils.isEmpty(privacyStatementLink) ?
                FinjanUrls.URL_Privacy_statement : privacyStatementLink;
    }

    public String getAboutUrl() {
        return TextUtils.isEmpty(aboutUrl) ? "" : aboutUrl;
    }

    public String getMaxTabCount() {
        return TextUtils.isEmpty(maxTabCount) ? "" : maxTabCount;
    }

    public String getBingKey() {
        return TextUtils.isEmpty(bingKey) ? "" : bingKey;
    }

    public String getUserLocalDb() {
        return TextUtils.isEmpty(userLocalDb) ? "" : userLocalDb;
    }

    public String getRecommend() {
        return TextUtils.isEmpty(recommend) ? "" : recommend;
    }

    //Tracker part
    private void setTrackerPart() {
        setIsTrackerEnabled();
        setTrackerFileLocation();
        setIsTrackerBlockingEnabled();
    }

    private void setIsTrackerEnabled() {
        if (!TextUtils.isEmpty(getParentLavelValue(PlistConstants.KEY_ENABLED, trackerOptions))) {
            String value = getParentLavelValue(PlistConstants.KEY_ENABLED, trackerOptions);
            isTrackerEnabled = (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("yes"));
        }
    }

    private void setTrackerFileLocation() {
        if (!TextUtils.isEmpty(getParentLavelValue(PlistConstants.KEY_FILELOCATION, trackerOptions))) {
            trackerFileLocation = getParentLavelValue(PlistConstants.KEY_FILELOCATION, trackerOptions);
        }
    }

    public String getTrackerFileLocation() {
        return TextUtils.isEmpty(trackerFileLocation) ? "" : trackerFileLocation;
    }

    public boolean getIsTrackerEnabled() {
        return isTrackerEnabled;
//        return false;
    }

    private void setRattingOption() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_RATINGOPTIONS) != null) {
            PList rattingPlist = parentPlist.get(PlistConstants.KEY_RATINGOPTIONS);
            RattingOptionPlistModel rattingOptionPlistModel = new RattingOptionPlistModel();
            rattingOptionPlistModel.setAndroid_package(getParentLavelValue(PlistConstants.KEY_ANDROID_PACKAGE, rattingPlist));
            rattingOptionPlistModel.setDays_until_prompt(getParentLavelValue(PlistConstants.KEY_DAYS_UNTIL_PROMPT, rattingPlist));
            rattingOptionPlistModel.setMessage(getParentLavelValue(PlistConstants.KEY_MESSAGE + "." + def_lang, rattingPlist));
            rattingOptionPlistModel.setTime_before_reminding(getParentLavelValue(PlistConstants.KEY_TIME_BEFORE_REMINDING, rattingPlist));
            rattingOptionPlistModel.setUses_until_prompt(getParentLavelValue(PlistConstants.KEY_USES_UNTIL_PROMPT, rattingPlist));
            this.rattingOptionPlistModel = rattingOptionPlistModel;
        }
        setRateUsModel();
    }

    public void setRateUsModel() {
//        PList plist = ModelManager.getInstance().getPList();
//        if (plist != null) {
        RateUsModel rateModel = new RateUsModel();
        RattingOptionPlistModel rattingOptionPlistModel = PlistHelper.getInstance().getRattingOptionPlistModel();
        rateModel.setMessage(rattingOptionPlistModel.getMessage());
//            rateModel.setDaysUntillPromt(0);
        rateModel.setDaysUntillPromt(rattingOptionPlistModel.getDays_until_prompt());
        rateModel.setPackageName(rattingOptionPlistModel.getAndroid_package());
//            rateModel.setUsagesUntillPromt(1);
        rateModel.setUsagesUntillPromt(rattingOptionPlistModel.getUses_until_prompt());
        rateModel.setTimeBeforeReminding(rattingOptionPlistModel.getTime_before_reminding());
        ModelManager.getInstance().setRateUsModel(rateModel);
//        }
    }

    public RattingOptionPlistModel getRattingOptionPlistModel() {
        return rattingOptionPlistModel == null ? new RattingOptionPlistModel() : rattingOptionPlistModel;
    }

    private void setGoogleEnalyticKey() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_GOOGLEANALYTICS + "." +
                PlistConstants.KEY_ANDROID) != null) {
            googleEnalyticKey = parentPlist.get(PlistConstants.KEY_GOOGLEANALYTICS + "." +
                    PlistConstants.KEY_ANDROID).data;
        }
    }

    private void setLocalyticsKey() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_LOCALYTICS) != null) {
            if (parentPlist.get(PlistConstants.KEY_LOCALYTICS + "." +
                    PlistConstants.KEY_VICEANIMATIONCOUNT) != null) {
                vinceAnimationCount = parentPlist.get(PlistConstants.KEY_LOCALYTICS + "." +
                        PlistConstants.KEY_VICEANIMATIONCOUNT).data;
            }
            if (parentPlist.get(PlistConstants.KEY_LOCALYTICS + "." +
                    PlistConstants.KEY_LOCATIONCHANGE) != null) {
                showFreeUserLocationChangeEventNotification = parentPlist.get(PlistConstants.KEY_LOCALYTICS + "." +
                        PlistConstants.KEY_LOCATIONCHANGE).array.get(0).data;
            }
        }
    }


    public int getVinceAnimationCount() {
        String count = TextUtils.isEmpty(vinceAnimationCount) ? "1" : vinceAnimationCount;
        return Integer.parseInt(count);
    }

    public boolean getShowFreeUserLocationChangePopup() {
        return !TextUtils.isEmpty(showFreeUserLocationChangeEventNotification) && showFreeUserLocationChangeEventNotification.equals("true");
    }

    public String getGoogleEnalyticKey() {
        return TextUtils.isEmpty(googleEnalyticKey) ? "" : googleEnalyticKey;
    }

    private boolean singleLine = false;
    private boolean showActual = false;
    private void setInAppPurchaseModel() {
        if (parentPlist != null && parentPlist.get(/*"android." +*/ PlistConstants.KEY_INAPPPURCHASES) != null) {
            PList inAppPurchasePlist = parentPlist.get(/*"android." +*/ PlistConstants.KEY_INAPPPURCHASES);
//            PList buttonOption = parentPlist.get("android." + PlistConstants.KEY_INAPPPURCHASES);
            if (inAppPurchasePlist.get(PlistConstants.KEY_BUTTON_OPTIONS)!=null && inAppPurchasePlist.get(PlistConstants.KEY_BUTTON_OPTIONS).get(PlistConstants.KEY_SINGLE_LINE) != null && inAppPurchasePlist.get(PlistConstants.KEY_BUTTON_OPTIONS).getData(PlistConstants.KEY_SINGLE_LINE).equalsIgnoreCase("true"))
            {
                singleLine = true;
            }
            if (inAppPurchasePlist.get(PlistConstants.KEY_BUTTON_OPTIONS)!=null && inAppPurchasePlist.get(PlistConstants.KEY_BUTTON_OPTIONS).get(PlistConstants.KEY_SHOW_ACTUAL) != null && inAppPurchasePlist.get(PlistConstants.KEY_BUTTON_OPTIONS).getData(PlistConstants.KEY_SHOW_ACTUAL).equalsIgnoreCase("true"))
            {
                showActual = true;
            }
            if (inAppPurchase == null) {
                inAppPurchase = new InAppPurchase();
                if (inAppPurchasePlist.get(PlistConstants.KEY_ENABLED) != null) {

                    if (!TextUtils.isEmpty(inAppPurchasePlist.getData(PlistConstants.KEY_ENABLED)) &&
                            (inAppPurchasePlist.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("true") ||
                                    inAppPurchasePlist.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("yes"))) {
                        inAppPurchase.setEnabled(true);
                    } else {
                        inAppPurchase.setEnabled(false);
                    }
                }
                if (inAppPurchasePlist.get(PlistConstants.KEY_FREEPRIVACYSCAN) != null) {
                    inAppPurchase.setFreeprivacyscan(Integer.parseInt(inAppPurchasePlist.getData(PlistConstants.KEY_FREEPRIVACYSCAN)));
                }
                if (inAppPurchasePlist.get(PlistConstants.KEY_CREDITSUPDATEURL) != null) {
                    inAppPurchase.setCreditsupdateurl((inAppPurchasePlist.getData(PlistConstants.KEY_CREDITSUPDATEURL)));
                }
//                if (inAppPurchasePlist.get(PlistConstants.KEY_SANDBOXTESTING) != null) {
//
//                    if (!TextUtils.isEmpty(inAppPurchasePlist.getData(PlistConstants.KEY_SANDBOXTESTING)) &&
//                            (inAppPurchasePlist.getData(PlistConstants.KEY_SANDBOXTESTING).equalsIgnoreCase("true") ||
//                                    inAppPurchasePlist.getData(PlistConstants.KEY_SANDBOXTESTING).equalsIgnoreCase("yes"))) {
//                        inAppPurchase.setSandboxtesting(true);
//                    } else {
//                        inAppPurchase.setSandboxtesting(false);
//                    }
//                }
                if (inAppPurchasePlist.get(PlistConstants.KEY_NOTIFICATIONINTERVAL) != null) {
                    inAppPurchase.setNotificationinterval(Integer.parseInt(inAppPurchasePlist.
                            getData(PlistConstants.KEY_NOTIFICATIONINTERVAL)));
                }
                if (inAppPurchasePlist.get(PlistConstants.KEY_PRODUCTS) != null) {
                    ArrayList<PList> productpList = inAppPurchasePlist.get(PlistConstants.KEY_PRODUCTS).array;
                    ArrayList<String> productList = new ArrayList<>();
                    for (PList pList : productpList) {
                        productList.add(pList.data);
                    }
                    inAppPurchase.setProducts(productList);
                }
            }
        }
    }

    public InAppPurchase getInAppPurchase() {
        return inAppPurchase == null ? new InAppPurchase() : inAppPurchase;
    }

    private void setAppStrings() {
        appStrings = parentPlist.get(PlistConstants.KEY_APPSTRINGS);
    }

    private void setPlists() {
        if (parentPlist != null) {
            setAppStrings();
            setTrackerOptions();
        }

    }

    private void setTrackerOptions() {
        trackerOptions = parentPlist.get(PlistConstants.KEY_TRACKEROPTIONS);
    }

    private void setHomePage() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_DEFAULT_HOMEPAGE) != null) {
            homePage = parentPlist.getData(PlistConstants.KEY_DEFAULT_HOMEPAGE);
        }
    }

    private void setWhiteList() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_TRACKEROPTIONS + "." + PlistConstants.KEY_WHITELIST) != null) {
            ArrayList<PList> whitelist = parentPlist.get(PlistConstants.KEY_TRACKEROPTIONS + "." + PlistConstants.KEY_WHITELIST).array;
            for (PList pList : whitelist) {
                if (pList != null) {
                    String[] array = pList.data.split("/");
                    String[] subCatArray = array[1].split(",");
                    ArrayList<String> subCatArrayList = new ArrayList<>();
                    for (String subCat : subCatArray) {
                        subCatArrayList.add(subCat);
                    }
                    whiteListMap.put(array[0], subCatArrayList);
//                    categoryWhiteList.add(pList.data);
                }
            }
        }
//        if(categoryWhiteList!=null && categoryWhiteList.size()>0 && categoryWhiteList.contains("Content")){
//            categoryWhiteList.add("Disconnect");
//        }
    }

    private void setSearchEngineType() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_DEFAULT_SEARCH_ENGINE) != null) {
            int type = NativeHelper.getInstnace().getSearchEngineType(parentPlist.get(PlistConstants.KEY_DEFAULT_SEARCH_ENGINE).data);
            if (!UserPref.getInstance().getScanTypeChanged()) {
                UserPref.getInstance().setScanType(type);
            }
        }
    }

    public HashMap<String, ArrayList<String>> getCategoryWhiteList() {

        return whiteListMap;
//        return categoryWhiteList;
    }

    public String getHomePage() {
        return homePage == null ? "" : homePage;
    }

    public HashMap<REPORT_TYPE, ReportCard> getReportCard() {
        if (reportCardHashMap == null || reportCardHashMap.size() == 0) {
            reportCardHashMap = new HashMap<>();
            if (appStrings != null && appStrings.get(PlistConstants.KEY_REPORTCARD) != null) {
                Map<String, PList> reportMap = appStrings.get(PlistConstants.KEY_REPORTCARD).getMap();
                for (String key : reportMap.keySet()) {
                    PList pList = reportMap.get(key);
                    ReportCard reportCard = new ReportCard(key, pList.getData(PlistConstants.KEY_HEADING + "." + def_lang),
                            pList.getData(PlistConstants.KEY_DESCRIPTION + "." + def_lang));
                    switch (key) {
                        case "reportdanger":
                            reportCardHashMap.put(REPORT_TYPE.DANGER, reportCard);
                            break;
                        case "reportsuspicious":
                            reportCardHashMap.put(REPORT_TYPE.SUSPICIOUS, reportCard);
                            break;
                        case "reportsafe":
                            reportCardHashMap.put(REPORT_TYPE.SAFE, reportCard);
                            break;
                    }
                }
            }
        }
        return reportCardHashMap;
    }

    private void setAnalyticalTrackers() {
        if (parentPlist != null && parentPlist.get(PlistConstants.KEY_ANALYTICS_TRACKERS) != null) {

            PList trackers = parentPlist.get(PlistConstants.KEY_ANALYTICS_TRACKERS);
            PList kochava = trackers.get(PlistConstants.KEY_KOCHAVA);
            PList fb = trackers.get(PlistConstants.KEY_FACEBOOK);
            PList tune = trackers.get(PlistConstants.KEY_TUNE);
            PList mixpanel = trackers.get(PlistConstants.KEY_MIXPANEL);
            if (kochava != null && kochava.get(PlistConstants.KEY_ENABLED) != null) {
                boolean enabled = kochava.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("true") ||
                        kochava.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("yes");
                KochavaConfig.plistConfig(enabled, kochava.getData(PlistConstants.KEY_KOCHAVA_APP_ID));
                KochavaConfig.getInstance().initKochavaTracking(FinjanVPNApplication.getInstance());
            }
            if (fb != null && fb.get(PlistConstants.KEY_ENABLED) != null) {
                boolean enabled = fb.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("true") ||
                        fb.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("yes");
                FbTrackingConfig.plistConfig(enabled, fb.getData(PlistConstants.KEY_APPID));
                FbTrackingConfig.getInstance().init(FinjanVPNApplication.getInstance());
            }
            if (tune != null && tune.get(PlistConstants.KEY_ENABLED) != null) {
                boolean enabled = tune.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("true") ||
                        tune.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("yes");
                TuneTrackingConfig.plistConfig(enabled, tune.getData(PlistConstants.KEY_ADVERTISER_ID),
                        tune.getData(PlistConstants.KEY_CONVERSION_KEY));
                TuneTrackingConfig.getInstance().initTuneTracking(FinjanVPNApplication.getInstance());

                if (!UserPref.getInstance().getAppInstalled()) {
                    UserPref.getInstance().setAppInstalled(true);
                    TuneTracking.getInstance().trackAppInstalled();
                }
            }
//            if (mixpanel != null && mixpanel.get(PlistConstants.KEY_ENABLED) != null) {
//                boolean enabled = mixpanel.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("true") ||
//                        mixpanel.getData(PlistConstants.KEY_ENABLED).equalsIgnoreCase("yes");
//                MixpanelTrackingConfig.plistConfig(enabled, mixpanel.getData(PlistConstants.KEY_TOKEN));
//                TrackingManager.initialize(FinjanVPNApplication.getInstance(), MixpanelTrackingConfig.getInstance());
//            }
        }
    }

    private void setInAppSettings() {
        if (appStrings != null && appStrings.get(PlistConstants.KEY_INAPPVIEWSTRINGS) != null) {
            PList inAppPlist = appStrings.get(PlistConstants.KEY_INAPPVIEWSTRINGS);
            inAppTitle = inAppPlist.getData(PlistConstants.KEY_HEADERTEXT + "." + def_lang);
            if (inAppPlist.get(PlistConstants.KEY_BULLETTEXT) != null) {
                inAppButtetPoints = new ArrayList<>();
                for (PList pList : inAppPlist.get(PlistConstants.KEY_BULLETTEXT).array) {
                    inAppButtetPoints.add(pList.get(def_lang).data);
                }
            }
        }
    }

    public ArrayList<String> getInAppButtetPoints() {

        return (inAppButtetPoints == null || inAppButtetPoints.size() == 0) ? new ArrayList<String>() : inAppButtetPoints;
    }

    public String getInAppTitle() {
        return TextUtils.isEmpty(inAppTitle) ? "" : inAppTitle;
    }

    public PList getParentPlist() {
        return parentPlist;
    }

    public void setParentPlist(PList parentPlist) {
        this.parentPlist = parentPlist;
        isPlistAvailable = true;
        setDefaults();
    }

    public static void setUpdatePlist() {
        if (!isPlistAvailable) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String response = PlistConfig.getConfigFromLocal(true);
                            PListParser pListParser = new PListParser();
                            pListParser.parse(new ByteArrayInputStream(response.getBytes()));
                            final PList pList = pListParser.current_element;
                            if (pList != null) {
                                ModelManager.getInstance().setPList(pList);
                            }
                            PlistHelper.getInstance().setParentPlist(pList);
                            isPlistAvailable = true;

                            Thread.sleep(500);
                            String response1 = PlistConfig.getConfigFromLocal(false);
                            ModelManager.getInstance().setTrackerModel(response1);

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }).start();

        }
    }

    static boolean isPlistAvailable = false;

    public boolean isSafeScanUpgradeEnable() {
        return PlistHelper.getInstance().getInAppPurchase().isEnabled();
//        return Utils.getPlistvalue(Constants.KEY_APP_PURCHASE, Constants.KEY_APP_PURCHASE_ENABLE).equalsIgnoreCase("true");
    }

    private ArrayList<String> getFactoryArray(String key) {
        if (factoryArray == null || factoryArray.size() == 0) {
            factoryArray = new ArrayList<>();
            if (appStrings != null && appStrings.get(key) != null) {
                ArrayList<PList> factorList = appStrings.get(key).array;
                for (PList facotsPlist : factorList) {
                    factoryArray.add(facotsPlist.get(def_lang).data);
                }
            }
        }
        return factoryArray;
    }

    public ArrayList<String> getFactoryArray(FACTOR_TYPE type) {
        if (type == FACTOR_TYPE.TYPE_FACTOR_ID_NEW) {
            return getFactoryArray(PlistConstants.KEY_FACTOIDS);
        }
        return getFactoryArray(PlistConstants.KEY_FACTOIDS_OLD);
    }

    public String getVpnLocationChangeAlert() {
        return getString(vpnLocationChangeAlert);
    }

    public String getVpnLocationChangeAlertTitle() {
        return getString(vpnLocationChangeAlertTitle);
    }

    public String getUpgradeDefaultAlert() {
        return getString(upgradeDefaultAlert);
    }

    public String getUpgradeAlertAflterLogin() {
        return getString(upgradeAlertAflterLogin);
    }

    public String getUpgradeAlertAflterLoginTitle() {
        return getString(upgradeDefaultAlertTitle);
    }

    private void setAlerts() {
        if (parentPlist != null) {
            if (parentPlist.get(PlistConstants.KEY_APPSTRINGS + "." + PlistConstants.KEY_IAPALERTS) != null) {
                PList alert = parentPlist.get(PlistConstants.KEY_APPSTRINGS + "." + PlistConstants.KEY_IAPALERTS);

                vpnLocationChangeAlert = alert.getData(PlistConstants.KEY_LOCATIONCHANGEDISABLED + "." + PlistConstants.KEY_MESSAGE + "." + def_lang) + "";
                vpnLocationChangeAlertTitle = alert.getData(PlistConstants.KEY_LOCATIONCHANGEDISABLED + "." + PlistConstants.KEY_TITLE + "." + def_lang) + "";
                upgradeDefaultAlert = alert.getData(PlistConstants.KEY_FREETRIAL + "." + PlistConstants.KEY_MESSAGE + "." + def_lang) + "";
                upgradeDefaultAlertTitle = alert.getData(PlistConstants.KEY_FREETRIAL + "." + PlistConstants.KEY_TITLE + "." + def_lang) + "";
                interestedbutton = alert.getData(PlistConstants.KEY_INTERESTEDBUTTON + "." + PlistConstants.KEY_TITLE + "." + def_lang) + "";
                laterbutton = alert.getData(PlistConstants.KEY_LATERBUTTON + "." + PlistConstants.KEY_TITLE + "." + def_lang) + "";
                okbutton = alert.getData(PlistConstants.KEY_OKBUTTON + "." + PlistConstants.KEY_TITLE + "." + def_lang) + "";
                footertext = alert.getData(PlistConstants.KEY_CAN_TRIAL_MSG + "." + PlistConstants.KEY_TITLE + "." + def_lang) + "";

                vpnLocationChangeAlertCBtn = laterbutton;
                vpnLocationChangeAlertOkBtn = interestedbutton;

//            upgradeDefaultAlert=alert.getData(PlistConstants.KEY_msg);
                upgradeDefaultAlertCBtn = "";
                upgradeDefaultAlertOkBtn = okbutton;

//            alert=parentPlist.get(PlistConstants.KEY_IAPALERTS).get(PlistConstants.KEY_UPGRADEALERTAFLTERLOGIN);
                upgradeAlertAflterLogin = upgradeDefaultAlert;
                upgradeAlertAflterLoginCBtn = laterbutton;
                upgradeAlertAflterLoginOkBtn = interestedbutton;
            }
            PList cssPlist = parentPlist.get(PlistConstants.KEY_CSS);
            if (cssPlist != null) {
                IAPInterestedButtonBg = cssPlist.getData("IAPInterestedButton.backgroundColor") == null ? "255,157,30" : cssPlist.getData("IAPInterestedButton.backgroundColor");
                IAPLaterButtonBg = cssPlist.getData("IAPLaterButton.backgroundColor") == null ? "30,128,240" : cssPlist.getData("IAPLaterButton.backgroundColor");
                IAPOKButtonBg = cssPlist.getData("IAPOKButton.backgroundColor") == null ? "255,157,30" : cssPlist.getData("IAPOKButton.backgroundColor");
                CLCancelBtn = cssPlist.getData("CLCancelBtn.backgroundColor") == null ? "30,128,240" : cssPlist.getData("CLCancelBtn.backgroundColor");
                CLOkButton = cssPlist.getData("CLOkBtn.backgroundColor") == null ? "255,157,30" : cssPlist.getData("CLOkBtn.backgroundColor");
            }
        }
    }
    private static final String KEY_Notification_Action =  "out_of_data_notification";
    private void setOutOfDataNotificationArrayList(@NonNull PList parentPlist) {
        if (parentPlist.get(KEY_Notification_Action) != null) {
            outOfDataNotificationArrayList = new ArrayList<>();
            for (PList notification : parentPlist.get(KEY_Notification_Action).array) {
                OutOfDataNotification outnotification = new OutOfDataNotification(notification);
                outOfDataNotificationArrayList.add(outnotification);
            }
        }
        OutOfDataNotificationModel outOfDataNotificationModel = new OutOfDataNotificationModel();
        outOfDataNotificationModel.setOutOfDataNotifications(outOfDataNotificationArrayList);
        UserPref.getInstance().setNotificatioJson(JsonUtils.convertModelToJson(outOfDataNotificationModel));


        if (outOfDataNotificationArrayList!=null && outOfDataNotificationArrayList.size()>0)
        {
            putTheArrayInSP(outOfDataNotificationArrayList);
        }



    }

    private void putTheArrayInSP(ArrayList<OutOfDataNotification> outOfDataNotificationArrayList)
    {
        arrayOfnotificationPersentage = new Integer[outOfDataNotificationArrayList.size()];
        for (int i =0;i<outOfDataNotificationArrayList.size();i++)
        {
            arrayOfnotificationPersentage[i] = outOfDataNotificationArrayList.get(i).getPercent_remaining();
        }
        Arrays.sort(arrayOfnotificationPersentage);

        UserPref.getInstance().setNotificatioPersentageJson(JsonUtils.convertModelToJson(arrayOfnotificationPersentage));

    }
    private void setVpnMenuDrawer(@NonNull PList parentPlist) {
        if (parentPlist.get("menus") != null) {
            vpnMenuMap = new HashMap<>();
            for (PList menu : parentPlist.get("menus").array) {
                VpnMenu vpnMenu = new VpnMenu(menu);
                vpnMenuMap.put(vpnMenu.key, vpnMenu);
            }
        }
        if (/*AppConfig.Companion.getDebugReporting()*/isTestmode()) {
            VpnMenu vpnMenu = new VpnMenu();
            vpnMenu.setName("Submit Debug Report");
            vpnMenu.setKey("submit_debug_report");
            vpnMenu.setIcon("submit_debug_report");
            vpnMenu.setMo_itendifier(106);
            vpnMenu.setOrder(106);
            vpnMenuMap.put(vpnMenu.key, vpnMenu);

//
            VpnMenu makeTokenExpire = new VpnMenu();
            makeTokenExpire.setName("Make your token expire");
            makeTokenExpire.setKey("make_you_token_expire");
            makeTokenExpire.setIcon("make_you_token_expire");
            makeTokenExpire.setMo_itendifier(107);
            makeTokenExpire.setOrder(107);
            vpnMenuMap.put(makeTokenExpire.key, makeTokenExpire);

            VpnMenu makeTokenTimeExpire = new VpnMenu();
            makeTokenTimeExpire.setName("Make your token time expire");
            makeTokenTimeExpire.setKey("make_you_time_expire");
            makeTokenTimeExpire.setIcon("make_you_time_expire");
            makeTokenTimeExpire.setMo_itendifier(108);
            makeTokenTimeExpire.setOrder(108);
            vpnMenuMap.put(makeTokenTimeExpire.key, makeTokenTimeExpire);

            VpnMenu cancel_all_promos = new VpnMenu();
            cancel_all_promos.setName("Cancel all Promos");
            cancel_all_promos.setKey("cancel_all_promos");
            cancel_all_promos.setIcon("make_you_time_expire");
            cancel_all_promos.setMo_itendifier(109);
            cancel_all_promos.setOrder(109);
            cancel_all_promos.setHide(0);
            vpnMenuMap.put(cancel_all_promos.key, cancel_all_promos);

           /* VpnMenu testing_Location_promo = new VpnMenu();
            testing_Location_promo.setName("Testing Location Promo (softcoded)");
            testing_Location_promo.setKey("testing_Location_promo");
            testing_Location_promo.setIcon("make_you_time_expire");
            testing_Location_promo.setMo_itendifier(110);
            testing_Location_promo.setOrder(110);
            testing_Location_promo.setHide(0);
            vpnMenuMap.put(testing_Location_promo.key,testing_Location_promo);*/

        }


        addMoIdentifier(vpnMenuMap);
    }

    private void setDataGaugeMultiplier(@NonNull PList parentPlist) {
        if (parentPlist.get(PlistConstants.KEY_DataGaugeMultiplier) != null) {
            dataGaugeMultiplier = Integer.parseInt(parentPlist.getData(PlistConstants.KEY_DataGaugeMultiplier));
            dataGaugeMultiplier = dataGaugeMultiplier * AppConfig.Companion.getDataMultiplier();
        }
    }

    private void setIfRandomNoGenerate() {
        if (parentPlist != null && parentPlist.get("substitutedeviceids.generateRandom") != null) {
            String data = (parentPlist.get("substitutedeviceids.generateRandom")).data;
            if (data.equals("true")) {
                shouldGenerateRandomId = true;
            }
        }
    }

    public void setShouldShowLocalNotificationForTraffic() {
        if (parentPlist != null && parentPlist.get("ShouldShowLocalNotificationForTraffic") != null) {
            String data = (parentPlist.get("ShouldShowLocalNotificationForTraffic").data);
            if (data.equals("true")) {
                ShouldShowLocalNotificationForTraffic = true;
            }
        }
    }

    public boolean shouldShowLocalNotificationForTraffic() {
        return ShouldShowLocalNotificationForTraffic;
    }


    private String defaultAutoConnect;

    private String getDefaultAutoConnect() {
        return defaultAutoConnect;
    }

    public boolean isShouldGenerateRandomId() {
        return shouldGenerateRandomId;
    }

    public int getDataGaugeMultiplier() {
        if (dataGaugeMultiplier == 0) {
            dataGaugeMultiplier = 1;
        }
        return dataGaugeMultiplier;
//        return 200;
    }

    public HashMap<String, VpnMenu> getVpnMenuMap() {
        return vpnMenuMap;
    }

    private void addMoIdentifier(HashMap<String, VpnMenu> vpnMenulist) {
        for (String vpnMenu : vpnMenulist.keySet()) {
            switch (vpnMenu) {
                case "logout":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_logout;
                    break;
                case "watch":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menue_watch;
                    break;
                case "upgrade":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_upgrade;
                    break;
                case "browser":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_secure_browser;
                    break;
                case "share":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_share;
                    break;
                case "rate":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_rate;
                    break;
                case "about":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_about;
                    break;
                case "help":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_help;
                    break;
                case "redeem_offer":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_redeem_offer;
                    break;
                case "findmyip":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_my_ip;
                    break;
                case "settings":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_settings;
                    break;
                case "forgot_password":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_change_pwd;
                    break;
                case "vs-desktop":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_vs_desktop;
                    break;
                case "auto_connect":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_auto_connect;
                    break;
                case "trustednetworks":
                    vpnMenulist.get(vpnMenu).mo_itendifier = VPNDrawer.MoIdentifier.menu_trustednetworks;
                    break;
            }
        }
    }

    public String getUpgrade_discount() {
        return upgrade_discount;
    }

    public boolean isEnable_redeem() {
        return enable_redeem;
    }

    public boolean isSingleLine() {
        return singleLine;
    }

    public boolean isShowActual() {
        return showActual;
    }

    public String getNotification_action_msg() {
        return notification_action_msg;
    }

    public String getNotification_action_diplink_url() {
        return notification_action_diplink_url;
    }

    public enum FACTOR_TYPE {
        TYPE_FACTOR_ID_NEW, TYPE_FACTOR_ID_OLD
    }

    public enum REPORT_TYPE {
        DANGER, SUSPICIOUS, SAFE
    }

    private interface PlistConstants {
        String KEY_DEFAULT_HOMEPAGE = "defaulthomepage";
        String KEY_TRACKEROPTIONS = "trackeroptions";
        String KEY_BLOCK_TRACKER_OPTIONS = "blocktrackeroptions";
        String KEY_ENABLED = "enabled";
        String KEY_BUTTON_OPTIONS = "button_options";
        String KEY_SINGLE_LINE = "single_line";
        String KEY_SHOW_ACTUAL = "show_actual";
        String KEY_WHITELIST = "whitelist";
        String KEY_EULA_LINK = "eula_link";
        //        String KEY_AUTOCONNECT = "autoconnect";
        String KEY_OnDemand = "OnDemand";
        String KEY_UPDATED_AT = "updated_at";
        String KEY_PASSWORD_RESET_LINK = "password_reset";
        String KEY_PRIVACY = "privacy";
        String KEY_URL = "url";
        String KEY_DEFAULT_SEARCH_ENGINE = "defaultsearchengine";
        String KEY_APPSTRINGS = "appstrings";
        String KEY_CSS = "css";
        String KEY_FACTOIDS = "factoids";
        String KEY_FACTOIDS_OLD = "factoids-old";
        String KEY_REPORTCARD = "reportcard";
        String KEY_SCANNERDATAFILTER = "scannerdatafilter";
        String KEY_INAPPVIEWSTRINGS = "inappviewstrings";
        String KEY_HEADING = "heading";
        String KEY_DESCRIPTION = "description";
        String KEY_HEADERTEXT = "headertext";
        String KEY_BULLETTEXT = "bullettext";
        String KEY_INAPPPURCHASES = "inapppurchases";
        String KEY_FREEPRIVACYSCAN = "freeprivacyscan";
        String KEY_CREDITSUPDATEURL = "creditsupdateurl";
        String KEY_SANDBOXTESTING = "sandboxtesting";
        String KEY_NOTIFICATIONINTERVAL = "notificationinterval-days";
        String KEY_PRODUCTS = "products_old_android";
        String KEY_GOOGLEANALYTICS = "googleanalytics";
        String KEY_LOCALYTICS = "localytics";
        String KEY_LOCATIONCHANGE = "locationChange";
        String KEY_showFreeUserLocationChangeLocalyticsEventNotification = "showFreeUserLocationChangeLocalyticsEventNotification";
        String KEY_VICEANIMATIONCOUNT = "viceAnimationCount";
        String KEY_ANDROID = "android";
        String KEY_APP_URL = "appurl";
        String KEY_ABOUT = "about";
        String KEY_HELP = "help";
        String KEY_MAXTABCOUNT = "maxtabcount";
        String KEY_SEARCHAPIKEY = "searchapikey";
        String KEY_BING = "bing";
        String KEY_USELOCALDB = "uselocalDB";
        String KEY_RECOMMEND = "recommend";
        String KEY_FILELOCATION = "filelocation";
        String KEY_RATINGOPTIONS = "ratingoptions";
        String KEY_ANDROID_PACKAGE = "android_package";
        String KEY_DAYS_UNTIL_PROMPT = "days_until_prompt";
        String KEY_MESSAGE = "message";
        String KEY_DISABLEMESSAGE = "disablemessage";
        String KEY_SAFE_SCAN_DISABLE_TEXT = "safescandisabletext";
        String KEY_PRIVACY_POLICY_UPDATE_TEXT = "privacypolicyupdatetext";
        String KEY_LOCATION_CHANGE_ALERT = "locationchangealert";
        String KEY_EULA_SKIP_MSG_TEXT = "eulaskipmsgtext";
        String KEY_REGISTRATION_POPUP_TEXT = "registrationpopuptext";
        String KEY_VPNSCREEN_POPUP_TEXT = "vpnscreenpopuptext";
        String KEY_VPNSHARE_POPUP_TEXT = "vpnsharepopup";
        String KEY_invincibull_update = "invincibull_update";
        String KEY_VPN_DOWNLOAD_TEXT = "vpndownloadtext";
        String KEY_VPN_TIP_COUNT = "vpntipcount";
        String KEY_VPN_MODE_ALERT_TEXT = "vpnmodealerttext";
        String KEY_BROWSER_MODE_ALERT_TEXT = "browsermodealerttext";
        String KEY_TIME_BEFORE_REMINDING = "time_before_reminding";
        String KEY_USES_UNTIL_PROMPT = "uses_until_prompt";
        String KEY_APPVERSION = "appversion";
        String KEY_MALICIOUS_SITE = "malicious site";
        String KEY_MALWARE_SITE = "malware site";
        String KEY_SETTINGS_OPTIONS = "settingsoptions";
        String KEY_Menus = "menus";
        String KEY_Tutorialscreens = "tutorialscreens";
        //trackers keys
        String KEY_ANALYTICS_TRACKERS = "analytics_trackers";
        String KEY_KOCHAVA = "kochava";
        String KEY_KOCHAVA_APP_ID = "kochava_app_id";
        String KEY_APPID = "appid";
        String KEY_TUNE = "tune";
        String KEY_ADVERTISER_ID = "advertiserid";
        String KEY_CONVERSION_KEY = "conversionkey";
        String KEY_FACEBOOK = "facebook";
        String KEY_MIXPANEL = "mixpanel";
        String KEY_TOKEN = "token";

        String KEY_IAPALERTS = "iapalerts";
        String KEY_LOCATIONCHANGEDISABLED = "locationchangedisabled";
        String KEY_msg = "msg";
        String KEY_NEGATIVEBTN = "negativeBtn";
        String KEY_LATERBUTTON = "laterbutton";
        String KEY_POSITIVEBTN = "positiveBtn";
        String KEY_INTERESTEDBUTTON = "interestedbutton";
        String KEY_FREETRIAL = "freetrial";
        String KEY_TITLE = "title";
        String KEY_OKBUTTON = "okbutton";
        String KEY_CAN_TRIAL_MSG = "footertext-android";
        String KEY_UPGRADEALERTAFLTERLOGIN = "upgradeAlertAflterLogin";
        String KEY_DataGaugeMultiplier = "dataGaugeMultiplier";
        String KEY_PlistExpirePeriodInHours = "plistExpirePeriodInHours";
        String KEY_timeIntervalToUpdateCache = "time_interval_for_update_cache";
        String KEY_timeIntervalToHitApi = "time_interval_for_hit_api";
        String KEY_timeIntervalToUpdateCacheoftrafficApi = "time_interval_for_update_cache_of_traffic_api";
        String KEY_promos = "promos";
        String KEY_Notification_Action = "out_of_data_notification";
        String KEY_events = "events";
        String KEY_animations = "animations";


        String KEY_REGISTRATION_ALERT = "registrationAlert";
        String KEY_REGISTRATION_MESSAGE = "message";
        String KEY_REGISTRATION_HTML_MESSAGE = "htmlmessage";
        String KEY_IGNORE_BUTTON = "ignoreButton";
        String KEY_ACCEPT_BUTTON = "acceptButton";
        String KEY_HTML = "html";

        String KEY_EMAIL_VALIDATION = "emailvalidations";
        String KEY_EMAIL_VAL_PROVIDER = "provider";
        String KEY_EMAIL_REG = "email_registration";
        String KEY_EMAIL_VAL_OPT = "validation_option";
        String KEY_EMAIL_WARN_MSG = "warning_message";
        String KEY_EMAIL_ERR_MSG = "error_message";
        String KEY_SOCIAL_REG = "social_registration";
        String KEY_SOCIAL_EMAIL_REQ = "email_required";

    }

    private static final class PlistHelperHolder {
        private static final PlistHelper Instance = new PlistHelper();
    }

    public class OutOfDataNotification implements Serializable {

        int percent_remaining;
        String alert_type, deep_link_url, msg;
        boolean enable;



        OutOfDataNotification(@NonNull PList pList) {
            percent_remaining = Integer.parseInt(pList.getData("percent_remaining"));
            alert_type = pList.getData("alert_type");
            deep_link_url = pList.getData("deep_link_url" );
            msg = pList.getData("msg");
            if (pList.getData("enable").equals("true")) {
                enable = true;
            } else {
                enable = false;
            }
        }

        public int getPercent_remaining() {
            return percent_remaining;
        }

        public void setPercent_remaining(int percent_remaining) {
            this.percent_remaining = percent_remaining;
        }

        public String getAlert_type() {
            return alert_type;
        }

        public void setAlert_type(String alert_type) {
            this.alert_type = alert_type;
        }

        public String getDeep_link_url() {
            return deep_link_url;
        }

        public void setDeep_link_url(String deep_link_url) {
            this.deep_link_url = deep_link_url;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }




    }
    public class OutOfDataNotificationModel  {

        private ArrayList<OutOfDataNotification> outOfDataNotifications;

        public ArrayList<OutOfDataNotification> getOutOfDataNotifications() {
            return outOfDataNotifications;
        }

        public void setOutOfDataNotifications(ArrayList<OutOfDataNotification> outOfDataNotifications) {
            this.outOfDataNotifications = outOfDataNotifications;
        }
    }

    public class VpnMenu implements Serializable {

        int order, hide, mo_itendifier;
        String key, name, icon;

        VpnMenu() {
        }

        VpnMenu(@NonNull PList pList) {
            order = Integer.parseInt(pList.getData("order"));
            key = pList.getData("key");
            name = pList.getData("name." + UserPref.getInstance().getUserLang());
            icon = pList.getData("icon");
            if (pList.get("hide") != null) {
                hide = Integer.parseInt(pList.getData("hide"));
            } else {
                hide = 0;
            }
        }

        public void setOrder(Integer order) {
            this.order = order;
        }

        public void setHide(Integer hide) {
            this.hide = hide;
        }

        public void setMo_itendifier(Integer mo_itendifier) {
            this.mo_itendifier = mo_itendifier;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public Integer getMo_itendifier() {
            return mo_itendifier;
        }

        public Integer getOrder() {
            return order;
        }

        public int getHide() {
            return hide;
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name;
        }

        public String getIcon() {
            return icon;
        }
    }

    public static class PlistConfig {

        public static void updateConfigToLocal(String data, boolean configList) {
            try {
                UserPref.getInstance().setPlistUpdateTime(System.currentTimeMillis());
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(FinjanVPNApplication.getInstance().openFileOutput(configList ? "config_plist.txt" : "tracker_plist.txt", Context.MODE_PRIVATE));
                outputStreamWriter.write(data);
                outputStreamWriter.close();
            } catch (IOException e) {
                Logger.logE("Exception", "File write failed: " + e.toString());
            }
        }

        public static boolean shouldUpdatePlist() {
            String lastPlistVersion = UserPref.getInstance().getPlistVersion();
            if (TextUtils.isEmpty(lastPlistVersion) || !lastPlistVersion.equalsIgnoreCase(FinjanUrls.URL_LAUNCH)){
                return true;
            }
            long savedTime = UserPref.getInstance().getPlistUpdateTime();
            long futureExpireTime = (1000 * 60 * 60 * PlistHelper.getInstance().getPlistExpirePeriodInHours()) + savedTime;
            return System.currentTimeMillis() > futureExpireTime;
        }

        private static String getConfigFromLocal(boolean configList) {

            String ret = "";

            try {
                InputStream inputStream = FinjanVPNApplication.getInstance().openFileInput(configList ? "config_plist.txt" : "tracker_plist.txt");

                if (inputStream != null) {
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String receiveString = "";
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((receiveString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(receiveString);
                    }

                    inputStream.close();
                    ret = stringBuilder.toString();
                }
            } catch (FileNotFoundException e) {
                Logger.logE("login activity", "File not found: " + e.toString());
            } catch (IOException e) {
                Logger.logE("login activity", "Can not read file: " + e.toString());
            }

            return ret;
        }
    }

}
