package com.finjan.securebrowser.helpers.config;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 28/2/18
 *
 * not in use
 */
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.provider.Settings;

import com.finjan.securebrowser.helpers.logger.Logger;

public class HardwareInformation {
    public static final String TAG = "HardwareInformation";

    public static String platform;                            // Device OS
    public static String uuid;                                // Device UUID

    private static final String ANDROID_PLATFORM = "Android";
    private static final String AMAZON_PLATFORM = "amazon-fireos";
    private static final String AMAZON_DEVICE = "Amazon";

    /**
     * Constructor.
     */
    public HardwareInformation() {
    }

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     */
    public  void initialize(Context context) {
        HardwareInformation.uuid = getUuid(context);
        try {
            Logger.logE("Barun pandit ",execute().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @return                  True if the action was valid, false if not.
     */
    public JSONObject execute() throws JSONException {
        JSONObject r=new JSONObject();
            r.put("uuid", HardwareInformation.uuid);
            r.put("timeZoneId", this.getTimeZoneID());
            r.put("platform", this.getPlatform());
            r.put("isVirtual", this.isVirtual());
            r.put("buildProduct", this.getProductName());
            r.put("buildModel", this.getModel());
            r.put("buildManufacturer", this.getManufacturer());
            r.put("buildSerial", this.getSerial());
            r.put("buildBoard", this.getBoard());
            r.put("buildBrand", this.getBrand());
            r.put("buildHost", this.getHost());
            r.put("buildFingerprint", this.getFingerprint());
            r.put("buildId", this.getId());
            r.put("buildType", this.getType());
            r.put("buildUser", this.getUser());
            r.put("buildVersionRelease", this.getOSVersionRelease());
            r.put("buildVersionBase", this.getOSVersionBase());
            r.put("buildVersionIncremental", this.getOSVersionIncremental());
            r.put("buildVersionSDK", this.getOSVersionSDK());
        return r;
    }

    /**
     * Get the OS name.
     *
     * @return
     */
    public String getPlatform() {
        String platform;
        if (isAmazonDevice()) {
            platform = AMAZON_PLATFORM;
        } else {
            platform = ANDROID_PLATFORM;
        }
        return platform;
    }


    /**
     * Get the device's Universally Unique Identifier (UUID).
     *
     * @return
     */
    public static String getUuid(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getTimeZoneID() {
        TimeZone tz = TimeZone.getDefault();
        return (tz.getID());
    }

    public String getModel() {
        String model = android.os.Build.MODEL;
        return model;
    }

    public String getProductName() {
        String productname = android.os.Build.PRODUCT;
        return productname;
    }

    public String getManufacturer() {
        String manufacturer = android.os.Build.MANUFACTURER;
        return manufacturer;
    }

    public String getSerial() {
        String serial = android.os.Build.SERIAL;
        return serial;
    }

    public String getBoard() {
        String board = android.os.Build.BOARD;
        return board;
    }

    public String getBrand() {
        String brand = android.os.Build.BRAND;
        return brand;
    }

    public String getHost() {
        String host = android.os.Build.HOST;
        return host;
    }

    public String getFingerprint() {
        String fingerprint = android.os.Build.FINGERPRINT;
        return fingerprint;
    }

    public String getId() {
        String id = android.os.Build.ID;
        return id;
    }

    public String getType() {
        String type = android.os.Build.TYPE;
        return type;
    }

    public String getUser() {
        String serial = android.os.Build.SERIAL;
        return serial;
    }


    public String getOSVersionRelease() {
        String release = android.os.Build.VERSION.RELEASE;
        return release;
    }

    public String getOSVersionBase() {
        String baseOs = android.os.Build.VERSION.BASE_OS;
        return baseOs;
    }

    public String getOSVersionIncremental() {
        String incremental = android.os.Build.VERSION.INCREMENTAL;
        return incremental;
    }

    public String getOSVersionSDK() {
        String sdk = String.valueOf(android.os.Build.VERSION.SDK_INT);
        return sdk;
    }

    /**
     * Function to check if the device is manufactured by Amazon
     *
     * @return
     */
    public boolean isAmazonDevice() {
        if (android.os.Build.MANUFACTURER.equals(AMAZON_DEVICE)) {
            return true;
        }
        return false;
    }

    public boolean isVirtual() {
        return android.os.Build.FINGERPRINT.contains("generic") ||
                android.os.Build.PRODUCT.contains("sdk");
    }



}
