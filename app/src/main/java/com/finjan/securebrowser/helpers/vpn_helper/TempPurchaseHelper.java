package com.finjan.securebrowser.helpers.vpn_helper;

import android.text.TextUtils;
import android.widget.Toast;

import com.finjan.securebrowser.helpers.ToastHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.licensing.models.billing.Inventory;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.ui.iab.LicenseUtil;
import com.finjan.securebrowser.vpn.ui.iab.UpgradeVpnActivity;
import com.finjan.securebrowser.vpn.util.TrafficController;

import de.greenrobot.event.EventBus;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 14/09/17.
 *
 * This class check and update if there is any already purchase on device.
 *
 */

public class TempPurchaseHelper {

    private static final class TempPurchaseHelperHolder{
        private static final TempPurchaseHelper instance=new TempPurchaseHelper();
    }
    private TempPurchaseHelper(){}
    public static TempPurchaseHelper getInstance(){
        return TempPurchaseHelperHolder.instance;
    }
    private boolean checkIfPurExists(Inventory mInventory){
        return mInventory!=null && !TextUtils.isEmpty(getExistingSku(mInventory));
    }

    /**
     * check if there is any existing purchase on device
     *
     * @param mInventory
     * @return
     */
    public String getExistingSku(Inventory mInventory){
        if(mInventory!=null) {
            String sku = null;
            if (mInventory.hasPurchase(LicenseUtil.getSkuAllDevicesMonthly())
                    && mInventory.getPurchase(LicenseUtil.getSkuAllDevicesMonthly()).getAutoRenewing()) {
                sku = LicenseUtil.getSkuAllDevicesMonthly();
            } else if (mInventory.hasPurchase(LicenseUtil.getSkuAllDevicesYearly())
                    && mInventory.getPurchase(LicenseUtil.getSkuAllDevicesYearly()).getAutoRenewing()) {
                sku = LicenseUtil.getSkuAllDevicesYearly();
            } else if (mInventory.hasPurchase(LicenseUtil.getSkuThisDevicesYearly())
                    && mInventory.getPurchase(LicenseUtil.getSkuThisDevicesYearly()).getAutoRenewing()) {
                sku = LicenseUtil.getSkuThisDevicesYearly();
            } else if (mInventory.hasPurchase(LicenseUtil.getSkuThisDevicesMonthly())
                    && mInventory.getPurchase(LicenseUtil.getSkuThisDevicesMonthly()).getAutoRenewing()) {
                sku = LicenseUtil.getSkuThisDevicesMonthly();
            }
            return !TextUtils.isEmpty(sku)?sku:"";
        }
        return "";
    }


    public void updateTempPur(Inventory inventory){
        if(inventory==null || !checkIfPurExists(inventory)){
                UserPref.getInstance().setTempPurchaseToken(null);
        }
    }
}