package com.finjan.securebrowser.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;

import com.finjan.securebrowser.activity.Finger_Passcode_Activity;
import com.finjan.securebrowser.activity.HomeActivity;
import com.finjan.securebrowser.activity.PasscodeActivity;
import com.finjan.securebrowser.constants.DrawerConstants;
import com.finjan.securebrowser.fragment.ParentHomeFragment;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper;

import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 * for redirecting to appropriate screen when user launch app, i.e. whether to show VPN or Browser
 */
public class ScreenNavigation {

    //screens
    public static final int HOME_SCREEN=0;
    public static final int VPN_SCREEN=1;

    public static final String CHECKING_PASSWORD="checking_password";
    private boolean isHomeScreenInitiated;
    private boolean isVpnScreenInitiated;

    private static final class ScreenNavigationHolder{
        private static final ScreenNavigation Instance=new ScreenNavigation();
    }
    public static ScreenNavigation getInstance(){
        return ScreenNavigationHolder.Instance;
    }
    public void saveLastScreen(Object scren){
        if(scren instanceof ParentHomeFragment){
            UserPref.getInstance().setMyLastPage(HOME_SCREEN);
            return;
        }
        if(scren instanceof VpnActivity || scren instanceof VpnHomeFragment){
            UserPref.getInstance().setMyLastPage(VPN_SCREEN);
        }
    }

    public void initLastScreen(Context context,String screen){
        if(!TextUtils.isEmpty(GlobalVariables.getInstnace().lastDirectUrl)){
            if(NativeHelper.getInstnace().checkActivity(VpnActivity.getInstance())){
                VpnActivity.getInstance().finish();
            }
            callHomeActivity(context,screen);
            return;
        }
        if(UserPref.getInstance().getIsBrowser()){
            if(UserPref.getInstance().getMYLastpage()==VPN_SCREEN && TextUtils.isEmpty(GlobalVariables.getInstnace().lastDirectUrl)){
//                GlobalVariables.getInstnace().userStatusChanged=true;
                callVPNScreen(context);
            }else {
//                GlobalVariables.getInstnace().userStatusChanged=false;
                callHomeActivity(context,screen);
            }
        }else {
            callVPNScreen(context);
        }
    }

    private static HashSet<String> activeScreenList= new LinkedHashSet<>();
    public static void pushActivApp(String screenName){
        activeScreenList.add(screenName);
    }
    public static HashSet<String> getActiveScreenList(){return activeScreenList;}

    public void callVPNScreen(Context context){
        Intent intent = new Intent(context, VpnActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        isVpnScreenInitiated=true;
        isHomeScreenInitiated=false;
        context.startActivity(intent);
    }
    public Intent getcallVPNScreen(Context context){
        Intent intent = new Intent(context, VpnActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        isVpnScreenInitiated=true;
        isHomeScreenInitiated=false;
//        context.startActivity(intent);
        return intent;
    }
    public void callVPNScreenPromoDeepLink(Context context,String promoId){
        PromoHelper.Companion.getInstance().setFailedDeepLinkingTry(true);
        Intent intent = new Intent(context, VpnActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("promo_id",promoId);
        isVpnScreenInitiated=true;
        isHomeScreenInitiated=false;
        context.startActivity(intent);
    }
    public Intent getcallVPNScreenPromoDeepLink(Context context,String promoId){
        PromoHelper.Companion.getInstance().setFailedDeepLinkingTry(true);
        Intent intent = new Intent(context, VpnActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("promo_id",promoId);
        isVpnScreenInitiated=true;
        isHomeScreenInitiated=false;
//        context.startActivity(intent);
        return intent;
    }

    public void callVPNScreen(Context context,String screenToOpen){
        Intent intent = new Intent(context, VpnActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("ll_deep_link_url",screenToOpen);
        isVpnScreenInitiated=true;
        isHomeScreenInitiated=false;
        context.startActivity(intent);
    }
    public Intent getcallVPNScreen(Context context,String screenToOpen){
        Intent intent = new Intent(context, VpnActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("ll_deep_link_url",screenToOpen);
        isVpnScreenInitiated=true;
        isHomeScreenInitiated=false;
//        context.startActivity(intent);
        return intent;
    }
    public void callHomeActivity(Context context,String screen) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if(!TextUtils.isEmpty(screen)){
            intent.putExtra("screen",screen);
        }
        context.startActivity(intent);
        GlobalVariables.getInstnace().isCheckingScreenLock = false;
        isHomeScreenInitiated=TextUtils.isEmpty(screen);
    }
    public Intent getcallHomeActivity(Context context,String screen) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if(!TextUtils.isEmpty(screen)){
            intent.putExtra("screen",screen);
        }

//        context.startActivity(intent);
        GlobalVariables.getInstnace().isCheckingScreenLock = false;
        isHomeScreenInitiated=TextUtils.isEmpty(screen);
        return intent;
    }

    public void callRedeemOffer(String offer){

    }

    public void setHomeScreenInitiated(boolean screenInitiated){
        this.isHomeScreenInitiated=screenInitiated;
    }
    public boolean getIsHomeScreenInitiated(){return isHomeScreenInitiated;}
    public boolean getIsVPNScreenInitiated(){return isVpnScreenInitiated;}

    public void startHome(Context context,String screen){
        if (UserPref.getInstance().getPasscode() && !TextUtils.isEmpty(UserPref.getInstance().getLockCode()) &&
                UserPref.getInstance().getLockCode().trim().length() > 0) {

            boolean skip=false;
            if (ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.USE_FINGERPRINT) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.USE_FINGERPRINT}, 225);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //Fingerprint API only available on from Android 6.0 (M)
                FingerprintManager fingerprintManager = (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);
                if (fingerprintManager!=null && !fingerprintManager.isHardwareDetected()) {
                    // Device doesn't support fingerprint authentication
                    skip = true;
                }
            } else {
                // Device doesn't support fingerprint authentication
                skip = true;
            }

            GlobalVariables.getInstnace().isCheckingScreenLock=true;
            if (skip) {
                GlobalVariables.getInstnace().isCheckingScreenLock=true;
                Intent intent =new Intent(context, Finger_Passcode_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
                intent.putExtra(ScreenNavigation.CHECKING_PASSWORD,true);
                context.startActivity(intent);
            } else {
                Intent intent = new Intent(context, PasscodeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(ScreenNavigation.CHECKING_PASSWORD,true);
                context.startActivity(intent);
            }
        } else {
            ScreenNavigation.getInstance().initLastScreen(context,screen);
        }
    }

    /**
     * for specific screen from homeactivity to fragment on main browser.
     */
    private String currentDefaultScreen= DrawerConstants.DKEY_HOME;
    public void setCurrentDefaultScreen(String currentDefaultScreen){
        if(!TextUtils.isEmpty(currentDefaultScreen)){
            this.currentDefaultScreen=currentDefaultScreen;
        }
    }
    public String  getCurrentDefaultScreen(){return currentDefaultScreen;}
}
