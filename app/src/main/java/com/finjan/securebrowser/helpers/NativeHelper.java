package com.finjan.securebrowser.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.TypedValue;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.avira.common.backend.WebUtility;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.controller.JsonParser;
import com.finjan.securebrowser.vpn.helpers.JsonConstans;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.util.PermissionUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 * Native helper class contain various methods used in fragments and classes
 */

public class NativeHelper {

    private static final String TAG = NativeHelper.class.getSimpleName();

    public static NativeHelper getInstnace() {
        return NativeHelperHolder.INSTANCE;
    }

    public Activity currentAcitvity;
    public boolean checkContext(Context context) {
        if (context == null) {
            Logger.logE(TAG, "context null handled");
            return false;
        }
        Activity activity = (Activity) context;
        if (activity.isFinishing()) {
            Logger.logE(TAG, "context finishing handled");
            return false;
        }
        else if (activity.isDestroyed()) {
            Logger.logE(TAG, "context destroyed handled");
            return false;
        }

        return true;
    }

    public boolean checkActivity(Activity activity) {
        if (activity == null) {
            return false;
        }
        else if (activity.isFinishing()) {
            Logger.logE(TAG, "activity finishing handled");
            return false;
        }
        else if (activity.isDestroyed()) {
            Logger.logE(TAG, "activity destroyed handled");
            return false;
        }
        else {
            return true;
        }
    }

    public boolean isSupportFingurePrint() {
        return isAbove23();
    }

    public boolean isSupportElevation() {
        return isAbove21();
    }


    private boolean isAbove21() {
        return Build.VERSION.SDK_INT >= 21;
    }

    private boolean isAbove23() {
        return Build.VERSION.SDK_INT >= 23;
    }

    /**
     * method to hide key board
     *
     * @param activity
     */
    public void hideKeyBoard(Activity activity) {

        if (checkActivity(activity) && null != activity.getCurrentFocus()) {

            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
//            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);

//            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public boolean isKeyboardShowing(Activity activity) {
        if (!NativeHelper.getInstnace().checkActivity(activity)) {
            return false;
        }
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        return imm.isAcceptingText();
    }

    public void showKeyBoard(Activity activity) {

        if (checkActivity(activity) && null != activity.getCurrentFocus()) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            inputMethodManager.showSoftInput(activity.getCurrentFocus(), InputMethodManager.SHOW_FORCED);
        }
    }


    public float getPxValue(float value, Context context) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, value, context.getResources().getDisplayMetrics());
    }

    public int getScanRemain() {
        int spendScans = UserPref.getInstance().getSafeScanRemain();
        int totalScans = getTotalScans();
//        return 1;
        return totalScans - spendScans;
    }

    private int getTotalScans() {

//        int days = 0;
//        String daysTotal = Utils.getPlistvalue(Constants.KEY_APP_PURCHASE, Constants.KEY_FREE_SCAN_NUMBER);
//        if (!TextUtils.isEmpty(daysTotal)) {
//            days = Integer.parseInt(daysTotal);
//        }
//        return days;
        return PlistHelper.getInstance().getInAppPurchase().getFreeprivacyscan();
//        return 0;
    }

    public void changeStatusBarColor(int color, Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourceHelper.getInstance().getColor(color));
        }
    }

    public void changeStatusBarTransparent(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
    public void changeStatusBarTransSplash(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

//            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public int getDrawerHeight(boolean isPortrait, Context context) {
        Resources r = context.getResources();
        float valueToEvaluate = 0;
        if (isPortrait) {
//            valueToEvaluate = r.getDimension(R.dimen._160sdp) + r.getDimension(R.dimen._30sdp);
            valueToEvaluate = r.getDimension(R.dimen._80sdp) + r.getDimension(R.dimen._80sdp);
        } else {
            valueToEvaluate = r.getDimension(R.dimen._80sdp);
//            valueToEvaluate = r.getDimension(R.dimen._70sdp) + r.getDimension(R.dimen._30sdp);
        }
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, valueToEvaluate, r.getDisplayMetrics()));
    }

    public int getSearchEngineType(String searchEngine) {
        int type = 1;
        switch (searchEngine.toLowerCase()) {
            case "safe":
                type = 0;
                break;
            case "google":
                type = 1;
                break;
            case "bing":
                type = 2;
                break;
            case "yahoo":
                type = 3;
                break;
            case "aol":
                type = 4;
                break;
            case "ask":
                type = 5;
                break;
        }
        return type;
    }

    public boolean isPhoneReadStatePermissionGranted(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public void setDeviceId(Context context) {
        if (PermissionUtil.isPhoneReadStatePermissionGranted(context)) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String android_id = telephonyManager == null ? "" : telephonyManager.getDeviceId();
            UserPref.getInstance().setDeviceId(android_id);
        }
    }

    public void showToast(Context context, String msg, int toastLength) {
        if (context == null) {
            context = FinjanVPNApplication.getInstance().getApplicationContext();
        }
        Toast.makeText(context, msg, toastLength).show();
    }

    public void showToast(String msg, int toastLength) {
        showToast(FinjanVPNApplication.getInstance().getApplicationContext(), msg, toastLength);
    }

    public void serverErrorToast(Context context) {
        showToast(context, context.getString(R.string.unable_to_connect_try_again_after_some_time));
    }

    public void showToast(Context context, VolleyError error) {
        if (error == null || WebUtility.getMessage(error) == null) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(WebUtility.getMessage(error));
            String msg = JsonParser.getInstance().getMsgFromError(jsonObject);
            if (!TextUtils.isEmpty(msg)) {
                if (context == null) {
                    context = FinjanVPNApplication.getInstance().getApplicationContext();
                }
                showToast(context, msg, Toast.LENGTH_SHORT);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){e.printStackTrace();}
    }
    public String getVollyErrorMsg(VolleyError error){
        String errorMsg="";
        if (!(error == null || WebUtility.getMessage(error) == null)) {
            try {
                JSONObject jsonObject = new JSONObject(WebUtility.getMessage(error));
                errorMsg= JsonParser.getInstance().getMsgFromError(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return errorMsg;
    }

    public void showToast(Context context, Object object) {
        if (object instanceof VolleyError) {
            showToast(context, (VolleyError) object);
        } else if (object instanceof JSONObject) {
            try {
                JSONObject response = (JSONObject) object;
                if (response.has(JsonConstans.MESSAGE)) {
                    NativeHelper.getInstnace().showToast(context, response.getString(JsonConstans.MESSAGE), Toast.LENGTH_SHORT);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getIpAddressOfDevice() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Logger.logE(TAG, "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Logger.logE(TAG, ex.toString());
        }
        return "";
    }

    public void setStatusIcon(Context context, String status, ImageView ivStatus) {
        if (ivStatus != null && !TextUtils.isEmpty(status)) {

            switch (status.toLowerCase()) {
                case AppConstants.RATING_DANGEROUS:
                    ivStatus.setImageResource(R.drawable.scan_status_danger);
                    break;
                case AppConstants.RATING_GREEN:
                    ivStatus.setImageResource(R.drawable.scan_status_safe);
                    break;
                case AppConstants.RATING_BLUE:
                    Drawable drawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.scan_status_safe, context.getTheme());
                    if (drawable != null) {
                        drawable.setColorFilter(ResourceHelper.getInstance().getColor(R.color.accent_color_1), PorterDuff.Mode.SRC_ATOP);
                    }
                    break;
                case AppConstants.RATING_INVALID:
                    break;
                case AppConstants.RATING_SUSPICIOUS:
                    ivStatus.setImageResource(R.drawable.scan_status_caution);
                    break;
                case AppConstants.RATING_SAFE:
                    ivStatus.setImageResource(R.drawable.scan_status_safe);
                    break;
                default:
                    return;

            }
        }
    }

    public String getRoundOffString(String actuallString) {
        if (!TextUtils.isEmpty(actuallString) && !actuallString.startsWith("0")) {
//            return actuallString;
            String[] a = actuallString.split(" ");
            if(a[0].contains(".")){
                a[0] = a[0].replace(a[0].substring(a[0].indexOf(".")
                        ,a[0].length()),"");
            }
            if(a.length>1){
                return a[0]+" "+a[1];
            }else return a[0];
        }
        return "0";
    }

    public String getUrlDomain(String orignalURL) {
        try {
            URI uri = new URI(orignalURL);
            return uri.getHost();
        } catch (URISyntaxException exceptions) {
            exceptions.printStackTrace();
            return orignalURL;
        }
    }

    public Purchase getStaticPurchase() {
        Purchase purchase = null;
        try {
            purchase = new Purchase("subs", "{\"orderId\":\"GPA.3387-0765-3289-22341\",\"packageName\":\"com.finjan.securebrowser\",\"productId\":\"monthly_subscription_0.99\",\"purchaseTime\":1499407226410,\"purchaseState\":0,\"purchaseToken\":\"kbplchhbfjfmmhejjigmmcnd.AO-J1Ow_QdN7dpWqRYaALmdxB0CTrMxvXjzukSTjW44GOg94gifErhefZQu64VLz7pFa1PJL35HQhrhnGPAZz59Dq8OAjcrfyqfTIIZYwclx_SeJCyo9_iBAMitkcaB2h5cNeL8C4lkhGZinQL7cSPLvAt_UE5eO5g\",\"autoRenewing\":true}"
                    , "");

            // TODO check signature
//                // Verify signature
//                if (!Security.verifyPurchase(mSignatureBase64, purchaseData, dataSignature)) {
//                    logError("Purchase signature verification FAILED for sku " + sku);
//                    result = new IabResult(IABHELPER_VERIFICATION_FAILED, "Signature verification failed for sku " + sku);
//                    if (mPurchaseListener != null) mPurchaseListener.onIabPurchaseFinished(result, purchase);
//                    return true;
//                }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return purchase;
    }

    public interface ProgressBarTypes {
        String NORMAL = "NORMAL";
    }

    private static final class NativeHelperHolder {
        private static final NativeHelper INSTANCE = new NativeHelper();
    }
}
