package com.finjan.securebrowser.helpers;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.widget.Toast;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 22/02/18.
 *
 */
public class DownloadHelper {

    private static final class DownloadHelperHolder{
        private static final DownloadHelper Instance=new DownloadHelper();
    }
    public static DownloadHelper getInstance(){
        return DownloadHelperHolder.Instance;
    }
    public void downloadFile(Context context,String userAgent, String downloadURL, String downloadfileName) {
        if (TextUtils.isEmpty(downloadURL)) {
            if(context!=null){
                Toast.makeText(context, R.string.unabel_to_download, Toast.LENGTH_SHORT).show();
            }
        } else {

            if(!downloadURL.startsWith("http")){
                if(context!=null){
                    Toast.makeText(context, "Please enter valid path", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            DownloadManager.Request request = new DownloadManager
                    .Request(Uri.parse(downloadURL));
            String fileName = "";
            if (TextUtils.isEmpty(downloadfileName)) {
                fileName = getFileName(downloadURL);
            } else {
                fileName = downloadfileName;
            }
            String cookies = CookieManager.getInstance().getCookie(downloadURL);
            request.addRequestHeader("cookie", cookies);
            if(!TextUtils.isEmpty(userAgent)){
                request.addRequestHeader("User-Agent", userAgent);
            }
            request.setTitle(ResourceHelper.getInstance().getString(R.string.app_name));
            request.setDescription(fileName);
            request.allowScanningByMediaScanner();
            request.setAllowedOverRoaming(true);
            request.setVisibleInDownloadsUi(true);
            try{
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
            }catch (IllegalStateException e){}
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
            DownloadManager mDownloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            GlobalVariables.getInstnace().mDownloadedFileID = mDownloadManager.enqueue(request);
            ToastHelper.showToast(context,"Downloading file...",Toast.LENGTH_SHORT);
        }
    }

    private String getFileName(String downloadURL) {
        int index = downloadURL.lastIndexOf("/");
        index++;
        return downloadURL.substring(index);
    }
}
