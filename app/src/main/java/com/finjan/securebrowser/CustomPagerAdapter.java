package com.finjan.securebrowser;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;

import android.view.View;
import android.view.ViewGroup;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class CustomPagerAdapter extends PagerAdapter {

    private Context mContext;
    private boolean isHistoryTab;

    public CustomPagerAdapter(Context context,boolean isHistoryTab) {
        mContext = context;
        this.isHistoryTab=isHistoryTab;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
//        CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
//        LayoutInflater inflater = LayoutInflater.from(mContext);
//        ViewGroup layout = (ViewGroup) inflater.inflate(customPagerEnum.getLayoutResId(), collection, false);
//        collection.addView(layout);
//        return layout;
        return null;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 2;
//        return CustomPagerEnum.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position>=2){
            return "";
        }
        if(!isHistoryTab){
            CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position+2];
            return mContext.getString(customPagerEnum.getTitleResId());
        }else {
            CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
            return mContext.getString(customPagerEnum.getTitleResId());
        }

    }

}