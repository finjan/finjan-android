package com.finjan.securebrowser;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class UrlEditText extends AutoCompleteTextView {

    public String state;
    public static final String EDITING = "EDITING";
    public static final String EDITABLE = "EDITABLE";
    public static final String TITLE = "TITLE";
    public DisplayWebView delegate;
    public Handler scanHandler = new Handler();

    private static final String sf="sf-ui-regular.ttf";
    private void setDefaults(Context context){
        Typeface font=Typeface.createFromAsset(context.getAssets(), "fonts/"+sf);
        this.setTypeface(font);
        this.setTextColor(ResourceHelper.getInstance().getColor(R.color.white));

    }
    public void setAnchor(View view){
        if(view!=null){
            this.setDropDownAnchor(view.getId());
        }
    }

    public UrlEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        state = EDITING;
        setDefaults(context);

        /*
            Setup functions for view listeners and handlers
        */
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (state.equals(EDITABLE)) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        delegate.popCurrentView();
                    }
                } else if (state.equals(EDITING)) {
                    return false;
                }

                // Return true unless we're actively editing
                return true;
            }
        });

        setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Utils.showKeyboard(view);
                }
            }
        });

        setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    complete(view);
                    return true;
                }
                return false;
            }
        });

        setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    complete(view);
                    return true;
                }
                return false;
            }
        });
    }

    public void complete(View view) {
        String searchText = getText().toString();
        if (searchText.length() > 0) {
            view.clearFocus();
            Utils.hideKeyboard(view);
            delegate.omnibarResults(searchText);
        }
    }


    /*
        Animations
     */
    public void setAsTitle() {
        setTextColor(getResources().getColor(R.color.fj_blue_50));
        state = TITLE;
    }

    public void setAsReportTitle() {
        setTextColor(getResources().getColor(R.color.transWhite_50));
        state = TITLE;
    }

    public void setAsEditable() {
        setTextColor(getResources().getColor(R.color.fj_blue));
        state = EDITABLE;
    }

    public void setAsEditing() {
        setTextColor(getResources().getColor(R.color.fj_blue));
        state = EDITING;
        selectAll();
        requestFocus();
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            delegate.hideSoftKeyBoard();
        }
        return super.onKeyPreIme(keyCode, event);
    }
}
