package com.finjan.securebrowser;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class CountedRunnable implements Runnable {
    Integer counter = 0;

    public void run() {}
}
