package com.finjan.securebrowser;

import android.os.Handler;

import com.finjan.securebrowser.model.GoogleResults;
import com.finjan.securebrowser.model.SearchResult;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public interface DisplayWebView {
     void popCurrentView();

     void loadSearchResult(int index);

     void failedToGetReport();

     void allowCancel(Handler handler);

     void showScanningView();

     void omnibarResults(String contents);

     void searchResultDisplayed(int currentDisplayIndex);

     void showReportAtSearchIndex(int index);

     SearchResult resultAtIndex(int index);

     void checkSafety(int index);
     void hideSoftKeyBoard();
}
