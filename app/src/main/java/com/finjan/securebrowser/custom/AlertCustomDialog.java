package com.finjan.securebrowser.custom;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import android.text.TextUtils;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.NativeHelper;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 */

public class AlertCustomDialog {

    AlertDialog alertDialog;

    public AlertCustomDialog(Context context, String message) {
        this(context, null, message);
    }

    public AlertCustomDialog(Context context, String title, String message) {
        this(context, title, message, null);
    }

    public AlertCustomDialog(Context context, String message, AlertListener listener) {
        this(context, message, null, null, listener);
    }

    public AlertCustomDialog(Context context, String message, String positiveBtn, AlertListener listener) {
        this(context, null, message, positiveBtn, null, listener);
    }

    public AlertCustomDialog(Context context, String message, String positiveBtn, String negativeBtn,
                             AlertListener listener) {
        this(context, null, message, positiveBtn, negativeBtn, listener);
    }

    public AlertCustomDialog(Context mContext, String title, String message, String positiveText, String negativeText, final AlertListener listener) {
        String postiveButtonText = "Ok";
        String negativeButtonText = "Cancel";
        if (NativeHelper.getInstnace().checkContext(mContext)) {
            ContextThemeWrapper ctw = new ContextThemeWrapper(mContext, R.style.LightDialogTheme);
            AlertDialog.Builder alertDialogBuilder1 = new AlertDialog.Builder(ctw);
            alertDialogBuilder1.setCancelable(false);
            if (!isEmpty(title)) {
                alertDialogBuilder1.setTitle(title);
            }
            if (!isEmpty(positiveText)) {
                postiveButtonText = positiveText;
            }
//        if (!isEmpty(negativeText)){
            negativeButtonText = negativeText;
//        }
            alertDialogBuilder1.setMessage(message);
            alertDialogBuilder1.setPositiveButton(postiveButtonText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (listener == null) {
                        dialog.dismiss();
                    } else {
                        listener.onPositiveListener(alertDialog);
                    }
                }
            });

            if (!isEmpty(negativeButtonText)) {
                alertDialogBuilder1.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener == null) {
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            listener.onNegativeListener();
                        }
                    }
                });
            }


            alertDialog = alertDialogBuilder1.create();
            alertDialog.show();
        }

    }

    public boolean isEmpty(String text) {
        return TextUtils.isEmpty(text);
    }

    public interface AlertListener {
        public void onPositiveListener(DialogInterface dialog);

        public void onNegativeListener();
    }

}
