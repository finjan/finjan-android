package com.finjan.securebrowser.custom;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.model.WatchCatsModel;
import com.finjan.securebrowser.vpn.auto_connect.AutoConnectHelper;
import com.finjan.securebrowser.vpn.controller.database.ContentProviderClient;
import com.finjan.securebrowser.vpn.controller.database.DatabaseContract;
import com.finjan.securebrowser.vpn.util.AutoClose;
import com.squareup.picasso.Picasso;

public class ContentDialog extends Dialog {
    private Context context;
    private WatchCatsModel.Category watchCatsModelCategory;
    private ImageView iv_content;
    private TextView tv_tital,tv_description,tv_location,tv_connect;
    private BroadcastReceiver receiver;
    private ProgressDialog mProgressDialog;
    private LinearLayout ll_share;

    public ContentDialog(Context context, WatchCatsModel.Category watchCatsModelCategory) {
        super(context);
        this.context = context;
        this.watchCatsModelCategory = watchCatsModelCategory;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.content_dialog_layout);
        findViews();
        setTextonViews();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                 do something here.
                mProgressDialog.hide();
                LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
                dismiss();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(watchCatsModelCategory.getAttributes().getUrl()));
                context.startActivity(i);
            }
        };
    }

    private void setTextonViews() {

        try {
            Picasso.get().load(watchCatsModelCategory.getAttributes().getImage()).into(iv_content);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (watchCatsModelCategory != null && watchCatsModelCategory.getAttributes() != null && !TextUtils.isEmpty(watchCatsModelCategory.getAttributes().getTitle()))
        tv_tital.setText(watchCatsModelCategory.getAttributes().getTitle());
        else
            tv_tital.setVisibility(View.GONE);
        if (watchCatsModelCategory != null && watchCatsModelCategory.getAttributes() != null && !TextUtils.isEmpty(watchCatsModelCategory.getAttributes().getDescription()))
            tv_description.setText(watchCatsModelCategory.getAttributes().getDescription());
        else
            tv_description.setVisibility(View.GONE);
        String title="Unknown";
        if (!TextUtils.isEmpty(watchCatsModelCategory.getAttributes().getCountry())) {
            ContentProviderClient contentProviderClient = new ContentProviderClient();
            Cursor cursor = contentProviderClient.getServer(context, watchCatsModelCategory.getAttributes().getCountry());
            if (cursor != null) {
                if (cursor.moveToNext()) {
                 title   = cursor.getString(cursor.getColumnIndex(DatabaseContract.ServerTable.NAME));
                }
                AutoClose.closeSilently(cursor);
            }
        }
        tv_location.setText("our "+title+" Location");
        tv_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AutoConnectHelper.Companion.getInstance().reachedTrafficLimit(context);
                Utils.connectToVpn(context,watchCatsModelCategory.getAttributes().getCountry().trim());
//                mProgressDialog = ProgressDialog.show(context,"","Please wait while vpn is not connected successfully");
                mProgressDialog = new ProgressDialog(context,R.style.VpnProgressDialog);
                mProgressDialog.setMessage("Please wait while vpn is connected successfully");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, new IntentFilter(AppConstants.CONNECTED_ACTION));

                    }
                },3000);
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(watchCatsModelCategory.getAttributes().getUrl()));
//                context.startActivity(i);
            }
        });

    }

    private void findViews() {
        iv_content = (ImageView) findViewById(R.id.iv_content);
        tv_tital = (TextView) findViewById(R.id.tv_title);
        tv_description = (TextView) findViewById(R.id.tv_description);
        tv_location = (TextView) findViewById(R.id.tv_location);
        tv_connect = (TextView) findViewById(R.id.tv_connect);
        ll_share = (LinearLayout) findViewById(R.id.ll_share);
        String udata="Conect Now?";
        tv_connect.setText(Html.fromHtml("<u>Connect Now?</u>"));
        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, watchCatsModelCategory.getAttributes().getUrl());
                context.startActivity(Intent.createChooser(intent, "Url"));

            }
        });

    }



}
