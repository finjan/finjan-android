package com.finjan.securebrowser.custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.fragment.ParentHomeFragment;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Web view helper class that help VideoEnabledWeb to play videos on web view
 */


public class WebVideoHelper {
    private static final String TAG = WebVideoHelper.class.getSimpleName();
    private static final String PORTRAIT = "Portrait";
    private static final String NOT_DEFINED = "Not defined";
    private static final String LANDSCAPE = "Landscape";
    private static final String AUTO_ROTATION = "Auto";
    private static final String ROTATION_ON = "On Rotation";
    private static final String ROTATION_OFF = "Off Rotation";
    private RelativeLayout videoLayout, webViewContainer, rlVideoLayout;
    private View loaderVideoView, parentView;
    private VideoEnabledWebView urlWebView;
    private VideoEnabledWebChromeClient webChromeClient = null;
//    private View lineSeparator, v_touch_area;
    private TextView tvOrientation;
    private ImageView ivOrientation;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
//                case R.id.tv_orientation:
//                    onClickOrientation(activity,((TextView) v).getText().toString(), (TextView) v);
//                    break;
//                case R.id.iv_fullscreen_status:
//                    webChromeClient.exitFullScreen();
//                    break;
//                case R.id.tv_rotation:
//                    if (((TextView) v).getText().toString().equalsIgnoreCase(ROTATION_OFF)) {
//
////                    onClickOrientation(NOT_DEFINED,tvOrientation);
//
//                        if (tvOrientation.getText() == PORTRAIT) {
//                            onClickOrientation(activity,LANDSCAPE, tvOrientation);
//                        } else if (tvOrientation.getText() == LANDSCAPE) {
//                            onClickOrientation(activity,PORTRAIT, tvOrientation);
//                        } else {
//                            if (FinjanVPNApplication.getInstance().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                                onClickOrientation(activity,LANDSCAPE, tvOrientation);
//                            } else if (FinjanVPNApplication.getInstance().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//                                onClickOrientation(activity,PORTRAIT, tvOrientation);
//                            } else {
//                                onClickOrientation(activity,LANDSCAPE, tvOrientation);
//                            }
//                        }
////                        tvRotation.setText(ROTATION_ON);
//                    } else if (((TextView) v).getText().toString().equalsIgnoreCase(ROTATION_ON)) {
//                        /**
//                         * user cliecked her to make rotation on
//                         */
//                        onClickOrientation(activity,NOT_DEFINED, tvOrientation);
//
////                    if(tvOrientation.getText()==PORTRAIT){
////                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
////                    }else {
////                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
////                    }
////                        tvRotation.setText(ROTATION_OFF);
//                    }
//                    break;
            }
        }
    };

    public static WebVideoHelper getInstnace() {
        return WebVideoHelperHolder.Instance;
    }

    private Activity activity;
    private ParentHomeFragment parentHomeFragment;
    public WebVideoHelper initWebVideo(final ParentHomeFragment parentHomeFragment, View parentView,
                                       RelativeLayout webViewContainer, VideoEnabledWebView urlWebView,
                                       final WebChromeListener listener) {

        this.urlWebView = urlWebView;
        this.webViewContainer = webViewContainer;
        this.parentView = parentView;
        this.activity=parentHomeFragment.getActivity();
        this.parentHomeFragment=parentHomeFragment;
        this.listener=listener;
        setReferences(activity, parentView);
        setfullScreenVideoLayout(activity);
        rlVideoLayout.setVisibility(View.GONE);
        loaderVideoView.setVisibility(View.GONE);

        Logger.logE(TAG, "setWebChromeClient() called");
        webChromeClient = new VideoEnabledWebChromeClient(webViewContainer, videoLayout, loaderVideoView, urlWebView) // See all available constructors...
        {

            // For Android 4.1+
            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                if(listener!=null)
                    listener.filechooserPre(uploadMsg);
                if(NativeHelper.getInstnace().checkActivity(activity))
                    fileChooseIntent(activity,true);
            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if(listener!=null)
                listener.filechooser(filePathCallback);
                if(NativeHelper.getInstnace().checkActivity(activity))
                fileChooseIntent(activity,false);
                return true;
//                return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
            }

            // Subscribe to standard events, such as onProgressChanged()...
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if(listener!=null){
                    listener.onProgressChanged(view, progress);
                }
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                if(listener!=null){ listener.onCreateWindow(view, isDialog, isUserGesture, resultMsg);}
                return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
            }
        };
        webChromeClient.setOnToggledFullscreen(new VideoEnabledWebChromeClient.ToggledFullscreenCallback()

                                               {
                                                   @Override
                                                   public void toggledFullscreen(boolean fullscreen) {
                                                       // Your code to handle the full-screen change, for example showing and hiding the title bar. Example:
                                                       if (fullscreen) {
                                                           setFullScreenVideo(activity);
                                                       } else {
                                                           exitFullScreenVideo(activity);
                                                       }
                                                   }
                                               }

        );
        urlWebView.setWebChromeClient(webChromeClient);

//        urlWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        return this;
    }
    public VideoEnabledWebChromeClient getWebChromeClient(){
        return webChromeClient;
    }
    private WebChromeListener listener;
    public interface WebChromeListener{
        void onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg);
        void onProgressChanged(WebView view, int newProgress);
        void filechooser(ValueCallback<Uri[]> filePathCallback);
        void filechooserPre(ValueCallback<Uri> filePathCallback);
        void onFullScreen();
        void onExitFullScreen();
    }

    private String getOrientation(Activity activity){
        if(NativeHelper.getInstnace().checkActivity(activity)){
            if(activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                return  LANDSCAPE;
            } if(activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                return PORTRAIT;
            }
        }
        return NOT_DEFINED;
    }
    public static boolean isPlayingVideo;
    private void setFullScreenVideo(Activity activity) {

        if(listener!=null){
            listener.onFullScreen();
        }
        isPlayingVideo=true;
        rlVideoLayout.setVisibility(View.VISIBLE);
        onClickOrientation(activity,getOrientation(activity) , tvOrientation);
        WindowManager.LayoutParams attrs = activity.getWindow().getAttributes();
        attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        activity.getWindow().setAttributes(attrs);
        //noinspection all
        activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
    }

    private void onClickOrientation(Activity activity, String orientation, TextView tvOrientation) {
        Logger.logE(TAG, "onClickOrientation() called");
        if (orientation.equalsIgnoreCase(NOT_DEFINED)) {

            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//                tvOrientation.setText(LANDSCAPE);
            } else if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                tvOrientation.setText(PORTRAIT);
            } else {
//                tvOrientation.setText(AUTO_ROTATION);
            }
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            return;
        }

        if (orientation.equalsIgnoreCase(PORTRAIT)) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
//            tvOrientation.setText(LANDSCAPE);
        } else {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//            tvOrientation.setText(PORTRAIT);
        }
    }

    private void fileChooseIntent(Activity activity,boolean pre){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        activity.startActivityForResult(intent, pre?AppConstants.PICKFILE_REQUEST_CODE_Pre:
                AppConstants.PICKFILE_REQUEST_CODE);
    }
    private void exitFullScreenVideo(Activity activity) {

        if(listener!=null){
            listener.onExitFullScreen();
        }
//        if (videoNavigation.getVisibility() == View.VISIBLE) {
//            videoNavigation.setVisibility(View.GONE);
//            animateVideoController(R.anim.slide_down, View.GONE);
//        }
        rlVideoLayout.setVisibility(View.GONE);
        parentHomeFragment.hideUrlHeader();
//        NativeHelper.showTitleBARs();
        WindowManager.LayoutParams attrs = activity.getWindow().getAttributes();
        attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        activity.getWindow().setAttributes(attrs);
        activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

//        GlobalConfig.isVideoNotPlaying = true;
        if (activity.getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        if(orientationToOrignal==null){
            orientationToOrignal = new Handler();
        }
        orientationToOrignal.postDelayed(runnable,2000);
    }
    private Handler orientationToOrignal;
    private Runnable runnable=new Runnable() {
        @Override
        public void run() {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }
    };

    private void setReferences(Activity activity, View parentView) {
        rlVideoLayout = (RelativeLayout) parentView.findViewById(R.id.rl_videolayout);
        videoLayout = (RelativeLayout) parentView.findViewById(R.id.videoLayout);
        // Your own view, read class comments
        loaderVideoView = activity.getLayoutInflater().inflate(R.layout.video_loadingview, null);

        ((ProgressBar) loaderVideoView.findViewById(R.id.pb_video_layout)).getIndeterminateDrawable().setColorFilter(
                ResourceHelper.getInstance().getColor(R.color.accent_color_0), PorterDuff.Mode.MULTIPLY);

    }

    private void setfullScreenVideoLayout(final Activity activity) {

//        lineSeparator = (View) rlVideoLayout.findViewById(R.id.v_separator);
//        videoNavigation = (RelativeLayout) rlVideoLayout.findViewById(R.id.rl_video_navigation);
//        tvOrientation = (TextView) rlVideoLayout.findViewById(R.id.tv_orientation);
//        tvRotation = (TextView) rlVideoLayout.findViewById(R.id.tv_rotation);
//        ivOrientation = (ImageView) rlVideoLayout.findViewById(R.id.iv_fullscreen_status);
//        ivPlay=(ImageView)rlVideoLayout.findViewById(R.id.iv_play);
//        ivPause=(ImageView)rlVideoLayout.findViewById(R.id.iv_pause);
//        ivPlay.setVisibility(View.GONE);
//        videoNavigation.setVisibility(View.GONE);
//        v_touch_area = (View) rlVideoLayout.findViewById(R.id.v_touch_area);
//        v_touch_area.setVisibility(View.GONE);
//        tvRotation.setText(ROTATION_OFF);
//        tvRotation.setVisibility(View.VISIBLE);
//        ivOrientation.setImageResource((R.drawable.fullscreen_exit));
//        ivOrientation.setOnClickListener(onClickListener);
//        tvOrientation.setOnClickListener(onClickListener);
//        tvRotation.setOnClickListener(onClickListener);
//        if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            tvOrientation.setText(LANDSCAPE);
//        } else if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//            tvOrientation.setText(PORTRAIT);
//        } else {
//            tvOrientation.setText(AUTO_ROTATION);
//        }
//        ivPause.setOnClickListener(controllClicks);
//        ivPlay.setOnClickListener(controllClicks);
//        webChromeClient.mediaPlayer.
//
//        v_touch_area.setOnTouchListener(new View.OnTouchListener() {
//            float initialPointX = 0;
//            float initialPointY = 0;
//            float variyingPoint = 0;
//            boolean validity = false;
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Display display = activity.getWindowManager().getDefaultDisplay();
//                final int width = display.getWidth();
//                final int heitht = display.getHeight();
//
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        initialPointY = event.getRawY();
//                        if (initialPointY >= heitht - 100 && webChromeClient != null && webChromeClient.isVideoFullscreen()) {
//                            v_touch_area.setVisibility(View.GONE);
//                            animateVideoController(activity, R.anim.enter_from_bottom, View.VISIBLE, 300);
//                        }
//                        break;
//                }
//                return false;
//            }
//        });
    }

    private void animateVideoController(final Context context, final int animation, int visibility, int duration) {

        visibility=View.GONE;
        Animation animation1 = AnimationUtils.loadAnimation(context, animation);
        animation1.setDuration(duration);
//        videoNavigation.setAnimation(animation1);
//        videoNavigation.setVisibility(visibility);

        if (visibility == View.VISIBLE) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation animation1 = AnimationUtils.loadAnimation(context, R.anim.enter_to_bottom);
                    animation1.setDuration(100);
                    if (webChromeClient.isVideoFullscreen()) {

//                        videoNavigation.setAnimation(animation1);
//                        videoNavigation.setVisibility(View.GONE);
//                        v_touch_area.setVisibility(View.VISIBLE);
//                        v_touch_area.setVisibility(View.GONE);
                    }
                }
            }, 4000);
        }
    }

    private static final class WebVideoHelperHolder {
        private static final WebVideoHelper Instance = new WebVideoHelper();
    }
}
