package com.finjan.securebrowser.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment;

/**
 *
 *
 *  Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 03/08/17.
 *
 * Top base activity that fire methods to make sure if app is in foreground or in background
 *
 */

public class ParentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(FinjanVPNApplication.getInstance()!=null){
            FinjanVPNApplication.getInstance().setIsMyAppInBackGround(ParentActivity.this,false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(GlobalVariables.getInstnace().isLaunchVpnAcivityCalled ){return;}
        if(FinjanVPNApplication.getInstance()!=null && !VpnHomeFragment.userCalledDisconnect){
            FinjanVPNApplication.getInstance().setIsMyAppInBackGround(ParentActivity.this,true);
        }
    }
}
