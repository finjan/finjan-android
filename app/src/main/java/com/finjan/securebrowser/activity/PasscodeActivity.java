package com.finjan.securebrowser.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import androidx.core.app.ActivityCompat;
import android.os.CancellationSignal;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.custom_views.PasskeyIconView;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 03/08/17.
 *
 * Class responsible for  pass code validation
 */

    public class PasscodeActivity extends ParentActivity implements View.OnClickListener {

    private ImageView iv_cicle1,iv_cicle2,iv_cicle3,iv_cicle4;
//    Preferences pref;
    private Vibrator vibe;
    Dialog dialog;
    Context context = this;

    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private static final String KEY_NAME = "touch_id";
    private Cipher cipher;
    private FingerprintManager.CryptoObject cryptoObject;

    private static int SCREEN_LOCK = 0;
    private static int FINGERPRINT = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passcode_orientation);
//        Fabric.with(this, new Crashlytics());
        setReferences();
        setDefaults();
        GoogleTrackers.Companion.setSPasscode();
//        LocalyticsTrackers.Companion.setSPasscode();
        setDialog();
        try {

            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

            if (!keyguardManager.isKeyguardSecure()&& UserPref.getInstance().getTouchId()) {
                dialog.dismiss();
                UserPref.getInstance().setTouchId(false);
            }

            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.USE_FINGERPRINT) !=
                    PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this,
                        getString(R.string.fingerprint_authentication_permission_not_enabled),
                        Toast.LENGTH_SHORT).show();
                return;
            }

            if(NativeHelper.getInstnace().isSupportFingurePrint()){
                fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                if (!fingerprintManager.hasEnrolledFingerprints()&& UserPref.getInstance().getTouchId()) {

                    dialog.dismiss();
                    UserPref.getInstance().setTouchId(false);
                }

                generateKey();

                if (cipherInit()) {
                    cryptoObject =
                            new FingerprintManager.CryptoObject(cipher);
                    FingerprintHandler helper = new FingerprintHandler(this);
                    helper.startAuth(fingerprintManager, cryptoObject);
                }
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }


        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }
    private void setDialog(){
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.touch_id);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        if(dialog.getWindow()!=null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout((7*width)/8, RelativeLayout.LayoutParams.WRAP_CONTENT);
        }

        TextView tv_canceltouchid = (TextView)dialog.findViewById(R.id.tv_canceltouchid);
        TextView tv_enterpass = (TextView)dialog.findViewById(R.id.tv_enterpass);

        tv_canceltouchid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tv_enterpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (UserPref.getInstance().getTouchId()){
            dialog.show();
        }
//        dialog.show();
    }

    private PasskeyIconView.PasskeyListener backKeylistener=new PasskeyIconView.PasskeyListener() {
        @Override
        public void onTouchKey() {
            if(GlobalVariables.getInstnace().isCheckingScreenLock && HomeActivity.getInstance()!=null){
                HomeActivity.getInstance().finish();
            }
            onBackPressed();
        }
    };
    private PasskeyIconView.PasskeyListener closeKeyListener=new PasskeyIconView.PasskeyListener() {
        @Override
        public void onTouchKey() {
            if (!TextUtils.isEmpty(UserPref.getInstance().getCheckLockCode()) &&
                    UserPref.getInstance().getCheckLockCode().trim().length() > 0) {
                UserPref.getInstance().setCheckLockCode(UserPref.getInstance().
                        getCheckLockCode().substring(0, UserPref.getInstance().getCheckLockCode().length() - 1));
            }
            setCirclesPasscode();
        }
    };
    private void onClickButtons(String value){
        UserPref.getInstance().setCheckLockCode(value);
        setCirclesPasscode();
        checkPasscode();
    }

    private void setReferences() {
        //<-------------Imageview Passcode Circles--------------->

        iv_cicle1 = (ImageView) findViewById(R.id.imageView8);
        iv_cicle2 = (ImageView) findViewById(R.id.imageView9);
        iv_cicle3 = (ImageView) findViewById(R.id.imageView10);
        iv_cicle4 = (ImageView) findViewById(R.id.imageView11);

        //<-------------Relative Layout Number Button Passcode--------------->
        (findViewById(R.id.rl_1)).setOnClickListener(this);
        (findViewById(R.id.rl_2)).setOnClickListener(this);
        (findViewById(R.id.rl_3)).setOnClickListener(this);
        (findViewById(R.id.rl_4)).setOnClickListener(this);
        (findViewById(R.id.rl_5)).setOnClickListener(this);
        (findViewById(R.id.rl_6)).setOnClickListener(this);
        (findViewById(R.id.rl_7)).setOnClickListener(this);
        (findViewById(R.id.rl_8)).setOnClickListener(this);
        (findViewById(R.id.rl_9)).setOnClickListener(this);
        (findViewById(R.id.rl_0)).setOnClickListener(this);
//        (findViewById(R.id.rl_x)).setOnClickListener(this);
//        (findViewById(R.id.rl_goback)).setOnClickListener(this);
        ((PasskeyIconView)(findViewById(R.id.iv_back))).setOnTouch(closeKeyListener,this);
        ((PasskeyIconView)(findViewById(R.id.iv_close))).setOnTouch(backKeylistener,this);
    }
    private void handleClick(View view){
        switch (view.getId()){
            case R.id.rl_1:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "1");
                break;
            case R.id.rl_2:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "2");
                break;
            case R.id.rl_3:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "3");
                break;
            case R.id.rl_4:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "4");
                break;
            case R.id.rl_5:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "5");
                break;
            case R.id.rl_6:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "6");
                break;
            case R.id.rl_7:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "7");
                break;
            case R.id.rl_8:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "8");
                break;
            case R.id.rl_9:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "9");
                break;
            case R.id.rl_0:
                onClickButtons(UserPref.getInstance().getCheckLockCode() + "0");
                break;
        }
    }


    private void setDefaults(){
        if(solidCircle==null){
            solidCircle= ResourceHelper.getInstance().getDrawable(R.drawable.pin_fill);
        }
        if(emptyCircle==null){
            emptyCircle=ResourceHelper.getInstance().getDrawable(R.drawable.pin_blank);
        }
        UserPref.getInstance().setCheckLockCode("");
    }

    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to setPromoList Cipher", e);
        }
    }

    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            keyGenerator = KeyGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES,
                    "AndroidKeyStore");
        } catch (NoSuchAlgorithmException |
                NoSuchProviderException e) {
            throw new RuntimeException(
                    "Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            //Api 23 handled as it is not possible to come there without api 23 or above
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onClick(View v) {
        handleClick(v);
    }

    public class FingerprintHandler extends
            FingerprintManager.AuthenticationCallback {

        private CancellationSignal cancellationSignal;
        private Context appContext;

        public FingerprintHandler(Context context) {
            appContext = context;
        }
        public void startAuth(FingerprintManager manager,
                              FingerprintManager.CryptoObject cryptoObject) {

            cancellationSignal = new CancellationSignal();

            if (ActivityCompat.checkSelfPermission(appContext,
                    Manifest.permission.USE_FINGERPRINT) !=
                    PackageManager.PERMISSION_GRANTED) {
                return;
            }
            manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }
        @Override
        public void onAuthenticationError(int errMsgId,
                                          CharSequence errString) {
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId,
                                         CharSequence helpString) {
            Toast.makeText(appContext,
                    "Authentication help\n" + helpString,
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onAuthenticationFailed() {
            Toast.makeText(appContext,
                    getString(R.string.authentication_failed),
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onAuthenticationSucceeded(
                FingerprintManager.AuthenticationResult result) {

            if(UserPref.getInstance().getTouchId()) {
                Toast.makeText(appContext,
                        getString(R.string.authentication_succeeded),
                        Toast.LENGTH_SHORT).show();
                GlobalVariables.getInstnace().isCheckingScreenLock=false;
                if(checkingPassword){
                    ScreenNavigation.getInstance().initLastScreen(PasscodeActivity.this,"");
                }else {
                    ScreenNavigation.getInstance().callHomeActivity(PasscodeActivity.this,"");
//                NativeHelper.getInstnace().callHomeActivity(PasscodeActivity.this);
                }
                finish();
            }
        }
    }

    public void checkPasscode(){
        String checkLockcode= UserPref.getInstance().getCheckLockCode();
        String lockCode=UserPref.getInstance().getLockCode();

        if(!TextUtils.isEmpty(checkLockcode) && checkLockcode.length()==4){
            if(checkLockcode.equals(lockCode)){
                Toast.makeText(getApplicationContext(),getString(R.string.correct_passcode),Toast.LENGTH_SHORT).show();
                if(checkingPassword){
                    ScreenNavigation.getInstance().initLastScreen(PasscodeActivity.this,"");
                }else {
                    ScreenNavigation.getInstance().callHomeActivity(PasscodeActivity.this,"");
//                NativeHelper.getInstnace().callHomeActivity(PasscodeActivity.this);
                }
                GlobalVariables.getInstnace().isCheckingScreenLock=false;
                finish();
            }else {
                Toast.makeText(getApplicationContext(),getString(R.string.wrong_passcode),Toast.LENGTH_SHORT).show();
                vibe.vibrate(100);
                iv_cicle1.setBackground(emptyCircle);
                iv_cicle2.setBackground(emptyCircle);
                iv_cicle3.setBackground(emptyCircle);
                iv_cicle4.setBackground(emptyCircle);
                UserPref.getInstance().setCheckLockCode("");
            }
        }
    }

    private Drawable solidCircle,emptyCircle;
    public void setCirclesPasscode(){

        String checkLockCode=UserPref.getInstance().getCheckLockCode();
        if(checkLockCode==null){
            return;
        }

        checkLockCode=checkLockCode.trim();
        int codeLenght=checkLockCode.length();

        switch (codeLenght){
            case 1:
                iv_cicle1.setBackground(solidCircle);
                iv_cicle2.setBackground(emptyCircle);
                iv_cicle3.setBackground(emptyCircle);
                iv_cicle4.setBackground(emptyCircle);
                break;
            case 2:
                iv_cicle1.setBackground(solidCircle);
                iv_cicle2.setBackground(solidCircle);
                iv_cicle3.setBackground(emptyCircle);
                iv_cicle4.setBackground(emptyCircle);
                break;
            case 3:
                iv_cicle1.setBackground(solidCircle);
                iv_cicle2.setBackground(solidCircle);
                iv_cicle3.setBackground(solidCircle);
                iv_cicle4.setBackground(emptyCircle);

                break;
            case 4:

                iv_cicle1.setBackground(solidCircle);
                iv_cicle2.setBackground(solidCircle);
                iv_cicle3.setBackground(solidCircle);
                iv_cicle4.setBackground(solidCircle);
                break;
            case 0:
                iv_cicle1.setBackground(emptyCircle);
                iv_cicle2.setBackground(emptyCircle);
                iv_cicle3.setBackground(emptyCircle);
                iv_cicle4.setBackground(emptyCircle);
                break;
        }
    }

    private boolean checkingPassword;
    @Override
    protected void onResume() {
        super.onResume();
        try {
            checkingPassword=(PasscodeActivity.this.getIntent()!=null
                    && PasscodeActivity.this.getIntent().getBooleanExtra(ScreenNavigation.CHECKING_PASSWORD,false));
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

            if (!keyguardManager.isKeyguardSecure() && UserPref.getInstance().getTouchId()) {

                dialog.dismiss();
                UserPref.getInstance().setTouchId(false);
            }

            if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.USE_FINGERPRINT) !=
                    PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.fingerprint_authentication_permission_not_enabled),
                        Toast.LENGTH_SHORT).show();
                return;
            }

            if(NativeHelper.getInstnace().isSupportFingurePrint()){
                fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                if (!fingerprintManager.hasEnrolledFingerprints() && UserPref.getInstance().getTouchId()) {

                    dialog.dismiss();
                    UserPref.getInstance().setTouchId(false);
                }

                generateKey();

                if (cipherInit()) {
                    cryptoObject =
                            new FingerprintManager.CryptoObject(cipher);
                    FingerprintHandler helper = new FingerprintHandler(getApplicationContext());
                    helper.startAuth(fingerprintManager, cryptoObject);
                }
            }


        }
        catch (Exception e){
            e.printStackTrace();
        }

}
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FINGERPRINT){
            if(checkingPassword){
                ScreenNavigation.getInstance().initLastScreen(PasscodeActivity.this,"");
            }else {
                ScreenNavigation.getInstance().callHomeActivity(PasscodeActivity.this,"");
//                NativeHelper.getInstnace().callHomeActivity(PasscodeActivity.this);
            }
            finish();
        }
        else if(requestCode == SCREEN_LOCK && UserPref.getInstance().getTouchId()){
            try {

                keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

                if (!keyguardManager.isKeyguardSecure()) {

                    Toast.makeText(this,
                            getString(R.string.lock_screen_security_not_enabled_in_settings),
                            Toast.LENGTH_SHORT).show();
                    startActivityForResult(new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD),SCREEN_LOCK);
                    return;
                }

                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.USE_FINGERPRINT) !=
                        PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this,
                            getString(R.string.fingerprint_authentication_permission_not_enabled),
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if(NativeHelper.getInstnace().isSupportFingurePrint()){
                    fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                    if (!fingerprintManager.hasEnrolledFingerprints()) {

                        UserPref.getInstance().setTouchId(true);

                        // This happens when no fingerprints are registered.
                        Toast.makeText(this,
                                "Register at least one fingerprint in Settings",
                                Toast.LENGTH_SHORT).show();
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS), FINGERPRINT);
                        // finish();
                        return;
                    }

                    generateKey();

                    if (cipherInit()) {
                        cryptoObject =
                                new FingerprintManager.CryptoObject(cipher);
                        FingerprintHandler helper = new FingerprintHandler(this);
                        helper.startAuth(fingerprintManager, cryptoObject);
                    }
                }


            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        else {
            ScreenNavigation.getInstance().callHomeActivity(PasscodeActivity.this,"");
//            NativeHelper.getInstnace().callHomeActivity(PasscodeActivity.this);
            finish();
        }
    }
}
