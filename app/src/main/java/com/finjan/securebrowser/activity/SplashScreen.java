package com.finjan.securebrowser.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.avira.common.id.HardwareId;
//import com.crashlytics.android.Crashlytics;
import com.electrolyte.sociallogin.socialLogin.FbSignInHandler;
import com.finjan.securebrowser.Appirater;
import com.finjan.securebrowser.Constants;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.helpers.DateHelper;
import com.finjan.securebrowser.helpers.DisplayHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.security.WifiSecurityHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.ModelManager;
import com.finjan.securebrowser.parser.PList;
import com.finjan.securebrowser.parser.PListParser;
import com.finjan.securebrowser.service.NotifyUnSecureNetwork;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.util.FinjanUrls;
import com.finjan.securebrowser.util.NotificationHandler;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.receiver.WifiConnectionReceiver;
import com.finjan.securebrowser.vpn.subscriptions.SubscriptionHelper;
import com.finjan.securebrowser.vpn.ui.main.VpnApisHelper;
import com.finjan.securebrowser.vpn.util.FinjanVpnPrefs;
import com.finjan.securebrowser.vpn.util.PermissionUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.security.ProviderInstaller;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
//import com.localytics.android.Localytics;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

//import io.fabric.sdk.android.Fabric;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved
 * Developed by NewOfferings, LLC.
 *
 * screen responsible for showing Splash screen with bull animation and downloading validating if cached
 * plist is available/valid or not.
 *
 */


public class SplashScreen extends ParentActivity implements ProviderInstaller.ProviderInstallListener {

    private static final String TAG = SplashScreen.class.getSimpleName();
    private static int SPLASH_TIME_OUT = 1000;
    final int READ_PHONE_STATE_PERMISSION = 3;
    private final int LAUNCH = 0;
    private final int TRACKER = 1;
    private int responseCode = 0;
    private ViewGroup vg_loader;
    private int REQUEST_TYPE;
//    private ConnectToServer asyncTask;
    private boolean isBackPressedCalled = false;
    private boolean isOnStopCalled;

    private boolean testingBuild=false;
    private boolean showingPlayserviceDiaglog = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,"onCreate");
        com.electrolyte.utils.DateHelper.getInstnace();
        com.electrolyte.logger.Logger.Companion.initLogger(AppConfig.Companion.getDebugReporting());
        FbSignInHandler.Companion.initLib(this);
        ProviderInstaller.installIfNeededAsync(this, this);
        System.gc();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);
        NativeHelper.getInstnace().currentAcitvity=this;
        setReference();
        newUi();
        checkIfCashedPlistAndLottieAnimation();
        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_to_left);
        setLocalytics();
        setJugad();
        isAppIsUpgraded();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this,new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                Logger.logE("token",token);

//                    Localytics.setPushRegistrationId(token);
//                    Localytics.registerPush();
//                    Logger.logE("Localytics version using", " " + Localytics.getLibraryVersion());


            }
        });
//        String token = FirebaseInstanceId.getInstance().getToken();
//        Localytics.setPushRegistrationId(token);
//        Localytics.registerPush();
        GoogleTrackers.Companion.setSConfigLoadingTracking();
        showingPlayserviceDiaglog = !isGooglePlayServicesAvailable(this);
        VpnApisHelper.Companion.getInstance().refreshApis();
        SubscriptionHelper.getInstance().setSubscriptionFetched(false);
        handleDynamicDeepLinking();
//        boolean retyTest = DateHelper.getInstnace().isMoreThanDaysDiff(1, System.currentTimeMillis(),"2018-06-11 12:03:49");
    }

    private void checkIfCashedPlistAndLottieAnimation(){
        if(!TextUtils.isEmpty(UserPref.getInstance().getLastSplashAnim()) &&
                Utils.isAssetExists(UserPref.getInstance().getLastSplashAnim(),"json")){
         iv_splash_bull.setAnimation(UserPref.getInstance().getLastSplashAnim()+".json");
        }
    }
    private static final int ERROR_DIALOG_REQUEST_CODE = 1;


    /**
     * This method is only called if the provider is successfully updated
     * (or is already up-to-date).
     */
    @Override
    public void onProviderInstalled() {
        // Provider is up-to-date, app can make secure network calls.
        Logger.logE(TAG,"onProviderInstalled");
    }

    /**
     * This method is called if updating fails; the error code indicates
     * whether the error is recoverable.
     */
    @Override
    public void onProviderInstallFailed(int errorCode, Intent recoveryIntent) {
        // we have nothing to do
        Logger.logE(TAG,"onProviderInstallFailed");
    }


    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            String msg = getString(R.string.no_playservice);
            if(status == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED){
                msg= getString(R.string.play_service_outdated);
            }
            if(googleApiAvailability.isUserResolvableError(status)) {

                final String LINK_TO_GOOGLE_PLAY_SERVICES = "play.google.com/store/apps/details?id=com.google.android.gms&hl=en";

                new FinjanDialogBuilder(this).setMessage(msg).
                        setPositiveButton(getString(R.string.alert_ok)).setNegativeButton(getString(R.string.alert_cancel))
                        .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                            @Override
                            public void onPositivClick() {
                                dismissUpdateDialog();
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://" + LINK_TO_GOOGLE_PLAY_SERVICES)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                }
                            }
                        }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                    @Override
                    public void onNegativeClick() {
                        dismissUpdateDialog();
                    }
                }).show();
            }
            return false;
        }
        return true;
    }
    private void dismissUpdateDialog(){
        showingPlayserviceDiaglog = false;
        if(wasUpdaingUI){
            wasUpdaingUI =false;
            updateUI();
        }
    }


    private void isAppIsUpgraded(){
        if(UserPref.getInstance().isOldApp()){
//            LocalyticsTrackers.Companion.getInstance().addAppsUpgradedApp();
            UserPref.getInstance().setOldApp();
        }
    }

    private void setJugad(){
//        getIntent().putExtra("ll_deep_link_url","invincibull://identifier=about");
    }

    private void setLocalytics(){
        LocalyticsTrackers.Companion.setBorrowerUser();
    }

    private void setDefaults(){
        NativeHelper.getInstnace().changeStatusBarColor(R.color.col_status,this);
        setNetworkTypeObj();
        initHit();
        NotifyUnSecureNetwork.startServiceWhenRequired(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(!FinjanVpnPrefs.getTempDeviceIdSet(SplashScreen.this)){
                    FinjanVpnPrefs.setTempDeviceId(SplashScreen.this,System.currentTimeMillis());
                    FinjanVpnPrefs.setTempDeviceIdSet(SplashScreen.this,true);
                }
            }
        }).start();

    }
//    private void tempLogException(){
//        Fabric.with(FinjanVPNApplication.getInstance().getApplicationContext(), new Crashlytics());
//    }

    private LinearLayout ll_caption;
    private LottieAnimationView iv_splash_bull;

    private void newUi(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ll_caption.setVisibility(View.VISIBLE);
            }
        },10);
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
//            Localytics.onNewIntent(this, intent);

    }

    private void testingBuild(){
        if(testingBuild){
            new FinjanDialogBuilder(this).
                    setMessage("Please select one option whether you want test normal app or select 'Testing Build' " +
                            "to add random deviceId for get 500 MB")
                    .setPositiveButton("Testing Build")
                    .setNegativeButton("Normal App")
                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                        @Override
                        public void onPositivClick() {
                            HardwareId.Def_unlimited_user=true;
                            setDefaults();
                        }
                    }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                @Override
                public void onNegativeClick() {
                    HardwareId.Def_unlimited_user=false;
                    setDefaults();
                }
            }).setCancelable(false).outSideClickable(false).show();
        }else {
            setDefaults();
        }
    }



    private void setNetworkTypeObj(){
        if(PermissionUtil.hasLocationPermission(this)){
            WifiSecurityHelper.getInstance().getNetworkType(this);
            WifiConnectionReceiver.initSmartScan(SplashScreen.this.getApplicationContext());
        }
    }
    public void showNetworkError(){
        if (this.isFinishing() || isDestroyed()){
            return;
        }
        FinjanDialogBuilder.networkErrorDialog(SplashScreen.this, new FinjanDialogBuilder.PositiveClickListener() {
            @Override
            public void onPositivClick() {
                GlobalVariables.getInstnace().isFinishCalled = true;
                SplashScreen.this.finish();
            }
        });
    }
    private void serverError(){
        if (this.isFinishing() || isDestroyed()){
            return;
        }
        FinjanDialogBuilder.connectionErrorDialog(SplashScreen.this, new FinjanDialogBuilder.PositiveClickListener() {
            @Override
            public void onPositivClick() {
                GlobalVariables.getInstnace().isFinishCalled = true;
                SplashScreen.this.finish();
            }
        });
    }
    private void initHit(){
        Intent intent = getIntent();
        if ((intent != null) && (intent.getExtras() != null) && intent.getExtras().getBoolean("fromNotification")) {
            finish();
        }
        GlobalVariables.getInstnace().lastDirectUrl =(intent != null && intent.getData() != null) ? intent.getData().toString() : "";

        if (Utils.isNetworkConnected(SplashScreen.this)) {
                fetchConfigList();
            } else {
                showNetworkError();
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {


            case READ_PHONE_STATE_PERMISSION:
//                if (grantResults.length >= 0 &&grantResults.length>0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                    initUpdateCountService();
//                    if (Utils.isNetworkConnected(SplashScreen.this)) {
//                        if(GlobalVariables.autoWifiConnect){
//                            new FinjanDialogBuilder(this).setMessage(getString(R.string.we_need_you_location_in_order_check_weather_connection_secure))
//                                    .setPositiveButton(getString(R.string.alert_ok))
//                                    .setNegativeButton(getString(R.string.alert_cancel))
//                                    .outSideClickable(false).setCancelable(false)
//                                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
//                                        @Override
//                                        public void onPositivClick() {
//                                            if (ActivityCompat.checkSelfPermission(SplashScreen.this,
//                                                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                                                ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, AppConstants.LOCATION_PERMISSION);
//                                            }
//                                        }
//                                    }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
//                                @Override
//                                public void onNegativeClick() {
//                                    fetchConfigList();
//                                }
//                            }).show();
//                        }else {
//                            fetchConfigList();
//                        }
//                } else {
//                        showNetworkError();
//                }
//
//                } else if (permissions.length>0 &&
//                        ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissions[0])) {
//                    showPermissionSettingDialog();
//                }
//                break;
//            case AppConstants.LOCATION_PERMISSION:
//                if (grantResults.length >= 0 &&grantResults.length>0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if (Utils.isNetworkConnected(SplashScreen.this)) {
//                        fetchConfigList();
//                        WifiConnectionReceiver.initSmartScan(SplashScreen.this.getApplicationContext());
//
//                    } else {
//                        showNetworkError();
//                    }
//
//                } else if (permissions.length>0 &&
//                        ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissions[0])) {
//                    new FinjanDialogBuilder(SplashScreen.this).setMessage(getString(R.string.we_need_you_location_later_turn_on))
//                            .outSideClickable(false).setCancelable(false).setPositiveButton(getString(R.string.alert_ok))
//                            .setNegativeButton(getString(R.string.alert_cancel))
//                            .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
//                                @Override
//                                public void onPositivClick() {
//                                    showPermissionSettingDialog();
//                                }
//                            })
//                            .show();
//                }
                break;
        }

    }
//    private void showPermissionSettingDialog(){
//        FinjanDialogBuilder.showPermissionSettingDialog(SplashScreen.this,new FinjanDialogBuilder.PositiveClickListener() {
//            @Override
//            public void onPositivClick() {
//                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
//                        Uri.fromParts("package", getPackageName(), null));
//                startActivityForResult(intent,101);
//            }
//        });
//    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            testingBuild();
        }
    };
    private void handleDynamicDeepLinking(){

        // Check for App Invite invitations and launch deep-link activity if possible.
        // Requires that an Activity is registered in AndroidManifest.xml to handle
        // deep-link URLs.

        Log.e(TAG,"handleDynamicDeepLinking");
        final Handler handler = new Handler();
        handler.postDelayed(runnable,3000);
        FirebaseDynamicLinks.getInstance().getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData data) {
                        Log.e(TAG,"handleDynamicDeepLinking onSuccess "+data);
                        handler.removeCallbacks(runnable);
                        if (data == null) {
                            Logger.logE(TAG, "getInvitation: no data");
                            testingBuild();
                            return;
                        }

                        // Get the deep link
                        Uri deepLink = data.getLink();
                        Logger.logE(TAG, "deeplin is "+ deepLink.toString());
                        if(!TextUtils.isEmpty(deepLink.toString())){
                            String modifiedDlink = deepLink.toString();
                            modifiedDlink = modifiedDlink.replace("https://invincibull.page.link/","invincibull://identifier=");
                            Logger.logE(TAG, "deeplin is "+ modifiedDlink);
                            getIntent().putExtra("ll_deep_link_url",modifiedDlink);
                        }
                        testingBuild();
                        // Extract invite (no need right now)
//                        FirebaseAppInvite invite = FirebaseAppInvite.getInvitation(data);
//                        if (invite != null) {
//                            String invitationId = invite.getInvitationId();
//                            Logger.logE(TAG, "deeplin is "+ invitationId);
//                        }

                        // Handle the deep link
                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        handler.removeCallbacks(runnable);
                        Log.e(TAG,"handleDynamicDeepLinking onFailure "+e);
                        Logger.logE(TAG, "getDynamicLink:onFailure"+e);
                        testingBuild();
                    }
                });

    }

    private void setReference(){
        vg_loader = (ViewGroup) findViewById(R.id.vg_loader);
        iv_splash_bull= (LottieAnimationView) findViewById(R.id.iv_splash_bull);
        ll_caption= (LinearLayout) findViewById(R.id.ll_caption);
    }

    private void fetchConfigList(){
        if(NotificationHandler.Companion.getInstance().isDeepLink(getIntent())){
            PlistHelper.setUpdatePlist();
            updateUI();
            return;
        }
        if(AppConfig.Companion.getLoadPlistOnce()){
            if(PlistHelper.PlistConfig.shouldUpdatePlist()){
                fetchMainConfig();
                SPLASH_TIME_OUT = 10;
                return;
            }else {
                PlistHelper.setUpdatePlist();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateUI();
                    }
                },3000);
            }

            return;
        }else {
            fetchMainConfig();
        }
    }
    private void fetchMainConfig(){
      /*  long lastTimeHit = UserPref.getInstance().getChecheAvilable(AppConstants.PLIST);
        if (System.currentTimeMillis() - lastTimeHit < AppConstants.interValHit)
        {
            EventManeger.getInstance().initialisingCache(0);
            com.finjan.securebrowser.vpn.controller.network.eventManeger.Response response = EventManeger.getInstance().getCacheValue(AppConstants.PLIST);
            if (response!=null)
            {

                    parseResponse(response.getResponseText());
                    return ;

            }


        }*/


        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                REQUEST_TYPE = LAUNCH;
                asyncTask = new ConnectToServer();
                asyncTask.execute(FinjanUrls.URL_LAUNCH);
            }
        }, SPLASH_TIME_OUT);
    }
    private ConnectToServer asyncTask;

    @Override
    protected void onStart() {
        super.onStart();
    }

    private String getResponse(String URL) throws IOException {
        StringBuffer response = null;
        String result = "";
        String url = URL;
        URL obj = new URL(url);
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
            // optional default is GET
            con.setRequestMethod("GET");
            con.setDoOutput(true);
            con.setConnectTimeout(60 * 2000);
            responseCode = con.getResponseCode();
            Logger.logE(TAG, "\nSending 'GET' request to URL : " + url);
            Logger.logE(TAG, "Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            result = response.toString();
//          parseResponse(result);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    private void manageNotificationChannel() {
        if (FirebaseMessaging.getInstance() != null && !UserPref.getInstance().getAutoSubscribe()) {
            String subs = Utils.getNotificationPlistvalue(Constants.KEY_NOTIFICATION, Constants.KEY_CHANNELS, Constants.KEY_AUTO_SUBSCRIBE);
            if (!TextUtils.isEmpty(subs) && subs.equalsIgnoreCase("yes") || subs.equalsIgnoreCase("true")) {
                String topic = Utils.getNotificationPlistvalue(Constants.KEY_NOTIFICATION, Constants.KEY_CHANNELS, Constants.KEY_KEY);
                if (!TextUtils.isEmpty(topic)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(topic);
                    UserPref.getInstance().setAutoSubscribe(true);
//                    pref.setBoolean(Constants.KEY_AUTO_SUBSCRIBE,true);
                }
            }
        }
    }

    private void handelNetworkError(){
        if(NativeHelper.getInstnace().checkActivity(this)){
            new FinjanDialogBuilder(SplashScreen.this).setMessage(getString(R.string.unable_to_connect_try_again_after_some_time))
                    .setPositiveButton(getString(R.string.alert_ok)).setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                @Override
                public void onPositivClick() {
                    SplashScreen.this.finish();
                }
            }).show();
        }
    }

    private void loadTracker() {

            REQUEST_TYPE = TRACKER;
//        if (Utils.getPlistvalue(Constants.KEY_TRACKER_OPTIONS, Constants.KEY_TRACKER_ENABLE).equalsIgnoreCase("true")) {
            if (PlistHelper.getInstance().getIsTrackerEnabled()) {
//                    final String fileLocation = Utils.getPlistvalue(Constants.KEY_TRACKER_OPTIONS, Constants.KEY_TRACKER_FILE_LOCATION);
                final String fileLocation = PlistHelper.getInstance().getTrackerFileLocation();

                if (Utils.isNetworkConnected(SplashScreen.this)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (asyncTask != null /*&& !Thread.interrupted()*/) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            asyncTask = new ConnectToServer();
                                            asyncTask.execute(fileLocation);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }

                        }
                    }, SPLASH_TIME_OUT);
                } else {
                    handelNetworkError();
                }
            }else {
                UserPref.getInstance().setTracker(false);
                parseResponse("");
            }
    }

    private boolean wasUpdaingUI = false;

    private void updateUI() {

        Log.e(TAG,"updateUI "+isBackPressedCalled+" -- isOnStopCalled "+isOnStopCalled+" -- showingPlayserviceDiaglog "+showingPlayserviceDiaglog);
        if (isBackPressedCalled || isOnStopCalled) {
            Logger.logE(TAG, "sorry application is in background");
            finish();
            return;
        }
        if(showingPlayserviceDiaglog){
            wasUpdaingUI = true;
            return;
        }
        NotificationHandler.Companion.getInstance().init(this,getIntent());
        if(!NotificationHandler.Companion.getInstance().processIntent()){
            if (!UserPref.getInstance().getIsTutorialScreenShowed()) {
                startActivity(new Intent(SplashScreen.this, TutorialActivity.class));
            } else {
                ScreenNavigation.getInstance().startHome(SplashScreen.this,"");
            }
        }
        Appirater.significantEvent(SplashScreen.this);
        HardwareId.setUnlimitedUser(PlistHelper.getInstance().isShouldGenerateRandomId());
        finish();
    }

    public void manageApplciation() {
        try {
            checkAppVersion();
        } catch (Exception e) {
            updateUI();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOnStopCalled = false;
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,false);
    }

    public void checkAppVersion() {
//        String serverVersionNameString = Utils.getPlistvalue(Constants.KEY_APP_VERSION, Constants.KEY_ANDROID);
        String serverVersionNameString = PlistHelper.getInstance().getAppVersion();
        String currentVerisonName=getVersionName();

        if(!TextUtils.isEmpty(serverVersionNameString) && !TextUtils.isEmpty(currentVerisonName)){
            serverVersionNameString=serverVersionNameString.replaceAll("\\.","");
            currentVerisonName=currentVerisonName.replaceAll("\\.","");
            int sVersion=0;
            int aVersion=0;

            try {
                sVersion=Integer.parseInt(serverVersionNameString);
                aVersion=Integer.parseInt(currentVerisonName);
                if(sVersion>aVersion){
                    final String updateMesssage = PlistHelper.getInstance().getUpdateMessage();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showForceUpdateDialog(updateMesssage);
                        }
                    });

                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateUI();
                    }
                });


            }catch (NumberFormatException e){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateUI();
                    }
                });

            }
        }else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateUI();
                }
            });

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        FinjanVPNApplication.getInstance().setIsMyAppInBackGround(this,true);
        isOnStopCalled = true;
    }

    private void showForceUpdateDialog(String message) {

        if (!this.isFinishing() && !this.isDestroyed()) {
            new FinjanDialogBuilder(this).setMessage(message)
                    .setPositiveButton(getString(R.string.alert_yes))
                    .setNegativeButton(getString(R.string.alert_no))
                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                        @Override
                        public void onPositivClick() {
                            try {
                                String url = PlistHelper.getInstance().getAppUrl();
                                if (TextUtils.isEmpty(url))
                                    return;

                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        SplashScreen.this.finish();
                                    }
                                }, 3000);
                            } catch (Exception e) {
                            }
                        }
                    }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                @Override
                public void onNegativeClick() {
                    SplashScreen.this.finish();
                }
            }).setCancelable(false).outSideClickable(false).show();
        }
    }

    public String getVersionName() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (pInfo != null) {
//            return "1";
            return pInfo.versionName;
        }
        return "";
    }

    @Override
    public void onBackPressed() {
        isBackPressedCalled = true;
        if (asyncTask != null) {
            asyncTask.cancel(true);
            asyncTask = null;
        }
        finish();
        if (HomeActivity.getInstance() != null) {
            HomeActivity.getInstance().finish();
        }
        super.onBackPressed();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode==101 ){
//            if(resultCode==RESULT_OK){
//                if(data!=null) {
//                    initHit();
//                }
//            }else {
//                showDialogToExitSplash();
//        }
//        }
    }
    private void showDialogToExitSplash(){
        new FinjanDialogBuilder(SplashScreen.this)
                .setMessage(getString(R.string.user_forcefully_disable_a_permission))
                .setPositiveButton("Exit").setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
            @Override
            public void onPositivClick() {
                SplashScreen.this.finish();
            }
        }).show();
    }
     private void parseLaunchResponse(final String response){
         if(response!=null){
             try {
                 PListParser pListParser = new PListParser();
                 pListParser.parse(new ByteArrayInputStream(response.getBytes()));
                 final PList pList = pListParser.current_element;
                 if (pList != null) {
                     ModelManager.getInstance().setPList(pList);
                 }
                 new Thread(new Runnable() {
                     @Override
                     public void run() {
                         PlistHelper.getInstance().setParentPlist(pList);
                         PlistHelper.PlistConfig.updateConfigToLocal(response,true);
                         if(PlistHelper.getInstance().getShowFestiveAnim()){
                         String val =PlistHelper.getInstance().showLottieKey();
                         if(Utils.isAssetExists("bull_superman_spinning_"+val,"json")){
                             UserPref.getInstance().setLastSplashAnim("bull_superman_spinning_"+val);
                         }
                     }else {
                         UserPref.getInstance().setLastSplashAnim("bull_superman_spinning");
                     }
                         UserPref.getInstance().setPlistVersion(FinjanUrls.URL_LAUNCH);
                 }
                 }).start();
                 new Handler().postDelayed(new Runnable() {
                     @Override
                     public void run() {
                         loadTracker();
                     }
                 },500);
             } catch (Exception e) {
                 e.printStackTrace();
                 updateUI();
             }
         }else{
             serverError();
         }
     }

    private void parseResponse(final String result) {
        switch (REQUEST_TYPE) {
            case LAUNCH:
                parseLaunchResponse(result);
                break;
            case TRACKER:

                    if(!TextUtils.isEmpty(result)){
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    PlistHelper.PlistConfig.updateConfigToLocal(result,false);
                                    ModelManager.getInstance().setTrackerModel(result);
                                } catch (Exception e) {
                                    Logger.logE(TAG,"unable to fetch tracker api "+e.getLocalizedMessage()+" resposne "+result);
                                    serverError();
                                }
                                manageNotificationChannel();
                                manageApplciation();
                            }
                        }).start();

                    }


                break;
        }
    }
    private class ConnectToServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {}

        @Override
        protected String doInBackground(String... params) {

            try {
                if(REQUEST_TYPE==LAUNCH){
                    DisplayHelper.getInstance().setDisplay(SplashScreen.this);
                }
                return getResponse(params[0]);
            } catch (MalformedURLException e) {
                Logger.logE(TAG,"exception getting display");
                Logger.logE(TAG,"unable to fetch tracker api "+e.getMessage()+" resposne "+params[0]);
                serverError();
            } catch (IOException e) {
                Logger.logE(TAG,"exception getting display");
                Logger.logE(TAG,"unable to fetch tracker api "+e.getMessage()+" resposne "+params[0]);
                serverError();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String value) {
            super.onPostExecute(value);
            vg_loader.setVisibility(View.GONE);
            try {
                if (TextUtils.isEmpty(value)) {
                    serverError();
/*
                        EventManeger.getInstance().initialisingCache(0);
                        com.finjan.securebrowser.vpn.controller.network.eventManeger.Response response = EventManeger.getInstance().getCacheValue(AppConstants.PLIST);
                        if (response!=null)
                        {

                            parseResponse(response.getResponseText());


                        }*/



                } else {

                  /*  EventManeger.getInstance().initialisingCache(0);
                    com.finjan.securebrowser.vpn.controller.network.eventManeger.Response response1 = new com.finjan.securebrowser.vpn.controller.network.eventManeger.Response();
                    response1.setResponseText(value);
                    EventManeger.getInstance().putValuesInCache(response1,AppConstants.PLIST,0);
                    UserPref.getInstance().setChecheAvilable(AppConstants.PLIST,System.currentTimeMillis());
*/
                    parseResponse(value);
                }
            } catch (Exception e) {
                e.printStackTrace();
               /* EventManeger.getInstance().initialisingCache(0);
                com.finjan.securebrowser.vpn.controller.network.eventManeger.Response response = EventManeger.getInstance().getCacheValue(AppConstants.PLIST);
                if (response!=null)
                {

                    parseResponse(response.getResponseText());


                }*/
            }
        }


        @Override
        protected void onCancelled() {
            vg_loader.setVisibility(View.GONE);
            checkAppVersion();
        }
    }

}
