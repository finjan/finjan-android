package com.finjan.securebrowser.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.fragment.FragmentNormalWebView;
import com.finjan.securebrowser.fragment.ParentFragment;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;

import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_ABOUT;
import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_EULA;
import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_HELP;
import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_PASSWORD_RESET;
import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_PRIVATE_PRIVACY;

/**
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on  17/08/17.
 *
 * Activity for normal webview that can have url provided in intent
 */

public class NormaWebviewActivity extends AppCompatActivity {



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_normal_webview);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setFramgent();
    }

    private void setFramgent(){
        if(getIntent()!=null){
            setWebviewFragment(getIntent().getStringExtra(AppConstants.BKEY_SCREEN));
        }
    }

    private void setWebviewFragment(String drawerConst) {
        if(TextUtils.isEmpty(drawerConst)){
            return;
        }
        String url="";
        String  title="";
        switch (drawerConst){
            case DKEY_EULA:
                url= PlistHelper.getInstance().getEulaUrl();
                title=getString(R.string.end_user_license_agreement);
                break;
            case DKEY_ABOUT:
                url=PlistHelper.getInstance().getAboutUrl();
                title=getString(R.string.about);
                GoogleTrackers.Companion.setSAbout();
//                LocalyticsTrackers.Companion.setSAbout();
                break;
            case DKEY_HELP:
                url=PlistHelper.getInstance().getHelpUrl();
                title=getString(R.string.help);
                GoogleTrackers.Companion.setSHelp();
//                LocalyticsTrackers.Companion.setSHelp();
                break;
            case DKEY_PASSWORD_RESET:
                url=PlistHelper.getInstance().getPasswordResetUrl();
                title=getString(R.string.password_reset);
                break;
            case DKEY_PRIVATE_PRIVACY:
                url=PlistHelper.getInstance().getPrivacyStatementLink();
                title=getString(R.string.privacy_policy_update);
//                GoogleTrackers.Companion.setSPrivacyPolicy();
//                LocalyticsTrackers.Companion.setSPrivacyPolicy();
                break;
        }
        String TAG = FragmentNormalWebView.class.getSimpleName();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BKEY_URL, url);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.enter_to_left, R.anim.enter_from_left, R.anim.enter_to_right);
        FragmentNormalWebView fragment= (FragmentNormalWebView) ParentFragment.newInstance(title, false, drawerConst, bundle);
        fragment.setParentActivity(this);
        ft.replace(R.id.fragment_container, fragment, title);
        try {
            ft.commitAllowingStateLoss();
        } catch (Exception e) {
            Logger.logE("changeFragment", " " + e.getMessage());
        }
    }
}
