package com.finjan.securebrowser.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.constants.DrawerConstants;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.util.PermissionUtil;

import static com.finjan.securebrowser.constants.AppConstants.READ_PHONE_STATE_PERMISSION;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 3/10/17.
 *
 * Class responsible for screen while he chooses VPN or Browser
 *
 */


public class AppSwitchActivity extends AppCompatActivity implements View.OnClickListener {


    private final boolean skipAppChooseScreen=true;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_chooser);
        setReferences();
        NativeHelper.getInstnace().changeStatusBarTransparent(this);
    }

    private void setReferences() {
        findViewById(R.id.ll_browser).setOnClickListener(this);
        findViewById(R.id.ll_vpn).setOnClickListener(this);
        ((TextView)findViewById(R.id.tv_msg)).setTypeface(null, Typeface.BOLD);

        ((TextView)findViewById(R.id.tv_vpn_vitalsecurity)).setTypeface(null, Typeface.BOLD);
        ((TextView)findViewById(R.id.tv_vpn)).setTypeface(null, Typeface.BOLD);
        ((TextView)findViewById(R.id.tv_vpn_brower)).setTypeface(null, Typeface.BOLD);
        ((TextView)findViewById(R.id.tv_vitalsecurity)).setTypeface(null, Typeface.BOLD);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_browser:
                onChooseBrowser();
                break;
            case R.id.ll_vpn:
                onChooseVPN();
                break;
        }
    }

    private void onChooseBrowser() {
        UserPref.getInstance().setIsBroswer(true);
        UserPref.getInstance().setMyLastPage(ScreenNavigation.HOME_SCREEN);
        LocalyticsTrackers.Companion.setBorrowerUser();
        appSwitchPopup(true);
    }

    private void onChooseVPN() {

        if(AppConfig.Companion.getRequestPhoneStatePermission()){
            if(PermissionUtil.isPhoneReadStatePermissionGranted(this)){
               chooseVPN();
            }else {
                PermissionUtil.askPhoneStatePermission(this);
            }
        }else {
            chooseVPN();
        }

    }
    private void chooseVPN(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                UserPref.getInstance().setIsBroswer(false);
                UserPref.getInstance().setMyLastPage(ScreenNavigation.VPN_SCREEN);
                appSwitchPopup(false);
            }
        },50);
    }

    private void appSwitchPopup(final boolean browser) {
        FinjanDialogBuilder builder = new FinjanDialogBuilder(this);
        builder.setMessage(browser ? PlistHelper.getInstance().getChooseBrowerMsg() :
                PlistHelper.getInstance().getChooseVpnMsg())
                .setPositiveButton("Continue")
                .setNegativeButton("SETTINGS")
                .setDialogClickListener(new FinjanDialogBuilder.DialogClickListener() {
                    @Override
                    public void onNegativeClick() {
                        ScreenNavigation.getInstance().callHomeActivity(AppSwitchActivity.this, DrawerConstants.DKEY_Settings);
//                        AppSwitchActivity.this.finish();
                    }

                    @Override
                    public void onPositivClick() {
                        startActivity(new Intent(AppSwitchActivity.this, TutorialActivity.class));
                        GoogleTrackers.Companion.setEVPN_Only();
//                        LocalyticsTrackers.Companion.setEVPN_Only();
                        AppSwitchActivity.this.finish();

//                        ScreenNavigation.getInstance().startHome(AppSwitchActivity.this,"");
//                        AppSwitchActivity.this.finish();
//                        UserPref.getInstance().setIsSetUpDone(true);
                    }

                    @Override
                    public void onContentClick(String content) {

                    }
                });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case READ_PHONE_STATE_PERMISSION:
                if (grantResults.length >= 0 &&grantResults.length>0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    initUpdateCountService();
                    if (Utils.isNetworkConnected(this)) {
                        onChooseVPN();
                    } else {
                        FinjanDialogBuilder.showNetworkError(this);
                    }

                } else if (permissions.length>0 &&
                        ActivityCompat.shouldShowRequestPermissionRationale(AppSwitchActivity.this, permissions[0])) {
                    FinjanDialogBuilder.showPermissionError(AppSwitchActivity.this);
                }
                break;
        }
    }
}
