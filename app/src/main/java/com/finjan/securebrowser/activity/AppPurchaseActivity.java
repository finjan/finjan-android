package com.finjan.securebrowser.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.vending.billing.IInAppBillingService;
import com.finjan.securebrowser.helpers.vpn_helper.TempPurchaseHelper;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.licensing.models.billing.IabResult;
import com.finjan.securebrowser.vpn.licensing.models.billing.Inventory;
import com.finjan.securebrowser.vpn.licensing.models.billing.SkuDetails;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper;
import com.finjan.securebrowser.vpn.ui.iab.UpgradeActivity;
import com.finjan.securebrowser.AppLog;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.SkuModel;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC
 *
 * Class initialize some basic requirement for purchase
 *
 */

public abstract class AppPurchaseActivity extends UpgradeActivity{


    public static final int MONTHLY_KEY = 10001;
    public static final int YEARLY_KEY = 10002;
    private static final String TAG = AppPurchaseActivity.class.getSimpleName();
    private IInAppBillingService mService;
    private GoogleApiClient client;
    private Tracker mTracker;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_purchase);
        setReferences();
        initializeGoogle();
    }

    //initialize some services
    private void afterGoogleConnected(){
        initializeService();
        initAppIndexapi();
    }

    private RelativeLayout rlNonSecureLoader;
    private LinearLayout ll_Finzen_loader;

// initialize some reference use in app
    private void setReferences() {
        ll_Finzen_loader = (LinearLayout) findViewById(R.id.vg_loader);
        ll_Finzen_loader.setVisibility(View.GONE);
        rlNonSecureLoader = (RelativeLayout) findViewById(R.id.rl_non_secure_loader);
//        llToolbar = (LinearLayout) findViewById(R.id.base_toolbar);
    }

// initializing app indexing
    private void initAppIndexapi(){
        if (client != null && client.isConnected()) {
            try {
                Action viewAction2 = Action.newAction(
                        Action.TYPE_VIEW, // TODO: choose an action type.
                        "UrlScanner Page", // TODO: Define a title for the content shown.
                        // TODO: If you have web page content that matches this app activity's content,
                        // make sure this auto-generated web page URL is correct.
                        // Otherwise, set the URL to null.
                        Uri.parse("http://host/path"),
                        // TODO: Make sure this auto-generated app URL is correct.
                        Uri.parse("android-app://com.finjan.securebrowser/http/host/path")
                );
                AppIndex.AppIndexApi.start(client, viewAction2);
            } catch (Exception e) {
                Logger.logE(TAG, e.getMessage());
            }
        }
    }

    private void alertIfUserNotsignUp(){
        new FinjanDialogBuilder(this).setMessage(getString(R.string.google_service_failed_msg))
                .setPositiveButton(getString(R.string.alert_ok)).setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
            @Override
            public void onPositivClick() {
                finish();
            }
        }).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            Action viewAction = Action.newAction(
                    Action.TYPE_VIEW,
                    "UrlScanner Page",

                    // make sure this auto-generated web page URL is correct.
                    // Otherwise, set the URL to null.
                    Uri.parse("http://host/path"),

                    Uri.parse("android-app://com.finjan.securebrowser/http/host/path")
            );
            AppIndex.AppIndexApi.end(client, viewAction);
        } catch (Exception e) {
        }
        if(client!=null && client.isConnected()){
            client.disconnect();
        }
    }

    protected void initializeGoogle() {
        try {
            client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                @Override
                public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                    alertIfUserNotsignUp();
                }
            }).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(@Nullable Bundle bundle) {
                    afterGoogleConnected();
                }

                @Override
                public void onConnectionSuspended(int i) {

                }
            })
                    .build();
            client.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private IabHelper mIabHelper;

    /**
     * user earlier
     * For checking if user has safe count purchase
     */
    private void initializeService() {

        if(AppConstants.paidScanVersion){
            if (Utils.isNetworkConnected(AppPurchaseActivity.this)) {
                if(!UserPref.getInstance().getIsBrowser()){
                    ll_Finzen_loader.setVisibility(View.GONE);
                }else {
                    ll_Finzen_loader.setVisibility(View.VISIBLE);
                }

                if (NativeHelper.getInstnace().checkActivity(this)) {

                    mIabHelper=new IabHelper(this);
                    mIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                        @Override
                        public void onIabSetupFinished(IabResult result) {
                            ll_Finzen_loader.setVisibility(View.GONE);
                            if(result.isSuccess()){


                                if(mIabHelper!=null && mIabHelper.getmService()!=null){
                                    mService=mIabHelper.getmService();
                                    checkAppPurchase(mIabHelper.getmService());
                                }else {
                                    serviceResult(null);
                                }
                            }else {
                                serviceResult(null);
                            }
                        }
                    });


                } else {
                    if (GlobalVariables.getInstnace().isFinishCalled) {
                        finish();
                        return;
                    }
                    ll_Finzen_loader.setVisibility(View.GONE);
                    showNetworkErrorDialog();
                }
            }else {
                ll_Finzen_loader.setVisibility(View.GONE);
            }
        }else {
            serviceResult(null);
        }

    }

    private void checkAppPurchase(Inventory inventory){
        TempPurchaseHelper.getInstance().updateTempPur(inventory);
    }


    /**
     * used earlier when we user purchase for safe purchase
     *
     * @param mService
     */
    private void checkAppPurchase(final IInAppBillingService mService) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> skuList = new ArrayList<String>();
                skuList.add(getString(R.string.app_purchase_key_monthly));
                skuList.add(getString(R.string.app_purchase_key_yearly));
                Bundle querySkus = new Bundle();
                querySkus.putStringArrayList("ITEM_ID_LIST", skuList);

                try {

                    Bundle skuDetails = mService.getPurchases(3,
                            getPackageName(), "subs", null);
                    int response = skuDetails.getInt("RESPONSE_CODE");
                    if (response == 0) {
                        ArrayList<String> responseList = skuDetails.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                        ArrayList<SkuModel> skuModelArrayList = new ArrayList<SkuModel>();
                        GlobalVariables.getInstnace().isMonthlyAutoRenewal = false;
                        GlobalVariables.getInstnace().isYearlyAutoRenewal = false;
                        UserPref.getInstance().setSafeSearchPurchase(false);
                        if (responseList == null || responseList.size() == 0) {
                            serviceResult(null);
                            return;
                        }
                        try {
                            for (String thisResponse : responseList) {

                                JSONObject object = new JSONObject(thisResponse);
                                String sku = object.getString("productId");
                                String price = object.getString("autoRenewing");
                                String purchaseTime = object.getString("purchaseTime");
                                final String purchaseState = object.getString("purchaseState");
                                skuModelArrayList.add(new SkuModel(sku, price, purchaseTime, purchaseState));

                                if (sku.equalsIgnoreCase(getString(R.string.app_purchase_key_monthly))) {
                                    if (purchaseState.equalsIgnoreCase("0")) {
                                        AppPurchaseActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                GlobalVariables.getInstnace().isMonthlyAutoRenewal = true;
                                                UserPref.getInstance().setSafeSearchPurchase(true);
                                            }
                                        });
                                    }
//                                    manageAppPurchaseBar(_SETTING);
                                } else if (sku.equalsIgnoreCase(getString(R.string.app_purchase_key_yearly))) {
                                    AppLog.printLog("******yearly purchase status " + purchaseState);
                                    if (purchaseState.equalsIgnoreCase("0")) {
                                        AppPurchaseActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                GlobalVariables.getInstnace().isYearlyAutoRenewal = true;
                                                UserPref.getInstance().setSafeSearchPurchase(true);
                                            }
                                        });
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            AppPurchaseActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    serviceError(new Error(ErrorType.SOME_EXCEPTION, getString(R.string.EMSG_Internal_Error)));
                                }
                            });


                        }
                        serviceResult(skuModelArrayList);
                    } else {
                        AppPurchaseActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                serviceError(new Error(ErrorType.APP_PURCHASE_RESPONSE_CODE, getString(R.string.EMSG_wrong_response)));
                            }
                        });
                    }


                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.logE(TAG,"onDestroy called");
//        if (mIabHelper != null) {
//            unbindService(mServiceConn);
//        }
        if (mIabHelper != null) {
            mIabHelper.dispose();
            mIabHelper = null;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        } else if (requestCode == MONTHLY_KEY) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                try {
                    UserPref.getInstance().setSafeSearchPurchase(true);
                    JSONObject jo = new JSONObject(purchaseData);
                    String renewal = jo.getString("autoRenewing");
//                    Intent intent = new Intent(AppConstants.LBCAST_AppPurchase_Success);
//                    intent.putExtra(AppConstants.IKEY_AppPurchase_Monthly, true);

                    Bundle bundle = new Bundle();
                    bundle.putBoolean(AppConstants.IKEY_AppPurchase_Monthly, true);
                    if (GlobalVariables.getInstnace().appPurchaseFragment != null) {
                        GlobalVariables.getInstnace().appPurchaseFragment.updateUiAfterPerchase(bundle);

                    }
//                    LocalBroadcastManager.getInstance(AppPurchaseActivity.this).sendBroadcast(intent);
                    GlobalVariables.getInstnace().isMonthlyAutoRenewal = true;
//                    Toast.makeText(AppPurchaseActivity.this,"Purchase successful for month",Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == YEARLY_KEY) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                try {
                    UserPref.getInstance().setSafeSearchPurchase(true);
                    JSONObject jo = new JSONObject(purchaseData);
                    String renewal = jo.getString("autoRenewing");
                    Button monthly = ((Button) findViewById(R.id.btn_app_purchase_yearly));
                    monthly.setText(getString(R.string.renew_yearly));
                    GlobalVariables.getInstnace().isYearlyAutoRenewal = true;
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(AppConstants.IKEY_AppPurchase_Yearly, true);
                    if (GlobalVariables.getInstnace().appPurchaseFragment != null) {
                        GlobalVariables.getInstnace().appPurchaseFragment.updateUiAfterPerchase(bundle);
                    }
//                    Toast.makeText(AppPurchaseActivity.this,"Purchase successful for year",Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected abstract void serviceResult(ArrayList<SkuModel> responseList);

    protected abstract void serviceError(Error error);

    public IInAppBillingService getIInAppBillingService() {
        return mService;
    }

//    public void setTrcakerName(String trcakerName) {
//
//        if (mTracker == null) {
//            AnalyticsApplication application = (AnalyticsApplication) getApplication();
////            String analyticsID = Utils.getPlistvalue(Constants.KEY_GOOGLE_ANALYTICS, Constants.KEY_ANDROID);
////            mTracker = application.getDefaultTracker((analyticsID.isEmpty()) ? "" : analyticsID);
//            mTracker = application.getDefaultTracker(PlistHelper.getInstance().getGoogleEnalyticKey());
//        }
//        mTracker.setScreenName(trcakerName);
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
//    }

    private void showNetworkErrorDialog() {
        new FinjanDialogBuilder(this).setMessage(getString(R.string.network_message))
                .setPositiveButton(getString(R.string.alert_ok))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        finish();
                    }
                }).show();
    }

    private enum ErrorType {
        INTERNET_NOT_AVAILABLE, APP_PURCHASE_RESPONSE_CODE, SOME_EXCEPTION, UNKNOWN
    }

    protected class Error {
        ErrorType errorType;
        private String msg;

        private Error(ErrorType errorType, String msg) {
            this.errorType = errorType;
            this.msg = msg;
        }

        private ErrorType getType() {
            return errorType;
        }

        private void setType(ErrorType type) {
            this.errorType = type;
        }

        public String getMsg() {
            return msg;
        }

        private void setMsg(String msg) {
            this.msg = msg;
        }
    }

    @Override
    protected void onInventoryAvailable(Inventory inventory) {
        checkAppPurchase(inventory);
    }
    private ArrayList<SkuModel> getSkuModelList(Inventory inventory){
        ArrayList<SkuModel> list=new ArrayList<>();
        if(inventory!=null && inventory.getAllSkuDetailsSubs()!=null && inventory.getAllSkuDetailsSubs().size()>0){
            for (SkuDetails skuDetails: inventory.getAllSkuDetailsSubs()){
                SkuModel skuModel=new SkuModel(skuDetails.getSku(),skuDetails.getPrice(),skuDetails.getmPurchaseTime(),
                        skuDetails.getmPurchaseState());
                list.add(skuModel);
            }
        }
        return list;
    }

    @Override
    public List<String> getSkuList() {
        return null;
    }

    @Override
    public void initializeViews() {

    }

    @Override
    public String getSelectedProductSku() {
        return null;
    }

    @Override
    public void onPriceAvailable(SkuDetails skuDetails) {

    }

    @Override
    protected void backUpdated(boolean success, String msg) {

    }
}
