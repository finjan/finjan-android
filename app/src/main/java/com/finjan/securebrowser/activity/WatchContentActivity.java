package com.finjan.securebrowser.activity;

import android.app.ProgressDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.adapter.CatagoryAdapter;
import com.finjan.securebrowser.model.WatchCatsModel;
import com.finjan.securebrowser.util.JsonUtils;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;

import org.json.JSONException;
import org.json.JSONObject;

public class WatchContentActivity extends AppCompatActivity {
private LinearLayout ll_back;
private RecyclerView rv_catagory;
private CatagoryAdapter catagoryAdapter;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_content);
        initView();
        getCategoryFromServer();
    }
    private void initView(){
        ll_back = (LinearLayout)findViewById(R.id.ll_back);
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("InvinciBull");
        rv_catagory = (RecyclerView)findViewById(R.id.rv_cats_list);
    }

    private void setAdapter(WatchCatsModel watchCatsModel){
       catagoryAdapter = new CatagoryAdapter(this,watchCatsModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_catagory.setLayoutManager(mLayoutManager);
        rv_catagory.setItemAnimator(new DefaultItemAnimator());
        rv_catagory.setAdapter(catagoryAdapter);
    }

    private void getCategoryFromServer(){
        mProgressDialog = new ProgressDialog(this,R.style.VpnProgressDialog);
        mProgressDialog.setMessage("Please wait");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        NetworkManager.getInstance(this).getStagingAuthTocken(this, new NetworkResultListener() {
            @Override
            public void executeOnSuccess(JSONObject response) {
                String authtocken = "";
                try {
                    authtocken = (String) response.get("access_token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                NetworkManager.getInstance(WatchContentActivity.this).getCategory(WatchContentActivity.this, new NetworkResultListener() {
                    @Override
                    public void executeOnSuccess(JSONObject response) {
                        WatchCatsModel watchCatsModel = (WatchCatsModel) JsonUtils.convertJsonToModel(response.toString(),WatchCatsModel.class);
                        setAdapter(watchCatsModel);
                        mProgressDialog.hide();
                    }

                    @Override
                    public void executeOnError(VolleyError error) {
                        mProgressDialog.hide();

                    }
                },authtocken);
            }

            @Override
            public void executeOnError(VolleyError error) {

            }
        });

    }
}
