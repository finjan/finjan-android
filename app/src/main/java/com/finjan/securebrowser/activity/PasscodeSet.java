package com.finjan.securebrowser.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.custom_views.PasskeyIconView;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class responsible for  pass code generation
 */


public class PasscodeSet extends ParentActivity implements View.OnClickListener{

    private ImageView iv_cicle1,iv_cicle2,iv_cicle3,iv_cicle4;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passcode_orientation);
//        Fabric.with(this, new Crashlytics());
        setReferences();
        setDefaults();
        GoogleTrackers.Companion.setSPasscodeChange();
//        LocalyticsTrackers.Companion.setSPasscodeChange();
//        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }
    private boolean isTouchIdEnabled;

    @Override
    protected void onResume() {
        super.onResume();
        if(getIntent()!=null && getIntent().getBooleanExtra(AppConstants.IKEY_ENABLE_TOUCH_ID,false)){
            isTouchIdEnabled=true;
        }
    }

    private void onClickButton(String value){
        UserPref.getInstance().setSetLockCode(value);
        setCirclesPasscode();
        checkPasscode();
    }

    private void setReferences() {
        iv_cicle1 = (ImageView) findViewById(R.id.imageView8);
        iv_cicle2 = (ImageView) findViewById(R.id.imageView9);
        iv_cicle3 = (ImageView) findViewById(R.id.imageView10);
        iv_cicle4 = (ImageView) findViewById(R.id.imageView11);

        (findViewById(R.id.rl_1)).setOnClickListener(this);
        (findViewById(R.id.rl_2)).setOnClickListener(this);
        (findViewById(R.id.rl_3)).setOnClickListener(this);
        (findViewById(R.id.rl_4)).setOnClickListener(this);
        (findViewById(R.id.rl_5)).setOnClickListener(this);
        (findViewById(R.id.rl_6)).setOnClickListener(this);
        (findViewById(R.id.rl_7)).setOnClickListener(this);
        (findViewById(R.id.rl_8)).setOnClickListener(this);
        (findViewById(R.id.rl_9)).setOnClickListener(this);
        (findViewById(R.id.rl_0)).setOnClickListener(this);
        ((PasskeyIconView)(findViewById(R.id.iv_back))).setOnTouch(closeKeyListener,this);
        ((PasskeyIconView)(findViewById(R.id.iv_close))).setOnTouch(backKeylistener,this);
    }
    private PasskeyIconView.PasskeyListener backKeylistener=new PasskeyIconView.PasskeyListener() {
        @Override
        public void onTouchKey() {
            onBackPressed();
        }
    };
    private PasskeyIconView.PasskeyListener closeKeyListener=new PasskeyIconView.PasskeyListener() {
        @Override
        public void onTouchKey() {
            if (UserPref.getInstance().getSetLockCode().length() > 0) {
                UserPref.getInstance().setSetLockCode(UserPref.getInstance().getSetLockCode()
                        .substring(0, UserPref.getInstance().getSetLockCode().length() - 1));
            }
            setCirclesPasscode();
        }
    };

    public void checkPasscode(){


        if(!TextUtils.isEmpty(UserPref.getInstance().getSetLockCode()) && UserPref.getInstance().getSetLockCode().trim().length()==4 ){
            startActivityForResult(new Intent(getApplicationContext(), ConfirmPasscode.class),AppConstants.AFR_callingConfirmPassword);
            GlobalVariables.getInstnace().appCodeSetNewly=false;
        }

    }
    private void setDefaults(){
        if(solidCircle==null){
            solidCircle= ResourceHelper.getInstance().getDrawable(R.drawable.pin_fill);
        }
        if(emptyCircle==null){
            emptyCircle= ResourceHelper.getInstance().getDrawable(R.drawable.pin_blank);
        }

//        pref = new Preferences(getApplicationContext());
        UserPref.getInstance().setSetLockCode("");
//        pref.set(Constants.SetLockcode,"").commit();
        UserPref.getInstance().setConfirmLockCode("");
//        pref.set(Constants.ConfirmLockcode,"").commit();
        UserPref.getInstance().setCheckLockCode("");
//        pref.set(Constants.CheckLockcode,"").commit();

        TextView tv_enterpasscode = (TextView) findViewById(R.id.textView2);
        tv_enterpasscode.setText("Set  Passcode");

    }

    private Drawable solidCircle,emptyCircle;
    public void setCirclesPasscode(){
        String setLockcode=UserPref.getInstance().getSetLockCode();
        if(setLockcode==null){
           return;
        }
        setLockcode=setLockcode.trim();
        int codeLenght=setLockcode.length();

        switch (codeLenght){
            case 1:
                iv_cicle1.setBackground(solidCircle);
                iv_cicle2.setBackground(emptyCircle);
                iv_cicle3.setBackground(emptyCircle);
                iv_cicle4.setBackground(emptyCircle);
                break;
            case 2:
                iv_cicle1.setBackground(solidCircle);
                iv_cicle2.setBackground(solidCircle);
                iv_cicle3.setBackground(emptyCircle);
                iv_cicle4.setBackground(emptyCircle);
                break;
            case 3:
                iv_cicle1.setBackground(solidCircle);
                iv_cicle2.setBackground(solidCircle);
                iv_cicle3.setBackground(solidCircle);
                iv_cicle4.setBackground(emptyCircle);
                break;
            case 4:
                iv_cicle1.setBackground(solidCircle);
                iv_cicle2.setBackground(solidCircle);
                iv_cicle3.setBackground(solidCircle);
                iv_cicle4.setBackground(solidCircle);
                break;
            case 0:
                iv_cicle1.setBackground(emptyCircle);
                iv_cicle2.setBackground(emptyCircle);
                iv_cicle3.setBackground(emptyCircle);
                iv_cicle4.setBackground(emptyCircle);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void handleClick(View view){
        switch (view.getId()){
            case R.id.rl_1:
                onClickButton(UserPref.getInstance().getSetLockCode()+"1");
                break;
            case R.id.rl_2:
                onClickButton(UserPref.getInstance().getSetLockCode()+"2");
                break;
            case R.id.rl_3:
                onClickButton(UserPref.getInstance().getSetLockCode()+"3");
                break;
            case R.id.rl_4:
                onClickButton(UserPref.getInstance().getSetLockCode()+"4");
                break;
            case R.id.rl_5:
                onClickButton(UserPref.getInstance().getSetLockCode()+"5");
                break;
            case R.id.rl_6:
                onClickButton(UserPref.getInstance().getSetLockCode()+"6");
                break;
            case R.id.rl_7:
                onClickButton(UserPref.getInstance().getSetLockCode()+"7");
                break;
            case R.id.rl_8:
                onClickButton(UserPref.getInstance().getSetLockCode()+"8");
                break;
            case R.id.rl_9:
                onClickButton(UserPref.getInstance().getSetLockCode()+"9");
                break;
            case R.id.rl_0:
                onClickButton(UserPref.getInstance().getSetLockCode()+"0");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==AppConstants.AFR_callingConfirmPassword){
            if(data!=null && data.hasExtra(AppConstants.IKEY_PASSWORD_CONFIRMED) &&
            data.getBooleanExtra(AppConstants.IKEY_PASSWORD_CONFIRMED,false)){

                if(GlobalVariables.getInstnace().fragmentSetting !=null){
                    GlobalVariables.getInstnace().fragmentSetting.setPasscordOnResult(true);
                }
                if(isTouchIdEnabled){
                    startActivityForResult(new Intent(PasscodeSet.this,FingerPrintGeneration.class),AppConstants.AFR_REGISTER_FINGER);
                    return;
                }
                setResult(AppConstants.AFR_Password_set,data);
                finish();
            }else {
                setResult(AppConstants.AFR_Password_set,null);
                finish();
            }
        }else if(requestCode==AppConstants.AFR_REGISTER_FINGER){
            if(data!=null && data.hasExtra(AppConstants.IKEY_FINGER_REGISTERED)){
                if(data.getBooleanExtra(AppConstants.IKEY_FINGER_REGISTERED,false)){
                    if(GlobalVariables.getInstnace().fragmentSetting !=null){
                        setResult(AppConstants.AFR_REGISTER_FINGER,data);
                        finish();
                        return;
                    }
                }
            }
            setResult(AppConstants.AFR_REGISTER_FINGER,null);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        handleClick(v);
    }
}
