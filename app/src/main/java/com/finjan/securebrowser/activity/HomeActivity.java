package com.finjan.securebrowser.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringListener;
import com.facebook.rebound.SpringSystem;
import com.finjan.securebrowser.Appirater;
import com.finjan.securebrowser.helpers.DisplayHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;
import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.AppLog;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.constants.DrawerConstants;
import com.finjan.securebrowser.custom_views.SwipeDismissTouchListener;
import com.finjan.securebrowser.fragment.FragmentAppPurchase;
import com.finjan.securebrowser.fragment.FragmentHistory;
import com.finjan.securebrowser.fragment.FragmentHome;
import com.finjan.securebrowser.fragment.FragmentNormalWebView;
import com.finjan.securebrowser.fragment.FragmentTabs;
import com.finjan.securebrowser.fragment.ParentFragment;
import com.finjan.securebrowser.fragment.FragmentSetting;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.SafetyResult;
import com.finjan.securebrowser.model.SkuModel;
import com.finjan.securebrowser.vpn.util.PermissionUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import webHistoryDatabase.bookmarks;

import static com.finjan.securebrowser.constants.AppConstants.READ_PHONE_STATE_PERMISSION;
import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_ABOUT;
import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_EULA;
import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_HELP;
import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_History;
import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_PASSWORD_RESET;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 3/10/17.
 *
 * Main class(Activity) for Browser app, all activity level methods includes here for browser.
 */

public class HomeActivity extends BaseActivity implements SpringListener, View.OnClickListener {
    public int displayHeight;
    private Spring reportSpring, settingsSpring, appPurchaseSpring;
    private ImageView iv_bookmark1, iv_bookmark_land;
    private ViewGroup vg_settins_menu, vg_settins_menu_landscape, vg_tracker_hint,
            reportMainButton, appPurchasePanel, reportContainer;
    private RelativeLayout reportButton;
    private ImageView reportButtonImg;
    private TextView reportButtonText, tv_detectratio, tv_lastseen;

    @Override
    protected void onResume() {
        super.onResume();
        onStopCalled=false;
        NativeHelper.getInstnace().currentAcitvity=this;
//        InitializeVpn.getInstance().connectVpn(this);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReferences(parentView);
        ScreenNavigation.pushActivApp("SCREEN_BROWSER");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Appirater.appLaunched(HomeActivity.this);
            }
        },1000);
        initializeSprings();
//        InitializeVpn.getInstance().setHomeActivity(this);
//        InitializeVpn.getInstance().initVpnService(this);
        if(!TextUtils.isEmpty(UserPref.getInstance().getDeviceID())){
            NativeHelper.getInstnace().setDeviceId(this);
        }
        setIntentExtras();
    }

    private void setIntentExtras(){
        if(getIntent()!=null && getIntent().getBundleExtra(AppConstants.BKEY_BUNDLE)!=null){
            Bundle bundle=getIntent().getBundleExtra(AppConstants.BKEY_BUNDLE);
            ScreenNavigation.getInstance().setCurrentDefaultScreen(bundle.getString(AppConstants.BKEY_SCREEN));
        }
    }
    private static final String TAG=HomeActivity.class.getSimpleName();
    @Override
    protected void serviceResult(ArrayList<SkuModel> responseList) {
        super.serviceResult(responseList);
        setDisplayHeights();
    }

    @Override
    protected void serviceError(Error error) {
        super.serviceError(error);
        setDisplayHeights();
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
//        InitializeVpn.getInstance().removeVpnService();
        if(GlobalVariables.getInstnace().currentHomeFragment!=null){
            UserPref.getInstance().setLastUrl(GlobalVariables.getInstnace().currentHomeFragment.getCurrentUrl());
            UserPref.getInstance().setLastId(GlobalVariables.getInstnace().currentHomeFragment.getWebviewId());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        onOrientationChanged(true);
    }
    private void onOrientationChanged(boolean isDeviceRotated) {

        final int drawerBottomBar= DisplayHelper.getInstance().getDrawerBottomBar(HomeActivity.this);

        if (UserPref.getInstance().getPortrainHeight() > 0 && UserPref.getInstance().getLandHeight() > 0) {
            displayHeight=DisplayHelper.getInstance().getDisplayHeight(isLandscapedOrientation(),this);
            if(isDeviceRotated){
                if (GlobalVariables.getInstnace().isDrawerShowing) {
                    GlobalVariables.getInstnace().orientationIsJustChanged=true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            GlobalVariables.getInstnace().orientationIsJustChanged=false;
                        }
                    },1000);
                    showDrawer();
                }else {
                    int changeValue=0;
                    if (isPortraitOrientation()) {
                        changeValue = displayHeight - NativeHelper.getInstnace().getDrawerHeight(true, this);
                    }
                    if (isLandscapedOrientation()) {
                        changeValue = displayHeight - NativeHelper.getInstnace().getDrawerHeight(false, this);
                    }
                    if (changeValue != -1) {
                        settingsSpring.setCurrentValue(changeValue);
                        settingsSpring.setEndValue(displayHeight+(2*drawerBottomBar));
                    }
                    GlobalVariables.getInstnace().isDrawerShowing=false;
                }
            }

            setBarsOnOrientation();
        }
        else {
            ViewTreeObserver observer = parentView.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    displayHeight = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getHeight();
                    displayHeight=displayHeight-drawerBottomBar;
                    parentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    if (GlobalVariables.getInstnace().isDrawerShowing) {
                        showDrawer();
                    }
                    setBarsOnOrientation();
                }
            });
        }
    }

    private void setDisplayHeights() {
        displayHeight=DisplayHelper.getInstance().getDisplayHeight(isLandscapedOrientation(),HomeActivity.this);
        HomeActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activityLevelLoader(View.GONE);
                getHeightDimension();
                if(!isDataSetledEarlier){
                    setData();
                    Intent intent=HomeActivity.this.getIntent();
                    if(intent!=null && !TextUtils.isEmpty(intent.getStringExtra("screen"))){
                        moveToScreen(intent.getStringExtra("screen"));
                    }
//                    moveToScreen(ScreenNavigation.getInstance().getCurrentDefaultScreen());
                }
            }
        });
    }

    private void getHeightDimension() {

        reportContainer.post(new Runnable() {
            public void run() {
//                int orignalHeight = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getHeight();
                reportSpring.setEndValue(DisplayHelper.getInstance().getOrignalHeight(isLandscapedOrientation(),HomeActivity.this));
                appPurchaseSpring.setEndValue(DisplayHelper.getInstance().getOrignalHeight(isLandscapedOrientation(),HomeActivity.this));
            }
        });
    }
    public void refreshDisplayHeight(){
        onOrientationChanged(false);
        setForShowReport();
    }

    private int getDefaultHeight() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
    }

    @Override
    protected void onStop() {
        super.onStop();
        onStopCalled=true;
        Logger.logE(TAG,"onStop called");
//        InitializeVpn.getInstance().disconnectVpn(this);
        GlobalVariables.getInstnace().fabVisibility=false;
        GlobalVariables.getInstnace().safeBarVisibility=false;
    }

    private boolean onStopCalled;

    public void showTrackFab() {
        if (GlobalVariables.getInstnace().fabVisibility) {
//        if (trackerSpring.getCurrentValue() < displayHeight ) {
            return;
        }
        if(!onStopCalled){
            GlobalVariables.getInstnace().fabVisibility = true;
        }
    }

    public void hideTrackerFab() {
        if (!UserPref.getInstance().getTrackerHintVisibility()) {
            GlobalVariables.getInstnace().fabVisibility = false;
        }
    }
    private boolean isWebviewHaveProperUrl(){
        FragmentHome fragmentHome=GlobalVariables.getInstnace().currentHomeFragment;
        return fragmentHome!=null && !TextUtils.isEmpty(fragmentHome.getCurrentUrl()) &&
                !fragmentHome.getCurrentUrl().equalsIgnoreCase(AppConstants.EMPTY_URL);
    }

    private void setBarsOnOrientation() {

        if (GlobalVariables.getInstnace().appbarVisibility && (!UserPref.getInstance().getSafeSearchPurchase() &&
                !(NativeHelper.getInstnace().getScanRemain() > 0) && isWebviewHaveProperUrl())) {
            setForShowPurchaseBar();
        } else {
            setForHidePurchaseBar();
        }
        if (GlobalVariables.getInstnace().safeBarVisibility &&
                GlobalVariables.getInstnace().currentHomeFragment.isReportAvailable && isWebviewHaveProperUrl()) {
            setForShowReport();
        } else {
            setForHideReport();
        }
    }


    /**
     * make driver hide in its own orientation
     */
    public void hideDrawer() {

        int changeValue = -1;
        if (GlobalVariables.getInstnace().isDrawerShowing) {
            if (isPortraitOrientation()) {
                changeValue = displayHeight - vg_settins_menu.getHeight();
            }
            if (isLandscapedOrientation()) {
                changeValue = displayHeight - vg_settins_menu_landscape.getHeight();
            }
            if (changeValue != -1) {
                settingsSpring.setCurrentValue(changeValue);
                settingsSpring.setEndValue(displayHeight+(2*DisplayHelper.getInstance().getDrawerBottomBar(getApplicationContext())));
                GlobalVariables.getInstnace().isDrawerShowing = false;
                GoogleTrackers.Companion.setEHamburger_Menu_Closed();
//                LocalyticsTrackers.Companion.setEHamburger_Menu_Closed();
            }
        }
        if(GlobalVariables.getInstnace().currentHomeFragment!=null){
            GlobalVariables.getInstnace().currentHomeFragment.drawerStatusChanged(false);
        }
    }


    private void sendNoThankClick() {
        Intent intent = new Intent(AppConstants.LBCAST_BAR_NO_THANKS_CLICK);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * show purchase bar
     */
    public void showReportBar() {
        if (GlobalVariables.getInstnace().isDrawerShowing || GlobalVariables.getInstnace().safeBarVisibility ) {
            return;
        }
        appPurchasePanel.setVisibility(View.GONE);
        reportContainer.setVisibility(View.VISIBLE);
        setForShowReport();
        if(!onStopCalled){
            GlobalVariables.getInstnace().safeBarVisibility = true;
        }
        startTimer();
    }

    public void setForShowReport() {
        reportSpring.setCurrentValue(displayHeight);
        reportSpring.setEndValue(displayHeight - getBarHeight());
    }

    public void hideReportBar() {

        setForHideReport();
        reportContainer.setVisibility(View.GONE);
        GlobalVariables.getInstnace().safeBarVisibility = false;
    }

    private void setForHideReport() {
        reportSpring.setCurrentValue(displayHeight);
        reportSpring.setEndValue(displayHeight + getBarHeight()+DisplayHelper.getInstance().getDrawerBottomBar(this));
    }

    public void hidePurchaseBar() {
        setForHidePurchaseBar();
        appPurchasePanel.setVisibility(View.GONE);
        GlobalVariables.getInstnace().appbarVisibility = false;
    }

    private void setForHidePurchaseBar() {
        appPurchaseSpring.setCurrentValue(displayHeight );
        appPurchaseSpring.setEndValue(displayHeight + getBarHeight()+DisplayHelper.getInstance().getDrawerBottomBar(this));
    }

    private float getBarHeight() {
        if ((UserPref.getInstance().getSafeScanEnabled() ||
                (GlobalVariables.getInstnace().currentHomeFragment!=null &&
                        GlobalVariables.getInstnace().currentHomeFragment.vtScan)) &&
        (NativeHelper.getInstnace().getScanRemain() > 0 || UserPref.getInstance().getSafeSearchPurchase())) {
            return getSafeBarHeight();
        } else {
            return getAppPurchaseBarHeight();
        }
    }

    private float getSafeBarHeight() {
        float value = getResources().getDimension(R.dimen._90sdp);
        return NativeHelper.getInstnace().getPxValue(value, this);
    }

    private float getAppPurchaseBarHeight() {
        float value = getResources().getDimension(R.dimen._50sdp);
        return NativeHelper.getInstnace().getPxValue(value, this);
    }

    public void setReport(SafetyResult safetyResult) {
        SpannableString spanString = new SpannableString("" + safetyResult.getPositive() + "/" + safetyResult.getTotal());
        tv_detectratio.setText(spanString);
        Date date = null;
        try {
            String datestr = safetyResult.getLastSeen();
            AppLog.printLog("LAST Date ", "   " + datestr);
            DateFormat formatter;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = (Date) formatter.parse(datestr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String last_scan = "Last Scanned on " + date.getDate() + "/" + (date.getMonth() + 1);
        AppLog.printLog("LAST SCANNED ", "   " + last_scan);

        tv_lastseen.setText("" + (date.getMonth() + 1) + "/" + date.getDate());


        reportButtonText.setText(safetyResult.getButtonString());
        reportButtonImg.setImageResource(safetyResult.getButtonImage());

        if (safetyResult.color == R.color.red_1) {
            reportMainButton.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_scan_result_dangrous_selector));

        } else if (safetyResult.color == R.color.yellow_1) {
            reportMainButton.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_scan_result_suspecious_selector));

        } else if (safetyResult.color == R.color.green_colour_1) {
            reportMainButton.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.bg_scan_result_safe_selector));
        }
    }

    public void setreportResources() {
        if (UserPref.getInstance().getSafeScanEnabled()) {
            reportButtonImg.setImageDrawable(ResourceHelper.getInstance().getDrawable(R.drawable.scan_status_big_safe));
            tv_lastseen.setText("");
            tv_detectratio.setText("");
            reportButton.setClickable(false);
            reportButtonText.setText(getString(R.string.safe));
        }
    }

    /**
     * show purchase bar
     * Todo make it visible only when fragment drawing completed i.e. isDrawingComplete
     */
    public void showPurchaseBar() {

        TextView titleTXT = ((TextView) parentView.findViewById(R.id.txt_upgrade_title_home));
        titleTXT.requestLayout();
        titleTXT.setVisibility(View.VISIBLE);
        titleTXT.setText(getNoThanksMessage());
        titleTXT.setMovementMethod(LinkMovementMethod.getInstance());

        if (GlobalVariables.getInstnace().isDrawerShowing) {
            return;
        }
        reportContainer.setVisibility(View.GONE);
        appPurchasePanel.setVisibility(View.VISIBLE);
        setForShowPurchaseBar();
        if(!onStopCalled){
            GlobalVariables.getInstnace().appbarVisibility = true;
        }
    }

    private void setForShowPurchaseBar() {
        appPurchaseSpring.setCurrentValue(displayHeight);
        appPurchaseSpring.setEndValue(displayHeight - getBarHeight());
    }

    private SpannableStringBuilder getNoThanksMessage() {
        String message = getString(R.string.no_longer_protected);
        String noThanks = getString(R.string.no_thanks);
        message = message + "   " + noThanks + "   ";


        SpannableStringBuilder ssb = new SpannableStringBuilder(message);
        final ForegroundColorSpan fcs = new ForegroundColorSpan(ResourceHelper.getInstance().getColor(R.color.fj_white));
        int idx1 = message.indexOf(noThanks);
//        int idx2 = message.length();
        int idx2 = message.lastIndexOf("   ");
        ssb.setSpan(new ClickableSpan() {

            @Override
            public void onClick(View widget) {
                manageNoThanksCLick();
            }
        }, idx1, idx2, 0);
        ssb.setSpan(fcs, idx1, idx2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return ssb;
    }

    private void manageNoThanksCLick() {
        try {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 10);
            String updatedTime = "";
            long time = c.getTimeInMillis();
            updatedTime = "" + time;
            UserPref.getInstance().setNoThanksRemain(updatedTime);
            FragmentHome fragmentHome=GlobalVariables.getInstnace().currentHomeFragment;
            if(fragmentHome!=null){
                fragmentHome.setBottomBars(fragmentHome,true);

            }

            sendNoThankClick();
            // number of days to add
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * make drawer visible in its desired orientation
     */
    public void showDrawer() {
        int endValue = -1;

        if (isPortraitOrientation()) {
            endValue = displayHeight - NativeHelper.getInstnace().getDrawerHeight(true, this);
            if (vg_settins_menu.getVisibility() == View.GONE ||
                    vg_settins_menu.getVisibility() == View.INVISIBLE) {
                vg_settins_menu.setVisibility(View.VISIBLE);
                GoogleTrackers.Companion.setEHamburger_Menu_Open();
//                LocalyticsTrackers.Companion.setEHamburger_Menu_Open();

            }
            vg_settins_menu_landscape.setVisibility(View.GONE);
        } else if (isLandscapedOrientation()) {
            endValue = displayHeight - NativeHelper.getInstnace().getDrawerHeight(false, this);
            if (vg_settins_menu_landscape.getVisibility() == View.GONE ||
                    vg_settins_menu_landscape.getVisibility() == View.INVISIBLE) {
                vg_settins_menu_landscape.setVisibility(View.VISIBLE);
                GoogleTrackers.Companion.setEHamburger_Menu_Open();
//                LocalyticsTrackers.Companion.setEHamburger_Menu_Open();
            }
            vg_settins_menu.setVisibility(View.GONE);
        }
        if (endValue != -1) {
            settingsSpring.setCurrentValue(displayHeight);
            settingsSpring.setEndValue(endValue);
            GlobalVariables.getInstnace().isDrawerShowing = true;
        }
        if(GlobalVariables.getInstnace().currentHomeFragment!=null){
            GlobalVariables.getInstnace().currentHomeFragment.drawerStatusChanged(true);
        }

    }

    private void initializeSprings() {
        SpringSystem springSystem = SpringSystem.create();

        reportSpring = springSystem.createSpring();
        reportSpring.addListener(this);

        appPurchaseSpring = springSystem.createSpring();
        appPurchaseSpring.addListener(this);
        settingsSpring = springSystem.createSpring();
        settingsSpring.addListener(this);
    }

    private void setReferences(View parentView) {
        //drawer
        //main drawer views
        vg_settins_menu = (ViewGroup) parentView.findViewById(R.id.vg_settins_menu);
        vg_settins_menu_landscape = (ViewGroup) parentView.findViewById(R.id.vg_settins_menu_landscape);
        //drawer top bar options
        iv_bookmark1 = (ImageView) parentView.findViewById(R.id.iv_bookmark);
        iv_bookmark_land = (ImageView) parentView.findViewById(R.id.iv_bookmark_land);

        reportMainButton = (ViewGroup) parentView.findViewById(R.id.reportMainButton);
        reportContainer= (ViewGroup) parentView.findViewById(R.id.report_container);

        LinearLayout safetyClose = (LinearLayout) reportMainButton.findViewById(R.id.lyt_safety_close);
        reportButton = (RelativeLayout) reportMainButton.findViewById(R.id.reportButton);
        reportButtonImg = (ImageView) reportButton.findViewById(R.id.reportButtonImg);
        reportButtonText = (TextView) reportButton.findViewById(R.id.reportButtonText);
        tv_detectratio = (TextView) reportButton.findViewById(R.id.tv_detectratio);
        tv_lastseen = (TextView) reportButton.findViewById(R.id.tv_lastseen);
        reportContainer.setVisibility(View.GONE);
        reportButton.setOnClickListener(this);
        safetyClose.setOnClickListener(this);

        //bottom perchase bar
        appPurchasePanel = (ViewGroup) parentView.findViewById(R.id.app_purchase_layout_bar);
        parentView.findViewById(R.id.btn_layout_upgrade1).setOnClickListener(this);
        appPurchasePanel.setVisibility(View.GONE);


        //tracker count icon tutorial screen
        vg_tracker_hint = (ViewGroup) parentView.findViewById(R.id.tracker_hint_layout);
        vg_tracker_hint.setOnClickListener(this);


//click listeners
        (parentView.findViewById(R.id.ll_share)).setOnClickListener(this);
        (parentView.findViewById(R.id.rl_share_land)).setOnClickListener(this);
        (parentView.findViewById(R.id.ll_Vpn)).setOnClickListener(this);
        (parentView.findViewById(R.id.rl_Vpn_land)).setOnClickListener(this);
        (parentView.findViewById(R.id.ll_settings)).setOnClickListener(this);
        (parentView.findViewById(R.id.rl_settings_land)).setOnClickListener(this);
        (parentView.findViewById(R.id.ll_bookmark)).setOnClickListener(this);
        (parentView.findViewById(R.id.rl_bookmark_land)).setOnClickListener(this);
        (parentView.findViewById(R.id.ll_history)).setOnClickListener(this);
        (parentView.findViewById(R.id.rl_history_land)).setOnClickListener(this);
        parentView.findViewById(R.id.ll_about).setOnClickListener(this);
        parentView.findViewById(R.id.rl_about_land).setOnClickListener(this);
        parentView.findViewById(R.id.ll_help).setOnClickListener(this);
        parentView.findViewById(R.id.rl_help_land).setOnClickListener(this);
        parentView.findViewById(R.id.ll_rate).setOnClickListener(this);
        parentView.findViewById(R.id.rl_rate_land).setOnClickListener(this);

        reportButton.setOnTouchListener(new SwipeDismissTouchListener(reportContainer, null, new SwipeDismissTouchListener.DismissCallbacks() {
            @Override
            public boolean canDismiss(Object token) {
                return true;
            }

            @Override
            public void onDismiss(View view, Object token) {
                doThisONBottomBarAway();
            }
        }));
        safetyClose.setOnTouchListener(new SwipeDismissTouchListener(reportContainer, null, new SwipeDismissTouchListener.DismissCallbacks() {
            @Override
            public boolean canDismiss(Object token) {
                return true;
            }

            @Override
            public void onDismiss(View view, Object token) {
               doThisONBottomBarAway();
            }
        }));


    }
    private void doThisONBottomBarAway(){
        FragmentHome fragmentHome=GlobalVariables.getInstnace().currentHomeFragment;
        if(fragmentHome!=null){
            fragmentHome.barGoneClicked = true;
            fragmentHome.setBottomBars(fragmentHome,true);
        }
    }

    public void hintVisibility() {
        if (GlobalVariables.getInstnace().currentHomeFragment != null &&
                GlobalVariables.getInstnace().currentHomeFragment.isVisible()) {
            if (!UserPref.getInstance().getFirstTrackerHintVisibility()) {
                toggleTrackerHint(true);
                return;
            }
            toggleTrackerHint(false);
        }
    }

    private void onClickTrackHint() {
        UserPref.getInstance().setFirstTrackerHintVisibility(true);
        hintVisibility();
        if(GlobalVariables.getInstnace().currentHomeFragment!=null){
            GlobalVariables.getInstnace().currentHomeFragment.setBottomBars(GlobalVariables.getInstnace().currentHomeFragment,true);
        }
    }

    private void toggleTrackerHint(boolean shouldShow) {

        if (shouldShow) {
            if(GlobalVariables.getInstnace().isDrawerShowing){
                hideDrawer();
            }
            GlobalVariables.getInstnace().currentHomeFragment.makeDownScrollFalse();
            vg_tracker_hint.setVisibility(View.VISIBLE);
            UserPref.getInstance().setTrackerHintVisibility(true);
            finzenLoaderVisibility(View.GONE,null);
            return;
        }
        if (vg_tracker_hint.getVisibility() == View.VISIBLE) {
            UserPref.getInstance().setTrackerHintVisibility(false);
            vg_tracker_hint.setVisibility(View.GONE);
        }
    }


    @Override
    public void onSpringUpdate(Spring spring) {
        if (spring == reportSpring) {
            float value = (float) spring.getCurrentValue();
            reportContainer.setY(value);
        } else if (spring == settingsSpring) {
            float value = (float) spring.getCurrentValue();
            vg_settins_menu.setY(value);
            vg_settins_menu_landscape.setY(value);
        }
//        if (spring == trackerSpring) {
//            float value = (float) spring.getCurrentValue();
//            vg_tracker_fab_layout.setY(value);
//        }
        if (spring == appPurchaseSpring) {
            float value = (float) spring.getCurrentValue();
            appPurchasePanel.setY(value);
        }
    }

    @Override
    public void onSpringAtRest(Spring spring) {

    }

    @Override
    public void onSpringActivate(Spring spring) {

    }

    @Override
    public void onSpringEndStateChange(Spring spring) {

    }

    @Override
    public void onBackPressed() {
//        if(UserPref.getInstance().getIsBrowser()){
//            finish();
//            return;
//        }
        if(GlobalVariables.getInstnace().isDrawerShowing){
            hideDrawer();
            FragmentHome fragmentHome=GlobalVariables.getInstnace().currentHomeFragment;
            if(fragmentHome!=null){
                fragmentHome.setBottomBars(fragmentHome,true);
            }
            return;
        }
        super.onBackPressed();
    }


    public void updateTrackerCount(final int size) {
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.logE(TAG,"onPause called");
    }

    private void callShareIntent() {
        GoogleTrackers.Companion.setEHamburger_Menu_Share();
//        LocalyticsTrackers.Companion.setEHamburger_Menu_Share();
        String url=GlobalVariables.getInstnace().currentHomeFragment.currentUrl;
        String hostName=null;
        if(!TextUtils.isEmpty(url)){
            try{
                URL url1=new URL(url);
                hostName=url1.getHost();
            }catch (MalformedURLException e){}
        }
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        StringBuffer values = new StringBuffer();
        values.append(getTitle());
        values.append("  ");
        values.append(GlobalVariables.getInstnace().currentHomeFragment.currentUrl);
        values.append("  ");
        sendIntent.putExtra(Intent.EXTRA_TEXT, values.toString());
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, hostName!=null?hostName:"");
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getString(R.string.share_via)));
    }

    public void openVPNScreen(){

        if(AppConfig.Companion.getRequestPhoneStatePermission()){
            if(PermissionUtil.isPhoneReadStatePermissionGranted(this)){
                startVpnIntent();
            }else {
                PermissionUtil.askPhoneStatePermission(this);
            }
        }else {
            startVpnIntent();
        }
    }
    private void startVpnIntent(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(HomeActivity.this, VpnActivity.class);
                startActivity(intent);
            }
        },50);
    }
    public void openTabsScreen() {
        GoogleTrackers.Companion.setSTabs();
//        LocalyticsTrackers.Companion.setSTabs();
        FragmentHome fragmentHome= GlobalVariables.getInstnace().currentHomeFragment;
        if(fragmentHome==null|| TextUtils.isEmpty(fragmentHome.getCurrentUrl())){
            Toast.makeText(getApplicationContext(),"Please wait...",Toast.LENGTH_SHORT).show();
            return;
        }

        NativeHelper.getInstnace().hideKeyBoard(this);
        String TAG = FragmentTabs.class.getSimpleName();
        Fragment fragment=ParentFragment.newInstance("Tabs", false, DrawerConstants.DKEY_TABS);
        changeFragment(fragment, true, TAG, TAG);
    }
    private Fragment getFragmentByTag(String tag){
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    public void openSettingScreen() {
        GoogleTrackers.Companion.setSSettings();
        LocalyticsTrackers.Companion.setESettings();
        GoogleTrackers.Companion.setEHamburger_Menu_Settings();
//        LocalyticsTrackers.Companion.setEHamburger_Menu_Settings();
        String TAG = FragmentSetting.class.getSimpleName();
        changeFragment(ParentFragment.newInstance("Settings", false, DrawerConstants.DKEY_Settings), true, TAG, TAG);
    }

    public void openBookmarkScreen() {
        GoogleTrackers.Companion.setSBookmarks();
//        LocalyticsTrackers.Companion.setSBookmarks();
        GoogleTrackers.Companion.setEHamburger_Menu_Bookmarks();
//        LocalyticsTrackers.Companion.setEHamburger_Menu_Bookmarks();
        String TAG = FragmentHistory.class.getSimpleName();
        changeFragment(ParentFragment.newInstance(getResources().
                getString(R.string.bookmarks), false, DrawerConstants.DKEY_Bookmarks), true, TAG, TAG);
    }

    public void openHistoryScreen() {
        GoogleTrackers.Companion.setSHistory();
//        LocalyticsTrackers.Companion.setSHistory();
        GoogleTrackers.Companion.setEHamburger_Menu_History();
//        LocalyticsTrackers.Companion.setEHamburger_Menu_History();
        String TAG = FragmentHistory.class.getSimpleName();
        changeFragment(ParentFragment.newInstance(getResources().getString(R.string.history), false, DKEY_History), true, TAG, TAG);
    }

    private void popCurrentFragment(){
        FragmentManager manager=getSupportFragmentManager();
        if(manager!=null && manager.getBackStackEntryCount()>0){
            manager.popBackStackImmediate();
        }
    }

    private void handleClick(View view) {
        if (view.getId() != R.id.iv_settings) {
            hideDrawer();
        }
        switch (view.getId()) {

            case R.id.tracker_hint_layout:
                onClickTrackHint();
                break;
            case R.id.rl_share_land:
            case R.id.ll_share:
                callShareIntent();
                break;
            case R.id.ll_Vpn:
            case R.id.rl_Vpn_land:
                GoogleTrackers.Companion.setEHamburger_Menu_Vpn();
//                LocalyticsTrackers.Companion.setEHamburger_Menu_Vpn();
                openVPNScreen();
                break;
            case R.id.ll_settings:
            case R.id.rl_settings_land:
                if(!amIOnHomeFragment()){
                    popCurrentFragment();
                }
                openSettingScreen();
                break;
            case R.id.ll_bookmark:
            case R.id.rl_bookmark_land:
                openBookmarkScreen();
                break;
            case R.id.ll_history:
            case R.id.rl_history_land:
                openHistoryScreen();
                break;

            case R.id.reportButton:
                toggleReport();
                break;
            case R.id.lyt_safety_close:
                onClickReportBarAway();
                break;
            case R.id.txt_tracker_count:
            case R.id.lyt_tracker_count:
                trackerFabClick();
                break;
            case R.id.ll_about:
            case R.id.rl_about_land:
                GoogleTrackers.Companion.setSAbout();
//                LocalyticsTrackers.Companion.setSAbout();
                GoogleTrackers.Companion.setEHamburger_Menu_About();
//                LocalyticsTrackers.Companion.setEHamburger_Menu_About();
                openNormalWebView(DKEY_ABOUT);
                break;
            case R.id.ll_help:
            case R.id.rl_help_land:
                GoogleTrackers.Companion.setSHelp();
//                LocalyticsTrackers.Companion.setSHelp();
                GoogleTrackers.Companion.setEHamburger_Menu_Help();
//                LocalyticsTrackers.Companion.setEHamburger_Menu_Help();
                openNormalWebView(DKEY_HELP);
                break;
            case R.id.ll_rate:
            case R.id.rl_rate_land:
                callRate(view);
                break;
            case R.id.btn_layout_upgrade1:
                showAppPurchaseScreen();
                break;
        }
    }

    public void onClickReportBarAway() {
        doThisONBottomBarAway();
    }
    public void startTimer(){
        if(UserPref.getInstance().getRmSecs()>0){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                   onClickReportBarAway();
                }
            },UserPref.getInstance().getRmSecs()*1000);
        }
    }

    protected void showAppPurchaseScreen() {

        Fragment fragment = FragmentAppPurchase.newInstance(getString(R.string.app_purchase_title),
                false, DrawerConstants.DKEY_APP_PERCHASE_DIALOG);
        String tag = FragmentAppPurchase.class.getSimpleName();
        changeFragment(fragment, true, tag, tag);
    }

    private void callRecommendedIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String values = Utils.getRecommendedValue();
        sendIntent.putExtra(Intent.EXTRA_TEXT, values);
        sendIntent.setType("text/plain");
        startActivityForResult(Intent.createChooser(sendIntent, getString(R.string.share_via)), 1234);
    }

    public void callRate(View view) {
        openRateIntent();
        GoogleTrackers.Companion.setEHamburger_Menu_Rate();
//        LocalyticsTrackers.Companion.setEHamburger_Menu_Rate();
    }

    private void openRateIntent() {
        GoogleTrackers.Companion.setERate();
        LocalyticsTrackers.Companion.setERate();
        Intent i = new Intent(Intent.ACTION_VIEW);
        String url = PlistHelper.getInstance().getAppUrl();
        if (url != null && !url.isEmpty()) {
            i.setData(Uri.parse(url));
            startActivityForResult(i, 1234);
        }
    }

    private void openNormalWebView(String drawerConst) {
        if(!amIOnHomeFragment()){
            popCurrentFragment();
        }
        hideDrawer();
        String url="";
        String title="";
        switch (drawerConst){
            case DKEY_EULA:
                url=PlistHelper.getInstance().getEulaUrl();
                title=getString(R.string.end_user_license_agreement);
                break;
            case DKEY_ABOUT:
                url=PlistHelper.getInstance().getAboutUrl();
                title=getString(R.string.about);
                break;
            case DKEY_HELP:
                url=PlistHelper.getInstance().getHelpUrl();
                title=getString(R.string.help);
                break;
            case DKEY_PASSWORD_RESET:
                url=PlistHelper.getInstance().getPasswordResetUrl();
                title=getString(R.string.password_reset);
                break;
        }
        String TAG = FragmentNormalWebView.class.getSimpleName();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BKEY_URL, url);
        changeFragment(ParentFragment.newInstance(title, false, drawerConst, bundle), true, TAG, TAG);
    }


    private void trackerFabClick() {
        GlobalVariables.getInstnace().currentHomeFragment.onClickTrackerFab();

    }

    private void toggleReport() {

        if(UserPref.getInstance().getTrackerHintVisibility()){
            onClickTrackHint();
            return;
        }
        GlobalVariables.getInstnace().currentHomeFragment.toggleReport();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()!=R.id.tracker_hint_layout){
            if(UserPref.getInstance().getTrackerHintVisibility() && vg_tracker_hint.getVisibility()==View.VISIBLE){
                hideDrawer();
                return;
            }
        }
        handleClick(v);
    }

    public void changeBookMarkIcon(String url) {
        List<bookmarks> bookmarksListtemp = AnalyticsApplication.daoSession.getBookmarksDao().loadAll();
        Collections.reverse(bookmarksListtemp);
        for (int i = 0; i < bookmarksListtemp.size(); i++) {
            if (bookmarksListtemp.get(i).getUrl().trim().equalsIgnoreCase(url)) {
                iv_bookmark1.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_bookmark_white_24dp));
                iv_bookmark_land.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_bookmark_white_24dp));
                break;
            } else {
                iv_bookmark1.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_bookmark_border_white_24dp));
                iv_bookmark_land.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_bookmark_border_white_24dp));
            }
        }
    }

    public void setBookMarks(boolean isBookMark) {
        Drawable drawable = ResourceHelper.getInstance().getDrawable(R.drawable.ic_bookmark_white_24dp);
        if (isBookMark) {
            iv_bookmark1.setBackground(drawable);
            iv_bookmark_land.setBackground(drawable);
            iv_bookmark1.setTag(true);
            iv_bookmark_land.setTag(true);
            return;
        }
        drawable = ResourceHelper.getInstance().getDrawable(R.drawable.ic_bookmark_border_white_24dp);
        iv_bookmark1.setBackground(drawable);
        iv_bookmark_land.setBackground(drawable);
        iv_bookmark1.setTag(false);
        iv_bookmark_land.setTag(false);
    }

    private boolean isCurrenPageBookMarked() {
        if (isPortraitOrientation()) {
            if(iv_bookmark1.getTag()!=null && iv_bookmark1.getTag().equals(true)){
                return true;
            }
        }
        if (isLandscapedOrientation()) {
            if(iv_bookmark_land.getTag()!=null && iv_bookmark_land.getTag().equals(true)){
                return true;
            }
        }
        return false;
    }

    public boolean amIOnHomeFragment(){
        Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        return (fragment!=null && fragment instanceof FragmentHome);
    }

    /**
     * for future to direclty open a particular screen from home
     *
     * @param drawerConstants
     */
    private void moveToScreen(String drawerConstants){
        switch (drawerConstants){
            case DrawerConstants.DKEY_History:
                openHistoryScreen();
                break;
            case DrawerConstants.DKEY_Bookmarks:
                openBookmarkScreen();
                break;
            case DrawerConstants.DKEY_Settings:
                openSettingScreen();
                break;
            case DrawerConstants.DKEY_TABS:
                openTabsScreen();
                break;
            case DrawerConstants.DKEY_HOME:
                openBrowserHome();
                break;
            case DrawerConstants.DKEY_HELP:
                openNormalWebView(drawerConstants);
                GoogleTrackers.Companion.setSHelp();
//                LocalyticsTrackers.Companion.setSHelp();
                GoogleTrackers.Companion.setEHamburger_Menu_Help();
//                LocalyticsTrackers.Companion.setEHamburger_Menu_Help();
                break;
            case DrawerConstants.DKEY_ABOUT:
                openNormalWebView(drawerConstants);
                GoogleTrackers.Companion.setSAbout();
//                LocalyticsTrackers.Companion.setSAbout();
                GoogleTrackers.Companion.setEHamburger_Menu_About();
//                LocalyticsTrackers.Companion.setEHamburger_Menu_About();
                break;
            case DrawerConstants.DKEY_PASSWORD_RESET:
                openNormalWebView(drawerConstants);
                break;
            case DrawerConstants.DKEY_VPN:
                openVPNScreen();
                break;
            case DrawerConstants.DKEY_EULA:
                openNormalWebView(drawerConstants);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case READ_PHONE_STATE_PERMISSION:
                if (grantResults.length >= 0 &&grantResults.length>0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    initUpdateCountService();
                    if (Utils.isNetworkConnected(this)) {
                       openVPNScreen();
                    } else {
                        FinjanDialogBuilder.showNetworkError(this);
                    }

                } else if (permissions.length>0 &&
                        ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, permissions[0])) {
                    FinjanDialogBuilder.showPermissionError(HomeActivity.this);
                }
                break;
        }
    }
}
