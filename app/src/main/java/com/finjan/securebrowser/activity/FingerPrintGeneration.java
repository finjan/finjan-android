package com.finjan.securebrowser.activity;

import android.Manifest;
import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import androidx.core.app.ActivityCompat;

import android.widget.Toast;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 3/10/17.
 *
 * Class responsible for validation of user bio info for finger scan
 */

public class FingerPrintGeneration extends ParentActivity {

    private static final String KEY_NAME = "touch_id";
    private static int SCREEN_LOCK = 0;
    private static int FINGERPRINT = 1;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private Cipher cipher;
    private FingerprintManager.CryptoObject cryptoObject;
    //    Preferences pref;
    private boolean isSettingCalled, isFingurePrintSettingCalled;

    private void setPopUpWhenPassowrdNotSet() {
        if (isSettingCalled) {

            new FinjanDialogBuilder(this).setMessage(getString(R.string.lockscreen_security_yet_not_enabled_in_Setting))
                    .setPositiveButton(getString(R.string.action_settings))
                    .setNegativeButton(getString(R.string.alert_cancel))
                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                        @Override
                        public void onPositivClick() {
                            startActivityForResult(new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD), SCREEN_LOCK);
                        }
                    }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                @Override
                public void onNegativeClick() {
                    if(GlobalVariables.getInstnace().fragmentSetting !=null &&
                            GlobalVariables.getInstnace().appCodeSetNewly){
                        GlobalVariables.getInstnace().fragmentSetting.setPasscordOnResult(true);
                    }
                    FingerPrintGeneration.this.finish();
                }
            }).setCancelable(false).outSideClickable(false).show();
        }
    }

    private void setPopUpWhenFingurePringNotSet() {
        if (isFingurePrintSettingCalled) {
            new FinjanDialogBuilder(this).setMessage(getString(R.string.no_fingure_registered_yet))
                    .setPositiveButton(getString(R.string.action_settings))
                    .setNegativeButton(getString(R.string.alert_cancel))
                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                        @Override
                        public void onPositivClick() {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS), FINGERPRINT);
                        }
                    }).setDialogClickListener(new FinjanDialogBuilder.NegativeClickListener() {
                @Override
                public void onNegativeClick() {
                    FingerPrintGeneration.this.finish();
                }
            }).setCancelable(false).outSideClickable(false).show();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passcode_orientation);
//        pref = new Preferences(FingerPrintGeneration.this);
        try {
            keyguardManager =
                    (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            fingerprintManager =
                    (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

            if (!keyguardManager.isKeyguardSecure()) {
                callingKeyguardSetting();
//                isSettingCalled = true;
//                Toast.makeText(getApplicationContext(),
//                        "Lock screen security not enabled in Settings",
//                        Toast.LENGTH_SHORT).show();
//                startActivityForResult(new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD), SCREEN_LOCK);

                return;
            }

            if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.USE_FINGERPRINT) !=
                    PackageManager.PERMISSION_GRANTED) {


                Toast.makeText(getApplicationContext(),
                        getString(R.string.fingerprint_authentication_permission_not_enabled),
                        Toast.LENGTH_SHORT).show();
                return;
            }

            if (!fingerprintManager.hasEnrolledFingerprints()) {
                    callingFigerAuthActivity();
//                isFingurePrintSettingCalled = true;
//                UserPref.getInstance().setTouchId(true);
//
//                // This happens when no fingerprints are registered.
//                Toast.makeText(getApplicationContext(),
//                        "Register at least one fingerprint in Settings",
//                        Toast.LENGTH_SHORT).show();
//                startActivityForResult(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS), FINGERPRINT);
                return;
            } else {
                UserPref.getInstance().setTouchId(true);

                // This happens when no fingerprints are registered.
//                Toast.makeText(getApplicationContext(),
//                        "Register more fingerprints in Settings",
//                        Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(),
                        getString(R.string.registered_fingerprints_in_settings_found),
                        Toast.LENGTH_SHORT).show();
                Intent intent=new Intent();
                intent.putExtra(AppConstants.IKEY_FINGER_REGISTERED,true);
                setResult(AppConstants.AFR_REGISTER_FINGER,intent);
                finish();
//                startActivityForResult(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS), FINGERPRINT);

            }

            generateKey();

            if (cipherInit()) {
                cryptoObject =
                        new FingerprintManager.CryptoObject(cipher);
                FingerprintHandler helper = new FingerprintHandler(getApplicationContext());
                helper.startAuth(fingerprintManager, cryptoObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to setPromoList Cipher", e);
        }
    }

    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            keyGenerator = KeyGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES,
                    "AndroidKeyStore");
        } catch (NoSuchAlgorithmException |
                NoSuchProviderException e) {
            throw new RuntimeException(
                    "Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(AppConstants.AFR_REGISTER_FINGER,null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FINGERPRINT) {

            try {

                keyguardManager =
                        (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
                fingerprintManager =
                        (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                if (!keyguardManager.isKeyguardSecure()) {
                    callingKeyguardSetting();
                    return;
                }

                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.USE_FINGERPRINT) !=
                        PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this,
                            "Fingerprint authentication permission not enabled",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    callingFigerAuthActivity();
                    return;
                }

                generateKey();

                if (cipherInit()) {
                    cryptoObject =
                            new FingerprintManager.CryptoObject(cipher);
                    FingerprintHandler helper = new FingerprintHandler(this);
                    helper.startAuth(fingerprintManager, cryptoObject);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent=new Intent();
            intent.putExtra(AppConstants.IKEY_FINGER_REGISTERED,true);
            setResult(AppConstants.AFR_REGISTER_FINGER,intent);
            finish();
        } else if (requestCode == SCREEN_LOCK) {
            try {

                keyguardManager =
                        (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
                fingerprintManager =
                        (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                if (!keyguardManager.isKeyguardSecure()) {
                    callingKeyguardSetting();
                    return;
                }

                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.USE_FINGERPRINT) !=
                        PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this,
                            getString(R.string.fingerprint_authentication_permission_not_enabled),
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    callingFigerAuthActivity();
                    return;
                }

                generateKey();

                if (cipherInit()) {
                    cryptoObject =
                            new FingerprintManager.CryptoObject(cipher);
                    FingerprintHandler helper = new FingerprintHandler(this);
                    helper.startAuth(fingerprintManager, cryptoObject);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            setResult(AppConstants.AFR_REGISTER_FINGER,null);
            finish();
//            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            finish();
        }
    }

    private void checkifAnyFingerEnabled() {

    }

    private void callingFigerAuthActivity() {
        if (isFingurePrintSettingCalled) {
            setPopUpWhenFingurePringNotSet();
            return;
        }
        isFingurePrintSettingCalled = true;
        UserPref.getInstance().setTouchId(true);

        // This happens when no fingerprints are registered.
        Toast.makeText(this,
                getString(R.string.register_at_least_one_fingerprint_in_settings),
                Toast.LENGTH_SHORT).show();
        startActivityForResult(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS), FINGERPRINT);
    }

    private void callingKeyguardSetting() {
        if (isSettingCalled) {
            setPopUpWhenPassowrdNotSet();
            return;
        }
        isSettingCalled = true;
        Toast.makeText(this,
                getString(R.string.lock_screen_security_not_enabled_in_settings),
                Toast.LENGTH_SHORT).show();
        startActivityForResult(new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD), SCREEN_LOCK);
    }


    public class FingerprintHandler extends
            FingerprintManager.AuthenticationCallback {

        private CancellationSignal cancellationSignal;
        private Context appContext;

        public FingerprintHandler(Context context) {
            appContext = context;
        }

        public void startAuth(FingerprintManager manager,
                              FingerprintManager.CryptoObject cryptoObject) {

            cancellationSignal = new CancellationSignal();

            if (ActivityCompat.checkSelfPermission(appContext,
                    Manifest.permission.USE_FINGERPRINT) !=
                    PackageManager.PERMISSION_GRANTED) {
                return;
            }
            manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }

        @Override
        public void onAuthenticationError(int errMsgId,
                                          CharSequence errString) {
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId,
                                         CharSequence helpString) {

        }

        @Override
        public void onAuthenticationFailed() {

        }

        @Override
        public void onAuthenticationSucceeded(
                FingerprintManager.AuthenticationResult result) {
        }
    }

}
