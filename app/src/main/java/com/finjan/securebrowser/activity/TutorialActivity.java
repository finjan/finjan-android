package com.finjan.securebrowser.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.adapter.TutorialAdapter;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.util.AppConfig;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;
import com.finjan.securebrowser.vpn.ui.authentication.SetupAccountActivity;
import com.finjan.securebrowser.vpn.ui.main.VpnActivity;
import com.finjan.securebrowser.vpn.util.PermissionUtil;
import com.finjan.securebrowser.vpn.util.TrafficController;

import static com.finjan.securebrowser.constants.AppConstants.READ_PHONE_STATE_PERMISSION;

/**
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Screen responsible for showing Tutorial screen
 */

public class TutorialActivity extends ParentActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {

    static final int REQUEST_UPGRADE_AFTER_LOGIN = 100;
    protected View view;
    private TextView btnNext, btnSkip;
    private TextView tvSkip;
    private ViewPager intro_images;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private TutorialAdapter mAdapter;
   /* private String[] mImageResourcesForVPN = {
           *//* "screen_1.json",
            "screen_2.json",
            "screen_3.json",
            "screen_4.json"*//*
            TextUtils.isEmpty(PlistHelper.getInstance().getTutorialScreenList().get(0))?"screen_1.json":PlistHelper.getInstance().getTutorialScreenList().get(0),
            TextUtils.isEmpty(PlistHelper.getInstance().getTutorialScreenList().get(1))?"screen_2.json":PlistHelper.getInstance().getTutorialScreenList().get(1),
            TextUtils.isEmpty(PlistHelper.getInstance().getTutorialScreenList().get(2))?"screen_3.json":PlistHelper.getInstance().getTutorialScreenList().get(2),
            TextUtils.isEmpty(PlistHelper.getInstance().getTutorialScreenList().get(3))?"screen_4.json":PlistHelper.getInstance().getTutorialScreenList().get(3)
    };*/
   private String[] mImageResourcesForVPN;

    public static void finishTutorial(Activity context) {
        ScreenNavigation.getInstance().startHome(context, "");
        UserPref.getInstance().setIsSetUpDone(true);
        UserPref.getInstance().setIsTutorialScreenShowed(true);
        context.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_tutorial);
            addResourse();
        view = findViewById(R.id.container);
        setReference();
        NativeHelper.getInstnace().changeStatusBarTransparent(this);
        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_to_left);
        showInvincibullUpdateMsg();
    }

    private void showInvincibullUpdateMsg(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(UserPref.getInstance().isAppUpgraded())
                    new FinjanDialogBuilder(TutorialActivity.this).setMessage(PlistHelper.getInstance().getInvincibull_update())
                            .setPositiveButton(getString(R.string.alert_ok))
                            .show();
                UserPref.getInstance().consumedAppUpgraded();
            }
        },10);

    }
    private void addResourse()
    { mImageResourcesForVPN = new String[4];
        try{
        mImageResourcesForVPN[0] =PlistHelper.getInstance() == null || PlistHelper.getInstance().getTutorialScreenList() == null || PlistHelper.getInstance().getTutorialScreenList().size() > 0 || TextUtils.isEmpty(PlistHelper.getInstance().getTutorialScreenList().get(0))?"screen_1.json":(PlistHelper.getInstance().getTutorialScreenList().get(0).contains(".json")?PlistHelper.getInstance().getTutorialScreenList().get(0):PlistHelper.getInstance().getTutorialScreenList().get(0)+".json");
        }catch (Exception ex){
            mImageResourcesForVPN[0] = "screen_1.json";
        }
        try{
        mImageResourcesForVPN[1] =PlistHelper.getInstance() == null || PlistHelper.getInstance().getTutorialScreenList() == null || PlistHelper.getInstance().getTutorialScreenList().size() > 1 || TextUtils.isEmpty(PlistHelper.getInstance().getTutorialScreenList().get(1))?"screen_2.json":(PlistHelper.getInstance().getTutorialScreenList().get(1).contains(".json")?PlistHelper.getInstance().getTutorialScreenList().get(1):PlistHelper.getInstance().getTutorialScreenList().get(1)+".json");
        }catch (Exception ex){
            mImageResourcesForVPN[1] = "screen_2.json";
        }
        try{
        mImageResourcesForVPN[2] =PlistHelper.getInstance() == null || PlistHelper.getInstance().getTutorialScreenList() == null || PlistHelper.getInstance().getTutorialScreenList().size() > 2 || TextUtils.isEmpty(PlistHelper.getInstance().getTutorialScreenList().get(2))?"screen_3.json":(PlistHelper.getInstance().getTutorialScreenList().get(2).contains(".json")?PlistHelper.getInstance().getTutorialScreenList().get(2):PlistHelper.getInstance().getTutorialScreenList().get(2)+".json");
        }catch (Exception ex){
            mImageResourcesForVPN[2] = "screen_3.json";
        }
        try{
        mImageResourcesForVPN[3] =PlistHelper.getInstance() == null || PlistHelper.getInstance().getTutorialScreenList() == null || PlistHelper.getInstance().getTutorialScreenList().size() > 3 || TextUtils.isEmpty(PlistHelper.getInstance().getTutorialScreenList().get(3))?"screen_4.json":(PlistHelper.getInstance().getTutorialScreenList().get(3).contains(".json")?PlistHelper.getInstance().getTutorialScreenList().get(3):PlistHelper.getInstance().getTutorialScreenList().get(3)+".json");
        }catch (Exception ex){
            mImageResourcesForVPN[3] = "screen_4.json";
        }
    }
    public void setReference() {
        intro_images = (ViewPager) view.findViewById(R.id.pager_introduction);
        intro_images.setOffscreenPageLimit(0);
        btnNext = (TextView) view.findViewById(R.id.btn_next);
        tvSignUp = (TextView) view.findViewById(R.id.tv_sign_up);
        tvAlreadyMember = (TextView) view.findViewById(R.id.tv_sign_up_later);
        tvSignUp.setOnClickListener(this);
        tvAlreadyMember.setOnClickListener(this);
        btnSkip = (TextView) view.findViewById(R.id.btn_finish);
        btnNext.setTextColor(ResourceHelper.getInstance().getColor(R.color.white));
        btnSkip.setTextColor(ResourceHelper.getInstance().getColor(R.color.white));
        tvSkip = (TextView) view.findViewById(R.id.tv_skip);
        tvSkip.setText(Html.fromHtml("<u>" + getString(R.string.skip) + "</u>"));
        pager_indicator = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);

        btnNext.setVisibility(View.VISIBLE);
        btnSkip.setVisibility(View.VISIBLE);
        btnNext.setOnClickListener(this);
        btnSkip.setOnClickListener(this);

        mAdapter = new TutorialAdapter(TutorialActivity.this, UserPref.getInstance().getIsBrowser() ? mImageResourcesForVPN: mImageResourcesForVPN);
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        intro_images.setOnPageChangeListener(this);
        setUiPageViewController();

        new Thread(new Runnable() {
            @Override
            public void run() {
                GoogleTrackers.Companion.setSOnboarding1();
                LocalyticsTrackers.Companion.setEOnboarding1();
            }
        }).start();

    }


    private TextView tvSignUp,tvAlreadyMember;

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.bg_tutorial_noselect_item));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(20, 0, 20, 0);
            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.bg_tutorial_select_item));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if (intro_images.getCurrentItem() < 4) {
                    intro_images.setCurrentItem(intro_images.getCurrentItem() + 1);
                }
                break;
            case R.id.btn_finish:
                intro_images.setCurrentItem(4);
                break;
            case R.id.tv_sign_up:
                askPermission(true);
                break;
            case R.id.tv_sign_up_later:
                askPermission(false);
                break;
        }
    }
    private boolean isLetsBegin;


    private void askPermission(final boolean isLetsBegin){
        this.isLetsBegin=isLetsBegin;
        if(!AppConfig.Companion.getRequestPhoneStatePermission()){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    UserPref.getInstance().setMyLastPage(ScreenNavigation.VPN_SCREEN);
                    if(isLetsBegin){
                        onClickLetsBegin();
                    }else onClickAlreadyAmember();
                }
            },50);
        }else if(PermissionUtil.isPhoneReadStatePermissionGranted(this)){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    UserPref.getInstance().setMyLastPage(ScreenNavigation.VPN_SCREEN);
                    if(isLetsBegin){
                        onClickLetsBegin();
                    }else onClickAlreadyAmember();
                }
            },50);
        }else {
            PermissionUtil.askPhoneStatePermission(this);
        }
    }
    private void onClickLetsBegin(){
        if(TrafficController.getInstance(this).isRegistered()){
            finishTutorial(this);
            return;
        }
        SetupAccountActivity.newLoginInstanceForResult( this,
                VpnActivity.REQUEST_NAV, SetupAccountActivity.ACTION_SIGN_NAV);

    }
    private void onClickAlreadyAmember(){
        if(TrafficController.getInstance(this).isRegistered()){
            finishTutorial(this);
            return;
        }
        SetupAccountActivity.newLoginInstanceForResult( this,
                VpnActivity.REQUEST_LOGIN, SetupAccountActivity.ACTION_SIGN_IN);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        switch (position){
            case 0:
                GoogleTrackers.Companion.setSOnboarding1();
                LocalyticsTrackers.Companion.setEOnboarding1();
                break;
            case 1:
                GoogleTrackers.Companion.setSOnboarding2();
                LocalyticsTrackers.Companion.setEOnboarding2();
                break;
            case 2:
                GoogleTrackers.Companion.setSOnboarding3();
                LocalyticsTrackers.Companion.setEOnboarding3();
                break;
            case 3:
                GoogleTrackers.Companion.setSOnboarding4();
                LocalyticsTrackers.Companion.setEOnboarding4();
                break;
        }
        if(mAdapter.getLoggtieImg(position)!=null){
            mAdapter.getLoggtieImg(position).playAnimation();
        }
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.bg_tutorial_noselect_item));
            }
            dots[position].setImageDrawable(getResources().getDrawable(R.drawable.bg_tutorial_select_item));

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case VpnActivity.REQUEST_NAV:
                if (resultCode == VpnActivity.RESULT_SIGNUP) {
                   onSignUpSuccess(data);
                }else {onSignInSuccess(data);}
                break;
            case VpnActivity.REQUEST_LOGIN:
                if (resultCode == VpnActivity.RESULT_LOGIN) {
                    onSignInSuccess(data);
                }else {onSignUpSuccess(data);}
                break;
        }
    }

    private void onSignUpSuccess(Intent data){
        if(data!=null && data.getBooleanExtra("success",false)){
            finishTutorial(this);
//            startActivity(VpnHomeFragment.upgradeIntent(this, true, true));
            GlobalVariables.getInstnace().fixForPopPupAfterRegistrationFromHome();
        }else {
            NativeHelper.getInstnace().showToast(getString(R.string.unable_to_authenticate_you_try_again_after_some_time),Toast.LENGTH_SHORT);
        }
    }


    private void onSignInSuccess(Intent data){
        if(data!=null && data.getBooleanExtra("success",false)){
            finishTutorial(this);
//            startActivity(VpnHomeFragment.upgradeIntent(this, true, true));
            GlobalVariables.getInstnace().fixForPopPupAfterRegistrationFromHome();
        }else {
            NativeHelper.getInstnace().showToast(getString(R.string.unable_to_authenticate_you_try_again_after_some_time),Toast.LENGTH_SHORT);
//            new FinjanDialogBuilder(this).setMessage(getString(R.string.action_cancelled)).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case READ_PHONE_STATE_PERMISSION:
                if (grantResults.length >= 0 &&grantResults.length>0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Utils.isNetworkConnected(this)) {
                        askPermission(isLetsBegin);
                    } else {
                        FinjanDialogBuilder.showNetworkError(this);
                    }

                } else if (permissions.length>0 &&
                        ActivityCompat.shouldShowRequestPermissionRationale(TutorialActivity.this, permissions[0])) {
                    FinjanDialogBuilder.showPermissionError(TutorialActivity.this);
                }
                break;
        }
    }
}
