package com.finjan.securebrowser.activity;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.avira.common.backend.WebUtility;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.model.RedeemListData;
import com.finjan.securebrowser.vpn.controller.network.NetworkManager;
import com.finjan.securebrowser.vpn.controller.network.NetworkResultListener;
import com.finjan.securebrowser.vpn.eventbus.AllPromoFetched;
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 * Class responsible for  showing Redeem Screen that will check if entered redeem code is valid or not.
 */


public class RedeemActivity extends AppCompatActivity {
    private LinearLayout llback;

    private TextView btnReedeem,tvSkip;
    private EditText edOfferCode;
//    private ImageView ivVarifyDone;
    private LinearLayout vg_loader;

    @SuppressWarnings("unused")
    public void onEventMainThread(final AllPromoFetched event) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private String redeemCode;
    private void setView(){
        Intent intent = getIntent();
        if(intent.getExtras()==null){return;}
        if(intent.getExtras().getString("redeemcode") !=null){
            redeemCode = intent.getExtras().getString("redeemcode");
            edOfferCode.setText(redeemCode);
        }
    }

    public Object checkCode(String redeemCode){
        ArrayList<RedeemListData> list = PromoHelper.Companion.getInstance().getAllPromoList(this);
        if(list==null){
            return null;
        }
        for (RedeemListData redeemListData: list){
            if (redeemListData.getAttributes().getRedeem_code().equals(redeemCode)){
                return redeemListData.getAttributes().getIapDiscount().getAndroid();
            }
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Hi guys, please, never ever remove this part of code from onDestroy
        // additionally do not forget to check isAdded()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem);
        initViews();
    }

    private boolean disableRedeemValidity =true;

    private void initViews()
    {
        btnReedeem = (TextView)findViewById(R.id.btn_redeem_offer);
        vg_loader = findViewById(R.id.vg_loader);
        edOfferCode = (EditText) findViewById(R.id.et_redeem_offer_code);
//        ivVarifyDone = (ImageView) findViewById(R.id.verify_done);

        InputMethodManager inputMethodManager =
                (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                findViewById(R.id.rl_redeem).getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
        llback = (LinearLayout)findViewById(R.id.ll_back);
        llback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnReedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String redeemCode = edOfferCode.getText().toString();
                if(TextUtils.isEmpty(redeemCode)){
                    Toast.makeText(RedeemActivity.this, getString(R.string.please_enter_redeem_code), Toast.LENGTH_SHORT).show();
                    return;
                }
                Object value = checkCode(redeemCode);
                if(value== null){return;}
                if(value instanceof Boolean && !(Boolean) value){
                    Toast.makeText(RedeemActivity.this, getString(R.string.you_entered_wrong_redeem_key), Toast.LENGTH_SHORT).show();
                    return;
                }else if(value instanceof ArrayList){
//                    startActivity(VpnHomeFragment.getDiscountedIntent(RedeemActivity.this,(ArrayList<String>) value,"",));
                    return;
                }
                vg_loader.setVisibility(View.VISIBLE);
              //
                if(disableRedeemValidity){return;}
               NetworkManager.getInstance(RedeemActivity.this).redeemCode(RedeemActivity.this, edOfferCode.getText().toString().trim(), new NetworkResultListener() {
                   @Override
                   public void executeOnSuccess(JSONObject response) {

                       vg_loader.setVisibility(View.GONE);
                       Toast.makeText(RedeemActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

                   }

                   @Override
                   public void executeOnError(VolleyError error) {
                       vg_loader.setVisibility(View.GONE);
                       String response = WebUtility.getMessage(error);
                       if(!TextUtils.isEmpty(response)){
                           if(response.contains("message")){
                               try {
                                   JSONObject jsonObject = new JSONObject(response);
                                   String msg = jsonObject.getString("message");
                                   Toast.makeText(RedeemActivity.this, msg, Toast.LENGTH_SHORT).show();
                               } catch (JSONException e) {
                                   e.printStackTrace();
                               }
                           }
                       }

                   }
               });
            }
        });
    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.enter_to_right);
    }
}
