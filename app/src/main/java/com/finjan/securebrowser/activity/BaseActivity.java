package com.finjan.securebrowser.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringListener;
import com.facebook.rebound.SpringSystem;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.constants.DrawerConstants;
import com.finjan.securebrowser.eventbus_pojo.UploadingMsg;
import com.finjan.securebrowser.fragment.FragmentHistory;
import com.finjan.securebrowser.fragment.FragmentHome;
import com.finjan.securebrowser.fragment.FragmentNormalWebView;
import com.finjan.securebrowser.fragment.FragmentReport;
import com.finjan.securebrowser.fragment.FragmentSetting;
import com.finjan.securebrowser.fragment.FragmentTabs;
import com.finjan.securebrowser.fragment.ParentFragment;
import com.finjan.securebrowser.fragment.ParentHomeFragment;
import com.finjan.securebrowser.fragment.SearchedResultFragment;
import com.finjan.securebrowser.helpers.DbHelper;
import com.finjan.securebrowser.helpers.DownloadHelper;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.security.WifiSecurityHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.model.SkuModel;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.util.dialogs.FinjanDialogBuilder;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;

import static com.finjan.securebrowser.constants.DrawerConstants.DKEY_HOME;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 3/10/17.
 *
 * Base class of all activities that contain some common methods for all activites.
 */

public class BaseActivity extends AppPurchaseActivity implements SpringListener {

    private static final String TAG = BaseActivity.class.getSimpleName();
    private static BaseActivity Instance;
    public boolean isLoaderVisible;
    protected View parentView;
    protected boolean isDataSetledEarlier;
    private Bundle changeCarrier;
    ;
    private TextView tvTitle;
    private HashMap<Long, Fragment> fragmentMap = new HashMap<>();
    private LinearLayout ll_Finzen_loader;
    private Spring headerTitle;
    private RelativeLayout rlNonSecureLoader;
    private MediaPlayer mPlay, mPlay2;
    private Handler loaderNotResponding = new Handler();
    private Runnable loaderRunnable = new Runnable() {
        @Override
        public void run() {
            if (isLoaderVisible) {
                Logger.logE(TAG, "loader runnable called");
                isLoaderVisible = false;
//                Toast.makeText(getApplicationContext(),"Please try again...",Toast.LENGTH_SHORT).show();
                finzenLoaderVisibility(View.GONE, GlobalVariables.getInstnace().currentHomeFragment);
            }
        }
    };

    //HomeAcitivty references
    public static BaseActivity getInstance() {
        return Instance;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        parentView = findViewById(R.id.parentView);
        Instance = this;
        setReferences();
        initailizeSprings();
    }

    public Bundle getChangeCarrier() {
        return changeCarrier;
    }

    public void setChangeCarrier(Bundle bundle) {
        this.changeCarrier = bundle;
    }

    @Override
    protected void serviceResult(ArrayList<SkuModel> responseList) {
        GlobalVariables.getInstnace().responseList = responseList;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.logE("onStop", "" + isDataSetledEarlier);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.logE("onDestroy", "" + isDataSetledEarlier);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void serviceError(Error error) {
    }

    private void setReferences() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        ll_Finzen_loader = (LinearLayout) findViewById(R.id.vg_loader);
        rlNonSecureLoader = (RelativeLayout) findViewById(R.id.rl_non_secure_loader);
    }

    protected void setData() {
        ll_Finzen_loader.setVisibility(View.GONE);
        isDataSetledEarlier = true;
        setDefaults();
        openBrowserHome();
    }


    private void setDefaults() {
        setDefaultSprings();
    }



    protected void openBrowserHome() {
        if(TextUtils.isEmpty(GlobalVariables.getInstnace().lastDirectUrl) && !UserPref.getInstance().getIsBrowser()){
            if(getIntent()==null || TextUtils.isEmpty(getIntent().getStringExtra("screen")) ){
                String TAG = FragmentSetting.class.getSimpleName();
                changeFragment(ParentFragment.newInstance("Settings", false, DrawerConstants.DKEY_Settings), true, TAG, TAG);
                return;
            }
        }
        Fragment fragment = FragmentHome.newInstance("", true, DKEY_HOME);
        fragmentMap.put(UserPref.getInstance().getLastId(), fragment);
        changeFragment(fragment, false, FragmentHome.TAG, FragmentHome.TAG);
    }

    public void updateFragmentMap(Long id, Fragment fragment) {
        if (fragmentMap == null) {
            return;
        }
        Long requiredKey = null;
        for (Long key : fragmentMap.keySet()) {
            Fragment mapFragment = fragmentMap.get(key);
            if (mapFragment.equals(fragment)) {
                requiredKey = key;
                break;
            }
        }
        if (requiredKey != null) {
            fragmentMap.remove(requiredKey);
            fragmentMap.put(id, fragment);
        }
    }

    public void updateFragmentId(Long oldId, Long newId) {
        if (fragmentMap == null) {
            return;
        }
        Fragment fragment = fragmentMap.get(oldId);
        fragmentMap.remove(oldId);
        fragmentMap.put(newId, fragment);
        ((FragmentHome) fragment).id = newId;
    }

    public void addFragment(Long id, Fragment fragment) {
        fragmentMap.put(id, fragment);
    }

    public HashMap<Long, Fragment> getFragmentMap() {
        return fragmentMap;
    }

    public void clearAllFragments() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (ft == null) {
            return;
        }
        for (Long id : fragmentMap.keySet()) {
            Fragment fragment = fragmentMap.get(id);
            if (fragment != null) {
                ft.remove(fragment);
            }
            ft.commitAllowingStateLoss();
        }
        fragmentMap.clear();
    }

    public void clearAllPrivateFragments() {
        if (fragmentMap != null && fragmentMap.size() > 0) {
            ArrayList<Long> idToRemove = new ArrayList<>();
            for (Long id : fragmentMap.keySet()) {
                if (fragmentMap.get(id) != null && ((ParentHomeFragment) fragmentMap.get(id)).isPrivate) {
                    idToRemove.add(id);
                }
            }
            for (Long id : idToRemove) {
                fragmentMap.remove(id);
            }
        }
    }

    public void clearAllNonPrivateFragment() {
        if (fragmentMap != null && fragmentMap.size() > 0) {
            ArrayList<Long> idToRemove = new ArrayList<>();
            for (Long id : fragmentMap.keySet()) {
                if (fragmentMap.get(id) != null && !((ParentHomeFragment) fragmentMap.get(id)).isPrivate) {
                    idToRemove.add(id);
                }
            }
            for (Long id : idToRemove) {
                fragmentMap.remove(id);
            }
        }
    }

    public void updateFragmentID(FragmentHome fragmentHome, Long id) {
        if (fragmentMap == null) {
            return;
        }
        for (Long ids : fragmentMap.keySet()) {
            FragmentHome fragmentHome1 = (FragmentHome) fragmentMap.get(ids);
            if (fragmentHome1 == fragmentHome) {
                fragmentMap.remove(ids);
                fragmentMap.put(id, fragmentHome);
                return;
            }
        }
    }

    private void initailizeSprings() {
        SpringSystem springSystem = SpringSystem.create();
        headerTitle = springSystem.createSpring();
        headerTitle.addListener(this);
    }

    private void setDefaultSprings() {
        headerTitle.setCurrentValue(-10);
    }

    @Override
    public void onSpringUpdate(Spring spring) {
    }

    @Override
    public void onSpringAtRest(Spring spring) {
    }

    @Override
    public void onSpringActivate(Spring spring) {
    }

    @Override
    public void onSpringEndStateChange(Spring spring) {
    }

    private String currentFragment;
    public void changeFragment(Fragment fragment, boolean addToBackStack, String backStackTag, String fragmentTag) {

        if (fragment == null || fragment.isAdded()) {
            return;
        }
        if(TextUtils.isEmpty(fragmentTag) || (!TextUtils.isEmpty(currentFragment) && fragmentTag.equals(FragmentHome.TAG)
        && !currentFragment.equals(FragmentHome.TAG))){
            GoogleTrackers.Companion.setSBrowser();
//            LocalyticsTrackers.Companion.setSBrowser();
        }
        currentFragment=fragmentTag;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (fragment instanceof FragmentHome) {
            ft.setCustomAnimations(R.anim.enter_from_left, R.anim.enter_to_right);
            ft.replace(R.id.fragment_container, fragment);
//            ft.commit();
        } else if (fragment instanceof FragmentReport || fragment instanceof SearchedResultFragment) {
//            showBottomToUpFragment(fragment,GlobalVariables.getInstnace().currentHomeFragment,backStackTag);
            ft.setCustomAnimations(R.anim.enter_from_bottom, R.anim.enter_to_top, R.anim.enter_from_top, R.anim.enter_to_bottom);
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            if (addToBackStack) {
                if (!TextUtils.isEmpty(backStackTag)) {
                    ft.addToBackStack(backStackTag);
                } else {
                    ft.addToBackStack(null);
                }
            }
        } else {
//            showNormalFragment(fragment,GlobalVariables.getInstnace().currentHomeFragment,backStackTag);
            ft.setCustomAnimations(R.anim.enter_from_right, R.anim.enter_to_left, R.anim.enter_from_left, R.anim.enter_to_right);
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            if (addToBackStack) {
                if (!TextUtils.isEmpty(backStackTag)) {
                    ft.addToBackStack(backStackTag);
                } else {
                    ft.addToBackStack(null);
                }
            }
        }
        try {
            ft.commitAllowingStateLoss();
        } catch (Exception e) {
            Logger.logE("changeFragment", " " + e.getMessage());
        }
    }

    public void hideNormalFragment(Fragment removeFragment, Fragment showFragment) {
        if (removeFragment != null && showFragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.enter_from_left, R.anim.enter_to_right, R.anim.enter_from_right, R.anim.enter_to_left);
            ft.remove(removeFragment);
            ft.show(showFragment);
            ft.commit();
        }
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setLoaderAnimaation();
    }

    private void setLoaderAnimaation() {
        ImageView iv_gif1 = (ImageView) findViewById(R.id.iv_gif);
        iv_gif1.setImageResource(R.drawable.loader_animation);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ((Animatable) iv_gif1.getDrawable()).start();
        } else {
            ((AnimationDrawable) iv_gif1.getDrawable()).start();
        }
    }

    public void finzenLoaderVisibility(int visibility, FragmentHome fragment) {
        if(!UserPref.getInstance().getIsBrowser()){
            return;
        }
        if (UserPref.getInstance().getSafeScanEnabled() ||
                (GlobalVariables.getInstnace().currentHomeFragment != null &&
                        GlobalVariables.getInstnace().currentHomeFragment.vtScan)) {
            rlNonSecureLoader.setVisibility(View.GONE);
            if (visibility == View.GONE) {
                isLoaderVisible = false;
                loaderNotResponding.removeCallbacks(loaderRunnable);
                ll_Finzen_loader.setVisibility(View.GONE);
                return;
            }
            if (fragment.isVisible() && !UserPref.getInstance().getTrackerHintVisibility()) {
                isLoaderVisible = true;
                loaderNotResponding.postDelayed(loaderRunnable, 1000 * 30);
                ll_Finzen_loader.setVisibility(visibility);
            }
        } else {
            ll_Finzen_loader.setVisibility(View.GONE);
            if (visibility == View.GONE) {
                rlNonSecureLoader.setVisibility(View.GONE);
                return;
            }
            if (fragment.isVisible()) {
                rlNonSecureLoader.setVisibility(visibility);
            }
        }
    }

    public boolean isLoaderVisible() {
        return ll_Finzen_loader.getVisibility() == View.VISIBLE || rlNonSecureLoader.getVisibility() == View.VISIBLE;
    }


    public void nonSecureLoaderVisibility(int visibility) {

        if (ll_Finzen_loader.getVisibility() == View.VISIBLE) {
            ll_Finzen_loader.setVisibility(View.GONE);
        }
        rlNonSecureLoader.setVisibility(visibility);

    }

    public void activityLevelLoader(int visibility) {
        ll_Finzen_loader.setVisibility(visibility);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(permissions.length==0|| grantResults.length==0){return;}
        switch (requestCode) {
            case AppConstants.STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    DownloadHelper.getInstance().downloadFile(BaseActivity.this,"",GlobalVariables.getInstnace().downloadURL,
                            GlobalVariables.getInstnace().downloadfileName);
                } else {
                    Toast.makeText(this, getString(R.string.unabel_to_download), Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.READ_PHONE_STATE_PERMISSION:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Intent intent = new Intent(this, UpdateSafeCountService.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("type", Constants.UPDATE_PARAM);
//                    bundle.putString("uuid", UserPref.getInstance().getDeviceID());
//                    intent.putExtras(bundle);
//                    this.startService(intent);
//                } else {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])) {
//                        FinjanDialogBuilder.showPermissionDeclinedAlert(this);
//                    }
//                }
                break;
            case AppConstants.LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   if(GlobalVariables.getInstnace().fragmentSetting!=null){
                       GlobalVariables.getInstnace().fragmentSetting.onClickVpnAutoConnect(true,true);
                       WifiSecurityHelper.getInstance().getNetworkType(BaseActivity.this);
                   }
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])) {
                        FinjanDialogBuilder.showPermissionDeclinedAlert(this);
                    }
                }
                break;
        }
    }

    public String getCurrentFragmentName() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        return fragment.getClass().getSimpleName();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    @Override
    public void onBackPressed() {

        /*if(!UserPref.getInstance().getIsBrowser()){
            finish();
        }*/
        if(UserPref.getInstance().getIsBrowser()){
            ScreenNavigation.getInstance().callHomeActivity(this,"");
        }else {
            ScreenNavigation.getInstance().callVPNScreen(this);
          //  ParentFragment.this.getActivity().finish();
            finish();
        }
        Fragment fragment = getCurrentFragment();
        if (fragment == null) {
            return;
        }
        if (fragment instanceof FragmentHome) {
            makeExit();
            return;
        }

        if (fragment instanceof FragmentTabs) {
            ((FragmentTabs) fragment).onClickBack();
        } else if (fragment instanceof FragmentHistory) {
            ((FragmentHistory) fragment).onClickBack();
        }
        if (fragment instanceof FragmentNormalWebView) {
            ((FragmentNormalWebView) fragment).onClickBack();
        } else {
            ((ParentFragment) fragment).onClickParentBack();
        }
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    private void makeExit() {

        if (GlobalVariables.getInstnace().isKeyboardShowing) {
            Utils.hideKeyboard(parentView);
            GlobalVariables.getInstnace().isKeyboardShowing = false;
        } else {
            exitAlert();
        }
    }

    public void clearAllHistory() {
        DbHelper.getInstnace().clearWebHistory();
        if (fragmentMap != null && fragmentMap.size() > 0) {
            for (Long id : fragmentMap.keySet()) {
                if (id != null) {
                    FragmentHome fragment = (FragmentHome) fragmentMap.get(id);
                    if (fragment != null) {
                        fragment.clearHistory();
                    }
                }
            }
            fragmentMap.clear();
        }
    }

    public void clearCookies() {
        if (GlobalVariables.getInstnace().currentHomeFragment != null) {
            GlobalVariables.getInstnace().currentHomeFragment.clearCookies();
        }
    }

    public void clearCashe() {
        if (GlobalVariables.getInstnace().currentHomeFragment != null) {
            GlobalVariables.getInstnace().currentHomeFragment.clearCaches();
        }
    }

    private void exitAlert() {

        if (NativeHelper.getInstnace().checkActivity(this)) {
            new FinjanDialogBuilder(this).setMessage(getString(R.string.exit_message))
                    .setPositiveButton(getString(R.string.alert_ok))
                    .setNegativeButton(getString(R.string.alert_cancel))
                    .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                        @Override
                        public void onPositivClick() {
                            try {
                                finishAffinity();
                            } catch (Exception e) {
                                Process.killProcess(Process.myPid());
                            }
                        }
                    }).show();
        }
    }

    protected boolean isPortraitOrientation() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    protected boolean isLandscapedOrientation() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public void playWarningMusic() {
        mPlay = MediaPlayer.create(this, R.raw.warning);
        mPlay2 = MediaPlayer.create(this, R.raw.warning);

        mPlay.start();
        mPlay.setNextMediaPlayer(mPlay2);

    }

    public void stopWarningMusic() {
        if (mPlay != null) {
            mPlay.stop();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean isSuccess = false;
        switch (requestCode) {
            case AppConstants.AFR_Password_set:
                if (!UserPref.getInstance().getPasscode()) {
                    if (data != null && data.hasExtra(AppConstants.IKEY_PASSWORD_CONFIRMED) &&
                            data.getBooleanExtra(AppConstants.IKEY_PASSWORD_CONFIRMED, false)) {
                        isSuccess = true;
                    }
                    if(GlobalVariables.getInstnace().fragmentSetting!=null){
                        GlobalVariables.getInstnace().fragmentSetting.setPasscordOnResult(isSuccess);
                    }
                }

                break;
            case AppConstants.AFR_REGISTER_FINGER:
                if (data != null && data.hasExtra(AppConstants.IKEY_FINGER_REGISTERED) &&
                        data.getBooleanExtra(AppConstants.IKEY_FINGER_REGISTERED, false)) {
                    isSuccess = true;
                }
                if(GlobalVariables.getInstnace().fragmentSetting!=null){
                    GlobalVariables.getInstnace().fragmentSetting.setTouchIdOnResult(isSuccess);
                }
                break;
            case AppConstants.PICKFILE_REQUEST_CODE:
                Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
                Uri[] resultsArray = new Uri[1];
                if(result==null){
                    resultsArray=new Uri[]{};
                }else {
                    resultsArray[0] = result;
                }
                EventBus.getDefault().post(new UploadingMsg(resultsArray));
                break;
            case AppConstants.PICKFILE_REQUEST_CODE_Pre:
                Uri resultPre = data == null || resultCode != RESULT_OK ? null : data.getData();
//                if (GlobalVariables.getInstnace().currentHomeFragment != null) {
//                    if (resultPre != null) {
//                        GlobalVariables.getInstnace().currentHomeFragment.
//                                mFilePathCallbackPre.onReceiveValue(resultPre);
//                    }
//                    GlobalVariables.getInstnace().currentHomeFragment.
//                            mFilePathCallbackPre = null;
//                }
                EventBus.getDefault().post(new UploadingMsg(resultPre));
                break;
        }
    }
}
