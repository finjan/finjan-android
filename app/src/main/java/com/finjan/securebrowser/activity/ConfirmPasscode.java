package com.finjan.securebrowser.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Vibrator;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.custom_views.PasskeyIconView;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 3/10/17.
 *
 * Class for showing confirm passcode screen while setting pass code on Browser.
 */
public class ConfirmPasscode extends ParentActivity{
//    private RelativeLayout rl_1,rl_2,rl_3,rl_4,rl_5,rl_6,rl_7,rl_8,rl_9,rl_0,rl_x,rl_goback;
    private ImageView iv_cicle1,iv_cicle2,iv_cicle3,iv_cicle4;
//    private TextView tv_enterpasscode;
//    Preferences pref;
//    private Vibrator vibe;

    private static int SCREEN_LOCK = 0;
    private static int FINGERPRINT = 1;

    public static boolean touch_on = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passcode_orientation);
//        Fabric.with(this, new Crashlytics());

//        pref = new Preferences(getApplicationContext());
        UserPref.getInstance().setConfirmLockCode("");
//        pref.set(Constants.ConfirmLockcode,"").commit();

        TextView tv_enterpasscode = (TextView) findViewById(R.id.textView2);
        tv_enterpasscode.setText("Confirm Code");

        //<-------------Imageview Passcode Circles--------------->

        iv_cicle1 = (ImageView) findViewById(R.id.imageView8);
        iv_cicle2 = (ImageView) findViewById(R.id.imageView9);
        iv_cicle3 = (ImageView) findViewById(R.id.imageView10);
        iv_cicle4 = (ImageView) findViewById(R.id.imageView11);

        //<-------------Relative Layout Number Button Passcode--------------->
        final RelativeLayout rl_1 = (RelativeLayout) findViewById(R.id.rl_1);
        final RelativeLayout rl_2 = (RelativeLayout) findViewById(R.id.rl_2);
        final RelativeLayout rl_3 = (RelativeLayout) findViewById(R.id.rl_3);
        final RelativeLayout rl_4 = (RelativeLayout) findViewById(R.id.rl_4);
        final RelativeLayout rl_5 = (RelativeLayout) findViewById(R.id.rl_5);
        final RelativeLayout rl_6 = (RelativeLayout) findViewById(R.id.rl_6);
        final RelativeLayout rl_7 = (RelativeLayout) findViewById(R.id.rl_7);
        final RelativeLayout rl_8 = (RelativeLayout) findViewById(R.id.rl_8);
        final RelativeLayout rl_9 = (RelativeLayout) findViewById(R.id.rl_9);
        final RelativeLayout rl_0 = (RelativeLayout) findViewById(R.id.rl_0);

        ((PasskeyIconView)(findViewById(R.id.iv_back))).setOnTouch(closeKeyListener,this);
        ((PasskeyIconView)(findViewById(R.id.iv_close))).setOnTouch(backKeylistener,this);

//        final RelativeLayout rl_x = (RelativeLayout) findViewById(R.id.rl_x);
//        final RelativeLayout rl_goback = (RelativeLayout) findViewById(R.id.rl_goback);

       final Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//        rl_goback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//
//        rl_x.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if(event.getAction() == MotionEvent.ACTION_DOWN) {
//                    if(!TextUtils.isEmpty(UserPref.getInstance().getConfirmLockCode()) &&
//                            UserPref.getInstance().getConfirmLockCode().trim().length()>0) {
//                        UserPref.getInstance().setConfirmLockCode(UserPref.getInstance().getConfirmLockCode().substring(0,
//                                UserPref.getInstance().getConfirmLockCode().length() - 1));
//                        setCirclesPasscode();
//                        rl_x.setBackground(ContextCompat.getDrawable(ConfirmPasscode.this,R.drawable.solid_circle));
//                    }else {
//                        setCirclesPasscode();
//                    }
//                } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                    rl_x.setBackground(ContextCompat.getDrawable(ConfirmPasscode.this,R.drawable.circle_border));
//                }
//                return true;
//            }
//        });



        rl_1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_1,"1",vibe,event);
                return true;
            }
        });
        rl_2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_2,"2",vibe,event);
                return true;
            }
        });
        rl_3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_3,"3",vibe,event);
                return true;
            }
        });
        rl_4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_4,"4",vibe,event);
                return true;
            }
        });
        rl_5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_5,"5",vibe,event);
                return true;
            }
        });
        rl_6.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_6,"6",vibe,event);
                return true;
            }
        });
        rl_7.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_7,"7",vibe,event);
                return true;
            }
        });
        rl_8.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_8,"8",vibe,event);
                return true;
            }
        });
        rl_9.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_9,"9",vibe,event);
                return true;
            }
        });
        rl_0.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setPressedKeys(rl_0,"0",vibe,event);
                return true;
            }
        });
    }

    private PasskeyIconView.PasskeyListener backKeylistener=new PasskeyIconView.PasskeyListener() {
        @Override
        public void onTouchKey() {
            onBackPressed();
        }
    };
    private PasskeyIconView.PasskeyListener closeKeyListener=new PasskeyIconView.PasskeyListener() {
        @Override
        public void onTouchKey() {
            if(!TextUtils.isEmpty(UserPref.getInstance().getConfirmLockCode()) &&
                    UserPref.getInstance().getConfirmLockCode().trim().length()>0) {
                UserPref.getInstance().setConfirmLockCode(UserPref.getInstance().getConfirmLockCode().substring(0,
                        UserPref.getInstance().getConfirmLockCode().length() - 1));
            }
            setCirclesPasscode();

        }
    };


    private void setPressedKeys(RelativeLayout rlKey,String charactor,Vibrator vibe,MotionEvent event){
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            UserPref.getInstance().setConfirmLockCode(UserPref.getInstance().getConfirmLockCode()+charactor);
            setCirclesPasscode();
            rlKey.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.solid_circle));

        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            rlKey.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.circle_border));
            checkPasscode(vibe);
        }
    }
    @Override
    public void onBackPressed() {
        setResult(AppConstants.AFR_callingConfirmPassword,null);
        finish();
    }

    public void checkPasscode(Vibrator vibrator){

        String confirmLockCode= UserPref.getInstance().getConfirmLockCode();
        String lockCode=UserPref.getInstance().getSetLockCode();

        if(!TextUtils.isEmpty(confirmLockCode) && confirmLockCode.trim().length()==4){
            if(confirmLockCode.equals(lockCode.trim())){
                UserPref.getInstance().setLockCode(UserPref.getInstance().getSetLockCode());
                UserPref.getInstance().setSetLockCode("");
                UserPref.getInstance().setConfirmLockCode("");
                UserPref.getInstance().setPasscode(true);
                GlobalVariables.getInstnace().appCodeSetNewly=true;
                Toast.makeText(getApplicationContext(),getString(R.string.lockcode_successfully_set) , Toast.LENGTH_SHORT).show();
                Intent intent=new Intent();
                intent.putExtra(AppConstants.IKEY_PASSWORD_CONFIRMED,true);
                setResult(AppConstants.AFR_callingConfirmPassword,intent);
                finish();
//                if(touch_on){
//                    if(NativeHelper.getInstnace().isSupportFingurePrint()){
//                        startActivityForResult(new Intent(ConfirmPasscode.this,FingerPrintGeneration.class),555);
//                    }
//                }
//                else {
//                    Intent intent=new Intent(getApplicationContext(), HomeActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
//                    startActivity(intent);
//
//                    finish();
//
//                }
            }else {
                onWrongCodeEntered(vibrator);
            }
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode==555){
//            Intent intent=new Intent(getApplicationContext(), HomeActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
//            startActivity(intent);
//            finish();
//        }
//    }

    private void onWrongCodeEntered(Vibrator vibe){
        vibe.vibrate(100);
        Toast.makeText(getApplicationContext(),getString(R.string.passcode_not_matched),Toast.LENGTH_LONG).show();
        Drawable emptyDrawable= ResourceHelper.getInstance().getDrawable(R.drawable.circle_border);

        iv_cicle1.setBackground(emptyDrawable);
        iv_cicle2.setBackground(emptyDrawable);
        iv_cicle3.setBackground(emptyDrawable);
        iv_cicle4.setBackground(emptyDrawable);

        UserPref.getInstance().setConfirmLockCode("");
    }

    public void setCirclesPasscode(){

        String confirmLockCode=UserPref.getInstance().getConfirmLockCode();
        if(confirmLockCode==null){
            return;
        }
        confirmLockCode=confirmLockCode.trim();
        int codeLenght=confirmLockCode.length();

        Drawable solidDrawable=ResourceHelper.getInstance().getDrawable(R.drawable.pin_fill);
        Drawable emptyDrawable=ResourceHelper.getInstance().getDrawable(R.drawable.pin_blank);
        switch (codeLenght){
            case 1:
                iv_cicle1.setBackground(solidDrawable);
                iv_cicle2.setBackground(emptyDrawable);
                iv_cicle3.setBackground(emptyDrawable);
                iv_cicle4.setBackground(emptyDrawable);
                break;
            case 2:
                iv_cicle1.setBackground(solidDrawable);
                iv_cicle2.setBackground(solidDrawable);
                iv_cicle3.setBackground(emptyDrawable);
                iv_cicle4.setBackground(emptyDrawable);
                break;
            case 3:
                iv_cicle1.setBackground(solidDrawable);
                iv_cicle2.setBackground(solidDrawable);
                iv_cicle3.setBackground(solidDrawable);
                iv_cicle4.setBackground(emptyDrawable);
                break;
            case 4:
                iv_cicle1.setBackground(solidDrawable);
                iv_cicle2.setBackground(solidDrawable);
                iv_cicle3.setBackground(solidDrawable);
                iv_cicle4.setBackground(solidDrawable);
                break;
            case 0:
                iv_cicle1.setBackground(emptyDrawable);
                iv_cicle2.setBackground(emptyDrawable);
                iv_cicle3.setBackground(emptyDrawable);
                iv_cicle4.setBackground(emptyDrawable);
                break;
        }
    }
}
