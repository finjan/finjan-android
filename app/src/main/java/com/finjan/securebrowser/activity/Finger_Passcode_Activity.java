package com.finjan.securebrowser.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.custom_views.PasskeyIconView;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.ScreenNavigation;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 3/10/17.
 *
 * Class responsible for  pass code validation and finer print validation as well
 *
 */

public class Finger_Passcode_Activity extends ParentActivity implements View.OnClickListener{

    private ImageView iv_cicle1, iv_cicle2, iv_cicle3, iv_cicle4;
    private Vibrator vibe;
    private  Context context = this;
    private boolean isPasswordChange = false;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private void setKeyboardTitle(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && !TextUtils.isEmpty(bundle.getString("password"))
                && bundle.getString("password").equalsIgnoreCase("true")) {
            isPasswordChange = true;
            ((TextView) findViewById(R.id.textView2)).setText(getResources().getString(R.string.enter_previous_passcode));
        } else {
            isPasswordChange = false;
            ((TextView) findViewById(R.id.textView2)).setText(getResources().getString(R.string.enter_passcode));
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passcode_orientation);
        setReferences();
        setDefaults();
    }

    private  boolean checkingPassword;
    @Override
    protected void onResume() {
        super.onResume();
        checkingPassword=(Finger_Passcode_Activity.this.getIntent()!=null
                && Finger_Passcode_Activity.this.getIntent().getBooleanExtra(ScreenNavigation.CHECKING_PASSWORD,false));
    }


    /**
     *
     * showing dialog for finger print scan
     */
    private void setDialog(){
//        TouchIdDialog dialog=new TouchIdDialog(this);
//        dialog.show();
       final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.touch_id);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        if(dialog.getWindow()!=null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout((7*width)/8, RelativeLayout.LayoutParams.WRAP_CONTENT);
        }

        ((TextView) dialog.findViewById(R.id.tv_canceltouchid)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.logE("TAG","cancel touch id ");
                dialog.dismiss();
                Finger_Passcode_Activity.this.finish();
            }
        });
        (dialog.findViewById(R.id.tv_enterpass)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.logE("TAG","enter pass");
                dialog.dismiss();

            }
        });
        if (UserPref.getInstance().getTouchId() &&!isPasswordChange) {
            dialog.show();
        }
    }

    private void onClickButton(String value){
        UserPref.getInstance().setCheckLockCode(value);
        setCirclesPasscode();
        checkPasscode();
    }


    private void setReferences() {
        iv_cicle1 = (ImageView) findViewById(R.id.imageView8);
        iv_cicle2 = (ImageView) findViewById(R.id.imageView9);
        iv_cicle3 = (ImageView) findViewById(R.id.imageView10);
        iv_cicle4 = (ImageView) findViewById(R.id.imageView11);

        (findViewById(R.id.rl_1)).setOnClickListener(this);
        (findViewById(R.id.rl_2)).setOnClickListener(this);
        (findViewById(R.id.rl_3)).setOnClickListener(this);
        (findViewById(R.id.rl_4)).setOnClickListener(this);
        (findViewById(R.id.rl_5)).setOnClickListener(this);
        (findViewById(R.id.rl_6)).setOnClickListener(this);
        (findViewById(R.id.rl_7)).setOnClickListener(this);
        (findViewById(R.id.rl_8)).setOnClickListener(this);
        (findViewById(R.id.rl_9)).setOnClickListener(this);
        (findViewById(R.id.rl_0)).setOnClickListener(this);
//        (findViewById(R.id.rl_x)).setOnClickListener(this);
//        (findViewById(R.id.rl_goback)).setOnClickListener(this);
        ((PasskeyIconView)(findViewById(R.id.iv_back))).setOnTouch(closeKeyListener,this);
        ((PasskeyIconView)(findViewById(R.id.iv_close))).setOnTouch(backKeylistener,this);
    }
    private PasskeyIconView.PasskeyListener backKeylistener=new PasskeyIconView.PasskeyListener() {
        @Override
        public void onTouchKey() {
            if(GlobalVariables.getInstnace().isCheckingScreenLock && HomeActivity.getInstance()!=null){
                HomeActivity.getInstance().finish();
            }
            onBackPressed();
        }
    };
    private PasskeyIconView.PasskeyListener closeKeyListener=new PasskeyIconView.PasskeyListener() {
        @Override
        public void onTouchKey() {
            if (!TextUtils.isEmpty(UserPref.getInstance().getCheckLockCode()) &&
                    UserPref.getInstance().getCheckLockCode().trim().length() > 0) {

                UserPref.getInstance().setCheckLockCode(UserPref.getInstance().getCheckLockCode().substring(0,
                        UserPref.getInstance().getCheckLockCode().length() - 1));
            }
            setCirclesPasscode();
        }
    };

    public void checkPasscode() {

        String checkLockCode=UserPref.getInstance().getCheckLockCode();
        String lockCode= UserPref.getInstance().getLockCode();

        if(!TextUtils.isEmpty(checkLockCode) && !TextUtils.isEmpty(lockCode)
                && checkLockCode.trim().length()==4){
            if(checkLockCode.equals(lockCode.trim())){
                Toast.makeText(getApplicationContext(), getString(R.string.correct_passcode), Toast.LENGTH_SHORT).show();
                if (isPasswordChange) {
                    startActivity(new Intent(getApplicationContext(), PasscodeSet.class));
                } else {
                    if(checkingPassword){
                        ScreenNavigation.getInstance().initLastScreen(Finger_Passcode_Activity.this,"");
                    }else {
                        ScreenNavigation.getInstance().callHomeActivity(Finger_Passcode_Activity.this,"");
//                NativeHelper.getInstnace().callHomeActivity(PasscodeActivity.this);
                    }
                }
                GlobalVariables.getInstnace().isCheckingScreenLock=false;
                finish();
            } else{
                vibe.vibrate(100);
                Toast.makeText(getApplicationContext(), getString(R.string.wrong_passcode), Toast.LENGTH_SHORT).show();
                iv_cicle1.setBackground(emptyCircle);
                iv_cicle2.setBackground(emptyCircle);
                iv_cicle3.setBackground(emptyCircle);
                iv_cicle4.setBackground(emptyCircle);
                UserPref.getInstance().setCheckLockCode("");
            }
        }
    }

    private Drawable solidCircle,emptyCircle;
    private void setDefaults(){
        if(solidCircle==null){
            solidCircle= ResourceHelper.getInstance().getDrawable(R.drawable.pin_fill);
        }
        if(emptyCircle==null){
            emptyCircle=ResourceHelper.getInstance().getDrawable(R.drawable.pin_blank);
        }
        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

//        Fabric.with(this, new Crashlytics());
        UserPref.getInstance().setCheckLockCode("");
        setKeyboardTitle();
        initializeGoogleClient();
        setDialog();
    }
    private void initializeGoogleClient(){
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void setCirclesPasscode() {

        String checkLockCode=UserPref.getInstance().getCheckLockCode();
        if(checkLockCode!=null){
            int codeLength=checkLockCode.trim().length();


            switch (codeLength){
                case 1:
                    iv_cicle1.setBackground(solidCircle);
                    iv_cicle2.setBackground(emptyCircle);
                    iv_cicle3.setBackground(emptyCircle);
                    iv_cicle4.setBackground(emptyCircle);
                    break;
                case 2:
                    iv_cicle1.setBackground(solidCircle);
                    iv_cicle2.setBackground(solidCircle);
                    iv_cicle3.setBackground(emptyCircle);
                    iv_cicle4.setBackground(emptyCircle);
                    break;
                case 3:
                    iv_cicle1.setBackground(solidCircle);
                    iv_cicle2.setBackground(solidCircle);
                    iv_cicle3.setBackground(solidCircle);
                    iv_cicle4.setBackground(emptyCircle);

                    break;
                case 4:

                    iv_cicle1.setBackground(solidCircle);
                    iv_cicle2.setBackground(solidCircle);
                    iv_cicle3.setBackground(solidCircle);
                    iv_cicle4.setBackground(solidCircle);
                    break;
                case 0:
                    iv_cicle1.setBackground(emptyCircle);
                    iv_cicle2.setBackground(emptyCircle);
                    iv_cicle3.setBackground(emptyCircle);
                    iv_cicle4.setBackground(emptyCircle);
                    break;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();

        try {
            Action viewAction = Action.newAction(
                    Action.TYPE_VIEW, // TODO: choose an action type.
                    "Finger_Passcode_ Page", // TODO: Define a title for the content shown.
                    // TODO: If you have web page content that matches this app activity's content,
                    // make sure this auto-generated web page URL is correct.
                    // Otherwise, set the URL to null.
                    Uri.parse("http://host/path"),
                    // TODO: Make sure this auto-generated app URL is correct.
                    Uri.parse("android-app://com.finjan.securebrowser.activity/http/host/path")
            );
            AppIndex.AppIndexApi.end(client, viewAction);
        }catch (Exception e){}
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        try {
            Action viewAction = Action.newAction(
                    Action.TYPE_VIEW, // TODO: choose an action type.
                    "Finger_Passcode_ Page", // TODO: Define a title for the content shown.
                    // TODO: If you have web page content that matches this app activity's content,
                    // make sure this auto-generated web page URL is correct.
                    // Otherwise, set the URL to null.
                    Uri.parse("http://host/path"),
                    // TODO: Make sure this auto-generated app URL is correct.
                    Uri.parse("android-app://com.finjan.securebrowser.activity/http/host/path")
            );
            AppIndex.AppIndexApi.end(client, viewAction);
        }catch (Exception e){}
        client.disconnect();
    }

    private void handleClick(View view){
        switch (view.getId()){
            case R.id.rl_1:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"1");
                break;
            case R.id.rl_2:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"2");
                break;
            case R.id.rl_3:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"3");
                break;
            case R.id.rl_4:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"4");
                break;
            case R.id.rl_5:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"5");
                break;
            case R.id.rl_6:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"6");
                break;
            case R.id.rl_7:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"7");
                break;
            case R.id.rl_8:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"8");
                break;
            case R.id.rl_9:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"9");
                break;
            case R.id.rl_0:
                onClickButton(UserPref.getInstance().getCheckLockCode()+"0");
                break;
            case R.id.rl_x:



//                if (UserPref.getInstance().getSetLockCode().trim().length() > 0) {
//                    UserPref.getInstance().setSetLockCode(UserPref.getInstance().getSetLockCode().substring(0, UserPref.getInstance().getSetLockCode().length() - 1));
//                    setCirclesPasscode();
//                    rl_x.setBackground(ResourceHelper.getInstance().getDrawable(R.drawable.solid_circle));
//                }
                break;
            case R.id.rl_goback:

                break;
        }
    }

    @Override
    public void onClick(View v) {
        handleClick(v);
    }
}
