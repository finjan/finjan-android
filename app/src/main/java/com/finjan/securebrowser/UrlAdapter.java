package com.finjan.securebrowser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class UrlAdapter extends ArrayAdapter<UrlPojo> {

    Context context;
//    int resource, textViewResourceId;
    private List<UrlPojo> items, tempItems, suggestions;

    public UrlAdapter(Context context, int resource, int textViewResourceId, List<UrlPojo> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
//        this.resource = resource;
//        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<UrlPojo>(items); // this makes the difference.
        suggestions = new ArrayList<UrlPojo>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_actv, parent, false);
        }
        UrlPojo people = items.get(position);
        if (people != null) {
            TextView lblName = (TextView) view.findViewById(R.id.lbl_name);
            if (lblName != null)
                lblName.setText(people.getUrl());
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((UrlPojo) resultValue).getUrl();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (UrlPojo people : tempItems) {
                    if (people.getUrl().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
         try {
             List<UrlPojo> filterList = (ArrayList<UrlPojo>) results.values;
             if (results != null && results.count > 0) {
                 clear();
                 for (UrlPojo people : filterList) {
                     add(people);
                     notifyDataSetChanged();
                 }
             }
         }catch (Exception e){}
        }
    };
}