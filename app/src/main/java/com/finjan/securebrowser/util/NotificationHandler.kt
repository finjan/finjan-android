package com.finjan.securebrowser.util

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import com.finjan.securebrowser.Appirater
import com.finjan.securebrowser.activity.HomeActivity
import com.finjan.securebrowser.constants.DrawerConstants
import com.finjan.securebrowser.helpers.NativeHelper
import com.finjan.securebrowser.helpers.PlistHelper
import com.finjan.securebrowser.helpers.ScreenNavigation
import com.finjan.securebrowser.util.dialogs.NativeDialogs
import com.finjan.securebrowser.vpn.FinjanVPNApplication
import com.finjan.securebrowser.vpn.ui.main.VpnActivity
import com.finjan.securebrowser.vpn.ui.main.VpnHomeFragment
import com.finjan.securebrowser.vpn.ui.promo.PromoHelper


/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 21/07/17.
 *
 * Used for deep linking of iap and pushes of localytics
 *
 */

class NotificationHandler{


    private lateinit var activity:Context
    private lateinit var intent:Intent
    public fun init(activity:Activity,intent: Intent){
        this.activity= activity
        this.intent = intent
    }
    public fun init(activity:Context){
        this.activity= activity

    }

    companion object {
        private final object NotificationHandlerHolder {
            val Instance = NotificationHandler()
        }
        public fun getInstance(): NotificationHandler {
            return NotificationHandlerHolder.Instance
        }
    }


    fun isDeepLink(intent: Intent):Boolean{
        return (intent.data!=null ||  intent.hasExtra("ll_deep_link_url") ||
                (intent!=null && intent.dataString!=null &&
                        intent.dataString.toString().contains("invincibull://identifier")) ||
                (intent.getData()!=null && intent.getData().toString().contains("invincibull://url=")))
    }


    fun processIntent():Boolean{

        if(isDeepLink(intent)){
            if(intent.data!=null && intent.data.toString().startsWith("http") &&
                    !intent.data.toString().startsWith("https://invincibull.page.link/")){return false}
            var url = ""
            if(intent.hasExtra("ll_deep_link_url")){
                url = intent.extras.getString("ll_deep_link_url")
            }else{
                url =intent.dataString.toString()
            }
            val parameter = url.split("-")

            val path = parameter[0].replace("invincibull://identifier=","")
            when(path){
                "inapp"->{
                    openIAP(url)
                }
                "vpn"->{openVPNScreen(url)}
                "trustednetworks"->{openVPNScreen(url)}
                "settings"->{ScreenNavigation.getInstance().callHomeActivity(activity, DrawerConstants.DKEY_ABOUT)}
                "rate"->{callRate(path)}
                "share"->{openVPNScreen(url)}
                "vs-desktop"->{openVPNScreen(url)}
                "about"->{ScreenNavigation.getInstance().callHomeActivity(activity, DrawerConstants.DKEY_ABOUT)}
                "browser"->{ScreenNavigation.getInstance().callHomeActivity(activity, "")}
                "enablepushnotifications"->{openNotificationSetting()}
//                "promo"->{openNotificationSetting()}
                "promo"->{
                    if(parameter.size>1){
//                        ScreenNavigation.getInstance().callVPNScreenPromoDeepLink(activity,"1")}
                        ScreenNavigation.getInstance().callVPNScreenPromoDeepLink(activity,parameter[1])}

//                        openRedeemOfferScreen(parameter[1])}
                }
            }
            return true
        }
        return false
    }

    fun processIntent(url: String ): Intent? {


        val parameter = url.split("-")

        val path = parameter[0].replace("invincibull://identifier=","")
        when(path){
            "inapp"->{
                return getopenIAP(url)
            }
            "vpn"->{return getopenVPNScreen(url)}
            "trustednetworks"->{return getopenVPNScreen(url)}
            "settings"->{return ScreenNavigation.getInstance().getcallHomeActivity(activity, DrawerConstants.DKEY_ABOUT)}
            /*"rate"->{callRate(path)}*/
            "share"->{return getopenVPNScreen(url)}
            "vs-desktop"->{return getopenVPNScreen(url)}
            "about"->{return ScreenNavigation.getInstance().getcallHomeActivity(activity, DrawerConstants.DKEY_ABOUT)}
            "browser"->{return ScreenNavigation.getInstance().getcallHomeActivity(activity, "")}
            "enablepushnotifications"->{return getopenNotificationSetting()}
//                "promo"->{openNotificationSetting()}
            "promo"->{
                if(parameter.size>1){
//                        ScreenNavigation.getInstance().callVPNScreenPromoDeepLink(activity,"1")}
                    return    ScreenNavigation.getInstance().getcallVPNScreenPromoDeepLink(activity,parameter[1])}

//                        openRedeemOfferScreen(parameter[1])}
            }

        }
        return null
    }

    fun openNotificationSetting(){
        val intent = Intent()
        intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP ||
                Build.VERSION.SDK_INT < Build.VERSION_CODES.N){
            intent.putExtra("app_package", FinjanVPNApplication.getInstance().getPackageName())
            intent.putExtra("app_uid", FinjanVPNApplication.getInstance().getApplicationInfo().uid)
        }
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N){
            intent.putExtra("android.provider.extra.APP_PACKAGE", FinjanVPNApplication.getInstance().getPackageName())
        }
        FinjanVPNApplication.getInstance().startActivity(intent)
    }

    fun getopenNotificationSetting():Intent{
        val intent = Intent()
        intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP ||
                Build.VERSION.SDK_INT < Build.VERSION_CODES.N){
            intent.putExtra("app_package", FinjanVPNApplication.getInstance().getPackageName())
            intent.putExtra("app_uid", FinjanVPNApplication.getInstance().getApplicationInfo().uid)
        }
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N){
            intent.putExtra("android.provider.extra.APP_PACKAGE", FinjanVPNApplication.getInstance().getPackageName())
        }
        return intent
        // FinjanVPNApplication.getInstance().startActivity(intent)
    }

    fun getopenVPNScreen(url: String): Intent? {
        val path = url.replace("invincibull://identifier=","")
        if(ScreenNavigation.getActiveScreenList().size>0){
            if(path.equals("inapp")){
                /*activity.startActivity(VpnHomeFragment.upgradeIntent(activity,false, true,
                        true))*/
                return VpnHomeFragment.upgradeIntent(activity,false, true,
                        true)
            }else
                if(path.contains("inapp")){
                    try {
                        var par= path.replace( "inapp-",  "")

                        val percent = par.toInt()
                        var intent = VpnHomeFragment.getUpgradeIntent(activity)
                        var sku = PromoHelper.Companion.getDiscountedArray(percent)
                        /* activity.startActivity(VpnHomeFragment.getDiscountedIntent(intent,sku, "",
                                 "","0"))*/


//                        activity.startActivity(VpnHomeFragment.getDiscountedIntent(intent,sku, PromoHelper.Companion.currentActivePromo!!.attributes.title,
//                                PromoHelper.Companion.currentActivePromo!!.attributes.title_2,"0"))
                        if (PromoHelper.Companion.isPromoEnded == true)
                            PromoHelper.Companion.currentActivePromo = null

                        return VpnHomeFragment.getDiscountedIntent(intent,sku, "",
                                "","0")

                    }
                    catch (e:Exception)
                    {
                        /* activity.startActivity(VpnHomeFragment.upgradeIntent(activity,false, true,
                                 true))*/
                        return VpnHomeFragment.upgradeIntent(activity,false, true,
                                true)
                    }



//                    if (PromoHelper.Companion.isPromoEnded == true)
//                         PromoHelper.isPromoActive = false

                }
            when(path){
                "vpn"->{ return ScreenNavigation.getInstance().getcallVPNScreen(activity)}
                /*"trustednetworks"->{
                    VpnHomeFragment.onClickAutoConnect(activity)
                }
                "settings"->{
                    (HomeActivity.getInstance() as HomeActivity).openSettingScreen()
                }
                "rate"->{showRateWidget()}
                "share"->{showShareDialog()}
                "vs-desktop"->{
                    showVsDialog()
                }*/
                "about"->{return ScreenNavigation.getInstance().getcallHomeActivity(activity, DrawerConstants.DKEY_ABOUT) }
                "browser"->{return ScreenNavigation.getInstance().getcallHomeActivity(activity,"")}
            }
        }else{
            return ScreenNavigation.getInstance().getcallVPNScreen(activity,url)
        }
        return null
    }

    fun openVPNScreen(url: String){
        val path = url.replace("invincibull://identifier=","")
        if(ScreenNavigation.getActiveScreenList().size>0){
            if(path.equals("inapp")){
                activity.startActivity(VpnHomeFragment.upgradeIntent(activity,false, true,
                        true))
                return
            }else
                if(path.contains("inapp")){
                    try {
                        var par= path.replace( "inapp-",  "")

                        val percent = par.toInt()
                        var intent = VpnHomeFragment.getUpgradeIntent(activity)
                        var sku = PromoHelper.Companion.getDiscountedArray(percent)
                        activity.startActivity(VpnHomeFragment.getDiscountedIntent(intent,sku, "",
                                "","0"))


//                        activity.startActivity(VpnHomeFragment.getDiscountedIntent(intent,sku, PromoHelper.Companion.currentActivePromo!!.attributes.title,
//                                PromoHelper.Companion.currentActivePromo!!.attributes.title_2,"0"))
                        if (PromoHelper.Companion.isPromoEnded == true)
                            PromoHelper.Companion.currentActivePromo = null

                    }
                    catch (e:Exception)
                    {
                        activity.startActivity(VpnHomeFragment.upgradeIntent(activity,false, true,
                                true))
                    }



//                    if (PromoHelper.Companion.isPromoEnded == true)
//                         PromoHelper.isPromoActive = false
                    return
                }
            when(path){
                "vpn"->{ ScreenNavigation.getInstance().callVPNScreen(activity)}
                "trustednetworks"->{
                    VpnHomeFragment.onClickAutoConnect(activity)
                }
                "settings"->{
                    (HomeActivity.getInstance() as HomeActivity).openSettingScreen()
                }
                "rate"->{showRateWidget()}
                "share"->{showShareDialog()}
                "vs-desktop"->{
                    showVsDialog()
                }
                "about"->{ ScreenNavigation.getInstance().callHomeActivity(activity, DrawerConstants.DKEY_ABOUT) }
                "browser"->{ScreenNavigation.getInstance().callHomeActivity(activity,"")}
            }
        }else{
            ScreenNavigation.getInstance().callVPNScreen(activity,url)
        }
    }
    fun showRateWidget(){
        android.os.Handler().postDelayed({
            val prefs = activity?.getSharedPreferences(activity?.getPackageName() + ".appirater", 0)
            val editor = prefs?.edit()
            Appirater.showRateDialog(NativeHelper.getInstnace().currentAcitvity, editor)
        }, 800)

    }
    fun showShareDialog(){
        android.os.Handler().postDelayed({
            VpnHomeFragment.onClickVpnAppShare()
        },800)
    }
    fun showVsDialog(){
        android.os.Handler().postDelayed({
            NativeDialogs.getShareDesktopAppDialog(NativeHelper.getInstnace().currentAcitvity,PlistHelper.getInstance().vsScreenPopUpText)
        },800)
    }
    fun showVsDesktopDialog(){
        android.os.Handler().postDelayed({
            NativeDialogs.getShareDesktopAppDialog(NativeHelper.getInstnace().currentAcitvity,PlistHelper.getInstance().vsScreenPopUpText)
        },800)
    }
    fun openIAP(url:String){
        openVPNScreen(url)
    }
    fun getopenIAP(url:String): Intent? {
        return getopenVPNScreen(url)
    }
    fun openVPN(){ScreenNavigation.getInstance().callVPNScreen(activity)}
    fun callRate(path: String){
        if(NativeHelper.getInstnace().checkActivity(HomeActivity.getInstance()) ||
                NativeHelper.getInstnace().checkActivity(VpnActivity.getInstance())){
            showRateWidget()
        }else{
            ScreenNavigation.getInstance().initLastScreen(activity,"")
            android.os.Handler().postDelayed({showRateWidget()},2000)
        }
    }



}