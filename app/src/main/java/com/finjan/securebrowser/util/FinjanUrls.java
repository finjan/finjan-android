package com.finjan.securebrowser.util;

/**
 * Created by anurag on 17/08/17.
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public interface FinjanUrls {

    String URL_LAUNCH = "http://mobilesecurebrowser.finjanmobile.com/api/v1/v5.02.01/config_options_android.plist"; //production
    String URL_PASSWORD_RESET="https://ulm.finjanmobile.com/mobile-password-reset/";
    String URL_EULA="https://www.finjanmobile.com/vital-security-eula/";
    String URL_Privacy_statement="https://ulm.finjanmobile.com/privacy-statement/";







    String URL_searchURL = "https://api.cognitive.microsoft.com/";

    String URL_safetyURL = "http://mobilesecurebrowser.finjanmobile.com/";
    String URL_safetyCheckURL = "http://tacplugin.wpengine.com/wp-json/photoon-app/v1/credits";
//    String URL_LAUNCH = "http://mobilesecurebrowser.finjanmobile.com/api/v1/v3.7/config_options_android.plist"; //production

}
