package com.finjan.securebrowser.util.dialogs;

import android.content.Context;
import android.content.Intent;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.NativeHelper;
import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.tracking.localytics.LocalyticsTrackers;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 19/09/17.
 *
 */

public class NativeDialogs {

    private static final class NativeDialogsHolder{
        private static final NativeDialogs instance=new NativeDialogs();
    }
    private NativeDialogs getInstance(){
        return NativeDialogsHolder.instance;
    }

    public static void getShareDesktopAppDialog(final Context context,String msg){
        if(NativeHelper.getInstnace().checkContext(context)){
            FinjanDialogBuilder builder=new FinjanDialogBuilder(context);
            builder.setMessage(msg)
//            builder.setMessage(getTempText())
                    .setPositiveButton(FinjanVPNApplication.getInstance().getString(R.string.share))
                    .setNegativeButton(FinjanVPNApplication.getInstance().getString(R.string.alert_cancel))
                    .setDialogClickListener(new FinjanDialogBuilder.DialogClickListener() {
                        @Override
                        public void onNegativeClick() {

                        }

                        @Override
                        public void onPositivClick() {
                            shareAppShare(context);
                        }

                        @Override
                        public void onContentClick(String content) {

                        }
                    });
            builder.show();
            LocalyticsTrackers.Companion.setEDesktop_Share();
            GoogleTrackers.Companion.setEDesktop_Share();
        }
    }

    public static void getVPNShareLinkDialog(final Context context, String msg){
            FinjanDialogBuilder builder=new FinjanDialogBuilder(context);
            builder.setMessage(msg)
//            builder.setMessage(getTempText())
                    .setPositiveButton(ResourceHelper.getInstance().getString(R.string.share))
                    .setNegativeButton(ResourceHelper.getInstance().getString(R.string.alert_cancel))
                    .setDialogClickListener(new FinjanDialogBuilder.DialogClickListener() {
                        @Override
                        public void onNegativeClick() {

                        }

                        @Override
                        public void onPositivClick() {
                            shareVPNAppShare(context);
                        }

                        @Override
                        public void onContentClick(String content) {

                        }
                    });
            builder.show();
    }
    private static void shareVPNAppShare(Context context) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.download_the_free_vitalSecurityVPN));
        share.putExtra(Intent.EXTRA_TEXT, PlistHelper.getInstance().getvpnsharetext());
        context.startActivity(Intent.createChooser(share, context.getString(R.string.share_with)));
    }
    private static void shareAppShare(Context context) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.download_the_free_vitalSecurityVPN_desktop));
        share.putExtra(Intent.EXTRA_TEXT, PlistHelper.getInstance().getVpndownloadtext());
        context.startActivity(Intent.createChooser(share, context.getString(R.string.share_with)));
    }
}
