/*
 * *
 *  * <p>
 *  * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 *  * Finjan Mobile Vital Security Browser
 *  * <p>
 *  * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *  * Developed by NewOfferings, LLC.
 *  *
 *  *
 *
 */

package com.finjan.securebrowser.util;

import com.google.gson.Gson;

public class JsonUtils {
    public static <T> Object convertJsonToModel(String json, Class<T> type) {
        Gson gson = new Gson();
        try {
            return gson.fromJson(json, type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertModelToJson(Object model) {
        Gson gson = new Gson();
        return gson.toJson(model);
    }
}

