package com.finjan.securebrowser.util;


import android.content.Context;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.custom_exceptions.EventsListHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.util.android_publisher_api.AndroidPublisherHelper;
import com.finjan.securebrowser.util.android_publisher_api.ApplicationConfig;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.AndroidPublisherScopes;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 21/07/17.
 *
 */


public class AndroidPublisherValidation {

    private Context context;
    private AndroidPublisher publisher;

    public static AndroidPublisherValidation getInstance() {
        return InAppPurchaseValidationHolder.Instance;
    }

    public AndroidPublisherValidation init(Context context) {
        this.context = context;
        return this;
    }

    private AndroidPublisher getPublisher() {
//        HttpTransport httpTransport = new com.google.api.client.http.javanet.NetHttpTransport();
        HttpTransport httpTransport = new NetHttpTransport();

        JsonFactory jsonFactory = new JacksonFactory();
        List<String> scopes = new ArrayList<String>();
        scopes.add(AndroidPublisherScopes.ANDROIDPUBLISHER);
        Credential credential = null;
        try {

            credential = AndroidPublisherHelper.authorizeWithServiceAccount(ResourceHelper.getInstance().getString(R.string.developer_server_account));
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new AndroidPublisher.Builder(httpTransport, jsonFactory, credential).build();
    }
    private static final String TAG=AndroidPublisherValidation.class.getSimpleName();

    public SubscriptionPurchase getPurchase(Context context,String productId, String token) {
        SubscriptionPurchase purchase=null;
//        token = "ikklhfllpimfapgabjmagmjb.AO-J1OzUBSSqNgHViprmLXduLzHhvrWtHIdeB7OVQ8EdYOj_UtQTOHxze0fWVtEYNPwFApIJnMH7OcBmOZIgataWs7EX1q_l3P_xzgMRggMY1KC189k08s1AAOkBVcFGWpDnb5JOBX50ccbbjbkwJs5K3xXWznQ7ag";
//        token = "hnmfbpgiajikpeagjgmpikoh.AO-J1OxC9XX5pAOC7LHaAn6AJa1wHsl6phB4u6B0CuWtByqTRLnhQdru20C64fDlFglWZp7Ry8TiJELFGyXXVq6CaKiVuBBmXePW9lWa2bIGOUnTysrU5KhBoJZsyaScYu2AM9OhbwX1fgR7M2kaOnAnlZoSZqz44Q";
//        productId="vpn_monthly_single_device";
//        productId="monthly_subscription_0.99";

            publisher=getPublisher();
        try {
            Logger.logE(TAG,"Init publisher Helper");
            publisher = AndroidPublisherHelper.init(
                    ApplicationConfig.APPLICATION_NAME,ApplicationConfig.SERVICE_ACCOUNT_EMAIL);
                    Logger.logE(TAG,"Finish publisher Helper");
                    final AndroidPublisher.Purchases.Subscriptions.Get request = publisher.purchases().subscriptions().
                    get(FinjanVPNApplication.getInstance().getPackageName(),
                            productId==null?"":productId, token==null?"":token);
                    Logger.logE(TAG,request.toString());
                    try {
                        purchase = request.execute();
                        Logger.logE("Sbs", " " + purchase.toString());
                    } catch (GoogleJsonResponseException e1){
                        EventsListHelper.Companion.getInstance().add(TAG,"GoogleJsonResponseException",""+e1.toString());
                        Logger.logE(TAG,"SubscriptionPurchase is null parameter error GoogleJsonResponseException "+e1.toString());
                    } catch (IOException e) {
                        Logger.logE(TAG,"IO Exception from request execute"+e.toString());
                        EventsListHelper.Companion.getInstance().add(TAG,"IOException",""+e.toString());
                        e.printStackTrace();
                    }

        } catch (IOException e) {
            Logger.logE(TAG,"IO Exception"+e.toString());
            EventsListHelper.Companion.getInstance().add(TAG,"IOException",""+e.toString());
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            Logger.logE(TAG,"Security  Exception"+e.toString());
            EventsListHelper.Companion.getInstance().add(TAG,"GeneralSecurityException",""+e.toString());
            e.printStackTrace();
        }
        return purchase;
    }

    public boolean isSubsExpires(SubscriptionPurchase purchase){
        Calendar purchaseExpCal=Calendar.getInstance();
        purchaseExpCal.setTimeInMillis(purchase.getExpiryTimeMillis());
        return Calendar.getInstance().after(purchaseExpCal);
    }
    public boolean isSubsCancelled(SubscriptionPurchase purchase){
        return (!purchase.getAutoRenewing() && purchase.getCancelReason()==0);
    }

    private static final class InAppPurchaseValidationHolder {
        private static final AndroidPublisherValidation Instance = new AndroidPublisherValidation();
    }
}
