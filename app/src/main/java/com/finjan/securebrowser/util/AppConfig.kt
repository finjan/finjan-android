package com.finjan.securebrowser.util

import android.content.Context
import android.util.Log
import com.avira.common.backend.Backend
import com.finjan.securebrowser.helpers.logger.Logger
import com.finjan.securebrowser.helpers.sharedpref.UserPref
import com.finjan.securebrowser.vpn.FinjanVPNApplication
import com.finjan.securebrowser.vpn.util.TrafficController
import org.json.JSONObject
import java.io.*
import java.nio.charset.Charset

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 21/07/17.
 *
 * class that handle various features in application with toggle feature using boolean for testing perposes
 *
 */


class AppConfig{
    companion object {
        val showAutoConnect = true
        val dataMultiplier = 1
        val HandleSocialBackPress = true
        val LoadPlistOnce = true
        val onlyRegisteredUserCanPlay = true
        val supportOfLocalJsons= true
        val useFinjanLicenseApii = true


        //new feature
        val feature_reddem_offer =true
        val promo_offer =true
        val reddem_offer_show_msg_befor_act_promo =false

        //testing purpose
        val testHalfInapp = true
        val DebugReporting = true
        val MakeConfigListExpiry = false
        val MakeVpnConnectEventOnTrafficReached = false
        val letsMakeRegisteredChangeReg =false
        val customDevice = false


        //other app features.
        val requestPhoneStatePermission = false


        val upAndDownBarShowing = true
        val isStagingUrl = false
        var isHardCodedPromoResponse  = false
        var response1 = JSONObject("{\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"id\": \"7\",\n" +
                "            \"type\": \"promos\",\n" +
                "            \"attributes\": {\n" +
                "                \"title\": \"Change Location Promo\",\n" +
                "                \"title_2\": \"Promo\",\n" +
                "                \"popup\": \"1\",\n" +
                "                \"popup_begin_title\": \"Change Location - Enjoy Unlimited Plan for 3 days!\",\n" +
                "                \"popup_begin_text\": \"Enjoy unlimited plan with unlimited data and choose any location for the next 3 days on us!\",\n" +
                "                \"popup_end_title\": \"Change Location - Unlimited Plan Promo Over\",\n" +
                "                \"popup_end_text\": \"We hope you enjoyed the unlimited plan.  \",\n" +
                "                \"popup_button_text\": \"Purchase Now!\",\n" +
                "                \"promo_type\": \"profile_event\",\n" +
                "                \"start_date\": \"0000-00-00 00:00:00\",\n" +
                "                \"end_date\": \"0000-00-00 00:00:00\",\n" +
                "                \"days\": \"3\",\n" +
                "                \"redeem_code\": \"0\",\n" +
                "                \"localytics_event_no\": \"1\",\n" +
                "                \"max_redemptions\": \"0\",\n" +
                "                \"iap_button_text\": \"\",\n" +
                "                \"profile_event_type\": \"location_change\",\n" +
                "                \"iap_discount\": \"\"\n" +
                "            }\n" +
                "        }\n" +
                "    ]\n" +
                "}")

    }

    class CustomUser{
        companion object {

            val deviceId = "444ab72f-b602-3f2f-ab47-688611eea1ee"
            val token = "nsilkhlt3zjdnhlbq34463d1spptjwpxqzck7yi3"
//            val token = "x5snwaustbigylc2zzsh50ltly1titu3uzg4nhsg"
        }
    }

    class Colours{
        companion object {
            val col_desc = "#F77C03"
            val col_content = "#BC202E"
        }
    }
    
    class LocalDataHelper{

        companion object {
            val LICENSE = 0
            val TRAFFIC= 1
            val REGIONS = 2
            fun getOneGBUser():JSONObject{
//                return getJsonObject("jsons/license.json")
                return JSONObject(getLocalJson(LICENSE))
            }
            fun getOnGBTrafficUsage():JSONObject{
//                return getJsonObject("jsons/traffic.json")
                return JSONObject(getLocalJson(TRAFFIC))
            }
            fun getLocalRegions():JSONObject{

//                return getJsonObject("jsons/regions.json")
                return JSONObject(getLocalJson(REGIONS))
            }
            fun getAssetsJson(type: Int):String{
                var json = ""
                try {
                    val ism = FinjanVPNApplication.getInstance().getAssets().open("jsons/"+getFileName(type)+".json")
                    val size = ism.available();
                    val buffer = ByteArray(size)
                    ism.read(buffer)
                    ism.close()
                    json = String(buffer, Charset.forName("UTF-8"))
                } catch (ex:IOException ) {
                    ex.printStackTrace()
                    return ("")                    }
                return (json)
            }
//            fun getJsonObject(type:Int):JSONObject{
//                return JSONObject(getAssetsJson(type))
//            }

            fun getLocalJson(type: Int):String{
                var ret = ""

                try {
                        val inputStream = FinjanVPNApplication.getInstance().openFileInput(getFileName(type)+".txt")

                    if (inputStream != null) {
                        val stringBuilder = StringBuilder()
                        val lineList = mutableListOf<String>()
                        inputStream.bufferedReader().useLines { lines -> lines.forEach { lineList.add(it)} }
                        lineList.forEach{stringBuilder.append(it)}
                        inputStream.close()
                        ret = stringBuilder.toString()
                        return  ret
                    }
                } catch (e: FileNotFoundException) {
                    val str =getAssetsJson(type)
                    updateJson(str,type)
                    Logger.logE("login activity", "File not found: " + e.toString())
                    return str
                } catch (e: IOException) {
                    Logger.logE("login activity", "Can not read file: " + e.toString())
                }

                return ret
            }

            fun updateConsumedData(totalTraffic: Long){
                try {
                    val json = JSONObject(getLocalJson(TRAFFIC))
                    json.put("used",totalTraffic)
                    updateTrfficJson(json)
                }catch (ex:Exception){}
            }
            fun getFileName(type: Int):String{
                var fileName = ""
                when(type){
                    LICENSE -> {
                        fileName = "license"
                    }
                    TRAFFIC->{
                        fileName = "traffic"
                    }
                    REGIONS->{
                        fileName = "regions"
                    }
                }
                return fileName
            }
            fun updateTrfficJson(jsonObject: JSONObject){
                updateJson(jsonObject, TRAFFIC)
            }
            fun updateLicenseJson(jsonObject: JSONObject){
                updateJson(jsonObject, LICENSE)
            }
            fun updateRegions(jsonObject: JSONObject){
                updateJson(jsonObject, REGIONS)
            }
            fun updateJson(jsonObject: JSONObject, type: Int){
                updateJson(jsonObject.toString(),type)
            }
            fun updateJson(data: String, type: Int) {

                if (supportOfLocalJsons) {
                    Thread(Runnable {
                        try {
                            val outputStreamWriter = OutputStreamWriter(FinjanVPNApplication.getInstance().openFileOutput(getFileName(type) + ".txt", Context.MODE_PRIVATE))
//                        val outputStreamWriter = FileOutputStream (File(getFileName(type)), true)
                            outputStreamWriter.write(data)
                            outputStreamWriter.close()
                        } catch (e: IOException) {
                            Logger.logE("Exception", "File write failed: " + e.toString())
                        }
                    }).start()
                }
            }
        }
    }
}