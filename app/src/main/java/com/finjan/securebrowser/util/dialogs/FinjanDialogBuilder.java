package com.finjan.securebrowser.util.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finjan.securebrowser.R;
import com.finjan.securebrowser.helpers.GlobalVariables;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.parser.PListParser;

/**
 * Created by anurag on 20/07/17.
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class FinjanDialogBuilder implements View.OnClickListener {
    private Context context;
    private String msg, positiveButton, negativeButton, title;
    private String[] contents;
    private LayoutInflater inflater;
    private boolean cancelable,havingCancelButton;
    private DialogClickListener dialogClickListener;
    private PositiveClickListener positiveClickListener;
    private NegativeClickListener negativeClickListener;
    private boolean outsideClickable = true;
    private Dialog dialog;

    public FinjanDialogBuilder(Context context) {
        if(context==null){return;}
        this.context = context;
        inflater = LayoutInflater.from(context);
    }
    public boolean isShowing(){
        return dialog!=null && dialog.isShowing();
    }
    public void dismiss(){
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    public FinjanDialogBuilder setMessage(String msg) {
        this.msg = msg;
        return this;
    }

    public FinjanDialogBuilder setTitle(String title) {
        this.title = title;
        return this;
    }


    public FinjanDialogBuilder setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }

    public FinjanDialogBuilder setContent(String[] content) {
        this.contents = content;
        return this;
    }
    public String[] getContents(){return contents;}

    public FinjanDialogBuilder setPositiveButton(String positiveButton) {
        this.positiveButton = positiveButton;
        return this;
    }

    public FinjanDialogBuilder setNegativeButton(String negativeButton) {
        this.negativeButton = negativeButton;
        return this;
    }
    public FinjanDialogBuilder shouldHaveCancelButton(boolean shouldHaveCancelButton){
        this.havingCancelButton = shouldHaveCancelButton;
        return this;
    }

    private int negativeBtnColour=0;
    private int positiveBtnColour=0;
    private int okBtnColour=0;
    public FinjanDialogBuilder setCustomNegativeBtnColour(@NonNull  String colour){
        this.negativeBtnColour=PListParser.stringToColor(colour);
        return this;
    }

    public FinjanDialogBuilder setCustompOkBtnColour(@NonNull  String colour){
        this.okBtnColour= PListParser.stringToColor(colour);
        return this;
    }
    public FinjanDialogBuilder setCustomPositiveBtnColour(@NonNull  String colour){
        this.positiveBtnColour=PListParser.stringToColor(colour);
        return this;
    }

    public FinjanDialogBuilder setDialogClickListener(PositiveClickListener dialogClickListener) {
        this.positiveClickListener= dialogClickListener;
        return this;
    }
    public FinjanDialogBuilder setDialogClickListener(NegativeClickListener dialogClickListener) {
        this.negativeClickListener= dialogClickListener;
        return this;
    }
    public FinjanDialogBuilder setDialogClickListener(DialogClickListener dialogClickListener) {
        this.dialogClickListener = dialogClickListener;
        return this;
    }

    public FinjanDialogBuilder outSideClickable(boolean outsideClickable) {
        this.outsideClickable = outsideClickable;
        return this;
    }
    private static final String TAG=FinjanDialogBuilder.class.getSimpleName();
    public void show() {
        if (!Looper.getMainLooper().equals(Looper.myLooper())) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    showDialog();
                }
            });
        }else{
            showDialog();
        }
    }
    private void showDialog(){
        if (context == null) {
            Logger.logE(TAG,"Context can not be empty");
            return;
        }
        if (TextUtils.isEmpty(msg)) {
            Logger.logE(TAG,"Message can not be empty");
            return;
        }
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.view_dialog_holder);
        dialog.setCanceledOnTouchOutside(outsideClickable);
        dialog.setCancelable(cancelable);
        setTitle();
        setContents();
        setButtons();
        dialog.show();
    }

    private void setTitle(){
        TextView tvMsg = ((TextView)dialog.findViewById(R.id.txt_dialg_title));
        tvMsg.setText(msg);
        TextView textView= ((TextView)dialog.findViewById(R.id.txt_title));
        dialog.findViewById(R.id.iv_dialog_close).setVisibility(havingCancelButton?View.VISIBLE:View.GONE);
    if(!TextUtils.isEmpty(title)){
        textView.setVisibility(View.VISIBLE);
        textView.setText(title);
    }else {
        textView.setVisibility(View.GONE);
    }
        dialog.findViewById(R.id.iv_dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    if(!TextUtils.isEmpty(title) && !TextUtils.isEmpty(msg)){
        int commonPadding = Math.round(context.getResources().getDimension(R.dimen._15sdp));
        int topPadding = Math.round(context.getResources().getDimension(R.dimen._10sdp));
        int bottomPadding = Math.round(context.getResources().getDimension(R.dimen._3sdp));

        tvMsg.setPadding(commonPadding,bottomPadding,commonPadding,topPadding);
        textView.setPadding(commonPadding,topPadding,commonPadding,bottomPadding);
    }
    }

    private void setButtons() {
        TextView tvDone, tvCancel, tvDoneSingle;
        View view = inflater.inflate(R.layout.view_dialog_done_button, null);
        LinearLayout frame = (LinearLayout) dialog.findViewById(R.id.ll_container);
        tvDone = (TextView) view.findViewById(R.id.tv_done);
        tvCancel = (TextView) view.findViewById(R.id.tv_cancel);
        tvDoneSingle = (TextView) view.findViewById(R.id.tv_done_single);

        if (!TextUtils.isEmpty(positiveButton) || !TextUtils.isEmpty(negativeButton)) {
            if (!TextUtils.isEmpty(positiveButton) && !TextUtils.isEmpty(negativeButton)) {
                view.findViewById(R.id.ll_one_button).setVisibility(View.GONE);
                tvDone.setText(positiveButton);
                tvCancel.setText(negativeButton);
            } else {
                view.findViewById(R.id.ll_two_buttons).setVisibility(View.GONE);
                if (!TextUtils.isEmpty(positiveButton)) {
                    tvDoneSingle.setText(positiveButton);
                } else {
                    tvDoneSingle.setText(negativeButton);
                }
            }
        } else if (TextUtils.isEmpty(positiveButton) && TextUtils.isEmpty(negativeButton)) {
            view.findViewById(R.id.ll_two_buttons).setVisibility(View.GONE);
            view.findViewById(R.id.ll_one_button).setVisibility(View.VISIBLE);
            tvDoneSingle.setText(context.getString(R.string.alert_ok));
//            tvCancel.setText(context.getString(R.string.cancel));
        }

        if(negativeBtnColour!=0){
            tvCancel.setBackground(new SomeDrawable(tvCancel.getContext(),negativeBtnColour,1));
        }
        if(positiveBtnColour!=0){
            tvDone.setBackground(new SomeDrawable(tvCancel.getContext(),positiveBtnColour,0));
        }
        if(okBtnColour!=0){
            tvDoneSingle.setBackground(new SomeDrawable(tvCancel.getContext(),positiveBtnColour,2));
        }

        tvDone.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvDoneSingle.setOnClickListener(this);
        frame.addView(view);
    }

    private void setContents() {
        if (contents != null &&
                contents.length > 0 && dialog != null) {
            LinearLayout frame = (LinearLayout) dialog.findViewById(R.id.ll_container);
            for (int i=0;i<contents.length;i++) {
                if(TextUtils.isEmpty(contents[i])){
                    continue;
                }
                TextView textView = (TextView) inflater.inflate(R.layout.view_dialog_content, null);
                textView.setText(contents[i]);
                frame.addView(textView);
                textView.setTag(contents[i]);
//                textView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialogClickListener.onContentClick(title);
//                    }
//                });
                textView.setOnClickListener(this);
            }
        }
    }

    @Override
    public void onClick(View v) {
        dialog.dismiss();
            switch (v.getId()) {
                case R.id.tv_cancel:
                    dialog.dismiss();
                    if(dialogClickListener!=null){
                        dialogClickListener.onNegativeClick();
                    }
                    if(negativeClickListener!=null){
                        negativeClickListener.onNegativeClick();
                    }

                    break;
                case R.id.tv_done:
                    if(dialogClickListener!=null){
                        dialogClickListener.onPositivClick();
                    }
                    if(positiveClickListener!=null){
                        positiveClickListener.onPositivClick();
                    }
                    break;
                case R.id.tv_done_single:
                    if(dialogClickListener!=null){
                        dialogClickListener.onPositivClick();
                    }
                    if(positiveClickListener!=null){
                        positiveClickListener.onPositivClick();
                    }
                    break;
                case R.id.txt_dialog_use_home:
                    if(dialogClickListener!=null){
                        dialogClickListener.onContentClick((String) v.getTag());
                    }

                    break;
            }
    }

    public static void connectionErrorDialog(Context context,PositiveClickListener positiveClickListener){
        new FinjanDialogBuilder(context).setMessage(context.getString(R.string.unable_to_connect_try_again_after_some_time))
                .setCancelable(false)
                .outSideClickable(false)
                .setPositiveButton(context.getString(R.string.alert_ok))
                .setDialogClickListener(positiveClickListener)
                .show();
    }

    public static void networkErrorDialog(Context context,PositiveClickListener positiveClickListener){
        new FinjanDialogBuilder(context).setMessage(context.getString(R.string.network_message))
                .setCancelable(false)
                .outSideClickable(false)
                .setPositiveButton(context.getString(R.string.alert_ok))
                .setDialogClickListener(positiveClickListener)
                .show();
    }
    public static void showPermissionSettingDialog(final Context context,PositiveClickListener positiveClickListener){
        new FinjanDialogBuilder(context).setMessage(context.getString(R.string.user_forcefully_disable_a_permission))
                .setPositiveButton(context.getString(R.string.alert_ok))
                .setNegativeButton(context.getString(R.string.alert_cancel))
                .setDialogClickListener(positiveClickListener).show();
    }
    public static void permissionDialog(final Context context, String msg,PositiveClickListener positiveClickListener){
        new FinjanDialogBuilder(context).setMessage(msg)
                .setCancelable(false)
                .outSideClickable(false)
                .setPositiveButton(context.getString(R.string.alert_accept))
                .setNegativeButton(context.getString(R.string.alert_cancel))
                .setDialogClickListener(new NegativeClickListener() {
                    @Override
                    public void onNegativeClick() {
                        ((Activity)context).finish();
                    }
                }).setDialogClickListener(positiveClickListener).show();
    }

    public interface PositiveClickListener{
        void onPositivClick();
    }
    public interface NegativeClickListener{
        void onNegativeClick();
    }
    public interface DialogClickListener {
        void onNegativeClick();

        void onPositivClick();

        void onContentClick(String content);
    }
    public static void showPermissionDeclinedAlert(final Context context){
        new FinjanDialogBuilder(context).setMessage(context.getString(R.string.user_forcefully_disable_a_permission))
                .setPositiveButton(context.getString(R.string.alert_ok))
                .setNegativeButton(context.getString(R.string.alert_cancel))
                .setDialogClickListener(new FinjanDialogBuilder.PositiveClickListener() {
                    @Override
                    public void onPositivClick() {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", context.getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                }).show();
    }
    public static void showNetworkError(final Activity activity){
        FinjanDialogBuilder.networkErrorDialog(activity, new FinjanDialogBuilder.PositiveClickListener() {
            @Override
            public void onPositivClick() {
                GlobalVariables.getInstnace().isFinishCalled = true;
                activity.finish();
            }
        });
    }
    public static void showPermissionError(final Activity activity){
        FinjanDialogBuilder.showPermissionSettingDialog(activity,new FinjanDialogBuilder.PositiveClickListener() {
            @Override
            public void onPositivClick() {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", activity.getPackageName(), null));
                activity.startActivityForResult(intent,101);
            }
        });    }

    public static void showPermissionRequest(Activity activity,int msgRes,NegativeClickListener negativeClickListener,PositiveClickListener positiveClickListener){

        new FinjanDialogBuilder(activity).setMessage(ResourceHelper.getInstance().getString(msgRes))
                .setPositiveButton(ResourceHelper.getInstance().getString(R.string.alert_ok))
                .setNegativeButton(ResourceHelper.getInstance().getString(R.string.alert_cancel))
                .setDialogClickListener(positiveClickListener)
                .setDialogClickListener(negativeClickListener)
                .show();
          }



    public class SomeDrawable extends GradientDrawable {

        private static final int rightBttn=0;
        private static final int leftBttn=1;
        private static final int okBttn=2;

        public SomeDrawable(Context context,int colour,int buttenSide){
            super(Orientation.BOTTOM_TOP,new int[]{colour,colour,colour});
            setShape(GradientDrawable.RECTANGLE);
            float value=context.getResources().getDimension(R.dimen._8sdp);
            switch (buttenSide){
                case rightBttn:
                    setCornerRadii(new float[]{0,0,0,0,value,value,0,0});
                    break;
                case leftBttn:
                    setCornerRadii(new float[]{0,0,0,0,0,0,value,value});
                    break;
                case okBttn:
                    setCornerRadii(new float[]{0,0,0,0,value,value,value,value});
                    break;
            }
        }

    }

}
