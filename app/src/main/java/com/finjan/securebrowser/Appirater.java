package com.finjan.securebrowser;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finjan.securebrowser.helpers.PlistHelper;
import com.finjan.securebrowser.helpers.context_helper.ResourceHelper;
import com.finjan.securebrowser.model.ModelManager;
import com.finjan.securebrowser.model.RateUsModel;

/*	
 * @source https://github.com/sbstrm/appirater-android
 * @license MIT/X11
 * 
 * Copyright (c) 2011-2013 sbstrm Y.K.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

public class Appirater {

	private static final String PREF_LAUNCH_COUNT = "launch_count";
	private static final String PREF_EVENT_COUNT = "event_count";
	private static final String PREF_RATE_CLICKED = "rateclicked";
	private static final String PREF_DONT_SHOW = "dontshow";
	private static final String PREF_DATE_REMINDER_PRESSED = "date_reminder_pressed";
	private static final String PREF_DATE_FIRST_LAUNCHED = "date_firstlaunch";
	private static final String PREF_APP_VERSION_CODE = "versioncode";
    private static final String PREF_APP_LOVE_CLICKED= "loveclicked";
    private static RateUsModel rateUsModel;

    public static void appLaunched(final Activity mContext) {
//    	boolean testMode = mContext.getResources().getBoolean(R.bool.appirator_test_mode);
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean testMode =false;
                SharedPreferences prefs = mContext.getSharedPreferences(mContext.getPackageName()+".appirater", 0);
                rateUsModel=ModelManager.getInstance().getRateUsModel();
                if(!testMode && (prefs.getBoolean(PREF_DONT_SHOW, false) || prefs.getBoolean(PREF_RATE_CLICKED, false))) {return;}

                if (rateUsModel==null)
                    return;


                SharedPreferences.Editor editor = prefs.edit();

                if(testMode){
                    if (prefs.getBoolean(PREF_APP_LOVE_CLICKED, true)) {
                        showRateDialog(mContext,editor);
                    } else {
//                showLoveDialog(mContext, editor);
                    }
                    return;
                }

                // Increment launch counter
                long launch_count = prefs.getLong(PREF_LAUNCH_COUNT, 0);

                // Get events counter
                long event_count = prefs.getLong(PREF_EVENT_COUNT, 0);

                // Get date of first launch
                long date_firstLaunch = prefs.getLong(PREF_DATE_FIRST_LAUNCHED, 0);

                // Get reminder date pressed
                long date_reminder_pressed = prefs.getLong(PREF_DATE_REMINDER_PRESSED, 0);

                try{
                    int appVersionCode = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode;
                    if(prefs.getInt(PREF_APP_VERSION_CODE, 0)  != appVersionCode){
                        //Reset the launch and event counters to help assure users are rating based on the latest version.
                        launch_count = 0;
                        event_count = 0;
                        editor.putLong(PREF_EVENT_COUNT, event_count);
                    }
                    editor.putInt(PREF_APP_VERSION_CODE, appVersionCode);
                }catch(Exception e){
                    //do nothing
                }

                launch_count++;
                editor.putLong(PREF_LAUNCH_COUNT, launch_count);

                if (date_firstLaunch == 0) {
                    date_firstLaunch = System.currentTimeMillis();
                    editor.putLong(PREF_DATE_FIRST_LAUNCHED, date_firstLaunch);
                }

                // Wait at least n days or m events before opening
                if (launch_count > rateUsModel.getDaysUntillPromt()) {
                    long millisecondsToWait = rateUsModel.getDaysUntillPromt()* 24 * 60 * 60 * 1000L;
                    if (System.currentTimeMillis() >= (date_firstLaunch + millisecondsToWait) || event_count >= rateUsModel.getUsagesUntillPromt()) {
                        if(date_reminder_pressed == 0){
                            if (prefs.getBoolean(PREF_APP_LOVE_CLICKED, true)){
                                showRateDialog(mContext, editor);
                            } else {
//                        showLoveDialog(mContext, editor);
                            }
                        }else{
                            long remindMillisecondsToWait = rateUsModel.getTimeBeforeReminding() * 24 * 60 * 60 * 1000L;
                            if(System.currentTimeMillis() >= (remindMillisecondsToWait + date_reminder_pressed)){
                                if (prefs.getBoolean(PREF_APP_LOVE_CLICKED, true)) {
                                    showRateDialog(mContext, editor);
                                } else {
//                            showLoveDialog(mContext, editor);
                                }
                            }
                        }
                    }
                }
                editor.commit();
            }
        }).start();

    }   

    public static void rateApp(Context mContext)
    {
        SharedPreferences prefs = mContext.getSharedPreferences(mContext.getPackageName()+".appirater", 0);
        SharedPreferences.Editor editor = prefs.edit();
        rateApp(mContext, editor);
    }

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static void significantEvent(final Context mContext) {
        new Thread(new Runnable() {
            @Override
            public void run() {
//        boolean testMode = mContext.getResources().getBoolean(R.bool.appirator_test_mode);
                boolean testMode = true;
                SharedPreferences prefs = mContext.getSharedPreferences(mContext.getPackageName()+".appirater", 0);
                if(!testMode && (prefs.getBoolean(PREF_DONT_SHOW, false) || prefs.getBoolean(PREF_RATE_CLICKED, false))) {return;}

                long event_count = prefs.getLong(PREF_EVENT_COUNT, 0);
                event_count++;
                prefs.edit().putLong(PREF_EVENT_COUNT, event_count).apply();
            }
        }).start();

    }

    private static void rateApp(Context mContext, final SharedPreferences.Editor editor) {
        mContext.startActivity(new Intent(Intent.ACTION_VIEW,
//                Uri.parse(String.format(Utils.getPlistvalue(Constants.KEY_APP_URL,Constants.KEY_ANDROID),
//                        rateUsModel.getPackageName()))));
                Uri.parse(String.format(PlistHelper.getInstance().getAppUrl(),
                        rateUsModel.getPackageName()))));
        if (editor != null) {
            editor.putBoolean(PREF_RATE_CLICKED, true);
            editor.commit();
        }
    }

	@SuppressLint("NewApi")
	public static void showRateDialog(final Activity mContext, final SharedPreferences.Editor editor) {
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String appName = mContext.getString(R.string.app_name_rate);
                final Dialog dialog = new Dialog(mContext);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                if (dialog.getWindow() != null) {
                    dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                }
                View layout = LayoutInflater.from(mContext).inflate(R.layout.appirater, null);

                TextView tv =(TextView) layout.findViewById(R.id.message);
                tv.setText(rateUsModel.getMessage());
                if(TextUtils.isEmpty(rateUsModel.getMessage())){tv.setVisibility(View.GONE);}

                ((TextView)layout.findViewById(R.id.tv_title)).setText(appName);
                TextView rateButton = (TextView)layout.findViewById(R.id.rate);
                rateButton.setText(mContext.getString(R.string.rate_now));
                rateButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        rateApp(mContext, editor);
                        dialog.dismiss();
                    }
                });

                TextView rateLaterButton = (TextView)layout.findViewById(R.id.rateLater);
                rateLaterButton.setText(mContext.getString(R.string.rate_later));
                rateLaterButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (editor != null) {
                            editor.putLong(PREF_DATE_REMINDER_PRESSED,System.currentTimeMillis());
                            editor.commit();
                        }
                        dialog.dismiss();
                    }
                });

                TextView cancelButton =(TextView)layout.findViewById(R.id.cancel);
                cancelButton.setText(mContext.getString(R.string.rate_cancel));
                cancelButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (editor != null) {
                            editor.putBoolean(PREF_DONT_SHOW, true);
                            editor.commit();
                        }
                        dialog.dismiss();
                    }
                });

                dialog.setContentView(layout);
                dialog.show();
            }
        });

    }
}