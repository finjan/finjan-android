package com.finjan.securebrowser;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public enum CustomPagerEnum {

    History(R.string.history, R.layout.history_expandable),
    Bookmarks(R.string.bookmarks, R.layout.history_view),
    Tabs(R.string.title_tabs,0),
    Private(R.string.title_private,0);

    private int mTitleResId;
    private int mLayoutResId;

    CustomPagerEnum(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}