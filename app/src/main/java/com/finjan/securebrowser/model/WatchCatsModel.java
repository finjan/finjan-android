package com.finjan.securebrowser.model;

import java.util.ArrayList;

public class WatchCatsModel {

private ArrayList<Category> data;

    public ArrayList<Category> getData() {
        return data;
    }

    public void setData(ArrayList<Category> data) {
        this.data = data;
    }

    public class Category {
        private String id;
        private String type;
        Attributes attributes;


        // Getter Methods

        public String getId() {
            return id;
        }

        public String getType() {
            return type;
        }

        public Attributes getAttributes() {
            return attributes;
        }

        // Setter Methods

        public void setId(String id) {
            this.id = id;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setAttributes(Attributes attributesObject) {
            this.attributes = attributesObject;
        }
    }
    public class Attributes {
        private String title;
        private String description;
        private String image;
        private String country;
        private String url;
        private String expires;
        private String featured;


        // Getter Methods

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getImage() {
            return image;
        }

        public String getCountry() {
            return country;
        }

        public String getUrl() {
            return url;
        }

        public String getExpires() {
            return expires;
        }

        public String getFeatured() {
            return featured;
        }

        // Setter Methods

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void setExpires(String expires) {
            this.expires = expires;
        }

        public void setFeatured(String featured) {
            this.featured = featured;
        }
    }
}
