package com.finjan.securebrowser.model;

import android.text.TextUtils;

import com.finjan.securebrowser.parser.PList;

import org.json.JSONException;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 */

public class ModelManager {

    private RateUsModel rateUsModel = null;
    private PList launchModel = null;
    private int orientation = 0;
    private TrackerModel trackerModel = null;
    private SafeSearchCount safeSearchCount = null;

    private static final class ModelManagerHolder {
        private static final ModelManager INSTANCE = new ModelManager();
    }

    public static ModelManager getInstance() {
        return ModelManagerHolder.INSTANCE;
    }

    private ModelManager() {
    }


    public void setPList(PList response) {
        try {
            launchModel = response;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PList getPList() {
        return launchModel;
    }

    public void setRateUsModel(RateUsModel model) {
        this.rateUsModel = model;
    }

    public RateUsModel getRateUsModel() {
        return rateUsModel;
    }

    public void setTrackerModel(String jsonData) throws JSONException {
        if (TextUtils.isEmpty(jsonData))
            return;
        this.trackerModel = new TrackerModel(jsonData);
    }

    public TrackerModel getTrackerModel() {
        return trackerModel;
    }


    public void setSafeSearchCountModel(String jsonData) {
        if (TextUtils.isEmpty(jsonData))
            return;

        try {
            this.safeSearchCount = new SafeSearchCount(jsonData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public SafeSearchCount getSafeSearchCountModel() {
        return safeSearchCount;
    }
    private boolean safeSearchPurchase;
    public void setSafeSearchPurchase(boolean safeSearchPurchase){
        this.safeSearchPurchase=safeSearchPurchase;
    }
    public boolean getSafeSearchPurchase(){return safeSearchPurchase;}
}
