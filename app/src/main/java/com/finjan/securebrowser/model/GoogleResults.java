package com.finjan.securebrowser.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class GoogleResults {
    String title="";
    String url="";
    String Desc="";
    ArrayList<GoogleResults> mGoogleResults=new ArrayList<>();

public GoogleResults(){}
    public GoogleResults(Object response) throws JSONException {
    if (response instanceof JSONArray){
        try {
            JSONArray jsonArray=new JSONArray(response.toString());

        }catch(Exception e){}

    }
//    else if (response instanceof JSONObject){
try {
    JSONObject mJsonObject=new JSONObject(response.toString());
    if (mJsonObject!=null && mJsonObject.length()>0 && mJsonObject.has("items")){
        if (mJsonObject.get("items") instanceof JSONArray){
            JSONArray itemArray = (JSONArray) mJsonObject.get("items");
            for (int i=0;i<itemArray.length();i++){
                JSONObject itemObject = (JSONObject) itemArray.get(i);
                if (itemObject==null)
                    continue;
                mGoogleResults.add(googleResultObject(itemObject));

            }
        }else {
            mGoogleResults.add(googleResultObject(mJsonObject.getJSONObject("items")));
        }

    }
}catch(Exception e){}
//    }
    }

    private GoogleResults googleResultObject(JSONObject itemObject) throws JSONException {
        GoogleResults results = new GoogleResults();
        if (itemObject.has("title") && !itemObject.isNull("title")){
            results.title=itemObject.getString("title");
        }
        if (itemObject.has("htmlTitle") && !itemObject.isNull("htmlTitle")){
            results.Desc=itemObject.getString("htmlTitle");
        }
        if (itemObject.has("link") && !itemObject.isNull("link")){
            results.url=itemObject.getString("link");
        }

        return results;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public ArrayList<GoogleResults> getmGoogleResults() {
        return mGoogleResults;
    }

    public void setmGoogleResults(ArrayList<GoogleResults> mGoogleResults) {
        this.mGoogleResults = mGoogleResults;
    }
}
