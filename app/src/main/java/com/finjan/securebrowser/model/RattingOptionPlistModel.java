package com.finjan.securebrowser.model;

import android.text.TextUtils;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 30/05/17.
 *
 */

public class RattingOptionPlistModel {

    public RattingOptionPlistModel(){}
    public RattingOptionPlistModel(int dUtilPromp,String msg,int time_before_reminding,int uses_until_prompt,String android_package){
        this.days_until_prompt=dUtilPromp;
        this.message=msg;
        this.time_before_reminding=time_before_reminding;
        this.uses_until_prompt=uses_until_prompt;
        this.android_package=android_package;
    }
    private String message,android_package;
    private int days_until_prompt,time_before_reminding,uses_until_prompt;

    public int getDays_until_prompt() {
        return days_until_prompt;
    }

    public void setDays_until_prompt(String days_until_prompt) {
        if(TextUtils.isEmpty(days_until_prompt)){
            this.days_until_prompt=0;
        }else {
            this.days_until_prompt = Integer.parseInt(days_until_prompt);
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTime_before_reminding() {
        return time_before_reminding;
    }

    public void setTime_before_reminding(String time_before_reminding) {

        if(TextUtils.isEmpty(time_before_reminding)){
            this.time_before_reminding=0;
        }else {
            this.time_before_reminding = Integer.parseInt(time_before_reminding);
        }
    }

    public int getUses_until_prompt() {
        return uses_until_prompt;
    }

    public void setUses_until_prompt(String uses_until_prompt) {
        if(TextUtils.isEmpty(uses_until_prompt)){
            this.uses_until_prompt=0;
        }else {
            this.uses_until_prompt = Integer.parseInt(uses_until_prompt);
        }
    }

    public String getAndroid_package() {
        return android_package;
    }

    public void setAndroid_package(String android_package) {
        this.android_package = android_package;
    }
}
