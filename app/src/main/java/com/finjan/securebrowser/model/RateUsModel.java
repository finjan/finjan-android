package com.finjan.securebrowser.model;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 *
 */

public class RateUsModel
{
    private String message="";
    private String packageName="";
    private int daysUntillPromt=0;
    private int timeBeforeReminding=0;
    private int  usagesUntillPromt=0;

    public RateUsModel(){}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getDaysUntillPromt() {
//        return 0;
        return daysUntillPromt;
    }

    public void setDaysUntillPromt(int daysUntillPromt) {
        this.daysUntillPromt = daysUntillPromt;
    }

    public int getTimeBeforeReminding() {
        return timeBeforeReminding;
//        return 0;
    }

    public void setTimeBeforeReminding(int timeBeforeReminding) {
        this.timeBeforeReminding = timeBeforeReminding;
    }

    public int getUsagesUntillPromt() {
        return usagesUntillPromt;
//        return 2;
    }

    public void setUsagesUntillPromt(int usagesUntillPromt) {
        this.usagesUntillPromt = usagesUntillPromt;
    }
}
