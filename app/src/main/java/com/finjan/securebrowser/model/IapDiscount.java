package com.finjan.securebrowser.model;


import java.util.ArrayList;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 12/11/18.
 *
 */

public class IapDiscount{
    private String name,ios;
            private ArrayList<String> android;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIos() {
        return ios;
    }

    public void setIos(String ios) {
        this.ios = ios;
    }

    public ArrayList<String> getAndroid() {
        return android;
    }

    public void setAndroid(ArrayList<String> android) {
        this.android = android;
    }
}
