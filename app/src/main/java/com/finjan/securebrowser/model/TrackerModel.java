package com.finjan.securebrowser.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class TrackerModel {
    private ArrayList<TrackerDataModel> Content;
    private ArrayList<TrackerDataModel> Advertising;
    private ArrayList<TrackerDataModel> Analytics;
    private ArrayList<TrackerDataModel> Social;
    private ArrayList<TrackerDataModel> Disconnect;
    private String CATEGORIES_TAG = "categories";
    private String ADVERTISING_TAG = "Advertising";
    private String ANALYTICS_TAG = "Analytics";
    private String SOCIAL_TAG = "Social";
    private String DISCONNECT_TAG = "Disconnect";
    private String TRACKER_TAG = "Content";


    public TrackerModel(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        if (jsonObject != null && jsonObject.has(CATEGORIES_TAG)) {
            JSONObject categoryJson = jsonObject.getJSONObject(CATEGORIES_TAG);
            if (categoryJson != null) {
                getAdvertising(categoryJson);
                getContent(categoryJson);
                getAnalytics(categoryJson);
                getSocial(categoryJson);
                getDisconnect(categoryJson);

            }

        }
    }

    private void getDisconnect(JSONObject categoryJson) throws JSONException {
        Object disconnectObject = categoryJson.get(DISCONNECT_TAG);
        Iterator<String> keys;
        if (disconnectObject != null) {
            Disconnect = new ArrayList<>();
            if (disconnectObject instanceof JSONObject) {
                JSONObject object = categoryJson.getJSONObject(DISCONNECT_TAG);
                Disconnect.add(getObjectValue(object,DISCONNECT_TAG));
            } else {
                JSONArray array = categoryJson.getJSONArray(DISCONNECT_TAG);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    Disconnect.add(getObjectValue(jsonObject,DISCONNECT_TAG));
                }
            }
        }

    }

    private void getSocial(JSONObject categoryJson) throws JSONException {
        Object disconnectObject = categoryJson.get(SOCIAL_TAG);
        Iterator<String> keys;
        if (disconnectObject != null) {
            Social = new ArrayList<>();
            if (disconnectObject instanceof JSONObject) {
                JSONObject object = categoryJson.getJSONObject(SOCIAL_TAG);
                Social.add(getObjectValue(object,SOCIAL_TAG));
            } else {
                JSONArray array = categoryJson.getJSONArray(SOCIAL_TAG);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    Social.add(getObjectValue(jsonObject,SOCIAL_TAG));
                }
            }
        }
    }

    private void getAnalytics(JSONObject categoryJson) throws JSONException {
        Object disconnectObject = categoryJson.get(ANALYTICS_TAG);
        Iterator<String> keys;
        if (disconnectObject != null) {
            Analytics = new ArrayList<>();
            if (disconnectObject instanceof JSONObject) {
                JSONObject object = categoryJson.getJSONObject(ANALYTICS_TAG);
                Analytics.add(getObjectValue(object,ANALYTICS_TAG));
            } else {
                JSONArray array = categoryJson.getJSONArray(ANALYTICS_TAG);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    Analytics.add(getObjectValue(jsonObject,ANALYTICS_TAG));
                }
            }
        }
    }

    private void getContent(JSONObject categoryJson) throws JSONException {
        Object disconnectObject = categoryJson.get(TRACKER_TAG);
        Iterator<String> keys;
        if (disconnectObject != null) {
            Content = new ArrayList<>();
            if (disconnectObject instanceof JSONObject) {
                JSONObject object = categoryJson.getJSONObject(TRACKER_TAG);
                Content.add(getObjectValue(object,TRACKER_TAG));
            } else {
                JSONArray array = categoryJson.getJSONArray(TRACKER_TAG);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    Content.add(getObjectValue(jsonObject,TRACKER_TAG));
                }
            }
        }
    }

    private void getAdvertising(JSONObject categoryJson) throws JSONException {
        Object disconnectObject = categoryJson.get(ADVERTISING_TAG);
        Iterator<String> keys;
        if (disconnectObject != null) {
            Advertising = new ArrayList<>();
            if (disconnectObject instanceof JSONObject) {
                JSONObject object = categoryJson.getJSONObject(ADVERTISING_TAG);
                Advertising.add(getObjectValue(object,ADVERTISING_TAG));

            } else {
                JSONArray array = categoryJson.getJSONArray(ADVERTISING_TAG);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    Advertising.add(getObjectValue(jsonObject,ADVERTISING_TAG));
                }
            }
        }
    }

    private TrackerDataModel getObjectValue(JSONObject object,String type) throws JSONException {
        TrackerDataModel model = new TrackerDataModel();
        if (object != null) {
            model.setTrackerType(type);
            Iterator<String> keys = object.keys();
            if (keys.hasNext()) {
                model.setTrackerName(keys.next());
                JSONObject urlObject = object.getJSONObject(model.getTrackerName());
                Iterator<String> urlKeys = urlObject.keys();
                if (urlKeys.hasNext()) {
                    model.setTrackerUrl(urlKeys.next());
                    JSONArray domainArray = urlObject.getJSONArray(model.getTrackerUrl());
                    if (domainArray != null && domainArray.length() > 0) {
                        ArrayList<String> domainList = new ArrayList<>();
                        for (int i = 0; i < domainArray.length(); i++) {
                            domainList.add(domainArray.get(i).toString());
                        }
                        model.setTrackerDataList(domainList);
                    }
                }
            }
        }
        return model;
    }

    public ArrayList<TrackerDataModel> getAdvertising() {
        return Advertising;
    }

    public ArrayList<TrackerDataModel> getContent() {
        return Content;
    }

    public ArrayList<TrackerDataModel> getAnalytics() {
        return Analytics;
    }

    public ArrayList<TrackerDataModel> getSocial() {
        return Social;
    }

    public ArrayList<TrackerDataModel> getDisconnect() {
        return Disconnect;
    }

    public String getCategoriesTag() {
        return CATEGORIES_TAG;
    }

    public String getAdvertisingTag() {
        return ADVERTISING_TAG;
    }

    public String getAnalyticsTag() {
        return ANALYTICS_TAG;
    }

    public String getSocialTag() {
        return SOCIAL_TAG;
    }

    public String getDisconnectTag() {
        return DISCONNECT_TAG;
    }

    public String getTrackerTag() {
        return TRACKER_TAG;
    }
}
