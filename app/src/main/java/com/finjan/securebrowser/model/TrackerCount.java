package com.finjan.securebrowser.model;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class TrackerCount {

    private String name="";
    private int count=0;
    private boolean ischecked=false;



    public TrackerCount(String name, int count){
        this.name=name;
        this.count=count;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }
    public boolean ischecked() {
        return ischecked;
    }

    public void setIschecked(boolean ischecked) {
        this.ischecked = ischecked;
    }
}
