package com.finjan.securebrowser.model;

import android.os.Parcelable;
import android.text.TextUtils;

import com.finjan.securebrowser.helpers.sharedpref.UserPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anurag on 09/08/17.
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 *
 */

public class FinjanUser{
    String first_name;
    String last_name;
    String email;
    String phone;
    String salutation;
    String address;
    String optin;
    String user_type;
    String fb_id;
    String gg_id;
    String beta_tester;
    String status_tfa;
    String phone_tfa;
    String country;
    String state;
    String city;
    String zip;
    String organization_name;
    String profile_image_full;
    String profile_image_large;
    String profile_image_medium;
    String profile_image_small;
    String user_registered;
    String registration_type;
    String source;
    String devices_count;
    String desktop_registered;
    String ios_registered;
    String android_registered;
    String login_by;

    public String getId() {
        return id;
    }

    String id;

    public FinjanUser(JSONObject jsonObject){

        if(jsonObject!=null && jsonObject.has("data")){
            try {
                JSONArray jsonArray=jsonObject.getJSONArray("data");

                if(jsonArray.length()>0 && jsonArray.getJSONObject(0)!=null){
                    id = jsonArray.getJSONObject(0).getString("id");
                    JSONObject user=jsonArray.getJSONObject(0).getJSONObject("attributes");
                    this.first_name=user.getString("first_name");
                    this.last_name=user.getString("last_name");
                    this.email=user.getString("email");
                    this.phone=user.optString("phone");
                    this.salutation=user.optString("salutation");
                    this.address=user.optString("address");
                    this.optin=user.optString("optin");
                    this.user_type=user.optString("user_type");
                    this.fb_id=user.optString("fg_id");
                    this.gg_id=user.optString("gg_id");
                    this.beta_tester=user.optString("beta_tester");
                    this.status_tfa=user.optString("status_tfa");
                    this.phone_tfa=user.optString("phone_tfa");
                    this.country=user.optString("country");
                    this.state=user.optString("state");
                    this.city=user.optString("city");
                    this.zip=user.optString("zip");
                    this.organization_name=user.optString("organization_name");
                    this.profile_image_full=user.optString("profile_image_full");
                    this.profile_image_large=user.optString("profile_image_large");
                    this.profile_image_medium=user.optString("profile_image_medium");
                    this.profile_image_small=user.optString("profile_image_small");
                    this.user_registered=user.optString("user_registered");
                    this.registration_type=user.optString("registration_type");
                    this.source=user.optString("source");
                    this.devices_count=user.optString("devices_count");
                    this.android_registered=user.optString("android_registered");
                    this.desktop_registered=user.optString("desktop_registered");
                    this.ios_registered=user.optString("ios_registered");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getFirst_name() {
        return first_name!=null?first_name:"";
    }
    public String  getUserName(){
        return getFirst_name()+(TextUtils.isEmpty(getLast_name())?"":" "+getLast_name());
    }

    public String getLast_name() {
        return last_name!=null?last_name:"";
    }

    public String getEmail() {
        return email!=null?email:"";
    }

    public String getPhone() {
        return phone!=null?phone:"";
    }

    public String getSalutation() {
        return salutation!=null?salutation:"";
    }

    public String getAddress() {
        return address!=null?address:"";
    }

    public String getOptin() {
        return optin!=null?optin:"";
    }

    public String getUser_type() {
        return user_type!=null?user_type:"";
    }

    public String getFb_id() {
        return fb_id!=null?fb_id:"";
    }

    public String getGg_id() {
        return gg_id!=null?gg_id:"";
    }

    public String getBeta_tester() {
        return beta_tester!=null?beta_tester:"";
    }

    public String getStatus_tfa() {
        return status_tfa!=null?status_tfa:"";
    }

    public String getPhone_tfa() {
        return phone_tfa!=null?phone_tfa:"";
    }

    public String getCountry() {
        return country!=null?country:"";
    }

    public String getState() {
        return state!=null?state:"";
    }

    public String getCity() {
        return city!=null?city:"";
    }

    public String getZip() {
        return zip!=null?zip:"";
    }

    public String getOrganization_name() {
        return organization_name!=null?organization_name:"";
    }

    public String getProfile_image_full() {
        return profile_image_full!=null?profile_image_full:"";
    }

    public String getProfile_image_large() {
        return profile_image_large!=null?profile_image_large:"";
    }

    public String getProfile_image_medium() {
        return profile_image_medium!=null?profile_image_medium:"";
    }

    public String getProfile_image_small() {
        return profile_image_small!=null?profile_image_small:"";
    }

    public String getUser_registered() {
        return user_registered;
    }

    public String getLogin_by(){return login_by;}
    public  void setLogin_by(String login_by){
        this.login_by= login_by;
        UserPref.getInstance().setLoginBy(login_by);
    }
    public String getUserRegisteredType(){return registration_type;}
    public String getSource(){return source;}
    public String getDevices_count(){return devices_count;}
    public String getDesktop_registered(){return desktop_registered;}
    public String getIos_registered(){return ios_registered;}
    public String getAndroid_registered(){return android_registered;}
}
