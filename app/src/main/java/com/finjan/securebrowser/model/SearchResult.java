package com.finjan.securebrowser.model;

import com.google.gson.annotations.SerializedName;
/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 */


public class SearchResult {
//
//    @SerializedName("value")
//    public value[] value;

    // Send down through a separate request and set later.
    public SafetyResult safetyResult;

//    public static class value {

        @SerializedName("id")
        public String id;

        @SerializedName("name")
        public String name;

        @SerializedName("snippet")
        public String snippet;

        @SerializedName("displayUrl")
        public String displayUrl;

        @SerializedName("url")
        public String url;
//    }
}