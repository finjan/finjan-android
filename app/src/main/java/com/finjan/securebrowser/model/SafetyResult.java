package com.finjan.securebrowser.model;

import android.text.TextUtils;
import android.util.Log;

import com.finjan.securebrowser.Constants;
import com.finjan.securebrowser.R;
import com.finjan.securebrowser.Utils;
import com.finjan.securebrowser.helpers.logger.Logger;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.neovisionaries.i18n.CountryCode;

import java.util.Date;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 */


public class SafetyResult implements Cloneable {

    public int title;
    public int verboseResponse;
    public int color;
    public int buttonString;
    public int buttonImage;


    public void setUrl(String url) {
        this.url = url;
    }

    @SerializedName("categories")
    public String[] categories;

    @SerializedName("country")
    public String country;

    @SerializedName("rating")
    public String rating;

    @SerializedName("scans")
    public JsonObject scans;


    @SerializedName("url")
    public String url;

    @SerializedName("last_seen")
    public String last_seen;

    @SerializedName("total")
    public String total;

    @SerializedName("positives")
    public String positives;

    @SerializedName("status")
    public String status;

    @SerializedName("redirected_from")
    public int redirectedFrom;


    public void setAttributes() {
        if (rating.equals("dangerous"))
        {
            title = R.string.bad_site_title;
            color = R.color.red_1;
            verboseResponse = R.string.bad_site_verbose;
            buttonString = R.string.bad_site_button;
            buttonImage = R.drawable.scan_status_big_danger;
        }
        else if (rating.equals("suspicious"))
        {
            title = R.string.suspicious_site_title;
            color = R.color.yellow_1;
            verboseResponse = R.string.suspicious_site_verbose;
            buttonString = R.string.suspicious_site_button;
            buttonImage = R.drawable.scan_status_big_caution;
        }
        else if (rating.equals("safe"))
        {
            title = R.string.good_site_title;
            color = R.color.green_colour_1;
            verboseResponse = R.string.good_site_verbose;
            buttonString = R.string.good_site_button;
            buttonImage = R.drawable.scan_status_big_safe;
        }
        else if (rating.equals("invalid"))
        {
            Logger.logD("Invalid", "Invalid");
        }
    }

    public String getUrl() {
        return url;
    }

    public JsonObject getScans() {
        return scans;
    }

    public String getLastSeen() {
        return last_seen;
    }

    public String getTotal() {
        return total;
    }

    public String getPositive() {
        return positives;
    }

    public String getRating() {
        return rating;
    }

    public Integer getTitle() {
        return title;
    }

    public Integer getColor() {
        return color;
    }

    public Integer getVerboseResponse() {
        return verboseResponse;
    }

    public Integer getButtonString() {
        return buttonString;
    }

    public Integer getButtonImage() {
        return buttonImage;
    }

    public String getCountryName() {
        if (country != null) {
            try {
                CountryCode cc = CountryCode.getByCode(country);
                return cc!=null?cc.getName():"";
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
        return "Unknown Country";
    }

    public String getCountryCode() {
        return country;
    }

    public String getFlagName() {
        if (country == null) {
            return "no_country";  // for no_country.png
        } else if (country == "DO") {
            return "dom";  // `do` is reserved word so do.png fails
        } else {
            return country.toLowerCase();
        }
    }

    @Override
    public SafetyResult clone() throws CloneNotSupportedException {
        try {
            return (SafetyResult) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
    public SafetyResult copyMe(){
        SafetyResult newSafetyResult=new SafetyResult();
        newSafetyResult.setUrl(this.getUrl());
        newSafetyResult.rating=this.rating;
        newSafetyResult.buttonImage=this.buttonImage;
        newSafetyResult.buttonString=this.buttonString;
        newSafetyResult.categories=this.categories;
        newSafetyResult.color=this.color;
        newSafetyResult.country=this.country;
        newSafetyResult.last_seen=this.last_seen;
        newSafetyResult.positives=this.positives;
        newSafetyResult.redirectedFrom=this.redirectedFrom;
        newSafetyResult.scans=this.scans;
        newSafetyResult.status=this.status;
        newSafetyResult.title=this.title;
        newSafetyResult.total=this.total;
        newSafetyResult.verboseResponse=this.verboseResponse;
        return newSafetyResult;
    }
}
