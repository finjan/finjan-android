package com.finjan.securebrowser.model;

import java.util.ArrayList;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class TrackerFoundModel implements Cloneable {
    private String trackerScript = "";
    private String trackerName = "";
    private String trackerType = "";
    private String checkedString = "";
    private ArrayList<String> detailData = new ArrayList<>();
    private ArrayList<TrackerCount> trackerCounts = new ArrayList<>();


    private ArrayList<TrackerListModel> trackerURL = new ArrayList<>();

    public TrackerFoundModel() {
    }

    public TrackerFoundModel(String trackerName, String trackerType, String trackerScript) {
        this.trackerName = trackerName;
        this.trackerScript = trackerScript;
        this.trackerType = trackerType;
    }

    public ArrayList<String> getDetailData() {
        return detailData;
    }

    public void setDetailData(ArrayList<String> detailData) {
        this.detailData = detailData;
    }

    public ArrayList<TrackerListModel> getTrackerURL() {
        return trackerURL;
    }

    public void setTrackerURL(ArrayList<TrackerListModel> trackerURL) {
        this.trackerURL = trackerURL;
    }

    public String getTrackerScript() {
        return trackerScript;
    }

    public void setTrackerScript(String trackerScript) {
        this.trackerScript = trackerScript;
    }

    public String getTrackerName() {
        return trackerName;
    }

    public void setTrackerName(String trackerName) {
        this.trackerName = trackerName;
    }

    public String getTrackerType() {
        return trackerType;
    }

    public void setTrackerType(String trackerType) {
        this.trackerType = trackerType;
    }

    public ArrayList<TrackerCount> getTrackerCounts() {
        return trackerCounts;
    }

    public void setTrackerCounts(ArrayList<TrackerCount> trackerCounts) {
        this.trackerCounts = trackerCounts;
    }

    public String getCheckedString() {
        return checkedString;
    }

    public void setCheckedString(String checkedString) {
        this.checkedString = checkedString;
    }

    public TrackerFoundModel clone() {
        try {
            return (TrackerFoundModel) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}
