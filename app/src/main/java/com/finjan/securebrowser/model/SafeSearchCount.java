package com.finjan.securebrowser.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC.
 */

public class SafeSearchCount {

    private String count;



    public  SafeSearchCount(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        if (jsonObject != null && jsonObject.has("creditdetails")) {
            JSONObject categoryJson = jsonObject.getJSONObject("creditdetails");
            if (categoryJson != null && categoryJson.has("avaliable_credits")) {
                    count=categoryJson.getString("avaliable_credits");
            }

        }
    }

    public String getCount() {
        return count;
    }
}
