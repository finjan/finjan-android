package com.finjan.securebrowser.model;

import java.util.ArrayList;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 29/05/17.
 *
 */

public class InAppPurchase {

    private boolean enabled,sandboxtesting;
    private int freeprivacyscan,notificationinterval;
    private  String creditsupdateurl;
    private ArrayList<String > products;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isSandboxtesting() {
        return sandboxtesting;
    }

    public void setSandboxtesting(boolean sandboxtesting) {
        this.sandboxtesting = sandboxtesting;
    }

    public int getFreeprivacyscan() {
        return freeprivacyscan;
    }

    public void setFreeprivacyscan(int freeprivacyscan) {
        this.freeprivacyscan = freeprivacyscan;
    }

    public int getNotificationinterval() {
        return notificationinterval;
    }

    public void setNotificationinterval(int notificationinterval) {
        this.notificationinterval = notificationinterval;
    }

    public String getCreditsupdateurl() {
        return creditsupdateurl;
    }

    public void setCreditsupdateurl(String creditsupdateurl) {
        this.creditsupdateurl = creditsupdateurl;
    }

    public ArrayList<String> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<String> products) {
        this.products = products;
    }
}
