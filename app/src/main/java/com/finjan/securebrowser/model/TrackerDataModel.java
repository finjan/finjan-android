package com.finjan.securebrowser.model;

import java.util.ArrayList;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class TrackerDataModel {
    private String trackerType="";
    private String trackerName="";
    private String trackerUrl="";
    private ArrayList<String> trackerDataList=new ArrayList<>();

    public String getTrackerName() {
        return trackerName;
    }

    public void setTrackerName(String trackerName) {
        this.trackerName = trackerName;
    }

    public String getTrackerUrl() {
        return trackerUrl;
    }

    public void setTrackerUrl(String trackerUrl) {
        this.trackerUrl = trackerUrl;
    }

    public ArrayList<String> getTrackerDataList() {
        return trackerDataList;
    }

    public void setTrackerDataList(ArrayList<String> trackerDataList) {
        this.trackerDataList = trackerDataList;
    }

    public String getTrackerType() {
        return trackerType;
    }

    public void setTrackerType(String trackerType) {
        this.trackerType = trackerType;
    }
}
