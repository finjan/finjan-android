package com.finjan.securebrowser.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class SkuModel implements Parcelable{

    public SkuModel(String sku, String price, String purchaseTime, String purchaseState) {
        this.sku = sku;
        this.price = price;
        this.purchaseTime = purchaseTime;
        this.purchaseState = purchaseState;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public void setPurchaseState(String purchaseState) {
        this.purchaseState = purchaseState;
    }

    public String getSku() {
        return sku;
    }

    public String getPrice() {
        return price;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public String getPurchaseState() {
        return purchaseState;
    }

    private String sku, price, purchaseTime, purchaseState;

    public SkuModel(Parcel in){
        String[] data=new String[4];
        in.readStringArray(data);
        sku=data[0];
        price=data[1];
        purchaseTime=data[2];
        purchaseState=data[3];
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{sku,price,purchaseTime,purchaseState});
    }
    public static final Parcelable.Creator CREATOR=new Parcelable.Creator<SkuModel>(){
        @Override
        public SkuModel[] newArray(int size) {
            return new SkuModel[size];
        }

        @Override
        public SkuModel createFromParcel(Parcel source) {
            return new SkuModel(source);
        }
    };
}
