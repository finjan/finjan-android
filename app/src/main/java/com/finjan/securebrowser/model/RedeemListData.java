package com.finjan.securebrowser.model;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/*
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 30/05/17.
 *
 */

public class RedeemListData {

    public RedeemListData(JSONObject jsonObject1){
        try{
            id = jsonObject1.getString("id");
            type = jsonObject1.getString("type");
            attributes = getAttribute(jsonObject1.getJSONObject("attributes"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static ArrayList<RedeemListData> parseRedeemList(JSONObject jsonObject){
        ArrayList<RedeemListData> redeemListDataArrayList = new ArrayList<>();
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i=0;i < jsonArray.length();i++){
                redeemListDataArrayList.add(new RedeemListData((JSONObject) jsonArray.get(i)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return redeemListDataArrayList;
    }

  private  String id, type;
  private Attributes attributes;

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public Attributes getAttribute(JSONObject jsonAtribute){
        Attributes attribute = new Attributes();
        try{
            attribute.setTitle(jsonAtribute.optString("title"));
            attribute.setPopup(jsonAtribute.optString("popup"));
            attribute.setPopup_begin_title(jsonAtribute.optString("popup_begin_title"));
            attribute.setPopup_begin_text(jsonAtribute.optString("popup_begin_text"));
            attribute.setPopup_end_title(jsonAtribute.optString("popup_end_title"));
            attribute.setPopup_end_text(jsonAtribute.optString("popup_end_text"));
            attribute.setIap_button_text(jsonAtribute.optString("iap_button_text"));
            attribute.setTitle_2(jsonAtribute.optString("title_2"));
            attribute.setPopup_button_text(jsonAtribute.optString("popup_button_text"));
            attribute.setPromo_type(jsonAtribute.optString("promo_type"));
            attribute.setStart_date(jsonAtribute.optString("start_date"));
            attribute.setEnd_date(jsonAtribute.optString("end_date"));
            attribute.setDays(jsonAtribute.optString("days"));
            attribute.setRedeem_code(jsonAtribute.optString("redeem_code"));
            attribute.setLocalytics_event_no(jsonAtribute.optString("localytics_event_no"));
            attribute.setRedemptions(jsonAtribute.optString("max_redemptions"));
            attribute.setProfile_event_type(jsonAtribute.optString("profile_event_type"));
            if ("".equalsIgnoreCase(jsonAtribute.getString("iap_discount")))
            {
                attribute.setIapDiscount(null);
            }
            else {
                JSONObject jsonIapDiscount = jsonAtribute.getJSONObject("iap_discount");
                IapDiscount iapDiscount = new IapDiscount();
                iapDiscount.setName(jsonIapDiscount.optString("name"));
                iapDiscount.setIos(jsonIapDiscount.optString("ios"));
                JSONArray arr = new JSONArray(jsonIapDiscount.optString("android"));
                ArrayList<String> stringArrayList = new ArrayList<>();
                for (int i = 0; i < arr.length(); i++) { // Walk through the Array.
                    String obj = arr.getString(i);
                    stringArrayList.add(obj);
                }
                iapDiscount.setAndroid(stringArrayList);
                attribute.setIapDiscount(iapDiscount);
            }
        }catch (Exception e){e.printStackTrace();}
        return attribute;
    }
    public class Attributes{
        private String title,popup,popup_begin_title,popup_begin_text,popup_end_text,
                popup_end_title,popup_button_text,promo_type,start_date,end_date,
                days,redeem_code,localytics_event_no,redemptions,
                iap_button_text,title_2,profile_event_type;

        public String getPopup_end_text() {
            return popup_end_text;
        }

        public void setPopup_end_text(String popup_end_text) {
            this.popup_end_text = popup_end_text;
        }

        public void setIap_button_text(String iap_button_text) {
            this.iap_button_text = iap_button_text;
        }

        public String getIap_button_text() {
            return iap_button_text;
        }

        public void setTitle_2(String title_2) {
            this.title_2 = title_2;
        }

        public String getTitle_2() {
            return title_2;
        }

        public String getRedemptions() {
            return redemptions;
        }

        public void setRedemptions(String redemptions) {
            this.redemptions = redemptions;
        }

        private IapDiscount iapDiscount;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPopup() {
            return popup;
        }

        public void setPopup(String popup) {
            this.popup = popup;
        }

        public String getPopup_begin_title() {
            return popup_begin_title;
        }

        public void setPopup_begin_title(String popup_begin_title) {
            this.popup_begin_title = popup_begin_title;
        }

        public String getPopup_begin_text() {
            return popup_begin_text;
        }

        public void setPopup_begin_text(String popup_begin_text) {
            this.popup_begin_text = popup_begin_text;
        }

        public String getPopup_end_title() {
            return popup_end_title;
        }

        public void setPopup_end_title(String popup_end_title) {
            this.popup_end_title = popup_end_title;
        }

        public String getPopup_button_text() {
            return popup_button_text;
        }

        public void setPopup_button_text(String popup_button_text) {
            this.popup_button_text = popup_button_text;
        }

        public String getPromo_type() {
            return promo_type;
        }

        public void setPromo_type(String promo_type) {
            this.promo_type = promo_type;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getEnd_date() {
            return end_date;
        }

        public void setEnd_date(String end_date) {
            this.end_date = end_date;
        }

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }

        public String getRedeem_code() {
            return redeem_code;
        }

        public void setRedeem_code(String redeem_code) {
            this.redeem_code = redeem_code;
        }

        public String getLocalytics_event_no() {
            return localytics_event_no;
        }

        public void setLocalytics_event_no(String localytics_event_no) {
            this.localytics_event_no = localytics_event_no;
        }

        public IapDiscount getIapDiscount() {
            return iapDiscount;
        }

        public void setIapDiscount(IapDiscount iapDiscount) {
            this.iapDiscount = iapDiscount;
        }


        public String getProfile_event_type() {
            return profile_event_type;
        }

        public void setProfile_event_type(String profile_event_type) {
            this.profile_event_type = profile_event_type;
        }
    }

}

