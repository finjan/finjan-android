package com.finjan.securebrowser.model;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class TrackerListModel {

    private String trackerName;
    private String trackerURl;
    private boolean isCheck;

    public TrackerListModel(){}
    public TrackerListModel(String name,String url){
        this.trackerName=name;
        this.trackerURl=url;
    }
    public TrackerListModel(String name,String url,boolean isCheck){
        this.trackerName=name;
        this.trackerURl=url;
        this.isCheck=isCheck;
    }

    public String getTrackerName() {
        return trackerName;
    }

    public String getTrackerURl() {
        return trackerURl;
    }

    public boolean isCheck() {
        return isCheck;
    }
}
