package com.finjan.securebrowser.model;

import com.google.gson.annotations.SerializedName;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class BingSearchResponse {

//    @SerializedName("_type")
//    public ResultsContent type;
    @SerializedName("webPages")
    public ResultsValues webPages;

//    public static class ResultsContent {


//        @SerializedName("__next")
//        public String __next;

            public static class ResultsValues {
                @SerializedName("value")
                public SearchResult[] value;

//        @SerializedName("__next")
//        public String __next;
//}
    }


}
