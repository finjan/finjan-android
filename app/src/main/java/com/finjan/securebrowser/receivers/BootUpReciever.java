package com.finjan.securebrowser.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.service.NotifyUnSecureNetwork;

/**
 * Created by anurag on 31/10/17.
 */

public class BootUpReciever extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    NotifyUnSecureNetwork.startServiceWhenRequired(context);
                }
            }, 2000);
        }
    }
}
