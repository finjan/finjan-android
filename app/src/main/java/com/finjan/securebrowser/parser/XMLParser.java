package com.finjan.securebrowser.parser;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Stack;
import java.util.TimeZone;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class XMLParser extends DefaultHandler {

	StringBuilder sb = null; // new StringBuilder();

	Stack<PList> stack = new Stack<PList>();

	// previous element:

	// PList current_container; // current container is the PListDict only...
	public PList current_element; // always starts with string.... or something
									// like that...

	public XMLParser() {
		current_element = new PList("root", (Attributes) null);
	}

	@Override
	public void startElement(String uri, String localName, String qName,
							 Attributes attributes) {
		// System.out.println("Start:" + qName); // + ":" +
		// sb.toString().trim());

		// if (current_element!=null)
		stack.push(current_element);
		// stack.push(current_element);
		current_element = new PList(qName, attributes);
		// if (!stack.empty()){
		stack.peek().add(current_element);
		// }

	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		// if (sb==null)
		// System.out.println("End:" + qName); //+ ":" + sb.toString().trim());
		if (sb != null) {
			current_element.data = sb.toString().trim();
//			if (sb.toString().trim().equalsIgnoreCase("Food Stand"))
//				current_element.data = "Food Stands";
			sb = null;
		}
		// if (!stack.empty()){
		current_element = stack.pop();
		// }
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		if (sb == null)
			sb = new StringBuilder();
		sb.append(ch, start, length);
	}

	@Override
	public InputSource resolveEntity(String pid, String sid) {
		// System.out.println("resolveEntity:" + pid + ", "+ sid);
		return new InputSource(new ByteArrayInputStream(new byte[0]));
		// return null;

	}

	/*
	 * protected void set_add(Object V){ PList current=stack.peek(); if (current
	 * instanceof PListArray){ ((PListArray)current).add(V); }else if (current
	 * instanceof PListDict){ ((PListDict)current).put(last_key, V);
	 * last_key=null; } }
	 */
	public void parse(InputStream src) throws Exception {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setValidating(false);
		spf.setNamespaceAware(false);
		SAXParser sp = spf.newSAXParser();
		// PListParserHandler result=new PListParserHandler();
		sp.parse(src, this);
 	}

	public static int stringToColor(String V) {
		try {
			String[] c = V.split(",");
			return 0xFF000000 | (Integer.parseInt(c[0]) << 16)
					| (Integer.parseInt(c[1]) << 8) | (Integer.parseInt(c[2]));
		} catch (Exception e) {
			return 0xFFFFFFFF;
		}
	}

	static public Date gameDate(PList dict) {

		try {
			// System.out.println(dict.getData("date") +
			// "  "+dict.getData("time"));

			String date = dict.getData("date");
			if (date != null) {
				for (int i = 0; i < date.length(); i++) {
					if (Character.isDigit(date.charAt(i))) {
						date = date.substring(i);
						break;
					}
				}
			} else
				return null;
			String time = dict.getData("time");

			String[] ds = date.split("/");

			{
				StringBuilder sb = new StringBuilder();
				Character prev_c = time.charAt(0);
				sb.append(prev_c);
				for (int i = 1; i < time.length(); i++) {
					Character c = time.charAt(i);
					if (Character.isDigit(prev_c)
							&& Character.isJavaIdentifierStart(c)) {
						sb.append(" ");
					}
					sb.append(c);
					prev_c = c;
				}
				time = sb.toString();
			}
			String[] ts = time.split("[ |:]");

			// System.out.println("................................");
			// for (int i=0; i<ds.length; i++){
			// // System.out.println(ds[i]);
			// }
			// for (int i=0; i<ts.length; i++){
			// // System.out.println(ts[i]);
			// }

			if (ds.length < 3)
				return null;
			if (ts.length < 3) {
				ts = new String[] { "12", "00", "am" };
			}
			Date result = null;
			Calendar cal = null;
			if (ts.length == 3) {
				cal = Calendar.getInstance(TimeZone
						.getTimeZone("America/Los_Angeles"));
			} else if (ts.length == 4) {
				if (ts[3].equals("ET")) {
					cal = Calendar.getInstance(TimeZone
							.getTimeZone("America/New_York"));
				} else if (ts[3].equals("CT")) {
					cal = Calendar.getInstance(TimeZone
							.getTimeZone("America/Chicago"));
				} else if (ts[3].equals("MT")) {
					cal = Calendar.getInstance(TimeZone
							.getTimeZone("America/Denver"));
				} else if (ts[3].equals("HT")) {
					cal = Calendar.getInstance(TimeZone
							.getTimeZone("Pacific/Honolulu"));
				} else {
					cal = Calendar.getInstance(TimeZone
							.getTimeZone("America/Los_Angeles"));
				}
			}

			ds[2] = ds[2].trim();
			if (ds[2].length() > 2) {
				cal.set(Calendar.YEAR, Integer.parseInt(ds[2]));
			} else {
				cal.set(Calendar.YEAR, 2000 + Integer.parseInt(ds[2]));
			}
			cal.set(Calendar.MONTH, Integer.parseInt(ds[0]) - 1);
			cal.set(Calendar.DATE, Integer.parseInt(ds[1]));

			cal.set(Calendar.AM_PM, ts[2].equals("am") ? Calendar.AM
					: Calendar.PM);
			cal.set(Calendar.HOUR, Integer.parseInt(ts[0]));
			cal.set(Calendar.HOUR_OF_DAY,
					ts[2].equals("am") ? Integer.parseInt(ts[0]) : Integer
							.parseInt(ts[0]) == 12 ? 12 : Integer
							.parseInt(ts[0]) + 12);
			cal.set(Calendar.MINUTE, Integer.parseInt(ts[1]));
			cal.set(Calendar.SECOND, 0);

			result = cal.getTime();
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

}
