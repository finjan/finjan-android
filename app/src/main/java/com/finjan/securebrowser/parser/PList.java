package com.finjan.securebrowser.parser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.xml.sax.Attributes;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


// it is also an array and map of all the internal fields as well...
// this is fundamental node for the rese
public class PList implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1803789952277586238L;

	public final static int SEP_CHAR = '.';

	public String qname;
	public String data = "";
	// public Attributes attrs;

	public HashMap<String, String> attrs;

	public ArrayList<PList> array;
	public TreeMap<String, PList> map; // of the the childs inside the
	// thing...similar elements are
	// represented as as an array with
	// inherited attributes from the upper
	// level...

	/************ Fields added for section pin *************/
	public String pin_section;
	public String pin_row;
	public String pin_seat;

	public String getPin_section() {
		return pin_section;
	}

	public String getPin_row() {
		return pin_row;
	}

	public void setPin_row(String pin_row) {
		this.pin_row = pin_row;
	}

	public String getPin_seat() {
		return pin_seat;
	}

	public void setPin_seat(String pin_seat) {
		this.pin_seat = pin_seat;
	}

	public void setPin_section(String pin_section) {
		this.pin_section = pin_section;
	}

	@Override
	public String toString() {

		return "QName : " + qname + " : data : " + data;
	}

	public void dump(StringBuilder sb) {
		sb.append('<').append(qname).append('>');
		if (array != null) {
			for (PList e : array) {
				e.dump(sb);
			}
		} else if (data != null) {
			sb.append(data);
		} else {
		}
		sb.append("</").append(qname).append(">\n");
	}

	public PList(String QName, Attributes Attrs) {
		qname = QName;
		// data="";

		attrs = new HashMap<String, String>();
		if (Attrs != null) {
			for (int i = 0; i < Attrs.getLength(); i++) {
				// System.out.println(Attrs.getQName(i)+":"+Attrs.getValue(i));
				attrs.put(Attrs.getQName(i), Attrs.getValue(i));
			}
		}
	}

	public PList(String QName, String Data) {
		qname = QName;
		data = Data;
	}

	protected PList(PList from) {
		qname = from.qname;
		data = from.data;
		attrs = from.attrs;
	}

	/*
	 * public PList(PList from, ArrayList<PList> fromArray){ qname=from.qname;
	 * data=from.data; attrs=from.attrs; array=fromArray; }
	 */
	public void add(PList lst) {
		if (array == null)
			array = new ArrayList<PList>(1);
		array.add(lst);
		// map is updated later on... i'll figure it out...eventually...
	}

	/*
	 * public int getArraySize(){ if (array!=null) return array.size(); else
	 * return 0; }
	 */

	private void create_map() {
		TreeMap<String, Object> m2 = new TreeMap<String, Object>();
		map = new TreeMap<String, PList>();
		if (array != null) {
			for (PList p : array) {
				Object o = m2.get(p.qname);
				if (o instanceof ArrayList) {
					((ArrayList<PList>) o).add(p);
				} else if (o instanceof PList) {
					ArrayList<PList> arr = new ArrayList<PList>(2);
					arr.add((PList) o);
					arr.add(p);
					m2.put(p.qname, arr);
				} else {
					m2.put(p.qname, p);
				}
			}
			// here it should be a way to describe is it a single element or
			// many of them....
			// and it is tricky....
			for (Map.Entry<String, Object> me : m2.entrySet()) {
				Object v = me.getValue();
				if (v instanceof ArrayList) {
					map.put(me.getKey(), new PListArray(this,
							(ArrayList<PList>) v));
				} else {
					map.put(me.getKey(), (PList) v);
				}
			}
		}
	}

	// it can create something interesting of course.... but it should be very
	// much different....

	// access using dictionary of the elements:
	public PList get(String path) { // throws Exception {
		return get(path.split("\\.", 0), 0);
	}

	public String getData(String path) { // throws Exception {
		/*
		 * String[] s=path.split("\\.", 0);
		 *
		 * System.out.print("--"+Integer.toString(s.length)+ "--"); for (int
		 * i=0; i<s.length; i++){ System.out.print(s[i]); if (i<s.length-1){
		 * System.out.print("."); } } System.out.println(); //"-----------");
		 */
		PList pl = get(path.split("\\.", 0), 0);
		if (pl != null)
			return pl.data;
		else
			return null;

	}

	public Map<String, PList> getMap() {
		if (map == null)
			create_map();
		return map;
	}

	public PList getAt(int Index) {
		if (Index == 0) {
			return this;
		} else {
			return null;
		}
	}

	public int size() {
		return 1;
	}

	/*
	 * if ("".equals(path)){ return this; }else{ int k=path.indexOf(SEP_CHAR);
	 * if (k<0){ if (Character.isDigit(path.charAt(0))){ // access to the array:
	 * return array.get(Integer.parseInt(path)); }else{ if (map==null)
	 * create_map(); return map.get(path); } }else{ // split it into the array
	 * and return as array i guess... String l=path.substring(0, k); String
	 * r=path.substring(k+1); if (Character.isDigit(path.charAt(0))){ return
	 * array.get(Integer.parseInt(l)).get(r); }else{ // this is a map reference:
	 * if (map==null) create_map(); return map.get(l).get(r); } } } }
	 */
	public PList get(String[] path, int Index) { // throws Exception {
		if (Index >= path.length) {
			return this;
		} else {
			if (Index == path.length - 1) {
				// if (path[Index].charAt(0)=='0'){
				// if (Character.isDigit(path[Index].charAt(0))){
				// return array.get(Integer.parseInt(path[Index]));
				// }else{
				if (map == null)
					create_map();
				return map.get(path[Index]);
				// }
			} else {
				/*
				 * if (Character.isDigit(path[Index].charAt(0))){ return
				 * array.get(Integer.parseInt(path[Index])); }else{
				 */
				if (map == null)
					create_map();
				PList pl = map.get(path[Index]);
				if (pl != null) {
					return pl.get(path, Index + 1);
				} else {
					return null;
				}
				// }
			}
		}
	}

	public void set(String path, String V) { // throws Exception {
		set(path.split("\\.", 0), 0, V);
		// throw new Exception("cannot set itself");
	}

	public void set(String path[], int Index, String V) { // throws Exception {
		if (Index >= path.length) {
			data = V;
		} else {
			if (Index == path.length - 1) {
				/*
				 * if (Character.isDigit(path[Index].charAt(0))){
				 * array.get(Integer.parseInt(path[Index])).data=V; }else{
				 */
				if (map == null)
					create_map();
				map.get(path[Index]).data = V;
				// / }
			} else {
				/*
				 * if (Character.isDigit(path[Index].charAt(0))){
				 * array.get(Integer.parseInt(path[Index])).set(path, Index+1,
				 * V); }else{
				 */
				if (map == null)
					create_map();
				map.get(path[Index]).set(path, Index + 1, V);
				// }
			}
		}
	}

	// public String getType();
	// public void setType(String T);

	// public void setAttributes(Attributes attributes);

}