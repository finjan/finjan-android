package com.finjan.securebrowser.parser;


import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class PListParser extends DefaultHandler {

	StringBuilder sb = null; // new StringBuilder();

	Stack<PList> stack = new Stack<PList>();
	String last_key = null;

	private boolean isPushDict;
	// public PList root;

	// previous element:

	// PList current_container; // current container is the PListDict only...
	public PList current_element; // always starts with string.... or something
									// like that...

	private boolean isApplicationID;

	private boolean isClientID;

	private boolean isChannelName;

	public PListParser() {
		// current_element=new PList("root", null);
//		AppLog.e("PList Parser", "PList Parser");
	}

	@Override
	public void startElement(String uri, String localName, String qName,
							 Attributes attributes) {
		// System.out.println("Start:" + qName + ":" + sb.toString().trim());

		if (qName.equals("plist")) {
			// current_element=new PList("plist", (Attributes)null);
		} else if (qName.equals("dict")) {
			if (current_element != null)
				stack.push(current_element);
			current_element = new PList(qName, attributes);
			// stack.push(current_element);
			if (last_key != null) {
				current_element.qname = last_key;
				last_key = null;
			}
			if (!stack.empty()) {
				stack.peek().add(current_element);
			}
		} else if (qName.equals("array")) {
			if (current_element != null)
				stack.push(current_element);
			current_element = new PListArray(qName, attributes);
			// stack.push(current_element);
			if (last_key != null) {
				current_element.qname = last_key;
				last_key = null;
			}
			if (!stack.empty()) {
				stack.peek().add(current_element);
			}
		} else if (qName.equals("playerstab")) {
			if (current_element != null)
				stack.push(current_element);
			current_element = new PListArray(qName, attributes);
			// stack.push(current_element);
			// last_key = qName;
			if (last_key != null) {
				current_element.qname = last_key;
				last_key = null;
			}
			// if (!stack.empty()){
			stack.push(current_element);
			// }
		} else if (qName.equals("player")) {
			// if (current_element!=null) stack.peek().add(current_element);
			current_element = new PListArray(qName, attributes);
			// stack.push(current_element);
			// last_key = "player";
			if (last_key != null) {
				current_element.qname = last_key;
				last_key = null;
			}
			if (!stack.empty()) {
				stack.peek().add(current_element);
			}
		}

		else if (qName.equals("pushdetails")) {
			isPushDict = true;
			isApplicationID = false;
			isClientID = false;
			isChannelName = false;
		}

		else if (qName.equals("awardstab")) {
			if (current_element != null)
				stack.push(current_element);
			current_element = new PListArray(qName, attributes);
			// stack.push(current_element);
			// last_key = qName;
			if (last_key != null) {
				current_element.qname = last_key;
				last_key = null;
			}
			// if (!stack.empty()){
			stack.push(current_element);
			// }
		} else if (qName.equals("awardtrophy")) {
			// if (current_element!=null) stack.peek().add(current_element);
			current_element = new PListArray(qName, attributes);
			// stack.push(current_element);
			// last_key = "player";
			if (last_key != null) {
				current_element.qname = last_key;
				last_key = null;
			}
			if (!stack.empty()) {
				stack.peek().add(current_element);
			}
		}

		else if (qName.equals("historictab")) {
			if (current_element != null)
				stack.push(current_element);
			current_element = new PListArray(qName, attributes);
			// stack.push(current_element);
			// last_key = qName;
			if (last_key != null) {
				current_element.qname = last_key;
				last_key = null;
			}
			// if (!stack.empty()){
			stack.push(current_element);
			// }
		} else if (qName.equals("historic")) {
			// if (current_element!=null) stack.peek().add(current_element);
			current_element = new PListArray(qName, attributes);
			// stack.push(current_element);
			// last_key = "player";
			if (last_key != null) {
				current_element.qname = last_key;
				last_key = null;
			}
			if (!stack.empty()) {
				stack.peek().add(current_element);
			}
		} else if (isPushDict && qName.equals("applicationid")
				&& qName.equals("clientkey") && qName.equals("channelname")) {
			if (qName.equals("applicationid")) {
				isApplicationID = true;
				isClientID = false;
				isChannelName = false;
			} else if (qName.equals("clientkey")) {
				isClientID = true;
				isApplicationID = false;
				isChannelName = false;
			} else {
				isChannelName = true;
				isApplicationID = false;
				isClientID = false;
			}
		}

		else {

			sb = new StringBuilder();
		}

		/*
		 * }else if (qName.equals("dict") || qName.equals("array")){ if
		 * (current_element!=null) stack.push(current_element);
		 * current_element=new PList(qName, attributes); //
		 * stack.push(current_element); if (last_key!=null){
		 * current_element.qname=last_key; last_key=null; } if (!stack.empty()){
		 * stack.peek().add(current_element); } }else {
		 */

		/*
		 * 
		 * stack.push(current_element); current_element=new PList(qName,
		 * attributes); stack.peek().add(current_element);
		 */

	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		// if (sb==null)
		// System.out.println("End:" + qName+ ":" + sb.toString().trim());
		// current_element=stack.pop();
		if (qName.equals("dict") || qName.equals("array")
				|| qName.equals("playerstab") || qName.equals("awardstab")
				|| qName.equals("historictab")) {
			if (!stack.empty()) {
				current_element = stack.pop();
			}
		} else if (qName.equals("plist")) {
			// do nothing here really...
		} else if (qName.equals("pushdetails")) {
			isPushDict = false;
		} else if (isPushDict) {
			if (isApplicationID) {
				isApplicationID = false;
				if (sb != null) {
					String v = sb.toString();	//Manish removed trim()
					sb = null;
//					ApplicationClass.applicaitonKey = v;
				}
			} else if (isChannelName) {
				isChannelName = false;
				if (sb != null) {
					String v = sb.toString();	//Manish removed trim()
					sb = null;
//					ApplicationClass.pushChannel = v;
				}
			} else if (isClientID) {
				isClientID = false;
				if (sb != null) {
					String v = sb.toString();	//Manish removed trim()
					sb = null;
//					ApplicationClass.clientKey = v;
				}
			}

		} else {
			// System.out.println("End:" + qName); // ":" +
			// sb.toString().trim());
			String v = sb.toString();	//Manish removed trim()
			sb = null;
			if (qName.equals("key")) {
				/*
				 * if(v.contains("|")) { v = v.substring(0,v.indexOf("|")); }
				 */
				last_key = v;
			} else if (qName.equals("player")) {
				last_key = "player";
			} else if (qName.equals("playerstab")) {
				last_key = "playerstab";
			} else if (qName.equals("awardtrophy")) {
				last_key = "awardtrophy";
			} else if (qName.equals("awardstab")) {
				last_key = "awardstab";
			} else if (qName.equals("historic")) {
				last_key = "historic";
			} else if (qName.equals("historictab")) {
				last_key = "historictab";
			} else {

				if (last_key != null) {
					current_element.add(new PList(last_key, v));
					last_key = null;
				} else {
					current_element.add(new PList(qName, v));
				}
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		if (sb == null)
			sb = new StringBuilder();
		sb.append(ch, start, length);
	}

	@Override
	public InputSource resolveEntity(String pid, String sid) {
		// System.out.println("resolveEntity:" + pid + ", "+ sid);
		return new InputSource(new ByteArrayInputStream(new byte[0]));
		// return null;

	}

	/*
	 * protected void set_add(Object V){ PList current=stack.peek(); if (current
	 * instanceof PListArray){ ((PListArray)current).add(V); }else if (current
	 * instanceof PListDict){ ((PListDict)current).put(last_key, V);
	 * last_key=null; } }
	 */
	public void parse(InputStream src) throws Exception {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setValidating(false);
		spf.setNamespaceAware(false);
		SAXParser sp = spf.newSAXParser();
		// PListParserHandler result=new PListParserHandler();
		sp.parse(src, this);
	}

	public static int stringToColor(String V) {
		try {
			String[] c = V.split(",");
			return 0xFF000000 | (Integer.parseInt(c[0]) << 16)
					| (Integer.parseInt(c[1]) << 8) | (Integer.parseInt(c[2]));
		} catch (Exception e) {
			return 0xFFFFFFFF;
		}
	}

}
