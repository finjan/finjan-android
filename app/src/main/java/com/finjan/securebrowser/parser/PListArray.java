package com.finjan.securebrowser.parser;

import org.xml.sax.Attributes;

import java.util.ArrayList;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */


public class PListArray extends PList {



    public PListArray(PList from, ArrayList<PList> fromArray) {
        super(from);
        array=fromArray;
    }
    public PListArray(String QName, Attributes Attrs){
        super(QName, Attrs);
//        array=fromArray;
    }

    public PList getAt(int Index){
        return array.get(Index);
    }
    public int size(){
        return array.size();
    }

}
