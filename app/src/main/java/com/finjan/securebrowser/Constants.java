package com.finjan.securebrowser;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class Constants {

    public static final String tracker = "tracker";
    public static final int ID_OPEN_IN_NEW_TAB_MENU =0;
    public static final int ID_COPY_URL_MENU =1;
    public static final int ID_CANCEL_MENU=2;
    public static final String URL_SEPRATOR="#$%";

    //PList parser contant

    public static final String KEY_APP_STRING= "appstrings";
    public static final String KEY_NOTIFICATION= "notifications";
    public static final String KEY_CHANNELS= "channels";
    public static final String KEY_KEY= "key";
    public static final String KEY_AUTO_SUBSCRIBE= "autosubscribe";
    public static final String KEY_OPTIONAL_MESSAGE= "optional_message";


    public static final String UPDATE_PARAM= "update";
    public static final String CHECK_PARAM= "check";

}
