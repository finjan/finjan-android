package com.finjan.securebrowser;

import com.finjan.securebrowser.application.AnalyticsApplication;
import com.finjan.securebrowser.helpers.logger.Logger;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class AppLog {

    private static final String appName="Finzan";


    public static void printLog(String msg){
        printLog(appName,msg);
    }

    public static void printLog(String tag,String msg){
        if (AnalyticsApplication.showLog){
            Logger.logD(tag,msg);
        }
    }
}
