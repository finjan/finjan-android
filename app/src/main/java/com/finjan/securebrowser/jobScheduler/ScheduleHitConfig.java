package com.finjan.securebrowser.jobScheduler;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;

import com.finjan.securebrowser.constants.AppConstants;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.Driver;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 08/09/17.
 *
 */

public class ScheduleHitConfig {


    private static Driver myDriver;
    private static FirebaseJobDispatcher jobDispatcher;
//    private static final int START_TIME = 20 * 60 * 1000;
    private static final int START_TIME = 30 * 1000;
//    private static final int END_TIME = 40 * 60 * 1000;
    private static final int END_TIME = 40 * 1000;


    private static final class ScheduleHitHolder{
        private static final ScheduleHitConfig instance=new ScheduleHitConfig();
    }
    public static ScheduleHitConfig getInstance(){
        jobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(FinjanVPNApplication.getInstance()));
        return ScheduleHitHolder.instance;
    }
    public void scheduleFirebaseJobSchedule(String tag, Bundle bundle){

        Job job = jobDispatcher.newJobBuilder()
                .setService(ScheduleJobService.class)
                .setTag(tag)
                .setLifetime(Lifetime.FOREVER)
                .setExtras(bundle)
                .build();
        jobDispatcher.mustSchedule(job);
    }


    public void schedulePurchase(Purchase purchase,String userName){
        Bundle bundle=new Bundle();
        bundle.putString(AppConstants.BKEY_ITEMTYPE,purchase.getItemType());
        bundle.putString(AppConstants.BKEY_originalJson,purchase.getOriginalJson());
        bundle.putString(AppConstants.BKEY_ORDERID,purchase.getOrderId());
        bundle.putString(AppConstants.BKEY_PACKAGENAME,purchase.getPackageName());
        bundle.getString(AppConstants.BKEY_SKU,purchase.getSku());
        bundle.putLong(AppConstants.BKEY_PURCHASETIME,purchase.getPurchaseTime());
        bundle.putInt(AppConstants.BKEY_purchaseState,purchase.getPurchaseState());
        bundle.putString(AppConstants.BKEY_developerPayload,purchase.getDeveloperPayload());
        bundle.putString(AppConstants.BKEY_token,purchase.getToken());
        bundle.putLong(AppConstants.BKEY_expiryTimeMillis,purchase.getmExpiryTimeMillis());
        bundle.putBoolean(AppConstants.BKEY_autoRenewing,purchase.getAutoRenewing());
        bundle.putString(AppConstants.BKEY_signature,purchase.getSignature());

//        bundle.putParcelable(AppConstants.BKEY_PURCHASE,purchase);
        bundle.putString(AppConstants.BKEY_USERNAME,userName);
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//            scheduleAndroidJobSchedule(schedulePurchaseJobTAG,bundle);
//        }else {
            scheduleFirebaseJobSchedule(schedulePurchaseJobTAG,bundle);
//        }
    }

    public void scheduleWifiStatusChange(){
        Job job = jobDispatcher.newJobBuilder().setService(ScheduleJobService.class)
                .setTag(scheduleWifiStatusChange)
                .setLifetime(Lifetime.FOREVER)
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                .setRecurring(true)
                .setReplaceCurrent(true)
                .setTrigger(Trigger.executionWindow(0, 0))
                .setConstraints(Constraint.ON_UNMETERED_NETWORK)
                .build();
        jobDispatcher.mustSchedule(job);
    }



    private static final String TAG=ScheduleHitConfig.class.getSimpleName();
    public static final String schedulePurchaseJobTAG="schedulePurchaseJob";
    public static final String scheduleWifiStatusChange="scheduleWifiStatusChange";
}
