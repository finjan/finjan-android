package com.finjan.securebrowser.jobScheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;
import android.text.TextUtils;

import com.finjan.securebrowser.helpers.logger.Logger;
import com.finjan.securebrowser.helpers.security.WifiSecurityHelper;
import com.finjan.securebrowser.helpers.sharedpref.UserPref;
import com.finjan.securebrowser.tracking.google_tracking.GoogleTrackers;
import com.finjan.securebrowser.vpn.FinjanVPNApplication;
import com.finjan.securebrowser.vpn.auto_connect.AutoConnectHelper;
import com.finjan.securebrowser.vpn.subscriptions.SubscriptionHelper;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

/**
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 * Developed by NewOfferings, LLC on 08/09/17.
 *
 */

/**
 * Created by anurag on
 */

public class ScheduleJobService extends JobService {

    @Override
    public boolean onStartJob(JobParameters job) {

        switch (job.getTag()){
            case ScheduleHitConfig.schedulePurchaseJobTAG:
                Logger.logE("Jobschedule start","rescheduled the hit");
                if(!TextUtils.isEmpty(UserPref.getInstance().getCurrent_Access_Token())){
                    UserPref.getInstance().setCurrent_Access_Token("Jobschedule start ");
                }else {
                    UserPref.getInstance().setCurrent_Access_Token(UserPref.getInstance().getCurrent_Access_Token()+" 1 ");
                }
                if(job!=null && job.getExtras()!=null){
                    SubscriptionHelper.getInstance().createSubc(job.getExtras());
                }
                return false;
            case ScheduleHitConfig.scheduleWifiStatusChange:

                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(connectivityManager == null){return false;}
                    try{
                        connectivityManager.registerNetworkCallback(new NetworkRequest.Builder().build(),new ConnectivityManager.NetworkCallback(){
                            @Override
                            public void onAvailable(Network network) {
                                super.onAvailable(network);
                                if(FinjanVPNApplication.getInstance()!=null &&
                                        UserPref.getInstance().getIsMyAppWasInBackground())
                                    AutoConnectHelper.Companion.getInstance().whenStatusChanged(FinjanVPNApplication.getInstance());
                            }

                        });
                    }catch (IllegalArgumentException e){}
                    catch (Exception e){}

                }else{
                    registerReceiver(new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
//                            handleConnectivityChange(!intent.hasExtra("noConnectivity"), intent.getIntExtra("networkType", -1));
                            if(FinjanVPNApplication.getInstance()!=null &&
                                    UserPref.getInstance().getIsMyAppWasInBackground() && !AutoConnectHelper.Companion.getStartedAutoConnecting())
                            AutoConnectHelper.Companion.getInstance().whenStatusChanged(ScheduleJobService.this);
                        }
                    }, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
                return false;
                default:
                    return false;
        }

    }
    private void handleConnectivityChange(NetworkInfo networkInfo){
        // Calls handleConnectivityChange(boolean connected, int type)
    }

    private void handleConnectivityChange(boolean connected, int type){
        // Calls handleConnectivityChange(boolean connected, ConnectionType connectionType)
//        if(WifiSecurityHelper.getInstance().getNetworkType(getApplicationContext()).getType() != WifiSecurityHelper.NetworkType.NT_NONE){
//            Logger.logE(ScheduleHitConfig.scheduleWifiStatusChange, "change to wifi");
//        }
    }

    private void handleConnectivityChange(boolean connected, ConnectionType connectionType){
        // Logic based on the new connection
    }

    private enum ConnectionType{
        MOBILE,WIFI,VPN,OTHER
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        switch (job.getTag()){
            case ScheduleHitConfig.scheduleWifiStatusChange:
                return true;
                default:
                    return false;
        }
    }

}
