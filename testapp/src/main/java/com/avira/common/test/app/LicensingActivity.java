package com.avira.common.test.app;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.finjan.securebrowser.vpn.licensing.PurchaseLicenseBaseActivity;
import com.finjan.securebrowser.vpn.licensing.models.billing.IabResult;
import com.finjan.securebrowser.vpn.licensing.models.billing.Inventory;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.avira.common.test.R;

import java.util.Collection;

/**
 * @author ovidiu.buleandra
 * @since 09.11.2015
 */
public class LicensingActivity extends PurchaseLicenseBaseActivity {

    private static final String SKU_NON_CONSUMABLE = "com.avira.common.test.sku.non_consumable";
    private static final String SKU_CONSUMABLE = "com.avira.common.test.sku.consumable";
    private static final String SKU_SUBSCRIPTION = "com.avira.common.test.sku.subscription";

    private ViewGroup mContainer;
    private ScrollView mScrollView;
    private TextView mWaitView;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_licensing);

        mContainer = (ViewGroup) findViewById(R.id.container);
        mScrollView = (ScrollView) findViewById(R.id.scrollView);
        mWaitView = (TextView) findViewById(R.id.wait);
    }

    @Override
    protected String getDeveloperPayload() {
        return "";
    }


    @Override
    protected void complain(IabResult result) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error")
                .setMessage(result.getMessage())
                .create().show();
    }

    @Override
    protected void onInventoryAvailable(Inventory inventory) {

    }

    @Override
    protected void onPurchaseCompleted(Purchase purchase) {

    }

    @Override
    protected void onConsumeCompleted(Purchase purchase) {

    }

    @Override
    protected void setWaitScreen(boolean state) {
        mScrollView.setVisibility(state ? View.GONE : View.VISIBLE);
        mWaitView.setVisibility(state ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    protected Collection<String> getManagedSKUs() {
        return null;
    }
}
