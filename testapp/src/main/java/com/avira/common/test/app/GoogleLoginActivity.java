/*******************************************************************************
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 *******************************************************************************/
package com.avira.common.test.app;

import android.os.Bundle;
import android.util.Log;

import com.avira.common.authentication.Authentication;
import com.avira.common.authentication.AuthenticationListener;
import com.avira.common.authentication.googleconnect.GoogleConnectTemplateActivity;
import com.avira.common.authentication.models.Subscription;
import com.avira.common.authentication.models.User;
import com.avira.common.test.R;
import com.avira.common.utils.ProgressDialogUtil;
import com.google.android.gms.plus.model.people.Person;

/**
 * Created by valerica.plesu on 1/18/2016.
 */
public class GoogleLoginActivity extends GoogleConnectTemplateActivity implements AuthenticationListener{

    private static final String TAG = GoogleLoginActivity.class.getSimpleName();
    private ProgressDialogUtil mProgressDialogUtil;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_facebook_login);

        mProgressDialogUtil = new ProgressDialogUtil(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mProgressDialogUtil != null) {
            mProgressDialogUtil.dismiss();
        }
    }

    @Override
    protected void doneSuccessfully(Person person, String email, String accessToken) {
        Log.d(TAG, "doneSuccessfully");
        mProgressDialogUtil.show(getString(R.string.connecting_with_google));
        Authentication.loginGoogle(this, email, accessToken, person, this);
    }

    @Override
    protected void requestFailed() {
        Log.d(TAG, "requestFailed");

    }

    @Override
    public void onAuthSuccess(User user, Subscription subscription) {
        mProgressDialogUtil.dismiss();

    }

    @Override
    public void onAuthError(int errorCode, String errorMessage) {
        mProgressDialogUtil.dismiss();
    }

    @Override
    protected String getServerClientId() {
        return getString(R.string.google_server_client_id);
    }

}
