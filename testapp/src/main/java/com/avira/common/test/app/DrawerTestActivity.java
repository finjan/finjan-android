package com.avira.common.test.app;

import android.app.Activity;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.avira.common.authentication.models.UserProfile;
import com.avira.common.test.R;
import com.avira.common.ui.DrawerBuilder;
import com.avira.common.ui.DrawerBuilder.DrawerClickListener;

/**
 * @author ovidiu.buleandra
 * @since 14.12.2015
 */
public class DrawerTestActivity extends Activity implements DrawerClickListener{
    private final static String TAG = DrawerTestActivity.class.getSimpleName();

    DrawerBuilder drawerBuilder;
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.drawer_test_activity);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ViewGroup custom = (ViewGroup)LayoutInflater.from(this).inflate(R.layout.drawer_custom_content, null, false);
        custom.findViewById(R.id.upgrade_btn).setOnClickListener(null);

        UserProfile  userProfile = new UserProfile();
        userProfile.setFirstName("Ovidiu");
        userProfile.setLastName("Buleandra");
        userProfile.setEmail("ovidiu.buleandra@avira.com");
        userProfile.generateGenericPicture(this);

        drawerBuilder = new DrawerBuilder(this)
                .setIcon(R.drawable.drawer_icon)
                .setHeaderBackground(R.drawable.drawer_background)
                .setProfileName(userProfile.getFirstName() + " " +  userProfile.getLastName())
                .setProfileEmail(userProfile.getEmail())
                .setProfilePic(userProfile.getPictureBitmap())
                .addMenuItem(R.drawable.icon, R.string.Settings)
                .addMenuItem(R.drawable.icon_facebook, R.string.MyProfile)
                .addMenuItem(R.drawable.icon_google_plus, R.string.Help)
                .addMenuItem(R.drawable.icon_twitter, R.string.About)
                .addDivider()
                .addCustomContent(custom)
                .hideShareSection()
                .build(((ViewGroup) findViewById(R.id.drawer_content)));

    }

    @Override
    public void onDrawerClickListener(View clickedView, int id) {
        switch (id) {
            case 0:
                Log.d(TAG, "settings clicked");
                break;
            case 1:
                Log.d(TAG, "my profile clicked");
                break;
            case 2:
                Log.d(TAG, "help clicked");
                break;
            case 3:
                Log.d(TAG, "about clicked");
                break;

            case R.id.drawer_back:
                drawerLayout.closeDrawers();
                break;
            case R.id.share_facebook_button:
                Toast.makeText(this, "facebook clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.share_twitter_button:
                Toast.makeText(this, "twitter clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.share_gplus_button:
                Toast.makeText(this, "google plus clicked", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
