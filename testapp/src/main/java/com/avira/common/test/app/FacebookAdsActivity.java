package com.avira.common.test.app;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.avira.common.test.R;
import com.facebook.ads.AdSettings;

public class FacebookAdsActivity extends AppCompatActivity {

    // Nexus 5 test device
    private static final String DEVICE_HASH_ID = "a099809395b141935f24be8a314a7703";
    private View mAdsBanner;

    private FacebookAds mFacebookAdsManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_ads);

        mAdsBanner = findViewById(R.id.ads_banner);

        initFacebookAds();
        showFbAds();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mFacebookAdsManager != null) {
            mFacebookAdsManager.unregister();
        }
    }

    private void initFacebookAds() {
        // uses a factory class here when we have multiple ads provider
        mFacebookAdsManager = new FacebookAds(this);
        mFacebookAdsManager.register(this, mAdsBanner);
        AdSettings.addTestDevice(DEVICE_HASH_ID);

    }

    private void showFbAds() {
       if(mFacebookAdsManager != null) {
           mFacebookAdsManager.load();
       }
    }

    private void closeAds(boolean closedByUser) {
        mAdsBanner.setVisibility(View.GONE);
    }
}
