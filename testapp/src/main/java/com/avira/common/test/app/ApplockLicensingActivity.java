package com.avira.common.test.app;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.avira.common.authentication.models.Subscription;
import com.finjan.securebrowser.vpn.licensing.LicensesCallback;
import com.finjan.securebrowser.vpn.licensing.Licensing;
import com.finjan.securebrowser.vpn.licensing.LicensingService;
import com.finjan.securebrowser.vpn.licensing.ProcessPurchaseCallback;
import com.finjan.securebrowser.vpn.licensing.PurchaseLicenseBaseActivity;
import com.finjan.securebrowser.vpn.licensing.TrialRequestCallback;
import com.finjan.securebrowser.vpn.licensing.models.ProductItem;
import com.finjan.securebrowser.vpn.licensing.models.billing.IabResult;
import com.finjan.securebrowser.vpn.licensing.models.billing.Inventory;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.finjan.securebrowser.vpn.licensing.models.billing.SkuDetails;
import com.finjan.securebrowser.vpn.licensing.models.restful.License;
import com.finjan.securebrowser.vpn.licensing.utils.IabHelper;
import com.avira.common.test.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * @author ovidiu.buleandra
 * @since 09.11.2015
 */
public class ApplockLicensingActivity extends PurchaseLicenseBaseActivity
        implements View.OnClickListener, ProcessPurchaseCallback, LicensesCallback, TrialRequestCallback {

    private final static String SKU_MANAGED = "managed_101";
    private final static String SKU_CONSUMABLE = "consumable_102";
    private final static String SKU_SUBSCRIPTION = "subscription_103";

    public final static String GEO_LOCK = "algl0";
    public final static String SCHEDULED_LOCK = "alsl0";
    public final static String BUNDLE = "albl0";
    private final static HashMap<String, ProductItem> products;
    private final static HashMap<String, String> acronymSKUMap;

    static {
        products = new HashMap<>();
        acronymSKUMap = new HashMap<>();
        acronymSKUMap.put(GEO_LOCK, SKU_SUBSCRIPTION);
        acronymSKUMap.put(SCHEDULED_LOCK, SKU_CONSUMABLE);
        acronymSKUMap.put(BUNDLE, SKU_MANAGED);

        products.put(acronymSKUMap.get(GEO_LOCK), new ProductItem(acronymSKUMap.get(GEO_LOCK), GEO_LOCK, false));
        products.put(acronymSKUMap.get(SCHEDULED_LOCK), new ProductItem(acronymSKUMap.get(SCHEDULED_LOCK),
                SCHEDULED_LOCK, false));
        products.put(acronymSKUMap.get(BUNDLE), new ProductItem(acronymSKUMap.get(BUNDLE), BUNDLE, true));

    }

    private ViewGroup mContainer;
    private ScrollView mScrollView;
    private TextView mWaitView;
    private Inventory mCurrentInventory;
    private LayoutInflater mInflater;
    private HashMap<String, Button> mBuyMap;
    private HashMap<String, TextView> mTrialStatusMap;
    private HashMap<String, Button> mTrialMap;

    public static void startLicensingService(Context context) {
        if(!LicensingService.isEnabled(context)) {
            return;
        }
        HashMap<String, String> skuToAcronym = new HashMap<>();
        skuToAcronym.put(SKU_MANAGED, BUNDLE);
        skuToAcronym.put(SKU_SUBSCRIPTION, GEO_LOCK);
        skuToAcronym.put(SKU_CONSUMABLE, SCHEDULED_LOCK);
        LicensingService.forceStartService(context, skuToAcronym, App.ACRONYM);
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_licensing);

        mInflater = LayoutInflater.from(this);

        mContainer = (ViewGroup) findViewById(R.id.container);
        mScrollView = (ScrollView) findViewById(R.id.scrollView);
        mWaitView = (TextView) findViewById(R.id.wait);

        mBuyMap = new HashMap<>();
        mTrialStatusMap = new HashMap<>();
        mTrialMap = new HashMap<>();

//        Licensing.queryLicenses(this, getString(R.string.backend_url), App.ACRONYM, this);
// test license response interpretation
//        try {
//            InputStreamReader example = new InputStreamReader(getAssets().open("license_response_example.txt"));
//            LicenseArray licenses = new Gson().fromJson(example, LicenseArray.class);
//            example.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected String getDeveloperPayload() {
        return "";
    }

    @Override
    protected void complain(IabResult result) {
        setWaitScreen(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error")
                .setMessage(result.getMessage())
                .create().show();
    }

    @Override
    protected void onInventoryAvailable(Inventory inventory) {
        mCurrentInventory = inventory;
        mContainer.removeAllViewsInLayout();
        List<SkuDetails> list = inventory.getAllSkuDetails();
        if (list.size() > 0) {
            for (SkuDetails skuDetails : list) {
                createProduct(skuDetails);
            }
            Button btnClear = (Button) mInflater.inflate(R.layout.item_clear_btn, mContainer, false);
            btnClear.setOnClickListener(this);
            mContainer.addView(btnClear);
        } else {
            mContainer.addView(mInflater.inflate(R.layout.item_nothing, mContainer, false));
        }
    }

    private void createProduct(SkuDetails details) {
        View item = mInflater.inflate(R.layout.item_license, mContainer, false);

        String sku = details.getSku();
        ProductItem product = products.get(sku);
        product.setSKUDetails(details)
                .setPurchase(mCurrentInventory.getPurchase(sku));

        TextView title = (TextView) item.findViewById(R.id.title);
        title.setText(details.getTitle());

        TextView type = (TextView) item.findViewById(R.id.type);
        type.setText(details.getType());

        TextView description = (TextView) item.findViewById(R.id.description);
        description.setText(details.getDescription());

        TextView price = (TextView) item.findViewById(R.id.price);
        price.setText(details.getPrice());

        Log.i(TAG, String.format("sku %s %s- %s %s",
                details.getSku(), details.getTitle(), details.getPriceValue(), details.getCurrencyCode()));

        Button btnBuy = (Button) item.findViewById(R.id.buy_button);
        btnBuy.setTag(product);
        btnBuy.setOnClickListener(this);
        mBuyMap.put(sku, btnBuy);

        // trial stuff
        ViewGroup trialContainer = (ViewGroup) item.findViewById(R.id.trial_container);

        Button btnTrial = (Button) item.findViewById(R.id.trial_button);
        btnTrial.setTag(product);
        btnTrial.setOnClickListener(this);
        mTrialMap.put(sku, btnTrial);

        TextView textTrialStatus = (TextView) item.findViewById(R.id.trial_status);
        mTrialStatusMap.put(sku, textTrialStatus);
        textTrialStatus.setText("Trial status: unknown");

        trialContainer.setVisibility(product.hasTrial() ? View.VISIBLE : View.GONE);

        mContainer.addView(item);
    }

    @Override
    protected void onPurchaseCompleted(Purchase purchase) {
        ProductItem product = products.get(purchase.getSku());
        product.setPurchase(purchase);
        updateBuy(purchase.getSku());
        setWaitScreen(true);
        Licensing.processPurchase(this, purchase, product.getSKUDetails(), product.getAcronym(), this);
    }

    private void updateBuy(String sku) {
        Button buyButton = mBuyMap.get(sku);
        if (products.get(sku).getPurchase() != null) {
            buyButton.setText("Already owned!");
            buyButton.setEnabled(false);
        } else {
            buyButton.setText("Buy");
            buyButton.setEnabled(true);
        }
    }

    @Override
    protected void onConsumeCompleted(Purchase purchase) {
        products.get(purchase.getSku()).setPurchase(null);
        updateBuy(purchase.getSku());
    }

    @Override
    protected void setWaitScreen(boolean state) {
        mScrollView.setVisibility(state ? View.GONE : View.VISIBLE);
        mWaitView.setVisibility(state ? View.VISIBLE : View.GONE);
    }

    @Override
    protected Collection<String> getManagedSKUs() {
        return Arrays.asList(SKU_MANAGED, SKU_CONSUMABLE, SKU_SUBSCRIPTION);
    }

    @Override
    public void onClick(View v) {
        ProductItem product;
        switch (v.getId()) {
            case R.id.btn_clear:
                List<Purchase> purchases = new ArrayList<>();
                for (ProductItem prod : products.values()) {
                    if (prod.getPurchase() != null) {
                        if (prod.getSKUDetails().isManagedProduct()) {
                            purchases.add(prod.getPurchase());
                        }
                    }
                }
                if (purchases.size() > 0) {
                    consumePurchases(purchases);
                }
                break;
            case R.id.buy_button:
                product = (ProductItem) v.getTag();
                launchPurchase(product.getSKUDetails());
                break;
            case R.id.trial_button:
                product = (ProductItem) v.getTag();
                Licensing.requestTrialLicense(this, product.getAcronym(), this);
                break;
        }
    }

    @Override
    public void onProcessPurchaseError(int errorCode, String message) {
        setWaitScreen(false);
    }

    @Override
    public void onProcessPurchaseSuccessful(boolean result) {
        setWaitScreen(false);
    }

    @Override
    public void onLicenseQuerySuccess(List<License> licenses) {
        int i = 0;
        for (License license : licenses) {
            Log.i(TAG, String.format("[%d] license found for %s (%s - %s)",
                    i++,
                    license.getProductAcronym(),
                    license.getLicenseType(),
                    license.getExpirationDateString()));
            if(acronymSKUMap.containsKey(license.getProductAcronym())) {
                products.get(acronymSKUMap.get(license.getProductAcronym())).setLicense(license);
            }

        }
    }

    @Override
    public void onLicenseQueryError(int errorCode, String message) {
        complain(new IabResult(IabHelper.IABHELPER_ERROR_BASE, message));
    }

    @Override
    public void onTrialRequestSuccessful(String productAcronym, Subscription subscription) {
        final String sku = acronymSKUMap.get(productAcronym);
        products.get(sku).setTrialStatus(subscription.getStatus());
        mTrialStatusMap.get(sku).setText("Trial status: active");
    }



    @Override
    public void onTrialRequestError(int errorCode, String errorMessage) {
        complain(new IabResult(IabHelper.IABHELPER_ERROR_BASE, errorMessage));
    }


}
