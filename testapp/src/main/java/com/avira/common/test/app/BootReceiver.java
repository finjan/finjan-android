package com.avira.common.test.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author ovidiu.buleandra on 08.12.2015.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ApplockLicensingActivity.startLicensingService(context);
    }
}
