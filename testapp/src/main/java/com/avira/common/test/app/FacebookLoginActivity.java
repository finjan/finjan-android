/*******************************************************************************
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 *******************************************************************************/
package com.avira.common.test.app;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.avira.common.authentication.Authentication;
import com.avira.common.authentication.AuthenticationListener;
import com.avira.common.authentication.facebookconnect.FBCFacebookTemplateActivity;
import com.avira.common.authentication.facebookconnect.FBFacebookUtils;
import com.avira.common.authentication.models.Subscription;
import com.avira.common.authentication.models.User;
import com.avira.common.test.R;
import com.avira.common.utils.ProgressDialogUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class FacebookLoginActivity extends FBCFacebookTemplateActivity implements AuthenticationListener{

    private static final String TAG = FacebookLoginActivity.class.getSimpleName();

    private Context mActivity;
    private ProgressDialogUtil mProgressDialogUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_login);

        mActivity = this;
        mProgressDialogUtil = new ProgressDialogUtil(this);
        mProgressDialogUtil.show(getString(R.string.connecting_with_facebook));
    }

    private void finishWithError()
    {
        Log.d(TAG, "finishWithError");
        Toast.makeText(mActivity, mActivity.getString(R.string.fbc_request_error), Toast.LENGTH_SHORT).show();
        mProgressDialogUtil.dismiss();
        setResult(RESULT_CANCELED);
        finish();
    }

    private void requestCanceled() {
        mProgressDialogUtil.dismiss();
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void facebookRequestSuccessful(String fbToken, JSONObject user) {
        Log.d(TAG, "facebookRequestSuccessful() called with: " + "fbToken = [" + fbToken + "], user = [" + user.toString() + "]");
        try {
            Authentication.loginFacebook(this, fbToken, user.getString(FBFacebookUtils.FB_EMAIL_PARAM), user, this);
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing facebook user data, " + e);
            finishWithError();
        }
    }

    @Override
    protected void facebookRequestCanceled() {
        Log.d(TAG, "facebookRequestCanceled()");
        requestCanceled();
    }

    @Override
    protected void facebookRequestFailed() {
        Log.d(TAG, "facebookRequestFailed() called with");
        finishWithError();
    }

    @Override
    protected void facebookShareSuccessful(boolean result) {
        String title = "";
        if(result) {
            title = "success posting!";
        } else {
            title = "error posting!";
        }
        Toast.makeText(mActivity, title, Toast.LENGTH_LONG).show();
        mProgressDialogUtil.dismiss();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void facebookShareFailed() {
        String title = "error post to fb.";
        Toast.makeText(mActivity, title, Toast.LENGTH_LONG).show();
        mProgressDialogUtil.dismiss();
        finish();
    }

    @Override
    protected void facebookShareCanceled() {
        requestCanceled();
    }

    @Override
    public void onAuthSuccess(User user, Subscription subscription) {
        mProgressDialogUtil.dismiss();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onAuthError(int errorCode, String errorMessage) {
        finishWithError();
    }
}
