package com.avira.common.test.app;

import android.app.Application;
import android.util.Log;

import com.avira.common.GeneralPrefs;
import com.avira.common.database.SecureDB;
import com.finjan.securebrowser.vpn.licensing.models.restful.License;
import com.finjan.securebrowser.vpn.licensing.models.restful.LicenseArray;
import com.google.gson.Gson;

/**
 * @author ovidiu.buleandra
 * @since 11.11.2015
 */
public class App extends Application {

    private final String TAG = App.class.getSimpleName();

    public static final String ACRONYM = "aplp0"; // applock+ acronym for testing purposes
    public static final String PARTNER = "avira"; // avira - for each avira products, should be override into Launcher
    private static App INSTANCE;

    public static App getInstance() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;

        SecureDB.init(this, null);

        GeneralPrefs.setAppId(this, ACRONYM);
        GeneralPrefs.setPartnerId(this, PARTNER);
        Log.d("App partner - ", PARTNER);

        LicenseArray licenseArray = new Gson().fromJson("{\"data\": [{\"relationships\": {\"device\": {\"data\": {\"type\": \"devices\", \"id\": 4414589}}, \"app\": {\"data\": {\"type\": \"apps\", \"id\": \"aobd0\"}}, \"user\": {\"data\": {\"type\": \"users\", \"id\": 3000059375}}}, \"attributes\": {\"runtime_unit\": \"months\", \"expiration_date\": \"2016-03-01T00:00:00Z\", \"key\": null, \"devices_limit\": 1, \"runtime\": 1, \"type\": \"eval\"}, \"type\": \"device-licenses\", \"id\": \"34798768-400001546-2a300f635bd187c7c94320fda977c9330db3263d\"}, {\"relationships\": {\"device\": {\"data\": {\"type\": \"devices\", \"id\": 4418798}}, \"app\": {\"data\": {\"type\": \"apps\", \"id\": \"albl0\"}}, \"user\": {\"data\": {\"type\": \"users\", \"id\": 3000059375}}}, \"attributes\": {\"runtime_unit\": \"months\", \"expiration_date\": \"2016-04-06T00:00:00Z\", \"key\": \"\", \"devices_limit\": 1, \"runtime\": 1, \"type\": \"eval\"}, \"type\": \"device-licenses\", \"id\": \"34798768-400001387-a42765210aebdeabca83c8082d0f1281212a3154\"}]}", LicenseArray.class);
        for(License license: licenseArray.getLicenses()) {
            license.adjustExpirationDate();
        }
        Log.d(TAG, "licenses=" + licenseArray);
    }
}
