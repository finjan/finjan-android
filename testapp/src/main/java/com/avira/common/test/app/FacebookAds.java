package com.avira.common.test.app;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avira.common.ads.AdsCallbackInterface;
import com.avira.common.test.R;
import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.NativeAd;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by valerica.plesu on 3/7/2016.
 */
public class FacebookAds implements AdsCallbackInterface, AdListener {

    private static final String TAG = FacebookAds.class.getSimpleName();
    private static final String AD_PLACEMENT = "1601539683400055_1603753363178687";

    private WeakReference<Context> mContext;
    private NativeAd mFacebookAd;
    private View mBannerLayout;

    public FacebookAds(Context context) {
        mContext = new WeakReference<>(context);
    }

    public void insertAddContentInLayout() {
        String adTitle = mFacebookAd.getAdTitle();
        NativeAd.Image imageAd = mFacebookAd.getAdIcon();
        String titleAdForButton = mFacebookAd.getAdCallToAction();

        if ( adTitle != null && imageAd != null && titleAdForButton != null) {


            String adSubTitle = mFacebookAd.getAdSubtitle() != null ? mFacebookAd.getAdSubtitle() : "";
            TextView genericAdTitle = (TextView) mBannerLayout.findViewById(R.id.generic_ad_title);
            TextView genericAdSubtitle = (TextView) mBannerLayout.findViewById(R.id.generic_ad_subtitle);
            TextView adButton = (TextView) mBannerLayout.findViewById(R.id.generic_ad_ctabutton);
            ImageView adImage = (ImageView) mBannerLayout.findViewById(R.id.generic_ad_image);


            genericAdTitle.setText(adTitle);
            genericAdSubtitle.setText(adSubTitle);
            genericAdSubtitle.setSelected(true);
            adButton.setText(titleAdForButton);
            NativeAd.downloadAndDisplayImage(imageAd, adImage);

            mBannerLayout.setVisibility(View.VISIBLE);
            List<View> clickableList = new ArrayList<View>();
            clickableList.add(adButton);

            ViewGroup adchoicesHolder = (ViewGroup) mBannerLayout.findViewById(R.id.adchoices_viewholder);
            Context context;
            if (adchoicesHolder != null && mContext != null && (context = mContext.get()) != null && adchoicesHolder.getChildCount() == 0) {
                adchoicesHolder.addView(new AdChoicesView(context, mFacebookAd, false));
            }
            mFacebookAd.registerViewForInteraction(mBannerLayout, clickableList);
        } else {
            mBannerLayout.setVisibility(View.GONE);

        }
    }

    public void onError() {
        if (mBannerLayout != null){
            mBannerLayout.setVisibility(View.GONE);

        }
    }

    public String getAdPlacementId() {
        return AD_PLACEMENT;
    }

    @Override
    public void register(Context context, View view) {
        mFacebookAd = new NativeAd(context, getAdPlacementId());
        mFacebookAd.setAdListener(this);
        mBannerLayout = view;
    }

    @Override
    public void load() {
        mFacebookAd.loadAd();
    }

    @Override
    public void unregister() {
        mFacebookAd.unregisterView();
    }

    @Override
    public void onError(Ad ad, AdError adError) {
        Log.d(TAG, "Error get add, " + adError.getErrorMessage());
        onError();
    }

    @Override
    public void onAdLoaded(Ad ad) {
        if (ad == null) {
            Log.d(TAG, "No ads");
            onError();
            return;
        }
        this.insertAddContentInLayout();
    }

    @Override
    public void onAdClicked(Ad ad) {
        Log.d(TAG, "Ad clicked");
    }

}
