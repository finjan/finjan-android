package com.avira.common.test.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.avira.common.authentication.Authentication;
import com.avira.common.dialogs.AviraDialog;
import com.avira.common.events.AuthFailEvent;
import com.avira.common.events.AuthSuccessEvent;
import com.avira.common.id.HardwareId;
import com.avira.common.test.R;
import com.avira.common.utils.AccessTokenUtil;
import com.avira.common.utils.DeviceInfo;
import com.avira.common.utils.SharedPreferencesUtilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String PREFS_EMAIL = "prefs_email";
    private final static String PREFS_PASSWORD = "prefs_password";
    private final static String PREFS_TOKEN = "prefs_token";

    private final static String DEFAULT_EMAIL = "jnegtoipwejasds@ssr.avira.com";//"androidsolutionteam@gmail.com";
    private final static String DEFAULT_PASS = "Agilefire2000!";
    private int contentHeight = 250;
    ListView mList;
    ListAdapter mListAdapter;
    List<ListItem> mListItems;
    Button mGenerateNewUser;
    TextView mUserEmailView;
    TextView mTokenView;
    String mEmail, mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();
        populateList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void bindViews() {
        mList = (ListView) findViewById(R.id.list);
        mListItems = new ArrayList<>();
        mListAdapter = new ListAdapter(this, mListItems);
        mList.setAdapter(mListAdapter);

        mGenerateNewUser = (Button) findViewById(R.id.generate_button);
        mGenerateNewUser.setOnClickListener(this);

        mTokenView = (TextView) findViewById(R.id.token);
        mUserEmailView = (TextView) findViewById(R.id.user);

        mEmail = SharedPreferencesUtilities.getString(this, PREFS_EMAIL);
        mPassword = SharedPreferencesUtilities.getString(this, PREFS_PASSWORD);
        Log.i("App", String.format("[load] user %s, password %s", mEmail, mPassword));
        boolean isLogin = true;
        if (TextUtils.isEmpty(mEmail)) {
            mEmail = DEFAULT_EMAIL;
            mPassword = DEFAULT_PASS;
            isLogin = false;
            saveCredentials(mEmail, mPassword);
        }

        mUserEmailView.setText(mEmail);
        ((TextView)findViewById(R.id.device_type)).setText("Is tablet = " +DeviceInfo.isTablet(this));
        Authentication.loginEmail(this, mEmail, mPassword, isLogin);

        Button showAviraDialog = (Button) findViewById(R.id.show_dialog);
        showAviraDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout customContent = new LinearLayout(MainActivity.this);
                customContent.setOrientation(LinearLayout.VERTICAL);
                customContent.setBackgroundColor(getResources().getColor(R.color.avira_red_color));

               contentHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                       contentHeight, getResources().getDisplayMetrics());

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        contentHeight
                );

                customContent.setLayoutParams(lp);

                LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
                View view = layoutInflater.inflate(R.layout.dialog_list_view, customContent, false);
                customContent.addView(view);



                ListView listView = (ListView) view.findViewById(R.id.dialog_list_view);
                String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
                        "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                        "Linux", "OS/2" };
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                        android.R.layout.simple_list_item_1, values);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.i("@@@", "id " + id);
                    }
                });

                AviraDialog dialog = new AviraDialog.Builder(MainActivity.this)
                        .setIcon(R.drawable.icon)
                        .setTitle("Title")
                        .setCancelable(true)
                        .setNoButtons(true)
                        .addCustomView(customContent)
                        .show(MainActivity.this.getSupportFragmentManager());

            }
        });
    }

    private void populateList() {
        mListItems.add(new ListItem("Google Static Licensing", StaticLicensingActivity.class));
        mListItems.add(new ListItem("Normal Licensing", LicensingActivity.class));
        mListItems.add(new ListItem("Applock Licensing Testing", ApplockLicensingActivity.class));
        mListItems.add(new ListItem("Drawer Test", DrawerTestActivity.class));
        mListItems.add(new ListItem("FacebookLogin", FacebookLoginActivity.class));
        mListItems.add(new ListItem("GoogleLogin", GoogleLoginActivity.class));
        mListItems.add(new ListItem("Facebook Ads", FacebookAdsActivity.class));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.generate_button) {
            loginNewUser();
        }
    }

    private void loginNewUser() {
        toggleButton(false);
        String email = String.format("%s@ssr.avira.com", generateRandomString(5));
        String pass = generateRandomString(6);
        Log.i("App", String.format("[save] user %s, password %s", email, pass));
        saveCredentials(email, pass);
        Authentication.loginEmail(this, email, pass, false);
    }

    private String generateRandomString(int length) {
        Random rand = new Random();
        StringBuilder sb = new StringBuilder();
        while (length-- > 0) {
            sb.append((char) (97 + rand.nextInt(26)));
        }

        return sb.toString();
    }

    private void saveCredentials(String email, String password) {
        mEmail = email;
        mPassword = password;
        SharedPreferencesUtilities.putString(this, PREFS_EMAIL, email);
        SharedPreferencesUtilities.putString(this, PREFS_PASSWORD, password);
    }

    @SuppressWarnings("unused")
    public void onEvent(AuthSuccessEvent event) {
        toggleButton(true);
        Log.d("App", "logged in successfully");
        String token = AccessTokenUtil.generateToken(this, App.ACRONYM);
        mTokenView.setText(token);
        SharedPreferencesUtilities.putString(this, PREFS_TOKEN, token);
        mUserEmailView.setText(mEmail);
        // start the licensing service only after user is registered or logged in
//        LicensingService.enableService(this);
//        ApplockLicensingActivity.startLicensingService(this);

        HardwareId.get(this);
    }

    public void onAuthError(AuthFailEvent event) {
        toggleButton(true);
        Log.d("App", "error logging in " + event.getErrorMessage());
        mUserEmailView.setText("error logging in " + mEmail);
    }

    private void toggleButton(boolean enabled) {
        mGenerateNewUser.setEnabled(enabled);
        mGenerateNewUser.setText(enabled ? "Generate new" : "Loading...");
    }

    private static class ListItem {
        private String mLabel;
        private Class<? extends Activity> mTargetActivity;

        public ListItem(String label, Class<? extends Activity> targetActivity) {
            mLabel = label;
            mTargetActivity = targetActivity;
        }

        public Class<? extends Activity> getTargetActivity() {
            return mTargetActivity;
        }

        @Override
        public String toString() {
            return mLabel;
        }
    }

    private static class ListAdapter extends ArrayAdapter<ListItem> {

        public ListAdapter(Context context, List<ListItem> listItems) {
            super(context, android.R.layout.simple_list_item_1, listItems);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            final ListItem listItem = getItem(position);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getContext().startActivity(new Intent(getContext(), listItem.getTargetActivity()));
                }
            });
            return view;
        }
    }
}
