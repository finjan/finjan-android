package com.avira.common.test.app;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.finjan.securebrowser.vpn.licensing.PurchaseLicenseBaseActivity;
import com.finjan.securebrowser.vpn.licensing.models.billing.IabResult;
import com.finjan.securebrowser.vpn.licensing.models.billing.Inventory;
import com.finjan.securebrowser.vpn.licensing.models.billing.Purchase;
import com.avira.common.test.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author ovidiu.buleandra
 * @since 04.11.2015
 */
public class StaticLicensingActivity extends PurchaseLicenseBaseActivity implements View.OnClickListener {

    private static final String SKU_TEST_PURCHASED = "android.test.purchased";
    private static final String SKU_TEST_CANCELED = "android.test.canceled";
    private static final String SKU_TEST_REFUNDED = "android.test.refunded";
    private static final String SKU_TEST_UNAVAILABLE = "android.test.item_unavailable";

    private View mLoadingView;
    private View mContent;
    private ListView mListView;
    private List<String> mListItems;
    private ArrayAdapter<String> mListAdapter;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_static_licensing);

        mLoadingView = findViewById(R.id.loading);
        mContent = findViewById(R.id.button_content);
        mListView = (ListView) findViewById(R.id.list);

        mListItems = new ArrayList<>();
        mListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mListItems);
        mListView.setAdapter(mListAdapter);

        findViewById(R.id.btn_check_purchase).setOnClickListener(this);
        findViewById(R.id.btn_check_canceled).setOnClickListener(this);
        findViewById(R.id.btn_check_refund).setOnClickListener(this);
        findViewById(R.id.btn_check_unavailable).setOnClickListener(this);
    }

    @Override
    protected String getDeveloperPayload() {
        return "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_check_purchase:
                launchPurchaseItem(SKU_TEST_PURCHASED);
                break;
            case R.id.btn_check_canceled:
                launchPurchaseItem(SKU_TEST_CANCELED);
                break;
            case R.id.btn_check_refund:
                launchPurchaseItem(SKU_TEST_REFUNDED);
                break;
            case R.id.btn_check_unavailable:
                launchPurchaseItem(SKU_TEST_UNAVAILABLE);
                break;
        }
    }

    @Override
    protected void complain(IabResult result) {
        logMessage(result.getMessage());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error")
                .setMessage(result.getMessage())
                .create().show();
    }

    @Override
    protected void onInventoryAvailable(Inventory inventory) {
        Log.i(TAG, "inventory available: " + inventory);
        Purchase purchase = inventory.getPurchase(SKU_TEST_PURCHASED);
        if(purchase != null) {
            consumePurchase(purchase);
        }
    }

    @Override
    protected void onPurchaseCompleted(Purchase purchase) {
        if(purchase.getSku().equals(SKU_TEST_PURCHASED)) {
            consumePurchase(purchase);
        }
        logMessage("Purchase successful: " + purchase.toString());

    }

    @Override
    protected void onConsumeCompleted(Purchase purchase) {
        logMessage("Consumption completed: " + purchase.toString());
    }

    @Override
    protected void setWaitScreen(boolean state) {
        mLoadingView.setVisibility(state ? View.VISIBLE : View.GONE);
        mContent.setVisibility(state ? View.GONE : View.VISIBLE);
    }

    @Override
    protected Collection<String> getManagedSKUs() {
        return Arrays.asList(SKU_TEST_PURCHASED, SKU_TEST_CANCELED, SKU_TEST_REFUNDED, SKU_TEST_UNAVAILABLE);
    }

    private void logMessage(String message) {
        mListItems.add(0, message);
        mListAdapter.notifyDataSetChanged();
        Log.i(TAG, message);
    }
}

