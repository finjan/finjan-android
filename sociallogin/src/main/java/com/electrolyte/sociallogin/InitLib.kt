package com.electrolyte.sociallogin

import android.app.Application
import android.content.Context
import com.electrolyte.sociallogin.socialLogin.FbSignInHandler
import com.electrolyte.sociallogin.tracking.facebook.FbTrackingConfig

class InitLib(context: Context){

    companion object {
        lateinit var context:Application


        fun initLib(context: Application){
            InitLib.context =context
            FbSignInHandler.initLib(context)
            FbTrackingConfig.getInstance().init(context)
        }
    }
}