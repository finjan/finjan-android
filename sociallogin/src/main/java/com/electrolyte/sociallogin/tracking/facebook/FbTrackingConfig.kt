package com.electrolyte.sociallogin.tracking.facebook

import android.app.Application
import android.content.Context
import android.os.BadParcelableException
import android.os.Bundle
import com.electrolyte.sociallogin.InitLib
import com.electrolyte.sociallogin.R
import com.electrolyte.sociallogin.tracking.CustomTrackers
import com.electrolyte.utils.DateHelper
import com.facebook.appevents.AppEventsConstants
import com.facebook.appevents.AppEventsLogger
import java.util.*

class FbTrackingConfig{

    fun init(context: Application) {
        try {
//            FacebookSdk.sdkInitialize(context)
            AppEventsLogger.activateApp(InitLib.context)
            logger = AppEventsLogger.newLogger(context, context.getString(R.string.facebook_app_id))
        } catch (e: BadParcelableException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun getLogger(context: Context): AppEventsLogger? {
        if (logger == null) {
            logger = AppEventsLogger.newLogger(context, context.getString(R.string.facebook_app_id))
        }
        return logger
    }

    private var logger: AppEventsLogger? = null

    fun logRegistration(context: Context, email:String, userName:String) {

        if (!enabled) {
            return
        }
        logger = getLogger(context)
        val bundle = Bundle()
        bundle.putString("Finjan UserEmail", email)
        bundle.putString("Finjan UserName", userName)
        logger!!.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION)
        logger!!.logEvent(CustomTrackers.VPN_REGISTER, bundle)
    }

    fun logLogin(context: Context,email: String,userName: String) {
        if (!enabled) {
            return
        }
        logger = getLogger(context)
        val bundle = Bundle()
        bundle.putString("Finjan UserEmail", email)
        bundle.putString("Finjan UserName", userName)
        logger!!.logEvent(CustomTrackers.VPN_LOGIN, bundle)
    }

    fun logPurchase(context: Context,email:String, userName: String,
                    purchaseStart:String, purchaseEnd:String,
                    subId:String,subInterval:String,sbsAmount:Double) {
        if (!enabled) {
            return
        }
        logger = getLogger(context)
        val bundle = Bundle()
        bundle.putString("Finjan UserEmail", email)
        bundle.putString("Finjan UserName",userName)
        bundle.putString("Subs Start", purchaseStart)
        bundle.putString("Subs Ends", purchaseEnd)
        bundle.putString("Subs Id", subId)
        bundle.putString("Subs Interval", subInterval)
        bundle.putDouble("Subs Amount", sbsAmount)
        logger!!.logPurchase(java.math.BigDecimal.valueOf(sbsAmount),
                Currency.getInstance("USD"), bundle)
        logger!!.logEvent(CustomTrackers.VPN_SUBSCRIBED, bundle)
    }

    fun logPurchaseRenew(context: Context,email: String,userName: String,subId: String,sbsAmount: Double,
                         subInterval: String) {
        if (!enabled) {
            return
        }
        logger = getLogger(context)
        val bundle = Bundle()
        bundle.putString("Finjan UserEmail", email)
        bundle.putString("Finjan UserName", userName)
        bundle.putString("Subs Renews", DateHelper.getInstnace().getServerFormatDate(System.currentTimeMillis()))
        bundle.putString("Subs Id", subId)
        bundle.putDouble("Subs Amount", sbsAmount)
        bundle.putString("Subs Interval", subInterval)
        logger!!.logPurchase(java.math.BigDecimal.valueOf(sbsAmount),
                Currency.getInstance("USD"), bundle)
        logger!!.logEvent(CustomTrackers.VPN_SUBSCRIPTION_RENEWED, bundle)
    }

   companion object {
       private var enabled = true
       private var appId: String? = null

       private object FbTrackingConfigHolder {
           val Instance1 = FbTrackingConfig()
       }

       fun getInstance(): FbTrackingConfig {
           return FbTrackingConfigHolder.Instance1
       }
   }
    fun plistConfig(enabled: Boolean, appId: String) {
        FbTrackingConfig.enabled = enabled
        FbTrackingConfig.appId = appId
    }
}