package com.electrolyte.sociallogin.tracking;

public interface CustomTrackers {
    String VPN_REGISTER = "VPN USER REGISTERED";
    String VPN_SUBSCRIBED = "VPN SUBSCRIBED";
    String VPN_SUBSCRIPTION_RENEWED = "Renewal";
    String VPN_LOGIN="VPN USER LOGIN";
}