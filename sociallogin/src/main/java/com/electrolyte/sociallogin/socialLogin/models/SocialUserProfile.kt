package com.electrolyte.sociallogin.socialLogin.models

import android.text.TextUtils

class SocialUserProfile{

    val FB_USER = "fb"
    val GG_USER = "gg"
    val Email_USER = "email"

    private var firstName: String = ""
    private var lastName:String = ""
    private var email:String = ""
    private var socialType:String = ""
    private var socialId: String = ""
    private var emailVerified: String = "social"

    fun getToken_id(): String {
        //        if(token_id=="429552714162294"){return "4295527141622941";}
        //        return token_id==null?getRandomToken():token_id;
        return token_id
    }

    fun setToken_id(token_id: String) {
        this.token_id = token_id
    }

    private var token_id: String = ""

    fun getSocialId(): String {
        return socialId
    }

    /**
     *
     * 429552714162294
     *
     * could you please remove these two user
     * @param socialId
     */

    fun setSocialId(socialId: String) {
        this.socialId = socialId
    }

    fun getSocialType(): String? {
        return socialType
    }

    fun setSocialType(socialType: String) {
        this.socialType = socialType
    }

    fun SocialUserProfile(firstName: String, lastName: String, email: String) {
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
    }

    fun getFirstName(): String? {
        return firstName
    }

    fun setFirstName(firstName: String) {
        this.firstName = firstName
    }

    fun getLastName(): String {
        if (TextUtils.isEmpty(lastName)) {
            lastName = ""
        }
        return lastName
    }

    fun setLastName(lastName: String) {
        this.lastName = lastName
    }

    fun getEmail(): String? {
        return email
        //        return "scot@scotrobinson.com";
    }

    fun setEmail(email: String) {
        //        if(TextUtils.isEmpty(email)){
        //            email=socialType.equals("fb_id")?socialId+"@facebook-social.com":socialId+"@google-social.com";
        //        }
        this.email = email
        //        this.email = "scot@scotrobinson.com";
    }

    fun setEmailVerified(emailVerified:String){
        this.emailVerified = emailVerified
    }

    fun getEmailVerified():String?{
        return emailVerified
    }


    fun makeSocialEmail() {
        if (!emailEndsWithSocialId()) {
            val socialTag = if (socialType == "fb_id") "@facebook-social.com" else "@google-social.com"
            email = socialId!! + socialTag
        }
    }

    fun emailEndsWithSocialId(): Boolean {
        return email!!.endsWith("@facebook-social.com") || email!!.endsWith("@google-social.com")
    }

    fun isSocialFb(): Boolean {
        //        return (email.endsWith("@facebook-social.com"));
        return socialType == "fb_id"
    }

    fun isSocialGoogle(): Boolean {
        return email!!.endsWith("@google-social.com")
    }


    fun getRandomToken(): String {
        return "4295527" + System.currentTimeMillis()
    }


    fun getScotsUser(): SocialUserProfile {
        val scotUser = SocialUserProfile()
        scotUser.setEmail("scot@scotrobinson.com")
        scotUser.setFirstName("Scot Robinson")
        scotUser.setLastName("")
        scotUser.setSocialId("10155208647842074")
        scotUser.setSocialType("fb_id")
        scotUser.setToken_id(this.token_id)
        return scotUser
    }
}