package com.electrolyte.sociallogin.socialLogin

import android.app.Application

class LibApplication: Application(){


    companion object {
        private lateinit var instance1: LibApplication

        fun getInstance(): LibApplication {
            return instance1
        }
    }


    override fun onCreate() {
        super.onCreate()
        instance1 = this
    }
}