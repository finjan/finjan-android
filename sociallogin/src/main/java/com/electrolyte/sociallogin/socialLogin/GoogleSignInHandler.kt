package com.electrolyte.sociallogin.socialLogin

import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import android.text.TextUtils
import com.electrolyte.logger.Logger
import com.electrolyte.sociallogin.socialLogin.models.SocialUserProfile
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient

class GoogleSignInHandler : GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private var mGoogleApiClient: GoogleApiClient? = null
    private val RC_SIGN_IN = 0
    private val RESULT_OK = 0
    private var result: GoogleSignInResult? = null
    private var frag: androidx.fragment.app.FragmentActivity? = null
    private var personProfile: SocialUserProfile? = null
    /* Is there a ConnectionResult resolution in progress? */
    private var mIsResolving = false

    /* Should we automatically resolve ConnectionResults when possible? */
    private var mShouldResolve = false



    companion object {

    }

    fun handle_initialization(fragmentActivity: androidx.fragment.app.FragmentActivity) {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        frag = fragmentActivity
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("648551950159-qn8djgas54g46ig7l8e0ovr5ub7hmmke.apps.googleusercontent.com")
                .requestProfile()
                .build()
        mGoogleApiClient = GoogleApiClient.Builder(fragmentActivity)
                .enableAutoManage(fragmentActivity /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build()
    }
    fun logout(){
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient)
        }
    }


    fun hanlde_onStartConfig() {
        mGoogleApiClient?.connect()
    }

    fun handle_onStopConfig() {
        mGoogleApiClient?.disconnect()
    }

    fun onClickSignInButton(): Intent {
        mShouldResolve = true
        mGoogleApiClient?.connect()
//GoogleTrackers.setEGoogle_login()

        Logger.logE("google signup", "siging in")
        // Show a message to the user that we are signing in.
        //        mStatus.setText(R.string.signing_in);
        return Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
    }

    fun onClickSignOutButton(): Boolean {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {
            //                        updateUI(false);
        }
        return false
    }


    fun performTaskOnActivityResult(requestCode: Int, resultCode: Int, data: Intent): SocialUserProfile? {
        // The Task returned from this call is always completed, no need to attach
        // a listener.
        if (requestCode == RC_SIGN_IN) {
            // If the error resolution was not successful we should not resolve further.
            if (resultCode != RESULT_OK) {
                mShouldResolve = false
            }
            mIsResolving = false
            mGoogleApiClient?.connect()
            result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            val acct = result?.getSignInAccount()
            if (acct != null) {
                personProfile = SocialUserProfile()
                personProfile?.setSocialType("gg_id")
                //                personProfile.setSocialId("106698897187359322472");
                personProfile?.setSocialId(acct.id as String)

                personProfile?.setToken_id(acct.idToken as String)
                if (TextUtils.isEmpty(acct.email)) {
                    personProfile?.setEmail(acct.id!! + "@google-social.com")
                } else {
                    personProfile?.setEmail(acct.email as String)
                }
                if (!TextUtils.isEmpty(acct.givenName))
                    personProfile?.setFirstName(acct.givenName as String)

                if (!TextUtils.isEmpty(acct.familyName))
                    personProfile?.setLastName(acct.familyName as String)
            }
        }
        return personProfile
    }

    /**
     * if(socialId=="429552714162294"){
     * socialId="429552714162272";
     * }
     * if(socialId=="106698897187359322439"){
     * socialId="106698897187359322472";
     * }
     * @param connectionResult
     */

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.
        Logger.logD("signup", "onConnectionFailed:$connectionResult")

        if (!mIsResolving && mShouldResolve) {
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(frag, RC_SIGN_IN)
                    mIsResolving = true
                } catch (e: IntentSender.SendIntentException) {
                    Logger.logE("singup handler", "Could not resolve ConnectionResult.$e")
                    mIsResolving = false
                    mGoogleApiClient?.connect()
                }

            } else {
                // Could not resolve the connection result, show the user an
                // error dialog.
            }
        } else {
            // Show the signed-out UI
            //            showSignedOutUI();
        }

    }

    override fun onConnected(bundle: Bundle?) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.
        mShouldResolve = false
        if (result != null) {

        }
    }

    override fun onConnectionSuspended(i: Int) {

    }

    private fun onSignOutClicked() {
        // Clear the default account so that GoogleApiClient will not automatically
        // connect in the future.
//        if (mGoogleApiClient?.isConnected() == true) {
//            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient)
//            mGoogleApiClient?.disconnect()
//        }
        //        showSignedOutUI();
    }
}