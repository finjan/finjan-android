package com.electrolyte.sociallogin.socialLogin

import android.content.Context
import androidx.fragment.app.Fragment
import com.facebook.login.widget.LoginButton

open class FbLoginBtn(context: Context):LoginButton(context){

    override fun setFragment(fragment: androidx.fragment.app.Fragment?) {
        super.setFragment(fragment)
    }

    fun setNewClassMethod(){}

}