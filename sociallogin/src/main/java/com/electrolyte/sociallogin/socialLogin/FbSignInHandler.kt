package com.electrolyte.sociallogin.socialLogin

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.TextUtils
import com.electrolyte.logger.Logger
import com.electrolyte.sociallogin.InitLib
import com.electrolyte.sociallogin.R
import com.electrolyte.sociallogin.socialLogin.models.SocialUserProfile
//import com.electrolyte.utils.ResourceHelper
import com.facebook.*
import com.facebook.internal.LoginAuthorizationType
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import org.json.JSONException
import org.json.JSONObject
import java.util.*

public class FbSignInHandler{

    private val EMAIL = "email"
    private var callbackManager: CallbackManager? = null
    private var loggedIn = false
    private val loginButton: LoginButton? = null
    private var activity: Activity? = null
    private val loginManager: LoginManager? = null
    private var socialUserProfile: SocialUserProfile? = null


    companion object {

        fun LogoutUser(){
            LoginManager.getInstance().logOut()
        }
        fun initLib(context: Context){
//            FacebookSdk.sdkInitialize(context)
        }
    }

    fun setFragment(fragment: androidx.fragment.app.Fragment, btn: FbLoginBtn){
        btn.setFragment(fragment)
    }

    fun handle_initialization(activity: Activity) {
        this.activity = activity
        callbackManager = CallbackManager.Factory.create()
        loggedIn = AccessToken.getCurrentAccessToken() == null || AccessToken.getCurrentAccessToken().isExpired
    }

    interface FBResultListener {
        fun onResult(socialUserProfiles: SocialUserProfile?, success: Boolean, msg: String)
    }



    public fun onClickSignInButton(loginButton: FbLoginBtn, listener: FBResultListener) {
        //        LocalyticsTrackers.Companion.setEFacebook_login();
        Logger.logE("Fb", "login clicked....$loggedIn$loginButton")
        LogoutUser()
        LoginManager.getInstance().logInWithReadPermissions(loginButton.fragment,
                Arrays.asList("public_profile", "email"))

        loginButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Logger.logE("fb", "log in successful")
                getUserDetails(loginResult, listener)
            }

            override fun onCancel() {

                listener.onResult(null, false, InitLib.context.getString(R.string.user_cancel_authentication))
                // App code
                Logger.logE("fb", "cancelled......")
            }

            override fun onError(exception: FacebookException) {
                if (!TextUtils.isEmpty(exception.message)) {
                    listener.onResult(null, false, exception.message as String)
                } else {
                    listener.onResult(null, false, InitLib.context.getString(R.string.error_while_parsing_user_detail))
                }
                // App code
                Logger.logE("fb", "error$exception")
            }
        })
    }

    fun performTaskOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }

    private fun getUserDetails(loginResult: LoginResult, listener: FBResultListener) {
        val data_request = GraphRequest.newMeRequest(
                loginResult.accessToken) { `object`, response ->
            try {
                socialUserProfile = getFacebookData(`object`, loginResult.accessToken.token, loginResult.accessToken.userId)
                listener.onResult(socialUserProfile, true, "")
            } catch (e: Exception) {
                listener.onResult(socialUserProfile, false, InitLib.context.getString(R.string.error_while_parsing_user_detail))
                e.printStackTrace()
            }
        }
        val permission_param = Bundle()
        permission_param.putString("fields", "id,name,email")
        data_request.parameters = permission_param
        data_request.executeAsync()
    }

    private fun getFacebookData(`object`: JSONObject,
                                token: String, userId: String): SocialUserProfile? {
        var fbEmail = ""
        val fbId: String
        var fname = ""
        var lname = ""
        try {
            val id = `object`.getString("id")
            if (`object`.has("email"))
            fbEmail = `object`.getString("email")
            fbId = `object`.getString("id")
            if (`object`.has("first_name"))
                fname = `object`.getString("first_name")
            if (`object`.has("last_name"))
                lname = `object`.getString("last_name")
            if (`object`.has("name")) {
                fname = `object`.getString("name")
            }
            /*if (TextUtils.isEmpty(fbEmail)) {
                fbEmail = "$userId@facebook-social.com"
            }*/
            socialUserProfile = SocialUserProfile()
            socialUserProfile?.setSocialType("fb_id")

            socialUserProfile?.setSocialId(userId)
            //            socialUserProfile.setSocialId("429552714162272");
            socialUserProfile?.setToken_id(token)
            socialUserProfile?.setFirstName(fname)
            socialUserProfile?.setLastName(lname)
            socialUserProfile?.setEmail(fbEmail)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return socialUserProfile
    }
}