# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\ovidiu\tools\android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep class org.apache.commons.logging.**

########## GSON ##########
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
-keep interface com.avira.common.GSONModel
#-keep class * implements com.avira.common.GSONModel { *; }

########## EVENT BUS ##########
-keepclassmembers class ** {
    public void onEvent*(***);
}

# remove this class since it can change the pinning programmatically (is just for testing purposes)
-assumenosideeffects class com.avira.common.backend.oe.OeServerPins {
    public void setOeServerPins(...);
}

-dontwarn com.android.volley.toolbox.**
-dontwarn com.mixpanel.**
-dontwarn com.google.android.gms.**
-dontwarn com.facebook.android.**
-dontwarn de.greenrobot.**

########## MISC ##########
-keepattributes InnerClasses
-keepattributes EnclosingMethod

-assumenosideeffects class android.util.Log {
    public static *** v(...);
    public static *** d(...);
    public static *** i(...);
    public static *** w(...);
    public static *** e(...);
}

# Basic ProGuard rules for Firebase Android SDK 2.0.0+
-keep class com.firebase.** { *; }
-keep class org.apache.** { *; }
-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }
-dontwarn org.apache.**
-dontwarn org.w3c.dom.**
-keep class com.avira.common.security.new_aes.AesCbcWithIntegrity$PrngFixes$* { *; }