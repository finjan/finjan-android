///*******************************************************************************
// * Copyright (C) 1986-2014 Avira GmbH. All rights reserved.
// *******************************************************************************/
//
//package com.avira.common.gcm;
//
//import android.app.IntentService;
//import android.content.Intent;
//
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//
//import java.io.IOException;
//
//public class GCMRegisterService extends IntentService {
//
//    public GCMRegisterService() {
//        super(GCMRegisterService.class.getSimpleName());
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//        GCMRegistration gcmRegistration = GCMRegistration.getInstance();
//
//        try {
//
//            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
//            String regId = gcm.register(GCMRegistration.SENDER_ID);
//            // the update is performed in the service because it's an heavy operation
//            gcmRegistration.updateStoredGCMId(regId);
//            gcmRegistration.onRegistered(getApplicationContext(), regId);
//            // TODO replace with Authentication.updateGCMId
//            //OeRequestManager.updateGcmRegistrationId(this);
//
//        } catch (IOException ex) {
//            gcmRegistration.updateStoredGCMId("");
//            gcmRegistration.onError(getApplicationContext(), ex.getMessage());
//        }
//
//    }
//
//}
