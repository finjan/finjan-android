///*******************************************************************************
// * Copyright (C) 1986-2014 Avira GmbH. All rights reserved.
// *******************************************************************************/
//package com.avira.common.gcm;
//
//import android.app.AlarmManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.os.AsyncTask;
//import android.text.TextUtils;
//import android.util.Log;
//
//import com.avira.common.GeneralPrefs;
//import com.avira.common.database.Settings;
//import com.avira.common.utils.PackageUtilities;
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//
//import java.io.IOException;
//import java.util.ArrayList;
//
///**
// *
// * @author daniela.stamati
// *
// *         Class used for registering and unregistering to GCM service. UPDATES: The only case when re-registering the
// *         app is mandatory is on updates (app version changes).
// *
// */
//
//public class GCMRegistration {
//
//    private static final int REGISTER_ALARM_ID = 2586423;
//
//    // no network coverage
//    public static final String ERROR_SERVICE_NOT_AVAILABLE = "SERVICE_NOT_AVAILABLE";
//
//    public static final String LOG_TAG = GCMRegistration.class.getSimpleName();
//    public static final String SENDER_ID = "550938343285";
//    private static final int MAX_NUMBER_OF_REGISTER_ATTEMPTS = 5;
//    private static final int MIN_BACKOFF_INTERVAL = 3000; // 3 secs
//
//    private static GCMRegistration INSTANCE;
//    private final ArrayList<IGcmDelegate> mGcmRegistrationDelegateList;
//    private String mRegId;
//    private int mRemainingRegisterAttempts;
//    private long mBackoffMillis;
//    public boolean mRegisterRequestOngoing;
//
//    public interface IGcmDelegate {
//        void onSuccess();
//
//        void onError(String errorId);
//    }
//
//    private GCMRegistration() {
//        mRegId = getStoredGCMId();
//        mRemainingRegisterAttempts = MAX_NUMBER_OF_REGISTER_ATTEMPTS;
//        mGcmRegistrationDelegateList = new ArrayList<>();
//        mBackoffMillis = MIN_BACKOFF_INTERVAL;
//        mRegisterRequestOngoing = false;
//    }
//
//    public static GCMRegistration getInstance() {
//        if (INSTANCE == null) {
//            INSTANCE = new GCMRegistration();
//        }
//
//        return INSTANCE;
//    }
//
//    public synchronized void register(Context context, IGcmDelegate gcmRegistrationDelegate) {
//        Log.d(LOG_TAG, "register request issued");
//
//        if (gcmRegistrationDelegate != null) {
//            mGcmRegistrationDelegateList.add(gcmRegistrationDelegate);
//        }
//
//        // 1. check if an register request was already issued
//        if (mRegisterRequestOngoing) {
//            return;
//        }
//
//        Context appContext = context.getApplicationContext();
//
//        // 2. check if the app has been updated
//        if (appNeedsNewRegId(appContext)) {
//            if (isRegistered()) {
//                //the app will re-register on unregister success
//                unregister(appContext);
//            } else {
//                startGcmRegisterService(appContext);
//            }
//        } else {
//            if (isRegistered()) {
//                notifyRegistrationDelegates(null);
//            } else {
//                startGcmRegisterService(appContext);
//
//            }
//        }
//
//    }
//
//    private void startGcmRegisterService(Context context) {
//        Intent serviceIntent = new Intent(context, GCMRegisterService.class);
//        context.startService(serviceIntent);
//        mRegisterRequestOngoing = true;
//        mRemainingRegisterAttempts--;
//    }
//
//    private void scheduleGcmRegisterService(Context context) {
//
//        Log.d(LOG_TAG, "remaining register attepts: " + mRemainingRegisterAttempts);
//        Log.d(LOG_TAG, "Current backoff: " + mBackoffMillis);
//
//        Intent intent = new Intent(context, GCMRegisterService.class);
//        PendingIntent scanAlarmIntent = PendingIntent.getService(context, REGISTER_ALARM_ID,
//                intent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + mBackoffMillis, scanAlarmIntent);
//
//        mBackoffMillis *= 2;
//        mRemainingRegisterAttempts--;
//    }
//
//    public synchronized boolean isRegistered() {
//        // mRegId is fetched in the constructor
//        if (TextUtils.isEmpty(mRegId)) {
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * According to Google documentation every time the app updates it needs to be re-registered to GCM. We shall
//     * determine if the app updated using the stored version number.
//     */
//    private boolean appNeedsNewRegId(Context context) {
//        return PackageUtilities.getVersionCode(context) == GeneralPrefs.getGCMLastVersionCode(context);
//    }
//
//
//    public void unregister(final Context context) {
//        if (!isRegistered()) {
//            return;
//        }
//        new AsyncTask<Object, Object, Object>() {
//            @Override
//            protected String doInBackground(Object... params) {
//                try {
//
//                    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
//                    gcm.unregister();
//                    updateStoredGCMId("");
//                    onUnregistered(context);
//
//                } catch (IOException ex) {
//                    Log.e(LOG_TAG, "Unregister failed with error:", ex);
//                }
//
//                return null;
//
//            }
//        }.execute(null, null, null);
//    }
//
//    public void updateStoredGCMId(String gcmId) {
//        Settings.writeGCMId(gcmId);
//    }
//
//    public synchronized String getGCMId() {
//        if (!TextUtils.isEmpty(mRegId)) {
//            return mRegId;
//        } else {
//            return getStoredGCMId();
//        }
//    }
//
//    // this should remain synchronous
//    private String getStoredGCMId() {
//        return Settings.readGCMId();
//    }
//
//    /**
//     * Handle delegate callbacks and update registration state
//     *
//     * @param errorMessage
//     */
//    protected void onError(Context context, String errorMessage) {
//        if (ERROR_SERVICE_NOT_AVAILABLE.equalsIgnoreCase(errorMessage) && mRemainingRegisterAttempts > 0) {
//            scheduleGcmRegisterService(context.getApplicationContext());
//        } else {
//            notifyRegistrationDelegates(errorMessage);
//            mRegId = "";
//            mRemainingRegisterAttempts = MAX_NUMBER_OF_REGISTER_ATTEMPTS;
//            mBackoffMillis = MIN_BACKOFF_INTERVAL;
//        }
//
//        mRegisterRequestOngoing = false;
//        Log.i(LOG_TAG, "GCM registration failed with error: " + (errorMessage != null ? errorMessage : ""));
//    }
//
//    protected void onRegistered(Context context, String regId) {
//        mRegId = regId;
//        mRemainingRegisterAttempts = MAX_NUMBER_OF_REGISTER_ATTEMPTS;
//        mBackoffMillis = MIN_BACKOFF_INTERVAL;
//        notifyRegistrationDelegates(null);
//        GeneralPrefs.setGCMLastVersionCode(context, PackageUtilities.getVersionCode(context));
//        mRegisterRequestOngoing = false;
//        Log.i(LOG_TAG, "Device registered, registration ID=" + regId);
//    }
//
//    /**
//     * the app should always be registered to GCM. Unregister is called only so that the GCM server will delete the
//     * track of the previous regId
//     */
//    private void onUnregistered(Context context) {
//        mRegId = "";
//        Log.i(LOG_TAG, "Device unregistered");
//        startGcmRegisterService(context);
//    }
//
//    private void notifyRegistrationDelegates(String errorMessage) {
//
//        if (mGcmRegistrationDelegateList.size() > 0) {
//            for (IGcmDelegate gcmRegistrationDelegate : mGcmRegistrationDelegateList) {
//                if (TextUtils.isEmpty(errorMessage)) {
//                    gcmRegistrationDelegate.onSuccess();
//                } else {
//                    gcmRegistrationDelegate.onError(errorMessage);
//                }
//
//            }
//
//            mGcmRegistrationDelegateList.clear();
//        }
//
//    }
//}
