package com.avira.common.events;

/**
 * standard authentication error event
 *
 * @author ovidiu.buleandra
 * @since 23.02.2016
 */
public class AuthFailEvent {
    private int mStatusCode;
    private String mErrorMessage;

    public AuthFailEvent(int status, String message) {
        mStatusCode = status;
        mErrorMessage = message;
    }

    public int getStatusCode() { return mStatusCode; }
    public String getErrorMessage() { return mErrorMessage; }

}
