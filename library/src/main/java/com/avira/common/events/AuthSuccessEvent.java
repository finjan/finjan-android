package com.avira.common.events;

import com.avira.common.authentication.models.LoginResponse_1;
import com.avira.common.authentication.models.Subscription;
import com.avira.common.authentication.models.User;

/**
 * standard authentication success event
 *
 * @author ovidiu.buleandra
 * @since 23.02.2016
 */
public class AuthSuccessEvent {

    private User mUser;
    private Subscription mSubscription;
    private LoginResponse_1 loginResponse_1;

    public AuthSuccessEvent(User user, Subscription subscription) {
        mUser = user;
        mSubscription = subscription;
    }
    public AuthSuccessEvent(LoginResponse_1 loginResponse_1){
        this.loginResponse_1=loginResponse_1;
    }

    public LoginResponse_1 getLoginResponse_1() {
        return loginResponse_1;
    }

    public User getUser() { return mUser; }
    public Subscription getSubscription() { return mSubscription; }
}
