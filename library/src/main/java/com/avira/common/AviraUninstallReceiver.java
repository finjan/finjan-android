package com.avira.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author ovidiu.buleandra
 * @since 30.09.2015
 */
public class AviraUninstallReceiver extends BroadcastReceiver {

    private static final String PACKAGE_FILTER = "com.avira.";

    private static final String UNINSTALL_EVENT = "App_Uninstall";
    private static final String EXTRA_SOURCE = "source";
    private static final String EXTRA_APK = "apk";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_PACKAGE_REMOVED.equals(intent.getAction())) {
            String packageName = intent.getData().getEncodedSchemeSpecificPart();
            boolean isReplacing = intent.getBooleanExtra(Intent.EXTRA_REPLACING, false);

            if(!isReplacing && packageName.startsWith(PACKAGE_FILTER)) {
                JSONObject extra = new JSONObject();
                try {
                    extra.put(EXTRA_SOURCE, getAppLabel(context));
                    extra.put(EXTRA_APK, packageName);

                    // extra stuff to do
                    onReceiveExtraStep(context, packageName);
                } catch (JSONException e) {
                    // ignore
                }
            }
        }
    }

    /**
     * ask the package manager to get the apps label (if it fails it will return the package name)
     * @param context given context
     * @return app display name or package name
     */
    private String getAppLabel(Context context) {
        return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    }

    /**
     * method to override in child classes in case extra work is needed when a specific package is uninstalled
     * not made abstract on purpose
     * @param packageName
     */
    protected void onReceiveExtraStep(Context context, String packageName) {

    }
}
