package com.avira.common.database;

public class ConstantValue {
    public static final String SETTINGS_EMPTY = "";
    public static final int SETTINGS_INVALID_INTEGER = -1;
    public static final String SETTINGS_ENABLED = "1";
    public static final String SETTINGS_DISABLED = "0";
}
