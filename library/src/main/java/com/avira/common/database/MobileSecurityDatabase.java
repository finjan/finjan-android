/*******************************************************************************
 * Copyright (C) 1986-2014 Avira GmbH. All rights reserved.
 *******************************************************************************/

package com.avira.common.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.avira.common.id.HardwareId;

import java.io.File;

/**
 * Class to manipulate application database
 * <p/>
 * comes with a "settings" table to hold common application settings (key-value pairs) and the encryption seed
 */
public class MobileSecurityDatabase {
    private static final String TAG = MobileSecurityDatabase.class.getSimpleName();
    private static final String DATABASE_NAME = "MobileSecurityDb";
    public static final String RESET_APP_MESSAGE = "com.avira.common.RESET_APP";


    private static MobileSecurityDatabase INSTANCE;

    private final MobileSecurityDatabaseHelper mMobileSecurityDatabaseHelper;
    /**
     * application context to use for encryption provider and helper
     */
    private final Context mContext;
    private final EncryptionProvider mEncryptionProvider;
    private SQLiteDatabase mDatabase;

    /**
     * Constructor for MobileSecurityDatabase
     *
     * @param customInitHandler handler used in executing custom scripts on create and upgrade scenarios for the db
     */
    private MobileSecurityDatabase(Context context, String deviceId, MobileSecurityDatabaseHelper.CustomInitialization customInitHandler) {
        // make sure we hold the application context so no leakage can occur
        mContext = context.getApplicationContext();

        mMobileSecurityDatabaseHelper = new MobileSecurityDatabaseHelper(mContext, DATABASE_NAME, customInitHandler);
        mEncryptionProvider = EncryptionProvider.init(deviceId);
    }

    /**
     * must be called method to initialize the database and provide extra scripts for initialization
     *
     * @param context valid context
     * @param deviceId generated deviceid to be used as salt inside the {@link EncryptionProvider}
     * @param customInitHandler handler to provide access to the onCreate and onUpgrade events
     *                          for custom scripts execution
     */
    public static void init(Context context, String deviceId, MobileSecurityDatabaseHelper.CustomInitialization customInitHandler) {
        Log.d(TAG, "init");
        INSTANCE = new MobileSecurityDatabase(context, deviceId, customInitHandler);
        EncryptionProvider.initSeed();
    }

    /**
     * must be called method to initialize the database and provide extra scripts for initialization
     *
     * @param context valid context
     * @param customInitHandler handler to provide access to the onCreate and onUpgrade events
     *                          for custom scripts execution
     */
    public static void init(Context context,MobileSecurityDatabaseHelper.CustomInitialization customInitHandler){
        init(context, HardwareId.getSalt(context), customInitHandler);
    }



    /**
     * Retrieve singleton MobileSecurityDatabase instance
     *
     * @return Initialized singleton MobileSecurityDatabase instance
     */
    public synchronized static MobileSecurityDatabase getInstance() {
        if (INSTANCE == null) {
            throw new RuntimeException("database must be first be initialized by calling 'init'");
        }
        return INSTANCE;
    }

    public synchronized boolean isOpen(){
        return mDatabase.isOpen();
    }

    /**
     * Convenience method to create and insert a column into a database table.
     *
     * @param database    the target SQLiteDatabase
     * @param table       table definition
     * @param columnName  the column name to insert
     * @param columnValue an initial String value for the column
     */
    public static void createColumn(SQLiteDatabase database, ITableDefinition table, String columnName,
                                    String columnValue) {
        createColumn(database, table, columnName, columnValue, true);
    }

    /**
     * Convenience method to create and insert a column into a database table.
     *
     * @param database the target SQLiteDatabase
     * @param table table definition
     * @param columnName the column name to insert
     * @param columnValue an initial String value for the column
     * @param encryptValue true to encrypt column value, otherwise false.
     */
    public static void createColumn(SQLiteDatabase database, ITableDefinition table, String columnName,
                                    String columnValue, boolean encryptValue) {
        if (encryptValue) {
            columnValue = EncryptionProvider.getInstance().encrypt(columnValue, columnName);
        }

        ContentValues contentValue = new ContentValues();
        contentValue.put(table.getSettingLabel(), columnName);
        contentValue.put(table.getValueLabel(), columnValue);

        database.insert(table.getName(), null, contentValue);
    }

    /**
     * Try to close the database if it has been created
     */
    public synchronized static void closeDatabase() {
        if (INSTANCE != null) {
            Log.d(TAG, "closing database on shutdown");
            INSTANCE.closeDatabaseHelper();
        }
    }

    public String getEncryptedValue(String key, String value) {
        return mEncryptionProvider.encrypt(value, key);
    }

    public String getDecryptedValue(String key, String value) {
        String decrypted = mEncryptionProvider.decrypt(value, key);

        // reset the app in the case of decryption failed
        if (decrypted == null) {
            sendResetSignal();
        }
        return decrypted;
    }

    @Override
    protected void finalize() throws Throwable {
        if (mDatabase != null) {
            mDatabase.close();
        }
        super.finalize();
    }

    public SQLiteDatabase getDatabase() {
        if (mDatabase == null || !mDatabase.isOpen()) {
            mDatabase = mMobileSecurityDatabaseHelper.getWritableDatabase();
        }
        return mDatabase;
    }


    public boolean readBooleanSetting(String settingName, ITableDefinition table, boolean defaultValue){
        return readSetting(settingName, table, String.valueOf(defaultValue)).equalsIgnoreCase(String.valueOf(true));
    }


    /**
     * Read application setting value from the database.
     *
     * @param settingName Name of the setting
     * @return Value of the setting, as String
     */
    public String readSetting(String settingName, ITableDefinition table, String defaultValue) {
        String value = defaultValue;
        Cursor cursor = null;

        if (table == null) {
            return defaultValue;
        }

        try {
            cursor = getDatabase().query(table.getName(),
                    new String[]{table.getSettingLabel(), table.getValueLabel()},
                    table.getSettingLabel() + "='" + settingName + "'", null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    value = cursor.getString(cursor.getColumnIndex(table.getValueLabel()));

                    if (!Settings.SETTINGS_REGISTERED_PATH.equals(settingName) && value != null) {
                        value = EncryptionProvider.getInstance().decrypt(value, settingName);
                        // reset the app if data corrupted
                        if (value == null) {
                            sendResetSignal();
                        }
                    }
                }
            }
        } catch (SQLiteException e) {
            // If no WIPE job declared
            Log.e(TAG, "Failed to read settings", e);
        }

        if (cursor != null) {
            cursor.close();
        }

        // In case Exception is thrown during decryption, set back to Default Value
        if (value == null) {
            value = defaultValue;
        }

        return value;
    }

    /**
     * Write application setting to the database.
     *
     * @param settingName  Name of the setting
     * @param settingValue Value of the setting
     */
    public int writeSetting(String settingName, boolean settingValue, ITableDefinition table) {
        return writeSetting(settingName, String.valueOf(settingValue), table);
    }

    /**
     * Write application setting to the database.
     *
     * @param settingName  Name of the setting
     * @param settingValue Value of the setting
     */
    public int writeSetting(String settingName, String settingValue, ITableDefinition table) {
        if (settingName == null || table == null) {
            throw new IllegalArgumentException();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(table.getSettingLabel(), settingName);
        contentValues.put(table.getValueLabel(), EncryptionProvider.getInstance().encrypt(settingValue, settingName));
        int rowAffected = 0;

        try {
            rowAffected = getDatabase().update(table.getName(), contentValues, table.getSettingLabel() + "=?",
                    new String[]{settingName});

            if (rowAffected == 0) {
                long result = getDatabase().insert(table.getName(), null, contentValues);
                if (result != -1) {
                    rowAffected = 1;
                }
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "failed to write setting", e);
        }

        return rowAffected;
    }

    /**
     * Check if a database table exists.
     *
     * @param tableName target table in database
     * @return boolean value true if table exists, else return false
     */
    public boolean isDatabaseTableExists(String tableName) {
        return mMobileSecurityDatabaseHelper.isTableExists(tableName, getDatabase());
    }

    /**
     * Method to delete all database tables.
     *
     * @return boolean value true if no errors occur, else return false
     */
    public boolean tryDeleteAllDatabaseTables() {
        return mMobileSecurityDatabaseHelper.tryDeleteAllDatabaseTables(getDatabase());
    }

    /**
     * Try to delete a database table
     *
     * @param tableName target table in database
     * @return boolean value true if table deletion succeeds, else return false
     */
    public boolean tryDeleteDatabaseTable(String tableName) {
        return mMobileSecurityDatabaseHelper.tryDeleteDatabaseTable(tableName, getDatabase());
    }

    /**
     * Try to delete entire database
     *
     * @return boolean value true if database deletion succeeds, else return false
     */
    public boolean tryDeleteDatabase() {
        if (mContext.deleteDatabase(DATABASE_NAME)) {
            getDatabase().close();
            INSTANCE = null;
            return true;
        }
        return false;
    }

    /**
     * function to remove singleton database instance
     * (used in test automation)
     */
    public static void deleteInstance(){
        if (INSTANCE != null) {
            closeDatabase();
        }
        INSTANCE = null;
    }
    private void closeDatabaseHelper() {
        mMobileSecurityDatabaseHelper.close();
    }


    /**
     * sends a local message that it detected a corruption in the database that signals a tampering try
     * the receiver should reset all settings inside the compromised application
     */
    private void sendResetSignal() {
        Intent resetIntent = new Intent(RESET_APP_MESSAGE);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(resetIntent);
    }

    public static boolean exists(Context context) {
        File dbFile = context.getDatabasePath(DATABASE_NAME);
        return dbFile.exists();
    }

    public static void deleteDatabase(Context context) {
        context.deleteDatabase(DATABASE_NAME);
    }
}
