package com.avira.common.database;

/*
 * Table definition interface, that compliance to MobileSecurityDatabase update 
 */
public interface ITableDefinition {
    /*
     * Retrieve table name
     *
     * @return Table name
     */
    String getName();

    /*
     * Retrieve setting label name
     *
     * @return Setting label
     */
    String getSettingLabel();

    /*
     * Retrieve value label name
     *
     * @return Value label
     */
    String getValueLabel();
}
