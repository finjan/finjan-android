package com.avira.common.database;

import android.database.sqlite.SQLiteDatabase;

import com.avira.common.deviceadmin.DeviceAdminManager;

//import com.finjan.securebrowser.a_vpn.licensing.models.restful.License;
//import com.finjan.securebrowser.a_vpn.licensing.models.restful.LicenseArray;

/**
 * common settings table in secure database (uses a key-value pair data structure)
 * should be used to store sensitive data
 *
 * @author ovidiu.buleandra
 * @since 24.08.2015
 */
public class Settings {
    public static final String SETTINGS_REGISTERED_PATH = "settingRegisteredPath";
    public static final String SETTINGS_KEYS = "settingKeys";
    public static final String SETTINGS_REGISTERED_SERVER_DEVICE_ID = "settingRegisteredServerDeviceId";
    public static final String SETTINGS_DEVICE_ADMIN_VERSION = "settingDeviceAdminVersion";
    public static final String SETTINGS_GCM_ID = "settingGCMId";
    public static final String SETTINGS_USER_PROFILE = "settingUserProfile";

    //    ~Anurag
    public static final String SETTINGS_USER_EMAIL = "user_email";
    public static final String SETTINGS_USER_PASSWORD = "user_password";
    public static final String SETTINGS_USER_TOKEN= "access_token";
    public static final String SETTINGS_USER_REFRESH_TOKEN= "refresh_token";


    public static final String SETTINGS_LICENSES = "settingLicenses";

    protected static final String TABLE_NAME = "settings";
    protected static final String SETTINGS_NAME = "settingName";
    protected static final String SETTINGS_VALUE = "settingValue";
    public static final String CREATE_TABLE = "create table " + TABLE_NAME + " (" + SETTINGS_NAME + " text not null, "
            + SETTINGS_VALUE + " blob not null);";
    protected static final ITableDefinition TABLE_DEFINITION = new ITableDefinition() {
        @Override
        public String getName() {
            return TABLE_NAME;
        }

        @Override
        public String getSettingLabel() {
            return SETTINGS_NAME;
        }

        @Override
        public String getValueLabel() {
            return SETTINGS_VALUE;
        }
    };

    public static boolean isAvailable() {
        try {
            if (SecureDB.isInitialized()) {
                return SecureDB.getInstance().isOpen();
            } else {
                return MobileSecurityDatabase.getInstance().isOpen();
            }
        } catch (RuntimeException e) {
            //the db was not initialized
            return false;
        }
    }


    /**
     * Write application setting to the database
     *
     * @param settingName  Name of the setting
     * @param settingValue Value of the setting
     */
    public static int writeSetting(String settingName, String settingValue) {
        if (SecureDB.isInitialized()) {
            return SecureDB.getInstance().writeSetting(settingName, settingValue, TABLE_DEFINITION);
        } else {
            return MobileSecurityDatabase.getInstance().writeSetting(settingName, settingValue, TABLE_DEFINITION);
        }
    }

    /**
     * Read application setting value from the database
     *
     * @param settingName Name of the setting
     * @return Value of the setting, as String
     */
    public static String readSetting(String settingName, String defaultValue) {
        if (SecureDB.isInitialized()) {
            return SecureDB.getInstance().readSetting(settingName, TABLE_DEFINITION, defaultValue);
        } else {
            return MobileSecurityDatabase.getInstance().readSetting
                    (settingName, TABLE_DEFINITION, defaultValue);
        }
    }

    /**
     * Only for New AES encryption, do not need be backwards compatible
     *
     * @return
     */
    public static byte[] readKey() {
        return SecureDB.getInstance().readKey();
    }

    /**
     * Only for New AES encryption, do not need be backwards compatible
     *
     * @return
     */
    public static void writeKey(byte[] value) {
        SecureDB.getInstance().writeKey(value);
    }

    public static String readDeviceId() {
        return readSetting(SETTINGS_REGISTERED_SERVER_DEVICE_ID, ConstantValue.SETTINGS_EMPTY);
    }

    public static void saveDeviceId(String deviceId) {
        writeSetting(SETTINGS_REGISTERED_SERVER_DEVICE_ID, deviceId);
    }


    public static String readDeviceAdminVersion() {
        return readSetting(SETTINGS_DEVICE_ADMIN_VERSION, DeviceAdminManager.NEW_DEVICE_ADMIN_VERSION);
    }

    public static void writeDeviceAdminVersion(String version) {
        writeSetting(SETTINGS_DEVICE_ADMIN_VERSION, version);
    }

    public static String readGCMId() {
        return readSetting(SETTINGS_GCM_ID, ConstantValue.SETTINGS_EMPTY);
    }

    public static void writeGCMId(String value) {
        writeSetting(SETTINGS_GCM_ID, value);
    }

    public static void saveUserProfile(String userProfileJson) {
        writeSetting(SETTINGS_USER_PROFILE, userProfileJson);
    }

    public static String readUserProfile() {
        return readSetting(SETTINGS_USER_PROFILE, ConstantValue.SETTINGS_EMPTY);
    }

    public static void writeEmail(String email) {
        writeSetting(SETTINGS_USER_EMAIL, email);
    }

    public static String readEmail() {
        return readSetting(SETTINGS_USER_EMAIL, ConstantValue.SETTINGS_EMPTY);
    }

    public static void writePassword(String password) {
        writeSetting(SETTINGS_USER_PASSWORD, password);
    }

    public static String readPassword() {
        return readSetting(SETTINGS_USER_PASSWORD, ConstantValue.SETTINGS_EMPTY);
    }

    public static void writeAccessToken(String accessToken) {
        writeSetting(SETTINGS_USER_TOKEN, accessToken);
    }

    public static String readAccessToken() {
        return readSetting(SETTINGS_USER_TOKEN, ConstantValue.SETTINGS_EMPTY);
    }
    public static void writeRefreshToken(String refreshToken) {
        writeSetting(SETTINGS_USER_REFRESH_TOKEN, refreshToken);
    }

    public static String readRefershToken() {
        return readSetting(SETTINGS_USER_REFRESH_TOKEN, ConstantValue.SETTINGS_EMPTY);
    }


//    public static void saveLicenses(List<License> licenses) {
//        LicenseArray array = new LicenseArray();
//        array.setLicenses(licenses);
//        writeSetting(SETTINGS_LICENSES, new Gson().toJson(array));
//    }

//    public static List<License> readLicenses() {
//        List<License> result;
//
//        String licensesJson = Settings.readSetting(SETTINGS_LICENSES, ConstantValue.SETTINGS_EMPTY);
//        if (!TextUtils.isEmpty(licensesJson)) {
//            LicenseArray licenseArray = new Gson().fromJson(licensesJson, LicenseArray.class);
//            result = licenseArray.getLicenses();
//        } else {
//            result = new ArrayList<>();
//        }
//
//        return result;
//
//    }

    public static void deleteTable() {
        if (SecureDB.isInitialized()) {
            SecureDB.getInstance().getDatabase().delete(TABLE_NAME, null, null);
        } else {
            MobileSecurityDatabase.getInstance().getDatabase().delete(TABLE_NAME, null, null);
        }
    }

    public static void init(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }
}
