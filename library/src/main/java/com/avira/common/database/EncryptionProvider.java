/*******************************************************************************
 * Copyright (C) 1986-2014 Avira GmbH. All rights reserved.
 *******************************************************************************/

package com.avira.common.database;

import android.text.TextUtils;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.concurrent.Semaphore;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionProvider {
    private static final String TAG = EncryptionProvider.class.getSimpleName();

    private final static int HEX_RADIX = 16;
    private final static int KEY_LENGTH = 16;

    public final static String ONE_TIME_SECURE_RANDOM = EncryptionProvider.bytesToHex(SecureRandom
            .getSeed(EncryptionProvider.KEY_LENGTH));

    private static EncryptionProvider INSTANCE;
    private static String sSecureRandom;

    private final String CHAR_SET = "UTF-8";
    private final Semaphore mSemaphore;
    private Cipher mCipher;
    private SecretKeySpec mSecretKeySpec;
    private byte[] mEncryptionBlob = null;
    private MessageDigest mMessageDigest;
    private String mDeviceId;

    private EncryptionProvider(String deviceId) {
        mSemaphore = new Semaphore(1);
        mDeviceId = deviceId;

        try {
            mCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            mMessageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "NoSuchAlgorithmException", e);
        } catch (NoSuchPaddingException e) {
            Log.e(TAG, "NoSuchPaddingException", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public static EncryptionProvider init(String deviceId) {
        Log.d(TAG, "init");
        INSTANCE = new EncryptionProvider(deviceId);
        return INSTANCE;
    }

    public synchronized static EncryptionProvider getInstance() {
        if (INSTANCE == null) {
            throw new RuntimeException("encryption provider must be first be initialized by calling 'init'");
        }
        return INSTANCE;
    }

    public static void initSeed() {
        String secureRandomSeed = MobileSecurityDatabase.getInstance().readSetting(
                Settings.SETTINGS_REGISTERED_PATH, Settings.TABLE_DEFINITION, ConstantValue.SETTINGS_EMPTY);

        if (TextUtils.isEmpty(secureRandomSeed)) { // AAMA-4229
            Log.d(TAG, "initSeed");

            // generate new seed & save to db
            secureRandomSeed = ONE_TIME_SECURE_RANDOM;
            MobileSecurityDatabase.getInstance().writeSetting(
                    Settings.SETTINGS_REGISTERED_PATH, secureRandomSeed, Settings.TABLE_DEFINITION);
        }

        Log.d(TAG, "initSeed secureRandomSeed " + secureRandomSeed);
        sSecureRandom = secureRandomSeed;
    }

    public static String getSecureRandomSeed() {
        if (TextUtils.isEmpty(sSecureRandom)) {
            initSeed(); // AAMA-4270
        }
        Log.d(TAG, "getSecureRandomSeed " + sSecureRandom);
        return sSecureRandom;
    }

    // Current implementation is performance optimized,
    // almost identical with org.apache.commons.codec.binary.Hex.encodeHex method
    public static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        }

        final char hexArray[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        final int len = data.length;
        char[] str = new char[len * 2]; // faster than StringBuilder

        for (int i = 0, j = 0; i < len; i++) {
            str[j++] = hexArray[(data[i] & 0xF0) >>> 4]; // faster than java.lang.Integer.toHexString
            str[j++] = hexArray[data[i] & 0x0F];
        }

        return new String(str);
    }

    public static byte[] hexToBytes(String str) {
        if ((str == null) || (str.length() < 2)) {
            return null;
        } else {
            int len = str.length();
            byte[] buffer = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                buffer[i / 2] = (byte) ((Character.digit(str.charAt(i), HEX_RADIX) << 4) + Character.digit(
                        str.charAt(i + 1), HEX_RADIX));
            }
            return buffer;
        }
    }

    private byte[] getKey() {
        byte[] key = null;

        if (mEncryptionBlob != null) {
            key = mEncryptionBlob;
        } else {
            final String randomSeed = getSecureRandomSeed();

            try {
                String concatSalt = randomSeed + mDeviceId;
                mMessageDigest.update(concatSalt.getBytes(CHAR_SET));

                // this code is weird - looks like duplicated logic branches
//                if (randomSeed.equals(ONE_TIME_SECURE_RANDOM)) {
//                    key = new byte[KEY_LENGTH];
//                    System.arraycopy(mMessageDigest.digest(), 0, key, 0, key.length);
//                } else {
                mEncryptionBlob = new byte[KEY_LENGTH];
                System.arraycopy(mMessageDigest.digest(), 0, mEncryptionBlob, 0, mEncryptionBlob.length);
                key = mEncryptionBlob;
//                }
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "UnsupportedEncodingException", e);
            }
        }
        return key;
    }

    private byte[] getRawKey() throws Exception {
        final int KEY_SIZE = 128;
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        SecureRandom secureRandom = null;
        if (android.os.Build.VERSION.SDK_INT > 16) {
            try {
                // use old crypto provider on Android 4.2 Jelly Bean and up
                secureRandom = SecureRandom.getInstance("SHA1PRNG", "Crypto");
            } catch (NoSuchProviderException e) {
                // failover to using default provider
                secureRandom = SecureRandom.getInstance("SHA1PRNG");
            }
        } else {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
        }
        // TODO: not recommended
        // http://android-developers.blogspot.de/2013/02/using-cryptography-to-store-credentials.html
        secureRandom.setSeed(getKey());

        keyGenerator.init(KEY_SIZE, secureRandom);
        SecretKey secretKey = keyGenerator.generateKey();
        return secretKey.getEncoded();
    }

    private byte[] padKey(String source, int paddingLength) {
        byte[] key = null;
        try {
            mMessageDigest.update(source.getBytes(CHAR_SET));
            key = new byte[paddingLength];
            System.arraycopy(mMessageDigest.digest(), 0, key, 0, key.length);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "UnsupportedEncodingException", e);
        }

        return key;
    }

    public String encrypt(byte[] input, String key) {
        String result = null;
        byte[] iv = null;
        byte[] encrypted = null;

        if (input == null) {
            input = "".getBytes();
        }

        try {
            mSemaphore.acquire();
            iv = padKey(key, KEY_LENGTH);
            mCipher.init(Cipher.ENCRYPT_MODE, getSecretKeySpec(), new IvParameterSpec(iv));
            encrypted = mCipher.doFinal(input);
        } catch (IllegalBlockSizeException e) {
            Log.e(TAG, "IllegalBlockSizeException", e);
        } catch (BadPaddingException e) {
            Log.e(TAG, "BadPaddingException", e);
        } catch (InvalidKeyException e) {
            Log.e(TAG, "InvalidKeyException", e);
        } catch (InterruptedException e) {
            Log.e(TAG, "InterruptedException", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        result = bytesToHex(encrypted);
        mSemaphore.release();
        return result;
    }

    public String encrypt(String input, String key) {
        String result = null;
        if (Settings.SETTINGS_REGISTERED_PATH.equals(key)) {
            Log.i(TAG, "Secure Random equals " + key);
            return input;
        }

        if (input == null) {
            input = "";
        }

        try {
            result = encrypt(input.getBytes(CHAR_SET), key);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "UnsupportedEncodingException", e);
        }

        return result;
    }

    private SecretKeySpec getSecretKeySpec() {
        if (mSecretKeySpec == null) {
            try {
                mSecretKeySpec = new SecretKeySpec(getRawKey(), "AES");
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }

        return mSecretKeySpec;
    }

    public void updateSecretKeySpec() {
        try {
            mSecretKeySpec = new SecretKeySpec(getRawKey(), "AES");
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public byte[] decryptByte(String input, String key) {
        byte[] decrypted = null;
        byte[] iv = null;
        if (input == null) {
            return null;
        }

        try {
            mSemaphore.acquire();

            iv = padKey(key, KEY_LENGTH);
            mCipher.init(Cipher.DECRYPT_MODE, getSecretKeySpec(), new IvParameterSpec(iv));
            byte[] bytes = hexToBytes(input);
            if (bytes != null) {
                decrypted = mCipher.doFinal(bytes);
            }
        } catch (InvalidKeyException e) {
            Log.e(TAG, "InvalidKeyException", e);
        } catch (IllegalBlockSizeException e) {
            Log.e(TAG, "IllegalBlockSizeException", e);
        } catch (BadPaddingException e) {
            Log.e(TAG, "BadPaddingException", e);
        } catch (InvalidAlgorithmParameterException e) {
            Log.e(TAG, "InvalidAlgorithmParameterException", e);
        } catch (InterruptedException e) {
            Log.e(TAG, "InterruptedException", e);
        } finally {
            mSemaphore.release();
        }

        return decrypted;
    }

    public String decrypt(String input, String key) {
        byte[] decrypted = null;
        String result = null;

        if (key == null || input == null || input.length() == 0 || key.length() == 0) {
            return null;
        }

        decrypted = decryptByte(input, key);

        try {
            if (decrypted != null) {
                result = new String(decrypted, CHAR_SET);
            }
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "UnsupportedEncodingException", e);
        }

        return result;
    }
}
