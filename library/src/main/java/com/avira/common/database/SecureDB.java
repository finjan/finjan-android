package com.avira.common.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.avira.common.id.HardwareId;
import com.avira.common.security.new_aes.AesCbcWithIntegrity;
import com.avira.common.security.new_aes.EncryptionProviderAes;
import com.avira.common.utils.HashUtility;

import java.util.Random;

/**
 * Created by Illia.Klimov on 6/23/2016.
 */

public class SecureDB {
    private static final String TAG = SecureDB.class.getSimpleName();
    private static final String DATABASE_NAME = "MobileSecurity.db";
    public static final String RESET_APP_MESSAGE = "com.avira.common.RESET_APP";


    private static SecureDB INSTANCE;

    private final MobileSecurityDatabaseHelper mMobileSecurityDatabaseHelper;
    /**
     * application context to use for encryption provider and helper
     */
    private final Context mContext;
    private final EncryptionProviderAes mEncryptionProvider;
    private SQLiteDatabase mDatabase;

    /**
     * Constructor for MobileSecurityDatabase
     *
     * @param customInitHandler handler used in executing custom scripts on create and upgrade scenarios for the db
     */
    private SecureDB(Context context, MobileSecurityDatabaseHelper.CustomInitialization customInitHandler) {
        // make sure we hold the application context so no leakage can occur
        mContext = context.getApplicationContext();
        mMobileSecurityDatabaseHelper = new MobileSecurityDatabaseHelper(mContext, DATABASE_NAME, customInitHandler);
        mEncryptionProvider = EncryptionProviderAes.getInstance();
    }

    /**
     * fills DB with encrypted data
     */
    private void randomize() {
        int entriesCount = 30;
        Random random = new Random(System.currentTimeMillis());
        String settingsTitle;
        for (int i = 0; i < entriesCount; i++) {
            settingsTitle = String.valueOf(random.nextGaussian()) + String.valueOf(System.currentTimeMillis()) + "i";
            writeSetting(settingsTitle, String.valueOf(random.nextGaussian()), Settings.TABLE_DEFINITION);
        }
    }

    private void migrateToAesEncryption(Context context, String partialSeed) {
        boolean doesDatabaseExist = MobileSecurityDatabase.exists(context);
        Log.i(TAG, "MobileSecurityDatabase.exists " + doesDatabaseExist);

        if (doesDatabaseExist) {
            MobileSecurityDatabase.init(context, partialSeed, new MobileSecurityDatabaseHelper.CustomInitialization() {
                @Override
                public void onCreate(SQLiteDatabase db) {

                }

                @Override
                public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

                }
            });

            SQLiteDatabase sqLiteDatabase = MobileSecurityDatabase.getInstance().getDatabase();
            Cursor settingsCursor = sqLiteDatabase.query(Settings.TABLE_NAME, null, null, null, null, null, null);
            if (settingsCursor.moveToFirst()) {
                while (!settingsCursor.isAfterLast()) {
                    String settingName = settingsCursor.getString(0);
                    String settingsValue = settingsCursor.getString(1);

                    settingsValue = MobileSecurityDatabase.getInstance().getDecryptedValue(settingName, settingsValue);

                    if (settingsValue == null) {
                        settingsValue = "";
                    }

                    try {
                        writeSetting(settingName, settingsValue, Settings.TABLE_DEFINITION);
                    } catch (SQLiteConstraintException e) {
                        Log.e(TAG, "settingName " + settingName + " settingsValue " + settingsValue, e);
                    }
                    settingsCursor.moveToNext();
                }
            }
            settingsCursor.close();

            MobileSecurityDatabase.deleteDatabase(context);
        }
    }

    /**
     * must be called method to initialize the database and provide extra scripts for initialization
     *
     * @param context           valid context
     * @param deviceId          generated deviceid to be used as salt inside the {@link EncryptionProvider}
     * @param customInitHandler handler to provide access to the onCreate and onUpgrade events
     *                          for custom scripts execution
     * @throws IllegalStateException if {@link EncryptionProviderAes#initKeys()} fails
     */

    public static void init(Context context, String deviceId, MobileSecurityDatabaseHelper.CustomInitialization customInitHandler)
            throws IllegalStateException {

        Log.d(TAG, "init");
        INSTANCE = new SecureDB(context, customInitHandler);
        byte[] key = INSTANCE.readKey();
        if (key == null) {
            // no encrypted entries in DB yet, need to fill DB
            INSTANCE.randomize();
        }
        INSTANCE.migrateToAesEncryption(context, deviceId);
    }

    public static boolean isInitialized() {
        return INSTANCE != null;
    }

    /**
     * must be called method to initialize the database and provide extra scripts for initialization
     *
     * @param context           valid context
     * @param customInitHandler handler to provide access to the onCreate and onUpgrade events
     *                          for custom scripts execution
     * @throws IllegalStateException if {@link EncryptionProviderAes#initKeys()} fails
     */
    public static void init(Context context, MobileSecurityDatabaseHelper.CustomInitialization customInitHandler)
            throws IllegalStateException {
        init(context, HardwareId.getSalt(context), customInitHandler);
    }


    /**
     * Retrieve singleton MobileSecurityDatabase instance
     *
     * @return Initialized singleton MobileSecurityDatabase instance
     */
    public synchronized static SecureDB getInstance() {
        if (INSTANCE == null) {
            throw new RuntimeException("database must be first be initialized by calling 'init'");
        }
        return INSTANCE;
    }

    public synchronized boolean isOpen() {
        return mDatabase.isOpen();
    }

    /**
     * Convenience method to create and insert a column into a database table.
     *
     * @param database    the target SQLiteDatabase
     * @param table       table definition
     * @param columnName  the column name to insert
     * @param columnValue an initial String value for the column
     */
    public static void createColumn(SQLiteDatabase database, ITableDefinition table, String columnName,
                                    String columnValue) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(table.getSettingLabel(), columnName);
        contentValue.put(table.getValueLabel(), columnValue);

        database.insert(table.getName(), null, contentValue);
    }

    /**
     * Convenience method to create and insert a column into a database table.
     *
     * @param database    the target SQLiteDatabase
     * @param table       table definition
     * @param columnName  the column name to insert
     * @param columnValue an initial byte array value for the column
     */
    public static void createColumn(SQLiteDatabase database, ITableDefinition table, String columnName,
                                    byte[] columnValue) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(table.getSettingLabel(), columnName);
        contentValue.put(table.getValueLabel(), columnValue);

        database.insert(table.getName(), null, contentValue);
    }


    /**
     * Try to close the database if it has been created
     */
    public synchronized static void closeDatabase() {
        if (INSTANCE != null) {
            Log.d(TAG, "closing database on shutdown");
            INSTANCE.closeDatabaseHelper();
        }
    }

    public String getDecryptedValue(String value) {
        String decrypted = mEncryptionProvider.decrypt(value);

        // reset the app in the case of decryption failed
        if (decrypted == null) {
            sendResetSignal();
        }
        return decrypted;
    }

    public String getEncryptedStringValue(String value) {
        return mEncryptionProvider.encryptToString(value);
    }

    public byte[] getEncryptedValue(String value) {
        byte[] enrypted = mEncryptionProvider.encrypt(value);

        if (enrypted == null) {
            enrypted = new byte[0];
        }
        return enrypted;
    }

    @Override
    protected void finalize() throws Throwable {
        if (mDatabase != null) {
            mDatabase.close();
        }
        super.finalize();
    }

    public SQLiteDatabase getDatabase() {
        if (mDatabase == null || !mDatabase.isOpen()) {
            mDatabase = mMobileSecurityDatabaseHelper.getWritableDatabase();
        }

        return mDatabase;
    }


    public boolean readBooleanSetting(String settingName, ITableDefinition table, boolean defaultValue) {
        return readSetting(settingName, table, String.valueOf(defaultValue)).equalsIgnoreCase(String.valueOf(true));
    }


    /**
     * Read application setting value from the database.
     *
     * @param settingName Name of the setting
     * @return Value of the setting, as String
     */
    public String readSetting(String settingName, ITableDefinition table, String defaultValue) {
        String value = defaultValue;
        Cursor cursor = null;

        if (table == null) {
            return defaultValue;
        }

        String hashSettingName = HashUtility.sha512(settingName);

        try {
            cursor = getDatabase().query(table.getName(),
                    new String[]{table.getSettingLabel(), table.getValueLabel()},
                    table.getSettingLabel() + "='" + hashSettingName + "'", null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    byte[] decryptedData = cursor.getBlob(cursor.getColumnIndex(table.getValueLabel()));

                    if (decryptedData != null && decryptedData.length > AesCbcWithIntegrity.ENCRYPTION_HEADERS_SIZE) {
                        value = mEncryptionProvider.decrypt(decryptedData);
                        // reset the app if data corrupted
                        if (value == null) {
                            sendResetSignal();
                        }
                    }
                }
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "Failed to read settings", e);
        }

        if (cursor != null) {
            cursor.close();
        }

        return value;
    }

    public byte[] readKey() {
        Cursor cursor = null;
        byte[] value = null;

        String hashSettingName = HashUtility.sha512(Settings.SETTINGS_KEYS);

        try {
            cursor = getDatabase().query(Settings.TABLE_NAME,
                    new String[]{Settings.SETTINGS_NAME, Settings.SETTINGS_VALUE},
                    Settings.SETTINGS_NAME + "='" + hashSettingName + "'", null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    value = cursor.getBlob(cursor.getColumnIndex(Settings.SETTINGS_VALUE));
                }
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "Failed to read settings", e);
        }

        if (cursor != null) {
            cursor.close();
        }

        return value;
    }

    /**
     * Write application setting to the database.
     *
     * @param settingName  Name of the setting
     * @param settingValue Value of the setting
     */
    public int writeSetting(String settingName, boolean settingValue, ITableDefinition table) {
        return writeSetting(settingName, String.valueOf(settingValue), table);
    }

    /**
     * Write application setting to the database.
     *
     * @param settingName  Name of the setting
     * @param settingValue Value of the setting
     */
    public int writeSetting(String settingName, @NonNull String settingValue, ITableDefinition table) {
        if (settingName == null || table == null) {
            throw new IllegalArgumentException();
        }
        String hashSettingName = HashUtility.sha512(settingName);
        ContentValues contentValues = new ContentValues();
        contentValues.put(table.getSettingLabel(), hashSettingName);
        byte[] encryptedValue = mEncryptionProvider.encrypt(settingValue);

        contentValues.put(table.getValueLabel(), encryptedValue);
        int rowAffected = 0;

        try {
            rowAffected = getDatabase().update(table.getName(), contentValues, table.getSettingLabel() + "=?",
                    new String[]{hashSettingName});

            if (rowAffected == 0) {
                long result = getDatabase().insert(table.getName(), null, contentValues);
                if (result != -1) {
                    rowAffected = 1;
                }
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "failed to write setting", e);
        }

        return rowAffected;
    }

    public int writeKey(@NonNull byte[] settingValue) {
        String hashSettingName = HashUtility.sha512(Settings.SETTINGS_KEYS);
        ContentValues contentValues = new ContentValues();
        contentValues.put(Settings.SETTINGS_NAME, hashSettingName);
        contentValues.put(Settings.SETTINGS_VALUE, settingValue);
        int rowAffected = 0;

        try {
            rowAffected = getDatabase().update(Settings.TABLE_NAME, contentValues, Settings.SETTINGS_NAME + "=?",
                    new String[]{hashSettingName});

            if (rowAffected == 0) {
                long result = getDatabase().insert(Settings.TABLE_NAME, null, contentValues);
                if (result != -1) {
                    rowAffected = 1;
                }
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "failed to write setting", e);
        }

        return rowAffected;
    }

    /**
     * Check if a database table exists.
     *
     * @param tableName target table in database
     * @return boolean value true if table exists, else return false
     */
    public boolean isDatabaseTableExists(String tableName) {
        return mMobileSecurityDatabaseHelper.isTableExists(tableName, getDatabase());
    }

    /**
     * Method to delete all database tables.
     *
     * @return boolean value true if no errors occur, else return false
     */
    public boolean tryDeleteAllDatabaseTables() {
        return mMobileSecurityDatabaseHelper.tryDeleteAllDatabaseTables(getDatabase());
    }

    /**
     * Try to delete a database table
     *
     * @param tableName target table in database
     * @return boolean value true if table deletion succeeds, else return false
     */
    public boolean tryDeleteDatabaseTable(String tableName) {
        return mMobileSecurityDatabaseHelper.tryDeleteDatabaseTable(tableName, getDatabase());
    }

    /**
     * Try to delete entire database
     *
     * @return boolean value true if database deletion succeeds, else return false
     */
    public boolean tryDeleteDatabase() {
        if (mContext.deleteDatabase(DATABASE_NAME)) {
            getDatabase().close();
            INSTANCE = null;
            return true;
        }
        return false;
    }

    /**
     * function to remove singleton database instance
     * (used in test automation)
     */
    public static void deleteInstance() {
        if (INSTANCE != null) {
            closeDatabase();
        }
        INSTANCE = null;
    }

    private void closeDatabaseHelper() {
        mMobileSecurityDatabaseHelper.close();
    }


    /**
     * sends a local message that it detected a corruption in the database that signals a tampering try
     * the receiver should reset all settings inside the compromised application
     */
    private void sendResetSignal() {
        Intent resetIntent = new Intent(RESET_APP_MESSAGE);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(resetIntent);
    }
}
