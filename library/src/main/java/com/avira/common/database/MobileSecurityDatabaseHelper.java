/*******************************************************************************
 * Copyright (C) 1986-2014 Avira GmbH. All rights reserved.
 *******************************************************************************/
package com.avira.common.database;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.avira.common.utils.PackageUtilities;

import java.util.ArrayList;

/*
 * Class to create or upgrade the database
 */
public class MobileSecurityDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = MobileSecurityDatabaseHelper.class.getSimpleName();
    private CustomInitialization mCustomInit;

    /*
     * Constructor of MobileSecurityDatabaseHelper
     *
     * @param context Application context
     */
    public MobileSecurityDatabaseHelper(Context context, String name, CustomInitialization customInitHandler) {
        this(context, name, null, PackageUtilities.getVersionCode(context), customInitHandler);
    }

    public MobileSecurityDatabaseHelper(Context context, String name, CursorFactory factory, int version,
                                        CustomInitialization customInitHandler) {
        super(context, name, factory, version);
        mCustomInit = customInitHandler;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        // make sure there is a Settings table containing the field to store the secure random
        Settings.init(database);

        if (mCustomInit != null) {
            mCustomInit.onCreate(database);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        if (mCustomInit != null) {
            mCustomInit.onUpgrade(database, oldVersion, newVersion);
        }
    }

    /**
     * Check if table with a given name exists in the database
     *
     * @param tableName
     * @param database
     *
     * @return true if the table exists, else return false
     */
    public boolean isTableExists(String tableName, SQLiteDatabase database) {
        boolean result = false;

        if (tableName == null) {
            return result;
        }

        try {
            String name = DatabaseUtils.stringForQuery(database,
                    "select ifnull((SELECT name FROM sqlite_master WHERE type='table' AND name=?), '')",
                    new String[]{tableName});
            result = (name != null) && (name.length() > 0);

        } catch (SQLiteException e) {
            Log.e(TAG, "Failed to check table existance in database", e);

        }

        return result;
    }

    /*
     * Method to try and delete a table with the given name
     *
     * @param String value tableName
     *
     * @return true if the table is deleted, else return false
     */
    public boolean tryDeleteDatabaseTable(String tableName, SQLiteDatabase database) {
        boolean result = false;
        // Cursor cursor = null;
        try {
            database.execSQL("DROP TABLE IF EXISTS " + tableName);

            // Check if table no longer exists
            result = !isTableExists(tableName, database);

        } catch (SQLiteException e) {
            Log.e(TAG, "Failed to delete table (" + tableName + ") from database", e);

        } catch (SQLException e) {
            Log.e(TAG, "Failed to delete table (" + tableName + ") from database", e);

        }

        return result;
    }

    /*
     * Method to try to delete all tables in the database
     *
     * @return boolean value true if no errors occur, else return false
     */
    public boolean tryDeleteAllDatabaseTables(SQLiteDatabase database) {
        Cursor cursor = null;
        try {
            cursor = database.rawQuery("SELECT name FROM sqlite_master WHERE type = 'table'", null);
            if (cursor != null && cursor.moveToFirst()) {
                ArrayList<String> tableNames = new ArrayList<String>();

                do {
                    tableNames.add(cursor.getString(cursor.getColumnIndex("name")));
                } while (cursor.moveToNext());

                boolean result = true;
                for (String tableName : tableNames) {
                    result &= tryDeleteDatabaseTable(tableName, database);
                }

                return result;
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "Failed to delete all tables in database", e);

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return false;
    }

    public interface CustomInitialization {
        void onCreate(SQLiteDatabase db);

        void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion);
    }
}