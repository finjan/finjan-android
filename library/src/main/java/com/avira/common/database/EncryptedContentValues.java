/*
 * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
 */
package com.avira.common.database;

import android.content.ContentValues;

import com.avira.common.security.new_aes.EncryptionProviderAes;

public class EncryptedContentValues {
    private ContentValues mContentValues;
    private EncryptionProvider mEncryptionProvider;
    private EncryptionProviderAes mEncryptionAes;

    public EncryptedContentValues() {
        mContentValues = new ContentValues();

        if (SecureDB.isInitialized()) {
            mEncryptionAes = EncryptionProviderAes.getInstance();
        } else {
            mEncryptionProvider = EncryptionProvider.getInstance();
        }
    }

    public EncryptedContentValues put(String key, String value) {
        if (SecureDB.isInitialized()) {
            mContentValues.put(key, mEncryptionAes.encryptToString(value));
        } else {
            mContentValues.put(key, mEncryptionProvider.encrypt(value, key));
        }
        return this;
    }

    public EncryptedContentValues put(String key, long value) {
        if (SecureDB.isInitialized()) {
            mContentValues.put(key, mEncryptionAes.encryptToString(String.valueOf(value)));
        } else {
            mContentValues.put(key, mEncryptionProvider.encrypt(String.valueOf(value), key));
        }
        return this;
    }

    public EncryptedContentValues put(String key, byte[] value) {
        if (SecureDB.isInitialized()) {
            mContentValues.put(key, mEncryptionAes.encryptToString(value));
        } else {
            mContentValues.put(key, mEncryptionProvider.encrypt(value, key));
        }
        return this;
    }

    public ContentValues getContentValues() {
        return mContentValues;
    }
}