package com.avira.common.authentication.models;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anurag on 14/06/17.
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class BaseResponse implements GSONModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
