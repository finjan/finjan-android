/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.authentication.models;

//@formatter:off
import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Gson model that is used to create "emailBreaches" OE backend json request data
 *
 * Sample:
 *
 *         {
 *            "breaches":[
 *               1,
 *               2
 *            ],
 *            "email":"ig.test19@avira.com"
 *         }
 *
 * Created by hui-joo.goh on 7/15/2015.
 *
 */
//@formatter:on
@SuppressWarnings("unused")
public class EmailBreach implements GSONModel {

    // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
    @SerializedName("email") private final String email;
    @SerializedName("breaches") private final int[] breaches;

    public EmailBreach(String email, int[] breaches) {
        this.email = email;
        this.breaches = breaches;
    }
}
