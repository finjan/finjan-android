/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.authentication.models;

import android.content.Context;

import com.avira.common.GSONModel;
import com.avira.common.backend.models.BasePayload;

//@formatter:off
 /**
 * Gson model that is used to create the OE backend json request data for "loginWithToken" (auto login) call
 *
 * Sample:
 *{
 *   "id":{
 *      "uid":"1435745523722-19210978921687767635",
 *      "token":"loginToken",
 *      "tokenType":"ckt",
 *      "uidType":"imei",
 *      "serial":"0af5794202997e4e"
 *   },
 *   "info":{
 *      "registrationId":"",
 *      "deviceModel":"Nexus 5",
 *      "deviceManufacturer":"LGE",
 *      "phoneNumber":"unknown",
 *      "versionNo":"4.1.3659",
 *      "osVersion":"5.1",
 *      "locale":"en_US",
 *      "platform":"android",
 *      "ssid":"av-guest"
 *   },
 *   "language":"en"
 *}
 *
 * Created by hui-joo.goh on 7/1/2015.
 *
 */
//@formatter:on
 @SuppressWarnings("unused")
 public class LoginWithTokenPayload extends BasePayload implements GSONModel {

     // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
     public LoginWithTokenPayload(Context context, String token, String tokenType) {
         super(context);

         id.addToken(token);
         id.addTokenType(tokenType);
     }
 }
