package com.avira.common.authentication;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RetryPolicy;
import com.avira.common.CommonLibrary;
import com.avira.common.GeneralPrefs;
import com.avira.common.authentication.models.BasicApiResponse;
import com.avira.common.authentication.models.DataPayload;
import com.avira.common.authentication.models.LoginResponse_1;
import com.avira.common.authentication.models.RefreshTokenResponse;
import com.avira.common.authentication.models.UserAttributesPayload;
import com.avira.common.authentication.models.UserPayload;
import com.avira.common.backend.Backend;
import com.avira.common.backend.models.AccessTokenPayLoad;
import com.avira.common.backend.models.AccessTokenPayLoadFb;
import com.avira.common.backend.models.AccessTokenPayLoadGg;
import com.avira.common.backend.models.BasePayload;
import com.avira.common.backend.models.BasicTokenPayload;
import com.avira.common.backend.models.RefreshTokenPayLoad;
import com.avira.common.backend.oe.OeRequest;
import com.avira.common.backend.oe.OeRequestQueue;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.security.ProviderInstaller;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

import javax.net.ssl.SSLContext;

/**
 * utility class to be used for authenticating the user to the server using various scenarios
 *
 * @author ovidiu.buleandra
 * @since 21.10.2015
 */
public class Authentication {

    private static final String TOKEN_TYPE_CKT = "ckt";

//    private static final String LOGIN_WITH_EMAIL = "auth";
    private static final String LOGIN_WITH_EMAIL = "oauth/token";
    private static final String REFRESH_TOKEN= "oauth/token";
    private static final String REGISTER_WITH_EMAIL = "register";
    private static final String REGISTER_ANONYMOUSLY = "registerAnonymous";
    private static final String USERS ="wp-json/api/v1/users";
    private static final String LOGIN_WITH_GOOGLE = "loginWithGoogle";
    private static final String LOGIN_WITH_FACEBOOK = "loginWithFacebook";
    private static final String LOGIN_WITH_TOKEN = "loginWithToken";

    private static final int AUTHENTICATION_TIMEOUT = 5000;
    /**
     * logs the user to the server using email and password scenario without providing a callback
     * <br/>
     * the internal callback emits events on success or failure and the implementing app needs to listen for
     * {@link com.avira.common.events.AuthSuccessEvent} and/or {@link com.avira.common.events.AuthFailEvent}
     *
     * @param context valid application context
     * @param email user's email
     * @param password user's password
     * @param isLogin if the action logs the user in an existing account or it needs to create a new one
     */
//    public static void loginEmail(Context context, String email, String password, boolean isLogin) {
//        loginEmail(context, email, password, isLogin, null);
//    }

    /**
     * logs the user to the server using email and password scenario
     *
     * @param context valid application context
     * @param email user's email
     * @param password user's password
     * @param isLogin if the action logs the user in an existing account or it needs to create a new one
     * @param responseCallback custom application callback
     */
    public static void loginEmail(Context context, String email, String password,
                                  boolean isLogin, AuthenticationListener responseCallback) {
        initializeSSLContext(context);
        AccessTokenPayLoad postData = new AccessTokenPayLoad(email, password);
        AuthenticationCallback callback = new AuthenticationCallback(context, responseCallback,email,password,new Gson().toJson(postData));
//        email="Barth_Chuk";
//        password="test";
        // Prepare post data

        // Create OE backend request
        String requestUrl = Backend.BACKEND_URL + (isLogin ? LOGIN_WITH_EMAIL : REGISTER_WITH_EMAIL);
        OeRequest<AccessTokenPayLoad, LoginResponse_1> request = new OeRequest<>(
                requestUrl, postData, LoginResponse_1.class, callback, callback);

        // Add to volley queue
        request.setRetryPolicy(getRetryPolicy());
        OeRequestQueue.getInstance(context).add(request);
    }

    /**
     * Initialize SSL
     * @param mContext
     */
    public static void initializeSSLContext(Context mContext){
        try {
            SSLContext.getInstance("TLSv1.2");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            ProviderInstaller.installIfNeeded(mContext.getApplicationContext());
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

        public static void loginUser(Context context, String type, String email, String password, AuthenticationListener responseCallback){

        switch (type){
            case "fb":
                loginSocial(context,password,email,true,responseCallback);
//                loginSocial(context,"EAAZAav4apAFkBABZAE1xcntjgrrrQ6aZAdWea3fZCak3ZCv6r5UZC0oWYJuUHUdeqGJVtslugU5JyUZAHoN0dPFJ7v8WTdaD7MVGgwfCNjOMWy4Sdj4oMlmCUdDsrgeBuoiBRnkFEXBZAAWCrXf5ZA8cyGCi27zfHkS9w7mZAFyFEFHZCnFUS91NdIIUySQfjJHgqUsU59IX9gONewJEr1nR7Lb6prlZBMmHLIkLWPuo37UuAcQuGjxhVcP0"
//                        ,"10155208647842074",true,responseCallback);
                break;
            case "gg":
                loginSocial(context,password,email,false,responseCallback);
                break;
            case "email":
                loginEmail(context,email,password,true,responseCallback);
                break;
            case "register":
                loginEmail(context,email,password,false,responseCallback);
                break;
        }
    }
    public static void loginSocial(Context context, String token, String socialId,
                                  boolean isFb, AuthenticationListener responseCallback) {
        initializeSSLContext(context);
        AccessTokenPayLoadFb postDataFb = new AccessTokenPayLoadFb(token, socialId);
        AccessTokenPayLoadGg postDataGg = new AccessTokenPayLoadGg(token, socialId);
        AuthenticationCallback callback = new AuthenticationCallback(context, responseCallback,new Gson().toJson(isFb?postDataFb:postDataGg));
//        email="Barth_Chuk";
//        password="test";
        // Prepare post data

        // Create OE backend request
        String requestUrl = Backend.refreshSocialToken;
        if(isFb){
            OeRequest<AccessTokenPayLoadFb, LoginResponse_1> request = new OeRequest<>(
                    requestUrl, postDataFb, LoginResponse_1.class, callback, callback);

            // Add to volley queue
            request.setRetryPolicy(getRetryPolicy());
            OeRequestQueue.getInstance(context).add(request);
        }else {
            OeRequest<AccessTokenPayLoadGg, LoginResponse_1> request = new OeRequest<>(
                    requestUrl, postDataGg, LoginResponse_1.class, callback, callback);

            // Add to volley queue
            request.setRetryPolicy(getRetryPolicy());
            OeRequestQueue.getInstance(context).add(request);
        }

    }
    public static RetryPolicy getRetryPolicy(){
        return new DefaultRetryPolicy(60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }


    public static void refereshToken(Context context,String refereshToken,RefreshTokenLintener refreshTokenLintener){

        initializeSSLContext(context);
        RefreshTokenPayLoad tokenPayLoad=new RefreshTokenPayLoad(refereshToken);
        RefershTokenCallback callback=new RefershTokenCallback(context,refreshTokenLintener,new Gson().toJson(tokenPayLoad));

        //create a backend request
        String url=Backend.BACKEND_URL+REFRESH_TOKEN;
//        Logger.getLogger("Auth Token",url);
        OeRequest<RefreshTokenPayLoad, RefreshTokenResponse> request = new OeRequest<>(
                url, tokenPayLoad, RefreshTokenResponse.class, callback, callback);

        // Add to volley queue
        request.setRetryPolicy(getRetryPolicy());
        OeRequestQueue.getInstance(context).add(request);
    }
    public static void registerUser(Context context, UserAttributesPayload userAttributesPayload, BasicApiListener basicApiListener){

        initializeSSLContext(context);
        DataPayload userPayload=new DataPayload(new UserPayload(userAttributesPayload));
        BasicApiCallback callback=new BasicApiCallback(context,basicApiListener,new Gson().toJson(userPayload));

        GeneralPrefs.setUserName(context,userAttributesPayload.getFirst_name());
        GeneralPrefs.setEmailId(context,userAttributesPayload.getEmail());
        GeneralPrefs.setPassword(context,userAttributesPayload.getPassword());
        //create a backend request
        String url=Backend.BACKEND_URL+ USERS;
        OeRequest<DataPayload, BasicApiResponse> request = new OeRequest<>(
                url, userPayload, BasicApiResponse.class, callback, callback);

        // Add to volley queue
        request.setRetryPolicy(getRetryPolicy());
        OeRequestQueue.getInstance(context).add(request);
    }

    /**
     * logs the user to the server using facebook account scenario without callbacks
     * <br/>
     * the internal callback emits events on success or failure and the implementing app needs to listen for
     * {@link com.avira.common.events.AuthSuccessEvent} and/or {@link com.avira.common.events.AuthFailEvent}
     *
     * @param context valid application context
     * @param email user's email
     * @param token user's facebook token received from facebook sdk after the user logs in and accepts the request
     * @param user user's facebook profile that contains the email and facebook id
     */
    public static void loginFacebook(Context context, String email, String token, JSONObject user) throws JSONException{
        loginFacebook(context, email, token, user, null);
    }

    /**
     * logs the user to the server using facebook account scenario
     *
     * @param context valid application context
     * @param email user's email
     * @param token user's facebook token received from facebook sdk after the user logs in and accepts the request
     * @param user user's facebook profile that contains the email and facebook id
     * @param responseCallback custom application callback
     */
    public static void loginFacebook(Context context, String email, String token,
                                     JSONObject user, AuthenticationListener responseCallback) throws JSONException{
//        AuthenticationCallback callback = new AuthenticationCallback(context, responseCallback);
//
//        // Prepare post data
//        LoginWithFacebookPayload postData = new LoginWithFacebookPayload(context, token, email, user);
//
//        // Create OE backend request
//        String requestUrl = Backend.BACKEND_URL + LOGIN_WITH_FACEBOOK;
//        OeRequest<LoginWithFacebookPayload, LoginResponse> request = new OeRequest<>(
//                requestUrl, postData, LoginResponse.class, callback, callback, AUTHENTICATION_TIMEOUT);
//
//        // Add to volley queue
//        OeRequestQueue.getInstance(context).add(request);
    }

    /**
     * logs the user to the server using google account scenario
     * <br/>
     * the internal callback emits events on success or failure and the implementing app needs to listen for
     * {@link com.avira.common.events.AuthSuccessEvent} and/or {@link com.avira.common.events.AuthFailEvent}
     *
     * @param context valid application context
     * @param email user's email
     * @param token user's google account token after he chooses the desired account to be used as login
     * @param person user's google person profile data for id and email access
     */
    public static void loginGoogle(Context context, String email, String token, Person person) {
        loginGoogle(context, email, token, person, null);
    }

    /**
     * logs the user to the server using google account scenario
     *
     * @param context valid application context
     * @param email user's email
     * @param token user's google account token after he chooses the desired account to be used as login
     * @param person user's google person profile data for id and email access
     * @param responseCallback custom application callback
     */
    public static void loginGoogle(Context context, String email, String token,
                                   Person person, AuthenticationListener responseCallback) {
//        AuthenticationCallback callback = new AuthenticationCallback(context, responseCallback);
//
//        // Prepare post data
//        LoginWithGooglePayload postData = new LoginWithGooglePayload(context, email, token, person);
//
//        // Create OE backend request
//        String requestUrl = Backend.BACKEND_URL + LOGIN_WITH_GOOGLE;
//        OeRequest<LoginWithGooglePayload, LoginResponse> request = new OeRequest<>(
//                requestUrl, postData, LoginResponse.class, callback, callback, AUTHENTICATION_TIMEOUT);
//
//        // Add to volley queue
//        OeRequestQueue.getInstance(context).add(request);
    }

    /**
     * logs the user to the server using google account scenario
     * <br/>
     * the internal callback emits events on success or failure and the implementing app needs to listen for
     * {@link com.avira.common.events.AuthSuccessEvent} and/or {@link com.avira.common.events.AuthFailEvent}
     *
     * @param context valid application context
     * @param email user's email
     * @param purchaseToken user's purchase token from a valid in-app purchase made with google
     * @param productId in-app purchase product id selected by the user
     */
    public static void loginWithPurchase(Context context, String email, String purchaseToken, String productId) {
        loginWithPurchase(context, email, purchaseToken, productId, null);
    }

    /**
     * logs the user to the server using google account scenario
     *
     * @param context valid application context
     * @param email user's email
     * @param purchaseToken user's purchase token from a valid in-app purchase made with google
     * @param productId in-app purchase product id selected by the user
     * @param responseCallback custom application callback
     */
    public static void loginWithPurchase(Context context, String email,
                                         String purchaseToken, String productId, AuthenticationListener responseCallback) {
//        AuthenticationCallback callback = new AuthenticationCallback(context, responseCallback);
//
//        // Prepare post data
//        LoginWithPurchasePayload postData = new LoginWithPurchasePayload(context, email, purchaseToken, productId);
//
//        // Create OE backend request
//        String requestUrl = Backend.BACKEND_URL + LOGIN_WITH_EMAIL;
//        OeRequest<LoginWithPurchasePayload, LoginResponse> request = new OeRequest<>(
//                requestUrl, postData, LoginResponse.class, callback, callback, AUTHENTICATION_TIMEOUT);
//
//        // Add to volley queue
//        OeRequestQueue.getInstance(context).add(request);
    }

    /**
     * the simplest form of registration
     * the backend receives the generated deviceId to have some knowledge about this current device
     * <br/>
     * the internal callback emits events on success or failure and the implementing app needs to listen for
     * {@link com.avira.common.events.AuthSuccessEvent} and/or {@link com.avira.common.events.AuthFailEvent}
     *
     * @param context    valid app context
     */
    public static void registerAnonymous(Context context) {
//        registerAnonymous(context, null);
    }

    /**
     * the simplest form of registration
     * the backend receives the generated deviceId to have some knowledge about this current device
     *
     * @param context    valid app context
     * @param responseCallback   {@link AuthenticationListener} object to deal with the response/error scenarios
     */
//    public static void registerAnonymous(Context context, AuthenticationListener responseCallback) {
//        BasePayload postData = new BasePayload(context);
//        AuthenticationCallback callback = new AuthenticationCallback(context, responseCallback,"","",postData==null?"":postData.toString());
//        // Prepare post data
//
//        // Create OE backend request
//        String requestUrl = Backend.BACKEND_URL + REGISTER_ANONYMOUSLY;
//        OeRequest<BasePayload, LoginResponse_1> request = new OeRequest<>(requestUrl, postData, LoginResponse_1.class,
//                callback, callback);
//
//        // Add to volley queue
//        OeRequestQueue.getInstance(context).add(request);
//    }

    /**
     * logs the user to the server using token scenario (auto login feature when deploying from Online Essentials)
     * <br/>
     * the internal callback emits events on success or failure and the implementing app needs to listen for
     * {@link com.avira.common.events.AuthSuccessEvent} and/or {@link com.avira.common.events.AuthFailEvent}
     *
     * @param context valid application context
     * @param token login token received from server through Google Play Service when deploying application
     */
    public static void loginToken(Context context, String token) {
        loginToken(context, token, null);
    }

    /**
     * logs the user to the server using token scenario (auto login feature when deploying from Online Essentials)
     *
     * @param context valid application context
     * @param token login token received from server through Google Play Service when deploying application
     * @param responseCallback custom application callback
     */
    public static void loginToken(Context context, String token,
                                  AuthenticationListener responseCallback) {
        loginToken(context, token, responseCallback,
                CommonLibrary.DEBUG ? OeRequest.DEFAULT_TIMEOUT_MS_DEBUG:OeRequest.DEFAULT_TIMEOUT_MS);
    }

    /**
     * logs the user to the server using token scenario (auto login feature when deploying from Online Essentials)
     * <br/>
     * the internal callback emits events on success or failure and the implementing app needs to listen for
     * {@link com.avira.common.events.AuthSuccessEvent} and/or {@link com.avira.common.events.AuthFailEvent}
     *
     * @param context valid application context
     * @param token login token received from server through Google Play Service when deploying application
     * @param initialTimeoutMs  The initial timeout for the policy
     */
    public static void loginToken(Context context, String token, int initialTimeoutMs) {
        loginToken(context, token, null, initialTimeoutMs);
    }

    /**
     * logs the user to the server using token scenario (auto login feature when deploying from Online Essentials)
     *
     * @param context valid application context
     * @param token login token received from server through Google Play Service when deploying application
     * @param responseCallback custom application callback
     * @param initialTimeoutMs  The initial timeout for the policy
     */
    public static void loginToken(Context context, String token,
                                  AuthenticationListener responseCallback, int initialTimeoutMs) {
//        AuthenticationCallback callback = new AuthenticationCallback(context, responseCallback);
//        // Prepare post data
//        LoginWithTokenPayload postData = new LoginWithTokenPayload(context, token, TOKEN_TYPE_CKT);
//
//        // Create OE backend request
//        String requestUrl = Backend.BACKEND_URL + LOGIN_WITH_TOKEN;
//        OeRequest<LoginWithTokenPayload, LoginResponse> request = new OeRequest<>(requestUrl, postData,
//                LoginResponse.class, callback, callback, initialTimeoutMs);
//
//        // Add to volley queue
//        OeRequestQueue.getInstance(context).add(request);
    }

    /**
     * checks if the user has some sort of registration data stored(at least it has a deviceId stored so the backend
     * knows about our device)
     * <p/>
     * this is a simple check in the stored shared preferences (unencrypted)
     * to make sure that no one tampered with the app load the deviceId from the database
     *
     * @param context valid app context
     * @return user is known to backend or not
     */
    public static boolean isUserIdentifiedByBackend(Context context) {
        return isRegistered(context) || isAnonymous(context);
    }

    /**
     * @return True if the user has been registered (not anonymous anymore), otherwise false.
     */
    public static boolean isRegistered(Context context) {
        return GeneralPrefs.isRegistered(context);
    }

    /**
     * @return True if this device is known to our backend as a anonymous user, otherwise false.
     */
    public static boolean isAnonymous(Context context) {
        return GeneralPrefs.isAnonymous(context);
    }
}
