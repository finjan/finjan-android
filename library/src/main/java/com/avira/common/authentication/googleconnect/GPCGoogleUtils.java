/*******************************************************************************
Copyright (C) 1986-2015 Avira GmbH. All rights reserved.

 *******************************************************************************/
package com.avira.common.authentication.googleconnect;

import android.os.Build;

/**
 * Google Plus Connect utility class
 * 
 * @author bogdan_oprea
 *
 */
public class GPCGoogleUtils
{
	/**
	 * Here are the conditions in witch google plus will function, this method should be used anywhere the google plus
	 * button appears
	 * 
	 * @return true if google plus login can be used
	 */
	public static final boolean canUseGooglePlus()
	{
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}
}
