package com.avira.common.authentication.models;

import com.avira.common.CommonLibrary;
import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anurag on 14/06/17.
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class UserPayload implements GSONModel{

    @SerializedName("client_id")
    private String  client_id=CommonLibrary.CLIENT_ID;

    @SerializedName("client_secret")
    private String client_secret= CommonLibrary.SECRET_ID;

    @SerializedName("attributes")
    private UserAttributesPayload userAtributesPayload;

    public UserPayload(String[] user){
        userAtributesPayload=new UserAttributesPayload(user);
    }
    public UserPayload(UserAttributesPayload userAttributesPayload){
        this.userAtributesPayload=userAttributesPayload;
    }
}
