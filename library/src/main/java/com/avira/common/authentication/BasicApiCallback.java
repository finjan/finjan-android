package com.avira.common.authentication;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.avira.common.authentication.models.BasicApiResponse;
import com.avira.common.authentication.models.DataPayload;
import com.avira.common.authentication.models.LoginResponse_1;
import com.avira.common.backend.WebUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * Created by anurag on 14/06/17.
 *
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class BasicApiCallback implements Response.Listener<BasicApiResponse>, Response.ErrorListener{


    private Context mContext;
    private WeakReference<BasicApiListener> mResponseCallbackRef;
    private static final String TAG = BasicApiCallback.class.getSimpleName();
    private String dataPayload;

    public BasicApiCallback(Context context) {
        mContext = context.getApplicationContext();
    }

    public BasicApiCallback(Context context, BasicApiListener responseCallback, String dataPayload) {
        this(context);
        this.dataPayload=dataPayload;
        if(responseCallback != null) {
            mResponseCallbackRef = new WeakReference<>(responseCallback);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        BasicApiListener refreshTokenLintener;
        if (mResponseCallbackRef != null && (refreshTokenLintener = mResponseCallbackRef.get()) != null) {
            refreshTokenLintener.onApiFailled(getErrorMsg(error),dataPayload);
        }
    }
    private BasicError getErrorMsg(VolleyError error){
        String json=WebUtility.getMessage(error);
        if(!TextUtils.isEmpty(json)){
            String code=null;
            String title=null,desc=null;
            int status=-1;
            try {
                JSONObject jsonObject=new JSONObject(json);
                if(jsonObject.has("error")){
                    title=jsonObject.optString("error");
                    desc=jsonObject.optString("error_description");
                }else {
                    desc=jsonObject.optString("message");
                    status=jsonObject.getJSONObject("data").optInt("status");
                    code=jsonObject.optString("code");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                return new BasicError("","Unbale to Connect,Try again",status,code,json);
            }
            return new BasicError(TextUtils.isEmpty(title)?"":title,TextUtils.isEmpty(desc)?"":desc,status,code,json);
        }

        return new BasicError("","Unknow error",400,"",json);
    }

    @Override
    public void onResponse(BasicApiResponse response) {
        BasicApiListener refreshTokenLintener;

        if (mResponseCallbackRef != null && (refreshTokenLintener = mResponseCallbackRef.get()) != null) {
            refreshTokenLintener.onApiSuccess(response);
        }
    }
    public class BasicError{
        String title,desc,code,json;
        int status;
        private BasicError(String title,String des,int status,String code,String json){
            this.title=title;desc=des;
            this.status=status;
            this.code=code;
            this.json=json;
        }
        private BasicError(String msg,int status){this.desc=msg;this.status=status;}

        public int getStatus() {
            return status;
        }

        public String getTitle() {
            return title;
        }

        public String getDesc() {
            return desc;
        }

        public String getCode() {
            return code;
        }
        public String getJson() {
            return json;
        }

    }
}
