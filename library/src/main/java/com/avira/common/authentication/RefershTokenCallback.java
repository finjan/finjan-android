package com.avira.common.authentication;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.avira.common.authentication.models.RefreshTokenResponse;
import com.avira.common.backend.WebUtility;
import com.avira.common.events.AuthFailEvent;

import java.lang.ref.WeakReference;

import de.greenrobot.event.EventBus;

/**
 * Created by anurag on 14/06/17.
 * <p>
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class RefershTokenCallback implements Response.Listener<RefreshTokenResponse>, Response.ErrorListener {

    private static final String TAG = RefershTokenCallback.class.getSimpleName();
    private Context mContext;
    private WeakReference<RefreshTokenLintener> mResponseCallbackRef;
    private String request;

    public RefershTokenCallback(Context context) {
        mContext = context.getApplicationContext();
    }

    public RefershTokenCallback(Context context, RefreshTokenLintener responseCallback,String request) {
        this(context);
        this.request=request;
        if (responseCallback != null) {
            mResponseCallbackRef = new WeakReference<>(responseCallback);
        }
    }

    @Override
    public void onResponse(RefreshTokenResponse response) {
        RefreshTokenLintener refreshTokenLintener=null;

        if (mResponseCallbackRef != null && (refreshTokenLintener = mResponseCallbackRef.get()) != null) {
            refreshTokenLintener.onRefershSuccess(response);
        }
//        EventBus.getDefault().post(new AuthSuccessEvent(response));

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        RefreshTokenLintener responseCallback=null;
        int status = WebUtility.getHTTPErrorCode(error);
        String message = WebUtility.getMessage(error);
        if (mResponseCallbackRef != null && (responseCallback = mResponseCallbackRef.get()) != null) {
            responseCallback.onErrorResponse(status, message,request);
        }
        EventBus.getDefault().post(new AuthFailEvent(status, message));
    }
}
