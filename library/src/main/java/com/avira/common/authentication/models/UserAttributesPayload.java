package com.avira.common.authentication.models;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anurag on 14/06/17.
 * <p>
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class UserAttributesPayload implements GSONModel {

    @SerializedName("first_name")
    private String first_name;
    @SerializedName("last_name")
    private String last_name;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("phone")
    private String phone;
    @SerializedName("salutation")
    private String salutation;
    @SerializedName("address")
    private String address;
    @SerializedName("optin")
    private String optin;
    @SerializedName("user_type")
    private String user_type;

    @SerializedName("source")
    private String source;


    @SerializedName("fb_id")
    private String fb_id;
    @SerializedName("gg_id")

    private String gg_id;
    @SerializedName("beta_tester")
    private String beta_tester;
    @SerializedName("status_tfa")
    private String status_tfa;
    @SerializedName("phone_tfa")
    private String phone_tfa;
    @SerializedName("country")
    private String country;
    @SerializedName("state")
    private String state;
    @SerializedName("city")
    private String city;
    @SerializedName("zip")
    private String zip;
    @SerializedName("organization_name")
    private String organization_name;
    @SerializedName("profile_image_full")
    private String profile_image_full;
    @SerializedName("profile_image_large")
    private String profile_image_large;
    @SerializedName("profile_image_medium")
    private String profile_image_medium;
    @SerializedName("profile_image_small")
    private String profile_image_small;

    private String email_verified;

    public UserAttributesPayload(){}


    public UserAttributesPayload(String[] arr) {
        first_name = arr[0];
        last_name = arr[1];
        email = arr[2];
        password = arr[3];
        phone = arr[4];
        salutation = arr[5];
        address = arr[6];
        optin = arr[7];
        user_type = arr[8];
        fb_id = arr[9];
        gg_id = arr[10];
        beta_tester = arr[11];
        status_tfa = arr[12];
        phone_tfa = arr[13];
        country = arr[14];
        state = arr[15];
        city = arr[16];
        zip = arr[17];
        organization_name = arr[18];
        profile_image_full = arr[19];
        profile_image_large = arr[20];
        profile_image_medium = arr[21];
        profile_image_small = arr[22];
        email_verified = arr[23];
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public String getSalutation() {
        return salutation;
    }

    public String getAddress() {
        return address;
    }

    public String getOptin() {
        return optin;
    }

    public String getUser_type() {
        return user_type;
    }

    public String getFb_id() {
        return fb_id;
    }

    public String getGg_id() {
        return gg_id;
    }

    public String getBeta_tester() {
        return beta_tester;
    }

    public String getStatus_tfa() {
        return status_tfa;
    }

    public String getPhone_tfa() {
        return phone_tfa;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }

    public String getOrganization_name() {
        return organization_name;
    }

    public String getProfile_image_full() {
        return profile_image_full;
    }

    public String getProfile_image_large() {
        return profile_image_large;
    }

    public String getProfile_image_medium() {
        return profile_image_medium;
    }

    public String getProfile_image_small() {
        return profile_image_small;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setOptin(String optin) {
        this.optin = optin;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public void setGg_id(String gg_id) {
        this.gg_id = gg_id;
    }

    public void setBeta_tester(String beta_tester) {
        this.beta_tester = beta_tester;
    }

    public void setStatus_tfa(String status_tfa) {
        this.status_tfa = status_tfa;
    }

    public void setPhone_tfa(String phone_tfa) {
        this.phone_tfa = phone_tfa;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setOrganization_name(String organization_name) {
        this.organization_name = organization_name;
    }

    public void setProfile_image_full(String profile_image_full) {
        this.profile_image_full = profile_image_full;
    }

    public void setProfile_image_large(String profile_image_large) {
        this.profile_image_large = profile_image_large;
    }

    public void setProfile_image_medium(String profile_image_medium) {
        this.profile_image_medium = profile_image_medium;
    }

    public void setProfile_image_small(String profile_image_small) {
        this.profile_image_small = profile_image_small;
    }
    public void setSource(){
        this.source = "android";
    }
    public String getSource(){return  source;}

    public String getEmail_verified() {
        return email_verified;
    }

    public void setEmail_verified(String email_verified) {
        this.email_verified = email_verified;
    }
}
