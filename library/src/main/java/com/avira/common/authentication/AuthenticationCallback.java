    package com.avira.common.authentication;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.avira.common.GeneralPrefs;
import com.avira.common.authentication.models.LoginResponse;
import com.avira.common.authentication.models.LoginResponse_1;
import com.avira.common.backend.WebUtility;
import com.avira.common.backend.models.AccessTokenPayLoad;
import com.avira.common.backend.oe.OeRequestQueue;
import com.avira.common.database.Settings;
import com.avira.common.events.AuthFailEvent;
import com.avira.common.events.AuthSuccessEvent;
import com.avira.common.events.UserProfileUpdatedEvent;
//import com.avira.common.gcm.GCMRegistration;
import com.avira.common.utils.SharedPreferencesUtilities;
import com.avira.common.utils.validation.EmailConstraintChecker;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;

import de.greenrobot.event.EventBus;

/**
 * general utility callback that is used in all the login scenarios and handles the actual verification of the server
 * response and storing on the device side
 * when the work is done it calls methods from the passed callback that gives the implementing application to do custom
 * work on that event (success or failure)
 *
 * @author ovidiu.buleandra
 * @since 26.10.2015
 */
public class AuthenticationCallback implements Response.Listener<LoginResponse_1>, Response.ErrorListener {

    private Context mContext;
    private WeakReference<AuthenticationListener> mResponseCallbackRef;
    private static final String TAG = AuthenticationCallback.class.getSimpleName();
    private String email,password;
    private String payLoad;


    public AuthenticationCallback(Context context) {
        mContext = context.getApplicationContext();
    }

    public AuthenticationCallback(Context context, AuthenticationListener responseCallback,
                                  String email,String password,String payLoad) {
        this(context);
        this.email=email;
        this.password=password;
        this.payLoad=payLoad;
        if(responseCallback != null) {
            mResponseCallbackRef = new WeakReference<>(responseCallback);
        }
    }

    public AuthenticationCallback(Context context, AuthenticationListener responseCallback,
                                 String payLoad) {
        this(context);
        this.payLoad=payLoad;
        if(responseCallback != null) {
            mResponseCallbackRef = new WeakReference<>(responseCallback);
        }
    }


    @Override
    public void onResponse(LoginResponse_1 response) {
        AuthenticationListener responseCallback;
//        if(!response.isSuccess()) {
//            String errorMessage = String.format("[%d] %s", response.getStatusCode(), response.getStatus());
//            if(mResponseCallbackRef != null && (responseCallback = mResponseCallbackRef.get()) != null) {
//                responseCallback.onAuthError(response.getStatusCode(), errorMessage);
//            }
//            EventBus.getDefault().post(new AuthFailEvent(response.getStatusCode(), errorMessage));
//            return;
//        }

//        GeneralPrefs.setOeServerOperation(mContext, response.getOperation());

//        String deviceId = response.getDeviceId();
//        boolean hasDeviceId = !TextUtils.isEmpty(deviceId);

//        if (hasDeviceId) {
//            Settings.saveDeviceId(Userp);
//            GCMRegistration.getInstance().register(mContext, null);
            // authentication was successful
//        }

        //check if user is not anonymous
//        User user = response.getUser();
//        boolean isAnonymous = !(user != null && user.getEmail() != null && EmailConstraintChecker.isValidEmailFormat(user.getEmail()));
//        GeneralPrefs.setAnonymous(mContext, isAnonymous);
//
//        if(!isAnonymous) {
            GeneralPrefs.setRegistered(mContext, true);
            // we know for sure the user doesn't have a profile saved in the DB and we create a new one
//        if(!TextUtils.isEmpty(email) ){
//            final UserProfile userProfile = new UserProfile();
//            userProfile.setEmail(email);
//            userProfile.save();
//        }


//            String imageUrl = user.getImageUrl();
//            if (!TextUtils.isEmpty(imageUrl)) {
//                ImageRequest imageRequest = new ImageRequest(imageUrl.replace("https", "http"), new Response.Listener<Bitmap>() {
//                    @Override
//                    public void onResponse(Bitmap response) {
//                        if (response != null) {
//                            ByteArrayOutputStream out = new ByteArrayOutputStream();
//                            response.compress(Bitmap.CompressFormat.PNG, 100, out);
//                            userProfile.setPicture(out.toByteArray());
//                            userProfile.save();
//                            EventBus.getDefault().post(new UserProfileUpdatedEvent());
//                        }
//                    }
//                }, 0, 0, null, Bitmap.Config.RGB_565, null);
//                OeRequestQueue.getInstance(mContext).add(imageRequest);
//            }
//        }
//        Subscription subscription = response.getSubscription();
        if(mResponseCallbackRef != null && (responseCallback = mResponseCallbackRef.get()) != null) {
//            responseCallback.onAuthSuccess(user, subscription);
            responseCallback.onAuthSuccess(response);
        }
        if(!TextUtils.isEmpty(email)){
            GeneralPrefs.setEmailId(mContext,email);
            if(!email.equalsIgnoreCase(GeneralPrefs.getEmailId(mContext))){
                GeneralPrefs.setUserName(mContext,"");
            }
        }
        if(!TextUtils.isEmpty(password)){
            GeneralPrefs.setPassword(mContext,password);
        }

//        EventBus.getDefault().post(new AuthSuccessEvent(user, subscription));
        EventBus.getDefault().post(new AuthSuccessEvent(response));
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AuthenticationListener responseCallback;
        int status = WebUtility.getHTTPErrorCode(error);
        String message = WebUtility.getErrorDesc(error);
        if(mResponseCallbackRef != null && (responseCallback = mResponseCallbackRef.get()) != null) {
            responseCallback.onAuthError(status, message,payLoad,error.getLocalizedMessage()+"----"+error.getMessage());
        }
        EventBus.getDefault().post(new AuthFailEvent(status, message));

    }
}
