package com.avira.common.authentication;

import com.avira.common.authentication.models.BasicApiResponse;

/**
 * Created by anurag on 14/06/17.
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public interface BasicApiListener {
    void onApiSuccess(BasicApiResponse basicApiResponse);
    void onApiFailled(BasicApiCallback.BasicError basicError,String request);
}
