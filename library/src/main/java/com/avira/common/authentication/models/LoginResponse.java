/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.authentication.models;

import com.avira.common.GSONModel;
import com.avira.common.backend.oe.BaseResponse;
import com.google.gson.annotations.SerializedName;

//@formatter:off
/** Gson model that is used to populate OE backend json response data after a successful user login/registration action
 *
 *{
 *"access_token": "hc47cwtq93doxjs88o3ranb6xcoitqniqysg9peg",
 *"expires_in": 3600,
 *"token_type": "Bearer",
 *"scope": "basic",
 *"refresh_token": "mpzsqvseoxppm93qyahcfcwkbri0w71s4nxsclnz"
 *}
 *
 *
 */
//@formatter:on
@SuppressWarnings("unused")
public class LoginResponse extends BaseResponse implements GSONModel {

    // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
    @SerializedName("storedRegistrationId") private String storedRegistrationId;
    @SerializedName("deviceId") private String deviceId;
    @SerializedName("user") private User user;
    @SerializedName("subscription") private Subscription subscription;
    @SerializedName("operation") private String operation;
    /**
     * Get the stored user registration ID
     *
     * @return The storedRegistrationId
     */
    public String getStoredRegistrationId() {
        return storedRegistrationId;
    }

    /**
     * Get the server registered device ID information
     *
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * Get the registered user information
     *
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * Get the registered user subscription information
     *
     * @return The subscription
     */
    public Subscription getSubscription() {
        return subscription;
    }

    /**
     * Get the server operation information (server performed a login or register call)
     * @return
     */
    public String getOperation() {
        return operation;
    }
}
