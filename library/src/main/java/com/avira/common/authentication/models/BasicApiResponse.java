package com.avira.common.authentication.models;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anurag on 14/06/17.
 * <p>
 * <p>
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 * <p>
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class BasicApiResponse implements GSONModel {


    @SerializedName("status")
    protected int status;
    @SerializedName("message")
    protected String message;
    @SerializedName("code")
    protected String code;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getCode(){return code;}
}
