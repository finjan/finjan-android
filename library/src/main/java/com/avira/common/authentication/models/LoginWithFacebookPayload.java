/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.authentication.models;

import android.content.Context;

import com.avira.common.GSONModel;
import com.avira.common.authentication.facebookconnect.FBFacebookUtils;
import com.avira.common.backend.models.BasePayload;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

//@formatter:off
/**
 * Gson model that is used to create the OE backend json request data for "loginWithFacebook" call
 *
 * Sample:
 *{
 *   "ft":"CAAGOc2m6YDgBAKzqrNJAfbDMwg4KhqNjKrcm7nAtAy9sQ7w9ZATvwFc0EhO13hPDCPZCa9YrXtuILssPBT7ZAWwAbJPHd3MuDwjWpOtBz12QJoBgvOzdfioqn7hlnA6LTnoUv7KLWYIuqwREbXl0REOZBJfho0qBXvgFgOr1nhJYVOwR6a8tT0Q8ZBpvv3XjMExR2dI7ZADIF2FQRZBPKsXEKGR9sSEwMDd7jiRpJmxPAZDZD",
 *   "fbi":"1595352737360024",
 *   "email":"androidsolutionteam@gmail.com",
 *   "language":"en",
 *   "id":{
 *      "uid":"1435671330059-19210978921687767635",
 *      "uidType":"imei",
 *      "serial":"0af5794202997e4e"
 *   },
 *   "info":{
 *      "registrationId":"",
 *      "deviceModel":"Nexus 5",
 *      "deviceManufacturer":"LGE",
 *      "phoneNumber":"unknown",
 *      "versionNo":"4.1.3659",
 *      "osVersion":"5.1",
 *      "locale":"en_US",
 *      "platform":"android",
 *      "ssid":"av-guest"
 *   },
 *   "details":{
 *      "firstName":"MobileTeam",
 *      "lastName":"Avira"
 *   }
 *}
 *
 * Created by hui-joo.goh on 6/30/2015.
 *
 */
//@formatter:on
@SuppressWarnings("unused")
public class LoginWithFacebookPayload extends BasePayload implements GSONModel {

    // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
    @SerializedName("ft") private String ft;
    @SerializedName("fbi") private String fbi;
    @SerializedName("email") private String email;
    @SerializedName("details") private Details details;

    public LoginWithFacebookPayload(Context context, String fbToken, String email, JSONObject user) throws JSONException{
        super(context);
        this.ft = fbToken;
        this.fbi = user.getString(FBFacebookUtils.FB_ID_PARAM);
        this.email = email;
        this.details = new Details(user);
    }
}
