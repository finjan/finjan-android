/*******************************************************************************
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 *******************************************************************************/
package com.avira.common.authentication.models;

import android.content.Context;

import com.avira.common.GSONModel;
import com.avira.common.backend.models.BasePayload;
import com.google.gson.annotations.SerializedName;

//@formatter:off
/**
 *
 * Authentification body used in in app billing anonymus flow
 *
 *  Example:
 * {
 *    "info": {
 *         "registrationId": "apa19Be84NP586Tk7qUSuxfUdFhzExWsRZY-4ShFIm6NVcPMMLY6niVgqmd7f08VVWjAuC_7OBcpVjcZGF_4Npe-UxOcaOmXWHSRXXJDrh65M1Gu9Jd7MJ5Oc178xY3IppFz8iX61zKI6lv-47Pf0W_AJXcsmtd9a"
 *         "versionNo":"4.1.3659",
 *         "platform":"android",
 *         "phoneNumber":"unknown",
 *         "ssid":"av-guest",
 *         "deviceModel":"Nexus 5",
 *         "locale":"en_US",
 *         "osVersion":"5.1",
 *         "deviceManufacturer":"LGE"
 *     },
 *     "withLogin": false,
 *     "type": "walletPurchase",
 *      "id": {
 *         "uid": "123456789012345",
 *         "email": "dev_android_mobile_0001@oemail.com",
 *         "uidType": "imei",
 *         "packageName": "com.avira.android",
 *         "purchaseToken": "gppenpcjkpahpmjeglfjmmdo.AO-J1Ow9pmQ1Cm95OKnOTdqpEfPE0gqR_Lf_KENgknkDKr-0kjLkKVwSr4xUnpiCE1TPb9bDuLLgQ34khB4vmY9DQWs-YA3S6ga0_v7m0SJlYhm1sEk7P94",
 *         "productId": "656"
 *         "language": "en"
 *     }
 * }
 *
 * @author bogdan_oprea
 */
//@formatter:on
@SuppressWarnings("unused")

public class LoginWithPurchasePayload extends BasePayload implements GSONModel {
    @SerializedName("withLogin") private Boolean withLogin;
    @SerializedName("type") private String type;

    public LoginWithPurchasePayload(Context context, String email, String purchaseToken, String productId) {
        super(context);

        id.addPurchaseInfo(email, purchaseToken, productId);

        type = "walletPurchase";
        withLogin = false;
    }


}
