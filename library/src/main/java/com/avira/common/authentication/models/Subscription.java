/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.authentication.models;

//@formatter:off
import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName; /**
 * Gson model that is used to populate "subscription" OE backend json response data
 *
 * Sample:
 *
 *   "subscription":{
 *      "enabled":false,
 *      "expireDate":"2016-06-24",
 *      "type":"premium"
 *      "hadBundledAndroid":false
 *   }
 *
 * Created by hui-joo.goh on 6/30/2015.
 *
 */
//@formatter:on
@SuppressWarnings("unused")
public class Subscription implements GSONModel {

    @SerializedName("enabled")
    private Boolean enabled;
    @SerializedName("expireDate")
    private String expireDate;
    @SerializedName("type")
    private String type;
    @SerializedName("hadBundledAndroid")
    private Boolean hadBundledAndroid;
    @SerializedName("status")
    private Integer status;

    /**
     * Get the subscription enabled status
     *
     * @return True if the subscription is enabled, otherwise false
     */
    public boolean getEnabled() {
        return (enabled == null ? false : enabled);
    }

    /**
     * Get the subscription expiry date
     *
     * @return The expireDate
     */
    public String getExpireDate() {
        return expireDate;
    }

    /**
     * Get the type of subscription
     *
     * @return The subscription type
     */
    public String getType() {
        return type;
    }

    /**
     * Check if the subscription had bundled android
     *
     * @return True if the subscription had bundled android, otherwise false
     */
    public boolean hadBundledAndroid() {
        return (hadBundledAndroid == null ? false : hadBundledAndroid);
    }

    /**
     * used only in response to a trial request
     *
     * @return 0 - error, 1 - ok, 2 - already in trial
     */
    public int getStatus() {
        return status;
    }
}
