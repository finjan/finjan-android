package com.avira.common.authentication;

import com.avira.common.authentication.models.LoginResponse_1;
import com.avira.common.authentication.models.Subscription;
import com.avira.common.authentication.models.User;

/**
 * callback interface to be implemented by the calling application to act at the end of authentication procedure
 *
 * @author ovidiu.buleandra
 * @since 22.10.2015
 */
public interface AuthenticationListener {
    void onAuthSuccess(User user, Subscription subscription);
    void onAuthError(int errorCode, String errorMessage,String request,String detailErrorMsg);
    void onAuthSuccess(LoginResponse_1 loginResponse_1);
}
