/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.authentication.models;

import android.content.Context;

import com.avira.common.GSONModel;
import com.avira.common.backend.models.BasePayload;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.annotations.SerializedName;

//@formatter:off
/**
 * Gson model that is used to create the OE backend json request data for "loginWithGoogle" call
 *
 * Sample:
 *{
 *   "details":{
 *      "birthDate":"PersonBirthday",
 *      "firstName":"PersonName",
 *      "language":"PersonLanguage",
 *      "lastName":"PersonName",
 *      "salutation":"0"
 *   },
 *   "email":"GooglePlusEmail",
 *   "gpi":"PersonId",
 *   "gpt":"AccessToken",
 *   "id":{
 *      "serial":"0af5794202997e4e",
 *      "uid":"1435563952501-19210978921687767635",
 *      "uidType":"imei"
 *   },
 *   "info":{
 *      "registrationId":"",
 *      "deviceModel":"Nexus 5",
 *      "deviceManufacturer":"LGE",
 *      "phoneNumber":"unknown",
 *      "versionNo":"4.1.3659",
 *      "osVersion":"5.1",
 *      "locale":"en_US",
 *      "platform":"android",
 *      "ssid":"av-guest"
 *   },
 *   "language":"en"
 *}
 *
 * Created by hui-joo.goh on 6/29/2015.
 *
 */
//@formatter:on
@SuppressWarnings("unused")
public class LoginWithGooglePayload extends BasePayload implements GSONModel {

    // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
    @SerializedName("email") private String email;
    @SerializedName("gpi") private String gpi;
    @SerializedName("gpt") private String gpt;
    @SerializedName("details") private Details details;

    /**
     * Create a Gson model that is used to create the OE backend json request data for "loginWithGoogle" call
     *
     * @param context valid application context
     * @param email         Users email from google plus
     * @param accessToken           Google plus access token
     * @param currentPerson User personal info
     */
    public LoginWithGooglePayload(Context context, String email, String accessToken, Person currentPerson) {
        super(context);
        this.email = email;
        this.gpi = currentPerson.getId();
        this.gpt = accessToken;
        this.details = new Details(currentPerson);
    }
}
