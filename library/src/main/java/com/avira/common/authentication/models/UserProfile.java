package com.avira.common.authentication.models;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;

import com.avira.common.GSONModel;
import com.avira.common.R;
import com.avira.common.database.Settings;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

import java.io.ByteArrayOutputStream;
import java.util.Random;

/**
 * model class for user profile storing
 * the most important thing in it is the user's email
 *
 * @author ovidiu.buleandra
 * @since 22.10.2015
 */
public class UserProfile implements GSONModel {

    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("email")
    private String email;
    @SerializedName("picture")
    private byte[] picture;

    /**
     * read the user profile stored in the secure database
     * @return a valid UserProfile object or null in case there's none
     */
//    public static UserProfile load() {
//        String json = Settings.readUserProfile();
//        if(TextUtils.isEmpty(json)) {
//            return null;
//        }
//
//        try {
//            return new Gson().fromJson(json, UserProfile.class);
//        } catch (JsonSyntaxException e) {
//            Log.e(UserProfile.class.getSimpleName(), "error de-serializing user profile", e);
//            // TODO check if extra steps are necessary
//            // -> why did it happen and where do we get a valid user profile if the user is already logged in
//            Settings.saveUserProfile(""); // erase malformed profile from DB
//        }
//        return null;
//    }

    public String getFirstName() {
        return firstName;
    }

    public UserProfile setFirstName(String fn) {
        firstName = fn;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserProfile setLastName(String ln) {
        lastName = ln;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserProfile setEmail(String e) {
        email = e;
        return this;
    }

    public byte[] getPicture() {
        return picture;
    }

    public Bitmap getPictureBitmap() {
        if(picture != null)
            return BitmapFactory.decodeByteArray(picture, 0, picture.length);
        else
            return null;
    }

    public UserProfile setPicture(byte[] p) {
        picture = p;
        return this;
    }

    /**
     * save the current UserProfile to the secure database
     */
    public void save() {
        Settings.saveUserProfile(new Gson().toJson(this));
    }

    public void generateGenericPicture(Context context) {
        final int PICTURE_SIZE = 196;
        final float TEXT_SIZE = 96;

        String text = "";
        if(!TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName)) {
            text = firstName.substring(0, Math.min(2, firstName.length()));
        } else if(TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName)) {
            text = lastName.substring(0, Math.min(2, lastName.length()));
        } else if(!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName)) {
            text = firstName.substring(0, 1) + lastName.substring(0, 1);
        } else if(!TextUtils.isEmpty(email)){
            text = email.substring(0, Math.min(2, email.indexOf('@')));
        }
        text = text.toUpperCase();

        // select a random color from an array of generic colors (taken from Android's Contacts app)
       Random random = new Random();


        Bitmap profilePic = Bitmap.createBitmap(PICTURE_SIZE, PICTURE_SIZE, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(profilePic);
        canvas.drawColor(context.getResources().getColor(R.color.profile_pic_color));
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        paint.setTextSize(TEXT_SIZE);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/KievitCompPro-Light.ttf");
        Typeface boldFont = Typeface.create(font, Typeface.BOLD);
        paint.setTypeface(boldFont);
        Rect textBounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text,
                PICTURE_SIZE / 2 - textBounds.exactCenterX(),
                PICTURE_SIZE / 2 - textBounds.exactCenterY(),
                paint);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        profilePic.compress(Bitmap.CompressFormat.PNG, 100, out);
        setPicture(out.toByteArray());
    }
}
