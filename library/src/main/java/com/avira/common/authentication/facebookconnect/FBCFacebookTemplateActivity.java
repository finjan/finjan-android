///*******************************************************************************
// * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
// *******************************************************************************/
//package com.avira.common.authentication.facebookconnect;
//
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.util.Log;
//
//import com.avira.common.activities.BaseFragmentActivity;
//import com.facebook.AccessToken;
//import com.facebook.AccessTokenTracker;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookAuthorizationException;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.FacebookSdk;
//import com.facebook.GraphRequest;
//import com.facebook.GraphResponse;
//import com.facebook.Profile;
//import com.facebook.ProfileTracker;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//import com.facebook.share.ShareApi;
//import com.facebook.share.Sharer;
//import com.facebook.share.model.ShareLinkContent;
//import com.facebook.share.widget.ShareDialog;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.Arrays;
//
///**
// * Abstract class that encapsulates all the facebook calls
// *
// * call loginWithFacebook to start login and the result will be returned trought the abstract methods:
// * facebookRequestSuccessful, facebookRequestPermissionsDenied and facebookRequestFailed
// *
// * ATTENTION: This class will be created/destroyed automatically multiple times during a facebook login call.
// * We can differentiate if the activity was started manually by us by checking if savedInstanceState is null in onCreate.
// *
// * @author bogdan_oprea
// *
// */
//public abstract class FBCFacebookTemplateActivity extends BaseFragmentActivity {
//
//    private static String TAG = FBCFacebookTemplateActivity.class.getSimpleName();
//
//    private static final String PROFILE_READ = "public_profile";
//    private static final String EMAIL_READ = "email";
//    private static final String BIRTHDAY_READ = "user_birthday";
//    private static final String FB_GRAPH_FIELDS = "fields";
//
//    /**
//     * This class will be created/destroyed automatically multiple times during a facebook login call.
//     * This flag tracks if this class was started automatically and not manually by us
//     * so that we can prevent calling fb login api multiple times.
//     */
//    protected static final String EXTRA_STARTED_AUTOMATICALLY = "extra_started_automatically";
//    protected static final String SHARE_TO_FB_CONTENT_EXTRA = "content_extra";
//    protected static final String SHARE_TO_FB_TITLE_EXTRA = "title_extra";
//    protected static final String SHARE_TO_FB_URL_EXTRA = "url_extra";
//    protected static final String SHARE_TO_FB_ACTION_EXTRA = "share_action";
//
//    private ProfileTracker mProfileTracker;
//    private AccessTokenTracker mAccessTokenTracker;
//    private ShareDialog mShareDialog;
//    private boolean mCanPresentShareDialog;
//
//    private boolean mIsPostToWallRequest = false;
//
//    private String mContentToShare;
//    private String mTitleToShare;
//    private String mUrlToShare;
//    private CallbackManager mCallbackManager;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        //init fb sdk
//        FacebookSdk.sdkInitialize(this.getApplicationContext());
//
//        // new callback manager instance
//        mCallbackManager = CallbackManager.Factory.create();
//
//        mProfileTracker = new ProfileTracker() {
//            @Override
//            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
//                // It's possible that we were waiting for Profile to be populated in order to
//                // post a status update.
//                Profile.setCurrentProfile(currentProfile);
//            }
//        };
//
//        mAccessTokenTracker = new AccessTokenTracker() {
//            @Override
//            protected void onCurrentAccessTokenChanged(
//                    AccessToken oldAccessToken,
//                    AccessToken currentAccessToken) {
//                if (currentAccessToken != null) {
//                    AccessToken.setCurrentAccessToken(currentAccessToken); //update current token manually, from listener.
//                }
//            }
//        };
//
//        // adding login callback
//        LoginManager.getInstance().registerCallback(mCallbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        Log.d(TAG, "Success Facebook login " + loginResult.getAccessToken().toString());
//                        if (!mIsPostToWallRequest) {
//                            getUserDetails(AccessToken.getCurrentAccessToken());
//                        } else {
//                            Log.i(TAG, "Continue with post link");
//                            shareLinkContentToFbWall();
//                        }
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        facebookRequestCanceled();
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        if (exception instanceof FacebookAuthorizationException) {
//                            facebookRequestFailed();
//                        }
//                    }
//                });
//
//        mShareDialog = new ShareDialog(this);
//        mShareDialog.registerCallback(
//                mCallbackManager,
//                mShareCallback);
//
//        mAccessTokenTracker.startTracking();
//        mProfileTracker.startTracking();
//
//        Intent intent = getIntent();
//        Bundle bundle = intent.getExtras();
//        //get extras
//        if (bundle != null) {
//            if (bundle.containsKey(SHARE_TO_FB_CONTENT_EXTRA)) {
//                mContentToShare = bundle.getString(SHARE_TO_FB_CONTENT_EXTRA);
//            }
//            if (bundle.containsKey(SHARE_TO_FB_TITLE_EXTRA)) {
//                mTitleToShare = bundle.getString(SHARE_TO_FB_TITLE_EXTRA);
//            }
//
//            if (bundle.containsKey(SHARE_TO_FB_URL_EXTRA)) {
//                mUrlToShare = bundle.getString(SHARE_TO_FB_URL_EXTRA);
//            }
//            if (bundle.containsKey(SHARE_TO_FB_ACTION_EXTRA)) {
//                mIsPostToWallRequest = bundle.getBoolean(SHARE_TO_FB_ACTION_EXTRA);
//            }
//        }
//
//        //continue to loginToFb
//        boolean activityManuallyStarted = savedInstanceState == null;
//        if (activityManuallyStarted) {
//            loginWithFacebook();
//        }
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        Log.d(TAG, "onSaveInstanceState");
//        outState.putBoolean(EXTRA_STARTED_AUTOMATICALLY, true);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        mCallbackManager.onActivityResult(requestCode, resultCode, data);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mProfileTracker.stopTracking();
//        mAccessTokenTracker.stopTracking();
//    }
//
//    protected abstract void facebookRequestSuccessful(String fbToken, JSONObject user);
//
//    protected abstract void facebookRequestCanceled();
//
//    protected abstract void facebookRequestFailed();
//
//    protected abstract void facebookShareSuccessful(boolean result);
//
//    protected abstract void facebookShareFailed();
//
//    protected abstract void facebookShareCanceled();
//
//    protected void loginWithFacebook() {
//        AccessToken token = AccessToken.getCurrentAccessToken();
//        if (token != null) {
//            Log.i(TAG, "Facebook accessToken present");
//            if (mIsPostToWallRequest) {
//                shareLinkContentToFbWall();
//            } else {
//                getUserDetails(token);
//            }
//        } else {
//            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(PROFILE_READ, EMAIL_READ, BIRTHDAY_READ));
//        }
//    }
//
//    private void getUserDetails(AccessToken accessToken) {
//        GraphRequest request = GraphRequest.newMeRequest(
//                accessToken,
//                new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(
//                            JSONObject user,
//                            GraphResponse response) {
//                        try {
//
//                            String emailProperty = (user == null) ? null : user.getString(FBFacebookUtils.FB_EMAIL_PARAM);
//                            if (emailProperty != null) {
//                                Log.d(TAG, "sending data to requester");
//                                facebookRequestSuccessful(AccessToken.getCurrentAccessToken().getToken(), user);
//                            } else {
//                                facebookRequestFailed();
//                            }
//                        } catch (JSONException e) {
//                            Log.e(TAG, "Error parsing GraphResponse: ", e);
//                            facebookRequestFailed();
//                        }
//                    }
//                });
//        Bundle parameters = new Bundle();
//        parameters.putString(FB_GRAPH_FIELDS, FBFacebookUtils.READ_PERMISSIONS);
//        request.setParameters(parameters);
//        request.executeAsync();
//    }
//
//    protected void disconnectFromFacebook() {
//        LoginManager.getInstance().logOut();
//    }
//
//    public void shareLinkContentToFbWall() {
//        // check if we are using native app or web to share content
//        mCanPresentShareDialog = ShareDialog.canShow(ShareLinkContent.class);
//        mIsPostToWallRequest = true;
//        postToFbWall();
//    }
//
//    private void postToFbWall() {
//        String content = String.format(mContentToShare, mUrlToShare);
//        Profile profile = Profile.getCurrentProfile();
//        ShareLinkContent linkContent = new ShareLinkContent.Builder()
//                .setContentTitle(mTitleToShare)
//                .setContentDescription(content)
//                .setContentUrl(Uri.parse(mUrlToShare))
//                .build();
//        if (mCanPresentShareDialog) {
//            mShareDialog.show(linkContent);
//        } else if (profile != null) {
//            ShareApi.share(linkContent, mShareCallback);
//        }
//    }
//
//    private FacebookCallback<Sharer.Result> mShareCallback = new FacebookCallback<Sharer.Result>() {
//        @Override
//        public void onCancel() {
//            Log.d(TAG, "onCancel()");
//            facebookShareCanceled();
//
//        }
//
//        @Override
//        public void onError(FacebookException error) {
//            Log.d(TAG, "onError() called with: " + "error = [" + error + "]");
//            facebookShareFailed();
//        }
//
//        @Override
//        public void onSuccess(Sharer.Result result) {
//            Log.d(TAG, "onSuccess()");
//            // if the sharing was done through web dialog, fb native app is not present
//            if(mCanPresentShareDialog) {
//                if(result != null) {
//                    facebookShareSuccessful(true);
//                } else {
//                    facebookShareSuccessful(false);
//                }
//            } else {
//                // sharing from native fb app doesn't return a postId if permission 'publish_actions' was not granted,
//                // our app doesn't need this permission for sharing a content link
//                facebookShareSuccessful(true);
//            }
//        }
//    };
//}