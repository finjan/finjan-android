/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.authentication.models;

import android.content.Context;

import com.avira.common.GSONModel;
import com.avira.common.backend.models.BasePayload;
import com.google.gson.annotations.SerializedName;

//@formatter:off

/**
 * Gson model that is used to create the OE backend json request data
 * for "auth" (login with email) & "register" (register with email) call
 * <p>
 * Sample:
 * {
 *"access_token": "hc47cwtq93doxjs88o3ranb6xcoitqniqysg9peg",
 *"expires_in": 3600,
 *"token_type": "Bearer",
 *"scope": "basic",
 *"refresh_token": "mpzsqvseoxppm93qyahcfcwkbri0w71s4nxsclnz"
 }
 *
 * <p>
 * Created by hui-joo.goh on 7/1/2015.
 */
//@formatter:on
@SuppressWarnings("unused")
public class LoginWithEmailPayload extends BasePayload implements GSONModel {

    // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
    @SerializedName("withLogin")
    private Boolean withLogin;

    public LoginWithEmailPayload(Context context, String email, String password, boolean withLogin) {
        super(context);
        this.withLogin = withLogin;

        id.addLoginInfo(email, password);
    }
}
