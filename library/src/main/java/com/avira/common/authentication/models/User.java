/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.authentication.models;

//@formatter:off
import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Gson model that is used to populate "user" OE backend json response data
 *
 * Sample:
 *
 *   "user":{
 *      "firstName":"Android",
 *      "lastName":"Docs",
 *      "imageUrl":"",
 *      "imageHeight":"400",
 *      "imageWidth":"400",
 *      "email":"androiddocsteam@gmail.com"
 *   }
 *
 * Created by hui-joo.goh on 6/30/2015.
 *
 */
//@formatter:on
@SuppressWarnings("unused")
public class User implements GSONModel {

    // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
    @SerializedName("firstName") private String firstName;
    @SerializedName("lastName") private String lastName;
    @SerializedName("imageUrl") private String imageUrl;
    @SerializedName("imageHeight") private String imageHeight;
    @SerializedName("imageWidth") private String imageWidth;
    @SerializedName("email") private String email;

    /**
     * The user first name
     *
     * @return The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * The user last name
     *
     * @return The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * The user profile image url
     *
     * @return The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * The user profile image height
     *
     * @return The imageHeight
     */
    public String getImageHeight() {
        return imageHeight;
    }

    /**
     * The user profile image width
     *
     * @return The imageWidth
     */
    public String getImageWidth() {
        return imageWidth;
    }

    /**
     * The user registered email
     *
     * @return The email
     */
    public String getEmail() {
        return email;
    }
}
