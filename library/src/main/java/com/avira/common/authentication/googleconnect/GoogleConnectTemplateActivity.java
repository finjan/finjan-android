/*******************************************************************************
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 *******************************************************************************/
package com.avira.common.authentication.googleconnect;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.avira.common.R;
import com.avira.common.activities.BaseFragmentActivity;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Abstract class that encapsulates all the google plus calls
 * <p/>
 * call startGoogleProcedure to start login and the result will be returned trought the abstract methods:
 * doneSuccessfully, requestFailed
 *
 * @author bogdan_oprea
 */

public abstract class GoogleConnectTemplateActivity extends BaseFragmentActivity implements
        GoogleApiClient.OnConnectionFailedListener {

    private final static String TAG = GoogleConnectTemplateActivity.class.getSimpleName();

    private static final int RC_SIGN_IN = 112;
    private GoogleApiClient mGoogleApiClient = null;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle bundle) {
        Log.d(TAG, "onCreate");
        super.onCreate(bundle);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.Loading));
        mProgressDialog.setIndeterminate(true);

        initializeGoogleApiClient();
    }

    private void initializeGoogleApiClient() {
        Log.i(TAG, "initializeGoogleApiClient");
        // Request only the user's ID token, which can be used to identify the
        // user securely to your backend. This will contain the user's basic
        // profile (name, profile picture URL, etc) so you should not need to
        // make an additional call to personalize your application.
        String serverClientId = getServerClientId();
        if(!validateServerClientID(serverClientId)) {
            throw new RuntimeException("Google server client id invalid!");
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(serverClientId)
                .requestEmail()
                .requestServerAuthCode(serverClientId, false)
                .build();

        // Build GoogleAPIClient with the Google Sign-In API and the above options.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");

        showProgressDialog();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        Log.d(TAG, "google auth is: " + opr.isDone());
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    Log.i(TAG, "onResult: " + googleSignInResult.isSuccess());
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgressDialog();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult status - " + result.getStatus());
        hideProgressDialog();
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            final GoogleSignInAccount acct = result.getSignInAccount();

            // auth token is required in order to maintain the compatibility with v1 api used on the server side
            new RetrieveTokenTask(this).execute(acct);
        } else {
            //check if sign-in required
            if(result.getStatus().getStatusCode() == ConnectionResult.SIGN_IN_REQUIRED) {
                signIn();
            } else {
                // Signed out, show unauthenticated UI.
                requestFailed();
            }
        }
    }

    /**
     * Validates that there is a reasonable server client ID in app client, this is only needed
     * to make sure that clients have valid settings
     */
    private boolean validateServerClientID(String clientId) {
        String suffix = ".apps.googleusercontent.com";
        if (!TextUtils.isEmpty(clientId) && !clientId.trim().endsWith(suffix)) {
            String message = "Invalid server client ID in client app, must end with " + suffix;
            Log.e(TAG, message);
            return false;
        }
        return true;
    }

    private void signIn() {
        Log.d(TAG, " Google sign-in ");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        hideProgressDialog();
        requestFailed();
    }

    private void showProgressDialog() {
        if(!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     *  Class used to get the Google Auth Token for server side validation
     */
    private static class RetrieveTokenTask extends AsyncTask<GoogleSignInAccount, Void, String> {

        private GoogleSignInAccount account;
        private final WeakReference<GoogleConnectTemplateActivity> mParentActivity;
        private Context appContext;

        public RetrieveTokenTask(GoogleConnectTemplateActivity activity) {
            mParentActivity = new WeakReference<>(activity);
            appContext = activity.getApplicationContext();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress dialog
            GoogleConnectTemplateActivity activity = mParentActivity.get();
            if(activity != null) {
                activity.showProgressDialog();
            }
        }

        @Override
        protected String doInBackground(GoogleSignInAccount... params) {
                account =  params[0];
                String scopes = "oauth2:profile email";
                String token = null;
                try {
                    token = GoogleAuthUtil.getToken(appContext, account.getEmail(), scopes);
                } catch (GoogleAuthException | IOException e) {
                    Log.e(TAG, "GoogleAuth getToken error: ", e);
                }
                return token;
        }

        @Override
        protected void onPostExecute(final String token) {
            final GoogleConnectTemplateActivity activity = mParentActivity.get();
            if(activity != null) {
                // dismiss the progress dialog
                activity.hideProgressDialog();
                if(token == null) {
                    activity.requestFailed();
                } else {
                    Plus.PeopleApi.load(activity.getGoogleApiClient(), account.getId()).setResultCallback(new ResultCallback<People.LoadPeopleResult>() {
                        @Override
                        public void onResult(@NonNull People.LoadPeopleResult loadPeopleResult) {
                            PersonBuffer personBuffer = loadPeopleResult.getPersonBuffer();
                            if(personBuffer != null && personBuffer.getCount() > 0) {
                                Person person = personBuffer.get(0);
                                Log.d(TAG, "Person loaded");
                                activity.doneSuccessfully(person, account.getEmail(), token);
                                personBuffer.close();
                            } else {
                                activity.requestFailed();
                            }
                        }
                    });
                }
            }
        }
    }

    private GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    protected abstract String getServerClientId();

    protected abstract void doneSuccessfully(Person person, String email, String accessToken);

    protected abstract void requestFailed();
}
