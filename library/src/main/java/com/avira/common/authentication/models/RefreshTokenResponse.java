package com.avira.common.authentication.models;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anurag on 14/06/17.
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class RefreshTokenResponse implements GSONModel {

    @SerializedName("access_token")
    protected String  access_token;
    @SerializedName("expires_in")
    protected int expires_in;
    @SerializedName("token_type")
    protected String token_type;
    @SerializedName("scope")
    protected String scope;
    @SerializedName("refresh_token")
    protected String refresh_token;

    public String getAccess_token() {
        return access_token;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public String getToken_type() {
        return token_type;
    }

    public String getScope() {
        return scope;
    }

    public String getRefresh_token() {
        return refresh_token;
    }
}
