/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.authentication.models;

import com.avira.common.GSONModel;
import com.avira.common.authentication.facebookconnect.FBFacebookUtils;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

//@formatter:off
/**
 * Gson model that is used to create "details" OE backend json request data
 *
 * Sample:
 *
 *   "details":{
 *      "birthDate":"PersonBirthday",
 *      "firstName":"PersonName",
 *      "language":"PersonLanguage",
 *      "lastName":"PersonName",
 *      "salutation":"0"
 *   }
 *
 * Created by hui-joo.goh on 6/26/2015.
 */
//@formatter:on
@SuppressWarnings("unused")
public class Details implements GSONModel{

    // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
    @SerializedName("birthDate") private String birthDate;
    @SerializedName("firstName") private String firstName;
    @SerializedName("lastName") private String lastName;
    @SerializedName("language") private String language;
    @SerializedName("salutation") private String salutation;

    /**
     * Retrieve all the available user information from the provided parameter
     * and populate into the relevant fields of this class
     *
     * @param currentPerson The current person
     */
    public Details(Person currentPerson) {
        this.birthDate = currentPerson.getBirthday();
        this.firstName = currentPerson.getDisplayName();
        this.lastName = currentPerson.getDisplayName();
        this.language = currentPerson.getLanguage();
        this.salutation = Integer.toString(currentPerson.getGender());
    }

    /**
     * Retrieve all the available user information from the provided parameter
     * and populate into the relevant fields of this class
     *
     * @param user The user information
     */
    public Details(JSONObject user) throws JSONException {
        this.birthDate = user.optString(FBFacebookUtils.FB_BIRTHDAY_PARAM);
        this.firstName = user.getString(FBFacebookUtils.FB_FIRST_NAME_PARAM);
        this.lastName = user.getString(FBFacebookUtils.FB_LAST_NAME_PARAM);
    }
}
