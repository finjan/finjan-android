/*******************************************************************************
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 *******************************************************************************/
package com.avira.common.authentication.facebookconnect;

/**
 * Contatains config info and simple functions that might be used elsewhere. For ex: use canUseFacebook() to see if you
 * should show the facebook button or not
 *
 * @author bogdan_oprea
 *
 */
public class FBFacebookUtils {
    public static final String READ_PERMISSIONS = "id,first_name,last_name,email,birthday";
    public static final String FB_BIRTHDAY_PARAM = "birthday";
    public static final String FB_FIRST_NAME_PARAM = "first_name";
    public static final String FB_LAST_NAME_PARAM = "last_name";
    public static final String FB_EMAIL_PARAM = "email";
    public static final String FB_ID_PARAM = "id";

}
