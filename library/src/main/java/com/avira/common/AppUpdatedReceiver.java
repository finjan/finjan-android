package com.avira.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.avira.common.utils.WhatsNewDialog;

/**
 * @author ovidiu.buleandra
 * @since 17-Jan-17
 */

public class AppUpdatedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        WhatsNewDialog.appWasUpdated(context);
    }
}
