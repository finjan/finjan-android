package com.avira.common;

import android.content.Context;

import com.avira.common.utils.SharedPreferencesUtilities;

/**
 * @author ovidiu.buleandra
 * @since 24.08.2015
 */
public class GeneralPrefs {

    private static final String ANONYMOUS_USER_KEY = "anonymous_user_key";
    private static final String REGISTERED_KEY = "registered_anonymously"; // retaining key value for backward compatibility
    private static final String DEVICE_ADMINISTRATOR_KEY = "device_aministrator_key";

    private static final String APP_ID = "app_id";
    private static final String PARTNER_ID = "partner";
    private static final String DEFAULT_PARTNER_ID = "avira";
    private static final String OE_SERVER_OPERATION = "operation";

    private static final String USER_NAME = "USER_NAME";
    private static final String GP_EMAIL_ID= "GP_EMAIL_ID";
    private static final String GP_EMAIL_PASSWORD= "GP_EMAIL_PASSWORD";


    /**
     * last version code of the app the gcm id was updated
     * the app checks this version against current version and triggers a new request when they don't correspond
     */
    private static final String GCM_UPDATE_LAST_VERSION = "prefs_gcm_update_last_version";

    /**
     * @return True if this device is known to our backend as a anonymous user, otherwise false.
     */
    public static boolean isAnonymous(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, ANONYMOUS_USER_KEY);
    }

    /**
     * @param value True to indicate that this device is known to our backend as a anonymous user, otherwise false.
     */
    public static void setAnonymous(Context context, boolean value) {
        SharedPreferencesUtilities.putBoolean(context, ANONYMOUS_USER_KEY, value);
    }

    /**
     * @param value True to indicate that the user has registered (not anonymous anymore), otherwise false.
     */
    public static void setRegistered(Context context, boolean value) {
        SharedPreferencesUtilities.putBoolean(context, REGISTERED_KEY, value);
    }

    /**
     * @return True if the user has been registered (not anonymous anymore), otherwise false.
     */
    public static boolean isRegistered(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, REGISTERED_KEY);
    }

    public static boolean isDeviceAdmin(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, DEVICE_ADMINISTRATOR_KEY);
    }

    public static void setDeviceAdmin(Context context, boolean value) {
        SharedPreferencesUtilities.putBoolean(context, DEVICE_ADMINISTRATOR_KEY, value);
    }

    public static void setGCMLastVersionCode(Context context, int versionCode) {
        SharedPreferencesUtilities.putInt(context, GCM_UPDATE_LAST_VERSION, versionCode);
    }

    public static int getGCMLastVersionCode(Context context) {
        return SharedPreferencesUtilities.getInt(context, GCM_UPDATE_LAST_VERSION, -1);
    }

    public static void setAppId(Context context, String appId) {
        SharedPreferencesUtilities.putString(context, APP_ID, appId);
    }

    public static String getAppId(Context context) {
        return SharedPreferencesUtilities.getString(context, APP_ID);
    }

    public static String getAppId(Context context, String defaultValue) {
        return SharedPreferencesUtilities.getString(context, APP_ID, defaultValue);
    }


    public static void setPartnerId(Context context, String partnerId) {
        SharedPreferencesUtilities.putString(context, PARTNER_ID, partnerId);
    }

    public static String getPartnerId(Context context) {
        return SharedPreferencesUtilities.getString(context, PARTNER_ID, DEFAULT_PARTNER_ID);
    }

    public static void setOeServerOperation(Context context, String value) {
        SharedPreferencesUtilities.putString(context, OE_SERVER_OPERATION, value);
    }

    public static String getOeServerOperation(Context context) {
        return SharedPreferencesUtilities.getString(context, OE_SERVER_OPERATION, "");
    }

    public static void  setUserName(Context context, String username) {
        SharedPreferencesUtilities.putString(context, USER_NAME, username);
    }

    public static String getUserName(Context context) {
        return SharedPreferencesUtilities.getString(context, USER_NAME, "");
    }


    public static void  setEmailId(Context context, String emailId) {
        SharedPreferencesUtilities.putString(context, GP_EMAIL_ID, emailId);
    }

    public static String getEmailId(Context context) {
        return SharedPreferencesUtilities.getString(context, GP_EMAIL_ID, "");
    }
    public static void  setPassword(Context context, String password) {
        SharedPreferencesUtilities.putString(context, GP_EMAIL_PASSWORD, password);
    }

    public static String getPassword(Context context) {
        return SharedPreferencesUtilities.getString(context, GP_EMAIL_PASSWORD, "");
    }

}
