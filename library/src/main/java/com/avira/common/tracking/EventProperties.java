//package com.avira.common.tracking;
//
//import android.util.Log;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
///**
// * A json object to be used to keep track of tracking properties
// */
//public class EventProperties extends JSONObject{
//    private static final String TAG = EventProperties.class.getSimpleName();
//    public void putProperty(String name, int value){
//        try {
//            put(name,value);
//        } catch (JSONException e) {
//            Log.e(TAG, "Json Exception", e);
//        }
//    }
//    public void putProperty(String name, long value){
//        try {
//            put(name,value);
//        } catch (JSONException e) {
//            Log.e(TAG, "Json Exception", e);
//        }
//    }
//    public void putProperty(String name, Object value){
//        try {
//            put(name,value);
//        } catch (JSONException e) {
//            Log.e(TAG, "Json Exception", e);
//        }
//    }
//    public void putProperty(String name, boolean value){
//        try {
//            put(name,value);
//        } catch (JSONException e) {
//            Log.e(TAG, "Json Exception", e);
//        }
//    }
//
//    public void putProperty(String name, double value){
//        try {
//            put(name,value);
//        } catch (JSONException e) {
//            Log.e(TAG, "Json Exception", e);
//        }
//    }
//}
