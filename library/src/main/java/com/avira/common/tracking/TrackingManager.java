///*
//* Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
//*/
//
//package com.avira.common.tracking;
//
//import android.content.Context;
//import android.text.TextUtils;
//import android.util.Log;
//
//import com.avira.common.utils.SharedPreferencesUtilities;
//import com.mixpanel.android.mpmetrics.MixpanelAPI;
//
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Class that uses Mixpanel API for event tracking
// *
// * @author hui-joo.goh
// */
//@SuppressWarnings("unused")
//public class TrackingManager {
//    private final static String TAG = TrackingManager.class.getSimpleName();
//
//    private static TrackingManager INSTANCE = null;
//    private MixpanelAPI mMixpanelAPI = null;
//    private ITrackingConfig mTrackingConfig;
//    private Context mAppContext;
//
//    private TrackingManager(Context appContext, String mixPanelToken, ITrackingConfig trackingConfig) {
//        if (TextUtils.isEmpty(mixPanelToken)) {
//            throw new IllegalArgumentException("Invalid initialization arguments");
//        } else {
//            mAppContext = appContext;
//            // get instance of MixPanel
//            mMixpanelAPI = MixpanelAPI.getInstance(appContext, mixPanelToken);
//            mTrackingConfig = trackingConfig;
//        }
//    }
//
//    public static TrackingManager getInstance() {
//        if (INSTANCE != null) {
//            return INSTANCE;
//        }
//
//        throw new IllegalStateException("Mixpanel was not initialized yet");
//    }
//
//    /**
//     * Initialize mixpanel
//     *
//     * @param context
//     * @param mixPanelToken the mixpanel token for analytics tracking
//     */
//    public static void initialize(Context context, String mixPanelToken) {
//        if (INSTANCE == null) {
//            INSTANCE = new TrackingManager(context.getApplicationContext(), mixPanelToken, null);
//        }
//    }
//
//    /**
//     * Initialize mixpanel
//     *
//     * @param context
//     * @param mixPanelToken  the mixpanel token for analytics tracking
//     * @param trackingConfig an object that implements {@code ITrackingConfig} to check if a feature is tracked or not
//     */
//    public static void initialize(Context context, String mixPanelToken, ITrackingConfig trackingConfig) {
//        if (INSTANCE == null) {
//            INSTANCE = new TrackingManager(context.getApplicationContext(), mixPanelToken, trackingConfig);
//        }
//    }
//
//    /**
//     * Track an Event
//     *
//     * @param event
//     */
//    public void trackFeature(TrackingEvent event) {
//        trackFeature(event, null);
//    }
//
//    /**
//     * Track an Event with properties
//     *
//     * @param event
//     * @param properties
//     */
//    public void trackFeature(TrackingEvent event, JSONObject properties) {
//        boolean isTracked = true;
//
//        if (mTrackingConfig != null) {
//            isTracked = mTrackingConfig.isTracked(event);
//        }
//
//        if (isTracked) {
//            if (mMixpanelAPI == null) {
//                return;
//            }
//            mMixpanelAPI.track(event.getEvent(), properties);
//            Log.d(TAG, "Tracked : " + event.getEvent());
//        }
//    }
//
//
//    public void signalTimeEventStarted(TrackingEvent event) {
//        boolean isTracked = true;
//
//        if (mTrackingConfig != null) {
//            isTracked = mTrackingConfig.isTracked(event);
//        }
//
//        if (isTracked) {
//            if (mMixpanelAPI == null) {
//                return;
//            }
//            mMixpanelAPI.timeEvent(event.getEvent());
//        }
//    }
//
//
//    public void signalTimeEventEnded(TrackingEvent event) {
//        boolean isTracked = true;
//
//        if (mTrackingConfig != null) {
//            isTracked = mTrackingConfig.isTracked(event);
//        }
//
//        if (isTracked) {
//            if (mMixpanelAPI == null) {
//                return;
//            }
//            mMixpanelAPI.track(event.getEvent());
//            Log.d(TAG, "Tracked : " + event.getEvent());
//        }
//    }
//
//    public void trackLimitedEvent(TrackingEvent event, int limitCount) {
//        trackLimitedEvent(event, null, limitCount);
//    }
//
//    public void registerSuperProperties(JSONObject props) {
//        mMixpanelAPI.registerSuperProperties(props);
//    }
//
//    /**
//     * helper method to track an event for a limited number of times. mainly used to avoid spamming an event that doesn't
//     * give too much info but needs to be tracked to see if the users actually hit that particular feature
//     * <br/>
//     * using a naming convention ('track_' + event name + '_limit') it stores in shared preferences the current count and
//     * if that count goes over the limit it disregards the call and nothing happens
//     *
//     * @param event      event name (choose something meaningful but not too long)
//     * @param payload    custom information attached to the event
//     * @param limitCount number of times this event is allowed to register
//     */
//    public void trackLimitedEvent(TrackingEvent event, JSONObject payload, int limitCount) {
//        String prefEvent = "tracking_" + event.getEvent().toLowerCase() + "_limit";
//        int current = SharedPreferencesUtilities.getInt(mAppContext, prefEvent, 0) + 1;
//        if (current <= limitCount) {
//            trackFeature(event, payload);
//            SharedPreferencesUtilities.putInt(mAppContext, prefEvent, current);
//        }
//    }
//
//    /**
//     * should be called when the user login with our own user id
//     * https://mixpanel.com/docs/integration-libraries/using-mixpanel-alias
//     *
//     * @param distinctId
//     */
//    public void identify(String distinctId) {
//        if (distinctId != null) {
//            mMixpanelAPI.identify(distinctId);
//            mMixpanelAPI.getPeople().identify(distinctId);
//        } else {
//            mMixpanelAPI.getPeople().identify(mMixpanelAPI.getDistinctId());
//        }
//    }
//
//    /**
//     * should be called when the user signup with our own user id
//     * https://mixpanel.com/docs/integration-libraries/using-mixpanel-alias
//     *
//     * @param newId
//     * @param originalId
//     */
//    public void alias(String newId, String originalId) {
//        mMixpanelAPI.alias(newId, originalId);
//    }
//
//
//    public MixpanelAPI.People getPeople() {
//        return mMixpanelAPI.getPeople();
//    }
//
//
//    /**
//     * Add the given amount to an existing event on the identified user. If the user does not
//     * already have the associated property, the amount will be added to zero.
//     * To reduce a property, provide a negative number for the value.
//     *
//     * @param event event
//     * @param value amount to be added
//     */
//    public void incrementInt(TrackingEvent event, int value) {
//        Map<String, Integer> properties =
//                new HashMap<>();
//        properties.put(event.getEvent(), value);
//        mMixpanelAPI.getPeople().increment(properties);
//    }
//
//    /**
//     * Push all queued Mixpanel events and People Analytics changes to Mixpanel servers.
//     * <p/>
//     * Events and People messages are pushed gradually throughout the lifetime of your application. This means that to
//     * ensure that all messages are sent to Mixpanel when your application is shut down, you will need to call flush()
//     * to let the Mixpanel library know it should send all remaining messages to the server. We strongly recommend
//     * placing a call to flush() in the onDestroy() method of your main application activity.
//     */
//    public void flush() // NO_UCD (use default)
//    {
//        if (mMixpanelAPI != null) {
//            mMixpanelAPI.flush();
//        }
//    }
//
//    public interface ITrackingConfig {
//        boolean isTracked(TrackingEvent event);
//    }
//
//}
