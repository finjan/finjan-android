//package com.avira.common.tracking;
//
///**
// * A simple class that represent an event and construct the event name based on our rules
// */
//public class TrackingEvent {
//    private String mFeature;
//    private String mFullEventString;
//    private static final String SEPERATOR = "_";
//
//    public TrackingEvent(String eventName) {
//        mFeature = "";
//        mFullEventString = eventName;
//    }
//
//    public TrackingEvent(String appName, String featureName, String eventName){
//        this(featureName, buildEventName(appName, featureName, eventName));
//    }
//
//    protected TrackingEvent(String featureName, String fullEventString){
//        mFullEventString = fullEventString;
//        mFeature = featureName;
//    }
//
//    private static String buildEventName(String appName, String featureName, String eventName){
//        StringBuilder sb = new StringBuilder();
//        sb.append(appName);
//        sb.append(SEPERATOR);
//        sb.append(featureName);
//        sb.append(SEPERATOR);
//        sb.append(eventName);
//        return sb.toString();
//    }
//
//    public String getFeatureName(){
//        return mFeature;
//    }
//
//    public String getEvent(){
//        return mFullEventString;
//    }
//}
