package com.avira.common.id.models;

import com.avira.common.GSONModel;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/**
 * Response data from uid update call
 * {
 *  "data":{
 *     "relationships":{
 *        "partner":{
 *           "data":{
 *              "type":"partners",
 *              "id":"avira"
 *           }
 *        },
 *        "user":{
 *           "data":{
 *              "type":"users",
 *              "id":3000057364
 *           }
 *        }
 *     },
 *     "attributes":{
 *        "type":"phone",
 *        "name":"",
 *        "user_state":0,
 *        "brand":"LGE",
 *        "hardware_id":"da4b9237bacccdf19c0760cab7aec4a8359010b0",
 *        "date_churned":null,
 *        "state":"active",
 *        "others":{
 *           "devAdmin":"OFF",
 *           "osver":"5.1",
 *           "agver":"4.1.3659",
 *           "imei":"356a192b7913b04c54574d18c28d46e6395428ab",
 *           "serial":"0af5794202997e4e",
 *           "phoneNo":"unknown"
 *        },
 *        "date_added":"2016-02-29T12:57:48Z",
 *        "dateUpdated":null,
 *        "country":"",
 *        "os":"android",
 *        "model":"Nexus 5"
 *     },
 *     "type":"devices",
 *     "id":4412452
 *  }
 *}
 */
public class UidUpdateResponse  implements GSONModel {
    @SerializedName("data") private JsonObject mData;

    public String getDeviceId(){
        return mData.get("id").toString();
    }
}
