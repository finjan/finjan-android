package com.avira.common.id;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.RequestFuture;
import com.avira.common.GeneralPrefs;
import com.avira.common.backend.Backend;
import com.avira.common.backend.oe.OeRequest;
import com.avira.common.backend.oe.OeRequestQueue;
import com.avira.common.database.Settings;
import com.avira.common.events.ServerUIDUpdatedEvent;
import com.avira.common.id.models.UidUpdatePayload;
import com.avira.common.id.models.UidUpdateResponse;
import com.avira.common.utils.AccessTokenUtil;
import com.avira.common.utils.HashUtility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.TaskParams;
//import com.google.android.gms.gcm.GcmNetworkManager;
//import com.google.android.gms.gcm.GcmTaskService;
//import com.google.android.gms.gcm.OneoffTask;
//import com.google.android.gms.gcm.TaskParams;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import de.greenrobot.event.EventBus;

/**
 * service used to schedule an update with the backend
 * in case the update is successful then all calls to the server from now on will be marked with the new hardware id
 *
 * @author ovidiu.buleandra
 * @since 25.03.2016
 */
public class UpdateUIDService extends GcmTaskService {
    private static final String TAG = UpdateUIDService.class.getSimpleName();

    private static final int MAXIMUM_WINDOW = 30; // 30 seconds
    private static final String INVALID_SERVER_ID = "0";

    private static final String DEVICES_ENDPOINT = "%sdevices/%s?access_token=%s";

    /**
     * schedule an update of the hardware id with the backend. in case it is successful the this service takes care
     * to cleanof the old type of ids
     * @param context
     */
    public static void scheduleUpdate(Context context) {
        if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {
            Log.d(TAG, "Google Play Services detected! Scheduling an update");

            final String serverDeviceId = Settings.readDeviceId();
            if(TextUtils.isEmpty(serverDeviceId) || INVALID_SERVER_ID.equals(serverDeviceId)) {
                // make sure not to schedule the UID update when the user is in an invalid state
                // missing the device id or if it's '0' - a known invalid value - will result in an invalid access token
                // the server will try to validate the call and will use CPU and bandwidth for it
                return;
            }
            OneoffTask task = new OneoffTask.Builder()
                    .setService(UpdateUIDService.class)
                    .setRequiredNetwork(OneoffTask.NETWORK_STATE_CONNECTED)
                    .setExecutionWindow(1, MAXIMUM_WINDOW)
                    .setPersisted(true)
                    .setTag(TAG)
                    .setUpdateCurrent(true)
                    .build();

            GcmNetworkManager.getInstance(context).schedule(task);
        } else {
            // TODO schedule normal task
            Log.d(TAG, "Google Play Services not available! Using a normal scheduler.");
        }
    }

    @Override
    public int onRunTask(TaskParams taskParams) {
        return updateUID(getApplicationContext());
    }

    private static int updateUID(Context context) {
        if(!Settings.isAvailable()){
            return GcmNetworkManager.RESULT_RESCHEDULE;
        }

        final String serverDeviceId = Settings.readDeviceId();
        final String currentId = HardwareId.get(context);
        if(TextUtils.isEmpty(serverDeviceId) || TextUtils.isEmpty(currentId) || INVALID_SERVER_ID.equals(serverDeviceId)) {
            // invalid token
            return GcmNetworkManager.RESULT_FAILURE;
        }
        final String accessToken = AccessTokenUtil.generateToken(GeneralPrefs.getAppId(context), currentId, serverDeviceId);

//        String requestUrl = String.format(DEVICES_ENDPOINT,
//                Backend.BACKEND_URL.replace("/android", ""), serverDeviceId, accessToken);
        String requestUrl = String.format(DEVICES_ENDPOINT,
                "https://ssld.oes.avira.com/android/v2/".replace("/android", ""), serverDeviceId, accessToken);
        UidUpdatePayload postData = new UidUpdatePayload(HashUtility.sha1(HardwareId.getHardwareId(context)));
        RequestFuture<UidUpdateResponse> future = RequestFuture.newFuture();
        OeRequest<UidUpdatePayload, UidUpdateResponse> request =
                new OeRequest<>(Request.Method.PUT, requestUrl, postData, UidUpdateResponse.class, future, future);

        OeRequestQueue.getInstance(context).add(request);

        try {
            // blocking call
            UidUpdateResponse response = future.get(OeRequest.DEFAULT_TIMEOUT_MS, TimeUnit.MILLISECONDS);
            String receivedDeviceId;
            // when we receive the right response we overwrite the server device id because it might get changed after
            // a device merge in the backend
            if(response != null && !TextUtils.isEmpty((receivedDeviceId = response.getDeviceId()))) {
                Log.d(TAG, String.format("serverDeviceId=%s receivedServerDeviceId=%s", serverDeviceId, receivedDeviceId));
                Settings.saveDeviceId(receivedDeviceId);
            }
            // clear all old ids so we only have the newest type stored
            HardwareId.clearOldIds(context);
            // notify all listeners about what happened
            EventBus.getDefault().post(new ServerUIDUpdatedEvent());
            return GcmNetworkManager.RESULT_SUCCESS;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            return GcmNetworkManager.RESULT_RESCHEDULE;
        } catch (Exception e) {
            return GcmNetworkManager.RESULT_FAILURE;
        }
    }
}
