/*
 * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
 */
package com.avira.common.id.models;

import com.avira.common.GSONModel;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/**
 * Payload to update the uid
 * {
 *     "data": {
 *         "attributes": {
 *             "hardware_id": "3592340452421890984209"
 *          }
 *      }
 * }
 */
public class UidUpdatePayload implements GSONModel{
    @SerializedName("data") private JsonElement mData;
    private static final String FORMAT ="{\"attributes\":{\"hardware_id\":\"%s\"}}";
    public UidUpdatePayload(String hwId){
        Gson gson = new Gson();
        mData = gson.fromJson(String.format(FORMAT, hwId), JsonObject.class);
    }

}
