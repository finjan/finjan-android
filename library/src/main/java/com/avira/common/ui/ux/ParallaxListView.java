/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ui.ux;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

/**
 * Elastic list view that support parallax effect. This list view takes a foreground layout and a background layout in
 * its container to create the parallax effect. The background layout will always take at least half the height of the
 * screen.
 *
 * @author yushu
 */
public class ParallaxListView extends ElasticListView {

    private static final float PARALLAX_MULTIPLIER = 0.5f;
    private View mHeader;
    private float mHeaderViewInitialTranslationY = 0;

    public ParallaxListView(Context context) {
        super(context);
    }

    public ParallaxListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ParallaxListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void addHeaderView(View view) {
        super.addHeaderView(view);
        if (getHeaderViewsCount() > 1) {
            throw new IllegalStateException("parallax list view has only one header");
        }
        mHeader = view;
        mHeaderViewInitialTranslationY = getTranslationY(mHeader);

    }

    @Override
    public void addHeaderView(View view, Object data, boolean isSelectable) {
        super.addHeaderView(view, data, isSelectable);
        if (getHeaderViewsCount() > 1) {
            throw new IllegalStateException("parallax list view has only one header");
        }
        mHeader = view;
        mHeaderViewInitialTranslationY = getTranslationY(mHeader);

    }

    @Override
    protected void onScrollChanged(int x, int y, int oldX, int oldY) {
        // Fix y scroll position:
        // http://stackoverflow.com/questions/10808387/android-getting-exact-scroll-position-in-listview
        View firstListViewItem = getChildAt(0);
        if (firstListViewItem != null) {
            int firstListViewItemTop = -firstListViewItem.getTop() + (int) getTranslationY(mHeader);
            y = firstListViewItemTop + getFirstVisiblePosition() * firstListViewItem.getHeight();
        }

        super.onScrollChanged(x, y, oldX, oldY);

        // Although parallax effect is already handled in onOverScrolled, it needs to be repeated here in order for
        // parallax to work when user scroll downwards IF the list view exceeds one page
        int scrollY = -mHeader.getTop();
        if (scrollY > 0) {
            slowDownHeaderViewTranslation(scrollY);
        }
    }

    @SuppressLint("NewApi")
    private float getTranslationY(View view) {
        if (view != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return view.getTranslationY();
        }
        return 0;
    }

    @SuppressLint("NewApi")
    private void setTranslationY(View view, float y) {
        if (view != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            view.setTranslationY(y);
        }

    }

    @Override
    public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);

        // onOverScrolled is triggered when scrolling upward & downward, make header view move slower than the user
        // scroll speed to achieve parallax effect
        slowDownHeaderViewTranslation(scrollY);
    }

    ;

    /**
     * Slow down the header view translation to achieve the parallax effect where the list view speed is in sync with
     * user scroll motion but the header view is moving slower by the amount defined in PARALLAX_MULTIPLIER
     *
     * @param scrollY New Y scroll value in pixels
     */
    private void slowDownHeaderViewTranslation(int scrollY) {
        if (mHeader != null) {
            float parallaxY = scrollY * PARALLAX_MULTIPLIER;
            setTranslationY(mHeader, mHeaderViewInitialTranslationY + parallaxY);
        }
    }
}
