package com.avira.common.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.drawerlayout.widget.DrawerLayout;

import com.avira.common.R;

/**
 * helper class that uses a well defined layout for a drawer and populates it with application specific stuff
 * the activity used as context must implement {@link DrawerClickListener} otherwise it will crash.
 * <p /> this is needed to access all the clicks inside the drawer
 *
 * <p/>
 * <b>typical usage example</b><br/><br/>
 * new DrawerBuilder(this)<br/>
    .setIcon(R.drawable.drawer_icon)<br/>
    .setHeaderBackground(R.drawable.drawer_background)<br/>
    .setProfileName(userProfile.getFirstName() + " " +  userProfile.getLastName())<br/>
    .setProfileEmail(userProfile.getEmail())<br/>
    .setProfilePic(userProfile.getPictureBitmap())<br/>
    .addMenuItem(R.drawable.icon, R.string.Settings)<br/>
    .addMenuItem(R.drawable.icon_facebook, R.string.MyProfile)<br/>
    .addMenuItem(R.drawable.icon_google_plus, R.string.Help)<br/>
    .addMenuItem(R.drawable.icon_twitter, R.string.About)<br/>
    .addDivider()<br/>
    .addCustomContent(custom)<br/>
    .hideShareSection()<br/>
    .build(((ViewGroup) findViewById(R.id.drawer_content)));<br/>
 * @author ovidiu.buleandra
 * @since 14.12.2015
 */
public class DrawerBuilder implements View.OnClickListener {

    private final Context mContext;
    private final LayoutInflater mInflater;
    private final ScrollView mLayout;

    private DrawerClickListener mListener;
    private int mMenuIdx;

    public interface DrawerClickListener {
        /**
         * implement this to access clicks inside the drawer
         * <br/>
         * each menu item has the id set as its index in the list (first item is 0)
         * the other stuff has static ids
         * <ul>
         * <li>R.id.drawer_back - for back button => closes the drawer</li>
         * <li>R.id.share_facebook_button - share on facebook</li>
         * <li>R.id.share_twitter_button - share on twitter</li>
         * <li>R.id.share_gplus_button - share on google plus</li>
         * </ul>
         * @param clickedView
         * @param id
         */
        void onDrawerClickListener(View clickedView, int id);
    }

    public DrawerBuilder(Activity parentActivity) {
        try {
            mListener = (DrawerClickListener) parentActivity;
        } catch (ClassCastException e) {
            throw new ClassCastException(parentActivity.toString()
                    + " must implement " + DrawerClickListener.class.getSimpleName());
        }

        mContext = parentActivity;
        mInflater = LayoutInflater.from(mContext);
        mLayout = (ScrollView) mInflater.inflate(R.layout.drawer_layout, null, false);

        mMenuIdx = 0;

        mLayout.findViewById(R.id.drawer_back).setOnClickListener(this);
        mLayout.findViewById(R.id.share_facebook_button).setOnClickListener(this);
        mLayout.findViewById(R.id.share_twitter_button).setOnClickListener(this);
        mLayout.findViewById(R.id.share_gplus_button).setOnClickListener(this);
    }

    public DrawerBuilder setIcon(@DrawableRes int resId) {
        ((ImageView)mLayout.findViewById(R.id.icon)).setImageResource(resId);
        return this;
    }

    public DrawerBuilder setHeaderBackground(@DrawableRes int resId) {
        ((ImageView)mLayout.findViewById(R.id.header_background)).setImageResource(resId);
        return this;
    }

    public DrawerBuilder setProfileName(String value) {
        TextView tvProfileName = (TextView)mLayout.findViewById(R.id.profile_name);
        tvProfileName.setText(value);
        tvProfileName.setVisibility(View.VISIBLE);

        return this;
    }

    public DrawerBuilder setProfileEmail(String value) {
        TextView tvEmail = (TextView)mLayout.findViewById(R.id.profile_email);
        tvEmail.setText(value);
        tvEmail.setVisibility(View.VISIBLE);
        return this;
    }

    /**
     * uses the provided bitmap to create a scaled round bitmap to be used as profile picture
     * @param profilePic the original bitmap
     * @return builder for chaining
     */
    public DrawerBuilder setProfilePic(Bitmap profilePic) {
        if(profilePic != null) {
            final Bitmap scaledPic = Bitmap.createScaledBitmap(profilePic,
                    (int) mContext.getResources().getDimension(R.dimen.drawer_profile_image_size),
                    (int) mContext.getResources().getDimension(R.dimen.drawer_profile_image_size),
                    true);

            final int profilePicMargin = (int) mContext.getResources().getDimension(R.dimen.drawer_profile_margin);
            final RoundDrawable profileDrawable = new RoundDrawable(scaledPic, profilePicMargin, false);
            final ImageView targetView = ((ImageView) mLayout.findViewById(R.id.profile_pic));
            targetView.setVisibility(View.VISIBLE);
            targetView.setImageDrawable(profileDrawable);
        }
        return this;
    }

    public DrawerBuilder hideProfileInfo(){
        mLayout.findViewById(R.id.profile_pic).setVisibility(View.GONE);
        mLayout.findViewById(R.id.profile_email).setVisibility(View.GONE);
        mLayout.findViewById(R.id.profile_name).setVisibility(View.GONE);
        return this;
    }

    /**
     * uses the provided drawable resource to create the round profile picture
     * @param resId drawable resource id
     * @return builder for chaining
     */
    public DrawerBuilder setProfilePic(@DrawableRes int resId) {
        final Bitmap profilePic = BitmapFactory.decodeResource(mContext.getResources(), resId);
        setProfilePic(profilePic);
        profilePic.recycle();
        return this;
    }

    /**
     * adds a menu item in the dedicated section inside the drawer. the order of the items is given by the order of
     * calls to this method
     * @param iconResId
     * @param textResId
     * @return
     */
    public DrawerBuilder addMenuItem(@DrawableRes int iconResId, @StringRes int textResId) {
        final ViewGroup parent = (ViewGroup) mLayout.findViewById(R.id.drawer_content);
        final ViewGroup itemLayout = (ViewGroup) mInflater.inflate(R.layout.drawer_item, parent, false);
        ((ImageView)itemLayout.findViewById(R.id.icon)).setImageResource(iconResId);
        ((TextView)itemLayout.findViewById(R.id.label)).setText(textResId);
        itemLayout.setTag(mMenuIdx++);
        itemLayout.setOnClickListener(this);
        parent.addView(itemLayout);
        return this;
    }

    /**
     * adds custom content to the drawer
     * </br>you have to inflate the view your self as you might need to link things inside that specific layout before
     * is added to the drawer
     * @param view
     * @return
     */
    public DrawerBuilder addCustomContent(View view) {
        final ViewGroup parent = (ViewGroup) mLayout.findViewById(R.id.drawer_content);
        parent.addView(view);
        return this;
    }

    /**
     * adds a horizontal 1dp high grady divider
     * @return
     */
    public DrawerBuilder addDivider() {
        final ViewGroup parent = (ViewGroup) mLayout.findViewById(R.id.drawer_content);
        View layout = mInflater.inflate(R.layout.drawer_divider, parent, false);
        parent.addView(layout);
        return this;
    }

    /**
     * changes the text in the share section to the specified string resource
     * @param resId text resource to be used
     * @return
     */
    public DrawerBuilder setShareDescription(@StringRes int resId) {
        ((TextView)mLayout.findViewById(R.id.share_desc)).setText(resId);
        return this;
    }

    /**
     * hides the share section
     * @return
     */
    public DrawerBuilder hideShareSection() {
        mLayout.findViewById(R.id.share_content).setVisibility(View.GONE);
        return this;
    }

    public DrawerBuilder addEmptyItem() {
        final ViewGroup parent = (ViewGroup) mLayout.findViewById(R.id.drawer_content);
        View layout = mInflater.inflate(R.layout.drawer_spacer, parent, false);
        parent.addView(layout);
        return this;
    }

    /**
     * the final method you should call when you finish configuring the drawer. you need to pass a {@link ViewGroup}
     * parent that the drawer layout will be added to. you can always use a {@link android.widget.FrameLayout} inside
     * the declaring xml layout for the {@link DrawerLayout}
     * @param drawerRoot the parent of the drawer
     * @return
     */
    public DrawerBuilder build(ViewGroup drawerRoot) {
        drawerRoot.addView(mLayout);
        return this;
    }

    @Override
    public void onClick(View v) {
        mListener.onDrawerClickListener(v, v.getTag() == null ? v.getId() : (int)v.getTag());
    }

    public View getMainLayout() {
        return mLayout;
    }

    public ViewGroup getContentLayout() {
        final ViewGroup parent = (ViewGroup) mLayout.findViewById(R.id.drawer_content);
        return parent;
    }
}
