package com.avira.common.ui;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ComposeShader;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * code taken from http://www.curious-creature.com/2012/12/11/android-recipe-1-image-with-rounded-corners/
 *
 * a trick devised by Google engineer Romain Guy that uses a BitmapShader to create a texture out of the bitmap
 * and draw it on the target shape. in our case the target is an oval
 *
 * @author ovidiu.buleandra
 * @since 14.12.2015
 */
public class RoundDrawable extends Drawable {
    private final RectF mBounds = new RectF();
    private final BitmapShader mShader;
    private final Paint mPaint;
    private final int mMargin;
    private final boolean mUseVignetteEffect;

    public RoundDrawable(Bitmap target, int margin, boolean useVignette) {
        mShader = new BitmapShader(target, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setShader(mShader);

        mMargin = margin;
        mUseVignetteEffect = useVignette;
    }

    @Override
    public void inflate(Resources r, XmlPullParser parser, AttributeSet attrs) throws XmlPullParserException, IOException {
        super.inflate(r, parser, attrs);
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        mBounds.set(mMargin, mMargin, bounds.width() - mMargin, bounds.height() - mMargin);

        if(mUseVignetteEffect) {
            RadialGradient vignette = new RadialGradient(
                    mBounds.centerX(), mBounds.centerY() * 1.0f / 0.7f, mBounds.centerX() * 1.3f,
                    new int[]{0, 0, 0x7f000000}, new float[]{0.0f, 0.7f, 1.0f},
                    Shader.TileMode.CLAMP);

            Matrix oval = new Matrix();
            oval.setScale(1.0f, 0.7f);
            vignette.setLocalMatrix(oval);

            mPaint.setShader(new ComposeShader(mShader, vignette, PorterDuff.Mode.SRC_OVER));
        }
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawOval(mBounds, mPaint);
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        mPaint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }
}
