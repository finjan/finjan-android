/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ui.ux;

import android.view.View;

/**
 * On scroll event listener interface
 *
 * @author yushu
 */
public interface IOnScrollListener {
    public void onScrollChanged(View scrollView, int x, int y, int oldX, int oldY);

    public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY);
}