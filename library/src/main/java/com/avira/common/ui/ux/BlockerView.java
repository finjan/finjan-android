/*******************************************************************************
 * Copyright (C) 1986-2014 Avira GmbH. All rights reserved.
 *******************************************************************************/
package com.avira.common.ui.ux;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * A transparent view that intercepts all the touch events on the views under it.
 *
 * @author yushu
 */
public class BlockerView extends View {

    public BlockerView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public BlockerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        return true;
    }
}
