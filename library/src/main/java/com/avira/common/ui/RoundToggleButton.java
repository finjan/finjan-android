/*******************************************************************************
Copyright (C) 1986-2014 Avira GmbH. All rights reserved.

 *******************************************************************************/
package com.avira.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ToggleButton;

public class RoundToggleButton extends ToggleButton
{
	public RoundToggleButton(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public RoundToggleButton(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public RoundToggleButton(Context context)
	{
		super(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int h = this.getMeasuredHeight();
		int w = this.getMeasuredWidth();
		int dimen = Math.max(h, w);
		setMeasuredDimension(dimen, dimen);
		// this is called so that the text position inside the toggle button readjusts
		setText(getText());
	}

}