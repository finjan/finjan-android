/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ui.ux;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.avira.common.R;

/**
 * Dashboard Activity containing list view with parallax effect
 *
 * @author yushu
 */
public class ParallaxListViewDashboard extends ParallaxDashboardView {
    public ParallaxListViewDashboard(Context context) {
        super(context);
    }

    public ParallaxListViewDashboard(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initializeUI() {
        mOverscrollableView = (ElasticListView) findViewById(R.id.pxlistview_elastic_listview);
        mOverscrollableView.setOnScrollListener(this);

        mBottomOverscrollBackgroundView = findViewById(R.id.pxlistview_bottom_overscroll_bg_view);
    }

    @Override
    public void inflateInnerLayouts(int contentLayout, int headerLayout, int topOverscrollBackgroundLayout,
                                    int toolbarLayout, int notificationBarLayout) {
        // content layout should not be specified as it is not applicable for list view
        if (contentLayout != NO_ID) {
            throw new RuntimeException("Content layout is not applicable for parallax list view dashboard");
        }

        // prepare the header
        View header = inflate(this.getContext(), headerLayout, null);
        ((ElasticListView) mOverscrollableView).addHeaderView(header);

        // initialize overscroll background view
        mTopOverscrollBackgroundView = inflateViewStub(R.id.pxlistview_top_overscroll_bg_stub,
                topOverscrollBackgroundLayout);

        // inflate toolbar
        mToolbarView = inflateViewStub(R.id.pxlistview_toolbar_stub, toolbarLayout);

        // inflate notification bar
        mNotificationBarView = inflateViewStub(R.id.pxlistview_nofitication_bar_stub, notificationBarLayout);

        // initialize parallax components
        initParallax();
    }

    @Override
    public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        mThreshold = scrollY;
    }

    public void resetScroll() {
    }
}
