/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ui.ux;

/**
 * Overscrollable interface
 *
 * @author yushu
 */
public interface IOverscrollable {
    /**
     * Get the maximum overscroll distance
     *
     * @return maximum overscroll distance in pixel
     */
    int getMaxOverscrollDistance();

    /**
     * set the onScroll listener
     *
     * @param listener
     */
    void setOnScrollListener(IOnScrollListener listener);


    void scrollToViewId(int id);
}
