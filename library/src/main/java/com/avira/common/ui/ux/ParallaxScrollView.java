/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ui.ux;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ViewGroup;

/**
 * Elastic Scrollview that support parallax effect. This scroll view takes a foreground layout and a background layout
 * in its container to create the parallax effect. The background layout will always take at least half the height of
 * the screen.
 *
 * @author yushu
 */
public class ParallaxScrollView extends ElasticScrollView {
    DisplayMetrics mDisplayMatrics = new DisplayMetrics();

    public ParallaxScrollView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public ParallaxScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public ParallaxScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        // call the onMeasure method on the parent, it ensures that there is always 1 container in this view
        super.onMeasure(widthSpec, heightSpec);

        final int FIRST_CHILD_INDEX = 0;
        final int SECOND_CHILD_INDEX = 1;

        // get half of the screen height
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(mDisplayMatrics);
        int halfScreenHeight = mDisplayMatrics.heightPixels / 2;

        // get the container of the scrollview
        ViewGroup container = (ViewGroup) getChildAt(FIRST_CHILD_INDEX);

        // ensure the container has exactly 2 child views
        if (container.getChildCount() != 2) {
            throw new IllegalStateException("ParrallaxScrollView takes exactly 2 child views in the container.");
        }

        ViewGroup child = (ViewGroup) container.getChildAt(FIRST_CHILD_INDEX);
        int childHeight = child.getMeasuredHeight();
        if (childHeight < halfScreenHeight) {
            // ensure the first child takes atleast half of the screen size
            childHeight = halfScreenHeight;
            measureChildHeight(child, childHeight, widthSpec);

            // adjust the height of second child accordingly
            ViewGroup secondChild = (ViewGroup) container.getChildAt(SECOND_CHILD_INDEX);
            int secondChildHeight = secondChild.getMeasuredHeight();
            secondChildHeight += (halfScreenHeight - childHeight);
            measureChildHeight(secondChild, secondChildHeight, widthSpec);

            // adjust the height of container accordingly
            int containerHeight = childHeight + secondChildHeight;
            measureChildHeight(container, containerHeight, widthSpec);
        }
    }

    private void measureChildHeight(ViewGroup viewGroup, int height, int widthSpec) {
        final ViewGroup.LayoutParams layoutParams = (ViewGroup.LayoutParams) viewGroup.getLayoutParams();
        int widthMeasureSpec = getChildMeasureSpec(widthSpec, 0, layoutParams.width);
        int heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        viewGroup.measure(widthMeasureSpec, heightMeasureSpec);
    }

}
