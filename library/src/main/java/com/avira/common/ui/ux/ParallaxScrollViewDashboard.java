/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ui.ux;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

import com.avira.common.R;

/**
 * Dashboard Activity containing scroll view with parallax effect
 *
 * @author yushu
 */
public class ParallaxScrollViewDashboard extends ParallaxDashboardView {
    private float mHeaderViewInitialTranslationY = 0;
    private View mContentView;
    private View mHeaderView;
    private View mScrollViewContent;
    private ScrollView mScrollView;

    public ParallaxScrollViewDashboard(Context context) {
        super(context);
    }

    public ParallaxScrollViewDashboard(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initializeUI() {
        mOverscrollableView = (ElasticScrollView) findViewById(R.id.pxscrollview_elastic_scrollview);
        mOverscrollableView.setOnScrollListener(this);

        mScrollView = (ScrollView) mOverscrollableView;
        mScrollViewContent = mScrollView.getChildAt(0);

        mBottomOverscrollBackgroundView = findViewById(R.id.pxscrollview_bottom_overscsroll_bg_view);
    }

    @Override
    public void inflateInnerLayouts(int contentLayout, int headerLayout, int topOverscrollBackgroundLayout,
                                    int toolbarLayout, int notificationBarLayout) {
        // inflate content
        mContentView = inflateViewStub(R.id.pxscrollview_content_stub, contentLayout);

        // prepare the header
        mHeaderView = inflateViewStub(R.id.pxscrollview_header_stub, headerLayout);

        // initialize overscroll background view
        mTopOverscrollBackgroundView = inflateViewStub(R.id.pxscrollview_top_overscroll_bg_stub,
                topOverscrollBackgroundLayout);

        // inflate toolbar
        mToolbarView = inflateViewStub(R.id.pxscrollview_toolbar_stub, toolbarLayout);

        // inflate notification bar
        mNotificationBarView = inflateViewStub(R.id.pxscrollview_nofitication_bar_stub, notificationBarLayout);

        // initialize parallax components
        initParallax();
    }

    @Override
    public void onScrollChanged(View scrollView, int x, int y, int oldx, int oldy) {
        // animate the header background view
        float parallaxY = y * PARALLAX_MULTIPLIER;
        setTranslationY(mHeaderView, mHeaderViewInitialTranslationY + parallaxY);

        super.onScrollChanged(scrollView, x, y, oldx, oldy);
    }

    @Override
    public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        mThreshold = scrollY;

        if (mScrollViewContent != null) {
            mThreshold += mScreenHeight - mScrollViewContent.getHeight();
        }

        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
    }

    @Override
    protected void initParallax() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (mContentView != null) {
                setBackgroundColor(mBottomOverscrollBackgroundView, mContentView.getBackground());
            }

            if (mHeaderView != null) {
                mHeaderViewInitialTranslationY = getTranslationY(mHeaderView);
            }
        }

        super.initParallax();
    }

    @SuppressLint("NewApi")
    protected void setBackgroundColor(View view, Drawable drawable) {
        if (view == null || drawable == null) {
            return;
        }

        int color = Color.TRANSPARENT;
        if (drawable instanceof ColorDrawable) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                color = ((ColorDrawable) drawable).getColor();
            } else {
                Bitmap bitmap = Bitmap.createBitmap(1, 1, Config.ARGB_4444);
                Canvas canvas = new Canvas(bitmap);
                View colorDrawable = new View(getContext());
                colorDrawable.draw(canvas);
                color = bitmap.getPixel(0, 0);
                bitmap.recycle();
            }
        }
        view.setBackgroundColor(color);
    }

    public void resetScroll() {
        mScrollView.scrollTo(0, 0);
    }
}
