/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ui.ux;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

import com.avira.common.R;

/**
 * An elastic scrollview with bouncing effect, used by parallax dashboard
 *
 * @author yushu
 */
public class ElasticScrollView extends ScrollView implements IOverscrollable {
    private static final int BOUNCE_BACK_HACK_DELAY = 50;
    private int mMaxYOverscrollDistance = 0;
    private IOnScrollListener mOnScrollListener;
    private boolean mIsTouched = false;
    private int mBounceBackHackTargetPosition = 0;
    /**
     * This hack is used for lollipop and above which does not bounce back properly in the case of flinging
     */
    private Runnable mBounceBackHackRunnable = new Runnable() {

        @Override
        public void run() {
            if (!mIsTouched) {
                ElasticScrollView.this.smoothScrollTo(ElasticScrollView.this.getScrollX(),
                        mBounceBackHackTargetPosition);
            }

        }
    };

    public ElasticScrollView(Context context) {
        super(context);
    }

    public ElasticScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(attrs);
    }

    public ElasticScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initAttributes(attrs);
    }

    private void initAttributes(AttributeSet attrs) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ElasticScrollView);
            mMaxYOverscrollDistance = a.getDimensionPixelSize(R.styleable.ElasticScrollView_maxOverscrollDistance, 0);
            a.recycle();
        }

    }

    @Override
    public int getMaxOverscrollDistance() {
        return mMaxYOverscrollDistance;
    }

    @Override
    public void setOnScrollListener(IOnScrollListener listener) {
        mOnScrollListener = listener;
    }

    @SuppressLint("NewApi")
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        // TODO: move this somewhere
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
                                   int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // overwrite the max overscroll distance
            return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX,
                    (mMaxYOverscrollDistance > 0) ? mMaxYOverscrollDistance : maxOverScrollY, isTouchEvent);
        }
        return false;

    }

    @SuppressLint("NewApi")
    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
            if (mOnScrollListener != null) {
                mOnScrollListener.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // This hack is used for lollipop and above which does not bounce back properly in the case of flinging

                if (scrollY <= 0) {
                    mBounceBackHackTargetPosition = 0;
                    ElasticScrollView.this.removeCallbacks(mBounceBackHackRunnable);
                    ElasticScrollView.this.postDelayed(mBounceBackHackRunnable, BOUNCE_BACK_HACK_DELAY);

                } else {
                    int mScrollViewHeight = this.getHeight();
                    int mContentHeight = this.getChildAt(0).getHeight();
                    if (mContentHeight - mScrollViewHeight - scrollY < 0)

                    {
                        mBounceBackHackTargetPosition = mContentHeight - mScrollViewHeight;
                        ElasticScrollView.this.removeCallbacks(mBounceBackHackRunnable);
                        ElasticScrollView.this.postDelayed(mBounceBackHackRunnable, BOUNCE_BACK_HACK_DELAY);

                    }
                }

            }
        }

    }

    @Override
    protected void onScrollChanged(int x, int y, int oldX, int oldY) {
        super.onScrollChanged(x, y, oldX, oldY);
        if (mOnScrollListener != null) {
            mOnScrollListener.onScrollChanged(this, x, y, oldX, oldY);
        }

    }

    @Override
    public boolean canScrollVertically(int direction) {
        // this is to enable the bouncing effect even if the view is not scrollable
        // for api 14 and above
        return true;

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mIsTouched = true;
                break;
            case MotionEvent.ACTION_UP:
                mIsTouched = false;
                break;

        }
        return super.onTouchEvent(ev);
    }


    public void scrollToViewId(int viewId) {
        final View targetView = findViewById(viewId);
        if (targetView != null) {
            post(new Runnable() {
                @Override
                public void run() {
                    scrollTo(0, targetView.getBottom());
                }
            });
        }
    }

}
