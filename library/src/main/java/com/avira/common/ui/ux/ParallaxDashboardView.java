/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ui.ux;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;

/**
 * Parallax dashboard view base class
 *
 * @author hui-joo.goh
 */
public abstract class ParallaxDashboardView extends RelativeLayout implements IOnScrollListener {
    protected static final float PARALLAX_MULTIPLIER = 0.5f;
    protected static final float ADJUSTMENT = 1.1f;

    protected IOverscrollable mOverscrollableView;
    protected View mToolbarView;
    protected View mNotificationBarView;
    protected View mTopOverscrollBackgroundView;
    protected View mBottomOverscrollBackgroundView;

    protected int mScreenHeight;
    protected int mThreshold;
    protected float mBackgroundViewInitialTranslationY = 0;

    private boolean mStickyToolbar;
    private boolean mStickyNotificationBar;

    public ParallaxDashboardView(Context context) {
        super(context);
    }

    public ParallaxDashboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Method to initialize UI elements
     */
    protected abstract void initializeUI();

    /**
     * Assign & inflate all the specified layout id inside parallax dashboard layout
     *
     * @param contentLayout                 the activity content layout
     * @param headerLayout                  the activity header layout
     * @param topOverscrollBackgroundLayout the header overscroll background layout
     * @param toolbarLayout                 the activity toolbar layout
     * @param notificationBarLayout         the activity notification layout
     */
    public abstract void inflateInnerLayouts(int contentLayout, int headerLayout, int topOverscrollBackgroundLayout,
                                             int toolbarLayout, int notificationBarLayout);

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initializeUI();

        // get the screen height
        DisplayMetrics displayMatrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMatrics);
        mScreenHeight = displayMatrics.heightPixels;
    }

    @Override
    public void onScrollChanged(View scrollView, int x, int y, int oldx, int oldy) {
        float parallaxY = y * PARALLAX_MULTIPLIER;

        // animate the overscroll background view
        setTranslationY(mTopOverscrollBackgroundView, mBackgroundViewInitialTranslationY - parallaxY);

        // If the toolbar/notification bar is not a sticky view, update its visibility accordingly
        if (!mStickyNotificationBar) {
            // Check if notification bar needs to be hidden/shown.
            // If needs to be hidden, it is purposely specified to GONE rather than INVISIBLE because the toolbar layout is underneath
            updateNonStickyViewVisibility(mNotificationBarView, y, true);
        }

        if (!mStickyToolbar) {
            // Check if toolbar needs to be hidden/shown.
            // If needs to be hidden, it is purposely specified to INVISIBLE rather than GONE to fix AAMA-1721
            updateNonStickyViewVisibility(mToolbarView, y, false);
        }
    }

    @Override
    public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        // hide the bottom background when user scroll down in the case of overscroll
        boolean showBottomOverScroll = mThreshold > 0 && scrollY > 0;
        mBottomOverscrollBackgroundView.setVisibility(showBottomOverScroll ? View.VISIBLE : View.INVISIBLE);
    }

    protected View inflateViewStub(int viewStubId, int layoutId) {
        if (layoutId != View.NO_ID) {
            ViewStub viewStub = (ViewStub) findViewById(viewStubId);

            if (viewStub != null) {
                viewStub.setLayoutResource(layoutId);
                return viewStub.inflate();
            }
        }

        return null;
    }

    protected void initParallax() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            int maxOverScrollDistance = mOverscrollableView.getMaxOverscrollDistance();
            // set the height of bottom to cover the case of upward overscroll

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBottomOverscrollBackgroundView
                    .getLayoutParams();
            layoutParams.height = (int) (maxOverScrollDistance * ADJUSTMENT);
            mBottomOverscrollBackgroundView.setLayoutParams(layoutParams);

            if (mTopOverscrollBackgroundView != null) {
                mBackgroundViewInitialTranslationY = getTranslationY(mTopOverscrollBackgroundView);
                mBackgroundViewInitialTranslationY -= mOverscrollableView.getMaxOverscrollDistance()
                        * PARALLAX_MULTIPLIER;
                setTranslationY(mTopOverscrollBackgroundView, mBackgroundViewInitialTranslationY);
            }
        }
    }

    @SuppressLint("NewApi")
    protected void setTranslationY(View background, float y) {
        if (background != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            background.setTranslationY(y);
        }
    }

    @SuppressLint("NewApi")
    protected float getTranslationY(View background) {
        if (background != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return background.getTranslationY();
        }
        return 0;
    }

    /**
     * Method to update the visibility of the non-sticky view (toolbar/notification bar) accordingly based on the
     * specified parameters. A non-sticky view means that it will be hidden when the parent activity is scrolled down
     * whereas a sticky view will stay visible.
     *
     * @param view             The non-sticky view (view that should not stay on screen when user scrolls down)
     * @param scrollY          Current vertical scroll origin
     * @param viewGoneIfHidden True to set the view to {@link #GONE} if it's required to be hidden.
     *                         False to set the view to {@link #INVISIBLE} if it's required to be hidden.
     */
    protected void updateNonStickyViewVisibility(View view, int scrollY, boolean viewGoneIfHidden) {
        if (view != null) {

            boolean hideView = scrollY > view.getHeight();
            if (hideView) {

                // Hide view
                setViewVisibilityFromHere(view, viewGoneIfHidden ? View.GONE : View.INVISIBLE);

            } else if (isViewSetHiddenFromHere(view)) { // If view not set gone from here, it was explicitly set gone
                // from elsewhere, we should not override & make it visible (AAMA-1824)

                // Show view
                setViewVisibilityFromHere(view, View.VISIBLE);
            }
        }
    }

    /**
     * Set specified view's visibility & remember the visibility value that was set from here
     *
     * @param view       The view to apply the specified visibility
     * @param visibility One of {@link #VISIBLE}, {@link #INVISIBLE}, or {@link #GONE}
     */
    private void setViewVisibilityFromHere(View view, int visibility) {

        // Do not remove this check. Need it to differentiate where the visibility was set from
        if (view.getVisibility() != visibility) {
            view.setVisibility(visibility);
            view.setTag(visibility);
        }
    }

    /**
     * Check if the specified view was hidden by this class previously
     *
     * @param view The view to check if it was hidden from this class
     * @return True if the specified view was hidden by this class previously. Otherwise false
     */
    private boolean isViewSetHiddenFromHere(View view) {
        Integer visibility = (Integer) view.getTag();

        if (visibility != null) {
            return visibility == View.INVISIBLE || visibility == View.GONE;
        }

        return false;
    }

    /**
     * Set the stickiness property of the toolbar (if the view will persist in the screen whenever scrolled)
     *
     * @param isSticky True for the view to always stay visible in the screen whenever scrolled. False to hide the view when
     *                 the parent activity is scrolled down.
     */
    public void setToolbarStickiness(boolean isSticky) {
        mStickyToolbar = isSticky;
    }

    /**
     * Set the stickiness property of the notification bar (if the view will persist in the screen whenever scrolled)
     *
     * @param isSticky True for the view to always stay visible in the screen whenever scrolled. False to hide the view when
     *                 the parent activity is scrolled down.
     */
    public void setNotificationBarStickiness(boolean isSticky) {
        mStickyNotificationBar = isSticky;
    }

    public float getParallaxMultiplier() {
        return PARALLAX_MULTIPLIER;
    }

    public int getMaxOverscrollDistance() {
        return mOverscrollableView.getMaxOverscrollDistance();
    }

    public abstract void resetScroll();

    public void scrollToViewId(int viewId) {
        mOverscrollableView.scrollToViewId(viewId);
    }


}
