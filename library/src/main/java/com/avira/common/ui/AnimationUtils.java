package com.avira.common.ui;

/**
 * Created by danielastamati on 20/10/15.
 */
public class AnimationUtils {
    //** transformer constants **//
    public static final String X_AXIS = "x";
    public static final String Y_AXIS = "y";
    public static final String ALPHA = "Alpha";
    public static final String SCALE_X = "scaleX";
    public static final String SCALE_Y = "scaleY";


    //** durations in millis **//
    public static final int STANDARD_ANIM_DURATION = 250;
    public static final int SHORT_ANIM_DURATION = 50;
    public static final int MEDIUM_ANIM_DURATION = 50;

}
