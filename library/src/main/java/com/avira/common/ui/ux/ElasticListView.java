/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ui.ux;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

import com.avira.common.R;


/**
 * An elastic list view with bouncing effect
 * -
 *
 * @author yushu
 */
public class ElasticListView extends ListView implements IOverscrollable {
    private int mMaxYOverscrollDistance = 0;
    private IOnScrollListener mOnScrollListener;

    public ElasticListView(Context context) {
        super(context);
    }

    public ElasticListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(attrs);
    }

    public ElasticListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initAttributes(attrs);
    }

    private void initAttributes(AttributeSet attrs) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ElasticScrollView);
            mMaxYOverscrollDistance = a.getDimensionPixelSize(R.styleable.ElasticScrollView_maxOverscrollDistance, 0);
            a.recycle();
        }
    }

    /**
     * Get the maximum overscroll distance
     *
     * @return maximum overscroll distance in pixel
     */
    public int getMaxOverscrollDistance() {
        return mMaxYOverscrollDistance;
    }

    @SuppressLint("NewApi")
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
                                   int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // overwrite the max overscroll distance
            return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX,
                    (mMaxYOverscrollDistance > 0) ? mMaxYOverscrollDistance : maxOverScrollY, isTouchEvent);
        }
        return false;

    }

    @SuppressLint("NewApi")
    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
            if (mOnScrollListener != null) {
                mOnScrollListener.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
            }
        }
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldX, int oldY) {
        super.onScrollChanged(x, y, oldX, oldY);
        if (mOnScrollListener != null) {
            mOnScrollListener.onScrollChanged(this, x, y, oldX, oldY);
        }
    }

    @Override
    public boolean canScrollVertically(int direction) {
        // this is to enable the bouncing effect even if the view is not
        // scrollable
        // for api 14 and above
        return true;
    }

    @Override
    public void setOnScrollListener(IOnScrollListener listener) {
        mOnScrollListener = listener;
    }


    /**
     * Scroll to
     *
     * @param id
     */
    public void scrollToViewId(int id) {
        final View targetView = findViewById(id);
        if (targetView != null) {
            post(new Runnable() {
                @Override
                public void run() {
                    scrollTo(0, targetView.getBottom());
                }
            });
        }
    }
}
