/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.menus;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Item that contains menu item name and drawable icon
 *
 * @author bogdan.oprea
 */
public class PopupMenuItem implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PopupMenuItem> CREATOR = new Parcelable.Creator<PopupMenuItem>() {
        @Override
        public PopupMenuItem createFromParcel(Parcel in) {
            return new PopupMenuItem(in);
        }

        @Override
        public PopupMenuItem[] newArray(int size) {
            return new PopupMenuItem[size];
        }
    };
    private int mNameResId = 0;
    private int mIconResId = 0;

    public PopupMenuItem(int name, int icon) {
        mNameResId = name;
        mIconResId = icon;
    }

    protected PopupMenuItem(Parcel in) {
        mNameResId = in.readInt();
        mIconResId = in.readInt();
    }

    public int getName() {
        return mNameResId;
    }

    public void setName(int name) {
        mNameResId = name;
    }

    public int getIconId() {
        return mIconResId;
    }

    public void setIconId(int icon) {
        mIconResId = icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mNameResId);
        dest.writeInt(mIconResId);
    }
}
