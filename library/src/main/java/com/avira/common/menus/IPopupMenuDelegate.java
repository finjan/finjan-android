/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.menus;

/**
 * Popup menu event listener interface
 *
 * @author daniela.stamati
 */
public interface IPopupMenuDelegate {
    void onPopupMenuOptionClick(int optionPosition);
}
