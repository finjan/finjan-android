/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.menus;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avira.common.R;

import java.util.List;

/**
 * Shows the menu items in the list with icons and name
 *
 * @author bogdan.oprea
 */
public class PopupMenuAdapter extends ArrayAdapter<PopupMenuItem> {
    private LayoutInflater mInflater;

    public PopupMenuAdapter(Context context, int resource, List<PopupMenuItem> objects) {
        super(context, resource, objects);
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PopupMenuItem item = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.popup_menu_list_item, null);
            holder = new ViewHolder();
            holder.nameTV = (TextView) convertView.findViewById(R.id.menu_item_name);
            holder.iconIV = (ImageView) convertView.findViewById(R.id.menu_item_icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (item.getIconId() == 0) {
            holder.iconIV.setVisibility(View.GONE);
        } else {
            holder.iconIV.setImageResource(item.getIconId());
        }

        holder.nameTV.setText(item.getName());
        return convertView;
    }

    private class ViewHolder {
        public TextView nameTV;
        public ImageView iconIV;
    }

}
