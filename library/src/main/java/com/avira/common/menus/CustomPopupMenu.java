/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.menus;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.avira.common.R;

import java.util.ArrayList;

/**
 * Custom popup menu fragment class
 *
 * @author daniela.stamati
 */
public class CustomPopupMenu extends DialogFragment implements OnItemClickListener {
    public static final String POPUP_MENU_TAG = "popup_menu_tag";
    private static final String MENU_OPTION_ARRAY_KEY = "menu_option_array_key";
    private static final String POS_Y_KEY = "pos_y_key";
    private ArrayList<PopupMenuItem> mMenuOptionArray;
    private IPopupMenuDelegate mMenuDelegate;
    private ListView mListView;
    private PopupMenuAdapter mArrayAdapter;
    private View mView;
    private int mPosY; // Y position of the menu icon

    public static CustomPopupMenu newInstance(ArrayList<PopupMenuItem> menuOptionArray, int posY) {
        CustomPopupMenu dialog = new CustomPopupMenu();

        Bundle args = new Bundle();
        args.putParcelableArrayList(MENU_OPTION_ARRAY_KEY, menuOptionArray);
        args.putInt(POS_Y_KEY, posY);
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // To ensure the container activity has implemented the callback interface:
        // http://cmikavac.net/2014/02/20/returning-value-from-fragment-into-parent-activity-on-android/
        try {
            mMenuDelegate = (IPopupMenuDelegate) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement "
                    + IPopupMenuDelegate.class.getSimpleName());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mMenuOptionArray = args.getParcelableArrayList(MENU_OPTION_ARRAY_KEY);
        mPosY = args.getInt(POS_Y_KEY);

        setCancelable(true);
        setStyle(STYLE_NO_TITLE, R.style.DialogNoTitle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.popup_menu_layout, container, false);
        mListView = (ListView) mView.findViewById(R.id.popup_menu_list_view);
        mArrayAdapter = new PopupMenuAdapter(getActivity(), R.layout.popup_menu_list_item, mMenuOptionArray);
        mListView.setAdapter(mArrayAdapter);
        mListView.setOnItemClickListener(this);

        // remove border
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

        setPosition();

        return mView;
    }

    private void setPosition() {
        WindowManager.LayoutParams layoutParams = getDialog().getWindow().getAttributes();
        layoutParams.gravity = Gravity.TOP | Gravity.RIGHT;
        layoutParams.x = (int) getResources().getDimension(R.dimen.bubble_menu_paddingX);
        layoutParams.y = mPosY + (int) getResources().getDimension(R.dimen.bubble_menu_paddingY);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mMenuDelegate != null) {
            mMenuDelegate.onPopupMenuOptionClick(position);
        }

        dismiss();
    }
}
