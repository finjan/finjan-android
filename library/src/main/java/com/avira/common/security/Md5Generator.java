package com.avira.common.security;

import com.avira.common.security.HashGeneratorFactory.IHashGenerator;

/**
 * Class to generate md5 hash
 *
 * @author jason.goh
 */
class Md5Generator implements IHashGenerator {
    private static final String MD5_TAG = "MD5";
    private GenericHashGenerator mGenericHashGenerator;

    public Md5Generator(GenericHashGenerator genericHashGenerator) {
        mGenericHashGenerator = genericHashGenerator;
    }

    /**
     * Get Md5 hashing of input string
     *
     * @param textToHash String to be hash
     * @return Md5 Hashed input string or null if MD5 is not supported in the android java version
     */
    @Override
    public String getHash(String textToHash) {
        return mGenericHashGenerator.getHash(textToHash, MD5_TAG);
    }

    /**
     * Get Md5 hashing with salt of input string
     *
     * @param textToHash String to be hash
     * @param salt       salt to input
     * @return Md5 Hash with salt input string or null if MD5 is not supported in the android java version
     */
    @Override
    public String getHashWithSalt(String textToHash, String salt) {
        return mGenericHashGenerator.getHashWithSalt(textToHash, salt, MD5_TAG);
    }
}
