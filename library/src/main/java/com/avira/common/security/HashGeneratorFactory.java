package com.avira.common.security;

/**
 * Factory class that creates Hash generator singleton
 *
 * @author jason.goh
 */
public class HashGeneratorFactory {
    private final static GenericHashGenerator sGenericHashGenerator = new GenericHashGenerator();
    private static IHashGenerator sMd5Generator;
    private static IHashGenerator sSha256Generator;

    /**
     * Obtain an instance of Md5 generator
     *
     * @return instance of Md5 generator
     */
    public static IHashGenerator getMd5GeneratorSingleton() {
        if (sMd5Generator == null) {
            sMd5Generator = new Md5Generator(sGenericHashGenerator);
        }
        return sMd5Generator;
    }

    /**
     * Obtain an instance of SHA-256 generator
     *
     * @return instance of SHA-256 generator
     */
    public static IHashGenerator getSha256GeneratorSingleton() {
        if (sSha256Generator == null) {
            sSha256Generator = new Sha256Generator(sGenericHashGenerator);
        }
        return sSha256Generator;
    }

    /**
     * Interface for all Hash generator class
     *
     * @author jason.goh
     */
    public interface IHashGenerator {
        /**
         * Generate hash from the text input
         *
         * @param textToHash text input to hash
         * @return hashed text input
         */
        public String getHash(String textToHash);

        /**
         * Generate hash with Salt from text input
         *
         * @param textToHash - text to input to hash
         * @param salt       - salt to input to hash
         * @return hashed with Salt text input
         */
        public String getHashWithSalt(String textToHash, String salt);
    }
}
