package com.avira.common.security;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Generic hash generator
 *
 * @author jason.goh
 */
class GenericHashGenerator {

    private static final String TAG = GenericHashGenerator.class.getSimpleName();

    /**
     * Get hash from text input based on selected hash algorithm
     *
     * @param textToHash    text input
     * @param hashAlgorithm selected hash algorithm
     * @return hashed text
     */
    String getHash(String textToHash, String hashAlgorithm) {
        String result = null;
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance(hashAlgorithm);
            messageDigest.reset();
            messageDigest.update(textToHash.getBytes());
            byte[] digest = messageDigest.digest();
            int digestLength = digest.length;
            StringBuilder stringBuilder = new StringBuilder(digestLength << 1);
            for (int i = 0; i < digestLength; i++) {
                stringBuilder.append(Character.forDigit((digest[i] & 0xf0) >> 4, 16));
                stringBuilder.append(Character.forDigit(digest[i] & 0x0f, 16));
            }
            result = stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Algorithm '" + hashAlgorithm + "' doesn't exist", e);
        }

        return result;
    }

    /**
     * Get hash with salth from text input based on selected hash algorithm
     *
     * @param textToHash    text input
     * @param salt          salt input
     * @param hashAlgorithm selected hash algorithm
     * @return hash with salt text
     */
    String getHashWithSalt(String textToHash, String salt, String hashAlgorithm) {
        String result = null;
        MessageDigest messageDigest;
        try {
            textToHash += salt;

            messageDigest = MessageDigest.getInstance(hashAlgorithm);
            messageDigest.reset();
            messageDigest.update(textToHash.getBytes());
            byte[] digest = messageDigest.digest();
            int digestLength = digest.length;
            StringBuilder stringBuilder = new StringBuilder(digestLength << 1);
            for (int i = 0; i < digestLength; i++) {
                stringBuilder.append(Character.forDigit((digest[i] & 0xf0) >> 4, 16));
                stringBuilder.append(Character.forDigit(digest[i] & 0x0f, 16));
            }
            result = stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Algorithm '" + hashAlgorithm + "' doesn't exist", e);
        }

        return result;
    }
}
