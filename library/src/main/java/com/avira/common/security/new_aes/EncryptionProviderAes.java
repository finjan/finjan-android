package com.avira.common.security.new_aes;

import android.text.TextUtils;
import android.util.Log;

import com.avira.common.database.EncryptionProvider;
import com.avira.common.database.Settings;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

/**
 * Created by Illia.Klimov on 6/23/2016.
 */

public class EncryptionProviderAes {
    private static final String TAG = EncryptionProvider.class.getSimpleName();

    private static EncryptionProviderAes INSTANCE;
    public final static String CHAR_SET = "UTF-8";
    AesCbcWithIntegrity.SecretKeys keys = null;


    private EncryptionProviderAes() {
    }

    /**
     * @throws IllegalStateException if keys failed to be initialized
     */
    private void initKeys() {
        try {
            byte[] sKeys = Settings.readKey();
            if (sKeys == null || sKeys.length <= AesCbcWithIntegrity.KEY_HEADERS_SIZE) {
                keys = AesCbcWithIntegrity.generateKey();
                Settings.writeKey(keys.toByteArray());
            } else {
                keys = AesCbcWithIntegrity.keys(sKeys);
            }
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException("No keys available", e);
        }
    }

    public synchronized static EncryptionProviderAes getInstance() {
        if (INSTANCE == null) {
            Log.d(TAG, "init");
            INSTANCE = new EncryptionProviderAes();
        }
        return INSTANCE;
    }

    /**
     * @throws IllegalStateException if {@link EncryptionProviderAes#initKeys()} fails
     */
    public byte[] encrypt(String input) {
        byte[] result = null;

        if (input == null) {
            input = "";
        }

        try {
            result = encrypt(input.getBytes(CHAR_SET));
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "UnsupportedEncodingException", e);
        }

        return result;
    }

    /**
     * @throws IllegalStateException if {@link EncryptionProviderAes#initKeys()} fails
     */
    public byte[] encrypt(byte[] input) {
        byte[] result = null;

        if (keys == null) {
            initKeys();
        }

        if (input == null) {
            input = "".getBytes();
        }

        try {
            AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac = getCipherTextIvMac(input);
            result = cipherTextIvMac.toByteArray();
        } catch (GeneralSecurityException e) {
            Log.e(TAG, "Can not encrypt", e);
        }

        return result;
    }

    /**
     * @param input The unencrypted string input
     * @return The encrypted input in the form of string
     */
    public String encryptToString(String input) {
        String result = null;

        if (input == null) {
            input = "";
        }

        try {
            byte[] inputByte = input.getBytes(CHAR_SET);
            result = encryptToString(inputByte);

        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Can not encrypt", e);
        }

        return result;
    }

    /**
     * @param input The unencrypted byte array input
     * @return The encrypted input in the form of string
     */
    public String encryptToString(byte[] input) {
        String result = null;

        if (input == null) {
            input = "".getBytes();
        }

        try {
            AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac = getCipherTextIvMac(input);
            result = cipherTextIvMac.toString();
        } catch (GeneralSecurityException e) {
            Log.e(TAG, "Can not encrypt", e);
        }

        return result;
    }

    /**
     * @throws IllegalStateException if {@link EncryptionProviderAes#initKeys()} fails
     */
    private AesCbcWithIntegrity.CipherTextIvMac getCipherTextIvMac(byte[] input) throws GeneralSecurityException {

        if (keys == null) {
            initKeys();
        }

        if (input == null) {
            input = "".getBytes();
        }

        return AesCbcWithIntegrity.encrypt(input, keys);
    }

    /**
     * @throws IllegalStateException if {@link EncryptionProviderAes#initKeys()} fails
     */
    public String decrypt(String input) {
        String result = null;

        if (TextUtils.isEmpty(input)) {
            return null;
        }

        if (keys == null) {
            initKeys();
        }

        AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac = new AesCbcWithIntegrity.CipherTextIvMac(input);
        try {
            result = AesCbcWithIntegrity.decryptString(cipherTextIvMac, keys);
        } catch (UnsupportedEncodingException | GeneralSecurityException e) {
            Log.e(TAG, "Can not decrypt", e);
        }

        return result;
    }

    /**
     * @throws IllegalStateException if {@link EncryptionProviderAes#initKeys()} fails
     */
    public String decrypt(byte[] input) {
        String result = null;

        if (input == null && input.length < AesCbcWithIntegrity.ENCRYPTION_HEADERS_SIZE) {
            return null;
        }

        if (keys == null) {
            initKeys();
        }

        AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac = new AesCbcWithIntegrity.CipherTextIvMac(input);
        try {
            result = AesCbcWithIntegrity.decryptString(cipherTextIvMac, keys);
        } catch (UnsupportedEncodingException | GeneralSecurityException e) {
            Log.e(TAG, "Can not decrypt", e);
        }

        return result;
    }
}