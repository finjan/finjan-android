package com.avira.common.security;

import com.avira.common.security.HashGeneratorFactory.IHashGenerator;

/**
 * Class to generate sha-256 hash
 *
 * @author jason.goh
 */
class Sha256Generator implements IHashGenerator {
    private static final String SHA_256_TAG = "SHA-256";
    private GenericHashGenerator mGenericHashGenerator;

    public Sha256Generator(GenericHashGenerator genericHashGenerator) {
        mGenericHashGenerator = genericHashGenerator;
    }

    /**
     * Get SHA-256 hashing of input string
     *
     * @param textToHash String to be hash
     * @return Sha256 Hashed input string or null if SHA-256 is not supported in the android java version
     */
    @Override
    public String getHash(String textToHash) {
        return mGenericHashGenerator.getHash(textToHash, SHA_256_TAG);
    }

    @Override
    public String getHashWithSalt(String textToHash, String salt) {
        return mGenericHashGenerator.getHashWithSalt(textToHash, salt, SHA_256_TAG);
    }

}
