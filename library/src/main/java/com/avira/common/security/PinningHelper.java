/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.security;

import android.content.Context;
import android.util.Log;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

//@formatter:off
/**
 * Helper class to provide:
 * 
 * - pinned SSLSocketFactory for a HttpsURLConnection
 * - pinned HttpClient
 * 
 * @author hui-joo.goh
 * 
 */
//@formatter:on
public class PinningHelper {
    private static final String TAG = PinningHelper.class.getSimpleName();

    /**
     * Contructs a SSLSocketFactory that will validate HTTPS connections against the set of specified pins
     *
     * @param context
     * @param pins    hex-encoded hash of the public key of each X.509 certificate in the server certificate chain
     * @return If successful, returns a pinned SSLSocketFactory that will validate HTTPS connections against the set of
     * specified pins. Otherwise, returns null
     */
    public static SSLSocketFactory getPinnedSSLSocketFactory(Context context, String[] pins) {
        try {
            TrustManager[] trustManagers = new TrustManager[1];

            SystemKeyStore keyStore = SystemKeyStore.getInstance(context);
            trustManagers[0] = new PinningTrustManager(keyStore, pins, 0);

            SSLContext sslContext = SSLContext.getInstance(org.apache.http.conn.ssl.SSLSocketFactory.TLS);
            sslContext.init(null, trustManagers, null);

            return sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, e.getMessage());
        } catch (KeyManagementException e) {
            Log.e(TAG, e.getMessage());
        }

        return null;
    }
}
