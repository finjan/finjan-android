/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.security;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.util.Log;

import com.avira.common.BuildConfig;
import com.avira.common.R;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;

//@formatter:off
/**
 * An interface to the system's trust anchors.  We're using our
 * own truststore, which is just the AOSP default, but in a place
 * we know to find it for sure.
 *
 * @author https://github.com/moxie0/AndroidPinning
 */
//@formatter:on
public class SystemKeyStore {
    private static final String TAG = SystemKeyStore.class.getSimpleName();
    private static final int CACERTS_FILE_SIZE = 1024 * 140;

    private static SystemKeyStore instance;
    final KeyStore trustStore;
    private final HashMap<Principal, X509Certificate> trustRoots;

    private SystemKeyStore(Context context) {
        final KeyStore trustStore = getTrustStore(context);

        if (BuildConfig.SERVER_PINS.equalsIgnoreCase("debug")) {
            setUnknownCertificateAuthority(context, trustStore);
        }
        this.trustRoots = initializeTrustedRoots(trustStore);
        this.trustStore = trustStore;
    }

    public static synchronized SystemKeyStore getInstance(Context context) {
        if (instance == null) {
            instance = new SystemKeyStore(context);
        }
        return instance;
    }

    public boolean isTrustRoot(X509Certificate certificate) {
        final X509Certificate trustRoot = trustRoots.get(certificate.getSubjectX500Principal());
        return trustRoot != null && trustRoot.getPublicKey().equals(certificate.getPublicKey());
    }

    public X509Certificate getTrustRootFor(X509Certificate certificate) {
        final X509Certificate trustRoot = trustRoots.get(certificate.getIssuerX500Principal());

        if (trustRoot == null) {
            return null;
        }

        if (trustRoot.getSubjectX500Principal().equals(certificate.getSubjectX500Principal())) {
            return null;
        }

        try {
            certificate.verify(trustRoot.getPublicKey());
        } catch (GeneralSecurityException e) {
            return null;
        }

        return trustRoot;
    }

    private HashMap<Principal, X509Certificate> initializeTrustedRoots(KeyStore trustStore) {
        try {
            final HashMap<Principal, X509Certificate> trusted = new HashMap<Principal, X509Certificate>();

            for (Enumeration<String> aliases = trustStore.aliases(); aliases.hasMoreElements(); ) {
                final String alias = aliases.nextElement();
                final X509Certificate cert = (X509Certificate) trustStore.getCertificate(alias);

                if (cert != null) {
                    trusted.put(cert.getSubjectX500Principal(), cert);
                }
            }

            return trusted;
        } catch (KeyStoreException e) {
            throw new AssertionError(e);
        }
    }

    private KeyStore getTrustStore(Context context) {
        Log.d(TAG, "getTrustStore");
        try {
            final KeyStore trustStore = KeyStore.getInstance("BKS");
            final BufferedInputStream bin = new BufferedInputStream(context.getResources().openRawResource(
                    R.raw.cacerts), CACERTS_FILE_SIZE);

            try {
                trustStore.load(bin, "changeit".toCharArray());
            } finally {
                try {
                    bin.close();
                } catch (IOException ioe) {
                    Log.w("SystemKeyStore", ioe);
                }
            }
            return trustStore;
        } catch (KeyStoreException kse) {
            throw new AssertionError(kse);
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        } catch (CertificateException e) {
            throw new AssertionError(e);
        } catch (NotFoundException e) {
            throw new AssertionError(e);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    //https://developer.android.com/training/articles/security-ssl.html#CommonProblems
    private void setUnknownCertificateAuthority(Context context, KeyStore trustStore) {
        Log.d(TAG, "setUnknownCertificateAuthority");
        // Load CAs from an InputStream
        // (could be from a resource or ByteArrayInputStream or ...)
        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X.509");
        } catch (CertificateException e) {
            Log.e(TAG, "cf error, ", e);
        }

        if(cf == null) {
            return;
        }
        // From https://www.washington.edu/itconnect/security/ca/load-der.crt
        final BufferedInputStream caInput = new BufferedInputStream(context.getResources().openRawResource(
                R.raw.aviraroot));
        Certificate ca;
        try {
            ca = cf.generateCertificate(caInput);
            Log.i(TAG, "ca= " + ((X509Certificate) ca).getSubjectDN());
            trustStore.setCertificateEntry("ca", ca);
        } catch (KeyStoreException|CertificateException e) {
            e.printStackTrace();
        } finally {
            try {
                caInput.close();
            } catch (IOException e) {
                Log.e(TAG, "caInput error, ", e);
            }
        }
    }
}
