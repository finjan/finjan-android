///*
// * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
// */
//package com.avira.common.ads;
//
//import android.content.Context;
//import android.util.Log;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.avira.common.R;
//import com.facebook.ads.Ad;
//import com.facebook.ads.AdChoicesView;
//import com.facebook.ads.AdError;
//import com.facebook.ads.AdListener;
//import com.facebook.ads.NativeAd;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Facebook Ads implementation
// * Each client should implement AdListener and AdsCallbackInterface Interface
// * AdListener Interface - taking care of the ad management as (Ad is loaded, Ad loading error and Ad click)
// * AdsCallbackInterface - taking care of the register, unregister and loading of an ad (despite of ad type like facebook, or other provider)
// */
//public class FacebookAds implements AdListener, AdsCallbackInterface, View.OnClickListener {
//
//    private static final String TAG = FacebookAds.class.getSimpleName();
//    private NativeAd mFacebookAd;
//    private View mBannerLayout;
//    private boolean mShowCloseButton = true;
//
//    public FacebookAds(Context context, String adPlacementId) {
//        mFacebookAd = new NativeAd(context, adPlacementId);
//        mFacebookAd.setAdListener(this);
//    }
//
//    // Implement methods from Facebook Ads AdListener Interface
//    @Override
//    public void onError(Ad ad, AdError adError) {
//        Log.d(TAG, "onError get add, " + adError.getErrorMessage());
//        showBannerView(false);
//    }
//
//    @Override
//    public void onAdLoaded(Ad ad) {
//        if (ad == null || ad != mFacebookAd) {
//            Log.d(TAG, "onAdLoaded error. No ads");
//            showBannerView(false);
//            return;
//        }
//        insertLoadedAdIntoLayoutContainer();
//    }
//
//    @Override
//    public void onAdClicked(Ad ad) {
//        Log.d(TAG, "Ad clicked");
//    }
//
//    // Implement methods from AdsCallbackInterface Interface
//    /**
//     * @param context
//     * @param view - Ad layout is - fb_ads_banner (if other ad view wanted please handle it into
//     *             the insertLoadedAdIntoLayoutContainer method)
//     */
//    @Override
//    public void register(Context context, View view) {
//        Log.d(TAG, "register view");
//        if(view == null) {
//            throw new RuntimeException("No Ad Content Provided!");
//        }
//        mBannerLayout = view;
//    }
//
//    public void register(Context context, View view, boolean showCloseButton) {
//        mShowCloseButton = showCloseButton;
//        register(context, view);
//    }
//
//    @Override
//    public void load() {
//        try {
//            mFacebookAd.loadAd();
//        } catch (IllegalStateException e) {
//            Log.e(TAG, "Error loading fb ads, " + e);
//            showBannerView(false);
//        }
//    }
//
//    @Override
//    public void unregister() {
//        Log.d(TAG, "unregister view");
//        mFacebookAd.unregisterView();
//    }
//
//    /**
//     * Ad loading into layout
//     */
//    public void insertLoadedAdIntoLayoutContainer() {
//
//        if (mBannerLayout == null) {
//            Log.e(TAG, "insertLoadedAdIntoLayoutContainer failed. Banner view is null.");
//            return;
//        }
//
//        Context context = mBannerLayout.getContext();
//        String adTitle = mFacebookAd.getAdTitle();
//        NativeAd.Image imageAd = mFacebookAd.getAdIcon();
//        String titleAdForButton = mFacebookAd.getAdCallToAction();
//
//        if (adTitle != null && imageAd != null && titleAdForButton != null) {
//
//            String adSubTitle = mFacebookAd.getAdSubtitle() != null ? mFacebookAd.getAdSubtitle() : "";
//            TextView genericAdTitle = (TextView) mBannerLayout.findViewById(R.id.generic_ad_title);
//            TextView genericAdSubtitle = (TextView) mBannerLayout.findViewById(R.id.generic_ad_subtitle);
//            TextView adButton = (TextView) mBannerLayout.findViewById(R.id.generic_ad_ctabutton);
//            ImageView adImage = (ImageView) mBannerLayout.findViewById(R.id.generic_ad_image);
//            ImageView closeIcon = (ImageView) mBannerLayout.findViewById(R.id.generic_close_button);
//
//            genericAdTitle.setText(adTitle);
//            genericAdSubtitle.setText(adSubTitle);
//            genericAdSubtitle.setSelected(true);
//            adButton.setText(titleAdForButton);
//            NativeAd.downloadAndDisplayImage(imageAd, adImage);
//
//            closeIcon.setVisibility(mShowCloseButton ? View.VISIBLE : View.INVISIBLE);
//            closeIcon.setOnClickListener(this);
//
//            List<View> clickableList = new ArrayList<>();
//            clickableList.add(adButton);
//
//            ViewGroup adChoicesHolder = (ViewGroup) mBannerLayout.findViewById(R.id.adchoices_viewholder);
//            if (adChoicesHolder != null && adChoicesHolder.getChildCount() == 0) {
//                adChoicesHolder.addView(new AdChoicesView(context, mFacebookAd, false));
//            }
//            showBannerView(true);
//            mFacebookAd.registerViewForInteraction(mBannerLayout, clickableList);
//
//        } else {
//            showBannerView(false);
//        }
//    }
//
//    public void showBannerView(boolean show) {
//        if (mBannerLayout != null){
//            mBannerLayout.setVisibility(show ? View.VISIBLE : View.GONE);
//        }
//    }
//
//    public boolean isAdLoaded() {
//        return mFacebookAd.isAdLoaded();
//    }
//
//    @Override
//    public void onClick(View v) {
//        if (v.getId() == R.id.generic_close_button) {
//            showBannerView(false);
//        }
//    }
//}
