/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.ads;

import android.content.Context;
import android.view.View;

/**
 * Interface class for different ads manager,  taking care of the register, unregister and loading
 * of an ad (despite of ad type like facebook, or other provider)
 * Each client should implement this interface, providing an ad Container and taking care of it's management insertLoadedAdIntoLayoutContainer()
 */
public interface AdsCallbackInterface {

    void register(Context context, View view);

    void load();

    void unregister();

}
