/*******************************************************************************
File.......: $URL: $
Copyright (C) 1986-2011 Avira GmbH. All rights reserved.

Revision...: $Revision: $
Modified...: $Date: $
Author.....: $Author: $
 *******************************************************************************/
package com.avira.common.deviceadmin;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

import com.avira.common.GeneralPrefs;
import com.avira.common.deviceadmin.DeviceAdminManager;

/*
 * Class to handle while DeviceAdmin being enabled or disabled
 */
public class DeviceAdminCustomReceiver extends DeviceAdminReceiver
{
	@Override
	public void onEnabled(Context context, Intent intent)
	{
		super.onEnabled(context, intent);
		saveDeviceAdministratorState(context, true);
	}

	@Override
	public void onDisabled(Context context, Intent intent)
	{
		super.onDisabled(context, intent);
		saveDeviceAdministratorState(context, false);
	}

	private void saveDeviceAdministratorState(Context context, boolean enabled)
	{
		GeneralPrefs.setDeviceAdmin(context, enabled);
		DeviceAdminManager.notifyDeviceAdminStatusChanged(context, enabled);
	}
}
