/*******************************************************************************
 * File.......: $URL: $
 * Copyright (C) 1986-2011 Avira Operations GmbH & Co. KG. All rights reserved.
 * <p/>
 * Revision...: $Revision: $
 * Modified...: $Date: $
 * Author.....: $Author: $
 *******************************************************************************/

package com.avira.common.deviceadmin;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.avira.common.R;
import com.avira.common.database.Settings;

/**
 * Class that provides methods related to DeviceAdministrator
 */
public class DeviceAdminManager {
    public final static String ACTION_DEVICE_ADMIN_CHANGED = "com.avira.common.deviceadmin.STATUS_CHANGED";
    public final static String EXTRA_DEVICE_ADMIN_STATE = "extra_device_admin_state";
    /**
     * The device admin policy version. Increment this value whenever there's a change in the device admin policy
     */
    public static final String NEW_DEVICE_ADMIN_VERSION = "1";
    public static final String DEFAULT_DEVICE_ADMIN_VERSION = "0";
    private final static String TAG = DeviceAdminManager.class.getSimpleName();

    /**
     * Disable DeviceAdministrator
     *
     * @param context Application context
     * @return boolean value true if DeviceAdministrator can be disabled, else return false
     */
    public static boolean disableDeviceAdministrator(Context context) {
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager)
                context.getSystemService(Context.DEVICE_POLICY_SERVICE);

        try {
            devicePolicyManager.removeActiveAdmin(new ComponentName(context, DeviceAdminCustomReceiver.class));
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return false;
        }

        return true;
    }

    /**
     * Publish state of DeviceAdministrator
     */
    public static void notifyDeviceAdminStatusChanged(Context context, boolean state) {
        Intent notifyIntent = new Intent(ACTION_DEVICE_ADMIN_CHANGED);
        notifyIntent.putExtra(EXTRA_DEVICE_ADMIN_STATE, state);

        LocalBroadcastManager.getInstance(context).sendBroadcast(notifyIntent);
    }

    /**
     * Get current device admin version
     *
     * @return String current device admin version
     */
    private static String getCurrentDeviceAdminVersion() {
        return Settings.readDeviceAdminVersion();
    }

    /**
     * Set current device admin version
     *
     * @param version current device admin version
     */
    private static void setCurrentDeviceAdminVersion(String version) {
        Settings.writeDeviceAdminVersion(version);
    }

    /**
     * Set current device admin version to the new device admin version
     */
    public static void setDeviceAdminPolicyChangeHandled() {
        setCurrentDeviceAdminVersion(NEW_DEVICE_ADMIN_VERSION);
    }

    /**
     * Check if the new device admin policy change has been handled.
     *
     * @return boolean true if the new device admin policy change has been handled, otherwise false
     */
    public static boolean isDeviceAdminPolicyChangeHandled() {
        return NEW_DEVICE_ADMIN_VERSION.equals(getCurrentDeviceAdminVersion());
    }

    /**
     * Get the intent for Avira System Device Admin screen
     *
     * @param context
     * @return The intent for Avira System Device Admin screen
     */
    public static Intent getAviraSystemDeviceAdminIntent(Context context) {
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, new ComponentName(context, DeviceAdminCustomReceiver.class));
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, context.getString(R.string.DeviceAdminExplanation));
        return intent;
    }
}
