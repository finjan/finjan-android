package com.avira.common;

/**
 * interface to be implemented by all model classes so proguard knows to avoid obfuscating them(avoids declaring custom
 * rules for each folder that contains model classes)
 *
 * @author ovidiu.buleandra
 * @since 26.10.2015
 */
public interface GSONModel {
}
