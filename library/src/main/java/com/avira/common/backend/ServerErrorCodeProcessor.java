package com.avira.common.backend;

import java.util.ArrayList;

public class ServerErrorCodeProcessor
{
	private ArrayList<Integer> mErrorCodeTerminateApp;
	private ArrayList<Integer> mErrorCodeResetApp;
	private ArrayList<Integer> mErrorCodeClearPasswordField;
	private static ServerErrorCodeProcessor sServerErrorCodeProcessor;

	private static final int INVALID_REGISTRATION = 903;
	private static final int INVALID_REGISTRATION_MISSING_IMEI_EMAIL = 904;
	private static final int INVALID_REGISTRATION_MISSING_C2DM_ID = 906;

	public static final int INVALID_IMEI_DEVICEID = 912;
	public static final int DEVICE_ID_NOT_FOUND = 915;
	public static final int IMEI_NOT_FOUND = 916;
	public static final int REGISTERED_EMAIL_NOT_FOUND = 919;

	public static final int INVALID_EMAIL_PASSWORD = 910;
	public static final int ACCOUNT_ALREADY_EXIST = 923;

	private ServerErrorCodeProcessor()
	{
		mErrorCodeTerminateApp = new ArrayList<Integer>();
		mErrorCodeResetApp = new ArrayList<Integer>();
		mErrorCodeClearPasswordField = new ArrayList<Integer>();

		initialize();
	}

	private void initialize()
	{
		mErrorCodeTerminateApp.add(INVALID_REGISTRATION);
		mErrorCodeTerminateApp.add(INVALID_REGISTRATION_MISSING_IMEI_EMAIL);
		mErrorCodeTerminateApp.add(INVALID_REGISTRATION_MISSING_C2DM_ID);

		mErrorCodeResetApp.add(INVALID_IMEI_DEVICEID);
		mErrorCodeResetApp.add(DEVICE_ID_NOT_FOUND);
		mErrorCodeResetApp.add(IMEI_NOT_FOUND);
		mErrorCodeResetApp.add(REGISTERED_EMAIL_NOT_FOUND);

		mErrorCodeClearPasswordField.add(INVALID_EMAIL_PASSWORD);
		mErrorCodeClearPasswordField.add(ACCOUNT_ALREADY_EXIST);
	}

	public synchronized static ServerErrorCodeProcessor getInstance()
	{
		if (sServerErrorCodeProcessor == null)
		{
			sServerErrorCodeProcessor = new ServerErrorCodeProcessor();
		}
		return sServerErrorCodeProcessor;
	}

	public boolean shouldClearPasswordField(int errorCode)
	{
		return mErrorCodeClearPasswordField.contains(errorCode);
	}

	public boolean getIsTerminateApplication(int errorCode)
	{
		return mErrorCodeTerminateApp.contains(errorCode);
	}

	public boolean getIsResetApplication(int errorCode)
	{
		return mErrorCodeResetApp.contains(errorCode);
	}
}
