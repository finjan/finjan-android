package com.avira.common.backend.models

import com.avira.common.CommonLibrary
import com.avira.common.GSONModel
import com.google.gson.annotations.SerializedName

/**
 * Created by anurag on 21/03/18.
 */
open class BaseTokenPayloadSocial: GSONModel{

    @SerializedName("client_id")
    var client_id = CommonLibrary.CLIENT_ID_Social

    @SerializedName("client_secret")
    var client_secret = CommonLibrary.SECRET_ID_Social

//    protected fun setClient_id(client_id: String) {
//        this.client_id = client_id
//    }
//
//    protected fun setClient_secret(client_secret: String) {
//        this.client_secret = client_secret
//    }
//
//    protected fun setGrantype(grantype: String) {
//        this.grantype = grantype
//    }
}