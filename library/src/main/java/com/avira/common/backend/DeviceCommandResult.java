package com.avira.common.backend;

/**
 * @author ovidiu.buleandra
 * @since 24.08.2015
 */
public class DeviceCommandResult {
    public static final String COMPLETED = "COMPLETED";
    public static final String FAILED = "FAILED";
    public static final String OK = "OK";
    public static final String ON = "ON";
    public static final String OFF = "OFF";
    public static final String PREMIUM = "premium";
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    public static final String ALREADY_LOCKED = "already_locked";
    public static final String ALREADY_UNLOCKED = "already_unlocked";
}
