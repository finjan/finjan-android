package com.avira.common.backend.models;

import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anurag on 13/06/17.
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class AccessTokenPayLoad extends BasicTokenPayload{

    public AccessTokenPayLoad(String username, String password){
        setGrantype("password");
        setUsername(username);
        setPassword(password);
    }

    @SerializedName("username")
    protected String username;


    @SerializedName("password")
    protected String password;

    protected void setUsername(String username){this.username=username;}
    protected void setPassword(String password){this.password=password;}

}
