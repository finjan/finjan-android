package com.avira.common.backend.oe;

import android.util.Log;

import com.android.volley.Response;

/**
 * @author ovidiu.buleandra
 * @since 20.11.2015
 */
public class RESTRequest<T> extends OeRequest<Object, T> {
    private static final String TAG = RESTRequest.class.getName();

    public RESTRequest(String url, Class<T> responseCls, Response.Listener<T> responseListener,
                       Response.ErrorListener errorListener) {
        super(Method.GET, url, null, responseCls, responseListener, errorListener);
        Log.d(TAG, "[request] " + url);
    }

    public RESTRequest(String url, Class<T> responseCls, Response.Listener<T> responseListener,
                       Response.ErrorListener errorListener, int initialTimeoutMs, int maxNumRetries,
                       float backoffMultiplier) {
        super(Method.GET, url, null, responseCls, responseListener, errorListener, initialTimeoutMs,
                maxNumRetries, backoffMultiplier);
        Log.d(TAG, "[request] " + url);
    }
}
