package com.avira.common.backend.models;

import com.avira.common.CommonLibrary;
import com.avira.common.GSONModel;

/**
 * Created by anurag on 13/06/17.
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class AccessTokenPayLoadGg implements GSONModel{
    public AccessTokenPayLoadGg(String tokenId, String socialId){
        data=new Data(tokenId, socialId);
    }
    Data data;

    class Data{
        public Data(String tokenId, String socialId){
           attributes=new Attributes(tokenId,socialId);
        }
        String client_id= CommonLibrary.CLIENT_ID_Social;
        String client_secret= CommonLibrary.SECRET_ID_Social;
        Attributes attributes;
    }

    class Attributes{
        public Attributes(String tokenId, String socialId){
            this.token_id=tokenId;
            this.gg_id=socialId;
        }
        String token_id="";
        String gg_id="";
        String device_type="android";

    }
}

