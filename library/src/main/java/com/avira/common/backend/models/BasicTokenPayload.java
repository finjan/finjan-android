package com.avira.common.backend.models;

import com.avira.common.CommonLibrary;
import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anurag on 13/06/17.
 *
 *
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class BasicTokenPayload implements GSONModel {

    @SerializedName("grant_type")
    protected String grantype;

    @SerializedName("client_id")
    protected String client_id= CommonLibrary.CLIENT_ID;

    @SerializedName("client_secret")
    protected String client_secret=CommonLibrary.SECRET_ID;

    protected void setClient_id(String client_id){this.client_id=client_id;}
    protected void setClient_secret(String client_secret){this.client_secret=client_secret;}
    protected void setGrantype(String grantype){this.grantype=grantype;}

}
