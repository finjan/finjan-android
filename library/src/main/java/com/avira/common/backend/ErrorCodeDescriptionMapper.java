package com.avira.common.backend;

import android.content.Context;
import android.util.Log;

import com.avira.common.R;

import java.util.HashMap;

/**
 * Class to process web error code to get error description.
 */
public class ErrorCodeDescriptionMapper {
    private static final String TAG = ErrorCodeDescriptionMapper.class.getSimpleName();
    private static final String ERROR_DESC_JSON_FORMAT = "{\"status\":\"error\",\"errorCode\":%d,\"errorMsg\":\"%s\"}";
    public static final int ACCOUNT_ERROR_CODE_MAX = 999;
    public static final int ACCOUNT_ERROR_CODE_MIN = 900;

    /**
     * Enum containing list of error codes and visible error description string respectively
     */
    public enum Error {
        // @formatter:off
		NO_COMMAND(R.string.web_error_no_command, 901),
		UNKNOWN_ERROR(R.string.web_error_unknown_error,902),
		REGISTRATION_FAILED(R.string.web_error_login_invalid_password,903),
		REGISTRATION_DEVICE_INCOMPATIBLE(R.string.web_error_reg_device_incompatible,904),
		REGISTRATION_MAX_DEVICE_REACHED(R.string.web_error_max_device,905),
		REGISTRATION_FAILED_C2DM_ID(R.string.web_error_reg_fail,906),
		INVALID_REQUEST(R.string.web_error_invalid_request,907),
		INVALID_DEVICE(R.string.web_error_invalid_device,908),
		INVALID_LOGIN_PASSWORD(R.string.web_error_login_invalid_password,910),
		UNLINK_INVALID_IMEI_DEVICEID(R.string.web_error_unlinked,912),
		UNLINK_DEVICE_ID_NOT_FOUND(R.string.web_error_unlinked,915),
		UNLINK_IMEI_NOT_FOUND(R.string.web_error_unlinked,916),
		UNLINK_REGISTERED_EMAIL_NOT_FOUND(R.string.web_error_unlinked,919),
		REGISTRATION_ACCOUNT_EXIST(R.string.web_error_login_invalid_password,923);
		// @formatter:on

        private int mResource;
        private int mErrorCode;

        /**
         * Constructor to group description strings id with error code
         *
         * @param resource  Description strings id
         * @param errorCode Error code
         */
        Error(int resource, int errorCode) {
            mResource = resource;
            mErrorCode = errorCode;
        }

        /**
         * Get error code
         *
         * @return error code
         */
        public int getErrorCode() {
            return mErrorCode;
        }

        /**
         * Get description strings id
         *
         * @return Description strings id
         */
        public int getResourceId() {
            return mResource;
        }
    }

    private final static HashMap<Integer, Error> ERROR_DESCRIPTION_MAPPING = new HashMap<>();

    static {
        for (Error errorMapEntry : Error.values()) {
            ERROR_DESCRIPTION_MAPPING.put(errorMapEntry.getErrorCode(), errorMapEntry);
        }
    }

    private static String formatMappedErrorDescriptionWithParameters(String errorDescription, Error error) {
        switch (error) {
            case REGISTRATION_ACCOUNT_EXIST:
                // not sure do we need this at all.
                return String.format(errorDescription, "");

            default:
                return errorDescription;
        }
    }

    /**
     * Get localized error description for error code
     * <p/>
     * <br>
     * <p/>
     * Example: 923 to An account already exists for {email}. Please log in or reset password instead.
     *
     * @param errorCode Error code to be mapped to the return error description
     * @return Error description strings or null if error code is unknown
     */
    public static String getErrorDescription(Context context, int errorCode) {
        String result = null;

        // Get error enum type from error code
        Error errorType = ERROR_DESCRIPTION_MAPPING.get(errorCode);
        if (errorType == null) {
            errorType = ERROR_DESCRIPTION_MAPPING.get(Error.UNKNOWN_ERROR.getErrorCode());
            Log.e(TAG, "Found an untreated server error code: " + errorCode);
        }

        Integer detectionResourceId = errorType.getResourceId();


        if (detectionResourceId != null) {
            result = formatMappedErrorDescriptionWithParameters(context.getString(detectionResourceId), errorType);
        } else {
            result = formatMappedErrorDescriptionWithParameters(context.getString(R.string.web_error_unknown_error),
                    errorType);
        }

        result = String.format(ERROR_DESC_JSON_FORMAT, errorCode, result);

        return result;
    }

    public static Error getError(int errorCode) {
        // Get error enum type from error code
        Error errorType = ERROR_DESCRIPTION_MAPPING.get(errorCode);

        if (errorType != null) {
            return errorType;
        } else {
            return Error.UNKNOWN_ERROR;
        }

    }
}
