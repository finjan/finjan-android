/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.backend.models;

import android.content.Context;

import com.avira.common.GSONModel;
import com.avira.common.authentication.models.EmailBreach;
import com.avira.common.backend.DeviceCommandResult;
//import com.avira.common.gcm.GCMRegistration;
import com.avira.common.utils.DeviceInfo;
import com.avira.common.utils.GeneralUtility;
import com.avira.common.utils.StorageUtilities;
import com.google.gson.annotations.SerializedName;

import java.util.List;

//@formatter:off
/**
 * Gson model that is used to create "info" OE backend json request data.
 * All fields can be cherry-picked according to the requirement because
 * only the explicitly set fields will appear in the resulting json data.
 * i.e. null fields are excluded
 *
 * Sample (not all fields under "info" shown in this sample):
 *
 *   "info":{
 *      "deviceManufacturer":"LGE",
 *      "deviceModel":"Nexus 5",
 *      "locale":"en_US",
 *      "osVersion":"5.1",
 *      "phoneNumber":"unknown",
 *      "platform":"android",
 *      "registrationId":"",
 *      "ssid":"av-guest",
 *      "versionNo":"4.1.3659"
 *   }
 *
 * Created by hui-joo.goh on 6/26/2015.
 */
//@formatter:on
@SuppressWarnings("unused")
public class Info implements GSONModel {

    // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
    @SerializedName("registrationId") private String registrationId;
    @SerializedName("deviceModel") private String deviceModel;
    @SerializedName("deviceManufacturer") private String deviceManufacturer;
    @SerializedName("phoneNumber") private String phoneNumber;
    @SerializedName("versionNo") private String versionNo;
    @SerializedName("osVersion") private String osVersion;
    @SerializedName("locale") private String locale;
    @SerializedName("platform") private String platform;
    @SerializedName("ssid") private String ssid;
    @SerializedName("mobileNetworkCode") private String mobileNetworkCode;
    @SerializedName("mobileCountryCode") private String mobileCountryCode;
    @SerializedName("statusCode") private String statusCode;
    @SerializedName("devAdmin") private String devAdmin;
    @SerializedName("storageFreeSpace") private String storageFreeSpace;
    @SerializedName("_checksum") private String _checksum;

    @SerializedName("latitude") private String latitude;
    @SerializedName("longitude") private String longitude;
    @SerializedName("accuracy") private String accuracy;
    @SerializedName("locatorType") private String locatorType;

    @SerializedName("orderId") private String orderId;
    @SerializedName("packageName") private String packageName;
    @SerializedName("productId") private String productId;
    @SerializedName("purchaseTime") private Long purchaseTime;
    @SerializedName("purchaseState") private Integer purchaseState;
    @SerializedName("purchaseToken") private String purchaseToken;
    @SerializedName("purchaseType") private String purchaseType;
    @SerializedName("licenseType") private String licenseType;
    @SerializedName("developerPayload") private String developerPayload;
    @SerializedName("price") private String price;
    @SerializedName("currency") private String currency;
    @SerializedName("emailBreaches") private List<EmailBreach> emailBreaches;

    @SerializedName("emails") private String[] emails;
    @SerializedName("productAcronym") private String productAcronym;

    @SerializedName("isTest") private Boolean isTest;

    // VPN monthly subscription
    @SerializedName("runtime") private Integer runtime;
    @SerializedName("subscriptionType") private String subscriptionType;

    /**
     * Include GCM registration ID information
     */
    public void addGcmRegistrationId() {
//        this.registrationId = GCMRegistration.getInstance().getGCMId();
    }

    /**
     * Include device model information
     */
    public void addDeviceModel() {
        this.deviceModel = GeneralUtility.defaultString(DeviceInfo.getDeviceModel(), DeviceInfo.Const.DEVICE_INFO_UNKNOWN);
    }

    /**
     * Include device manufacturer information
     */
    public void addDeviceManufacturer() {
        this.deviceManufacturer = GeneralUtility.defaultString(DeviceInfo.getDeviceManufacturer(), DeviceInfo.Const.DEVICE_INFO_UNKNOWN);
    }

    /**
     * Include phone number information
     */
    public void addPhoneNumber(Context context) {
        this.phoneNumber = GeneralUtility.defaultString(DeviceInfo.getDevicePhoneNumber(context), DeviceInfo.Const.DEVICE_INFO_UNKNOWN);
    }

    /**
     * Include application version number information
     */
    public void addAppVersionNo(Context context) {
        this.versionNo = DeviceInfo.getApplicationVersion(context);
    }

    /**
     * Include OS version information
     */
    public void addOsVersion() {
        this.osVersion = DeviceInfo.getOperatingSystemVersion();
    }

    /**
     * Include locale information
     */
    public void addLocale(Context context) {
        this.locale = new DeviceInfo(context).getLocale();
    }

    /**
     * Include platform information
     */
    public void addPlatform() {
        this.platform = DeviceInfo.PLATFORM;
    }

    /**
     * Include SSID information
     */
    public void addSsid(Context context) {
        this.ssid = new DeviceInfo(context).getSsid();
    }

    /**
     * Include mobile network code information
     */
    public void addMobileNetworkCode(Context context) {
        this.mobileNetworkCode = DeviceInfo.getDeviceSimOperatorNetworkCode(context);
    }

    /**
     * Include mobile country code information
     */
    public void addMobileCountryCode(Context context) {
        this.mobileCountryCode = DeviceInfo.getDeviceSimOperatorCountryCode(context);
    }

    /**
     * Include status code OK information
     */
    public void addStatusCodeOk() {
        this.statusCode = DeviceCommandResult.OK;
    }

    /**
     * Include device admin status information
     */
    public void addDeviceAdminState(Context context) {
        this.devAdmin = DeviceInfo.getDeviceAdminState(context);
    }

    /**
     * Include the available storage space information
     */
    public void addStorageFreeSpace() {
        this.storageFreeSpace = StorageUtilities.getAvailableSpace();
    }

    /**
     * Include tracking checksum information
     */
    public void addTrackingChecksum() {
        this._checksum = "";
        // TODO reactivate
        // TrackingConfigurations.getInstance().getTrackingChecksum();
    }

    /**
     * Include the specified location information
     *
     * @param locatorType The locator type (GPS, CELLTOWER or NO_LOCATOR)
     * @param latitude    The latitude (x-coordinate)
     * @param longitude   The longitude (y-coordinate)
     * @param accuracy    Estimated accuracy of the location, in meters.
     */
    public void addLocationInfo(String locatorType, String latitude, String longitude, String accuracy) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
        this.locatorType = locatorType;
    }

    /**
     * Include the specified IAB order ID information
     *
     * @param orderId The order id
     */
    public void addOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * Include the specified package name information
     *
     * @param packageName The package name
     */
    public void addPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * Include the specified IAB product id information
     *
     * @param productId The product ID
     */
    public void addProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Include the specified IAB purchase time information
     *
     * @param purchaseTime The purchase time
     */
    public void addPurchaseTime(long purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    /**
     * Include the specified IAB purchase state information
     *
     * @param purchaseState The purchase state
     */
    public void addPurchaseState(int purchaseState) {
        this.purchaseState = purchaseState;
    }

    /**
     * Include the specified IAB purchase token information
     *
     * @param purchaseToken The purchase token
     */
    public void addPurchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }

    /**
     * Include the specified IAB developer payload information
     *
     * @param developerPayload The developer payload
     */
    public void addDeveloperPayload(String developerPayload) {
        this.developerPayload = developerPayload;
    }

    /**
     * Include the specified IAB price information
     *
     * @param price The price
     */
    public void addPrice(String price) {
        this.price = price;
    }

    /**
     * Include the specified IAB currency information
     *
     * @param currency The currency
     */
    public void addCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Include the specified IAB runtime information
     *
     * @param runtime The runtime
     */
    public void addRuntime(int runtime) {
        this.runtime = runtime;
    }

    /**
     * Include the specified list of dismissed email breaches
     *
     * @param emailBreaches The list of dismissed email breaches
     */
    public void addEmailBreaches(List<EmailBreach> emailBreaches) {
        this.emailBreaches = emailBreaches;
    }

    /**
     * Include the specified list of emails obtained from the device
     *
     * @param emails The list of emails on the device
     */
    public void addEmails(String[] emails) {
        this.emails = emails;
    }

    public void setProductAcronym(String value) {
        productAcronym = value;
    }

    public void addPurchaseType(String type) {
        this.purchaseType = type;
    }

    public void addLicenseType(String type) {
        this.licenseType = type;
    }

    public void addSubscriptionType(String type) {
        this.subscriptionType = type;
    }

    public Boolean getIsTest() {
        return isTest;
    }

    public void setIsTest(Boolean isTest) {
        this.isTest = isTest;
    }

}
