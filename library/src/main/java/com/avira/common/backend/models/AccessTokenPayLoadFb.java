package com.avira.common.backend.models;

import com.avira.common.CommonLibrary;
import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anurag on 13/06/17.
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class AccessTokenPayLoadFb implements GSONModel{

    public AccessTokenPayLoadFb(String tokenId, String socialId){
        data=new Data(tokenId,socialId);
    }
    Data data;

class Data{
        public Data(String tokenId, String socialId){
            attributes=new Attributes(tokenId,socialId);
        }
        String client_id= CommonLibrary.CLIENT_ID_Social;
        String client_secret= CommonLibrary.SECRET_ID_Social;
        Attributes attributes;
}

class Attributes{
   public Attributes(String tokenId, String socialId){
       setFBId(socialId);
       setToken(tokenId);
   }
    private void setFBId(String fb_id){this.fb_id=fb_id;}
    private void setToken(String token_id){this.token_id=token_id;}
    String token_id="";
    String fb_id="";
    String device_type="android";
}
}
