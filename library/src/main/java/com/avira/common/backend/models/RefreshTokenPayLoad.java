package com.avira.common.backend.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anurag on 13/06/17.
 *
 *
 *
 *
 *
 * Finjan Mobile, Inc. CONFIDENTIAL INTERNAL DOCUMENT
 * Finjan Mobile Vital Security Browser
 *
 * Copyright (c) 2017 Finjan Mobile, Inc. All rights reserved.
 */

public class RefreshTokenPayLoad extends BasicTokenPayload{

    public RefreshTokenPayLoad(String refresh_token){
        setGrantype("refresh_token");
        setRefresh_token(refresh_token);
    }

    @SerializedName("refresh_token")
    String refresh_token;

    protected void setRefresh_token(String refresh_token){this.refresh_token=refresh_token;}
}
