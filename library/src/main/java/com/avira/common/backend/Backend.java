package com.avira.common.backend;

import com.avira.common.BuildConfig;
import com.avira.common.backend.oe.OeServerPins;

/**
 * Class to store respective server pins used for SSL Pinning - prevention for Man-In-The-Middle-Attack.
 * Server pins are hex-encoded hash of the public key of each X.509 certificate in the server certificate chain.
 * These set of pins will be compared against the set of pins that will be generated from the
 * respective server certificate chain when attempting to communicate with the server.
 * HTTPS connection will be established only if all pins match (SSL Pinning is not applicable for HTTP connection).
 * <p>
 * How to generate server pins?
 * 1. First, obtain the server certificate chain:
 * - Get certificate chain via this command: openssl s_client -connect {HOSTNAME}:{PORT} -showcerts
 * For example:
 * $ openssl s_client -connect ssld.oes.avira.com:443 -showcerts
 * - Then, convert into a PEM file
 * http://how2ssl.com/articles/working_with_pem_files/
 * <p>
 * 2. Generate the pins (hex-encoded hash of the public key of each X.509 certificate in the server certificate chain)
 * using pin.py script from:
 * - https://github.com/moxie0/AndroidPinning/tree/master/tools
 * $ python pin.py certificate_file.pem
 *
 * @author hui-joo.goh
 * @author ovidiu.buleandra
 */

public class Backend  {

    private static final String currentUrl="https://ulm.finjanmobile.com/";
//    private static final String currentUrl="https://vincestaging.wpengine.com/";
    public static final String liveUrl ="https://ulm.finjanmobile.com/wp-json/api/v1/";
    public static final String stagingurl="https://vitalulm.staging.wpengine.com/wp-json/api/v1/";
//    public static final String stagingurl2="https://vincestaging.wpengine.com/wp-json/api/v1/";
    public static final String stagingurl2="http://vitalulm.staging.wpengine.com/wp-json/api/v2/";
//    private static final String licentUrl=/*"https://ulm.finjanmobile.com/wp-json/api/v1/*/stagingurl2+"license";
    private static final String licentUrl=/*"https://ulm.finjanmobile.com/wp-json/api/v1/*/liveUrl +"license";


    //testing ulm social
    public static final String registerSocialUser="http://vitalulm.staging.wpengine.com/wp-json/api/v1/users";
//    public static final String refreshSocialToken=currentUrl+"wp-json/oauth/social/token/";
    public static final String refreshSocialToken=currentUrl+"wp-json/oauth/social/token/";


    public final static Profile ACCEPTANCE = new Profile(
            currentUrl, licentUrl, null);

    public final static Profile PRODUCTION = ACCEPTANCE;


//    public final static Profile ACCEPTANCE = new Profile(
//            "https://api.oeacc.avira.com/android/v2/",
//            "https://license.acc.avira.com",
//            new String[]{"f0ccf098ce26984691b0e189dac816e2713d0682",
//                    "497c6868e484ccf0ba0601a6c40b7f10072c6a3c",
//                    "6e584e3375bd57f6d5421b1601c2d8c0f53a9f6e",
//                    "4F9C7D21799CAD0ED8B90C579F1A0299E790F387"});
//
//    public final static Profile PRODUCTION = new Profile(
//            "https://api.my.avira.com/android/v2/",
//            "https://license.avira.com",
//            new String[] {"5d23c8bbbb5cc9521b950d38c54c14b3ab5fb486",
//                    "8f6fb615ce13fc9cc6740db87b1825695fab663f",
//                    "6e584e3375bd57f6d5421b1601c2d8c0f53a9f6e",
//                    "4f9c7d21799cad0ed8b90c579f1a0299e790f387"});

    public final static Profile TEST = ACCEPTANCE;

    public final static Profile DEV = ACCEPTANCE;



//    public static String BACKEND_URL = BuildConfig.BACKEND_URL;
    public static String BACKEND_URL = currentUrl;
//    public static String LICENSE_URL = BuildConfig.LICENSE_URL;
    public static String LICENSE_URL = licentUrl;
    public static String[] SERVER_PINS = BuildConfig.SERVER_PINS.equals("debug")
            ? OeServerPins.TEST_SERVER_PINS : OeServerPins.PRODUCTION_SERVER_PINS;

    /**
     * method should be used in the application class to select the server in the initialization faze
     *
     * normal use is Backend.selectServer(BuildConfig.SERVER)
     * BuildConfig.SERVER is taken care of by the avira plugin
     * for each build type and is deduced from the build name.
     *
     * for special builds this server can be selected from the predefined profiles or specified through
     * selectServer(String backendUrl, String licenseUrl, String[] pins)
     * @param profileName
     */
    public static void selectServer(String profileName) {
        switch (profileName) {
            default: // default or "acc"
                selectServer(ACCEPTANCE);
                break;
            case "prod":
                selectServer(PRODUCTION);
                break;
            case "test":
                selectServer(TEST);
                break;
            case "dev":
                selectServer(DEV);
                break;
        }
    }

    public static void selectServer(String backendUrl, String licenseUrl, String[] pins) {
        BACKEND_URL = backendUrl;
        LICENSE_URL = licenseUrl;
        SERVER_PINS = pins;
    }

    public static void selectServer(Profile serverProfile) {
        selectServer(serverProfile.backend_url, serverProfile.license_url, serverProfile.server_pins);
    }

    public static class Profile {
        String backend_url;
        String license_url;
        String[] server_pins;

        Profile(String url, String license, String[] pins) {
            backend_url = url;
            license_url = license;
            server_pins = pins;
        }
    }
}
