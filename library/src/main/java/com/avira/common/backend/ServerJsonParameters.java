/*******************************************************************************
 * File.......: $URL: $
 * Copyright (C) 1986-2011 Avira Operations GmbH & Co. KG. All rights reserved.
 * <p/>
 * Revision...: $Revision: $
 * Modified...: $Date: $
 * Author.....: $Author: $
 *******************************************************************************/

package com.avira.common.backend;
/**
 * Take caution when modifying because you might corupt the CommandIntegrator pacing
 */

import com.avira.common.utils.JsonCommandClass;

/**
 * Class to provide Json parameters being sent by the application server to the device
 */
public class ServerJsonParameters extends JsonCommandClass {
    public static final String SERVER_DEVICE_ID = "deviceId";
    public static final String SERVER_OE_DEVICE_ID = "oeDeviceId";
    public static final String STATUS = "status";
    public static final String STORED_REGISTRATION_ID = "storedRegistrationId";
    public static final String MESSAGE = "message";
    public static final String LOCK_MESSAGE = "lockMessage";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String LOCK_PHONE_NUMBER = "lockPhoneNumber";
    public static final String ERROR_MESSAGE = "errorMsg";
    public static final String ERROR_CODE = "errorCode";
    public static final String ENABLED = "enabled";
    public static final String EXPIREDATE = "expireDate";
    public static final String TYPE = "type";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String IMAGE_URL = "imageUrl";
    public static final String USER = "user";
    public static final String EMAIL = "email";
    public static final String STATUS_CODE = "statusCode";
}
