/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.backend.oe;

import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;
import com.avira.common.backend.Backend;
import com.avira.common.security.PinningHelper;

/**
 * Class to handle the requests made to OE backend server. Reference:
 * https://developer.android.com/training/volley/requestqueue.html
 * <p/>
 * Note: This is a SSL pinned request queue that only works for backend calls specifically made to the OE backend server
 *
 * @author ovidiu.buleandra - removed the old Apache HttpClient initialization
 *
 * @author yushu
 */
public class OeRequestQueue extends RequestQueue {
    private static OeRequestQueue INSTANCE;

    private OeRequestQueue(Cache cache, Network network) {
        super(cache, network);
    }

    /**
     * Get the OE request queue singleton instance. Reference: {@link Volley}
     *
     * @return Singleton instance of the OE request queue
     */
    public static synchronized OeRequestQueue getInstance(Context context) {
        if (INSTANCE == null) {

//            HttpStack stack = Backend.SERVER_PINS != null
//                    ? new HurlStack(null,
//                        PinningHelper.getPinnedSSLSocketFactory(context, Backend.SERVER_PINS))
//                    : new HurlStack();
            HttpStack stack = new HurlStack();

            Network network = new BasicNetwork(stack);

            INSTANCE = new OeRequestQueue(new NoCache(), network);
            INSTANCE.start();
        }
        return INSTANCE;
    }
}
