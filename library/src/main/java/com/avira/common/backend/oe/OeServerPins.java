/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.backend.oe;

//@formatter:off
import com.avira.common.BuildConfig; /**
 * Class to store respective server pins used for SSL Pinning - prevention for Man-In-The-Middle-Attack.
 * Server pins are hex-encoded hash of the public key of each X.509 certificate in the server certificate chain.
 * These set of pins will be compared against the set of pins that will be generated from the
 * respective server certificate chain when attempting to communicate with the server.
 * HTTPS connection will be established only if all pins match (SSL Pinning is not applicable for HTTP connection).
 * 
 * How to generate server pins?
 * 		1. First, obtain the server certificate chain:
 *  		- Get certificate chain via this command: openssl s_client -connect {HOSTNAME}:{PORT} -showcerts
 *   		  For example:
 *    			$ openssl s_client -connect ssld.oes.avira.com:443 -showcerts
 *     		- Then, convert into a PEM file
 *      		http://how2ssl.com/articles/working_with_pem_files/
 * 
 *      2. Generate the pins (hex-encoded hash of the public key of each X.509 certificate in the server certificate chain)
 *         using pin.py script from:
 *         	- https://github.com/moxie0/AndroidPinning/tree/master/tools
 *         		$ python pin.py certificate_file.pem
 * 
 * @author hui-joo.goh
 *
 */
//@formatter:on
public class OeServerPins {
    public static final String[] PRODUCTION_SERVER_PINS = new String[]{"598b730e348e69085a4dab5457b50544391c94be",
            "497c6868e484ccf0ba0601a6c40b7f10072c6a3c", "6e584e3375bd57f6d5421b1601c2d8c0f53a9f6e",
            "4f9c7d21799cad0ed8b90c579f1a0299e790f387"};

    public static final String[] TEST_SERVER_PINS = new String[]{"945AF49E76CB070D4B7139CEF1DD4E9431F93100",
            "497C6868E484CCF0BA0601A6C40B7F10072C6A3C", "6E584E3375BD57F6D5421B1601C2D8C0F53A9F6E",
            "4F9C7D21799CAD0ED8B90C579F1A0299E790F387"};

    /**
     * Get the array of OE Server Pins to be used for SSL Pinning
     *
     * @return the hex-encoded hash of the public key of each X.509 certificate in the server certificate chain
     */
    public static String[] getOeServerPins() {
        switch (BuildConfig.SERVER_PINS) {
            default:
                return TEST_SERVER_PINS;
            case "release":
                return PRODUCTION_SERVER_PINS;
        }
    }
}