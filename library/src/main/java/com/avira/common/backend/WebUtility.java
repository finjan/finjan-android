/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.backend;

import android.text.TextUtils;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Class containing web related utility methods that could be re-used
 * <p/>
 * Created by hui-joo.goh on 7/13/2015.
 */
public class WebUtility {

    private static String defaultError = "Please try again after some time,Unknown error";
    private static String networkError = "Unable to connect, Check you network or try later";
    public static String noConnectionError = "Unable to connect Server, Check you network and try again later";
    public static String connectionTimeOut = "Timeout while connecting to Server, try again later";


    /**
     * Retrieves the error message from the specified volley error
     *
     * @param error The volley error received from backend
     * @return The error message from the volley error. Returns null if no error message provided.
     */
    public static String getMessage(VolleyError error) {
        String errorMessage=errorMessageForUser(error);
        if(!TextUtils.isEmpty(errorMessage)){
            return errorMessage;
        }

        if (error.networkResponse != null && error.networkResponse.data != null) {
            try {
                errorMessage = new String(error.networkResponse.data,
                        HttpHeaderParser.parseCharset(error.networkResponse.headers));
            } catch (UnsupportedEncodingException e) {
                errorMessage = new String(error.networkResponse.data);
            }
        } else {
            errorMessage = error.getMessage();
        }
        return errorMessage;
    }
    public static String errorMessageForUser(VolleyError error){
        if(error instanceof NetworkError || error instanceof NoConnectionError){
            return noConnectionError  ;
        }
        if(error instanceof TimeoutError){
            return connectionTimeOut ;
        }
        return  "";
    }

    public static String getErrorDesc(VolleyError error) {
        String jsonString = getMessage(error);

        if (!TextUtils.isEmpty(jsonString) && jsonString.startsWith("{") && jsonString.endsWith("}")) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                String errorDesc = jsonObject.optString("error_description");
                if (!TextUtils.isEmpty(errorDesc)) {
                    return errorDesc;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return jsonString;
            }
        }
        return defaultError;
    }

    public static int getHTTPErrorCode(VolleyError error) {
        return error.networkResponse != null
                ? error.networkResponse.statusCode
                : ErrorCodeDescriptionMapper.Error.UNKNOWN_ERROR.getErrorCode();
    }
}
