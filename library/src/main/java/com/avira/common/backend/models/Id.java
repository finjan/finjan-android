/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.backend.models;

import android.content.Context;

import com.avira.common.GSONModel;

import com.avira.common.database.Settings;
import com.avira.common.security.HashGeneratorFactory;
import com.avira.common.utils.DeviceInfo;
import com.google.gson.annotations.SerializedName;

//@formatter:off
/**
 * Gson model that is used to create "id" OE backend json request data.
 * All fields can be cherry-picked according to the requirement because
 * only the explicitly set fields will appear in the resulting json data.
 * i.e. null fields are excluded
 *
 * Sample (not all fields under "id" shown in this sample):
 *
 *   "id":{
 *      "commandId":"commandID",
 *      "uid":"1435834774152-19210978921687767635",
 *      "clientId":"com.avira.android",
 *      "deviceId":"4329790",
 *      "oeDeviceId":""
 *   }
 *
 * Created by hui-joo.goh on 6/26/2015.
 */
//@formatter:on
@SuppressWarnings("unused")
public class Id implements GSONModel {

    public final static String IS_PHONE = "imei";
    public final static String IS_TABLET = "hash";
    private final static String PURCHASE_TYPE_PRODUCTS = "products";

    // DO NOT change the variable name for Gson to work. It represents the exact Json key specified by backend
    @SerializedName("uid") private String uid;
    @SerializedName("uidType") private String uidType;
    @SerializedName("serial") private String serial;
    @SerializedName("deviceId") private String deviceId;
    @SerializedName("oeDeviceId") private String oeDeviceId;
    @SerializedName("commandId") private String commandId;
    @SerializedName("clientId") private String clientId;
    @SerializedName("token") private String token;
    @SerializedName("tokenType") private String tokenType;
    @SerializedName("email") private String email;
    @SerializedName("password") private String password;
    @SerializedName("imei") private String imei;
    @SerializedName("isAnonymous") private int isAnonymous;
    @SerializedName("purchaseToken") private String purchaseToken;
    @SerializedName("purchaseType") private String purchaseType;
    @SerializedName("packageName") private String packageName;
    @SerializedName("productId") private String productId;
    // name of the provider, eg. Avira is "avira", dolce telekom is "dt"
    @SerializedName("partnerId") private String partnerId;

    @SerializedName("appId") private String appId;

    /**
     * Include UID (device ID) information
     */
    public void addUid(String uid) {
        this.uid = uid;
    }

    /**
     * Include UID (device ID) type information
     *
     * uidType field is actually used by the backend to determine whether the device is a phone or
     * tablet based on the assertions below
     */
    public void addUidType(Context context) {
        this.uidType = DeviceInfo.isTablet(context)?IS_TABLET:IS_PHONE;
    }

    /**
     * Include serial number information
     */
    public void addSerialNumber() {
        this.serial = DeviceInfo.getSerialNumber();
    }

    /**
     * Include server registered device ID information
     */
    public void addDeviceId() {
        this.deviceId = Settings.readDeviceId();
    }


    /**
     * Include the specified command ID information
     */
    public void addCommandId(String commandId) {
        this.commandId = commandId;
    }

    /**
     * Include the specified client ID information
     */
    public void addClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * Include the specified login token information
     *
     * @param token The login token
     */
    public void addToken(String token) {
        this.token = token;
    }

    /**
     * Include the specified login token type information
     *
     * @param tokenType The login token type
     */
    public void addTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    /**
     * Include the specified login/registration information
     *
     * @param email    The email provided via login/registration by email
     * @param password The password provided via login/registration by email
     */
    public void addLoginInfo(String email, String password) {
        this.email = email;
        if (password != null) {
            this.password = HashGeneratorFactory.getMd5GeneratorSingleton().getHash(password);
        }
    }


    /**
     * Include the specified email information
     *
     * @param email    The email provided via login/registration by email
     */
    public void addEmail(String email) {
        this.email = email;
    }

    /**
     * Add actual device IMEI if it exists
     */
    public void addIMEI(Context context) {
        this.imei = DeviceInfo.getIMEI(context);
    }

    /**
     * Adds parameter that indicates if the user is anonymous or not, this must be added to any call made by an anonymous instance of the app
     */
    public void addIsAnonymous(boolean anonymousUser) {
        this.isAnonymous = anonymousUser ? 1 : 0;
    }


    public void addPurchaseInfo(String email, String purchaseToken, String productId) {
        this.email = email;
        this.purchaseToken = purchaseToken;
        this.purchaseType = PURCHASE_TYPE_PRODUCTS;
        this.productId = productId;
    }

    public void setPackageName(Context context) {
        this.packageName = context.getApplicationContext().getPackageName();
    }

    public void setAppId(String applicationId) {
        appId = applicationId;
    }

    public void setPartner(String partnerId) {
        this.partnerId = partnerId;
    }
}
