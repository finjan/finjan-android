/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.backend.oe;

//@formatter:off
import com.avira.common.GSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Gson model that is used to populate Json response data from OE backend
 *
 * Sample:
 *{
 *   "status":"OK",
 *   "statusCode":200
 *}
 *
 * @author yushu
 */
//@formatter:on
@SuppressWarnings("unused")
public class BaseResponse implements GSONModel {

    private static final String OK = "OK";

    @SerializedName("status") private String status;
    @SerializedName("statusCode") private Integer statusCode;

    public String getStatus() {
        return status;
    }

    public boolean isSuccess() {
        return status.equals(OK);
    }

    public int getStatusCode() {
        return (statusCode == null) ? 0 : statusCode;
    }
}
