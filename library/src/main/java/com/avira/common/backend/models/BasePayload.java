package com.avira.common.backend.models;

import android.content.Context;

import com.avira.common.GSONModel;
import com.avira.common.GeneralPrefs;
import com.avira.common.id.HardwareId;
import com.avira.common.utils.DeviceInfo;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovidiu.buleandra
 * @since 22.10.2015
 */
public class BasePayload implements GSONModel {
    /**
     * device selected language
     */
    @SerializedName("language")
    protected String language;
    /**
     * common info payload
     */
    @SerializedName("info")
    protected Info info;

    /**
     * common id payload
     */
    @SerializedName("id")
    protected Id id;

    public BasePayload(Context context) {
        language = DeviceInfo.getLanguageCode(context);

        info = new Info();
//        info.addGcmRegistrationId();
        info.addAppVersionNo(context);
        info.addPlatform();
        info.addPhoneNumber(context);
        info.addSsid(context);
        info.addLocale(context);
        info.addOsVersion();
        info.addDeviceModel();
        info.addDeviceManufacturer();

        id = new Id();

        id.addUid(HardwareId.get(context));
        id.addUidType(context);
        id.addSerialNumber();
        id.addIMEI(context);
        id.setPackageName(context);
        id.setAppId(GeneralPrefs.getAppId(context));
        id.setPartner(GeneralPrefs.getPartnerId(context));
    }
}
