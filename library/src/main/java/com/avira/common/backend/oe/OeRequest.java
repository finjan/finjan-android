/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.backend.oe;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.avira.common.BuildConfig;
import com.avira.common.CommonLibrary;
import com.avira.common.backend.Backend;
import com.avira.common.backend.GsonRequest;
import com.avira.common.backend.ServerJsonParameters;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Class that extends {@link GsonRequest} to handle the network error case. The OE backend sends the errors as
 * network errors with status codes larger than 900 and they need to be handled separately
 *
 * @param <S> The relevant Gson model class that represents the request body
 * @param <T> Relevant listener object to receive normal backend response
 * @author yushu
 */
public class OeRequest<S, T> extends GsonRequest<S, T> {
    private static final String TAG = OeRequest.class.getName();

    public static final int DEFAULT_TIMEOUT_MS = 4000;
    public static final int DEFAULT_TIMEOUT_MS_DEBUG = 6000;

    /**
     * Content type for request.
     */
    private static final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", GsonRequest.PROTOCOL_CHARSET);

    /**
     * Construct an OE backend request with the specified parameters
     *
     * @param url              URL of the request to make
     * @param requestBody      Relevant GSON model request body
     * @param responseCls      Relevant class object, for Gson's reflection
     * @param responseListener Relevant listener object to receive normal backend response
     * @param errorListener    Listener to receive error response
     */
    public OeRequest(String url, S requestBody, Class<T> responseCls, Listener<T> responseListener,
                     ErrorListener errorListener) {

        this(url, requestBody, responseCls, responseListener, errorListener, CommonLibrary.DEBUG?DEFAULT_TIMEOUT_MS_DEBUG:DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }


    /**
     * Construct an OE backend request with the specified parameters
     *
     * @param url              URL of the request to make
     * @param requestBody      Relevant GSON model request body
     * @param responseCls      Relevant class object, for Gson's reflection
     * @param responseListener Relevant listener object to receive normal backend response
     * @param errorListener    Listener to receive error response
     * @param initialTimeoutMs  The initial timeout for the policy
     */
    public OeRequest(String url, S requestBody, Class<T> responseCls, Listener<T> responseListener,
                     ErrorListener errorListener, int initialTimeoutMs) {
        this(url, requestBody, responseCls, responseListener, errorListener, initialTimeoutMs,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

    /**
     * Construct an OE backend request with the specified parameters
     *
     * @param url               URL of the request to make
     * @param requestBody       Relevant GSON model request body
     * @param responseCls       Relevant class object, for Gson's reflection
     * @param responseListener  Relevant listener object to receive normal backend response
     * @param errorListener     Listener to receive error response
     * @param initialTimeoutMs  The initial timeout for the policy
     * @param maxNumRetries     The maximum number of retries
     * @param backoffMultiplier Backoff multiplier for the policy
     */
    public OeRequest(String url, S requestBody, Class<T> responseCls, Listener<T> responseListener,
                     ErrorListener errorListener, int initialTimeoutMs, int maxNumRetries, float backoffMultiplier) {
        super(url, requestBody, responseCls, null, responseListener, errorListener);
        this.setRetryPolicy(new DefaultRetryPolicy(initialTimeoutMs, maxNumRetries, backoffMultiplier));
    }

    /**
     * Construct an OE backend request with the specified parameters
     *
     * @param method           request method
     * @param url              URL of the request to make
     * @param requestBody      Relevant GSON model request body
     * @param responseCls      Relevant class object, for Gson's reflection
     * @param responseListener Relevant listener object to receive normal backend response
     * @param errorListener    Listener to receive error response
     */
    public OeRequest(int method, String url, S requestBody, Class<T> responseCls, Listener<T> responseListener,
                     ErrorListener errorListener) {
        this(method, url, requestBody, responseCls, responseListener, errorListener, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

    /**
     * Construct an OE backend request with the specified parameters
     *
     * @param method            request method
     * @param url               URL of the request to make
     * @param requestBody       Relevant GSON model request body
     * @param responseCls       Relevant class object, for Gson's reflection
     * @param responseListener  Relevant listener object to receive normal backend response
     * @param errorListener     Listener to receive error response
     * @param initialTimeoutMs  The initial timeout for the policy
     * @param maxNumRetries     The maximum number of retries
     * @param backoffMultiplier Backoff multiplier for the policy
     */
    public OeRequest(int method, String url, S requestBody, Class<T> responseCls, Listener<T> responseListener,
                     ErrorListener errorListener, int initialTimeoutMs, int maxNumRetries, float backoffMultiplier) {
        super(method, url, requestBody, responseCls, null, responseListener, errorListener);
        this.setRetryPolicy(new DefaultRetryPolicy(initialTimeoutMs, maxNumRetries, backoffMultiplier));
    }

    @Override
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {

        try {

            // Status code is not kept in the response body (response.data) but rather response.statusCode
            // so we need to manually include it into the json result here
            String oriJsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            JSONObject jsonObject = new JSONObject(oriJsonString);
            if (!jsonObject.has(ServerJsonParameters.STATUS_CODE)) {
                jsonObject.put(ServerJsonParameters.STATUS_CODE, response.statusCode);
            }

            String newJsonString = jsonObject.toString();
            Log.i(TAG, "[response] " + newJsonString);

            return Response.success(mGson.fromJson(newJsonString, mResponseClass), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }
}
