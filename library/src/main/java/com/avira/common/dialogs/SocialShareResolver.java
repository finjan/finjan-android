package com.avira.common.dialogs;

/**
 * @author ovidiu.buleandra
 * @since 25.11.2015
 */
public interface SocialShareResolver {
    String getShareText(SocialShare shareMedium);
}
