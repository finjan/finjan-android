package com.avira.common.dialogs;

import android.content.Intent;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentActivity;
import android.view.View;

import com.avira.common.R;

/**
 * Utility class to help showing some common types of dialogs
 *
 * @author danielastamati on 25/06/15.
 * @author ovidiu.buleandra refactoring and cleanup
 */
public class CommonDialogs {

    private static final String TAG = CommonDialogs.class.getName();
    private static final String LIST_PREFIX = "- ";
    private static final int INVALID_VERSION_CODE = -1;

    /**
     * Same as {@link #showNetworkUnavailable(FragmentActivity, int, int)} but with a default network unavailable title and
     * a default description
     *
     * @param context valid fragment activity context
     */
    public static void showNetworkUnavailable(final FragmentActivity context) {
        showNetworkUnavailable(context, R.string.PleaseEnableNetwork);
    }

    /**
     * Same as {@link #showNetworkUnavailable(FragmentActivity, int, int)} but with a default network unavailable title
     *
     * @param context   valid fragment activity context
     * @param descResId dialog description string resource id
     */
    public static void showNetworkUnavailable(final FragmentActivity context, @StringRes int descResId) {
        showNetworkUnavailable(context, R.string.NoNetworkAvailable, descResId);
    }

    /**
     * Shows a dialog to warn the user of the lack of network connectivity. This is just a convenience method that
     * customizes the positive button with a settings icon and redirects the user to the android settings screen     *
     *
     * @param context valid fragment activity context
     * @param title   dialog title string resource id
     * @param desc    dialog description string resource id
     */
    public static void showNetworkUnavailable(final FragmentActivity context, @StringRes int title, @StringRes int desc) {

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
            }
        };

        new AviraDialog.Builder(context)
                .setTitle(title)
                .setDesc(desc)
                .setNegativeButton(R.string.Cancel)
                .setPositiveButton(R.string.goto_settings, R.drawable.settings_icon, listener)
                .show(context.getSupportFragmentManager());

    }

    /**
     * Shows a dialog to enable the user to rate the application on the source store
     *
     * @param context   valid fragment activity context
     * @param descResId description resource id
     */
    public static void showRateMe(final FragmentActivity context, @StringRes int descResId) {

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateMeDialogUtils.gotoRatingPage(context);
            }
        };

        new AviraDialog.Builder(context)
                .setTitle(R.string.rateMeDialog_title)
                .setDesc(descResId)
                .setNegativeButton(R.string.not_now)
                .setPositiveButton(R.string.rateMeDialog_possitiveBtn, R.drawable.settings_icon, listener)
                .addImage(R.drawable.stars)
                .show(context.getSupportFragmentManager());

        // save the time when the dialog was last displayed
        RateMeDialogUtils.saveDisplayTime(context);
    }

//    private static void sendShareIntent(final Context context, int index, ShareDialogUtils.ShareZone shareZone) {
//        try {
//            context.startActivity(ShareDialogUtils.getListForPackages(context, shareZone).get(index));
//        } catch (ActivityNotFoundException e) {
//            Log.e("Share", "Share request failed", e);
//            context.startActivity(ShareDialogUtils.getFallbackItem(context, index, shareZone));
//        }
//    }
//

    /**
     * @param context  valid fragment activity context
     * @param resolver
     */
    public static void showSocialShare(final FragmentActivity context, SocialShareResolver resolver) {
        showSocialShare(context, R.string.share_dialog_title, R.string.share_dialog_details, resolver);
    }

    /**
     * @param context  valid fragment activity context
     * @param title
     * @param desc
     * @param resolver
     */
    public static void showSocialShare(final FragmentActivity context, @StringRes int title, @StringRes int desc,
                                       final SocialShareResolver resolver) {

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.btn_negative) {
                    SocialShare.FACEBOOK.share(context, resolver.getShareText(SocialShare.FACEBOOK));
                } else if (view.getId() == R.id.btn_middle) {
                    SocialShare.TWITTER.share(context, resolver.getShareText(SocialShare.TWITTER));
                } else if (view.getId() == R.id.btn_positive) {
                    SocialShare.GOOGLE_PLUS.share(context, resolver.getShareText(SocialShare.GOOGLE_PLUS));
                }
            }
        };

        new AviraDialog.Builder(context)
                .setTitle(title)
                .setDesc(desc)
                .setNegativeButton(null, R.drawable.icon_facebook, listener)
                .setMiddleButton(null, R.drawable.icon_twitter, listener)
                .setPositiveButton(null, R.drawable.icon_google_plus, listener)
                .show(context.getSupportFragmentManager());

    }
}
