package com.avira.common.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avira.common.R;

/**
 * Extended dialog window that has a configurable central icon above the title, slightly rounded corners for the window,
 * a content containing common reusable dialog view components (also can display a list through a custom adapter).
 * <p/>
 * All the dialog's configurable view component visibility is set to GONE by default.
 * Each configurable view component can be cherry-picked via the provided Builder object, or by child classes extending this class
 * according to it's own view needs.
 * <p/>
 * The default elements of the dialog are:
 * - title (can be empty)
 * - description (can be empty)
 * - OK button (rendered as default)
 *
 * @author ovidiu.buleandra
 * @author daniela.stamati
 */
public class AviraDialog extends DialogFragment implements View.OnClickListener, ExpandableListView.OnGroupExpandListener {
    private static final String TAG = AviraDialog.class.getSimpleName();
    private static final int DEFAULT_DESC_GRAVITY = Gravity.CENTER;
    private static final int NO_RESOURCE = 0;

    @DrawableRes private static int DIALOG_ICON_DEFAULT_RESOURCE = R.drawable.icon;
    @ColorRes private int mPositiveTextColor;

    private CharSequence mTitle, mDesc;
    private int mDescGravity;
    private Drawable mIcon;
    private boolean mDimBehind = true;
    private boolean mCancelable = true;
    private boolean mNoButtons = false;
    private View.OnClickListener mPositiveBtnClickListener, mNegativeBtnClickListener, mMiddleBtnClickListener;
    private DialogInterface.OnDismissListener mOnDismissListener;

    private LinearLayout mCustomContent;
    private View mPositiveBtn, mNegativeBtn, mMiddleBtn;
    private SparseArray<View.OnClickListener> mCustomContentListeners;

    private ExpandableListView mExpandableListView;


    private boolean mDisplayContentDivider;

    public static void setDefaultIcon(@DrawableRes int defaultIconResId) {
        DIALOG_ICON_DEFAULT_RESOURCE = defaultIconResId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setCancelable(mCancelable);
        setStyle(STYLE_NO_TITLE, R.style.DialogNoTitle);
        setRetainInstance(true);
    }

    public void show(FragmentManager manager) {
        super.show(manager, TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_layout, container, false);

        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        if (!mDimBehind) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        initializeUI(view);

        return view;
    }

    private void initializeUI(View view) {

        ImageView iconView = (ImageView) view.findViewById(R.id.icon);
        TextView titleView = (TextView) view.findViewById(R.id.title);
        TextView descView = (TextView) view.findViewById(R.id.desc);


        //set icon let not set icon for now
        if (mIcon != null) {
//            iconView.setImageDrawable(mIcon);
        } else {
//            iconView.setImageResource(DIALOG_ICON_DEFAULT_RESOURCE);
        }

        //set title
        if (!TextUtils.isEmpty(mTitle)) {
            titleView.setText(mTitle);
            titleView.setVisibility(View.VISIBLE);
        }

        //set description
        if (!TextUtils.isEmpty(mDesc)) {
            descView.setText(mDesc);
            descView.setGravity(mDescGravity);
            descView.setVisibility(View.VISIBLE);
        }

        //By default the visibility is gone
        if (mDisplayContentDivider) {
            view.findViewById(R.id.content_divider).setVisibility(View.VISIBLE);
        }

        //set custom content
        if (mCustomContent != null) {
            View customContentHolder = view.findViewById(R.id.ll_custom_content);
            customContentHolder.setVisibility(View.VISIBLE);
            ((ViewGroup) customContentHolder).addView(mCustomContent);
        }

        if(!mNoButtons) {
            setButtons(view);
        } else {
            //hide positive button layout, middle & negative buttons are already hidden by default
            View positiveBtn = view.findViewById(R.id.btn_positive);
            positiveBtn.setVisibility(View.GONE);
        }
    }


    public void adjustListViewHeightBasedOnChildren(BaseExpandableListAdapter adapter) {

        if (mExpandableListView != null) {

            if (adapter == null || adapter.getGroupCount() == 0) {
                return;
            }

            int totalHeight = 0;
            for (int i = 0; i < adapter.getGroupCount(); i++) {
                View listItem = adapter.getGroupView(i, false, null, mExpandableListView);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }

            View listItem = adapter.getChildView(0, 0, false, null, mExpandableListView);
            //if the list has expandable elements
            if (listItem != null) {
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams par = mExpandableListView.getLayoutParams();
            par.height = totalHeight + (mExpandableListView.getDividerHeight() * (adapter.getGroupCount() - 1));
            mExpandableListView.setLayoutParams(par);
            mExpandableListView.requestLayout();

            if (adapter.getGroupCount() > 0) {
                mExpandableListView.expandGroup(0);
            }
        }
    }

    public void setCancelable(boolean isCancelable) {
        mCancelable = isCancelable;
    }


    private void setButtons(View view) {

        View positiveBtn = view.findViewById(R.id.btn_positive);

        if (mPositiveBtn == null) {
            mPositiveBtn = buildBtnView(getActivity(), getString(R.string.OK), 0);
        }

        ((ViewGroup) positiveBtn).addView(mPositiveBtn);
        positiveBtn.setOnClickListener(this);

        TextView positiveBtnText = (TextView) positiveBtn.findViewById(R.id.btn_text);
        if (mPositiveTextColor != NO_RESOURCE) {
//            positiveBtnText.setTextColor(getResources().getColor(mPositiveTextColor));
            positiveBtnText.setTextColor(getResources().getColor(R.color.accent_color_1));
        } else if (mNegativeBtn != null || mMiddleBtn != null) {
            positiveBtnText.setTextColor(getResources().getColor(R.color.accent_color_1));
//            positiveBtnText.setTextColor(getResources().getColor(R.color.dialog_btn_text_green));
        }

        if (mNegativeBtn != null) {
            View negativeBtn = view.findViewById(R.id.btn_negative);
            ((ViewGroup) negativeBtn).addView(mNegativeBtn);
            negativeBtn.setOnClickListener(this);
            negativeBtn.setVisibility(View.VISIBLE);
            view.findViewById(R.id.btn_divider1).setVisibility(View.VISIBLE);
        }

        if (mMiddleBtn != null) {
            View middleBtn = view.findViewById(R.id.btn_middle);
            ((ViewGroup) middleBtn).addView(mMiddleBtn);
            middleBtn.setOnClickListener(this);
            middleBtn.setVisibility(View.VISIBLE);
            view.findViewById(R.id.btn_divider2).setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
        }
    }

    /**
     * workaround for a bug when the configuration changes (such as locale change) the message queue receives an empty
     * dismiss message from destroyView
     */
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public void setDimBehind(boolean dimBehind) {
        mDimBehind = dimBehind;
    }

    public void setIcon(Drawable icon) {
        mIcon = icon;
    }

    public void setTitle(Context context, int titleId) {
        mTitle = context.getString(titleId);
    }

    public void setTitle(CharSequence title) {
        mTitle = title;
    }

    public void setDesc(CharSequence desc, int gravity) {
        mDesc = desc;
        mDescGravity = gravity;
    }

    public void addImage(Context context, int resID, int tag, View.OnClickListener listener) {

        if (mCustomContent == null) {
            mCustomContent = new LinearLayout(context);
            mCustomContent.setOrientation(LinearLayout.VERTICAL);
        }

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.dialog_image_view, mCustomContent, false);
        view.setTag(tag);
        ((ImageView) view.findViewById(R.id.iv_dialog)).setImageResource(resID);
        mCustomContent.addView(view);


        if (mCustomContentListeners == null && listener != null) {
            mCustomContentListeners = new SparseArray<View.OnClickListener>();
        }

        if (listener != null) {
            mCustomContentListeners.append(view.getId(), listener);
            view.setOnClickListener(this);
        }
    }


    public void addText(Context context, int textRes, int colorRes, int tag, View.OnClickListener listener) {

        if (mCustomContent == null) {
            mCustomContent = new LinearLayout(context);
            mCustomContent.setOrientation(LinearLayout.VERTICAL);
        }

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.dialog_text_view, mCustomContent, false);
        view.setTag(tag);
        TextView tv = ((TextView) view.findViewById(R.id.tv_dialog));
        tv.setText(textRes);
        tv.setTextColor(context.getResources().getColor(colorRes));
        mCustomContent.addView(view);


        if (mCustomContentListeners == null && listener != null) {
            mCustomContentListeners = new SparseArray<View.OnClickListener>();
        }

        if (listener != null) {
            mCustomContentListeners.append(view.getId(), listener);
            view.setOnClickListener(this);
            view.setMinimumHeight((int) context.getResources().getDimension(R.dimen.min_touch_size));
        }
    }

    public void addExpandableList(Context context, BaseExpandableListAdapter listAdapter) {

        if (mCustomContent == null) {
            mCustomContent = new LinearLayout(context);
            mCustomContent.setOrientation(LinearLayout.VERTICAL);
        }
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.dialog_expandable_list, mCustomContent, false);
        mCustomContent.addView(view);

        mExpandableListView = (ExpandableListView) view.findViewById(R.id.elv_detection_list);
        mExpandableListView.setAdapter(listAdapter);
        mExpandableListView.setOnGroupExpandListener(this);
        adjustListViewHeightBasedOnChildren(listAdapter);
    }

    public void addCustomView(Context context, View view) {
        if (mCustomContent == null) {
            mCustomContent = new LinearLayout(context);
            mCustomContent.setOrientation(LinearLayout.VERTICAL);
        }

        mCustomContent.addView(view);
    }

    public void addCheckbox(Context context, int textRes, int colorRes, int tag, View.OnClickListener listener) {

        if (mCustomContent == null) {
            mCustomContent = new LinearLayout(context);
            mCustomContent.setOrientation(LinearLayout.VERTICAL);
        }

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.dialog_checkbox, mCustomContent, false);
        view.setTag(tag);
        CheckBox checkBox = ((CheckBox) view.findViewById(R.id.cb_dialog));
        checkBox.setText(textRes);
        checkBox.setTextColor(context.getResources().getColor(colorRes));

        checkBox.setPadding(checkBox.getPaddingLeft() + 20,
                checkBox.getPaddingTop(),
                checkBox.getPaddingRight(),
                checkBox.getPaddingBottom());

        mCustomContent.addView(view);


        if (mCustomContentListeners == null && listener != null) {
            mCustomContentListeners = new SparseArray<View.OnClickListener>();
        }

        if (listener != null) {
            mCustomContentListeners.append(view.getId(), listener);
            view.setOnClickListener(this);
        }
    }

    public void setPositiveButton(Context context, CharSequence text, int drawableRes,
                                  int textColorRes, View.OnClickListener listener) {

        if (listener != null) {
            mPositiveBtnClickListener = listener;
        }
        //build the layout
        mPositiveBtn = buildBtnView(context, text, drawableRes);

        mPositiveTextColor = textColorRes;
    }


    public void setPositiveBtnText(int stringRes) {
        ((TextView) mPositiveBtn.findViewById(R.id.btn_text)).setText(stringRes);
    }


    public void setNegativeButton(Context context, CharSequence text, int drawableRes, View.OnClickListener listener) {
        if (listener != null) {
            mNegativeBtnClickListener = listener;
        }
        //build the layout
        mNegativeBtn = buildBtnView(context, text, drawableRes);
    }


    public void setMiddleButton(Context context, CharSequence text, int drawableRes, View.OnClickListener listener) {
        if (listener != null) {
            mMiddleBtnClickListener = listener;
        }
        //build the layout
        mMiddleBtn = buildBtnView(context, text, drawableRes);
    }

    private View buildBtnView(Context context, CharSequence text, int drawableRes) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.dialog_btn, null, false);

        if (!TextUtils.isEmpty(text)) {
            TextView tv = (TextView) view.findViewById(R.id.btn_text);
            tv.setText(text);

            if (drawableRes != 0) {
                tv.setCompoundDrawablesWithIntrinsicBounds(drawableRes, 0, 0, 0);
            }

        } else if (drawableRes != 0) {
            ImageView iv = (ImageView) view.findViewById(R.id.btn_icon);
            iv.setImageResource(drawableRes);
            iv.setVisibility(View.VISIBLE);
            view.findViewById(R.id.btn_text).setVisibility(View.GONE);
        }

        return view;

    }

    public void displayContentDivider(boolean display) {
        mDisplayContentDivider = display;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        mOnDismissListener = listener;
    }

    @Override
    public void onClick(View v) {
        boolean dismissDialog = true;

        final int id = v.getId();
        if (id == R.id.btn_positive) {
            if (mPositiveBtnClickListener != null) {
                mPositiveBtnClickListener.onClick(v);
            }
        } else if (id == R.id.btn_negative) {

            if (mNegativeBtnClickListener != null) {
                mNegativeBtnClickListener.onClick(v);
            }
        } else if (id == R.id.btn_middle) {
            if (mMiddleBtnClickListener != null) {
                mMiddleBtnClickListener.onClick(v);
            }
        } else if (id == R.id.cb_dialog) {
            //send event to listener
            if (mCustomContentListeners != null) {
                View.OnClickListener listener = mCustomContentListeners.get(v.getId(), null);
                if (listener != null) {
                    listener.onClick(v);
                }
            }
            dismissDialog = false;
        }

        //pass on click event to listener
        if (mCustomContentListeners != null && mCustomContentListeners.indexOfKey(v.getId()) >= 0) {
            View.OnClickListener listener = mCustomContentListeners.get(v.getId(), null);
            if (listener != null) {
                listener.onClick(v);
            }
            dismissDialog = false;
        }

        if (dismissDialog) {
            dismiss();
        }
    }

    public void onGroupClick(int groupPosition) {
        if (mExpandableListView.isGroupExpanded(groupPosition)) {
            mExpandableListView.collapseGroup(groupPosition);
        } else {
            mExpandableListView.expandGroup(groupPosition);
            onGroupExpand(groupPosition);
        }
    }

    @Override
    public void onGroupExpand(int groupPosition) {
        if (mExpandableListView != null) {
            for (int i = 0; i < mExpandableListView.getExpandableListAdapter().getGroupCount(); i++) {
                if (i != groupPosition) {
                    mExpandableListView.collapseGroup(i);
                }
            }
        }
    }

    //================================================================================//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BUILDER  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //================================================================================//

    @SuppressWarnings("unused")
    public static class Builder {
        private final AviraDialog mDialog = new AviraDialog();
        private Context mContext;

        public Builder(FragmentActivity context) {
            mContext = context;
        }

        public Builder setCancelable(boolean isCancelable) {
            mDialog.setCancelable(isCancelable);
            return this;
        }

        public Builder setDimBehind(boolean dimBehind) {
            mDialog.setDimBehind(dimBehind);
            return this;
        }

        public Builder setTitle(int resId) {
            return setTitle(mContext.getString(resId));
        }

        public Builder setTitle(CharSequence title) {
            mDialog.setTitle(title);
            return this;
        }

        public Builder setIcon(int resId) {
            return setIcon(mContext.getResources().getDrawable(resId));
        }

        public Builder setIcon(Drawable icon) {
            mDialog.setIcon(icon);
            return this;
        }

        public Builder setDesc(int resId) {
            return setDesc(mContext.getString(resId));
        }

        public Builder setDesc(int resId, int gravity) {
            return setDesc(mContext.getString(resId), gravity);
        }

        public Builder setDesc(CharSequence desc) {
            return setDesc(desc, DEFAULT_DESC_GRAVITY);
        }

        public Builder setDesc(CharSequence desc, int gravity) {
            mDialog.setDesc(desc, gravity);
            return this;
        }

        public Builder addImage(int resID) {
            return addImage(resID, 0, null);
        }

        public Builder addImage(int resID, int tag, View.OnClickListener listener) {
            mDialog.addImage(mContext, resID, tag, listener);
            return this;
        }


        public Builder addText(int textRes) {
            return addText(textRes, R.color.dialog_desc_text);
        }

        public Builder addText(int textRes, int colorRes) {
            return addText(textRes, colorRes, 0, null);
        }

        public Builder addText(int textRes, int colorRes, int tag, View.OnClickListener listener) {
            mDialog.addText(mContext, textRes, colorRes, tag, listener);
            return this;
        }


        public Builder addCheckbox(int textRes) {
            return addCheckbox(textRes, R.color.dialog_desc_text, 0, null);
        }

        public Builder addCheckbox(int textRes, int colorRes) {
            return addCheckbox(textRes, colorRes, 0, null);
        }

        public Builder addCheckbox(int textRes, View.OnClickListener listener) {
            return addCheckbox(textRes, R.color.dialog_desc_text, 0, listener);
        }

        public Builder addCheckbox(int textRes, int colorRes, int tag, View.OnClickListener listener) {
            mDialog.addCheckbox(mContext, textRes, colorRes, tag, listener);
            return this;
        }


        //====== positive btn ======//
        public Builder setPositiveButton() {
            return setPositiveButton(R.string.OK);
        }

        public Builder setPositiveButton(@StringRes int labelResId) {
            return setPositiveButton(labelResId, null);
        }

        public Builder setPositiveButton(@StringRes int labelResId, View.OnClickListener listener) {
            return setPositiveButton(mContext.getString(labelResId), NO_RESOURCE, NO_RESOURCE, listener);
        }

        public Builder setPositiveButton(CharSequence text, View.OnClickListener listener, @ColorRes int textColor) {
            return setPositiveButton(text, NO_RESOURCE, textColor, listener);
        }

        public Builder setPositiveButton(@StringRes int labelResId, @DrawableRes int leftDrawable, View.OnClickListener listener) {
            return setPositiveButton(mContext.getString(labelResId), leftDrawable, NO_RESOURCE, listener);
        }

        public Builder setPositiveButton(CharSequence text, @DrawableRes int leftDrawable, View.OnClickListener listener) {
            return setPositiveButton(text, leftDrawable, NO_RESOURCE, listener);
        }

        public Builder setPositiveButton(CharSequence text, @DrawableRes int drawable, @ColorRes int textColor, View.OnClickListener listener) {
            mDialog.setPositiveButton(mContext, text, drawable, textColor, listener);
            return this;
        }


        //====== negative btn ======//
        public Builder setNegativeButton() {
            return setNegativeButton(R.string.Cancel);
        }

        public Builder setNegativeButton(@StringRes int labelResId) {
            return setNegativeButton(labelResId, null);
        }

        public Builder setNegativeButton(@StringRes int resId, View.OnClickListener listener) {
            return setNegativeButton(mContext.getString(resId), 0, listener);
        }

        public Builder setNegativeButton(@StringRes int resId, @DrawableRes int drawableId, View.OnClickListener listener) {
            return setNegativeButton(mContext.getString(resId), drawableId, listener);
        }

        public Builder setNegativeButton(CharSequence text, @DrawableRes int drawableId, View.OnClickListener listener) {
            mDialog.setNegativeButton(mContext, text, drawableId, listener);
            return this;
        }


        //===== middle btn ======//
        public Builder setMiddleButton(@StringRes int labelResId) {
            return setMiddleButton(labelResId, null);
        }

        public Builder setMiddleButton(@StringRes int resId, View.OnClickListener listener) {
            return setMiddleButton(mContext.getString(resId), 0, listener);
        }

        public Builder setMiddleButton(@StringRes int resId, @DrawableRes int leftDrawable, View.OnClickListener listener) {
            return setMiddleButton(mContext.getString(resId), leftDrawable, listener);
        }

        public Builder setMiddleButton(CharSequence text, @DrawableRes int leftDrawable, View.OnClickListener listener) {
            mDialog.setMiddleButton(mContext, text, leftDrawable, listener);
            return this;
        }


        public Builder addExpandableList(BaseExpandableListAdapter listAdapter) {
            mDialog.addExpandableList(mContext, listAdapter);
            return this;
        }

        public Builder addCustomView(View res) {
            mDialog.addCustomView(mContext, res);
            return this;
        }

        public Builder displayContentDivider(boolean display) {
            mDialog.displayContentDivider(display);
            return this;
        }

        public Builder setOnDismissListener(DialogInterface.OnDismissListener listener) {
            mDialog.setOnDismissListener(listener);
            return this;
        }

        public Builder setNoButtons(boolean value) {
            mDialog.mNoButtons = value;
            return this;
        }

        public AviraDialog create() {
            return mDialog;
        }

        public AviraDialog show(FragmentManager fragmentManager) {
            mDialog.show(fragmentManager);
            return mDialog;
        }
    }
}
