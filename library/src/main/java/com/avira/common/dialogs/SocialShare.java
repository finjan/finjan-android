/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.dialogs;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Utility class to help in sharing a message on some of the more popular social networks
 * <p/>
 * <p/>Please note that sharing a message using the URL fallback is possible only on TWITTER
 *
 * @author bogdan.oprea
 * @author ovidiu.buleandra
 */
@SuppressWarnings("unused")
public enum SocialShare {
    FACEBOOK("com.facebook.katana", "http://m.facebook.com/sharer.php?u="),
    TWITTER("com.twitter.android", "https://twitter.com/intent/tweet?text="),
    GOOGLE_PLUS("com.google.android.apps.plus", "http://plus.google.com/share?url=");

    private final String packageName;
    private final String webUrl;

    SocialShare(String pkg, String url) {
        packageName = pkg;
        webUrl = url;
    }

    public String packageName() {
        return packageName;
    }

    public String webUrl() {
        return webUrl;
    }

    /**
     * constructs an intent that opens the share medium application (if it's available on the device) or a web browser
     * to a specific URL with the purpose of sharing the given message
     *
     * @param context   valid application context (used for {@link PackageManager} and starting an activity)
     * @param shareText the message to post on the social network
     */
    public void share(Context context, String shareText) {
        final PackageManager packageManager = context.getPackageManager();

        ApplicationInfo appInfo = null;
        try {
            appInfo = packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            // ignore
        }

        Intent shareIntent;
        if (appInfo != null && appInfo.enabled) {
            shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage(packageName);
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        } else {
            shareIntent = new Intent(android.content.Intent.ACTION_VIEW);
            try {
                shareIntent.setData(Uri.parse(webUrl + URLEncoder.encode(shareText, "UTF-8")));
            } catch (UnsupportedEncodingException e) {
                // ignore
            }
        }

        try {
            context.startActivity(shareIntent);
        } catch (ActivityNotFoundException e) {
            // ignore
        }
    }
}
