/*******************************************************************************
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 *******************************************************************************/

package com.avira.common.dialogs;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import androidx.fragment.app.FragmentActivity;
import android.view.View;

import com.avira.common.R;
//import com.avira.common.tracking.EventProperties;
//import com.avira.common.tracking.TrackingEvent;
//import com.avira.common.tracking.TrackingManager;
import com.avira.common.utils.PackageUtilities;
import com.avira.common.utils.SharedPreferencesUtilities;

import static com.avira.common.CommonLibrary.DEBUG;

/**
 * @author daniela.stamati
 */
public class RateMeDialogUtils {
    // app states
    public static final String EMAIL_SENT_TO_BREACHED_CONTACT_PREF = "sent_email_to_breached_contact";
    public static final String SCAN_SCHEDULER_ACTIVATED_PREF = "scanSchedulerActivated";
    public static final String USER_INTERACTIONS_PREF = "userInteractions";
    public static final String DEVICE_LOCATED_OR_YELL_TRIGGERED_PREF = "located_or_yell_triggered";
    public static final String DEVICE_UNLOCKED_PREF = "device_unlocked";
    public static final String LAST_DIALOG_DISPLAY_TIME_PREF = "last_dialog_display_time_pref";
    private static final int DAYS_BETWEEN_RATE_REQUESTS = 10;
    private static final int MIN_USER_INTERACTIONS_BEFORE_RATE_DIALOG_IS_SHOWN = 3;
    private static final String APP_RATED_PREF = "app_rated";
    private static final String EVENT_NAME = "RateMeDialog_Click";
    private static final String EVENT_PROP = "action";


    public static void showRateMeDialog(FragmentActivity context, int titleId, int descId){
        showRateMeDialog(context, titleId, descId, null, null, null);
    }

    public static void showRateMeDialog(FragmentActivity context, int titleId, String descText){
        showRateMeDialog(context, titleId, 0, descText, null, null);
    }

    public static void showRateMeDialog(FragmentActivity context, int titleId, final int descId, String descText, final String appName, final String trackingFeature){

//        final EventProperties attr = new EventProperties();

        AviraDialog.Builder builder = new AviraDialog.Builder(context)
                .setTitle(titleId)
                .setNegativeButton(R.string.not_now, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(appName != null && trackingFeature != null) {
//                            final TrackingEvent trackEvent = new TrackingEvent(appName, trackingFeature, EVENT_NAME);
//                            attr.putProperty(EVENT_PROP, "negative");
//                            TrackingManager.getInstance().trackFeature(trackEvent, attr);
                        }
                    }
                })
                .setPositiveButton(R.string.rateMeDialog_possitiveBtn, R.drawable.settings_icon, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(appName != null && trackingFeature != null) {
//                            final TrackingEvent trackEvent = new TrackingEvent(appName, trackingFeature, EVENT_NAME);
//                            attr.putProperty(EVENT_PROP, "positive");
//                            TrackingManager.getInstance().trackFeature(trackEvent, attr);
                        }
                        gotoRatingPage(v.getContext());
                    }
                })
                .addImage(R.drawable.stars)
                .setCancelable(false);
        if(descId == 0) {
            builder.setDesc(descText);
        } else {
            builder.setDesc(descId);
        }
        builder.show(context.getSupportFragmentManager());

        // save the time when the dialog was last displayed
        SharedPreferencesUtilities.putLong(context,
                LAST_DIALOG_DISPLAY_TIME_PREF, SystemClock.elapsedRealtime());
    }

    /**
     * A RateMeDialog should be displayed if: - the app wasn't already rated - the uses has experienced rewarding
     * functionality more than MIN_USER_INTERACTIONS_BEFORE_RATE_DIALOG_IS_SHOWN times - the last rate me dialog was
     * displayed more than DAYS_BETWEEN_RATE_REQUESTS days ago
     *
     * @return
     */
    public static synchronized boolean shouldDisplayRateMeDialog(Context context) {
        if (SharedPreferencesUtilities.getBoolean(context, APP_RATED_PREF, false)) {
            return false;
        }

        // If it's a debug build, don't put any restrictions on the number of interaction the user has to have until the
        // rate me dialog is shown.
        if (!DEBUG && getNoOfUserInteractions(context) < MIN_USER_INTERACTIONS_BEFORE_RATE_DIALOG_IS_SHOWN) {
            return false;
        }

        long lastDisplayedTime = SharedPreferencesUtilities.getLong(context,
                RateMeDialogUtils.LAST_DIALOG_DISPLAY_TIME_PREF, 0);

        long daysBetweenRateRequestsInMillis;

        // If it's a debug build, shorten the time one has to wait before the rate me dialog is shown again to 5
        // seconds.
        if (DEBUG) {
            daysBetweenRateRequestsInMillis = 5000;
        } else {
            // convert days to millis
            daysBetweenRateRequestsInMillis = DAYS_BETWEEN_RATE_REQUESTS * 24 * 60 * 60 * 1000;
        }

        if (lastDisplayedTime == 0
                || SystemClock.elapsedRealtime() - lastDisplayedTime >= daysBetweenRateRequestsInMillis) {
            return true;
        }

        return false;
    }

    /**
     * Check if app was already rated
     * @param context
     * @return
     */
    public static boolean isAppRated(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, APP_RATED_PREF, false);
    }

    public static void increaseNoOfUserInteractions(Context context) {
        int currentUserInteractions = getNoOfUserInteractions(context);
        SharedPreferencesUtilities.putInt(context, USER_INTERACTIONS_PREF,
                currentUserInteractions + 1);
    }

    private static int getNoOfUserInteractions(Context context) {
        return SharedPreferencesUtilities.getInt(context, USER_INTERACTIONS_PREF, 0);
    }

    // ******** setters ********* //
    public static void setScanSchedulerActivated(Context context, boolean activated) {
        SharedPreferencesUtilities.putBoolean(context, SCAN_SCHEDULER_ACTIVATED_PREF,
                activated);
        increaseNoOfUserInteractions(context);
    }

    public static void setEmailSentToBreachedContact(Context context, boolean sent) {
        SharedPreferencesUtilities.putBoolean(context,
                RateMeDialogUtils.EMAIL_SENT_TO_BREACHED_CONTACT_PREF, sent);
        increaseNoOfUserInteractions(context);
    }

    public static void setLocatedOrYellTriggered(Context context, boolean activated) {
        SharedPreferencesUtilities.putBoolean(context, DEVICE_LOCATED_OR_YELL_TRIGGERED_PREF,
                activated);
        increaseNoOfUserInteractions(context);
    }

    public static void setDeviceUnlocked(Context context, boolean unlocked) {
        SharedPreferencesUtilities.putBoolean(context, RateMeDialogUtils.DEVICE_UNLOCKED_PREF,
                unlocked);

        increaseNoOfUserInteractions(context);
    }

    // ******** getters ******** //

    public static boolean wasScanSchedulerActivated(Context context) {
        return SharedPreferencesUtilities.getBoolean(context, SCAN_SCHEDULER_ACTIVATED_PREF,
                false);
    }

    public static boolean wasEmailSentToBreachedContact(Context context) {
        return SharedPreferencesUtilities.getBoolean(context,
                RateMeDialogUtils.EMAIL_SENT_TO_BREACHED_CONTACT_PREF, false);
    }

    public static boolean wasLocatedOrYellTriggered(Context context) {
        return SharedPreferencesUtilities.getBoolean(context,
                DEVICE_LOCATED_OR_YELL_TRIGGERED_PREF, false);
    }

    public static boolean wasDeviceUnlocked(Context context) {
        return SharedPreferencesUtilities.getBoolean(context,
                RateMeDialogUtils.DEVICE_UNLOCKED_PREF, false);
    }

    /*package */
    static void gotoRatingPage(Context context) {
        String storeLink = PackageUtilities.getStorePage(context);

        Uri uri = Uri.parse(storeLink + context.getPackageName());
        context.startActivity(new Intent(Intent.ACTION_VIEW, uri));

        SharedPreferencesUtilities.putBoolean(context, APP_RATED_PREF, true);
    }

    /*package*/
    static void saveDisplayTime(Context context) {
        SharedPreferencesUtilities.putLong(context, RateMeDialogUtils.LAST_DIALOG_DISPLAY_TIME_PREF,
                SystemClock.elapsedRealtime());
    }
}
