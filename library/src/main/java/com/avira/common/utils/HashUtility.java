/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.utils;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Hash generator utility class
 *
 * @author hui-joo.goh
 * @author ovidiu.buleandra added direct methods for common hash algorithms
 */
public class HashUtility {
    private static final String TAG = HashUtility.class.getSimpleName();
    private static final String SHA256 = "SHA-256";
    private static final String SHA1 = "SHA-1";
    private static final String MD5 = "MD5";
    private static final int KEY_LENGTH = 16;

    /**
     * Get SHA-256 hashing with salt of input string
     *
     * @param textToHash String to be hashed
     * @param salt       salt input
     * @return SHA-256 Hash with salt input string or null if SHA-256 is not supported in the android java version
     */
    public static String getSHA256HashWithSalt(String textToHash, String salt) {
        return getHashWithSalt(textToHash, salt, SHA256);
    }

    /**
     * Get hash with salt from text input based on selected hash algorithm
     *
     * @param textToHash    text input
     * @param salt          salt input
     * @param hashAlgorithm selected hash algorithm
     * @return hash with salt text
     */
    private static String getHashWithSalt(String textToHash, String salt, String hashAlgorithm) {
        String result = null;
        MessageDigest messageDigest;
        try {
            if(salt != null && salt != "") {
                textToHash += salt;
            }

            messageDigest = MessageDigest.getInstance(hashAlgorithm);
            messageDigest.reset();
            messageDigest.update(textToHash.getBytes());

            byte[] digest = messageDigest.digest();
            int digestLength = digest.length;
            StringBuilder stringBuilder = new StringBuilder(digestLength << 1);
            for (int i = 0; i < digestLength; i++) {
                stringBuilder.append(Character.forDigit((digest[i] & 0xf0) >> 4, 16));
                stringBuilder.append(Character.forDigit(digest[i] & 0x0f, 16));
            }
            result = stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "unknown algorithm " + hashAlgorithm, e);
        }

        return result;
    }

    private static String getHash(String text, String algorithm) {
        return getHashWithSalt(text, null, algorithm);
    }

    /**
     * generates a hash using the MD5 algorithm
     * @param text text to hash
     * @return MD5 hash
     */
    public static String md5(String text) { return getHash(text, MD5); }
    /**
     * generates a hash using the MD5 algorithm and a salt
     * @param text text to hash
     * @param salt string to use as salt
     * @return MD5 hash
     */
    public static String md5(String text, String salt) { return getHashWithSalt(text, salt, MD5); }
    /**
     * generates a hash using the SHA-1 algorithm
     * @param text text to hash
     * @return SHA-1 hash
     */
    public static String sha1(String text) { return getHash(text, SHA1); }
    /**
     * generates a hash using the SHA-1 algorithm and a salt
     * @param text text to hash
     * @param salt string to use as salt
     * @return SHA-1 hash
     */
    public static String sha1(String text, String salt) { return getHashWithSalt(text, salt, SHA1); }
    /**
     * generates a hash using the SHA-256 algorithm
     * @param text text to hash
     * @return SHA-256 hash
     */
    public static String sha256(String text) { return getHash(text, SHA256); }

    /**
     * generates a hash using the SHA-256 algorithm
     * @param text text to hash
     * @return SHA-256 hash
     */
    public static String sha512(String text) { return getHash(text, "SHA-512"); }
    /**
     * generates a hash using the SHA-256 algorithm and a salt
     * @param text text to hash
     * @param salt string to use as salt
     * @return SHA-256 hash
     */
    public static String sha256(String text, String salt) { return getHashWithSalt(text, salt, SHA256); }

    /**
     * Get random salt string
     *
     * @return a random salt string
     */
    public static String getRandomSalt() {
        return bytesToHex(SecureRandom.getSeed(KEY_LENGTH));
    }

    /**
     * Current implementation is performance optimized, almost identical with
     * org.apache.commons.codec.binary.Hex.encodeHex method
     *
     * @param data the byte data to convert
     * @return the converted bytes into hex
     */
    private static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        }

        final char hexArray[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        final int len = data.length;
        char[] str = new char[len * 2]; // faster than StringBuilder

        for (int i = 0, j = 0; i < len; i++) {
            str[j++] = hexArray[(data[i] & 0xF0) >>> 4]; // faster than java.lang.Integer.toHexString
            str[j++] = hexArray[data[i] & 0x0F];
        }

        return new String(str);
    }
}
