/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.os.Build;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;

/**
 * Class to store general utility methods that could be re-used
 *
 * @author hui-joo.goh
 */
public class GeneralUtility {

    /**
     * Method to check if a package is still installed
     *
     * @param packageName the package name
     * @param context
     * @return true if package is still installed, otherwise false
     */
    public static boolean isPackageInstalled(String packageName, Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static int getScreenWidth(Context context) {
        int screenWidth = 0;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        } else {
            screenWidth = display.getWidth();
        }

        return screenWidth;
    }

    /**
     * Returns either the passed in String, or if the String is null, an empty String ("").
     *
     * @param str The String to check, may be null
     * @return the passed in String, or the empty String if it was null
     */
    public static String defaultString(String str) {
        return TextUtils.isEmpty(str) ? "" : str;
    }

    /**
     * Returns either the passed in String, or if the String is null, the value of defaultStr.
     *
     * @param str        The String to check, may be null
     * @param defaultStr The default String to return if the input is null, may be null
     * @return The passed in String, or the default if it was null
     */
    public static String defaultString(String str, String defaultStr) {
        return TextUtils.isEmpty(str) ? defaultStr : str;
    }
}
