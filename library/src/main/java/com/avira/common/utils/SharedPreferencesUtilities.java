/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.preference.PreferenceManager;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Utilities which allows to handle operations on default @SharedPreferences
 *
 * @author hui-joo.goh
 */
public class SharedPreferencesUtilities {
    private static final int DEFAULT_INT = 0;
    private static final long DEFAULT_LONG = 0L;
    private static final float DEFAULT_FLOAT = 0f;
    private static final boolean DEFAULT_BOOLEAN = false;
    private static final String DEFAULT_STRING = "";

    private SharedPreferencesUtilities() {
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static Editor getEditor(Context context) {
        return getSharedPreferences(context).edit();
    }

    @SuppressLint("NewApi")
    public static void saveChanges(Editor editor) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // apply is doing the same as commit, but it is faster, and do not
            // return result of saving data (method available since API 9).
            editor.apply();
        } else {
            editor.commit();
        }
    }

    public static void clear(Context context) {
        saveChanges(getEditor(context).clear());
    }

    public static void remove(Context context, String key) {
        saveChanges(getEditor(context).remove(key));
    }

    public static boolean containsField(Context context, String fieldKey) {
        return getSharedPreferences(context).contains(fieldKey);
    }

    public static void putString(Context context, String key, String value) {
        saveChanges(getEditor(context).putString(key, value));
    }

    public static String getString(Context context, String key) {
        return getString(context, key, DEFAULT_STRING);
    }

    public static String getString(Context context, String key, String defValue) {
        return getSharedPreferences(context).getString(key, defValue);
    }

    public static void putBoolean(Context context, String key, boolean value) {
        saveChanges(getEditor(context).putBoolean(key, value));
    }

    public static boolean getBoolean(Context context, String key) {
        return getBoolean(context, key, DEFAULT_BOOLEAN);
    }

    public static boolean getBoolean(Context context, String key, boolean defValue) {
        return getSharedPreferences(context).getBoolean(key, defValue);
    }

    public static void putInt(Context context, String key, int value) {
        saveChanges(getEditor(context).putInt(key, value));
    }

    public static int getInt(Context context, String key) {
        return getInt(context, key, DEFAULT_INT);
    }

    public static int getInt(Context context, String key, int defValue) {
        return getSharedPreferences(context).getInt(key, defValue);
    }

    public static void putFloat(Context context, String key, float value) {
        saveChanges(getEditor(context).putFloat(key, value));
    }

    public static float getFloat(Context context, String key) {
        return getFloat(context, key, DEFAULT_FLOAT);
    }

    public static float getFloat(Context context, String key, float defValue) {
        return getSharedPreferences(context).getFloat(key, defValue);
    }

    public static void putLong(Context context, String key, long value) {
        saveChanges(getEditor(context).putLong(key, value));
    }

    public static long getLong(Context context, String key) {
        return getLong(context, key, DEFAULT_LONG);
    }

    public static long getLong(Context context, String key, long defValue) {
        return getSharedPreferences(context).getLong(key, defValue);
    }

    public static void putSerializable(Context context, String key, Serializable value) {
        String serializedValue = ObjectSerializerUtility.serializeObjectToString(value);
        saveChanges(getEditor(context).putString(key, serializedValue));
    }

    public static Serializable getSerializable(Context context, String key, Serializable defValue) {
        String serializedDefaultValue = ObjectSerializerUtility.serializeObjectToString(defValue);
        String serializedValue = getString(context, key, serializedDefaultValue);
        return (Serializable) ObjectSerializerUtility.deserializeStringToObject(serializedValue);
    }

    /**
     * Backward-compatible method to deserialize string back into an object if the serializable class has been renamed
     * or moved into a different package
     * <p/>
     * Note: This does not work for actual modification of the serializable class content
     *
     * @param oldSerializableClassNameList The list of serializable class name (e.g. com.avira.applock.library.data.LockedAppInfo) that was used to be converted into string previously
     * @param newSerializableClass         The renamed/moved serializable class
     * @return The serializable object
     */
    public static Serializable getSerializable(Context context, String key, Serializable defValue, List<String> oldSerializableClassNameList, Class<?> newSerializableClass) {
        String serializedDefaultValue = ObjectSerializerUtility.serializeObjectToString(defValue);
        String serializedValue = getString(context, key, serializedDefaultValue);
        return (Serializable) ObjectSerializerUtility.deserializeStringToObject(serializedValue, oldSerializableClassNameList, newSerializableClass);
    }

    public static void addStringSet(Context context, String key, Set<String> set) {
        saveChanges(getEditor(context).putStringSet(key, set));
    }

    public static Set<String> getStringSet(Context context, String key) {
        return getStringSet(context, key);
    }
}