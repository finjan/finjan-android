package com.avira.common.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import androidx.annotation.ArrayRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;

import com.avira.common.R;
import com.avira.common.dialogs.AviraDialog;

/**
 * @author ovidiu.buleandra
 * @since 16-Jan-17
 */

public class WhatsNewDialog {
    private static final String TAG = WhatsNewDialog.class.getSimpleName();

    private static final String PREF_SHOULD_SHOW = "pref_should_show";

    /**
     * puts a flag in preferences to mark that the Whats New dialog should be shown next time the app is started
     * @param context
     */
    public static void appWasUpdated(Context context) {
        SharedPreferencesUtilities.putBoolean(context, PREF_SHOULD_SHOW, true);
    }

    /**
     * Show a dialog which explains the changes in the latest app version.
     *
     * @param activity
     * @param titleRes
     * @param actionRes
     * @param changesArrayRes
     * @return True if the dialog is shown successfully, otherwise false if an error occurred
     */
    public static void showWhatsNewDialog(final FragmentActivity activity, @DrawableRes final int iconRes,
                                       @StringRes final int titleRes, @StringRes final int actionRes,
                                       @ArrayRes final int changesArrayRes) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean shouldShow = SharedPreferencesUtilities.containsField(activity, PREF_SHOULD_SHOW);

                if(!shouldShow) {
                    return;
                }

                String version;
                try {
                    PackageInfo thisPackageInfo = activity.getPackageManager()
                            .getPackageInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
                    version = "\nv" + thisPackageInfo.versionName + "(" + thisPackageInfo.versionCode + ")";
                } catch (PackageManager.NameNotFoundException e) {
                    version = "";
                }

                try {
                    String[] changes = activity.getResources().getStringArray(changesArrayRes);

                    if(changes.length == 0) {
                        return;
                    }

                    final String title = activity.getString(titleRes) + version;
                    final String action = activity.getString(actionRes);
                    final String content = "- " + TextUtils.join("\n -", changes);

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // construct dialog
                            AviraDialog.Builder builder = new AviraDialog.Builder(activity)
                                    .setIcon(iconRes)
                                    .setTitle(title)
                                    .setDesc(content, Gravity.NO_GRAVITY)
                                    .setPositiveButton(action, null, R.color.dialog_btn_text_green);

                            // show dialog
                            builder.show(activity.getSupportFragmentManager());
                        }
                    });

                } catch (Resources.NotFoundException ex) {
                    Log.e(TAG, "[whats new] string resources missing", ex);
                }

                // after the dialog was shown or we had resources error clear the flag
                SharedPreferencesUtilities.remove(activity, PREF_SHOULD_SHOW);
                return;
            }
        }).start();

    }
}
