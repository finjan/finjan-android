/*******************************************************************************
 * Copyright (C) 1986-2013 Avira GmbH. All rights reserved.
 *******************************************************************************/

package com.avira.common.utils;

/**
 * Class that handles Threading functionalities
 */
public class ThreadUtilities {
    /**
     * Execute runnable on a new Thread
     *
     * @param runnable
     */
    public static void runOnThread(Runnable runnable) {
        Thread task = new Thread(runnable);
        task.setPriority((Thread.MAX_PRIORITY + Thread.NORM_PRIORITY) / 2);
        task.start();
    }
}
