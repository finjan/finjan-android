/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ProviderInfo;
import android.util.Log;

import java.util.List;

/**
 * various methods to extract information from a package
 *
 * @author bogdan.oprea
 * @author ovidiu.buleandra
 */
public class PackageUtilities {

    public final static String GOOGLE_STORE = "com.android.vending";
    public static final String GOOGLE_STORE_LINK = "market://details?id=";
    public final static String AMAZON_STORE = "com.amazon.venezia";
    public static final String AMAZON_STORE_LINK = "amzn://apps/android?p=";
    public static final String GOOGLE_STORE_WEB_LINK = "http://play.google.com/store/apps/details?id=";
    public static final String GOOGLE_STORE_TINY_LINK = "http://www.avira.com/atd";
    public static final String AMAZON_STORE_TINY_LINK = "http://www.avira.com/atda";
    private final static String TAG = PackageUtilities.class.getSimpleName();

    /**
     * queries the package manager to see which store installed the application
     *
     * @param context
     * @return the store package name or "null" if unavailable(bug) or in debug
     */
    public static String getStoreName(Context context) {
        return context.getPackageManager().getInstallerPackageName(context.getPackageName());
    }

    public static String getStoreLink(Context context) {
        String storeName = getStoreName(context);
        if (GOOGLE_STORE.equals(storeName)) {
            return GOOGLE_STORE_LINK;
        } else if (AMAZON_STORE.equals(storeName)) {
            return AMAZON_STORE_LINK;
        } else {
            return GOOGLE_STORE_WEB_LINK;
        }
    }

    /**
     * Moved form rateMeDialog to PackageUtilities
     *
     * @param context
     * @return
     * @author daniela.stamati I bogdan_oprea only moved this here
     */
    public static String getStorePage(Context context) {
        String storeName = PackageUtilities.getStoreName(context);

        // if the installer package is no longer installed, go to google store web link
        if (PackageUtilities.isPackageInstalled(context, storeName)) {
            if (PackageUtilities.GOOGLE_STORE.equals(storeName)) {
                return PackageUtilities.GOOGLE_STORE_LINK;
            } else if (PackageUtilities.AMAZON_STORE.equals(storeName)) {
                return PackageUtilities.AMAZON_STORE_LINK;
            } else {
                return PackageUtilities.GOOGLE_STORE_WEB_LINK;
            }
        } else {
            // Google play will automatically get the intent if it's installed. In the highly unlikely possibility that
            // the google play app is not installed, the user will be redirected to browser.
            return PackageUtilities.GOOGLE_STORE_WEB_LINK;
        }
    }

    public static String getStorePageTinyUrl(Context context) {
        String storeName = PackageUtilities.getStoreName(context);

        if (PackageUtilities.GOOGLE_STORE.equals(storeName)) {
            return PackageUtilities.GOOGLE_STORE_TINY_LINK;
        } else if (PackageUtilities.AMAZON_STORE.equals(storeName)) {
            return PackageUtilities.AMAZON_STORE_TINY_LINK;
        } else {
            return PackageUtilities.GOOGLE_STORE_TINY_LINK;
        }

    }

    /**
     * checks if a package is installed in the current system
     * @param context valid context
     * @param targetPackage target package name to verify
     * @return if it is installed or not
     */
    public static boolean isPackageInstalled(Context context, String targetPackage) {
        try {
            context.getPackageManager().getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (NameNotFoundException e) {
            return false;
        }
        return true;
    }

    /**
     * check if a given package has a content provider described by the given {@code providerAuthority}
     * @param context valid context
     * @param targetPackage target package name
     * @param providerAuthority provider authority to search for
     * @return if the provider is available or not
     */
    public static boolean hasProvider(Context context, String targetPackage, String providerAuthority) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(targetPackage, PackageManager.GET_PROVIDERS);
            for(ProviderInfo providerInfo : packageInfo.providers) {
                if(providerAuthority.equals(providerInfo.authority)) {
                    return true;
                }
            }
        } catch (NameNotFoundException e) {
            // ignore
        }
        return false;
    }

    public static boolean checkServiceIsRunning(Context context, String targetService) {
        ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceInfoList = activityManager.getRunningServices(Integer.MAX_VALUE);
        for(ActivityManager.RunningServiceInfo serviceInfo: serviceInfoList) {
            if(serviceInfo.started && serviceInfo.service.getClassName().endsWith(targetService))
                return true;
        }
        return false;
    }

    /**
     * retrieves the version code from the package info
     * @param context valid context
     * @return version code
     */
    public static int getVersionCode(Context context) {
        int versionCode = -1;
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            Log.e(TAG, "couldn't get version code from own package");
        }
        return versionCode;
    }

    /**
     * method to change the visibility of the icon in launcher.
     * <br />
     * for this you have to target the activity that has the launcher intent filter
     *
     * @param context activity or application context
     * @param fullClassName the target's full class path
     * @param visibilityState true to make it visible, false to hide it
     */
    public static void toggleAppIconVisibility(Context context, String fullClassName, boolean visibilityState) {
        ComponentName componentName = new ComponentName(context, fullClassName);
        context.getPackageManager().setComponentEnabledSetting(componentName, visibilityState
                        ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                        : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
        Log.i(TAG, String.format("toggle app icon visibility for [%s] to %s",
                componentName.getClassName(), visibilityState));
    }

    /**
     * method to change the visibility of the icon in launcher.
     * <br />
     * for this you have to target the activity that has the launcher intent filter
     *
     * @param context activity or application context
     * @param launcherActivityClass the target's class
     * @param visibilityState true to make it visible, false to hide it
     */
    public static void toggleAppIconVisibility(Context context, Class<?> launcherActivityClass, boolean visibilityState) {
        toggleAppIconVisibility(context, launcherActivityClass.getName(), visibilityState);
    }
}
