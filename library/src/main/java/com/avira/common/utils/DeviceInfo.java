/**
 * ****************************************************************************
 * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
 *
 * *****************************************************************************
 */
package com.avira.common.utils;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.avira.common.R;
import com.avira.common.backend.DeviceCommandResult;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Class providing access to device information by wrapping Android SDK API calls
 */
public class DeviceInfo {
    public static final String PLATFORM = "android";
    private static final String MANUFACTURER_SERIAL_NUMBER_FIELD_NAME = "ril.serialnumber";
    private static final String SERIAL_NUMBER_FIELD_NAME = "ro.serialno";
    // Fix for some Samsung devices (confirmed at Samsung Galaxy S2) which contain Manufacturer Serial Number, but it
    // includes only zeros instead of correct serial number or null
    private static final String INVALID_MANUFACTURER_SERIAL_NUMBER = "00000000000";

    private String mSsid;
    private String mLocale;

    public DeviceInfo(Context context) {
        mSsid = getSsid(context);
        mLocale = context.getResources().getConfiguration().locale.toString();
    }

    /**
     * Get the actual IMEI of the device if any else will be null
     *
     * @return IMEI or null
     */
    public static String getIMEI(Context context) {
        if (context.checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(
                    Context.TELEPHONY_SERVICE);

            // IMEI for GSM and the MEID or ESN for CDMA phones
            return telephonyManager.getDeviceId();
        }
        return null;
    }

    /**
     * Get Device ID type
     *
     * @return String with device ID type, else UidTypeParameter.UNKNOWN
     */
    public static boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
    }

    /**
     * Get device serial number
     *
     * @return Serial number of device, or null if not exist
     */
    @SuppressLint("NewApi")
    public static String getSerialNumber() {
        String serialNumber = getSystemProp(MANUFACTURER_SERIAL_NUMBER_FIELD_NAME);
        if (!isValidManufacturerSerialNumber(serialNumber)) {
            if (checkIfHasValidBuildSerial()) {
                serialNumber = Build.SERIAL;
            } else {
                serialNumber = getSystemProp(SERIAL_NUMBER_FIELD_NAME);
            }
        }
        return serialNumber;
    }

    private static boolean isValidManufacturerSerialNumber(String serialNumber) {
        return serialNumber != null
                && !INVALID_MANUFACTURER_SERIAL_NUMBER.equals(serialNumber.trim());
    }

    @SuppressLint("NewApi")
    private static boolean checkIfHasValidBuildSerial() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
            if (!Build.UNKNOWN.equalsIgnoreCase(Build.SERIAL) && Build.SERIAL != null) {
                return true;
            }
        }
        return false;
    }

    private static String getSystemProp(String parameterName) {
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class);
            String serialNumber = (String) (get.invoke(c, parameterName, Build.UNKNOWN));
            if (!Build.UNKNOWN.equalsIgnoreCase(serialNumber)) {
                return serialNumber;
            }
        } catch (Exception e) {
        }
        return null;
    }


    /**
     * Get Device manufacturer
     *
     * @return String with model manufacturer, else null
     */
    public static String getDeviceManufacturer() {
        String productManufacturer = Build.MANUFACTURER;
        if (productManufacturer == Build.UNKNOWN) {
            productManufacturer = null;
        }

        return productManufacturer;
    }

    /**
     * Get Device model name
     *
     * @return String with model name, else null
     */
    public static String getDeviceModel() {
        /**
         * On some devices, the model number might include the manufacturer's name as well.
         */
        String productModel = Build.MODEL;
        if (productModel == Build.UNKNOWN) {
            productModel = null;
        }

        return productModel;
    }


    /**
     * Get Device phone number
     *
     * @return String with Phone Number, else null.
     */
    public static String getDevicePhoneNumber(Context context) {
        if(context.checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                || (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
                    && context.checkCallingOrSelfPermission(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED)) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            return telephonyManager.getLine1Number();
        }
        return "";
    }

    /**
     * Get Device SIM Operator Network Code
     *
     * @return the code from the SIM card
     */
    public static String getDeviceSimOperatorNetworkCode(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(
                Context.TELEPHONY_SERVICE);
        String simOperator = telephonyManager.getSimOperator();
        String simOperatorNetworkCode = "";

        if (simOperator != null && simOperator.length() >= 3) {
            simOperatorNetworkCode = simOperator.substring(3);
        }

        return simOperatorNetworkCode;
    }

    /**
     * Get Device SIM Operator Country Code
     *
     * @return the code from the SIM card
     */
    public static String getDeviceSimOperatorCountryCode(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(
                Context.TELEPHONY_SERVICE);
        String simOperator = telephonyManager.getSimOperator();
        String simOperatorCountryCode = "";

        if (simOperator != null && simOperator.length() >= 3) {
            simOperatorCountryCode = simOperator.substring(0, 3);
        }

        return simOperatorCountryCode;
    }

    /**
     * Get all Google accounts on Device
     *
     * @return List of Google account names
     */
    public static List<String> getDeviceGoogleAccountNames(Context context) {
        if (context.checkCallingOrSelfPermission(Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
            AccountManager accountManager = AccountManager.get(context);
            Account[] googleAccounts = accountManager.getAccountsByType(Const.GOOGLE_ACCOUNT_TYPE);
            List<String> googleEmails = new ArrayList<String>();

            for (Account googleAccount : googleAccounts) {
                googleEmails.add(googleAccount.name);
            }

            return googleEmails;
        }
        return null;
    }

    /**
     * Checks if Device Administrator is enabled on Device
     *
     * @return boolean value true if Device Administrator is enabled, else return false
     */
    public static boolean isDeviceAdmin(Context context) {
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        List<ComponentName> adminList = devicePolicyManager.getActiveAdmins();
        if (adminList != null && !adminList.isEmpty()) {
            String currentPackageName = context.getPackageName();
            for (ComponentName componentName : adminList) {
                if (componentName.getPackageName().equals(currentPackageName)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get application build number
     *
     * @return Application build number
     */
    public static int getVersionCode(Context context) {
        int buildNumber = -1;
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(),
                    0);
            buildNumber = packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            Log.e("DeviceInfo", "NameNotFoundException", e);
        }

        return buildNumber;
    }

    // TODO move it to a utils class in main project

    /**
     * Get application version name
     *
     * @return Application version name
     */
    public static String getVersionName(Context context) {
        PackageInfo packageInfo = getPackageInfo(context);
        if (packageInfo != null) {
            return packageInfo.versionName;
        }
        return "";
    }

    private static PackageInfo getPackageInfo(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            return packageManager.getPackageInfo(context.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            Log.e("DeviceInfo", "getPackageInfo() NameNotFoundException", e);
            return null;
        }
    }

    /**
     * Get application version string
     *
     * @return Application version string in {major}.{minor}.{build} format
     */
    public static String getApplicationVersion(Context context) {
        return getVersionName(context) + "." + getVersionCode(context);
    }

    /**
     * Get OS version
     *
     * @return a String representing the OS version
     */
    public static String getOperatingSystemVersion() {
        return android.os.Build.VERSION.RELEASE;
        // testing purposes
        // return "4." + String.valueOf((int) (Math.random() * 10));
    }

    /**
     * Get system language code
     *
     * @return a String representing the current language code in use
     */
    public static String getLanguageCode(Context context) {
        return context.getString(R.string.LanguageCode);
    }

    public static String getDeviceAdminState(Context context) {
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName deviceAdmin = new ComponentName(context, DeviceAdminReceiver.class);
        if (devicePolicyManager.isAdminActive(deviceAdmin)) {
            return DeviceCommandResult.ON;
        }

        return DeviceCommandResult.OFF;
    }

    /**
     * Get Ssid for connection. Uses network operator name in the case of connected to Mobile Network
     *
     * @param context
     * @return Ssid
     */
    private static String getSsid(Context context) {
        if(context.checkCallingOrSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return "";
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        String ssid = null;

        if (networkInfo == null) {
            return null; // Return null for No Network connectivity
        }

        if (networkInfo.isConnected()) {
            int connectionType = networkInfo.getType();
            switch (connectionType) {
                case ConnectivityManager.TYPE_WIFI:
                    if (context.checkCallingOrSelfPermission(Manifest.permission.ACCESS_WIFI_STATE)
                            != PackageManager.PERMISSION_GRANTED) {
                        return "";
                    }
                    WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo info = wifiManager.getConnectionInfo();
                    ssid = info.getSSID();
                    // remove enclosing quotes
                    // http://developer.android.com/reference/android/net/wifi/WifiInfo.html#getSSID()
                    ssid = ssid.replaceAll("^\"|\"$", "");
                    break;
                case ConnectivityManager.TYPE_MOBILE:
                    TelephonyManager telephonyManager = (TelephonyManager) context
                            .getSystemService(Context.TELEPHONY_SERVICE);
                    ssid = telephonyManager.getNetworkOperatorName();
                    break;
                default:
                    ssid = networkInfo.getTypeName();
            }
        }
        return ssid;
    }

    public String getSsid() {
        return mSsid;
    }

    public String getLocale() {
        return mLocale;
    }

    /**
     * Class providing constant values for com.avira.common.utils.DeviceInfo class
     */
    public class Const {
        public static final String DEVICE_INFO_UNKNOWN = "unknown";
        private static final int BATTERY_STATUS_UNKNOWN = -1;
        private static final String GOOGLE_ACCOUNT_TYPE = "com.google";
    }
}
