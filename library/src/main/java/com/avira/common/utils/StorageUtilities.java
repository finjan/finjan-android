/*******************************************************************************
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 *******************************************************************************/

package com.avira.common.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import java.io.File;
import java.util.Locale;

public class StorageUtilities
{
	private static final String TAG = StorageUtilities.class.getSimpleName();

	public static String getAvailableSpace()
	{
		// return by default no space if there is an error when retrieving it
		long freeSpace = 0;
		String externalStorageState = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(externalStorageState)
		    || Environment.MEDIA_MOUNTED_READ_ONLY.equals(externalStorageState))
		{
			freeSpace = bytesAvailable(Environment.getExternalStorageDirectory());
		}

		return sizeToString(freeSpace);
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	private static long bytesAvailable(File root)
	{
		StatFs statFS = new StatFs(root.getPath());
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2)
		{
			return (long) statFS.getBlockSize() * (long) statFS.getAvailableBlocks();
		}
		else
		{
			return statFS.getBlockSizeLong() * statFS.getAvailableBlocksLong();
		}
	}

	private static String sizeToString(long size)
	{
		float finalSize = size;
		String[] suffixes = new String[] { "B", "KB", "MB", "GB" };

		int currentSuffix = 0;
		while (currentSuffix < suffixes.length - 1 && finalSize > 1024f)
		{
			finalSize /= 1024f;
			currentSuffix++;
		}

		return String.format(Locale.US, "%.2f%s", finalSize, suffixes[currentSuffix]);
	}

	public static void deleteSanboxFolderContent(Context context){
		Log.d(TAG,"deleteingSandboxFolderContent");
		File cache = context.getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists()) {


			String[] children = appDir.list();
			for (String s : children) {
				if (!s.equals("lib")) {//the jni files will be skipped
					deleteDir(new File(appDir, s));
				}
			}
		}
	}

	public static boolean deleteDir(File dir) {
		if(dir==null){
			Log.d(TAG,"No file to delete");
			return false;
		}

		Log.d(TAG,"Deleting file "+dir.getPath()+"/"+dir.getName());
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

}
