/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class to store views related utility methods that could be re-used
 *
 * @author hui-joo.goh
 */
public class ViewUtility {
    //attach a view to scroll to in an activity
    public static String GO_TO_VIEW_INTENT_EXTRA = "target_view_intent_action";

    /**
     * Apply the specified style to only the specified substring contained within the full string.
     *
     * @param style                 Values are constants defined in Typeface. Examples include bold, italic, and normal.
     * @param fullString            The full string containing the substring
     * @param subStringToApplyStyle The substring that will be applied the specified style
     * @return The full string containing the substring that has been applied the specified style
     */
    public static CharSequence applyStyleToSubstring(int style, String fullString, String subStringToApplyStyle) {
        return applySpanToSubstring(new StyleSpan(style), fullString, subStringToApplyStyle);
    }

    /**
     * Apply the specified custom color to only the specified substring contained within the full string.
     *
     * @param resources
     * @param colorResId            Custom color resource ID to be applied to the substring
     * @param fullString            The full string containing the substring
     * @param subStringToApplyColor The substring that will be applied the specified custom color
     * @return The substring that will be applied the specified custom color
     */
    public static CharSequence applyCustomColorToSubstring(Resources resources, int colorResId, String fullString, String subStringToApplyColor) {
        String colorString = resources.getString(colorResId);
        int colorInt = Color.parseColor(colorString);
        return applyColorToSubstring(colorInt, fullString, subStringToApplyColor);
    }

    /**
     * Apply the specified color to only the specified substring contained within the full string.
     *
     * @param colorInt              The colors defined in {@link Color} to be applied to the substring
     * @param fullString            The full string containing the substring
     * @param subStringToApplyColor The substring that will be applied the specified color
     * @return The substring that will be applied the specified color
     */
    public static CharSequence applyColorToSubstring(int colorInt, String fullString, String subStringToApplyColor) {
        return applySpanToSubstring(new ForegroundColorSpan(colorInt), fullString, subStringToApplyColor);
    }

    /**
     * Apply the specified span to only the specified substring contained within the full string.
     *
     * @param span                 The object to apply on the specified range of text
     * @param fullString           The full string containing the substring
     * @param subStringToApplySpan The substring that will be applied the specified span
     * @return The substring that will be applied the specified span
     */
    public static CharSequence applySpanToSubstring(Object span, String fullString, String subStringToApplySpan) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(fullString);

        if (subStringToApplySpan.length() != 0 && fullString.contains(subStringToApplySpan)) {
            int startIndexOfSubString = fullString.indexOf(subStringToApplySpan);
            int endIndexOfSubString = startIndexOfSubString + subStringToApplySpan.length();
            spannableStringBuilder.setSpan(span, startIndexOfSubString, endIndexOfSubString,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }

        return spannableStringBuilder;
    }

    /**
     * Sets the enabled status for all nested view inside the specified layout
     *
     * @param layout  the layout containing nested views
     * @param enabled true if all nested views should be enabled. Otherwise false
     */
    public static void setEnabledForAllNestedView(ViewGroup layout, boolean enabled) {
        // set enabled for the specified parent view
        layout.setEnabled(enabled);

        // set enabled for all nested view
        if (layout instanceof ViewGroup) {
            ViewGroup groupLayout = (ViewGroup) layout;
            for (int i = 0; i < groupLayout.getChildCount(); i++) {
                View child = groupLayout.getChildAt(i);
                if (child instanceof ViewGroup) {
                    setEnabledForAllNestedView((ViewGroup) child, enabled);
                } else {
                    child.setEnabled(enabled);
                }
            }
        }
    }

    /**
     * Show a short toast with the specified string
     *
     * @param context
     * @param message the message to display
     */
    public static void showShortToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Show a short toast with the specified string
     *
     * @param context
     * @param resId   the string resource id
     */
    public static void showShortToast(Context context, int resId) {
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
    }

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    /**
     * Generate a value suitable for use in {@link #setId(int)}.
     * This value will not collide with ID values generated at build time by aapt for R.id.
     *
     * @return a generated ID value
     */
    public static int generateViewId() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            // the exact code as its in View.generateViewId which is available only from API 17
            for (;;) {
                final int result = sNextGeneratedId.get();
                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                int newValue = result + 1;
                if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
                if (sNextGeneratedId.compareAndSet(result, newValue)) {
                    return result;
                }
            }
        } else {
            return View.generateViewId();
        }
    }
}
