/*******************************************************************************
 * Copyright (C) 1986-2013 Avira GmbH. All rights reserved.
 *******************************************************************************/

package com.avira.common.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Class to provide Network Connectivity related functionality
 */
public class NetworkConnectionManager {
    private static final String TAG = "NWCONNMGR";

    /**
     * Function to return status of the Device Network connection
     *
     * @return boolean - True: has Network connection; False: no Network connection
     */
    public static boolean hasNetworkConnectivity(Context context) {
        if (context == null) {
            Log.e(TAG, "Unable to retrieve Application Service");
            return true; // Always return True for Error. Do not assume there is No connectivity in case of Error.
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager == null) {
            Log.e(TAG, "Unable to retrieve ConnectivityManager");
            return true; // Always return True for Error. Do not assume there is No connectivity in case of Error.
        }

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
