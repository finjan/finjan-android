/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.utils;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.List;

/**
 * Utility class to serialize object
 *
 * @author hui-joo.goh
 */
public class ObjectSerializerUtility {

    private static final String TAG = ObjectSerializerUtility.class.getSimpleName();

    /**
     * Method to serialize a serializable object
     *
     * @param serializableObject a serializable object
     * @return serialized string of the object provided. Returns null if Serializable param is null or if an error
     * happened
     */
    public static String serializeObjectToString(Serializable serializableObject) {
        if (serializableObject != null) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                objectOutputStream.writeObject(serializableObject);
                objectOutputStream.close();
                return encodeBytes(byteArrayOutputStream.toByteArray());
            } catch (IOException e) {
                Log.e(TAG, "Unable to serialize object " + e);
            }
        }

        return null;
    }

    /**
     * Method to deserialize string back into an object
     *
     * @param serializedObjectString a serialized object string
     * @return the object from the deserialized string. Returns null if string param is null or if an error happened
     */
    public static Object deserializeStringToObject(String serializedObjectString) {
        return deserializeStringToObject(serializedObjectString, null, null);
    }

    /**
     * Backward-compatible method to deserialize string back into an object if the serializable class has been renamed
     * or moved into a different package
     * <p/>
     * Note: This does not work for actual modification of the serializable class content
     *
     * @param oldSerializableClassNameList The list of serializable class name (e.g. com.avira.applock.library.data.LockedAppInfo) that was used to be converted into string previously
     * @param newSerializableClass         The renamed/moved serializable class
     * @return The object from the deserialized string. Returns null if string param is null or if an error happened
     */
    public static Object deserializeStringToObject(String serializedObjectString, List<String> oldSerializableClassNameList, Class<?> newSerializableClass) {
        if (serializedObjectString != null && !TextUtils.isEmpty(serializedObjectString)) {
            try {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                        decodeBytes(serializedObjectString));
                ObjectInputStream objectInputStream;
                if (oldSerializableClassNameList == null || newSerializableClass == null) {
                    objectInputStream = new ObjectInputStream(byteArrayInputStream);
                } else {
                    objectInputStream = new CustomObjectInputStream(byteArrayInputStream, oldSerializableClassNameList, newSerializableClass);
                }

                return objectInputStream.readObject();
            } catch (ClassNotFoundException e) {
                Log.e(TAG, "Unable to deserialize string " + e);
            } catch (StreamCorruptedException e) {
                Log.e(TAG, "Unable to deserialize string " + e);
            } catch (IOException e) {
                Log.e(TAG, "Unable to deserialize string " + e);
            }
        }

        return null;
    }

    /**
     * Method to encode the given byte array
     *
     * @param byteArrayToEncode the byte array to be encoded
     * @return encoded byte array in the form of string
     */
    private static String encodeBytes(byte[] byteArrayToEncode) {
        return new String(Base64.encode(byteArrayToEncode, Base64.DEFAULT));
    }

    /**
     * Method to decide the given string into a byte array
     *
     * @param stringToDecode the string to decode
     * @return the decoded string in the form of byte array
     */
    private static byte[] decodeBytes(String stringToDecode) {
        return Base64.decode(stringToDecode, Base64.DEFAULT);
    }

    /**
     * Custom {@link ObjectOutputStream} to cater for cases where the serializable object class has been renamed or moved into a
     * different package
     * <p/>
     * Reference: http://stackoverflow.com/questions/2358886/how-can-i-deserialize-the-object-if-it-was-moved-to-another-package-or-renamed/3916282#3916282
     */
    private static class CustomObjectInputStream extends ObjectInputStream {
        private final Class<?> mNewSerializableClass;
        private List<String> mOldSerializableClassNameList;

        /**
         * Custom ObjectInputStream to cater for cases where the serializable object class has been renamed or moved
         * into a different package
         *
         * @param input                        The non-null source InputStream to filter reads on.
         * @param oldSerializableClassNameList The list of serializable class name (e.g. com.avira.applock.library.data.LockedAppInfo) that was used to be converted into string previously
         * @param newSerializableClass         The renamed/moved serializable class
         * @throws IOException
         */
        public CustomObjectInputStream(InputStream input, List<String> oldSerializableClassNameList, Class<?> newSerializableClass) throws IOException {
            super(input);
            mOldSerializableClassNameList = oldSerializableClassNameList;
            mNewSerializableClass = newSerializableClass;
        }

        @Override
        protected ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
            ObjectStreamClass classDescriptor = super.readClassDescriptor();

            if (mOldSerializableClassNameList.contains(classDescriptor.getName())) {
                classDescriptor = ObjectStreamClass.lookup(mNewSerializableClass);
            }

            return classDescriptor;
        }
    }
}
