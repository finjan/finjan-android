/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AppOpsManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;

import com.avira.common.constants.CommonApps;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Utility class to obtain foreground application information
 *
 * @author hui-joo.goh
 */
public class ForegroundAppInfoUtility {
    private static String sCurrentAviraClassName = "";
    private AppOpsManager mAppOpsManager;
    private UsageStatsManager mUsageStatsManager;
    private final ActivityManager mActivityManager;
    private final String mAviraPackageName;
    private final OldAppInfoGetter mOldAppInfoGetter;
    private final AppInfoGetter mAppInfoGetter;
    private final AppInfoGetterUsingUsageAccess mAppInfoGetterUsingUsageAccess;
    private final PackageManager mPackageManager;

    public ForegroundAppInfoUtility(Context context) {
        mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        mPackageManager = context.getPackageManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            mUsageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE); // Context.USAGE_STATS_SERVICE
        }

        mAviraPackageName = context.getPackageName();
        mOldAppInfoGetter = new OldAppInfoGetter();
        mAppInfoGetter = new AppInfoGetter();
        mAppInfoGetterUsingUsageAccess = new AppInfoGetterUsingUsageAccess();
    }

    /**
     * Save current foreground Avira class name
     *
     * @param aviraClassName the current foreground Avira class name (e.g. com.avira.android.ClassA)
     */
    public static void saveCurrentAviraClassName(String aviraClassName) {
        sCurrentAviraClassName = aviraClassName;
    }

    /**
     * Check if the device requires usage access permission and
     * have the required permission for App Lock to obtain foreground app information
     *
     * @return True if the device require usage access permission, otherwise false.
     */
    public boolean needToRequestUsageAccess() {
        return requireUsageAccess() && !hasUsageAccess();
    }

    /**
     * Method to check if App Lock feature requires usage access (Settings > Security > Apps with usage access)
     *
     * @return True if App Lock feature requires usage access, otherwise false.
     */
    public boolean requireUsageAccess() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    /**
     * Check if user has granted Avira usage access permission (Settings > Security > Apps with usage access)
     * Reference: http://stackoverflow.com/questions/27215013/check-if-my-application-has-usage-access-enabled
     *
     * @return True if user has granted Avira usage access permission, otherwise false.
     */
    public boolean hasUsageAccess() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                ApplicationInfo applicationInfo = mPackageManager.getApplicationInfo(mAviraPackageName, 0);
                int mode = mAppOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid, applicationInfo.packageName);
                return (mode == AppOpsManager.MODE_ALLOWED);

            } catch (PackageManager.NameNotFoundException e) {}
        }
        return false;
    }

    /**
     * Method to get current foreground application information
     *
     * Note: Class name info is only obtainable in < API 20. For >= API 20, only Avira class name is obtainable
     *
     * @return The current foreground application information
     */
    public ForegroundAppInfo getForegroundAppInfo() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT_WATCH) {
            return getAppInfoByOrder(mOldAppInfoGetter, mAppInfoGetter);
        }

        if (requireUsageAccess() && hasUsageAccess()) {
            return getAppInfoByOrder(mAppInfoGetterUsingUsageAccess, mAppInfoGetter);
        }

        return getAppInfoByOrder(mAppInfoGetter, mOldAppInfoGetter);
    }

    /**
     * Get foreground app info via the specified primary method. If fails, then the secondary method will be executed next.
     *
     * @param primaryMethod   The primary method to obtain foreground app info
     * @param secondaryMethod The secondary method to obtain foreground app info if primary method fails
     * @return The current foreground application information
     */
    private ForegroundAppInfo getAppInfoByOrder(AppInfoGetter primaryMethod, AppInfoGetter secondaryMethod) {

        ForegroundAppInfo result = primaryMethod.getAppInfo();

        if (result == null) {
            result = secondaryMethod.getAppInfo();
        }

        if (result == null) {
            result = new ForegroundAppInfo("", "");
        }

        return result;
    }

    /**
     * Class containing implementation to obtain current foreground app information for devices with API 20 and above.
     * Class name info is only obtainable in < API 20 so extra handling is added here
     * to obtain class name for >= API 20 ONLY if the foreground app is Avira
     */
    private class AppInfoGetter {

        public ForegroundAppInfo getAppInfo() {

            String packageName = "";
            List<RunningAppProcessInfo> runningAppProcessInfoList = mActivityManager.getRunningAppProcesses();
            if (runningAppProcessInfoList != null && !runningAppProcessInfoList.isEmpty()) {

                packageName = (runningAppProcessInfoList.get(0)).processName;
                // No class info in >= Build.VERSION_CODES.KITKAT_WATCH
            }

            return getEnhancedAppInfo(packageName);
        }
    }

    /**
     * Class containing implementation to obtain current foreground app information
     * for devices that requires usage access permission (Settings > Security > Apps with usage access)
     *
     * Reference: http://stackoverflow.com/questions/27284205/how-to-get-list-of-recent-apps-with-android-api-21-lollipop?rq=1
     */
    private class AppInfoGetterUsingUsageAccess extends AppInfoGetter {

        public ForegroundAppInfo getAppInfo() {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String packageName = "";
                long currentTimeMillis = System.currentTimeMillis();
                List<UsageStats> queryUsageStats = mUsageStatsManager.queryUsageStats(0, currentTimeMillis - 10000, currentTimeMillis);
                if (queryUsageStats != null) {
                    SortedMap treeMap = new TreeMap();
                    for (UsageStats usageStats : queryUsageStats) {
                        treeMap.put(Long.valueOf(usageStats.getLastTimeUsed()), usageStats);
                    }
                    if (!(treeMap == null || treeMap.isEmpty())) {
                        packageName = ((UsageStats) treeMap.get(treeMap.lastKey())).getPackageName();
                    }
                }
                return getEnhancedAppInfo(packageName);
            }
            return null;
        }
    }

    /**
     * Method to:
     * 1. Apply unusual package name fixes (AAMA-1435)
     * 2. Since class name is not available in >= Build.VERSION_CODES.KITKAT_WATCH,
     * here we add some self-handling to get class name (we can only when the foreground is Avira app)
     *
     * @param packageName The current package name
     * @return The app info containing the current foreground package name with class name info if Avira is in the foreground
     */
    private ForegroundAppInfo getEnhancedAppInfo(String packageName) {
        if (!TextUtils.isEmpty(packageName)) {
            String className = "";

            // Own handling to obtain at least own Avira class name
            if (packageName.equalsIgnoreCase(mAviraPackageName)) {
                className = sCurrentAviraClassName;
            }

            // Fix issue where Avira is detected as Google Play (AAMA-1435)
            if (packageName.equalsIgnoreCase(CommonApps.PACKAGE_NAME_GOOGLE_PLAY_STORE)
                    && !TextUtils.isEmpty(sCurrentAviraClassName)) {
                packageName = mAviraPackageName;
                className = sCurrentAviraClassName;
            }

            return new ForegroundAppInfo(packageName, className);
        }
        return null;
    }

    /**
     * Class containing implementation to obtain current foreground app information for devices with API below 20
     */
    private class OldAppInfoGetter extends AppInfoGetter {

        @Override
        public ForegroundAppInfo getAppInfo() {

            try {

                List<RunningTaskInfo> runningTaskInfoList = mActivityManager.getRunningTasks(1);
                if (runningTaskInfoList != null && !runningTaskInfoList.isEmpty()) {

                    ComponentName componentName = runningTaskInfoList.get(0).topActivity;
                    return new ForegroundAppInfo(componentName.getPackageName(), componentName.getClassName());
                }

            } catch (SecurityException e) {
            }

            return null;
        }
    }

    /**
     * Structure to store information required from the foreground application
     *
     * @author hui-joo.goh
     */
    public static class ForegroundAppInfo {
        private String mForegroundPackageName;
        private String mForegroundClassName;

        public ForegroundAppInfo(String foregroundPackageName, String foregroundClassName) {
            mForegroundPackageName = foregroundPackageName;
            mForegroundClassName = foregroundClassName;
        }

        public ForegroundAppInfo() {
            mForegroundPackageName = "";
            mForegroundClassName = "";
        }

        /**
         * Get the package name
         *
         * @return the package name (e.g. com.avira.applock)
         */
        public String getPackageName() {
            return mForegroundPackageName;
        }

        /**
         * Get the class name
         *
         * @return the class name (e.g. com.avira.applock.ClassA)
         */
        public String getClassName() {
            return mForegroundClassName;
        }
    }
}