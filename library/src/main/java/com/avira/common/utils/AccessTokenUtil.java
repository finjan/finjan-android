package com.avira.common.utils;

import android.content.Context;
import android.text.TextUtils;

import com.avira.common.database.Settings;
import com.avira.common.id.HardwareId;

/**
 * utility class to generate the access token required for all RESTful calls
 *
 * @author ovidiu.buleandra
 */
public class AccessTokenUtil {

    /**
     * * generate an access token suitable to make queries on the REST interfaces exposed by the server
     *
     * @param context application context that will be used to retrieve the hardware id and the server device id
     * @param applicationId application id of the querying app usually retrieved with {@link com.avira.common.GeneralPrefs#getAppId(Context)}
     * @return valid token
     */
    public static String generateToken(Context context, String applicationId) {
        if(TextUtils.isEmpty(applicationId)) {
            return null;
        }
        String hardwareId = HardwareId.get(context); //new UserIdManager(context).getUserIdString();
        if(TextUtils.isEmpty(hardwareId)) {
            return null;
        }
        String serverDeviceId = Settings.readDeviceId();

        return generateToken(applicationId, hardwareId, serverDeviceId);
    }

    /**
     * generate an access token suitable to make queries on the REST interfaces exposed by the server
     *
     * @param applicationId application id of the querying app usually retrieved with {@link com.avira.common.GeneralPrefs#getAppId(Context)}
     * @param hardwareId hardware unique id (former local device id)
     * @param serverDeviceId the device number provided by the server at authentication
     * @return valid token
     */
    public static String generateToken(String applicationId, String hardwareId, String serverDeviceId) {
        return String.format("a-%s-%s-%s", applicationId, serverDeviceId, HashUtility.sha1(hardwareId));
    }
}
