/*******************************************************************************
File.......: $URL: $
Copyright (C) 1986-2011 Avira Operations GmbH & Co. KG. All rights reserved.

Revision...: $Revision: $
Modified...: $Date: $
Author.....: $Author: $
 *******************************************************************************/
package com.avira.common.utils;

import android.content.Context;

import com.avira.common.R;


/*
 * Class to provide URL formatting
 */
public class UrlFormatter
{
	/**
	 * Adds language code to the URL based on the device OS language and the application's supported languages
	 *
	 * @param context valid context for resource retrieving
	 * @param domain the domain portion of the URL
	 * @param path the path portion of the URL
	 * 
	 * @return String value containing the full URL
	 */
	public static String formatWithLanguageCode(Context context, String domain, String path)
	{
		String url = String.format(context.getString(R.string.UrlFormatter), domain,
		    DeviceInfo.getLanguageCode(context), path);
		return url;
	}

	//@formatter:off
	/**
	 * Returns the formatted Avira Answers URL that includes the device's language code and OS version.
	 * 
	 * @return Formatted Avira Answers URL
	 * 		   Example: http://redirect.avira.com/?operationtype=redirects&type=expert&redirectcontext=menu&lngprod=en&productid=302&osversion=4.2
	 * 					where
	 * 					lngprod: supported language code, EN for everything else
	 * 					productid: 302
	 * 					osversion: current OS version of the device. E.g. 4.4.2, 2.3.6, 2.2, etc...
	 * 					redirectcontext: menu
	 */
	//@formatter:on
	public static String getFormattedSupportURL(Context context)
	{
		String unformattedAviraAnswersURL = context.getString(R.string.avira_support_url);
		String languageCode = DeviceInfo.getLanguageCode(context).toLowerCase();
		if (!languageCode.equals("de"))
		{
			languageCode = "en";
		}

		return String.format(unformattedAviraAnswersURL, languageCode);
	}
}
