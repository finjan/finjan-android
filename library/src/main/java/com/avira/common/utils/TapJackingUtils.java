/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;

import java.util.List;

/**
 * Created by daniela.stamati on 5/11/2015.
 */
public class TapJackingUtils {

    //white listed apps that have android.permission.SYSTEM_ALERT_WINDOW
    private static final String[] WHITE_LISTED_APPS = {"com.urbandroid.lux", "jp.ne.hardyinfinity.bluelightfilter.free"};

    public static boolean isEnvironmentTapJackingSafe(Context context) {

        ActivityManager manager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processes = manager.getRunningAppProcesses();

        if(processes==null || processes.size() == 0){
            return false;
        }

        for (ActivityManager.RunningAppProcessInfo process : processes) {
            for (String whiteListedAppName : WHITE_LISTED_APPS) {
                if (whiteListedAppName.equals(process.processName)) {
                    return true;
                }
            }
        }
        return false;
    }

}
