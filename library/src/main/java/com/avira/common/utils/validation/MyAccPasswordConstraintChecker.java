package com.avira.common.utils.validation;

import java.util.HashSet;
import java.util.Set;

public class MyAccPasswordConstraintChecker {
    private final static int PASSWORD_MIN_LENGTH = 8;
    private final static int PASSWORD_MAX_LENGTH = 100;
    private Set<String> mForbiddenCharacters = new HashSet<String>();

    public MyAccPasswordConstraintChecker() {
        mForbiddenCharacters.add("^");
        mForbiddenCharacters.add(":");
        mForbiddenCharacters.add(".");
        mForbiddenCharacters.add("<");
        mForbiddenCharacters.add(">");
        mForbiddenCharacters.add("\"");
        mForbiddenCharacters.add("+");
        mForbiddenCharacters.add("&");
        mForbiddenCharacters.add("%");
        mForbiddenCharacters.add("$");
        mForbiddenCharacters.add("#");
        mForbiddenCharacters.add("*");
        mForbiddenCharacters.add("/");
        mForbiddenCharacters.add("\\");
    }

    public boolean checkIfPasswordLengthIsCorrect(String password) {
        boolean result = false;
        int passwordLength = password.length();
        if (passwordLength >= PASSWORD_MIN_LENGTH && passwordLength <= PASSWORD_MAX_LENGTH) {
            result = true;
        }
        return result;
    }

    public boolean checkIfPasswordLengthSatisfyMinLength(String password) {
        boolean result = false;
        if (password.length() >= PASSWORD_MIN_LENGTH) {
            result = true;
        }
        return result;
    }

    //TODO: the password length should be limited from the xml file + no one has passwords reaching 100chars
    public boolean checkIfPasswordLengthSatisfyMaxLength(String password) {
        boolean result = false;
        if (password.length() <= PASSWORD_MAX_LENGTH) {
            result = true;
        }
        return result;
    }

    public boolean checkIfPasswordSatisfyCharactersConstraint(String password) {
        boolean result = true;
        for (String forbiddenChar : mForbiddenCharacters) {
            if (password.contains(forbiddenChar)) {
                result = false;
                break;
            }
        }
        return result;
    }

    public boolean checkIfPasswordContainLogwerChar(String password) {
        boolean result=false;
        for (int i=0;i<password.length();i++){
            if(Character.isLowerCase(password.charAt(i))){
                result=true;
            }
        }
        return result;
    }
    public boolean checkIfPasswordContainUpperChar(String password) {
        boolean result=false;
        for (int i=0;i<password.length();i++){
            if(Character.isUpperCase(password.charAt(i))){
                result=true;
            }
        }
        return result;
    }
    public boolean checkIfPasswordContainNumber(String password) {
        boolean result=false;
        for (int i=0;i<password.length();i++){
            if(Character.isDigit(password.charAt(i))){
                result=true;
            }
        }
        return result;
    }
}
