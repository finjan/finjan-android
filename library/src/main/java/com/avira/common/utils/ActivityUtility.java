/*
 * Copyright (C) 1986-2016 Avira GmbH. All rights reserved.
 */

package com.avira.common.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Class to store re-usable utility methods related to activity.
 *
 * Created by hui-joo.goh on 15/03/16.
 */
public class ActivityUtility {
    private static final String TAG = ActivityUtility.class.getSimpleName();

    /**
     * Starts the specified intent with exception handling.
     *
     * @param context
     * @param intent The intent to be launched
     * @return True if the intent is launched successfully, otherwise false
     */
    public static boolean startActivity(Context context, Intent intent) {
        try {
            context.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException | SecurityException e) {
            Log.e(TAG, "Failed to startActivity: ", e);
            return false;
        }
    }
}
