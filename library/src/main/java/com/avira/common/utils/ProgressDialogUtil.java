package com.avira.common.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;

import com.avira.common.R;

import java.lang.ref.WeakReference;

/**
 * @author ovidiu.buleandra
 * @since 27.10.2015
 */
public class ProgressDialogUtil {
    private ProgressDialog mProgressDialog;
    private WeakReference<Context> mContextRef;

    public ProgressDialogUtil(Context context) {
        mContextRef = new WeakReference<>(context);
    }

    public void show(String text) {
//        dismiss();
        Context context = mContextRef.get();
        if(context != null) {
            mProgressDialog =new ProgressDialog(context, R.style.VpnProgressDialog);
        }
        if(!TextUtils.isEmpty(text)){
            mProgressDialog.setMessage(text);
        }
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
    }

    public void dismiss() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog = null;
    }
}
