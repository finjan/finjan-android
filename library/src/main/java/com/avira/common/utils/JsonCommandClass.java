/*******************************************************************************
 * File.......: $URL: $
 * Copyright (C) 1986-2011 Avira GmbH. All rights reserved.
 * <p/>
 * Revision...: $Revision: $
 * Modified...: $Date: $
 * Author.....: $Author: $
 *******************************************************************************/

package com.avira.common.utils;

import android.util.Log;

import java.lang.reflect.Field;

/*
 * Abstract Json command class, to provide basic functionalities for the extended class.
 */
public abstract class JsonCommandClass {

    private static final String TAG = JsonCommandClass.class.getSimpleName();

    /*
     * Validate a particular key from json
     *
     * @param key Input key from json
     *
     * @return True if valid key, else False.
     */
    public boolean validateJsonKey(String key) {
        boolean result = false;
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if (field.get(null).toString().equals(key)) {
                    result = true;
                    break;
                }
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "IllegalArgumentException", e);
            } catch (IllegalAccessException e) {
                Log.e(TAG, "IllegalAccessException", e);
            } catch (NullPointerException e) {
                Log.e(TAG, "NullPointerException", e);
            }
        }
        return result;
    }
}
