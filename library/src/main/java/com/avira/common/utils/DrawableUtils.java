package com.avira.common.utils;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

public class DrawableUtils {

    public static void tintDrawable(Drawable drawable, int color) {
        if (drawable != null)
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    /**
     * forces a color on a drawable
     *
     * @param drawable
     * @param color
     */
    public static void colorizeDrawable(Drawable drawable, int color) {
        if (drawable != null)
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }

    /**
     * Blend {@code color1} and {@code color2} using the given ratio.
     *
     * @param ratio of which to blend. 1.0 will return {@code color1}, 0.5 will give an even blend, 0.0 will return
     *              {@code color2}.
     */
    public static int blendColors(int color1, int color2, float ratio) {
        final float inverseRation = 1f - ratio;
        float r = (Color.red(color1) * ratio) + (Color.red(color2) * inverseRation);
        float g = (Color.green(color1) * ratio) + (Color.green(color2) * inverseRation);
        float b = (Color.blue(color1) * ratio) + (Color.blue(color2) * inverseRation);
        return Color.rgb((int) r, (int) g, (int) b);
    }

    public static int setAlpha(int color, float alpha) {
        int a = (int) (254 * alpha);
        return Color.argb(a, Color.red(color), Color.green(color), Color.blue(color));
    }
}
