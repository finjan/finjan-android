package com.avira.common.utils.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailConstraintChecker
{
	// rfc1035 pg 7, format for subdomain
	// rfc821 pg 29, format for email address
	private static final String LETTER = "[a-zA-Z]";
	// private static final String DIGIT = "[0-9]";
	private static final String LETTER_DIGIT = "[0-9a-zA-Z]";
	private static final String LETTER_DIGIT_HYPHEN = "(?:[0-9a-zA-Z._-])";
	private static final String QUOTEDSTRING = "(?:[\"\\\"](?:[^\\\"]|(?:[\\\\][\\\"]))*[\\\"])";
	private static final String ATOM = "(?:[\\!\\#-\\\'\\*\\+\\-\\/-9\\=\\?A-Z\\^-\\~]+)";
	private static final String SUBDOMAIN = "(?:" + LETTER_DIGIT + "(?:" + LETTER_DIGIT_HYPHEN + "*" + LETTER_DIGIT
	    + ")?)";
	private static final String MAINDOMAIN = LETTER + "*";
	private static final String WORD = "(?:" + ATOM + "|" + QUOTEDSTRING + ")";
	private static final String DOMAIN = "(?:" + SUBDOMAIN + "(?:[\\.]" + MAINDOMAIN + ")+)";
	private static final String LOCALPART = "(?:" + WORD + "(?:[\\.]" + WORD + ")*)";
	private static final String EMAIL = "(?:" + LOCALPART + "[\\@]" + DOMAIN + ")";
	// private static final String EMAIL_ADDRESS = "^" + EMAIL + "$";
	private static final String EMAIL_REGEX = "^(?:" + EMAIL + "?)$";

	/*
	 * Check the validity of the specified email address
	 * 
	 * @param String email address to be validated
	 * 
	 * @return TRUE if valid email format, else FALSE
	 */
	public static boolean isValidEmailFormat(String emailAddress)
	{
		Pattern pattern = Pattern.compile(EMAIL_REGEX);
		Matcher matcher = pattern.matcher(emailAddress);
		boolean isValidFormat = matcher.matches() && emailAddress.length() > 0;
		return isValidFormat;
	}
}
