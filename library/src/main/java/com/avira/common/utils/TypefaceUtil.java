/*******************************************************************************
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 *******************************************************************************/

package com.avira.common.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class TypefaceUtil {

    /**
     * Using reflection to override default typeface NOTICE: DO NOT FORGET TO SET TYPEFACE FOR APP THEME AS DEFAULT
     * TYPEFACE WHICH WILL BE OVERRIDDEN
     *
     * @param context                    to work with assets
     * @param defaultFontNameToOverride  for example "monospace"
     * @param customFontFileNameInAssets file name of the font from assets
     */
    @SuppressWarnings("unchecked")
    public static void overrideFont(Context context, String defaultFontNameToOverride,
                                    String customFontFileNameInAssets) throws Exception {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(),
                    customFontFileNameInAssets);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                final Field staticField = Typeface.class.getDeclaredField("sSystemFontMap");
                staticField.setAccessible(true);
                Map<String, Typeface> fontMap = (Map<String, Typeface>) staticField.get(null);
                for(Map.Entry<String, Typeface> fontPair: fontMap.entrySet())
                    fontPair.setValue(customFontTypeface);

                staticField.set(null, fontMap);
            }
            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);
    }
}
