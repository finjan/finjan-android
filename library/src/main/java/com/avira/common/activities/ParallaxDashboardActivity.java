/*******************************************************************************
 * Copyright (C) 1986-2014 Avira GmbH. All rights reserved.
 *******************************************************************************/
package com.avira.common.activities;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.avira.common.R;
import com.avira.common.ui.ux.ParallaxDashboardView;
import com.avira.common.ui.ux.ParallaxListViewDashboard;
import com.avira.common.ui.ux.ParallaxScrollViewDashboard;
import com.avira.common.utils.ViewUtility;

import java.util.Arrays;

/**
 * Dashboard Activity with parallax effect
 *
 * @author yushu
 */
public class ParallaxDashboardActivity extends com.avira.common.activities.BaseFragmentActivity {
    private int[] mLayoutId = new int[ViewStubType.values().length];
    private ParallaxDashboardView mDashboard = null;

    private boolean mStickyToolbar;
    private boolean mStickyNotificationBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Arrays.fill(mLayoutId, View.NO_ID);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        mDashboard = inflateParallaxDashboard();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        mDashboard = inflateParallaxDashboard();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        mDashboard = inflateParallaxDashboard();
    }

    /**
     * Set the content layout in the parallax dashboard
     *
     * @param resId layout resource ID
     */
    public void setParallaxContentLayout(int resId) {
        mLayoutId[ViewStubType.CONTENT.getIndex()] = resId;
    }

    /**
     * Set the header layout in the parallax dashboard
     *
     * @param resId layout resource ID
     */
    public void setParallaxHeaderLayout(int resId) {
        mLayoutId[ViewStubType.HEADER.getIndex()] = resId;
    }

    /**
     * Set the header background layout in the parallax dashboard
     *
     * @param resId layout resource ID
     */
    public void setParallaxBackgroundLayout(int resId) {
        mLayoutId[ViewStubType.HEADER_BACKGROUND.getIndex()] = resId;
    }

    /**
     * Set the toolbar layout in the parallax dashboard
     *
     * @param resId    layout resource ID
     * @param isSticky True for the view to always stay visible in the screen whenever scrolled. False to hide the view when
     *                 the parent activity is scrolled down.
     */
    public void setParallaxToolbarLayout(int resId, boolean isSticky) {
        mLayoutId[ViewStubType.TOOLBAR.getIndex()] = resId;
        mStickyToolbar = isSticky;
    }

    /**
     * Set the notification bar layout in the parallax dashboard
     *
     * @param resId    layout resource ID
     * @param isSticky True for the view to always stay visible in the screen whenever scrolled. False to hide the view when
     *                 the parent activity is scrolled down.
     */
    public void setParallaxNotificationBarLayout(int resId, boolean isSticky) {
        mLayoutId[ViewStubType.NOTIFICATION_BAR.getIndex()] = resId;
        mStickyNotificationBar = isSticky;
    }

    /**
     * Return the inflated dashboard
     *
     * @return inflated dashboard, null if not inflated
     */
    protected ParallaxDashboardView getParallaxDashboard() {
        return mDashboard;
    }

    private ParallaxDashboardView inflateParallaxDashboard() {
        // Automatically determine which type of parallax dashboard it is (ScrollView or ListView parallax dashboard)
        ParallaxDashboardView dashboard = (ParallaxScrollViewDashboard) findViewById(R.id.pxscrollview_dashboard_container);
        if (dashboard == null) {
            dashboard = (ParallaxListViewDashboard) findViewById(R.id.pxlistview_dashboard_container);
        }

        if (dashboard == null) {
            throw new IllegalStateException("Parallax dashboard layout has to be inflated before calling this method");
        } else {
            dashboard.inflateInnerLayouts(mLayoutId[ViewStubType.CONTENT.getIndex()],
                    mLayoutId[ViewStubType.HEADER.getIndex()], mLayoutId[ViewStubType.HEADER_BACKGROUND.getIndex()],
                    mLayoutId[ViewStubType.TOOLBAR.getIndex()], mLayoutId[ViewStubType.NOTIFICATION_BAR.getIndex()]);

            dashboard.setNotificationBarStickiness(mStickyNotificationBar);
            dashboard.setToolbarStickiness(mStickyToolbar);
        }

        return dashboard;
    }

    /**
     * Fetches GO_TO_VIEW_INTENT_EXTRA from intent and scrolls to the id if it finds it
     */
    protected void scrollToIntentTargetIfAny() {
        int view_to_scroll_to = getIntent().getIntExtra(ViewUtility.GO_TO_VIEW_INTENT_EXTRA, -1);
        if (view_to_scroll_to > 0) {
            mDashboard.scrollToViewId(view_to_scroll_to);
        }
    }


    public static enum ViewStubType {
        CONTENT, HEADER, HEADER_BACKGROUND, TOOLBAR, NOTIFICATION_BAR;

        public int getIndex() {
            return ordinal();
        }
    }
}
