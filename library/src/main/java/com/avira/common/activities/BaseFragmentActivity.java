/*******************************************************************************
 * \Copyright (C) 1986-2014 Avira GmbH. All rights reserved.
 *******************************************************************************/
package com.avira.common.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import com.avira.common.utils.ForegroundAppInfoUtility;
import com.avira.common.utils.TapJackingUtils;
import com.avira.common.utils.ThreadUtilities;

/**
 * Base fragment activity class for Avira Android Security
 */
public abstract class BaseFragmentActivity extends FragmentActivity {
    private BaseFragmentActivityBroadcastReceiver mBaseFragmentActivityBroadcastReceiver;

    @SuppressLint("InlinedApi")
    public static Intent addFlagToClearBackstack(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        return intent;
    }

    public BaseFragmentActivity getActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mBaseFragmentActivityBroadcastReceiver = new BaseFragmentActivityBroadcastReceiver();
        IntentFilter filter = new IntentFilter(BaseFragmentActivityBroadcastReceiver.APPLICATION_TERMINATION_ACTION);
        registerReceiver(mBaseFragmentActivityBroadcastReceiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();


//            View rootView = getWindow().getDecorView();
//            if (TapJackingUtils.isEnvironmentTapJackingSafe(this)) {
//                rootView.setFilterTouchesWhenObscured(false);
//            } else {
//                rootView.setFilterTouchesWhenObscured(true);
//            }


        ForegroundAppInfoUtility.saveCurrentAviraClassName(getClass().getName());
    }

    @Override
    protected void onPause() {
        super.onPause();

        ForegroundAppInfoUtility.saveCurrentAviraClassName("");
    }

    @Override
    protected void onDestroy() {
        unregisterCustomReceiver(mBaseFragmentActivityBroadcastReceiver);
        super.onDestroy();
    }

    protected void unregisterCustomReceiver(BroadcastReceiver receiver) {
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
        }
    }

    /**
     * Terminate application by killing all opened activity
     */
    public void terminateApplication() {
        Intent intent = new Intent(BaseFragmentActivityBroadcastReceiver.APPLICATION_TERMINATION_ACTION);
        sendBroadcast(intent);
    }

    protected void runWithoutBlockingUiThread(Runnable runnable) {
        ThreadUtilities.runOnThread(runnable);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    protected String getTextFromEditTextWithNullCheck(EditText editText) {
        return editText != null ? editText.getText().toString() : "";
    }

    protected void openActivityWithNewTaskFlag(final Class<?> classToStart) // NO_UCD (unused code)
    {
        Intent createAccountIntent = new Intent(BaseFragmentActivity.this, classToStart);
        createAccountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(createAccountIntent);
    }

    /**
     * Broadcast receiver to listen and terminate particular activity
     */
    private class BaseFragmentActivityBroadcastReceiver extends BroadcastReceiver {
        public final static String APPLICATION_TERMINATION_ACTION = "com.avira.android.action.APPLICATION_TERMINATION";

        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }
}