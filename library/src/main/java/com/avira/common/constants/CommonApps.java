/*
 * Copyright (C) 1986-2015 Avira GmbH. All rights reserved.
 */

package com.avira.common.constants;

import android.content.Context;

import com.avira.common.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class to keep track of the list of recommended application package names
 *
 * @author hui-joo.goh
 */
public class CommonApps {
    // Recommended application package names
    public static final String PACKAGE_NAME_GOOGLE_PLAY_STORE = "com.android.vending";
    public static final String PACKAGE_NAME_NATIVE_SETTINGS = "com.android.settings";
    private static final String PACKAGE_NAME_AMAZON_APPS = "com.amazon.venezia";
    private static final String PACKAGE_NAME_SAMSUNG_APPS = "com.sec.android.app.samsungapps";
    private static final String PACKAGE_NAME_PACKAGE_INSTALLER = "com.android.packageinstaller";
    //package installer package name for android L
    private static final String PACKAGE_NAME_PACKAGE_INSTALLER_L = "com.google.android.packageinstaller";
    private static final Integer NO_CUSTOM_DESCRIPTION = null;
    /**
     * Map recommended app package name to it's respective custom description resource id. If no custom description, put
     * NO_CUSTOM_DESCRIPTION as the value
     */
    private static Map<String, Integer> sRecommendedAppsMap = new HashMap<String, Integer>() {
        {
            put(PACKAGE_NAME_GOOGLE_PLAY_STORE, R.string.recommended_app_prevent_install_or_uninstall);
            put(PACKAGE_NAME_AMAZON_APPS, R.string.recommended_app_prevent_install_or_uninstall);
            put(PACKAGE_NAME_SAMSUNG_APPS, R.string.recommended_app_prevent_install_or_uninstall);
            put(PACKAGE_NAME_PACKAGE_INSTALLER, R.string.recommended_app_prevent_uninstall);
            put(PACKAGE_NAME_NATIVE_SETTINGS, R.string.recommended_app_prevent_uninstall_or_settings_change);
            put(PACKAGE_NAME_PACKAGE_INSTALLER_L, R.string.recommended_app_prevent_uninstall);
        }
    };

    /**
     * Check if the specified application is a recommended application
     *
     * @param packageName the package name (e.g. com.avira.applock)
     * @return true if the specified application is a recommended app. Otherwise false
     */
    public static boolean isRecommendedApp(String packageName) {
        return sRecommendedAppsMap.containsKey(packageName);
    }

    /**
     * Get the custom description that is mapped to the specified recommended application
     *
     * @param context
     * @param packageName the package name (e.g. com.avira.applock)
     * @return the custom description that is mapped to the specified recommended application
     */
    public static String getCustomDescription(Context context, String packageName) {
        String customDescString = "";
        Integer customDescResId = sRecommendedAppsMap.get(packageName);
        if (customDescResId != NO_CUSTOM_DESCRIPTION) {
            customDescString = context.getString(customDescResId);
        }

        return customDescString;
    }

    public static Set<String> getRecommendedPackageList() {
        return sRecommendedAppsMap.keySet();
    }
}
