package com.avira.common.utils;

import com.avira.common.utils.HashUtility;

import junit.framework.Assert;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by yushu on 1/22/16.
 */
public class  HashUtilityTest {

    @Test
    public void shouldReturnCorrectSha1(){

        Assert.assertEquals("86f7e437faa5a7fce15d1ddcb9eaeaea377667b8", HashUtility.sha1("a"));

    }

}
