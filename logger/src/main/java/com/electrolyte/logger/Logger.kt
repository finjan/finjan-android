package com.electrolyte.logger

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import androidx.core.content.FileProvider
import android.util.Log
import android.widget.Toast
import com.electrolyte.utils.DateHelper
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

class Logger {

    companion object {
        private var publishLog = false
        fun initLogger(publishLog: Boolean) {
            Logger.publishLog = publishLog
        }


        fun logV(key: String, value: String) {

            if (BuildConfig.DEBUG || publishLog) {
                Log.v(key, "->$value")
            }
            if (publishLog) {
                saveToFile(key, value)
            }
        }

        fun logD(key: String, value: String) {
            if (BuildConfig.DEBUG || publishLog) {
                Log.d(key, "->$value")
            }
            if (publishLog) {
                saveToFile(key, value)
            }
        }

        fun logE(key: String, value: String) {
            if (BuildConfig.DEBUG || publishLog) {
                Log.e(key, "->$value")
            }
            if (publishLog) {
                saveToFile(key, value)
            }
        }


        fun logI(key: String, value: String) {
            if (BuildConfig.DEBUG || publishLog) {
                Log.i(key, "->$value")
            }
            if (publishLog) {
                saveToFile(key, value)
            }
        }

        public fun saveToFile(title: String, message: String) {
            val yourFilePath = InitLib.context.getFilesDir().path + "/" + "invincibull_logs.txt"
            val file = File(yourFilePath)
            val exStorageState = Environment.getExternalStorageState()
            if (Environment.MEDIA_MOUNTED == exStorageState) {

                try {
                    //                File root = Environment.getExternalStorageDirectory();
                    //                String path = root + "/InvinciBull/";
                    val exists = file.exists()
                    if (!exists) {
                        File(InitLib.context.getFilesDir().path + "").mkdirs()
                    }
                    //                File logFile = new File(path + "InvinciBull_traces.txt");
//                    val isNew = file.createNewFile()
                    val logWriter = FileWriter(file, true)
                    val outer = BufferedWriter(logWriter)
                    outer.write("\r\n")
                    outer.write("\r\n")
                    outer.write("\r\n")
                    outer.write("Time: " + DateHelper.getInstnace().getCurrentDate(DateHelper.SERVER_FORMAT_3))
                    outer.write("\r\n")
                    outer.write("Title: $title")
                    outer.write("\r\n")
                    outer.write("Message: $message")
                    outer.write("\r\n")
                    outer.write("------------------------------------")

                    outer.close()
                    logWriter.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                    //          Toast.makeText(context, "Couldn't save", Toast.LENGTH_SHORT);
                }

            } else {
                //FAIL
                println("File not accessible")
                // Toast.makeText(context, "File not accessible", Toast.LENGTH_SHORT).show();
            }
        }


        fun sendEmailOfLog(context: Context) {

            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf<String>())
            intent.putExtra(Intent.EXTRA_SUBJECT, "InvinciBull Debug Logs")
            intent.putExtra(Intent.EXTRA_TEXT, "Please find attached Log file")

            val yourFilePath = context.getFilesDir().path + "/" + "invincibull_logs.txt"

            val file = File(yourFilePath)
            if (!file.exists() || !file.canRead()) {
                Toast.makeText(context, "Attachment Error", Toast.LENGTH_SHORT).show()
                return
            }
            var outputFileUri: Uri? = null
            outputFileUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file)

            if (intent.resolveActivity(context.getPackageManager()) != null) {
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                intent.putExtra(Intent.EXTRA_STREAM, outputFileUri)
                val resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
                for (resolveInfo in resInfoList) {
                    val packageName = resolveInfo.activityInfo.packageName
                    context.grantUriPermission(packageName, outputFileUri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
                }
                context.startActivity(Intent.createChooser(intent, "Send Logs via..."))
            }
        }
    }
}
    

  

    
